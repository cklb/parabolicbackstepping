%% Simulation academic example:
% this script creates the results  presented in 
% Examples 2.7.1, 3.7.1 and 3.8.6.


%% 1. Add folders to path
% The main part of the used files is located in the folder Lib, which has a structure based on
% namespaces.
addpath(genpath([pwd '\' 'Lib'])); 

% The common Library "conI" of the inifinite-dimensional systems group of the Chair of
% Automatic Control, FAU Erlangen contains various miscellanious functions which are used.
% Therefore, the state of the library at commit ... is located in the folder conI
addpath(genpath([pwd '\' 'conI']));


%% Store some auxiliary variables
% Start message:
formdate='dd.mm. HH:MM:SS';
fprintf(['Starting simulationAcademicExample [' datestr(now,formdate) ']...\n']);

% this path can be used to save and store mat files
myPath = 'Lib/+dps/+parabolic/+mat/+diss/+exampleAcademic/'; % path to save and load mat-files
if ~exist(myPath,'dir')
	mkdir(myPath);
end

plotPath = 'Lib/+dps/+parabolic/+plot/+diss/+exampleAcademic/'; % plot to save exported figures
if ~exist(plotPath,'dir')
	mkdir(plotPath);
end

% number of simulations to be performed 
% open-loop, state feedback, observerbased, state feedback mu 6, state feedback mu 10
plots = [1 2 3 4 5];

% change whether the created figures shall be exported 2 tex.
export= 0;

simSet{1}.name = 'OpenLoop';
simSet{2}.name = 'StateFeedback';
simSet{3}.name = 'ObserverBased';
simSet{4}.name = 'StateFeedback2';
simSet{5}.name = 'StateFeedback3';

	
%% the resolutions of the kernels in original coordinates are stored first.
% the structure simSet is just a container for the simulation settings that are used at
% different points.
for k=plots 
	simSet{k}.ndiscKernel = 31;
	simSet{k}.ndiscObsKernel = 51;
end


%% Configure simulation settings
% now further simulation settings are added to simSet. 
% for better readability, this is externalized into a function.
for k=[1 plots]
	simSet{k} = dps.parabolic.script.diss.simulationSettings.simulationSettingsAcademicExmaple(simSet{k});
end

%% configure plant
% the creation of the plant(s) is externalized to a function as well.
% the many arguments returned are all contained in the ppide itself and are only passed to
% easily export them to latex.
for k = plots
	% not efficient but parameters can be exported to tex in this format.
	[ppide{k}, lambda{k},a{k},mu{k},a0{k},F{k},g_strich{k},bDisc0,tBump0,bDisc1,tBump1] = dps.parabolic.script.diss.examples.exampleAcademic(simSet{k});
	% very important is the object ppide, which is a 
	% dps.parabolic.syste.ppide_sys and contains all the information that defines the system to
	% be controlled. For more details look into the above function!
end
% overwrite the reaction matrix of the target system for simulations 4 and 5
ppide{4}.ctrl_pars.mu = 4*eye(ppide{4}.n);
ppide{5}.ctrl_pars.mu = 16*eye(ppide{4}.n);

%% 1. Simulate 
% now the actual calculations and simulations are performed. 
% To this end, objects of the class
% dps.parabolic.control.backstepping.ppide_simulation
% are created. These objects contain all methods needed to calculate controllers and observers
% and perform simulations.

% The constructor of this class accepts a dps.parabolic.system.ppide_sys as first input and
% name-value-pairs after that. To keep the overview, these name-value-pairs, which acutally
% configure the performed simulations, are stored into variables simpars first.

% All parameters that are set to zero could also be omitted but are listed to show some more
% possible options.
% Funcionalities concerning output regulation or robust output regulation only work for
% time-invariant systems.

% The first simulation is the open-loop system:
simpars{1} = {...
	'x0',ppide{1}.x0,... % initial condition must be passed separately, though stored in the ppide
	'simPars',simSet{1}.sim_pars,... % the simulation settings concerning the solver
	'plotPars',simSet{1}.plot_pars,... % paramters to plot functions
	'useReferenceObserver',0,... % Deactivate a reference observer 
	'useDisturbanceObserver',0,... % Deactivate disturbance observer
	'useStateObserver',0,... % Deactivate state observer
	'useStateFeedback',0,... % Deactivate state feedback controller
	'useOutputRegulation',0,... % Deactivate disturbance and reference feedforward control 
	'useIntMod',0,... % Deactivate internal model 
	'name',simSet{1}.name,... % Name
	'path',myPath % A path can be stored to use later for saving the variable.
};

% second simulation: activated state feedback control  and the state observer, but observer is
% not used for feedback
simpars{2} = {...
	'x0',ppide{2}.x0,...
	'simPars',simSet{2}.sim_pars,...
	'plotPars',simSet{2}.plot_pars,...
	'useReferenceObserver',0,...
	'useDisturbanceObserver',0,...
	'useStateObserver',1,...  % state observer is activated
	'useObserverInFeedback',0,... % but not used in feedback
	'useStateFeedback',1,... % controller is activated
	'useOutputRegulation',0,...
	'useIntMod',0,...
	'name',simSet{2}.name,...
	'path',myPath
};

% third simulation: use observer in feedback 
simpars{3} = {...
	'x0',ppide{3}.x0,...
	'simPars',simSet{3}.sim_pars,...
	'plotPars',simSet{3}.plot_pars,...
	'useReferenceObserver',0,...
	'useDisturbanceObserver',0,...
	'useStateObserver',1,...
	'useObserverInFeedback',1,... % use observer for state feedback controller
	'useStateFeedback',1,...
	'useOutputRegulation',0,...
	'useIntMod',0,...
	'name',simSet{3}.name,...
	'path',myPath
};

% simulations 4 and 5 have different mu but use the same simulation settings a the second
% simulation.
simpars{4} = simpars{2};
simpars{5} = simpars{2};

% creating the simulation objects is performed by 
% simulations{k} = dps.parabolic.control.backstepping.ppide_simulation(ppide{k},simpars{k});
% However, if the object already exists, it is a lot faster to only check if the parameters
% have changed. This is performed by 
% simulations{k} = simulations{k}.setPars(ppide{k},simpars{k});
% Attention: there may still be bugs in these checks and its always safer to create new
% objects.
% Moreover, if simulation 1 has already been created, it is faster to create the second
% simulation as a copy of the first and then adjust the parameters by
% simulations{k} = simulations{k}.setPars(ppide{k},simpars{k});
% whcih is performed next.
for k=plots
	disp(['k=' num2str(k) ':...'])
	if ~exist('simulations','var')
		simulations{k} = dps.parabolic.control.backstepping.ppide_simulation(ppide{k},simpars{k});
	else
		if length(simulations) > k-1 && ~isempty(simulations{k}) && isa(simulations{k},'dps.parabolic.control.backstepping.ppide_simulation')
			simulations{k} = simulations{k}.setPars(ppide{k},simpars{k});
		else
			i=1;
			while i<k
				if ~isempty(simulations{k-i}) && isa(simulations{k-i},'dps.parabolic.control.backstepping.ppide_simulation')
					simulations{k} = simulations{k-i};
					simulations{k} = simulations{k}.setPars(ppide{k},simpars{k});
					break
				end
				i=i+1;
			end
			if i==k % no other simulations is available
				simulations{k} = dps.parabolic.control.backstepping.ppide_simulation(ppide{k},simpars{k});
			end
		end
	end
	% Finally, the simulation is initialized, which amounts to calculating required controller
	% and observer gains and everything that is needed to perform the simulation.
	% indeed it is not necessary to call this method separately, because the simulation
	% simulations{k}.simulate
	% would automatically notice, if the simulation ha not yet been initialized. 
	% This was used here for saving the calculated controller/observer kernels but is no more
	% needed due to the short computation times.
	if simulations{k}.controllerKernelFlag
		simulations{k} = simulations{k}.initializeSimulation();
	else
		simulations{k} = simulations{k}.initializeSimulation();
% 		save(simulations{k}.saveStr,'simulations')
	end
	if ~simulations{k}.simulated
		simulations{k} = simulations{k}.simulate;
	end
end


%% Create plots. 
% now everything has been computed and the results of the simulation may be looked at.
% To this end, some predefined functions in the ppide_simulation allow simple plots.

% The created figures contain the contents which are printed in the thesis. However, they may
% have a different format. In the end, only the created datapoints were used for the plots in
% the thesis.

%% plot the gevrey functions
figure('Name','StepFunction')
plot(tBump0,bDisc0);
figure('Name','BumpFunction')
plot(tBump1,bDisc1);

%% parameters for the mesh/surf plots
% the number of points in z and t direction stored in local variables.
meshZ = simSet{1}.plot_pars.meshZ;
meshT = simSet{1}.plot_pars.meshT;

%% plot open-loop states
simulations{1}.meshState(...
	 'numpointsT',meshT ...
	,'numpointsZ',meshZ);

%% controller Surface
simulations{2}.controller.plot();

%% State profile state feedback control
simulations{2} = simulations{2}.meshState('numpointsT',meshT ...
	,'numpointsZ',meshZ,'tLim',[0 0.6]);

%% Target and transformed
% remember that only the data is used. Is not put in one plot in the thesis.
simulations{2} = simulations{2}.meshTargetAndTransformed('numpointsT',meshT ...
	,'numpointsZ',meshZ,'tLim',[0 0.6]);


%% Norms for state feedback control
simulations{2}.plotNorm();

% manually plot the norm of simulation 1 into the same figure.
% it is stored in the simulation.
lastPlot = plot(simulations{1}.t,simulations{1}.xNorm);

%% observer surface
simulations{3}.observer.plot();

%% observer errror in and not in feedback
simulations{2} = simulations{2}.meshObserverError('numpointsT',meshT ...
	,'numpointsZ',meshZ,'tLim',[0 0.6]); % openloop observer
simulations{3} = simulations{3}.meshObserverError('numpointsT',meshT ...
	,'numpointsZ',meshZ,'tLim',[0 0.6]); % closedLoop observer only plot till 0.5

%% target and transformed closed-loop system
for k=[3]
	simulations{k} = simulations{k}.meshTargetAndTransformed('numpointsT',meshT ...
		,'numpointsZ',meshZ,'tLim',[0 0.6]);
end

%% Norm output feedback control
simulations{3}.plotNorm;

%% Norm of observer error
simulations{2}.plotObserverNorm(3);

%% States under output feedback
simulations{3}.meshState('numpointsT',meshT ...
	,'numpointsZ',meshZ,'tLim',[0 0.6]);

%% Plot observer norm to be included in norm plot manually
simulations{3}.plotObserverNorm;

%% normalized norms 2 4 5 
simulations{2}.plotNorm('rel');
simulations{4}.plotNorm('rel');
simulations{5}.plotNorm('rel');


%% Export plots to latex
if export
	if ~exist([plotPath '+data/'],'dir')
		mkdir([plotPath '+data/']);
	end
	misc.export2tikz(plotPath,...
				'parseStrings',false,...
				'externalData',true,...
				'relativeDataPath','plots/academicExample/+data/',...
				'dataPath','./+data/'...			'standalone',true...
				,'addLabels',true...
				,'showWarnings',false...
				,'floatFormat','%.3g'...
				);%,...
	% NOTE: floatFormat %.3g destroys Patch plots! Use separate export for patch plots.
	close all
end

%% Just a note:
fprintf(['Finished simulationAcademicExample [' datestr(now,formdate) ']!\n']);


%% Export parameters to tex:
% this is just to copy inside latex and to make export of the parameters easier.
if export
texStringargs = {};
for k=3
	texStringargs = {texStringargs{:},...
			'\Lambda(z)', lambda{k},...
			'A(z,t)', a{k},...
			'M', mu{k},...
			'B_{0}^n',simulations{k}.ppide.b_op1.b_n,...
			'B_{0}^d(t)',simulations{k}.ppide.b_op1.b_d,...
			'B_{1}^n',simulations{k}.ppide.b_op2.b_n,...
			'B_{1}^d(t)',simulations{k}.ppide.b_op2.b_d,...
			'F(z,zeta,t)',F{k},...
			'A_0(z,t)',a0{k}...
			,'n',simulations{k}.ppide.n...
			,'IterationsController',simulations{2}.controllerKernel.iteration-1 ...
			,'IterationsObserver',simulations{3}.observerKernel.iteration-1 ...
			,'tol',simSet{1}.tol ...
			,'diffXi',simSet{1}.min_xi_diff ...
			,'ndiscObs',simSet{1}.ndiscObs ...
			'ndiscTime',simSet{1}.ndiscTime 
		};
end
opts.filename = [plotPath 'pars.txt'];
opts.digits = 3;
opts.rational = 0;
string = misc.variables2tex(opts,texStringargs{:});
end

