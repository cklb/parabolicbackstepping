% this file creates the results for 
% Example 2.7.2: comparison of kernel computation times.


% need to be in folder infinitefirebutterfly and conl must be in the superdirectory.
addpath(genpath([pwd '\' 'Lib']));
addpath(genpath([pwd '\' 'conI']));
set(groot,'defaulttextinterpreter','none'); % do not interpret tex-string for export 


%% Paths and names of simulations

myPath = 'Lib/+dps/+parabolic/+mat/+diss/+exampleNumerik/'; % path to save and load mat-files
if ~exist(myPath,'dir')
	mkdir(myPath);
end
plotPath = 'Lib/+dps/+parabolic/+plot/+diss/+exampleNumerik/'; % path to save and load mat-files
if ~exist(plotPath,'dir')
	mkdir(plotPath);
end

% number of simulations to be performed 
numSims = 20;
export= 1;

	
% Most important resolutions to determine file name
simSet.ndiscKernel = 31;
simSet.ndiscObsKernel = 5;
simSet.name = 'Plant';


%% Configure simulation settings
simSet = dps.parabolic.script.diss.simulationSettings.simulationSettingsNumerik(simSet);

simpars = {...
		'simPars',simSet.sim_pars,...
		'plotPars',simSet.plot_pars,...
		'useReferenceObserver',0,...
		'useDisturbanceObserver',0,...
		'useStateObserver',0,...
		'useStateFeedback',1,...
		'useOutputRegulation',0,...
		'useIntMod',0,...
		'name',simSet.name,...
		'path',myPath
};


%% configure plant and simulate
% starting order of the kernel calculation
n=1;
lastTime = 0;
while lastTime < 15 % 3600 % Set the time in seconds to abort the calculations. If the last kernel took less than
	% this time, one more kernel will be calculated.
	fprintf('n = %u: \n',n);
	ppide = dps.parabolic.script.diss.examples.exampleNumerik(simSet,n);
	simulation = dps.parabolic.control.backstepping.ppide_simulation(ppide,simpars);
	gestime = tic;
	simulation = simulation.initializeSimulation();
	time(n)=toc(gestime);
	lastTime = time(n);
	fprintf('n = %u took %0.1f s.\n',n,time(n));
	avgTime(n) = simulation.controllerKernel.avgTime;
	fprintf('average iteration time: %0.1f s.\n',avgTime(n));
	n=n+1;
end
fprintf(['Finished all plants! Last one was \n' ...
	      '  n = %u, which took %0.1f s.\n'],n-1,time(n-1))

%% % plot
figure('Name','totalTime')
bar(1:n-1,round(time*100)/100)

figure('Name','iterationTime')
bar(1:n-1,round(avgTime*100)/100)

%% calculate average increase:
dtTot = diff(time);
incTot = dtTot./time(1:end-1);
meanInc = mean(incTot(8:end));

dtIt = diff(avgTime);
incIt = dtIt./avgTime(1:end-1);
meanIt = mean(incIt(8:end));

%% Export
% ACHTUNG: floatFormat %.3g zerst�rt Patch plots! Am besten getrennte Exportfunktionen f�r
% Patch-Plots und den Rest verwenden! Niedrig aufgeloeste Plots nicht mehr noetig.
if export
	if ~exist([plotPath '+data/'],'dir')
		mkdir([plotPath '+data/']);
	end
	misc.export2tikz(plotPath,...
		'parseStrings',false,...
		'externalData',true,...
		'relativeDataPath','plots/exampleNumeric/+data/',...
		'dataPath','./+data/'...			'standalone',true...
		,'addLabels',true...
		,'showWarnings',false...
		,'floatFormat','%.3g'...
		);%,...
	close all
end

	  
%% Export parameters to tex:
texStringargs = {};
% for k=1:numSims
	texStringargs = {...
			'n:',1:n-1,...
			'total time',round(time),...
			'average iteration time',round(avgTime*100)/100 ...
		};
% end
opts.filename = [plotPath 'pars.txt'];
opts.digits = 3;
opts.rational = 0;
string = misc.variables2tex(opts,texStringargs{:});