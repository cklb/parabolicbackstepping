%% Simulation 1: kernel visualization
% this script creates the simulation results for 
% Example 2.5.12 and Figure 2.11

% System with n=2, constant in time, surf of the kernel elements, lines of
% separation, degree of freedem eta,eta

%% 1. Add folders to path
% The main part of the used files is located in the folder Lib, which has a structure based on
% namespaces.
addpath(genpath([pwd '\' 'Lib'])); 

% The common Library "conI" of the inifinite-dimensional systems group of the Chair of
% Automatic Control, FAU Erlangen contains various miscellanious functions which are used.
% Therefore, the state of the library at commit ... is located in the folder conI
addpath(genpath([pwd '\' 'conI']));

%% Store some auxiliary variables
% Start message:
formdate='dd.mm. HH:MM:SS';
fprintf(['Starting simulationKernelVisualisation [' datestr(now,formdate) ']...\n']);

% this path can be used to save and store mat files
myPath = 'Lib/+dps/+parabolic/+mat/+diss/+example3/'; % path to save and load mat-files
if ~exist(myPath,'dir')
	mkdir(myPath);
end
plotPath = 'Lib/+dps/+parabolic/+plot/+diss/+example3/'; % path to save and load mat-files
if ~exist(plotPath,'dir')
	mkdir(plotPath);
end

% number of simulations to be performed 
plots = [1 2]; % g(eta) = 0 and g(eta) = -2eta

% set whether the resulting plots shall be exported to tex
export= 0;

for k=[1 plots] % 1 always!
	simSet{k}.name = 'sim';
end
	
%% the resolutions of the kernels in original coordinates are stored first.
% the structure simSet is just a container for the simulation settings that are used at
% different points.
simSet{1}.ndiscKernel = 31;
simSet{1}.ndiscObsKernel = 31;
simSet{2}.ndiscKernel = 31;
simSet{2}.ndiscObsKernel = 31;


%% Configure simulation settings
% now further simulation settings are added to simSet. 
% for better readability, this is externalized into a function.
for k=[1 plots]
	simSet{k} = dps.parabolic.script.diss.simulationSettings.simulationSettings3(simSet{k});
end

%% configure plant
% the creation of the plant(s) is externalized to a function as well.
% the many arguments returned are all contained in the ppide itself and are only passed to
% easily export them to latex.
ppide{1} = dps.parabolic.script.diss.examples.example3(simSet{1});
for k = plots
	[ppide{k}, lambda{k},a{k},mu{k},a0{k},F{k},g_strich{k}] = dps.parabolic.script.diss.examples.example3(simSet{k});
	% very important is the object ppide, which is a 
	% dps.parabolic.syste.ppide_sys and contains all the information that defines the system to
	% be controlled. For more details look into the above function!
end
% Degree of freedom.
g_strich{2} = [];
for i=1:ppide{1}.n
	for j=1:ppide{1}.n
		g_strich{2}{i,j} = @(eta) 0-2*eta; % Freiheitsgrad, anonyme Matrixfunktion mit ausschlie�lich Elementen unter der Hauptdiagonalen
	end
end
ppide{2}.ctrl_pars.g_strich = g_strich{2};



%% 1. Simulate 
% now the actual calculations and simulations are performed. 
% To this end, object of the class
% dps.parabolic.control.backstepping.ppide_simulation
% are created. These objects contain all methods needed to calculate controllers and observers
% and perform simulations.

% The constructor of this class accepts a dps.parabolic.system.ppide_sys as first input and
% name-value-pairs after that. To keep the overview, these name-value-pairs, which acutally
% configure the performed simulations, are stored into variables simpars first.

% All parameters that are set to zero could also be omitted but are listed to show some more
% possible options.
% Funcionalities concerning output regulation or robust output regulation only work for
% time-invariant systems.

simpars{1} = {...
		'x0',ppide{1}.x0,...
		'simPars',simSet{1}.sim_pars,...
		'plotPars',simSet{1}.plot_pars,...
		'useReferenceObserver',0,...
		'useDisturbanceObserver',0,...
		'useStateObserver',0,...
		'useStateFeedback',1,...
		'useOutputRegulation',0,...
		'useIntMod',0,...
		'name',simSet{1}.name,...
		'path',myPath
};


simpars{2} = {...
		'x0',ppide{2}.x0,...
		'simPars',simSet{2}.sim_pars,...
		'plotPars',simSet{2}.plot_pars,...
		'useReferenceObserver',0,...
		'useDisturbanceObserver',0,...
		'useStateObserver',0,...
		'useStateFeedback',1,...
		'useOutputRegulation',0,...
		'useIntMod',0,...
		'name',simSet{2}.name,...
		'path',myPath
};

for k=plots
	disp(['k=' num2str(k) ':...'])
	if ~exist('simulations','var')
		simulations{k} = dps.parabolic.control.backstepping.ppide_simulation(ppide{k},simpars{k});
	else
		if length(simulations)>k-1 && ~isempty(simulations{k}) && isa(simulations{k},'dps.parabolic.control.backstepping.ppide_simulation')
			simulations{k} = simulations{k}.setPars(ppide{k},simpars{k});
		else
			i=1;
			while i<k
				if ~isempty(simulations{k-i}) && isa(simulations{k-i},'dps.parabolic.control.backstepping.ppide_simulation')
					simulations{k} = simulations{k-i};
					simulations{k} = simulations{k}.setPars(ppide{k},simpars{k});
					break
				end
				i=i+1;
			end
			if i==k % no other simulations is available
				simulations{k} = dps.parabolic.control.backstepping.ppide_simulation(ppide{k},simpars{k});
			end
		end
	end
	if simulations{k}.controllerKernelFlag
		simulations{k} = simulations{k}.initializeSimulation();
	else
		simulations{k} = simulations{k}.initializeSimulation();
% 		save(simulations{k}.saveStr,'simulations')
	end
	if ~simulations{k}.simulated
		simulations{k} = simulations{k}.simulate;
	end
end


%% Plot distributed controller
figure('Name','distributedController')
hold on
dashpattern{1} = '-';
dashpattern{2} = '--';
if length(k)>1
	coldiff = 255/(number_sims-1);
else
	coldiff=255;
end

for k=plots	
	ndisc = length(simulations{k}.ppide.ctrl_pars.zdiscCtrl);
	for i=2:simulations{k}.ppide.n
		for j=1:simulations{k}.ppide.n
			blue=(j-1)*coldiff;
			green = (j-1)*coldiff;
			red = 255-(j-1)*coldiff;
			h{k,j} = plot(simulations{k}.ppide.ctrl_pars.zdiscCtrl,simulations{k}.controller.rz(i,(j-1)*ndisc+1:j*ndisc),...
				'Color',[red/255 green/255 blue/255],'lineStyle',dashpattern{k});
			legendtext{k,j} = ['$R_{2' num2str(j) '}(z)$'];
		end
	end
end
legend([h{1,1} h{1,2}],{[legendtext{1,1};legendtext{1,2}]},'interpreter','none')
xlabel('$z$')
box on

%% plot derivative
for k=1
	simulations{k}.controllerKernel.plot('dz_K_1_zeta')
end

%% Export
if export
if ~exist([plotPath '+data/'],'dir')
	mkdir([plotPath '+data/']);
end
misc.export2tikz(plotPath,'width','0.35\linewidth','height','0.35\linewidth',...
	        'parseStrings',false,...
			'extraAxisOptions',...
			['every axis title/.append style={font=\footnotesize},',...
			'every axis label/.append style={font=\footnotesize},'...
			'every tick label/.append style={font=\scriptsize},'...
          'every x tick label/.style={},'...
			'every y tick label/.style={},'...
          'every z tick label/.style={},'...
			'yticklabel style={/pgf/number format/fixed,/pgf/number format/precision=2},'...			'before end axis/.code={\legend{}},',...
			],...
			'floatFormat','%.3g',...
			'externalData',true,...
			'relativeDataPath','plots/exampleKernel/+data/',...
			'dataPath','./+data/'...			'standalone',true...
			,'addLabels',true...
			);%,...
	close all
end
% ACHTUNG: floatFormat %.3g zerst�rt Patch plots! Am besten getrennte Exportfunktionen f�r
% Patch-Plots und den Rest verwenden!


%% Export parameters to tex:
if export
texStringargs = {};
for k=plots
	texStringargs = {texStringargs{:},...
			'\Lambda(z)', lambda{k},...
			'A(z)', a{k},...
			'M', mu{k},...
			'g_{21}(\peta)', g_strich{k}{2,1},...
			'B_{0}^n',simulations{k}.ppide.b_op1.b_n,...
			'B_{0}^d',simulations{k}.ppide.b_op1.b_d,...
			'B_{1}^n',simulations{k}.ppide.b_op2.b_n,...
			'B_{1}^d',simulations{k}.ppide.b_op2.b_d,...
			'\phi_1(1)',simulations{k}.controllerKernel.value{1,1}.phi_i(end),...
			'\phi_2(1)',simulations{k}.controllerKernel.value{2,2}.phi_i(end)...
		};
end
opts.filename = [plotPath 'pars.txt'];
opts.digits = 3;
opts.rational = 0;
string = misc.variables2tex(opts,texStringargs{:});
end

%% Plot Kernel final with high resolution
for k=plots
	simulations{k}.controllerKernel.plot('K','contour',1,'plotPars',{'edgeColor','none'},'samples',51);
	simulations{k}.controllerKernel.plot('G','contour',1,'plotPars',{'edgeColor','none'},'samples',101);
end

%% Export high-res kernel plots
if export
relpath = 'plots/exampleKernel/~/';
misc.export2tikz(plotPath,'width','0.35\linewidth','height','0.35\linewidth',...
	        'parseStrings',false,...
			'extraAxisOptions',...
			[...'every axis title/.append style={font=\footnotesize},',...'every axis label/.append style={font=\footnotesize},'...'every tick label/.append style={font=\scriptsize},'...'every x tick label/.style={},'...'every y tick label/.style={},'...'every z tick label/.style={},'...'yticklabel style={/pgf/number format/fixed,/pgf/number format/precision=2},'...
			'before end axis/.code={\legend{}},',...
			'every axis plot/.append style={'...
				'contour prepared/.append style={line width={0.5pt},contour/draw color=black},},'...
			'every axis plot post/.append style={table/row sep=newline}'
			],...			'floatFormat','%.3g',...			
			'extraTikzpictureOptions',[...
				'execute at begin picture={'...
					'\colorlet{mycolor1}{\mycolA}'...
					'\colorlet{mycolor2}{\mycolB}'...
					'\colorlet{mycolor3}{\mycolC}'...
					'\colorlet{mycolor4}{\mycolD}}'...
			],...
			'externalData',true,...			
			'relativeDataPath',relpath,...			
			'dataPath','./+data/'...			'standalone',true
			);%,...
	close all
end
% ACHTUNG: floatFormat %.3g zerst�rt Patch plots! Am besten getrennte Exportfunktionen f�r
% Patch-Plots und den Rest verwenden!

%% Export with low resolution
for k=plots
	simulations{k}.controllerKernel.plot('K','contour',1,'plotPars',{'edgeColor','none'},'samples',11,'numPlots',3);
	simulations{k}.controllerKernel.plot('G','contour',1,'plotPars',{'edgeColor','none'},'samples',11);
end

%% Export
if export
if ~exist([plotPath '+dataLow/'],'dir')
	mkdir([plotPath '+dataLow/']);
end
relpath = 'plots/exampleKernel/~/';
misc.export2tikz(plotPath,'width','0.35\linewidth','height','0.35\linewidth',...
	        'parseStrings',false,...
			'extraAxisOptions',...
			[...'every axis title/.append style={font=\footnotesize},',...'every axis label/.append style={font=\footnotesize},'...'every tick label/.append style={font=\scriptsize},'...'every x tick label/.style={},'...'every y tick label/.style={},'...'every z tick label/.style={},'...'yticklabel style={/pgf/number format/fixed,/pgf/number format/precision=2},'...
			'before end axis/.code={\legend{}},',...
			'every axis plot/.append style={'...
				'contour prepared/.append style={line width={0.5pt},contour/draw color=black},},'...
			'every axis plot post/.append style={table/row sep=newline}'
			],...			'floatFormat','%.3g',...			
			'extraTikzpictureOptions',[...
				'execute at begin picture={'...
					'\colorlet{mycolor1}{\mycolA}'...
					'\colorlet{mycolor2}{\mycolB}'...
					'\colorlet{mycolor3}{\mycolC}'...
					'\colorlet{mycolor4}{\mycolD}}'...
			],...
			'externalData',true,...			
			'relativeDataPath',relpath,...			
			'dataPath','./+dataLow/'...			'standalone',true
			);%,...
	close all
end
% ACHTUNG: floatFormat %.3g zerst�rt Patch plots! Am besten getrennte Exportfunktionen f�r
% Patch-Plots und den Rest verwenden!
