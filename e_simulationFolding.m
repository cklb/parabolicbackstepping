%% this file creates the results of 
% Examples 4.1.9 and 4.9.2

% conI and Lib must be inside the current folder.
addpath(genpath([pwd '\' 'Lib']));
addpath(genpath([pwd '\' 'conI']));
set(groot,'defaulttextinterpreter','none'); % do not interpret tex-string for export 

%% Paths and names of simulations
formdate='dd.mm. HH:MM:SS';
fprintf(['Starting simulationFolding [' datestr(now,formdate) ']...\n']);

myPath = 'Lib/+dps/+parabolic/+mat/+diss/+exampleFolding/'; % path to save and load mat-files
if ~exist(myPath,'dir')
	mkdir(myPath);
end
plotPath = 'Lib/+dps/+parabolic/+plot/+diss/+exampleFolding/'; % path to save and load mat-files
if ~exist(plotPath,'dir')
	mkdir(plotPath);
end

% number of simulations to be performed 
numPlots = 10;
export = 1;


plots = [1 3 4 8 9 10];


for k=[plots] % 1 always!
	simSet{k}.name = 'simBilateral';
end
simSet{1}.name = 'Unilateral';
simSet{3}.name = 'Folding 0.2';
simSet{4}.name = 'Folding 0.375';
simSet{8}.name = 'Folding 0.85';
simSet{9}.name = 'Folding with Observer';
simSet{10}.name = 'New observer measurement';
 	
% Most important resolutions to determine file name
simSet{1}.ndiscKernel = 31;
simSet{1}.ndiscObsKernel = 31;

for k=plots
	simSet{k}.ndiscKernel = simSet{1}.ndiscKernel;
	simSet{k}.ndiscObsKernel = simSet{1}.ndiscObsKernel;
end


%% Configure simulation settings
for k=[1 plots]
	simSet{k} = dps.parabolic.script.diss.simulationSettings.simulationSettingsFolding(simSet{k});
end

%% configure plant
for k = 1 % ppide needs to be created only for the first, the rest is overtaken which is faster
	[ppide{k}, lambda{k}, a{k}] = dps.parabolic.script.diss.examples.exampleFolding(simSet{k});
end

%% configure different systems.
% first system is created unilateral.
% create bilateral plants

b_1 = [eye(ppide{1}.n,ppide{1}.n),zeros(ppide{1}.n,ppide{1}.n)];
b_2 = [zeros(ppide{1}.n,ppide{1}.n), eye(ppide{1}.n,ppide{1}.n)];
b = @(z) 0*z + zeros(ppide{1}.n,2*ppide{1}.n);
mus = 8;
mus(2:numPlots(end)) = [13]*ones(1,numPlots(end)-1);
% foldingPoints = [];			% 2   3    4   5    6   7    8      9: observer 10
foldingPoints(2:numPlots(end)) = [0.1 0.2 0.375 0.4 0.65 0.75 0.85 0.375 0.375];
for k=2:plots(end)
	ppide{k} = ppide{1};
	ppide{k}.b_1 = b_1;
	ppide{k}.b_2 = b_2;
	ppide{k}.b = b;
	ppide{k}.ctrl_pars.mu = mus(k)*eye(ppide{k}.n);
	ppide{k}.ctrl_pars.foldingPoint = foldingPoints(k);
end
% different measurement for last simulation:
n = ppide{10}.n;
ppide{10}.cm_op = dps.output_operator('z_m',0.3,'c_m',[zeros(n,n);eye(n)],'c_dm',[eye(n);zeros(n,n)]);


%%
%% 1. Simulate 
simpars{1} = {...
		'x0',ppide{1}.x0,...
		'simPars',simSet{1}.sim_pars,...
		'plotPars',simSet{1}.plot_pars,...
		'useReferenceObserver',0,...
		'useDisturbanceObserver',0,...
		'useStateObserver',0,...
		'useStateFeedback',1,...
		'useOutputRegulation',0,...
		'useIntMod',0,...
		'name',simSet{1}.name,...
		'path',myPath,...
		'debug',0
};


for k=plots
	simpars{k} = simpars{1};
end

% observer
% calculate suitable initial conditions.
% get measurement at folding point:
% is exactly the value of left end for both observers
for k=9:10
xm = repmat(ppide{k}.x0(ppide{k}.cm_op.z_m),2,1);
zdiscDiff = linspace(0,1,11);
xmDisc = misc.eval_pointwise(ppide{k}.x0,zdiscDiff);
xmDiff = quantity.Discrete(numeric.diff_num(zdiscDiff,xmDisc,1),'grid',{zdiscDiff},'gridName',{'z'});
xms = xmDiff.at(ppide{k}.cm_op.z_m);
% calculate derivative of different observers:
xmsTot = [-xms*ppide{k}.cm_op.z_m; xms*(1-ppide{k}.cm_op.z_m)];

xHat0 = @(z) 1*((2*xm+xmsTot).*z(:).'.^3 + (-3*xm-2*xmsTot).*z(:).'.^2 ...
			+(xmsTot).*z(:).' + xm);

simpars{k} = {...
		'x0',ppide{1}.x0,...
		'xHat0',xHat0,...
		'simPars',simSet{1}.sim_pars,...
		'plotPars',simSet{1}.plot_pars,...
		'useReferenceObserver',0,...
		'useDisturbanceObserver',0,...
		'useStateObserver',1,...
		'useObserverInFeedback',1,...
		'useStateFeedback',1,...
		'useOutputRegulation',0,...
		'useIntMod',0,...
		'name',simSet{1}.name,...
		'path',myPath,...
		'debug',0
};
end

for k=plots
	disp(['k=' num2str(k) ':...'])
	if ~exist('simulations','var')
		simulations{k} = dps.parabolic.control.backstepping.ppide_simulation(ppide{k},simpars{k});
	else
		if length(simulations)>k-1 && ~isempty(simulations{k}) && isa(simulations{k},'dps.parabolic.control.backstepping.ppide_simulation')
			simulations{k} = simulations{k}.setPars(ppide{k},simpars{k});
		else
			i=1;
			while i<k
				if length(simulations)>= k-i && ~isempty(simulations{k-i}) && isa(simulations{k-i},'dps.parabolic.control.backstepping.ppide_simulation')
					simulations{k} = simulations{k-i};
					simulations{k} = simulations{k}.setPars(ppide{k},simpars{k});
					break
				end
				i=i+1;
			end
			if i==k % no other simulations is available
				simulations{k} = dps.parabolic.control.backstepping.ppide_simulation(ppide{k},simpars{k});
			end
		end
	end
% 	simulations{1}.designPpide.plotLambdaSigns;
	if simulations{k}.controllerKernelFlag
		simulations{k} = simulations{k}.initializeSimulation();
	else
		simulations{k} = simulations{k}.initializeSimulation();
% 		save(simulations{k}.saveStr,'simulations')
	end
	if ~simulations{k}.simulated
		simulations{k} = simulations{k}.simulate;
	end
end



% dont create everything for last case
newPlots = [1 3 4 8 9];


%% mesh state
meshT = 21;
meshZ = 21;
extraT = [(1/(meshT-1))/3 2*(1/(meshT-1))/3];
for k=newPlots
	simulations{k} = simulations{k}.meshState(...
		'numpointsT',meshT ...
		,'numpointsZ',meshZ ...
		,'maxDiffT',0.2 ...
	);
end

%% u
for k=newPlots
	simulations{k} = simulations{k}.plotU;
end

%% Norms 
figure('name','Norms')
hold on
xlabel('t')
for k=newPlots
	plot(simulations{k}.t,simulations{k}.xNorm,'displayName',['k=' num2str(k)]);
end
plot(simulations{1}.t,3*exp(-8*simulations{1}.t));
legend('-DynamicLegend')

%%
simulations{1}.plotNorm(1) % for decay rate, not used!
%% check output feedback
simulations{9}.plotNorm();

%% observer Norm
simulations{9}.plotObserverNorm(1);

%% mesh observer error optimized for xHat 
simulations{9}.meshObserverError('numpointsT',21,'numpointsZ',21,...
	'tLim',[0,1],'maxDiffT',0.1); 

%% mesh observer error
simulations{10}.meshObserverError('numpointsT',21,'numpointsZ',21,...
	'tLim',[0,1],'maxDiffT',0.1); 

%% and again for error
simulations{9}.meshObserverError('numpointsT',21,'numpointsZ',21,...
	'tLim',[0,0.25],'maxDiffT',0.05); 

%% mesh observer error
simulations{10}.meshObserverError('numpointsT',21,'numpointsZ',21,...
	'tLim',[0,0.25],'maxDiffT',0.05);


%% Export
% ACHTUNG: floatFormat %.3g zerst�rt Patch plots! Am besten getrennte Exportfunktionen f�r
% Patch-Plots und den Rest verwenden! Niedrig aufgeloeste Plots nicht mehr noetig.
if export
	if ~exist([plotPath '+data/'],'dir')
		mkdir([plotPath '+data/']);
	end
	misc.export2tikz(plotPath,...
		'parseStrings',false,...
		'externalData',true,...
		'relativeDataPath','plots/exampleFolding/+data/',...
		'dataPath','./+data/'...			'standalone',true...
		,'addLabels',true...
		,'showWarnings',false...
		,'floatFormat','%.3g'...
		);%,...
	close all
end

%% print msg 
fprintf(['Finished simulationFolding [' datestr(now,formdate) ']!\n']);

%% Export parameters to tex:
if export
texStringargs = {... 
			'\Lambda(z)', lambda{1},...
			'A(z,t)', a{1},...
			'B_{0}^n',simulations{k}.ppide.b_op1.b_n,...
			'B_{0}^d(t)',simulations{k}.ppide.b_op1.b_d,...
			'B_{1}^n',simulations{k}.ppide.b_op2.b_n,...
			'B_{1}^d(t)',simulations{k}.ppide.b_op2.b_d...
			,'tol',simSet{1}.tol ...
			,'numXi',simSet{1}.maxNumXi ...
			,'ndiscObs',simSet{1}.ndiscObs ...
			,'ndiscTime',simSet{1}.ndiscTime ...
			,'ndiscKernel',simSet{1}.ndiscKernel ...
			,'-------------', '-----------' ...
		};
for k=plots
	if simulations{k}.useStateObserver
		obsIts = [num2str(simulations{k}.observerKernel1.iteration-1) '/' num2str(simulations{k}.observerKernel2.iteration-1)];
	else
		obsIts = '--';
	end
	texStringargs = {texStringargs{:},...
			'Name',simSet{k}.name,...
			'mu',mus(k)...
			,'IterationsController',simulations{k}.controllerKernel.iteration-1 ...
			,'IterationsObserver', obsIts...
			,'foldingPointController',simulations{k}.ppide.ctrl_pars.foldingPoint ...
			,'foldingPointObserver',simulations{k}.ppide.cm_op.z_m ...
		};
end
opts.filename = [plotPath 'pars.txt'];
opts.digits = 3;
opts.rational = 0;
string = misc.variables2tex(opts,texStringargs{:});
end


