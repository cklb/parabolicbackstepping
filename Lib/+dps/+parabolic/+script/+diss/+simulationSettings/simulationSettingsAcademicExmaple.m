function simulationSettings = simulationSettingsAcademicExmaple(simulationSettings)

%% Simulation settings
simulationSettings.sim_pars = misc.simulation_parameters('frel',1e-6,...
								 'fabs',1e-6,... 
						 		 'tsmax',2e-2,...
								 'tend',1); % 8
% Ploteinstellungen
simulationSettings.plot_pars.plot_res_1D = 101; % resolution of 1d lines
simulationSettings.plot_pars.plot_res_2D_lines = 150; % resolution of lines in mesh_sparse
simulationSettings.plot_pars.plot_res_2D_num_lines = 20; % number of lines in mesh_sparse
simulationSettings.plot_pars.plot_res_2D = 41; % discretisation for 2d plots in both directions
simulationSettings.plot_pars.meshZ = 21; % discretisation for 2d plots in z direction
simulationSettings.plot_pars.meshT = 31; % discretisation for 2d plots in t direction

							
% Sukzessive Approximation:
simulationSettings.itmax = 60; 
simulationSettings.itmin = 1;
simulationSettings.tol = 1e-5; % Abbruchkriterium sukzessive Approximation
simulationSettings.min_xi_diff = 0.02; % numerischer Parameter, f�r numerische Effizienz
simulationSettings.min_eta_diff = 0.02;
simulationSettings.min_xi_diff_border = 0.00;
simulationSettings.min_eta_diff_border = 0.00;

simulationSettings.ndiscPars = 151; % Aufloesung gespeicherte Parameter
simulationSettings.ndiscRegEq = 5; % Aufloesung Regulator equations
simulationSettings.ndiscPlant = 101;% Aufl�sung Streckenapproximation
simulationSettings.zdiscPlant = linspace(0,1,simulationSettings.ndiscPlant);% Aufl�sung Streckenapproximation
simulationSettings.ndiscObs = 11; % Aufloesung Beobachterrealisierung
simulationSettings.ndiscTarget = simulationSettings.ndiscPlant; % Aufloesung Zielsystem (f�r Analysezwecke)
simulationSettings.zdiscTarget = linspace(0,1,simulationSettings.ndiscTarget);% Aufl�sung Streckenapproximation
simulationSettings.ndiscTime = 31; % Aufl�sung Diskretisierung Zeitachse Kern
simulationSettings.ndiscTimePlant = 151; % Aufl�sung Zeitachse Simulatoin

end