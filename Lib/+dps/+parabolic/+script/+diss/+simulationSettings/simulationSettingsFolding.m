function simulationSettings = simulationSettingsFolding(simulationSettings)

%% Simulation settings
simulationSettings.sim_pars = misc.simulation_parameters('frel',1e-6,...
								 'fabs',1e-6,... 
						 		 'tsmax',2e-2,...
								 'tend',1); % 8
% Ploteinstellungen
simulationSettings.plot_pars.plot_res_1D = 50;
simulationSettings.plot_pars.plot_res_2D_lines = 150;
simulationSettings.plot_pars.plot_res_2D_num_lines = 20;
							
% Sukzessive Approximation:
simulationSettings.itmax = 40; 
simulationSettings.itmin = 1;
simulationSettings.tol = 1e-5; % Abbruchkriterium sukzessive Approximation
% ACHTUNG: Bei zeitkonstanten Lambda wird das xi-eta-grid immer so bestimmt, dass das Inkrement gleich ist.
% (Hat sehr viele Vorteile!). Deshalb hat min_eta_diff keinen Einfluss und zur Erh�hung der
% Genauigkeit am Rand muss min_xi_diff verbessert werden!
simulationSettings.min_xi_diff = 0.025; %0.006; % numerischer Parameter, f�r numerische Effizienz
simulationSettings.min_eta_diff = 0.025;
simulationSettings.min_xi_diff_border = 0.00;
simulationSettings.min_eta_diff_border = 0.00;
simulationSettings.maxNumXi = 80;

simulationSettings.ndiscPars = 251; % Aufloesung gespeicherte Parameter
simulationSettings.ndiscRegEq = 5; % Aufloesung Regulator equations
simulationSettings.ndiscPlant = 101;% Aufl�sung Streckenapproximation
simulationSettings.zdiscPlant = linspace(0,1,simulationSettings.ndiscPlant);% Aufl�sung Streckenapproximation
simulationSettings.ndiscObs = 11; % Aufloesung Beobachterrealisierung
simulationSettings.ndiscTarget = simulationSettings.ndiscPlant; % Aufloesung Zielsystem (f�r Analysezwecke)
simulationSettings.zdiscTarget = linspace(0,1,simulationSettings.ndiscTarget);% Aufl�sung Streckenapproximation
simulationSettings.ndiscTime = 31; % Aufl�sung Diskretisierung Zeitachse Kern
simulationSettings.ndiscTimePlant = 151; % Aufl�sung Zeitachse Simulatoin

end