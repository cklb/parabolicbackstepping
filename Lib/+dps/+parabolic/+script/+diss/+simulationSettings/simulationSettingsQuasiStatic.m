function simulationSettings = simulationSettingsQuasiStatic(simulationSettings)

%% Simulation settings
simulationSettings.sim_pars = misc.simulation_parameters('frel',1e-6,...
								 'fabs',1e-6,... 
						 		 'tsmax',2e-2,...
								 'tend',1); % 8
% Ploteinstellungen
simulationSettings.plot_pars.plot_res_1D = 50;
simulationSettings.plot_pars.plot_res_2D_lines = 150;
simulationSettings.plot_pars.plot_res_2D_num_lines = 20;
							
% Sukzessive Approximation:
simulationSettings.itmax = 40; 
simulationSettings.itmin = 1;
simulationSettings.tol = 1e-3; % Abbruchkriterium sukzessive Approximation
simulationSettings.min_xi_diff = 0.05; % numerischer Parameter, f�r numerische Effizienz
simulationSettings.min_eta_diff = 0.05;
simulationSettings.min_xi_diff_border = 0.00;
simulationSettings.min_eta_diff_border = 0.00;

simulationSettings.ndiscPars = 201; % Aufloesung gespeicherte Parameter
simulationSettings.ndiscRegEq = 5; % Aufloesung Regulator equations
simulationSettings.ndiscPlant = 151;% Aufl�sung Streckenapproximation
simulationSettings.zdiscPlant = linspace(0,1,simulationSettings.ndiscPlant);% Aufl�sung Streckenapproximation
simulationSettings.ndiscObs = 21; % Aufloesung Beobachterrealisierung
simulationSettings.ndiscTarget = simulationSettings.ndiscPlant; % Aufloesung Zielsystem (f�r Analysezwecke)
simulationSettings.zdiscTarget = linspace(0,1,simulationSettings.ndiscTarget);% Aufl�sung Streckenapproximation
simulationSettings.ndiscTime = 21; % Aufl�sung Diskretisierung Zeitachse Kern
simulationSettings.ndiscTimePlant = 151; % Aufl�sung Zeitachse Simulatoin

simulationSettings.x0 = @(z) dps.parabolic.tool.get_x0_ppides(z,1);

end