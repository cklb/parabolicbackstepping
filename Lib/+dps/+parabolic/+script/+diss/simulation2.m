%% Simulation 2:
% System with n=2, constant in time, just experimenting... with observer and state feedback
% Example mainly shows that under some choice of parameters, the state feedback controller
% works bad. Flipping the diffusion coefficients or the type of BC make observer behave bad,
% while state feedback works... This could still be a bug...

% need to be in folder infinitefirebutterfly and conl must be in the superdirectory.
addpath(genpath([pwd '\' 'external']));
addpath(genpath([pwd '\..\' 'miscLib']));
set(groot,'defaulttextinterpreter','none'); % do not interpret tex-string for export 
rmpath(genpath([pwd '\external\matlab2tikz1.0']));
rmpath(genpath([pwd '\external\matlab2tikz_0.4.6']));


%% Paths and names of simulations
myPath = '+dps/+parabolic/+mat/+diss/+example2/'; % path to save and load mat-files

% number of simulations to be performed 
plots = [2];

for k=[1 plots] % 1 always!
	simSet{k}.name = 'sim2';
end
	
% Most important resolutions to determine file name
simSet{1}.ndiscKernel = 15;
simSet{1}.ndiscObsKernel = 15;
simSet{2}.ndiscKernel = 15;
simSet{2}.ndiscObsKernel = 15;
simSet{3}.ndiscKernel = 15;
simSet{3}.ndiscObsKernel = 15;
simSet{4}.ndiscKernel = 21;
simSet{4}.ndiscObsKernel = 21;
simSet{5}.ndiscKernel = 21;
simSet{5}.ndiscObsKernel = 21;

% Load data, if stored.
% for i=1:length(names)
% 	if ~exist(names{i},'var')
% 		srchstr = [myPath... % myPath
% 			'*'...                    % new date
% 			names{i}...                 % new name
% 			'_n'...		% num States
% 			'*'...
% 			'_nc'...
% 			num2str(ndiscKernel)... % ndiscCtrl
% 			'_noK'... % rest unchanged
% 			num2str(ndiscObsKernel)...
% 			'.mat'];
% 		fls = dir(srchstr);
% 		if ~isempty(fls)
% 			load([myPath fls(end).name]); %nichts laden
% 		end
% 	end
% end


%% Configure simulation settings
for k=[1 plots]
	simSet{k} = dps.parabolic.script.diss.simulationSettings.simulationSettings2(simSet{k});
end

%% configure plant
ppide{1} = dps.parabolic.script.diss.examples.example2(simSet{1});
for k = plots
	ppide{k} = dps.parabolic.script.diss.examples.example2(simSet{k});
end
% ppide{3} = dps.parabolic.control.backstepping.computeDualSystem(ppide{2},ppide{2}.ctrl_pars.zdiscCtrl);
% ppide{5} = dps.parabolic.control.backstepping.computeDualSystem(ppide{4},ppide{4}.ctrl_pars.zdiscCtrl);


%% Simulate
% dps.parabolic.script.simScriptTimeDependent
%% 1. Simulate Open-Loop:
% simpars{1} = {...
% 		'x0',ppide{1}.x0,...
% 		'simPars',simSet.sim_pars,...
% 		'plotPars',simSet.plot_pars,...
% 		'useReferenceObserver',0,...
% 		'useDisturbanceObserver',0,...
% 		'useStateObserver',0,...
% 		'useStateFeedback',0,...
% 		'useOutputRegulation',0,...
% 		'useIntMod',0,...
% 		'name',simSet.name,...
% 		'path',myPath
% };
% 
% if ~exist('simulations','var')
% 	simulations{1} = dps.parabolic.control.backstepping.ppide_simulation(ppide{1},simpars{1});
% else
% 	if ~isempty(simulations{1}) && isa(simulations{1},'dps.parabolic.control.backstepping.ppide_simulation')
% 		simulations{1} = simulations{1}.setPars(ppide{1},simpars{1});
% 	else
% 		simulations{1} = dps.parabolic.control.backstepping.ppide_simulation(ppide{1},simpars{1});
% 	end
% end
% % Normal but resampled mesh:
% if simulations{1}.controllerKernelFlag
% 	simulations{1} = simulations{1}.initializeSimulation();
% else
% 	simulations{1} = simulations{1}.initializeSimulation();
% 	save(simulations{1}.saveStr,'simulations')
% end
% if ~simulations{1}.simulated
% 	simulations{1} = simulations{1}.simulate;
% end
% 
% %% Plot open-loop behaviour
% res_surf_t=21;
% res_surf_z=21;
% figure('Name','OpenLoop')
% idplot=1;
% t_plot{1} = simulations{1}.t;
% y_plot{1} = simulations{1}.x;
% for k=1:1 %length(plantlist)
% 	for i=1:ppide{1}.n
% 		subplot(1,ppide{1}.n,idplot)		
% 		% Option 1: normales Mesh
% 		tres = linspace(t_plot{k}(1),t_plot{k}(end),res_surf_t);
% 		zres = linspace(0,1,res_surf_z);
% 		[Z,T]=meshgrid(simSet.zdiscPlant,t_plot{k});
% 		[ZRES,TRES] = meshgrid(zres,tres);
% 		yres = interp2(Z,T,y_plot{k}(:,(i-1)*simSet.ndiscPlant+1:i*simSet.ndiscPlant),ZRES,TRES);
% % 		[T0,Y0] = meshgrid([0 1 1 0],[0 0 1 1]);
% % 		hold on
% 		surf(TRES,ZRES,yres)
% % 		plot3([0 1 1 0 0],[0 0 1 1 0],[0 0 0 0 0],'k');
% % 		view(-40,30);
% 		
% 		% Option 2: mesh_sparse
% % 		mesh_sparse(t_plot{k},zdisc_plant,y_plot{k}(:,(i-1)*ndisc_plant+1:i*ndisc_plant).',lines_t,lines_z,res_surf_t,res_surf_z);
% 		
% 		hold on
% 		box on
% 		grid on
% 		set(gca,'Ydir','reverse')
% 		xlabel('$t$')
% 		ylabel('$z$')
% 		zlabel(['$x_' num2str(i) '(z,t)$'])
% 		idplot=idplot+1;
% 	end
% end


% simRefScript
%%
%% 1. Simulate 
simpars{1} = {...
		'x0',ppide{1}.x0,...
		'simPars',simSet{1}.sim_pars,...
		'plotPars',simSet{1}.plot_pars,...
		'useReferenceObserver',0,...
		'useDisturbanceObserver',0,...
		'useStateObserver',0,...
		'useStateFeedback',0,...
		'useOutputRegulation',0,...
		'useIntMod',0,...
		'name',simSet{1}.name,...
		'path',myPath
};


simpars{2} = {...
		'x0',ppide{2}.x0,...
		'simPars',simSet{2}.sim_pars,...
		'plotPars',simSet{2}.plot_pars,...
		'useReferenceObserver',0,...
		'useDisturbanceObserver',0,...
		'useStateObserver',0,...
		'useStateFeedback',1,...
		'useOutputRegulation',0,...
		'useIntMod',0,...
		'name',simSet{2}.name,...
		'path',myPath
};
% simpars{4} = simpars{2};
% simpars{3} = {...
% 		'x0',ppide{2}.x0,...
% 		'simPars',simSet{3}.sim_pars,...
% 		'plotPars',simSet{3}.plot_pars,...
% 		'useReferenceObserver',0,...
% 		'useDisturbanceObserver',0,...
% 		'useStateObserver',1,...
% 		'useStateFeedback',1,...
% 		'useOutputRegulation',0,...
% 		'useIntMod',0,...
% 		'name',simSet{3}.name,...
% 		'path',myPath
% };
% simpars{5} = simpars{3};
% ppide{3}.ctrl_pars.quasiStatic = 1;

for k=plots
	disp(['k=' num2str(k) ':...'])
	if ~exist('simulations','var')
		simulations{k} = dps.parabolic.control.backstepping.ppide_simulation(ppide{k},simpars{k});
	else
		if length(simulations)>k-1 && ~isempty(simulations{k}) && isa(simulations{k},'dps.parabolic.control.backstepping.ppide_simulation')
			simulations{k} = simulations{k}.setPars(ppide{k},simpars{k});
		else
			i=1;
			while i<k
				if ~isempty(simulations{k-i}) && isa(simulations{k-i},'dps.parabolic.control.backstepping.ppide_simulation')
					simulations{k} = simulations{k-i};
					simulations{k} = simulations{k}.setPars(ppide{k},simpars{k});
					break
				end
				i=i+1;
				if i==k % no other simulations is available
					simulations{k} = dps.parabolic.control.backstepping.ppide_simulation(ppide{k},simpars{k});
				end
			end
		end
	end
	if simulations{k}.controllerKernelFlag
		simulations{k} = simulations{k}.initializeSimulation();
	else
		simulations{k} = simulations{k}.initializeSimulation();
		save(simulations{k}.saveStr,'simulations')
	end
	if ~simulations{k}.simulated
		simulations{k} = simulations{k}.simulate;
	end
end


%% Plot closed-loop behaviour
% res_surf_t=21;
% res_surf_z=21;
% figure('Name','ClosedLoop')
% idplot=1;
% t_plot{1} = simulations{2}.t;
% y_plot{1} = simulations{2}.x;
% for k=1:1 %length(plantlist)
% 	for i=1:ppide{1}.n
% 		subplot(1,ppide{1}.n,idplot)		
% 		% Option 1: normales Mesh
% 		tres = linspace(t_plot{k}(1),t_plot{k}(end),res_surf_t);
% 		zres = linspace(0,1,res_surf_z);
% 		[Z,T]=meshgrid(simSet.zdiscPlant,t_plot{k});
% 		[ZRES,TRES] = meshgrid(zres,tres);
% 		yres = interp2(Z,T,y_plot{k}(:,(i-1)*simSet.ndiscPlant+1:i*simSet.ndiscPlant),ZRES,TRES);
% % 		[T0,Y0] = meshgrid([0 1 1 0],[0 0 1 1]);
% % 		hold on
% 		surf(TRES,ZRES,yres)
% % 		plot3([0 1 1 0 0],[0 0 1 1 0],[0 0 0 0 0],'k');
% % 		view(-40,30);
% 		
% 		% Option 2: mesh_sparse
% % 		mesh_sparse(t_plot{k},zdisc_plant,y_plot{k}(:,(i-1)*ndisc_plant+1:i*ndisc_plant).',lines_t,lines_z,res_surf_t,res_surf_z);
% 		
% 		hold on
% 		box on
% 		grid on
% 		set(gca,'Ydir','reverse')
% 		xlabel('$t$')
% 		ylabel('$z$')
% 		zlabel(['$x_' num2str(i) '(z,t)$'])
% 		idplot=idplot+1;
% 	end
% end

%% plot further
for k=plots
% simulations{k} = simulations{k}.plotTargetAndTransformed();
% simulations{k} = simulations{k}.meshState();
	dps.parabolic.control.backstepping.checkControllerKernel(simulations{k}.ppide,simulations{k}.controllerKernel,1)
	if simulations{k}.useStateObserver
		dps.parabolic.control.backstepping.checkObserverKernel(simulations{k}.ppide,simulations{k}.observerKernel,1)
	end
end

%% Plot Norms
for k=plots
% 	simulations{k}.plotNorm;
% 	simulations{k}.plotU;
end
%%
for k=plots
	simulations{k}.plotTargetAndTransformed;
end

%%
for k=plots
	simulations{k}.plotNorm;
end

%%
for k=plots
	simulations{k}.controllerKernel.plot('K');
end

