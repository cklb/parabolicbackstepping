function ppide = example2(simSet)
% Example 2: experimenting time constant

% Plant setup
n=2; % number of states

% create a gevrey bump
b = signals.gevrey.Bump('K',1,'order',1.5,'N',simSet.ndiscPars);
[bDisc2, tBump2] = b.eval(1);
bDisc3 = bDisc2(floor(length(tBump2)*3/10):floor(length(tBump2)*3/3));
bInterp2 = numeric.interpolant({linspace(0,1,length(bDisc3))},bDisc3);

% plant parameters
% lambda = @(z) [3/2+z.^2.*cos(2*pi*z), 0;0, 1/2-1/4* sin(2*pi*z)];
lambda = @(z) [z.^2+2, 0;0, exp(-z)+1/2];
% lambda_diff = @(z) [2*z.*cos(2*pi*z)-z.^2.*sin(2*pi*z)*2*pi,0;0, -1/4*cos(2*pi*z)*2*pi];
lambda_diff = @(z) [2*z,0;0, -exp(-z)];
% 	a = @(z,t) 1*[bInterp.evaluate(t)-2 3*cos((z+t)*2*pi); (2+3*sin((z+t)*2*pi)) 2+5*(t+1)^(-4)]; %geht!
% a = @(z,t) 1*[exp(z/2)+t^2 -z+bInterp2.evaluate(t); 3*z/2-2+t z^2+t]; %geht!
a = @(z) 1*[exp(z/2) -0*z; 0*3*z/2-0*2 z^2]; %geht!
% 	mu = @(z,t) 1*[10 0; -(2+3*sin((z+t)*2*pi)) 10]; %geht!
% mu =@(z) [10 0;-(2+3*sin((z)*2*pi)) 10];
a0 = @(z) 0*[1*z 1-z; z 1-z]; 
F = @(z,zeta) 0*[exp(z+zeta) exp(z-zeta);1-exp(-(z-zeta)) exp(-(z+zeta))];
x0 = @(z) [dps.parabolic.tool.get_x0_ppides(z,1);dps.parabolic.tool.get_x0_ppides(z,1)];


numDirRight = 0; % number of Dirichlet BCs
% Bdr = @(t) 3*t*eye(n-numDirRight);
Bdr = eye(n-numDirRight,n-numDirRight);
if isa(Bdr,'function_handle')
	b_op2 = dps.parabolic.system.boundary_operator('z_b',1,...
							  'b_d',@(t) blkdiag(eye(numDirRight),Bdr(t)),... %Dirichlet
							  'b_n',blkdiag(zeros(numDirRight),eye(n-numDirRight))); % Robin
else
	b_op2 = dps.parabolic.system.boundary_operator('z_b',1,...
							  'b_d',blkdiag(eye(numDirRight),Bdr),... %Dirichlet
							  'b_n',blkdiag(zeros(numDirRight),eye(n-numDirRight))); % Robin					  
end
numDir = 2; % number of Dirichlet BCs left
% Bd = @(t) -2*sin(pi*t)*eye(n-numDir);
Bd = zeros(n-numDir,n-numDir);
if isa(Bd,'function_handle')
	b_op1 = dps.parabolic.system.boundary_operator('z_b',0,...
							  'b_d',@(t) blkdiag(eye(numDir),Bd(t)),... %Dirichlet
							  'b_n',blkdiag(zeros(numDir,numDir),eye(n-numDir))); %Neumann
else
	b_op1 = dps.parabolic.system.boundary_operator('z_b',0,...
							  'b_d',blkdiag(eye(numDir),Bd),... %Dirichlet
							  'b_n',blkdiag(zeros(numDir),eye(n-numDir))); % Robin	
end

% Measurement, if observer is used
Cm_op = dps.output_operator('c_0',blkdiag(zeros(numDir),eye(n-numDir)),...
							'c_d0',blkdiag(eye(numDir),zeros(n-numDir)));

% Configure controller and observer
mu_mat = 1*eye(n); % geht
mu_o_mat = 1*eye(n);

% Degree of freedom.
clearvars g_strich
for i=1:n
	for j=1:n
		g_strich{i,j} = @(eta) 0+0*eta; % Freiheitsgrad, anonyme Matrixfunktion mit ausschlie�lich Elementen unter der Hauptdiagonalen
	end
end


% Create some space vectors
zdiscKernel = linspace(0,1,simSet.ndiscKernel);
zdiscRegEq = linspace(0,1,simSet.ndiscRegEq);
zdiscTarget = linspace(0,1,simSet.ndiscTarget);
zdiscObsKernel = linspace(0,1,simSet.ndiscObsKernel);
zdiscObs = linspace(0,1,simSet.ndiscObs);
tDiscKernel = linspace(0,1,simSet.ndiscTime);

% ppide als Objekt erzeugen:
ppide = dps.parabolic.system.ppide_sys(...
				'name','Plant',...
				'l',lambda,...
				'l_diff',lambda_diff,...
				'b',@(z) 0*z + zeros(n,n),...  % ver
				'a', a,...
				'b_op1',b_op1,...
				'b_op2',b_op2,...
				'b_1',eye(n,n),...
				'b_2',eye(n,n),... % ein Eingang f�r jeden Zustand
				'ndisc',simSet.ndiscPlant,...
				'ndiscTime',simSet.ndiscTimePlant,...
				'ndiscPars',simSet.ndiscPars,...
				'f',F,...
				'a_0',a0,...
				'cm_op',Cm_op,...
				'x0',x0,...
				'ctrl_pars',dps.parabolic.control.ppde_controller_pars('eliminate_convection',0,...
												 'makeTargetNeumann',1,...
												 'mu',mu_mat,...
												 'mu_o',mu_o_mat,...
												 'zdiscCtrl',zdiscKernel,...
												 'zdiscRegEq',zdiscRegEq,...
												 'zdiscObsKernel',zdiscObsKernel,...
												 'zdiscObs',zdiscObs,...
												 'zdiscTarget',zdiscTarget,...
												 'tDiscKernel',tDiscKernel,...
												 'it_max',simSet.itmax,...
												 'it_min',simSet.itmin,...
												 'tol',simSet.tol,...
												 'g_strich',g_strich,...
												 'min_xi_diff',simSet.min_xi_diff,...
												 'min_eta_diff',simSet.min_eta_diff,...
												 'min_xi_diff_border',simSet.min_xi_diff_border,...
												 'min_eta_diff_border',simSet.min_eta_diff_border));			

											 
% Eigenwerte untersuchen und langsamsten Eigenwert anzeigen:
ppide_appr = dps.parabolic.system.ppide_approximation(ppide);
A = ppide_appr.A;
eigs=[];
for tIdx = 1:size(A,3)
	eigs = [eigs;eig(A(:,:,tIdx))];
end
	
[meigreal, ind] = max(real(eigs));
[meigimag, indImag] = max(imag(eigs));
meig = eigs(ind);
meigImag = eigs(indImag);
% noch verbessern:
if meig>0
	disp(['die Strecke ist instabil: ' num2str(meig)])
else
	disp(['die Strecke ist stabil: ' num2str(meig)])
end

% eigenwerte Zielsystem pr�fen, um sicherzugehen, dass die Wahl von mu
% passt:
ew_target = ppide.eigTarget(ppide.ctrl_pars.mu);
ew_obs = ppide.eigTarget(ppide.ctrl_pars.mu_o);

if max(real(ew_target)) > 0
	error(['Largest eigenvalue of the target system: ' num2str(max(real(ew_target))) '!'])
end
if max(real(ew_obs)) > 0
	error(['Largest eigenvalue of the observer target system: ' num2str(max(real(ew_obs))) '!'])
end

fprintf(['Positivster Eigenwert des Zielsystems (ohne mu) = ' num2str(max(ppide.eigTarget(zeros(n,n)))) '\n']);


end % function
%<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
% ENDE EINGABEN