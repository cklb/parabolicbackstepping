function [ppide, lambda, a] = exampleFolding(simSet)
% example for folding

% Plant setup
n=2; % number of states

% create a gevrey bump
% create a gevrey bump
% b = signals.gevrey.Bump('K',1,'order',1.8,'N',151);
b = signals.GevreyFunction('order',1.8,'N_t',151);
tBump0 = b.grid{1};
b1 = diff(b);
b1Norm = b1/max(b1.on);
tBump1 = tBump0;
bDisc1 = b1Norm.on;

K1 = -4;
O1 = 3;
a11Interp = numeric.interpolant({tBump1},K1*bDisc1+O1);

% plant parameters
lambda = @(z) [z.^2+2, 0;0, exp(-z)+1/2];
lambda_diff = @(z) [2*z,0;0, -exp(-z)];
a = @(z,t) 1*[1.5*a11Interp.evaluate(t) 2+cos((z+t)*2*pi);
			 (2+sin((z+t)*2*pi))      a11Interp.evaluate(t);];
% a = @(z) 1*[1.5 3*cos((z)*2*pi);
% 			 (2+2*sin((z)*2*pi))      2;];
a0 = @(z) zeros(n,n); 
F = @(z,zeta) zeros(n,n);
x0 = @(z) [dps.parabolic.tool.get_x0_ppides(z,1);dps.parabolic.tool.get_x0_ppides(z,1)];

mu_mat = 8*eye(n);
mu_o_mat = 16*eye(n);

numDirRight = 0; % number of Dirichlet BCs
% Bdr = @(t) 3*t*eye(n-numDirRight);
Bdr = 0*eye(n-numDirRight,n-numDirRight);
if isa(Bdr,'function_handle')
	b_op2 = dps.parabolic.system.boundary_operator('z_b',1,...
							  'b_d',@(t) blkdiag(eye(numDirRight),Bdr(t)),... %Dirichlet
							  'b_n',blkdiag(zeros(numDirRight),eye(n-numDirRight))); % Robin
else
	b_op2 = dps.parabolic.system.boundary_operator('z_b',1,...
							  'b_d',blkdiag(eye(numDirRight),Bdr),... %Dirichlet
							  'b_n',blkdiag(zeros(numDirRight),eye(n-numDirRight))); % Robin					  
end
numDir = 0; % number of Dirichlet BCs left
% Bd = @(t) -2*sin(pi*t)*eye(n-numDir);
Bd = -0*2*eye(n-numDir);
% Bd = zeros(n-numDir,n-numDir);
if isa(Bd,'function_handle')
	b_op1 = dps.parabolic.system.boundary_operator('z_b',0,...
							  'b_d',@(t) blkdiag(eye(numDir),Bd(t)),... %Dirichlet
							  'b_n',blkdiag(zeros(numDir,numDir),eye(n-numDir))); %Neumann
else
	b_op1 = dps.parabolic.system.boundary_operator('z_b',0,...
							  'b_d',blkdiag(eye(numDir),Bd),... %Dirichlet
							  'b_n',blkdiag(zeros(numDir),eye(n-numDir))); % Robin	
end

% Measurement, if observer is used
% In-domain measurement for folding
% always must be neumann first, then dirichlet measurement.
Cm_op = dps.output_operator('z_m',0.7,'c_m',[zeros(n,n);eye(n)],'c_dm',[eye(n);zeros(n,n)]);

% Degree of freedom.
clearvars g_strich
for i=1:n
	for j=1:n
		g_strich{i,j} = @(eta) 0+0*eta; % Freiheitsgrad, anonyme Matrixfunktion mit ausschlie�lich Elementen unter der Hauptdiagonalen
	end
end


% Create some space vectors
zdiscKernel = linspace(0,1,simSet.ndiscKernel);
zdiscRegEq = linspace(0,1,simSet.ndiscRegEq);
zdiscTarget = linspace(0,1,simSet.ndiscTarget);
zdiscObsKernel = linspace(0,1,simSet.ndiscObsKernel);
zdiscObs = linspace(0,1,simSet.ndiscObs);
tDiscKernel = linspace(0,1,simSet.ndiscTime);

% ppide als Objekt erzeugen:
folding = 0;
if folding
	b_1 = [eye(n,n),zeros(n,n)];
	b_2 = [zeros(n,n), eye(n,n)];
	b = @(z) 0*z + zeros(n,2*n);
else
	b_1 = [zeros(n,n)];
	b_2 = [eye(n,n)];
	b = @(z) 0*z + zeros(n,n);
end

ppide = dps.parabolic.system.ppide_sys(...
				'name','Plant',...
				'l',lambda,...
				'l_diff',lambda_diff,...
				'b',b,... 
				'a', a,...
				'b_op1',b_op1,...
				'b_op2',b_op2,...
				'b_1',b_1,...  % n oder 2n Eing�nge auf jeder Seite
				'b_2',b_2,... 
				'ndisc',simSet.ndiscPlant,...
				'ndiscTime',simSet.ndiscTimePlant,...
				'ndiscPars',simSet.ndiscPars,...
				'f',F,...
				'a_0',a0,...
				'cm_op',Cm_op,...
				'x0',x0,...
				'ctrl_pars',dps.parabolic.control.ppde_controller_pars('eliminate_convection',0,...
												 'makeTargetNeumann',1,...
												 'mu',mu_mat,...
												 'foldingPoint',0.3,...
												 'mu_o',mu_o_mat,...
												 'zdiscCtrl',zdiscKernel,...
												 'zdiscRegEq',zdiscRegEq,...
												 'zdiscObsKernel',zdiscObsKernel,...
												 'zdiscObs',zdiscObs,...
												 'zdiscTarget',zdiscTarget,...
												 'tDiscKernel',tDiscKernel,...
												 'it_max',simSet.itmax,...
												 'it_min',simSet.itmin,...
												 'tol',simSet.tol,...
												 'g_strich',g_strich,...
												 'min_xi_diff',simSet.min_xi_diff,...
												 'min_eta_diff',simSet.min_eta_diff,...
												 'min_xi_diff_border',simSet.min_xi_diff_border,...
												 'min_eta_diff_border',simSet.min_eta_diff_border,...
												 'maxNumXi',simSet.maxNumXi));

end % function
%<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
% ENDE EINGABEN