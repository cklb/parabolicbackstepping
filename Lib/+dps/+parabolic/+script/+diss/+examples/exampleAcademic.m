function [ppide, lambda,a,mu_mat,a0,F,g_strich,bDisc0,tBump0,bDisc1,tBump1] = exampleAcademic(simSet)
% In this function, the plant to be controlled in the academic examples simulated by the
% script simulationAcademicExample is created.

% At first the parameters are stored into local variables and in the end, the actual object is
% created.

% Plant setup
n=3; % number of states

% the plant parameters contain Gevrey-functions, which are created here.
% this is a Gevrey step
b = signals.GevreyFunction('order',1.8,'N_t',151);
tBump0 = b.grid{1};
bDisc0 = b.on;
% differentiating the step leads to the bump
b1 = diff(b);
b1Norm = b1/max(b1.on); % normed bump
tBump1 = tBump0;
bDisc1 = b1Norm.on;

% gain and offsets of the gevrey functions.
K1 = -4;
O1 = 3;
K2 = 1;
O2 = 0;
% the paramters must be given as functions of z or z and t. 
% Since the gevrey functions are only available in discretized form, they are interpolated as
% anonymous functions.
a11Interp = numeric.interpolant({tBump1},K1*bDisc1+O1);
bInterp = numeric.interpolant({tBump0},K2*bDisc0+O2);

% The actual plant parameters
% diffusion matrix:
lambda = @(z) [3/2+z.^2.*cos(2*pi*z), 0, 0;
			   0, 3/2+z.^2.*cos(2*pi*z), 0;
			   0 0 1.8*(1/2-1/4*sin(2*pi*z-pi/6))];
		   
% optional: its spatial derivative. (If not passed, it is calculated numerically!)
lambda_diff = @(z) [2*z.*cos(2*pi*z)-z.^2.*sin(2*pi*z)*2*pi,0, 0;
					0,2*z.*cos(2*pi*z)-z.^2.*sin(2*pi*z)*2*pi, 0;
					0, 0, -1.8*1/4*cos(2*pi*z-pi/6)*2*pi];
				
% the reaction matrix:
a = @(z,t) 1*[1.5*a11Interp.evaluate(t) 3*cos((z+t)*2*pi)     bInterp.evaluate(t);
			 (2+2*sin((z+t)*2*pi))      a11Interp.evaluate(t) 1;
			  3*cos((z+t)*2*pi)         -bInterp.evaluate(t)  1.5*a11Interp.evaluate(t)];

% local matrix
a0 = @(z,t) [0 sin(2*pi*(z+t)) cos(2*pi*(z+t));
			 0 sin(2*pi*(z-t)) cos(2*pi*(z-t));
			 0 sin(pi*(z+t))   cos(pi*(z+t))];
		 
% integral term
F = @(z,zeta,t) [exp(z+zeta+(1-t)) exp(z-zeta-t) sin(2*pi*(z-t));
				 cos(pi*(z+zeta-t)) exp(-z+zeta-t) 1-exp(-z-zeta-t);
				 cos(2*pi*(-z-zeta+t)) exp(-z-zeta-t) sin(pi*(z+zeta-t))];
			 
% initial value of the plant
x0 = @(z) [dps.parabolic.tool.get_x0_ppides(z,1);dps.parabolic.tool.get_x0_ppides(z,1);dps.parabolic.tool.get_x0_ppides(z,1)];

% the reaction matrices of the target systems:
mu_mat = 8*eye(n);
mu_o_mat = 15*eye(n);

% the BCs
numDirRight = 0; % number of Dirichlet BCs right
% the BCs must be specified as dps.parabolic.system.boundary_operator
% open it to see its definition.

% right BC:
Bdr = @(t) t*(1-t)*2*diag((n-numDirRight):-1:1);
if isa(Bdr,'function_handle')
	b_op2 = dps.parabolic.system.boundary_operator('z_b',1,...
							  'b_d',@(t) blkdiag(eye(numDirRight),Bdr(t)),... %Dirichlet
							  'b_n',blkdiag(zeros(numDirRight),eye(n-numDirRight))); % Robin
else
	b_op2 = dps.parabolic.system.boundary_operator('z_b',1,...
							  'b_d',blkdiag(eye(numDirRight),Bdr),... %Dirichlet
							  'b_n',blkdiag(zeros(numDirRight),eye(n-numDirRight))); % Robin					  
end

% left BC:
numDir = 1; % number of Dirichlet BCs left
Bd = @(t) -eye(n-numDir)*bInterp.evaluate(t);
if isa(Bd,'function_handle')
	b_op1 = dps.parabolic.system.boundary_operator('z_b',0,...
							  'b_d',@(t) blkdiag(eye(numDir),Bd(t)),... %Dirichlet
							  'b_n',blkdiag(zeros(numDir,numDir),eye(n-numDir))); %Neumann
else
	b_op1 = dps.parabolic.system.boundary_operator('z_b',0,...
							  'b_d',blkdiag(eye(numDir),Bd),... %Dirichlet
							  'b_n',blkdiag(zeros(numDir),eye(n-numDir))); % Robin	
end

% Measurement, if observer is used
% for further information, open the dps.output_operator
Cm_op = dps.output_operator('c_0',blkdiag(zeros(numDir),eye(n-numDir)),...
							'c_d0',blkdiag(eye(numDir),zeros(n-numDir)));


% Degree of freedom.
clearvars g_strich
for i=1:n
	for j=1:n
		g_strich{i,j} = @(eta) 0+0*eta; % Freiheitsgrad, anonyme Matrixfunktion mit ausschlie�lich Elementen unter der Hauptdiagonalen
	end
end

% Some spatial discretizations are passed as vectors, not just the number of nodes. However, it
% is NOT recommended to change to a non-linear grid, since it may be that internally, a linear
% grid is created at some point.
zdiscKernel = linspace(0,1,simSet.ndiscKernel);
zdiscRegEq = linspace(0,1,simSet.ndiscRegEq);
zdiscTarget = linspace(0,1,simSet.ndiscTarget);
zdiscObsKernel = linspace(0,1,simSet.ndiscObsKernel);
zdiscObs = linspace(0,1,simSet.ndiscObs);
tDiscKernel = linspace(0,1,simSet.ndiscTime);

% Now the actual object of class
% dps.parabolic.system.ppide_sys is created.
% It uses the "name-value-pair" syntax and arguments which are not passed are set to their
% default values (typically 0)
ppide = dps.parabolic.system.ppide_sys(...
				'name','Plant',... 
				'l',lambda,... % diffusion matrix
				'l_diff',lambda_diff,... % and its derivative. Remember, if not passed, it is computed numerically				'b',@(z) 0*z + zeros(n,n),...  % 
				'a', a,... % reaction matrix
				'b_op1',b_op1,... % left BC
				'b_op2',b_op2,... % right BC
				'b_1',eye(n,n),... % left input matrix b_op1(t)[x(t)](0) = b_1 * u_left
				'b_2',eye(n,n),... % right input matrix b_op2(t)[x(t)](1) = b_2 * u_right
				'ndisc',simSet.ndiscPlant,... % number of nodes for simulation
				'ndiscTime',simSet.ndiscTimePlant,... % time-resolution for simulation
				'ndiscPars',simSet.ndiscPars,... % resolution with which the discretized parameters are stored
				'f',F,... % integral term
				'a_0',a0,... % local coupling term
				'cm_op',Cm_op,... % measurement output
				'x0',x0,... % initial value
				'ctrl_pars',dps.parabolic.control.ppde_controller_pars(... % everything needed to parameterize the backsteppign controller and observer is stored in this object
					'makeTargetNeumann',1,...% whether robin BCs are converted to Neumann-BCs in the target system or not. If the BC is time-dependent, it must be set to true (1)!
					'mu',mu_mat,... % reaction parameters of target systems
					'mu_o',mu_o_mat,... 
					'zdiscCtrl',zdiscKernel,... % discretized z/zeta of kernel in original coordinates
					'zdiscRegEq',zdiscRegEq,... % discretized z of the regulator equations if calculated. Actually not needed here, but passed for completeness.
					'zdiscObsKernel',zdiscObsKernel,... % discretized z/zeta of observer kernel in original coordinates
					'zdiscObs',zdiscObs,... % discretized z for the observer REALIZATION
					'zdiscTarget',zdiscTarget,... % discretized z of the target system for simulation
					'tDiscKernel',tDiscKernel,... % discretized t for the kernel calculation. ONLY USE LINEAR SPACING!
					'it_max',simSet.itmax,... % maximum iterations 
					'it_min',simSet.itmin,... % minimum iterations
					'tol',simSet.tol,... % tolerance to stop the fixed-point iteration
					'g_strich',g_strich,... % DOF
					'min_xi_diff',simSet.min_xi_diff,... % minimum distance of two points in xi-coordinates
					'min_eta_diff',simSet.min_eta_diff,... % minimum distance of two points in eta-coordinates. (no more used, because the distances in xi and eta coordinates are equal to make plotting easier.)
					'min_xi_diff_border',simSet.min_xi_diff_border,... % minimum distances of two points which lie on a boundary
					'min_eta_diff_border',simSet.min_eta_diff_border));			

end % function
%<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
% END OF INPUTS