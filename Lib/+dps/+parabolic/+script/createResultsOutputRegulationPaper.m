
addpath(genpath([pwd '\' 'external']));
addpath(genpath([pwd '\..\' 'miscLib']));

set(groot,'defaulttextinterpreter','none'); 


%% Pfad und Namen der Simulationen:
path = 'results/ppides/FSA/autPaper/';
names{1} = 'simRef';
names{2} = 'simDist';

import misc.*
import numeric.*
import dps.parabolic.system.*
import dps.parabolic.control.backstepping.*

% wichtigsten Aufloesungen gleich hier festlegen:
ndiscKernel = 81;% Aufloesung Kern und Regulator Equations
ndiscObsKernel = 51; % Aufloesung Beobachterkern und Entkopplungsgleichungen

% Vorhandene Dateien laden, falls existent:
for i=1:length(names)
	if ~exist(names{i},'var')
		srchstr = [path... % path
			'*'...                    % new date
			names{i}...                 % new name
			'_nc'...
			num2str(ndiscKernel)... % ndiscCtrl
			'_noK'... % rest unchanged
			num2str(ndiscObsKernel)...
			'.mat'];

		fls = dir(srchstr);
		if ~isempty(fls)
			load([path fls(end).name]);
		end
	end
end


%%
% Simulationskonfiguration
sim_pars = simulation_parameters('frel',1e-6,...
								 'fabs',1e-6,... 
								 'tsmax',2e-2); % 8
							 
plot_pars.plot_res_1D = 50;
plot_pars.plot_res_2D_lines = 150;
plot_pars.plot_res_2D_num_lines = 20;
							
% Sukzessive Approximation:
itmax = 150; 
itmin = 1;
tol = 1e-4; % Abbruchkriterium sukzessive Approximation
min_xi_diff = 0.01; % numerischer Parameter, f�r numerische Effizienz
min_eta_diff = 0.01;

ndiscPars = 401; % Aufloesung gespeicherte Parameter
ndiscRegEq = 201; % Aufloesung Regulator equations
ndiscPlant = 201;% Aufl�sung Streckenapproximation
ndiscObs = 21; % Aufloesung Beobachterrealisierung
ndiscTarget = ndiscPlant; % Aufloesung Zielsystem (f�r Analysezwecke)
 
% Strecke festlegen:
n=2; % Anzahl Zust�nde

% Streckenparameter

if n==1
	lambda = @(z) 1+0*z; 
	C = @(z) 0+0*z;
	a = @(z) 1+0*z;
	a0 = @(z) 0+0*z;
	F = @(z,zeta) 0*z+0*zeta;
	x0 = @(z) get_x0_ppides(z,1);
	c_op = output_operator('c_m',1,'z_m',0.5,'c_0',0);
elseif n==2
	lambda = @(z) [3/2+z.^2.*cos(2*pi*z), 0;0, 1/2-1/4*sin(2*pi*z)];
	lambda_diff = @(z) [2*z.*cos(2*pi*z)-z.^2.*sin(2*pi*z)*2*pi,0;0, -1/4*cos(2*pi*z)*2*pi];
	C = @(z) [0 0; 0 0]; % Konvektionsmatrix
	a = @(z) 1*[1 1+z; 1/2+z 1]; 
	a0 = @(z) 1*[1*z 1-z; z 1-z]; 
	F = @(z,zeta) 1*[exp(z+zeta) exp(z-zeta);1-exp(-(z-zeta)) exp(-(z+zeta))];
% 	x0 = @(z) [get_x0_ppides(z,1);get_x0_ppides(z,1);];
	% Ausgangsoperator definieren:
	c_op = dps.output_operator('c_m',[1 0;0 0],'z_m',0.25,'c_c',@(z) [0 0;0 1]);
else 
	error(['n = ' num2str(n) ' not parametrized']);
end
% Messung
Cm_op = dps.output_operator('c_0',eye(n));

% Rechte Randbedingung festlegen:
Bdr = 0*eye(n,n);
Bnr = eye(n,n);
b_op2 = boundary_operator('z_b',1,...
						  'b_d',-Bdr,...
						  'b_n',Bnr);

% linken Rand-Operator definieren:
numDir = 0; % number of Dirichlet BCs
Bd = -1*diag(n:-1:1);
% Bd = eye(n);
% Bd = eye(n);
b_op1 = boundary_operator('z_b',0,...
						  'b_d',blkdiag(eye(numDir),Bd),... %Dirichlet
						  'b_n',blkdiag(zeros(numDir,numDir),eye(n-numDir))); %Neumann
					  
% Regelung konfigurieren:
mu_mat = 5*eye(n);
mu_o_mat = 2*mu_mat;

% Freiheitsgrad zu 0 festlegen.
for i=1:n
	for j=1:n
		g_strich{i,j} = @(eta) 0+0*eta; % Freiheitsgrad, anonyme Matrixfunktion mit ausschlie�lich Elementen unter der Hauptdiagonalen
	end
end


% Ortsvektoren festlegen
zdiscKernel = linspace(0,1,ndiscKernel);
zdiscRegEq = linspace(0,1,ndiscRegEq);
zdiscPlant = linspace(0,1,ndiscPlant);
zdiscTarget = linspace(0,1,ndiscTarget);
zdisc_obs_kernel = linspace(0,1,ndiscObsKernel);
zdiscObs = linspace(0,1,ndiscObs);
zdiscPars = linspace(0,1,ndiscPars);

% F wird in diskreter Form �bergeben
% F_disc = zeros(ndiscPars,ndiscPars,n,n);
% for z_idx = 1:ndiscPars
% 	F_disc(z_idx,:,:,:) = eval_pointwise(@(zeta) F(zdiscPars(z_idx),zeta),zdiscPars);
% end

% Signalmodell parametrieren
Sd = [0 4;-4 0];
Sr1 = [0 1;0 0];
Sr2 = [0 2;0 0];
pd_til = [1 0];
pr_til1 = [0.5 2];
pr_til2 = [0.5 -5];
Sr = blkdiag(Sr1,Sr2);
pr_til = blkdiag(pr_til1,pr_til2);
sM = dps.parabolic.control.outputRegulation.signalModel(Sd,Sr,pd_til,pr_til);

% Eigenwertvorgabe St�r- und F�hrungsbeobachter:
eigDist = eig(sM.Sd)-10;  % Eigenwerte St�rbeobachter
eigDist = [-6.5+1.8i -6.5-1.8i];  % Eigenwerte St�rbeobachter, just starting values, placeLQR is used internally.
eigRef = (-3-(1:sM.nr)).';  % Eigenwerte F�hrungsbeobachter
% eigRef = [-;-11;-12;-7];
	  
% ppide als Objekt erzeugen:
ppide = ppide_sys(...
				'l',lambda,...
				'l_diff',lambda_diff,...
				'c',C,...
				'b',@(z) 0*z + zeros(n,n),...  % ver
				'a', a,...
				'b_op1',b_op1,...
				'b_op2',b_op2,...
				'b_1',eye(n,n),...
				'b_2',eye(n,n),... % ein Eingang f�r jeden Zustand
				'g_2',ones(n,1),... % St�rung am linken Rand
				'g_3',ones(n,1),... % St�rung am rechten Rand
				'g_1',@(z) 2*ones(n,1),...
				'c_op',c_op,...
				'cm_op',Cm_op,...
				'ndisc',ndiscPlant,...
				'ndiscPars',ndiscPars,...
				'f',F,...
				'a_0',a0,...
				'ctrl_pars',dps.parabolic.control.ppde_controller_pars('mu',mu_mat,...
												 'mu_o',mu_o_mat,...
												 'zdiscCtrl',zdiscKernel,...
												 'zdiscRegEq',zdiscRegEq,...
												 'zdiscObsKernel',zdisc_obs_kernel,...
												 'zdiscObs',zdiscObs,...
												 'zdiscTarget',zdiscTarget,...
												 'eigDist',eigDist,...
												 'eigRef',eigRef,...
												 'it_max',itmax,...
												 'it_min',itmin,...
												 'tol',tol,...
												 'g_strich',g_strich,...
												 'min_xi_diff',min_xi_diff,...
												 'min_eta_diff',min_eta_diff));			
											 
% Eigenwerte untersuchen und langsamsten Eigenwert anzeigen:
ppide_appr = ppide_approximation(ppide);
A = ppide_appr.A;											 
meig = max(eig(A));
% noch verbessern:
if meig>0
	disp(['die Strecke ist instabil: ' num2str(meig)])
else
	disp(['die Strecke ist stabil: ' num2str(meig)])
end

% eigenwerte Zielsystem pr�fen, um sicherzugehen, dass die Wahl von mu
% passt:
ew_target = ppide.eigTarget(ppide.ctrl_pars.mu);
ew_obs = ppide.eigTarget(ppide.ctrl_pars.mu_o);

if max(real(ew_target)) > 0
	error(['Largest eigenvalue of the target system: ' num2str(max(real(ew_target))) '!'])
end
if max(real(ew_obs)) > 0
	error(['Largest eigenvalue of the observer target system: ' num2str(max(real(ew_obs))) '!'])
end

fprintf(['muMax = ' num2str(max(ppide.eigTarget(zeros(n,n)))) '\n']);

%<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
% ENDE EINGABEN



%% Simulate reference behaviour:
% create Initial values of signal model states
tickL = 3;
bigTicks = 3;
bigBlocks = 4;
tv = 0:tickL:tickL*bigTicks*bigBlocks-tickL;
smallTicks = 2;
r1C = 0;
r2C = 0;
v0_ref(1,:) = [zeros(1,sM.nd) 0 1 0 1];
for t_idx = 2:length(tv)
	% S�gezahn
	v0_ref(t_idx,:) = [zeros(1,sM.nd) 0 1 0 1];
	if r1C < bigTicks-1
		v0_ref(t_idx,3) = NaN;
		v0_ref(t_idx,4) = NaN;
		r1C = r1C+1;
	else
		r1C = 0;
	end
	if r2C < smallTicks-1
		v0_ref(t_idx,5) = NaN;
		v0_ref(t_idx,6) = NaN;
		r2C = r2C+1;
	else
		r2C = 0;
	end
end

sim_pars.tend = tickL*bigTicks*bigBlocks;
simRefpars = {...
		'simPars',sim_pars,...
		'sM',sM,...
		'v0',v0_ref,...
		'tv',tv,...
		'vHat0',zeros(sM.nv,1),...
		'plotPars',plot_pars,...
		'useReferenceObserver',1,...
		'useDisturbanceObserver',1,...
		'useStateObserver',1,...
		'useObserverInFeedback',1,...
		'useStateFeedback',1,...
		'useOutputRegulation',1,...
		'name',names{1},...
		'path',path,...
};
% 		'xHat0',0*IPOTot*x0_disc,...

if ~exist('simRef','var')
	if ~exist('simDist','var')
		simRef = ppide_simulation(ppide,simRefpars);
	else
		simRef = simDist; % Kerne etc. wurden vllt schon berechnet und koennen uebernommen werden!
		simRef = simRef.setPars(ppide,simRefpars);
	end
else
	simRef = simRef.setPars(ppide,simRefpars);
end
simRef = simRef.initializeSimulation;
if ~simRef.controllerKernelFlag
	simRef = simRef.computeControllerKernel();
% 	save(simRef.saveStr,'simRef');
end
if ~simRef.observerKernelFlag
	simRef = simRef.computeObserverKernel();
% 	save(simRef.saveStr,'simRef');	
end
if ~simRef.simulated
	simRef = simRef.simulate;
% 	save(simRef.saveStr,'simRef');	
end
% simRef = simRef.plotOutput();

%% Simulate disturbance behaviour
sim_parsDist = sim_pars;
sim_parsDist.tend = 5;
v0Dist = [0 1 zeros(1,sM.nr)];% simulate_ppide_sys
tvDist = 0;

simDistpars = {...
		'v0',v0Dist,...
		'simPars',sim_parsDist,...
		'sM',sM,...
		'tv',tvDist,...
		'vHat0',zeros(sM.nv,1),...
		'plotPars',plot_pars,...
		'useReferenceObserver',1,...
		'useDisturbanceObserver',1,...
		'useStateObserver',1,...
		'useStateFeedback',1,...
		'useObserverInFeedback',1,...
		'useOutputRegulation',1,...
		'name',names{2},...
		'path',path
};
if ~exist('simDist','var')
	if ~exist('simRef','var')
		simDist = ppide_simulation(ppide,simDistpars);
	else
		simDist = simRef; % Kerne etc. wurden vllt schon berechnet und koennen uebernommen werden!
		simDist = simDist.setPars(ppide,simDistpars);
	end
else
	simDist = simDist.setPars(ppide,simDistpars);
end
simDist = simDist.initializeSimulation;
if ~simDist.controllerKernelFlag
	simDist = simDist.computeControllerKernel();
% 	save(simDist.saveStr,'simDist');
end
if ~simDist.observerKernelFlag
	simDist = simDist.computeObserverKernel();
% 	save(simDist.saveStr,'simDist');	
end
% if ~simDist.simulated
	simDist = simDist.simulate;
% 	save(simDist.saveStr,'simDist');	
% end
% simDist = simDist.initializeSimulation;
% % simDist = simDist.plotOutput();
% simDist = simDist.plotDistEstimation();

%% Plots erzeugen
% (nicht mit vorgefertigten Funktionen, um manuell Darstellung anzupassen:
plotStyle = {};
plotStyle{1,1} = 'k';
plotStyle{1,2} = 'k';
plotStyle{2,1} = 'k';
plotStyle{2,2} = 'k--';
xLims = {};
xLims{1} = [0 22];
xLims{2} = [0 3];


for i=1:length(names)
	eval(['obj = ' names{i} ';']);
	output = (obj.ppideApproximation.C*obj.x.').';
	reference = (obj.sM.pr*obj.v.').';

	figure('Name','Outputs')
% 			plot(t_BS,output_BS,'b','DisplayName','State feedback')
	hold on
	box on
	xlabel('$t$')
	if i<2
		title('$r(t)(\ref{plot:r})$, $y(t)(\ref{plot:y})$')
	end
	titletext=[];
	for j = 1:obj.ppide.n
		plot(obj.t,output(:,j),plotStyle{i,j});
		if i<2
			plot(obj.t,reference(:,j),'k--');
		end
		if i>1
			titletext{j}=['$y_' num2str(j) '(t)(\ref{plot:' num2str(j) '})$'];
		end
	end
	if i>1
		titleTxt = [titletext{1} ', ' titletext{2}];
		title(titleTxt);
	end
	xlim(xLims{i});
end

%%
figure('name','RealAndObservedDisturbance')
hold on
box on
plot(obj.t,(obj.sM.pd*obj.v.').','k--');
plot(obj.t,(obj.sM.pd*obj.vHat.').','k');
xlabel('$t$')
title('$d(t)(\ref{plot:d}),\hat{d}(t)(\ref{plot:dHat})$')
xlim(xLims{i});

%%
simRef.plotRegEq;
simDist.plotRegEq;

	

%% resultierende Plots exportieren:
% export2tikz([path 'plots/' simRef.name],'width','0.35\linewidth','height','0.35\linewidth',...
% 	        'parseStrings',false,...
% 			'extraAxisOptions',...
% 			['every axis title/.append style={font=\footnotesize},',...
% 			'every axis label/.append style={font=\footnotesize},'...
% 			'every tick label/.append style={font=\scriptsize},'...
% 			'yticklabel style={/pgf/number format/fixed,/pgf/number format/precision=2},'...
% 			],...
% 			'extraTikzpictureOptions',...
% 			'scale=\tikzscale,remember picture',...
% 			'floatFormat','%.6g'...
% 			);%,...
% 	close all
	%
%           'every x tick label/.style={},'...
% 			'every y tick label/.style={},'...
%           'every z tick label/.style={},'...
%%
% simulate_ppide_sys_disturbance;

















