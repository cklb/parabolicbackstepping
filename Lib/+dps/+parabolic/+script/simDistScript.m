%simDistScript
sim_pars.tend = 1;
sim2pars = {...
		'x0',x0,...
		'simPars',sim_pars,...
		'plotPars',plot_pars,...
		'useReferenceObserver',0,...
		'useDisturbanceObserver',0,...
		'useStateObserver',0,...
		'useStateFeedback',1,...
		'useOutputRegulation',0,...
		'useIntMod',0,...
		'name',names{1},...
		'path',myPath
};

if ~exist('sim2','var')
	if ~exist('sim1','var')
		sim2 = dps.parabolic.control.backstepping.ppide_simulation(ppide2,sim2pars);
	else
		sim2 = sim1; % Kerne etc. wurden vllt schon berechnet und koennen uebernommen werden!
		sim2 = sim2.setPars(ppide2,sim2pars);
	end
else
	sim2 = sim2.setPars(ppide2,sim2pars);
end

%%
ppidek = dps.parabolic.control.backstepping.ppide_kernel(ppide2,ppide.ctrl_pars.zdiscCtrl);
%%
use_F_handle = @(ppidek) dps.parabolic.control.backstepping.use_F_ppides(ppidek,ppide2);
sim2.controllerKernel = dps.parabolic.tool.fixpoint_iteration(ppidek,use_F_handle,ppide2.ctrl_pars.it_min,ppide2.ctrl_pars.it_max,ppide2.ctrl_pars.tol);
%%
sim2.controllerKernel.transformed = 0;
sim2.controllerKernel = sim2.controllerKernel.transform_kernel(ppide2);
sim2.controllerKernelFlag = 1;
%%
% noch nicht zeitabhaengig implementiert!
%sim2.controllerKernel.plot('G')
%%
sim2.controllerKernel.checked_PDE = 0;
sim2.controllerKernel = dps.parabolic.control.backstepping.check_pde(sim2.controllerKernel,ppide2);
sim2.controllerKernel.checked_boundaries = 0;
sim2.controllerKernel = dps.parabolic.control.backstepping.check_ppde_boundaries(sim2.controllerKernel,ppide2);
sim2.controllerKernel.plot('err');
sim2.controllerKernel.plot('errb');


%%
sim2.controllerKernel.plot('K')
%%
sim2.controller = dps.parabolic.control.backstepping.ppide_controller(sim2.controllerKernel,ppide2,1);
sim2.controllerFlag =1;

%%
sim2.simulated = 0;
sim2 = sim2.initializeSimulation;
% if ~sim2.controllerKernelFlag
% 	sim2 = sim2.computeControllerKernel();
% 	save(sim2.saveStr,'sim2');
% end
% if ~sim2.observerKernelFlag
% 	sim2 = sim2.storeObserverKernel();
% 	save(sim2.saveStr,'sim2');	
% end
if ~sim2.simulated
% 	sim2.useStateFeedback = 1;
	sim2 = sim2.simulate;
	save(sim2.saveStr,'sim2');	
end
sim2.meshState
%%
sim2.simulatedTarget = 0;
sim2=sim2.plotTargetAndTransformed;


%%
% ||h|| = (int_0^1 ||h||^2_Cn dz)^1/2
y = sim2.x;
y_target = sim2.xETarget;
y_transformed = sim2.xTransformed;
t = sim2.t;
t_target = sim2.tTarget;
mu_max = max(real(ew_target));
x0_target_quad_sum = 0;
x0_transformed_quad_sum = 0;
x0_target = sim2.xETarget(1,:);
for i=1:n
	x0_target_quad_sum = x0_target_quad_sum + x0_target((i-1)*ndiscTarget+1:i*ndiscTarget).^2;
end
x0_target_L2_norm = sqrt(numeric.trapz_fast(zdiscTarget,x0_target_quad_sum));
x_target_soll = exp((mu_max)*t_target);
y_quad_sum = 0;
y_target_quad_sum = 0;
y_transformed_quad_sum = 0;

lPlant = misc.translate_to_grid(ppide.l_disc,{zdiscPlant});
lTarget = misc.translate_to_grid(ppide.l_disc,{zdiscTarget});

for i=1:n
	y_quad_sum = y_quad_sum + (permute(lPlant(:,i,i,:),[4,1,2,3]).^(-1/2).*y(:,(i-1)*ndiscPlant+1:i*ndiscPlant)).^2;
	y_target_quad_sum = y_target_quad_sum + (permute(lPlant(:,i,i,:),[4,1,2,3]).^(-1/2).*y_target(:,(i-1)*ndiscTarget+1:i*ndiscTarget)).^2;
	y_transformed_quad_sum = y_transformed_quad_sum + (permute(lPlant(:,i,i,:),[4,1,2,3]).^(-1/2).*y_transformed(:,(i-1)*ndiscPlant+1:i*ndiscPlant)).^2;
end
y_2_norm_quad = (y_quad_sum);
% 	for t_idx = 1:length(t)
y_L2_norm = sqrt(numeric.trapz_fast(zdiscPlant,y_2_norm_quad));
y_target_L2_norm = sqrt(numeric.trapz_fast(zdiscPlant,y_target_quad_sum));
y_transformed_L2_norm = sqrt(numeric.trapz_fast(zdiscPlant,y_transformed_quad_sum));
% 	end
% 	y_norm_sum(t_idx) = y_norm_sum(t_idx) + sqrt(sum(y(t_idx,(i-1)*ndiscPlant+1:i*ndiscPlant).^2))/ndiscPlant;
% 	y_norm_ges0 = sqrt(sum(y_norm(1,:).^2));
figure('Name','Norm_geregeltes_System')
hold on
plot(t,y_L2_norm,'DisplayName','ist');
yl = get(gca,'ylim');
plot(t,12*exp((mu_max)*t),'lineWidth',2,'Color','green','DisplayName','soll')
plot(sim2.tTransformed,y_transformed_L2_norm,'DisplayName','transformiert')
set(gca,'ylim',yl);
plot(t_target,y_target_L2_norm,'b:','LineWidth',2,'DisplayName','target_ist');	
% 	plot(t_target,1.5*max(y_target_L2_norm)*x_target_soll,'g:','lineWidth',2,'DisplayName','target_soll')
legend('-DynamicLegend');


%% Plot the time-slope of a kernel point
figure
hold on
K = sim2.controllerKernel.get_K;
for zIdx = 1:2:ndiscKernel
	for zetaIdx =1:2:zIdx
		plot(sim2.ppide.ctrl_pars.tDiscKernel,squeeze(K(zIdx,zetaIdx,1,1,:)))
	end
end

%%
save(sim2.saveStr,'sim2');	
%% Kern animieren �ber Zeit:
% x11 = [squeeze(K(:,:,1,1,:)).' squeeze(K(:,:,1,2,:)).' squeeze(K(:,:,2,1,:)).' squeeze(K(:,1,2,2,:)).'];
idx=0;
clearvars xAnim;
for i=1:n
	for j=1:n
		idx=idx+1;
		xAnim(:,(idx-1)*ndiscKernel+1:idx*ndiscKernel,:) = permute(K(:,:,i,j,:),[5 1 2 3 4]);
	end
end
t = sim2.ppide.ctrl_pars.tDiscKernel;
animation_time = 4;
fps = 30;
misc.animate_state(xAnim,t,n^2,zdiscKernel,animation_time,fps)

% WAS �BERLEGEN!
% for tIdx=1:length(sim2.ppide.ctrl_pars.tDiscKernel)

% end

%%
figure
hold on
for i=1:n
	for j=1:n
		plot(sim2.ppide.ctrl_pars.tDiscKernel,squeeze(K(misc.get_idx(0.4,zdiscKernel),misc.get_idx(0.1,zdiscKernel),i,j,:)),'DisplayName',[num2str(i) num2str(j)]);		
	end
end
legend('-dynamicLegend')

