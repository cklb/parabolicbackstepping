% Example for calculating a kernel
% Simon Kerschbaum, 07.10.2019

%% 1. Define some parameters:
% need to be in folder infinitefirebutterfly and conl must be in the superdirectory, named.
conlName = 'coni';
addpath(genpath([pwd '\' 'external']));
addpath(genpath([pwd '\..\' conlName]));
set(groot,'defaulttextinterpreter','none'); % do not interpret tex-string for export 
rmpath(genpath([pwd '\external\matlab2tikz1.0']));
rmpath(genpath([pwd '\external\matlab2tikz_0.4.6']));

%% 2. Define the plant, for with the kernel is calculated.
% The plant is an object of type dps.parabolic.system.ppide_sys;
% To get further information about the parameters that can be stored, just open the file.
%
% System Model:
% \dot{x} = x'' + (z+1)*x
%  x'(0) = 3*x(0)
%  x'(1) = u

% leads to the parameters
n=1; % number of states
% diffusion coefficient
lambda = @(z) 1+0*z; 
% passing the derivative of lambda explicitly increases computation accuracy. If not passed, it
% will be calculated numerically.
lambda_diff = @(z)0*z;
% reaction matrix
a = @(z) z+1;
% output operator
c_op = dps.output_operator('c_0',1);

% left boundary condition
boundaryOperatorLeft = dps.parabolic.system.boundary_operator(...
	'z_b',0,... the respective boundary (left)
	'b_n',eye(n),... neumann matrix
	'b_d',-3*eye(n)); % dirichlet matrix
% right boundary condition
boundaryOperatorRight = dps.parabolic.system.boundary_operator(...
	'z_b',1,... the respective boundary (left)
	'b_n',eye(n),... neumann matrix
	'b_d',zeros(n)); % dirichlet matrix

% signalModell
Sd = 0;
Sr = [0 pi; -pi 0];
pd_til = [1];
pr_til = [1 0];

sigMod =  dps.parabolic.control.outputRegulation.signalModel(Sd,Sr,pd_til,pr_til);

% Eigenwerte internes Modell
eigInt = [eig(Sd);eig(Sr)];
By = ones(sigMod.nv,1);
if rank(ctrb(sigMod.S,By)) < sigMod.nv
	error('The pair (S,By) must be controllable!')
end

% the object ppide_sys also stores the parameters for the kernel caluclation.
% Thus they need to be stored in an object of class dps.parabolic.control.ppde_controller_pars.
% Just open for further information about parameters.
ctrlPars = dps.parabolic.control.ppde_controller_pars(...
	'mu',5*eye(n),...  reaction parameter of the target system
    'By',By,...
    'eigInt',eigInt,...
	'eliminate_convection',0,... parameter for internal caluclations, just always set to 0!
	'zdiscCtrl',linspace(0,1,31)... % spatial vector for kernel calculation
	);

% Put everything together in ppide_sys object:
ndiscPlant = 101;
zdiscPlant = linspace(0,1,ndiscPlant);
ppide = dps.parabolic.system.ppide_sys(...
	'n',n,...
	'ndisc',ndiscPlant,...
	'l',lambda,...
	'l_diff',lambda_diff,...
	'a',a,...
	'b_op1',boundaryOperatorLeft,...
	'b_op2',boundaryOperatorRight,...
	'b_2',eye(n),... right boundary input matrix
    'c_op',c_op,...
	'ctrl_pars',ctrlPars);
	
%% 3. Calculate kernel
% initialize kernel object. A kernel is always stored as 
% dps.parabolic.control.backstepping.ppide_kernel
% ppide,spatial vector for kernel calculation
kernel = dps.parabolic.control.backstepping.ppide_kernel(ppide,ppide.ctrl_pars.zdiscCtrl);

% now the kernel is calculated via successive approximation (fixed point iteration)
% 1. the function handle used for the update law is parametrized by the ppide. 
use_F_handle = @(kernelObj) dps.parabolic.control.backstepping.use_F_ppides(kernelObj,ppide);

% 2. the kernel object is calculated
controllerKernel = dps.parabolic.tool.fixpoint_iteration(...
	kernel,use_F_handle,...
	3,... % minimum number of iteration steps
	20,... maximum number of iteration steps
	1e-3... tolerance to abort fixed-point iteration
); % numers of iterations and tolerance can be parametrized

%%
% 3. the result needs to be transformed in original coordinates
controllerKernel = controllerKernel.transform_kernel(ppide);

%% 4. Evaluate/visualize kernel:
% plot:
controllerKernel.plot('K');
% get the values of K as matrix:
K = controllerKernel.get_K;
%% 5. Calculate RobustDecouplingEquations:
    
[  KvHat,KvQPlant, Q, QTil, D] = dps.parabolic.control.backstepping.robustDecouplingEquations(ppide,sigMod,controllerKernel);

%% 6. Compute state feedback controller
controller = dps.parabolic.control.backstepping.ppide_controller(controllerKernel, ppide);

%% 7. Create approximation for ppide simulation
ppideApproximation = dps.parabolic.system.ppide_approximation(ppide);

%% 8. Simulate system

% \dot{x} = f(x)
f = @(t,x) ppideApproximation.A*x + ppideApproximation.Br*controller.r*x;
x0Sim = ones(size(ppideApproximation.A,1),1);

[t, y] = ode15s(f,[0 5], x0Sim);

%% 9. plot result:
figure('Name','Simulation result')
surf(t,zdiscPlant,y.');
set(gca,'YDir','Reverse')
xlabel('t')
ylabel('z')

%% alternative
misc.mesh_sparse(zdiscPlant,t,y,21,11,51,51)





