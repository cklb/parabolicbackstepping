% SimScript TAC
% sim2.controllerKernelFlag = 0;
% 1. Simulate Open-Loop:
% sim_pars.tend = 1;
% sim2pars = {...
% 		'x0',x0,...
% 		'simPars',sim_pars,...
% 		'plotPars',plot_pars,...
% 		'useReferenceObserver',0,...
% 		'useDisturbanceObserver',0,...
% 		'useStateObserver',0,...
% 		'useStateFeedback',0,...
% 		'useOutputRegulation',0,...
% 		'useIntMod',0,...
% 		'name',names{1},...
% 		'path',myPath
% };
% 
% if ~exist('sim2','var')
% 	sim2 = dps.parabolic.control.backstepping.ppide_simulation(ppide,sim2pars);
% else
% 	sim2 = sim2.setPars(ppide,sim2pars);
% end
% % Normal but resampled mesh:
% if sim2.controllerKernelFlag
% 	sim2 = sim2.initializeSimulation;
% else
% 	sim2 = sim2.initializeSimulation;
% 	save(sim2.saveStr,'sim2')
% end
% sim2 = sim2.simulate;
% res_surf_t=21;
% res_surf_z=21;
% figure('Name','OpenLoop')
% idplot=1;
% t_plot{1} = sim2.t;
% y_plot{1} = sim2.x;
% for k=1:1 %length(plantlist)
% 	for i=1:n
% 		subplot(1,n,idplot)		
% 		% Option 1: normales Mesh
% 		tres = linspace(t_plot{k}(1),t_plot{k}(end),res_surf_t);
% 		zres = linspace(0,1,res_surf_z);
% 		[Z,T]=meshgrid(zdiscPlant,t_plot{k});
% 		[ZRES,TRES] = meshgrid(zres,tres);
% 		yres = interp2(Z,T,y_plot{k}(:,(i-1)*ndiscPlant+1:i*ndiscPlant),ZRES,TRES);
% % 		[T0,Y0] = meshgrid([0 1 1 0],[0 0 1 1]);
% % 		hold on
% 		surf(TRES,ZRES,yres)
% % 		plot3([0 1 1 0 0],[0 0 1 1 0],[0 0 0 0 0],'k');
% % 		view(-40,30);
% 		
% 		% Option 2: mesh_sparse
% % 		mesh_sparse(t_plot{k},zdisc_plant,y_plot{k}(:,(i-1)*ndisc_plant+1:i*ndisc_plant).',lines_t,lines_z,res_surf_t,res_surf_z);
% 		
% 		hold on
% 		box on
% 		grid on
% 		set(gca,'Ydir','reverse')
% 		xlabel('$t$')
% 		ylabel('$z$')
% 		zlabel(['$x_' num2str(i) '(z,t)$'])
% 		idplot=idplot+1;
% 	end
% end



%% 2. Simulate Closed-Loop
sim2pars = {...
		'x0',x0,...
		'simPars',sim_pars,...
		'plotPars',plot_pars,...
		'useReferenceObserver',0,...
		'useDisturbanceObserver',0,...
		'useStateObserver',0,...
		'useStateFeedback',1,...
		'useOutputRegulation',0,...
		'useIntMod',0,...
		'name',names{1},...
		'path',myPath,...
		'debug',1 ...
};

if ~exist('sim2','var')
	sim2 = dps.parabolic.control.backstepping.ppide_simulation(ppide,sim2pars);
else
	sim2 = sim2.setPars(ppide,sim2pars);
end


% sim2 = sim2.storeObserverKernel();
% sim2.observer = dps.parabolic.control.backstepping.ppide_observer(...
% 					sim2.observerKernel, sim2.ppide, sim2.Gamma, sim2.sM);

% sim2=sim2.computeControllerKernel;
% sim2=sim2.storeObserverKernel;

sim2 = sim2.initializeSimulation;

% sim2 = sim2.simulate;
% sim2 = sim2.simTarget;
% sim2save = dps.parabolic.control.backstepping.ppide_simulation(sim2.ppide,sim2pars);
% varlist = {	'controllerKernel','observerKernel','controllerKernelFlag','observerKernelFlag'};
% for varIdx = 1:length(varlist)
% 	sim2save.(varlist{varIdx}) = sim2.(varlist{varIdx});
% end
% save(sim2save.saveStr,'sim2save');
% save(sim2.saveStr,'sim2');

% sim2.controllerKernel.plot('K');
% sim2 = sim2.plotTargetAndTransformed;
% res_surf_t=21;
% res_surf_z=21;
% figure('Name','ClosedLoop')
% idplot=1;
% t_plot{1} = sim2.t;
% y_plot{1} = sim2.x;
% for k=1:1 %length(plantlist)
% 	for i=1:n
% 		subplot(1,n,idplot)		
% 		% Option 1: normales Mesh
% 		tres = linspace(t_plot{k}(1),t_plot{k}(end),res_surf_t);
% 		zres = linspace(0,1,res_surf_z);
% 		[Z,T]=meshgrid(zdiscPlant,t_plot{k});
% 		[ZRES,TRES] = meshgrid(zres,tres);
% 		yres = interp2(Z,T,y_plot{k}(:,(i-1)*ndiscPlant+1:i*ndiscPlant),ZRES,TRES);
% % 		[T0,Y0] = meshgrid([0 1 1 0],[0 0 1 1]);
% % 		hold on
% 		surf(TRES,ZRES,yres)
% % 		plot3([0 1 1 0 0],[0 0 1 1 0],[0 0 0 0 0],'k');
% % 		view(-40,30);
% 		
% 		% Option 2: mesh_sparse
% % 		mesh_sparse(t_plot{k},zdisc_plant,y_plot{k}(:,(i-1)*ndisc_plant+1:i*ndisc_plant).',lines_t,lines_z,res_surf_t,res_surf_z);
% 		
% 		hold on
% 		box on
% 		grid on
% 		set(gca,'Ydir','reverse')
% 		xlabel('$t$')
% 		ylabel('$z$')
% 		zlabel(['$x_' num2str(i) '(z,t)$'])
% 		idplot=idplot+1;
% 	end
% end
% sim2 = sim2.meshObserverError;
sim2 = sim2.plotTargetAndTransformed;
% sim2 = sim2.meshState;

%%
sim2=sim2.plotU;

%%
%% Plot Norms
% ||h|| = (int_0^1 ||h||^2_Cn dz)^1/2
if 1
y = sim2.x;
y_target = sim2.xETarget;
y_transformed = sim2.xTransformed;
t = sim2.t;
t_target = sim2.tTarget;
mu_max = max(real(ew_target));
x0_target_quad_sum = 0;
x0_transformed_quad_sum = 0;
x0_target = sim2.xETarget(1,:);
for i=1:n
	x0_target_quad_sum = x0_target_quad_sum + x0_target((i-1)*ndiscTarget+1:i*ndiscTarget).^2;
end
x0_target_L2_norm = sqrt(numeric.trapz_fast(zdiscTarget,x0_target_quad_sum));
x_target_soll = exp((mu_max)*t_target);
y_quad_sum = 0;
y_target_quad_sum = 0;
y_transformed_quad_sum = 0;

lPlant = misc.translate_to_grid(ppide.l_disc,{zdiscPlant});
lTarget = misc.translate_to_grid(ppide.l_disc,{zdiscTarget});

for i=1:n
	y_quad_sum = y_quad_sum + (permute(lPlant(:,i,i,:),[4,1,2,3]).^(-1/2).*y(:,(i-1)*ndiscPlant+1:i*ndiscPlant)).^2;
	y_target_quad_sum = y_target_quad_sum + (permute(lPlant(:,i,i,:),[4,1,2,3]).^(-1/2).*y_target(:,(i-1)*ndiscTarget+1:i*ndiscTarget)).^2;
	y_transformed_quad_sum = y_transformed_quad_sum + (permute(lPlant(:,i,i,:),[4,1,2,3]).^(-1/2).*y_transformed(:,(i-1)*ndiscPlant+1:i*ndiscPlant)).^2;
end
y_2_norm_quad = (y_quad_sum);
% 	for t_idx = 1:length(t)
y_L2_norm = sqrt(numeric.trapz_fast(zdiscPlant,y_2_norm_quad));
y_target_L2_norm = sqrt(numeric.trapz_fast(zdiscPlant,y_target_quad_sum));
y_transformed_L2_norm = sqrt(numeric.trapz_fast(zdiscPlant,y_transformed_quad_sum));
% 	end
% 	y_norm_sum(t_idx) = y_norm_sum(t_idx) + sqrt(sum(y(t_idx,(i-1)*ndiscPlant+1:i*ndiscPlant).^2))/ndiscPlant;
% 	y_norm_ges0 = sqrt(sum(y_norm(1,:).^2));
figure('Name','NormClosedLoop')
hold on
box on
h1 = plot(t,y_L2_norm,'DisplayName','$\|x(t)\|$');
h2 = plot(sim2.tTransformed,y_transformed_L2_norm,'r','LineWidth',2,'DisplayName','transformiert');
h3 = plot(t_target,y_target_L2_norm,'b--','LineWidth',2,'DisplayName','target_ist');	
% 	plot(t_target,1.5*max(y_target_L2_norm)*x_target_soll,'g:','lineWidth',2,'DisplayName','target_soll')
yl = get(gca,'ylim');
set(gca,'ylim',yl);
plot(t,25*exp((mu_max)*t),'lineWidth',1.5,'Color','green')
% legend([h1 h2 h3],{'$\|x(t)\|$','$\|\T[x(t)]\|$','$\|\tilde{x}(t)\|$'});
legend([h2 h3],{'$\|\T[x(t),t]\|$','$\|\tilde{x}(t)\|$'},'interpreter','none');
xlabel('$t$')
end % if

%% Plot Time-slope of Kernel
if 0
K = sim2.controllerKernel.get_K;
figure('Name','kernel')
hold on
box on
for i=1:n
	for j=1:n
		plot(sim2.ppide.ctrl_pars.tDiscKernel,squeeze(K(misc.get_idx(0.4,zdiscKernel),misc.get_idx(0.1,zdiscKernel),i,j,:)),'DisplayName',['$K_{' num2str(i) num2str(j) '}$']);		
	end
end
legend('-dynamicLegend')
xlabel('$t$')
ylabel('$K_{ij}(0.4,0.1,t)$')
end %if

%%
% figure('Name','Regler, fixe Zeit')
% hold on 
% plot(rz(1,ndiscKernel+1:end,end))

%%
% sim2.controller = dps.parabolic.control.backstepping.ppide_controller(sim2.controllerKernel,ppide,1);

%%
% sim2.controllerKernel.plot('K','numPlots',4)

%%
sim2.controller.plot();

%% 
% sim2.controllerKernel.plot('G');
% sim2.controllerKernel.plot('H');

%%
% simTAC.controllerKernel.plot('G');
% simTAC.controllerKernel.plot('H');

%%
% sim2.controllerKernel.plot('dz_K_1_zeta')
% sim2.controllerKernel.plot('dzeta_K_z_0')

% sim2.observerKernel.plot('dz_K_1_zeta')
% sim2.observerKernel.plot('dzeta_K_z_0')

%% 
% sim2.controllerKernel = dps.parabolic.control.backstepping.check_ppde_boundaries(sim2.controllerKernel,sim2.ppide);
% sim2.controllerKernel = dps.parabolic.control.backstepping.check_pde(sim2.controllerKernel,sim2.ppide);
% sim2.controllerKernel.plot('err')
% sim2.controllerKernel.plot('errb')

%% Kern animieren �ber Zeit:
% x11 = [squeeze(K(:,:,1,1,:)).' squeeze(K(:,:,1,2,:)).' squeeze(K(:,:,2,1,:)).' squeeze(K(:,1,2,2,:)).'];
if 0
K = sim2.controllerKernel.get_K;
% K = sim2.observerKernel.get_K;
idx=0;
clearvars xAnim;
for i=1:n
	for j=1:n
		idx=idx+1;
		xAnim(:,(idx-1)*ndiscKernel+1:idx*ndiscKernel,:) = permute(K(:,:,i,j,:),[5 1 2 3 4]);
	end
end
t = sim2.ppide.ctrl_pars.tDiscKernel;
animation_time = 4;
fps = 30;
% misc.animate_state(xAnim,t,n^2,zdiscKernel,animation_time,fps,1,[],{[-40 0]})
misc.animate_state(xAnim,t,n^2,zdiscKernel,animation_time,fps);

end

%% Beobachterkern
if 0
% K = sim2.controllerKernel.get_K;
K = sim2.observerKernel.get_K;
idx=0;
clearvars xAnim;
for i=1:n
	for j=1:n
		idx=idx+1;
		xAnim(:,(idx-1)*ndiscKernel+1:idx*ndiscKernel,:) = permute(K(:,:,i,j,:),[5 1 2 3 4]);
	end
end
t = sim2.ppide.ctrl_pars.tDiscKernel;
animation_time = 4;
fps = 30;
% misc.animate_state(xAnim,t,n^2,zdiscKernel,animation_time,fps,1,[],{[-40 0]})
misc.animate_state(xAnim,t,n^2,zdiscKernel,animation_time,fps);

end

%% Plots exprtieren
if 0
misc.export2tikz(['/NewLib/+dps/+parabolic/+script/+TACPaperTimeDependent/plots/'],'width','0.35\linewidth','height','0.35\linewidth',...
	        'parseStrings',false,...
			'extraAxisOptions',...
			['every axis title/.append style={font=\footnotesize},',...
			'every axis label/.append style={font=\footnotesize},'...
			'every tick label/.append style={font=\scriptsize},'...
          'every x tick label/.style={},'...
			'every y tick label/.style={},'...
          'every z tick label/.style={},'...
			'yticklabel style={/pgf/number format/fixed,/pgf/number format/precision=2},'...
			],...
			'floatFormat','%.3g');%,...
	close all
end


