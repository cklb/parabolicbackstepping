
% external nochmal einbinden, falls man in NewLib arbeitet!
addpath(genpath([pwd '\' 'external']));
set(groot,'defaulttextinterpreter','none'); 
import misc.*
import numeric.*
import dps.parabolic.system.*
import dps.parabolic.control.backstepping.*


%% Pfad und Namen der Simulationen:
% Berechnungen, wie Kerne k�nnen automatisiert als 
% mat-file Abgespeichert und geladen werden:
myPath = '+dps/+parabolic/+mat/';
import misc.*
import numeric.*

% verschiedene Simulationen nacheinander ausf�hren
names{1} = 'sim1';

% wichtigsten Aufloesungen gleich hier festlegen:
ndiscKernel = 31;% Aufloesung Kern
ndiscObsKernel = 11; % Aufloesung Beobachterkern

% Vorhandene Dateien laden, falls existent:
for i=1:length(names)
	if ~exist(names{i},'var')
		srchstr = [myPath... % myPath
			'*'...                    % new date
			names{i}...                 % new name
			'_nc'...
			num2str(ndiscKernel)... % ndiscCtrl
			'_noK'... % rest unchanged
			num2str(ndiscObsKernel)...
			'.mat'];
		fls = dir(srchstr);
		if ~isempty(fls)
			load([myPath fls(end).name]);
		end
	end
end


%%
% Simulationskonfiguration
sim_pars = misc.simulation_parameters('frel',1e-6,...
								 'fabs',1e-6,... 
								 'tsmax',2e-2); % 8
% Ploteinstellungen
plot_pars.plot_res_1D = 50;
plot_pars.plot_res_2D_lines = 150;
plot_pars.plot_res_2D_num_lines = 20;
							
% Sukzessive Approximation:
itmax = 16; % max. Iterationen
itmin = 1;  % min. Iterationen
tol = 1e-3; % Abbruchkriterium sukzessive Approximation
min_xi_diff = 0.1; % numerischer Parameter, f�r numerische Effizienz
min_eta_diff = 0.1;
min_xi_diff_border = 0.05;
min_eta_diff_border = 0.05;


ndiscPars = 101; % Aufloesung gespeicherte Parameter
ndiscRegEq = 5; % Aufloesung Regulator equations
ndiscPlant = 101;% Aufl�sung Streckenapproximation
ndiscObs = 11; % Aufloesung Beobachterrealisierung
ndiscTarget = ndiscPlant; % Aufloesung Zielsystem (f�r Analysezwecke)
 
% Strecke festlegen:
n=2; % Anzahl Zust�nde

% Streckenparameter

if n==1
	lambda = @(z) 1+0*z; 
	lambda_diff = @(z) 0*z; 
	C = @(z) 0+0*z;
	a = @(z) 1+0*z;
	a0 = @(z) 0+0*z;
	F = @(z,zeta) 0*z+0*zeta;
	x0 = @(z) dps.parabolic.tool.get_x0_ppides(z,1);
	c_op = dps.output_operator('c_m',1,'z_m',0.5,'c_0',0);
elseif n==2
	lambda = @(z) [3/2+z.^2.*cos(2*pi*z), 0;0, 1/2-1/4*sin(2*pi*z)];
% 	lambda = @(z) [2 0;0 1];
	lambda_diff = @(z) [2*z.*cos(2*pi*z)-z.^2.*sin(2*pi*z)*2*pi,0;0, -1/4*cos(2*pi*z)*2*pi];
% 	lambda_diff = @(z) [0,0;0, 0];
	C = @(z) 0*[1 0; 0 1]; % Konvektionsmatrix
	a = @(z) 1*[1 (1+z); (1/2+z) 1]; 
	a0 = @(z) 0*[1*z 1-z; z 1-z]; 
	F = @(z,zeta) 0*[exp(z+zeta) exp(z-zeta);1-exp(-(z-zeta)) exp(-(z+zeta))];
	x0 = @(z) [dps.parabolic.tool.get_x0_ppides(z,1);dps.parabolic.tool.get_x0_ppides(z,1)];
else 
	error(['n = ' num2str(n) ' not parametrized']);
end
% Messung
Cm_op = dps.output_operator('c_0',eye(n));

% Rechte Randbedingung festlegen:
Bdr = 0*eye(n,n);
Bnr = eye(n,n);
b_op2 = dps.parabolic.system.boundary_operator('z_b',1,...
						  'b_d',Bdr,...
						  'b_n',Bnr);

% linken Rand-Operator definieren:
numDir = 2; % number of Dirichlet BCs
Bd = -1*diag((n-numDir):-1:1);
% Bd = eye(n);
% Bd = eye(n);
b_op1 = dps.parabolic.system.boundary_operator('z_b',0,...
						  'b_d',misc.diagZeroFill(eye(numDir),Bd),... %Dirichlet
						  'b_n',misc.diagZeroFill(zeros(numDir,numDir),eye(n-numDir))); %Neumann
					  
% Regelung konfigurieren:
mu_mat = 5*eye(n);
mu_o_mat = 2*mu_mat;

% Freiheitsgrad zu 0 festlegen.
for i=1:n
	for j=1:n
		g_strich{i,j} = @(eta) 0+0*eta; % Freiheitsgrad, anonyme Matrixfunktion mit ausschlie�lich Elementen unter der Hauptdiagonalen
	end
end


% Ortsvektoren festlegen
zdiscKernel = linspace(0,1,ndiscKernel);
zdiscRegEq = linspace(0,1,ndiscRegEq);
zdiscPlant = linspace(0,1,ndiscPlant);
zdiscTarget = linspace(0,1,ndiscTarget);
zdisc_obs_kernel = linspace(0,1,ndiscObsKernel);
zdiscObs = linspace(0,1,ndiscObs);
zdiscPars = linspace(0,1,ndiscPars);
	  
% ppide als Objekt erzeugen:
ppide = dps.parabolic.system.ppide_sys(...
				'name','Plant',...
				'l',lambda,...
				'l_diff',lambda_diff,...
				'c',C,...
				'cm_op',Cm_op,...
				'b',@(z) 0*z + zeros(n,n),...  
				'a', a,...
				'b_op1',b_op1,...
				'b_op2',b_op2,...
				'b_1',eye(n,n),...
				'b_2',eye(n,n),... % ein Eingang f�r jeden Zustand
				'ndisc',ndiscPlant,...
				'ndiscPars',ndiscPars,...
				'f',F,...
				'a_0',a0,...
				'ctrl_pars',dps.parabolic.control.ppde_controller_pars('eliminate_convection',0,...
												 'mu',mu_mat,...
												 'mu_o',mu_o_mat,...
												 'zdiscCtrl',zdiscKernel,...
												 'zdiscRegEq',zdiscRegEq,...
												 'zdiscObsKernel',zdisc_obs_kernel,...
												 'zdiscObs',zdiscObs,...
												 'zdiscTarget',zdiscTarget,...
												 'it_max',itmax,...
												 'it_min',itmin,...
												 'tol',tol,...
												 'g_strich',g_strich,...
												 'min_xi_diff',min_xi_diff,...
												 'min_eta_diff',min_eta_diff));			

											 
% Eigenwerte untersuchen und langsamsten Eigenwert anzeigen:
ppide_appr = dps.parabolic.system.ppide_approximation(ppide);
A = ppide_appr.A;											 
meig = max(eig(A));
% noch verbessern:
if meig>0
	disp(['die Strecke ist instabil: ' num2str(meig)])
else
	disp(['die Strecke ist stabil: ' num2str(meig)])
end

% eigenwerte Zielsystem pr�fen, um sicherzugehen, dass die Wahl von mu
% passt:
ew_target = ppide.eigTarget(ppide.ctrl_pars.mu);
ew_obs = ppide.eigTarget(ppide.ctrl_pars.mu_o);

if max(real(ew_target)) > 0
	error(['Largest eigenvalue of the target system: ' num2str(max(real(ew_target))) '!'])
end
if max(real(ew_obs)) > 0
	error(['Largest eigenvalue of the observer target system: ' num2str(max(real(ew_obs))) '!'])
end

fprintf(['Positivster Eigenwert des Zielsystems (ohne mu) = ' num2str(max(ppide.eigTarget(zeros(n,n)))) '\n']);

%<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
% ENDE EINGABEN

%% Simulate
sim_pars.tend = 1;

sim1pars = {...
		'x0',x0,...
		'simPars',sim_pars,...
		'plotPars',plot_pars,...
		'useReferenceObserver',0,...
		'useDisturbanceObserver',0,...
		'useStateObserver',0,...
		'useStateFeedback',1,...
		'useOutputRegulation',0,...
		'useIntMod',0,...
		'name',names{1},...
		'path',myPath
};


if ~exist('sim1','var')
	if ~exist('sim2','var')
		sim1 = dps.parabolic.control.backstepping.ppide_simulation(ppide,sim1pars);
	else
		sim1 = sim2; % Kerne etc. wurden vllt schon berechnet und koennen uebernommen werden!
		sim1 = sim1.setPars(ppide,sim1pars);
	end
else
	sim1 = sim1.setPars(ppide,sim1pars);
end
% In dieser Funktion werden alle f�r die Simulation n�tigen Gr��en berechnet.
sim1 = sim1.initializeSimulation;
if ~sim1.controllerKernelFlag
	sim1 = sim1.computeControllerKernel();
% 	save(sim1.saveStr,'sim1');
end
if ~sim1.simulated
	sim1 = sim1.simulate;
% 	save(sim1.saveStr,'sim1');	
end

% Ergebnis darstellen
sim1.meshState
sim1.controllerKernel.plot('K');
sim1.plotTargetAndTransformed();
% save(sim1.saveStr,'sim1');

% sim1 = sim1.plotOutput();

%%
%sim1.plotTargetAndTransformed
sim1.controllerKernel = dps.parabolic.control.backstepping.check_ppde_boundaries(sim1.controllerKernel,ppide);
sim1.controllerKernel.plot('errb')












