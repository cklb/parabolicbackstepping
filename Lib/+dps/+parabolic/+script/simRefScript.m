sim_pars.tend = 1;

sim1pars = {...
		'x0',x0,...
		'simPars',sim_pars,...
		'plotPars',plot_pars,...
		'useReferenceObserver',0,...
		'useDisturbanceObserver',0,...
		'useStateObserver',0,...
		'useStateFeedback',1,...
		'useOutputRegulation',0,...
		'useIntMod',0,...
		'name',names{1},...
		'path',myPath
};


if ~exist('sim1','var')
	if ~exist('sim2','var')
		sim1 = dps.parabolic.control.backstepping.ppide_simulation(ppide,sim1pars);
	else
		sim1 = sim2; % Kerne etc. wurden vllt schon berechnet und koennen uebernommen werden!
		sim1 = sim1.setPars(ppide,sim1pars);
	end
else
	sim1 = sim1.setPars(ppide,sim1pars);
end

%%
%%
ppidek = dps.parabolic.control.backstepping.ppide_kernel(ppide,ppide.ctrl_pars.zdiscCtrl);
%%
use_F_handle = @(ppidek) dps.parabolic.control.backstepping.use_F_ppides(ppidek,ppide);

%%
sim1.controllerKernel = dps.parabolic.tool.fixpoint_iteration(ppidek,use_F_handle,ppide2.ctrl_pars.it_min,ppide2.ctrl_pars.it_max,ppide2.ctrl_pars.tol);
%%
sim1.controllerKernel.transformed = 0;
sim1.controllerKernel = sim1.controllerKernel.transform_kernel(ppide);
sim1.controllerKernelFlag = 1;

%%
% TODO: HIER STEHEN GEBLIEBEN. REGLERBERECHNUNG FUER ZEITABHAENGIGE REGLER.
% AU�erdem pr�fen: wo wird numerishce Ableitung des Kerns gebildet? analytische erst mal
% kimplett ausgeschaltet!
sim1.controller = dps.parabolic.control.backstepping.ppide_controller(sim1.controllerKernel,ppide,1);
% falls keine anlytishe Ableitung verwendet wird:
% [~, sim1.controller.r , sim1.controller.rAna] = sim1.controller.compute_ppide_controller(sim1.controller.kernel,ppide,1);
sim1.controllerFlag =1;


%%
sim1.simulated = 0;
sim1 = sim1.initializeSimulation;
if ~sim1.controllerKernelFlag
	sim1 = sim1.computeControllerKernel();
% 	save(sim1.saveStr,'sim1');
end
if ~sim1.observerKernelFlag
	sim1 = sim1.storeObserverKernel();
% 	save(sim1.saveStr,'sim1');	
end
if ~sim1.simulated
	sim1 = sim1.simulate;
	save(sim1.saveStr,'sim1');	
end
sim1.meshState
sim1.controllerKernel.plot('K')
%%
sim1.simulatedTarget = 0;
sim1.plotTargetAndTransformed