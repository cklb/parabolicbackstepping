% sim quasiStatic


sim3 = sim2;

%%
sim3.name = 'sim3';
sim3.ppide.ctrl_pars.quasiStatic = 1;
ppidek3 = dps.parabolic.control.backstepping.ppide_kernel(sim3.ppide,ppide.ctrl_pars.zdiscCtrl);
%%
use_F_handle3 = @(ppidek) dps.parabolic.control.backstepping.use_F_ppides(ppidek,sim3.ppide);

%%
sim3.controllerKernel = dps.parabolic.tool.fixpoint_iteration(ppidek3,use_F_handle3,ppide2.ctrl_pars.it_min,ppide2.ctrl_pars.it_max,ppide2.ctrl_pars.tol);
%%
sim3.controllerKernel.transformed = 0;
sim3.controllerKernel = sim3.controllerKernel.transform_kernel(sim3.ppide);
sim3.controllerKernelFlag = 1;
%%
% noch nicht zeitabhaengig implementiert!
%sim2.controllerKernel.plot('G')

sim3.controllerKernel.checked_PDE = 0;
sim3.controllerKernel = dps.parabolic.control.backstepping.check_pde(sim3.controllerKernel,sim3.ppide);
sim3.controllerKernel.plot('err');


%%
sim3.controllerKernel.plot('K')
%%
sim3.controller = dps.parabolic.control.backstepping.ppide_controller(sim3.controllerKernel,sim3.ppide,1);
sim3.controllerFlag =1;


%%
sim3.simulated  =0;
sim3 = sim3.simulate;
sim3.meshState

%%
sim3.simulatedTarget = 0;
sim3.plotTargetAndTransformed
%%
save(sim3.saveStr,'sim3');	

% ppidek3 = dps.parabolic.control.backstepping.ppide_kernel(sim3.ppide,ppide.ctrl_pars.zdiscCtrl);
% use_F_handle = @(ppidek) dps.parabolic.control.backstepping.use_F_ppides(ppidek3,sim3.ppide);
% sim3.controllerKernel = dps.parabolic.tool.fixpoint_iteration(ppidek3,use_F_handle,ppide2.ctrl_pars.it_min,ppide2.ctrl_pars.it_max,ppide2.ctrl_pars.tol);