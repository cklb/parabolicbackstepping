function [k, k_its, avgTime] = fixpoint_iteration(G_0,F_G,it_min,it_max,tol)   
        
% k = FIXPOINT_ITERATION(...)
%       solves an integral equation by using fixpoint iteration.
%
%       k = FIXPOINT_ITERATION(G_0,F_G,it_min,it_max,tol)
%       uses at least IT_MIN iterations, at most IT_MAX iterations but
%       stops if the maximum-norm of the difference between two iterations
%       drops below TOL
%
%       k = FIXPOINT_ITERATION(G_0,F_G,num_iterations)
%       uses a fixed numer of NUM_ITERATIONS iteration numbers.%
%
%       k = FIXPOINT_ITERATION(...) in general solves an integral equation
%       using fixpoint iteration.
%       There, dG_i+1 = F[ dG_i ] 
%       and the final approximation of the solution is 
%       sum_i=0^number ( dGi )
%
%		[~,k_its] returns the result of each iteration step separately
%
%       INPUT PARAMETERS:
%     MATRIX      G_0          : Initial value for the successive
%                                 approximation
%     FUNCTION    F_G          : computing the following iteration step 
%                                 for G based on the current value of G
%
%       For iteration control: pass EITHER
%     SCALAR       number      : fixed number of iteration steps
%       OR:
%     SCALAR       it_min      : minimum number of iteration steps
%     SCALAR       it_max      : maximum number of iteration steps
%     SCALAR       tol         : tolerance for maximum norm of the error
%                                between 2 iterations
%     BOOLEAN      saveMemory  : set to 1 to ensure k

%
%       OUTPUT PARAMETERS:
%     MATRIX       k           : solution for the successive approximation
%     CELL-ARRAY   k_its       : solution of each iteration step

%
% benötigte Unterprogramme:
%   - trapz_fast
%
% history:
% created  on 12.09.2016 by Simon Kerschbaum


%%%%%%%%%%%%%%%%%%%%%%
%% Input Validation %%
%%%%%%%%%%%%%%%%%%%%%%
if it_max < it_min
	error('iterations_max must be greater or equal to iterations_min!')
end

if ~isa(F_G,'function_handle')
    error('F_G must be a function with one argument.')
end

%%%%%%%%%%%%%%%%%%%%%%%%
%% Fixed-point Iteration %%
%%%%%%%%%%%%%%%%%%%%%%%%
formdate='dd.mm. HH:MM:SS';
fprintf(['  Starting fixed-point iteration [' datestr(now,formdate) ']...\n']);
% stoploop i helpful in developing process but the window always steals focus so its disabled
% in final version.
% FS = stoploop({'Fixpunktiteration abbrechen'});
% cleanup = onCleanup( @()( delete( FS.waitbar ) ) );
delta_G = cell(it_max,1);
G = cell(it_max,1);
delta_G{1} = G_0;
delta_GFlat = G_0;
G{1} = delta_G{1};
GFlat = G{1};
reverseStr = '';
msg2='';
it=1; %iteration
last_error=inf;
saveMemory = 0;
if nargout < 2
	saveMemory = 1;
end
timeSum = 0;
while it<=it_max %&& ~FS.Stop()
	if (last_error<tol) && (it>it_min)
		last_iteration = it-1;
		breakmsg = sprintf('\n    -deviation<tolerance: %0.2e<%0.2e, stop after %u iterations.\n',last_error,tol,it-1);
		break;
	end
	msg1=sprintf('    -iteration %u...',it);
	fprintf([reverseStr msg1 msg2])
	tic
	if saveMemory
		delta_GFlat = F_G(delta_GFlat);
	else
		delta_G{it+1} = F_G(delta_G{it}); 
	end
    time = toc;
	if saveMemory
		GFlat = GFlat + delta_GFlat;
% 		figure('Name','K33'), surf(squeeze(GFlat(:,:,3,3,20)).'),xlabel('z'),ylabel('zeta')
		if isnumeric(delta_GFlat)
			last_error = max(abs(delta_GFlat(:)));
		else % Falls delta_G Objekt ist
			last_error = max(max(abs(delta_GFlat)));
		end
	else
		G{it+1} = G{it} + delta_G{it+1};
		if isnumeric(delta_G{it+1})
			last_error = max(abs(delta_G{it+1}(:)));
		else % Falls delta_G Objekt ist
			last_error = max(max(abs(delta_G{it+1})));
		end
	end
	
	msg = [msg1 msg2];
	breakmsg = sprintf('\n    -n=n_max: deviation= %0.2e, stop after %u iterations.\n',last_error,it);
	reverseStr = repmat(sprintf('\b'), 1, length(msg));
	msg2=sprintf('deviation: %0.2e (%0.2fs)',last_error,time);
	last_iteration=it;
	it=it+1;
	timeSum = timeSum+time;
% 	waitbar(it/it_max,FS.waitbar,'Close to abort fixed-point iteration!');
end
avgTime = timeSum/last_iteration;
% if FS.Stop()
% 	breakmsg=sprintf('\n  - iteration aborted after %u iterations, deviation= %0.2e.\n',it,last_error);
% end
if last_error > tol
	warning('Fixpoint iteration was stopped, eventhough the error is greater than the tolerance');
end
fprintf(breakmsg);
% FS.Clear();
% clear FS
if saveMemory
	k = GFlat;
else	
	k = G{last_iteration+1};
	k_its = G(1:last_iteration);
end
fprintf(['  Finished fixed-point iteration! [' datestr(now,formdate) ']\n']);
% userView = memory;
% disp(['Memory used: ' num2str(round(userView.MemUsedMATLAB/1e6)) 'MB'])

% dz_k = dz_G; % dz_g muss vorher berechnet werden, falls benötigt

end