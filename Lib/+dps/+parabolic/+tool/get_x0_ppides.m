function x0 = get_x0_ppides(z, x_mid )
%GET_X0_PPIDES compute initial conditions for parabolic system
%   x0 = get_x0_ppides(z,x_mid) calculates a function x0(z) which fulfills
%    x(0)=x_z(0)=x(1)=x_z(1) = 0
%    x(1/2) = x_mid
%   and thus is a suitable initial condition for all parabolic systems.

% history:
% created on 19.02.2018 by Simon Kerschbaum

x0 = x_mid*(3/4*sin(pi*z+2*pi)+1/4*cos(3*pi*z+pi/2));

end

