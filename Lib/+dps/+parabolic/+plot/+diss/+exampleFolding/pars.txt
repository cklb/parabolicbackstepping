\begin{align*}
\Lambda(z) &= \begin{bmatrix*} z^2+2 & 0\\ 
 0 & {\mathrm{e}}^{-z}+\frac{1}{2} \end{bmatrix*}\\ 
 A(z,t) &= @(z,t)1*[1.5*a11Interp.evaluate(t),2+cos((z+t)*2*pi);(2+sin((z+t)*2*pi)),a11Interp.evaluate(t);]\\ 
 B_{0}^n &= \begin{bmatrix*} 1.0 & 0\\ 
 0 & 1.0 \end{bmatrix*}\\ 
 B_{0}^d(t) &= \begin{bmatrix*} 0 & 0\\ 
 0 & 0 \end{bmatrix*}\\ 
 B_{1}^n &= \begin{bmatrix*} 1.0 & 0\\ 
 0 & 1.0 \end{bmatrix*}\\ 
 B_{1}^d(t) &= \begin{bmatrix*} 0 & 0\\ 
 0 & 0 \end{bmatrix*}\\ 
 tol &= 1.0\,{10}^{-5}\\ 
 numXi &= 80.0\\ 
 ndiscObs &= 11.0\\ 
 ndiscTime &= 31.0\\ 
 ndiscKernel &= 31.0\\ 
 ------------- &= -----------\\ 
 Name &= Unilateral\\ 
 mu &= 8.0\\ 
 IterationsController &= 16.0\\ 
 IterationsObserver &= --\\ 
 foldingPointController &= 0.3\\ 
 foldingPointObserver &= 0.7\\ 
 Name &= Folding 0.2\\ 
 mu &= 13.0\\ 
 IterationsController &= 15.0\\ 
 IterationsObserver &= --\\ 
 foldingPointController &= 0.2\\ 
 foldingPointObserver &= 0.7\\ 
 Name &= Folding 0.375\\ 
 mu &= 13.0\\ 
 IterationsController &= 13.0\\ 
 IterationsObserver &= --\\ 
 foldingPointController &= 0.375\\ 
 foldingPointObserver &= 0.7\\ 
 Name &= Folding 0.85\\ 
 mu &= 13.0\\ 
 IterationsController &= 14.0\\ 
 IterationsObserver &= --\\ 
 foldingPointController &= 0.85\\ 
 foldingPointObserver &= 0.7\\ 
 Name &= Folding with Observer\\ 
 mu &= 13.0\\ 
 IterationsController &= 13.0\\ 
 IterationsObserver &= 15/11\\ 
 foldingPointController &= 0.375\\ 
 foldingPointObserver &= 0.7\\ 
 Name &= New observer measurement\\ 
 mu &= 13.0\\ 
 IterationsController &= 13.0\\ 
 IterationsObserver &= 15/11\\ 
 foldingPointController &= 0.375\\ 
 foldingPointObserver &= 0.3
\end{align*}