function delta_kernel = use_F_ppidesFolding(kernel,ppide)
% USE_F_PPIDES compute the next step of the fixpoint iteration for systems
% of parabolic pides
%   
%   delta_G = USE_F_PPIDES(kernel,ppide)
%	Compute the next step of the fixpoint iteration
%      delta_G{i+1}(xi,eta) = F[G{i}](xi,eta).
%
%      INPUT ARGUMENTS
%    PPIDE_KERNEL  kernel   : each element of kernel.value is an object of the class
%	                         ppide_kernel_element, which represents the
%	                         corresponding element in the respective
%	                         iteration step.
%    PPIDE_SYS     ppide    : the system of parabolic equations for which
%                            the kernel equations are solved.
%
%      OUTPUT ARGUMENTS
%    PPIDE_KERNEL  delta_kernel  : result of the usage of F on G

% history:
% created on 30.07.2019 by Simon Kerschbaum starting from a copy of use_F_ppides


import misc.*
import numeric.*

n = ppide.n;
delta_GH = cell(n,n);
GH = kernel.value;
fast=1; % Set external in get_idx to 1 to be faster!

reverseStr = '';
msg2='';
msg1 = sprintf('');
fprintf(msg1);
run=0;
zdisc = kernel.zdisc;
ndisc = length(zdisc);
numtKernel = size(kernel.value{1,1}.G,3);

%%% Interpolate system parameters to kernel resolution
pars.lambda_disc = ppide.l_disc;  %translate_to_grid(,zdisc);
pars.A_disc = ppide.a_disc; %translate_to_grid(,zdisc);

% all time-dependent coefficients need to be resampled in time-direction
varlist = {'lambda_disc','A_disc'};

tDisc = ppide.ctrl_pars.tDiscKernel;
timeDependent=0;
for i=1:length(varlist)
	if size(pars.(varlist{i}),4)>1
		pars.(varlist{i}) = permute(misc.translate_to_grid(permute(pars.(varlist{i}),[4 1 2 3]),tDisc),[2 3 4 1]);
		timeDependent=1;
	end
end
if timeDependent
	tDiscKernel = tDisc;
else
	tDiscKernel = 1;
end


% if ndisc~= n
% 	if isa(ppide.ctrl_pars.mu,'function_handle') % function
% 		if nargin(ppide.ctrl_pars.mu)==1
% 			muHelp = eval_pointwise(ppide.ctrl_pars.mu,zdisc);
% 		else
% 			muHelp = zeros(ndisc,n,n,length(tDisc));
% 			for z_idx=1:ndisc
% 				muHelp(z_idx,:,:,:) = permute((eval_pointwise(@(z)ppide.ctrl_pars.mu(zdisc(z_idx),z),tDisc)),[2 3 1]);
% 			end
% 		end
% 	elseif size(ppide.ctrl_pars.mu,1) > length(ppide.ctrl_pars.zdiscCtrl) % is discretized
% 		if length(size(ppide.ctrl_pars.mu))>3
% 			muInterpolant = interpolant({linspace(0,1,size(ppide.ctrl_pars.mu,1)),1:n,1:n,tDisc}, ppide.ctrl_pars.mu);
% 			muHelp = muInterpolant.evaluate(zdisc,1:n,1:n,tDisc);
% 		else
% 			muInterpolant = interpolant({linspace(0,1,size(ppide.ctrl_pars.mu,1)),1:n,1:n}, ppide.ctrl_pars.mu);
% 			muHelp = muInterpolant.evaluate(zdisc,1:n,1:n);
% 		end
% 		%muHelp = ppide.ctrl_pars.mu;
% 	elseif isequal(size(ppide.ctrl_pars.mu,1),n) % constant matrix, two dimensions
% 		muHelp(1:ndisc,:,:) = repmat(shiftdim(ppide.ctrl_pars.mu,-1),ndisc,1,1);
% 	else % discretized matrix
% 		error('dimensions of mu not correct!')
% 	end
% else
% 	error('ndisc=n is a very bad choice!')
% end
% mu = muHelp;

% preallocate
G = cell(n,n,n,n);
J = cell(n,n,n,n);
H = cell(n,n,n,n);
Giksum = cell(n,n);

% store interpolants of A for integrations in different coordinate systems
% AInterp = numeric.interpolant({linspace(0,1,ppide.ndiscPars),1:n,1:n,1:size(pars.A_disc,4)},pars.A_disc);
% LambdaInterp = numeric.interpolant({linspace(0,1,ppide.ndiscPars),1:n,1:n,1:size(pars.lambda_disc,4)},pars.lambda_disc);
% muInterp = numeric.interpolant({zdisc,1:n,1:n,linspace(0,1,size(mu,4))},mu);
% Evaluate Interpolant to get A on each k,l grid
for i = 1:n
	for j=1:n
		GInterp = numeric.interpolant({kernel.value{i,j}.xi,kernel.value{i,j}.eta,1:numtKernel},kernel.value{i,j}.G);
% 		JInterp = numeric.interpolant({kernel.value{i,j}.xi,kernel.value{i,j}.eta,1:numtKernel},kernel.value{i,j}.J);
% 		HInterp = numeric.interpolant({kernel.value{i,j}.xi,kernel.value{i,j}.eta,1:numtKernel},kernel.value{i,j}.H);
		for k=1:n			
			% store interpolations of J
% 			xiMatXi = kernel.xiMatXi{i,k,i,j}; % xi_ij(xi_ik,eta_ik)
% 			etaMatXi = kernel.etaMatXi{i,k,i,j}; % eta_ij(xi_ik,eta_ik)
% 			JAll = misc.mydiag(JInterp.evaluate(xiMatXi(:),etaMatXi(:),1:numtKernel));
% 			JAll = JInterp.eval(repmat(xiMatXi(:),numtKernel,1),repmat(etaMatXi(:),numtKernel,1),...
% 					reshape(repmat(1:numtKernel,numel(xiMatXi),1),[numtKernel*numel(xiMatXi),1]));
			% J_ij(xi_ik,eta_ik)
% 			J{i,j,i,k} = reshape(JAll,size(xiMatXi,1),size(xiMatXi,2),numtKernel);
			
			% store interpolations of H
% 			HAll = misc.mydiag(HInterp.evaluate(xiMatXi(:),etaMatXi(:),1:numtKernel),1:numel(xiMatXi),1:numel(xiMatXi));
% 			HAll = HInterp.eval(repmat(xiMatXi(:),numtKernel,1),repmat(etaMatXi(:),numtKernel,1),...
% 					reshape(repmat(1:numtKernel,numel(xiMatXi),1),[numtKernel*numel(xiMatXi),1]));
			% H_ij(xi_ik,eta_ik)
% 			H{i,j,i,k} = reshape(HAll,size(xiMatXi,1),size(xiMatXi,2),numtKernel);
			for l=1:n				
				% store interpolations of G
				xiMatXi = kernel.xiMatXi{k,l,i,j}; % xi_ij(xi_kl,eta_kl)
				etaMatXi = kernel.etaMatXi{k,l,i,j}; % eta_ij(xi_kl,eta_kl)
				% Important! Do not evaluate Interpolant with a grid! This creates far too many
				% data points!
% 				GAll = misc.mydiag(GInterp.evaluate(xiMatXi(:),etaMatXi(:),1:numtKernel),1:numel(xiMatXi),1:numel(xiMatXi));
% 				GAll = zeros(numel(xiMatXi),numtKernel);
% 				for tIdx=1:numtKernel
% 					GAll(:,tIdx) = GInterp.eval(xiMatXi(:),etaMatXi(:),repmat(tIdx,numel(xiMatXi),1));
% 				end
				GAll = GInterp.eval(repmat(xiMatXi(:),numtKernel,1),repmat(etaMatXi(:),numtKernel,1),...
					reshape(repmat(1:numtKernel,numel(xiMatXi),1),[numtKernel*numel(xiMatXi),1]));
				% G_ij(xi_kl,eta_kl)
				G{i,j,k,l} = reshape(GAll,size(xiMatXi,1),size(xiMatXi,2),numtKernel);
			end
		end
	end
end
Azeta = kernel.Azeta;      % Azeta{i,j,k,j} = Aij(zeta(xi_kj,eta_kj))
MuZ = kernel.MuZ;		   % MuZ{i,j,i,k} =  muij(z(xi_ik,eta_ik))
lambdaZeta = kernel.lambdaZeta;  % lambdaZeta{k,i,j} = lambda_k(zeta(xi_ij,eta_ij))

pars.lambda_disc = translate_to_grid(pars.lambda_disc,zdisc);
pars.a_disc = translate_to_grid(pars.A_disc,zdisc);


% Only if lambda is time-dependent, the spatial coordinates xi eta become time-dependent. 
% Hartwig edited
numtL = size(pars.lambda_disc,4);
% if not, the kernel will be time-dependent, but as the coordinates are constant, everything
% can be calculated in one step.



% previously: check if any G or H is identicallz zero
% (as fixpoint iteration is used on basis of the differences, one of the
% two states is zero in each step!)
G_is_nonzero=0;
for i=1:n
	for j=1:n
		if any(GH{i,j}.G(:))
			G_is_nonzero=1;
		end
	end
end

eliminate_convection = ppide.ctrl_pars.eliminate_convection;
if timeDependent && eliminate_convection
	error('Time dependent coefficients only implemented without elimination of convection term!')
end

z0 = ppide.ctrl_pars.foldingPoint;
if isscalar(z0)
	z0 = z0*ones(1,n/2);
end
for lin_idx =1:n^2
% for i=1:n % all rows of G,H
% 	for j=1:n % all columns of G,H
	[i,j] = ind2sub([n n],lin_idx);
	kem = GH{lin_idx}; % abbrv.
	s = kem.s;
	delta_GH{lin_idx} = GH{lin_idx}; % overtake old values. entries G,H are overwritten later
	Gij = GH{lin_idx}.G;
	Hij = GH{lin_idx}.H; % H1
	Jij = GH{lin_idx}.J;
	lambda_jt_disc = kem.lambda_j_t;
	lambda_j_disc = kem.lambda_j;
	lambda_jt_interp = numeric.interpolant({zdisc,1:numtL},lambda_jt_disc);
	lambda_j_interp = numeric.interpolant({zdisc,1:numtL},lambda_j_disc);
	KInterp = numeric.interpolant({zdisc,zdisc,1:numtKernel},kem.K);
	KtInterp = numeric.interpolant({zdisc,zdisc,1:numtKernel},kem.K_t);
	a_i_discInterp = kem.a_iInterp;
	a_j_discInterp = kem.a_jInterp;
	
	a_sigma = -(kem.aiZXieta-kem.ajZetaXieta);
	a_delta = kem.aiZXieta+kem.ajZetaXieta;
	if numtL>1
		error('HIer ist noch etwas falsch implementiert bei zeitabh. Lambda!')
	end
	for tIdx = 1:numtL
		% QUATSCH! zMatT wird ja immerwiedr ueberschrieben!
		zMatT = kem.zMat(:,:,tIdx);
		zetaMatT = kem.zetaMat(:,:,tIdx);
	end
	a_i_disc_all = a_i_discInterp.evaluate(zMatT(:),1:numtL);
	a_j_disc_all = a_j_discInterp.evaluate(zetaMatT(:),1:numtL);
	if size(a_i_disc_all,2)==1
		a_i_disc_all = repmat(a_i_disc_all,1,numtKernel);
	end
	if size(a_j_disc_all,2)==1
		a_j_disc_all = repmat(a_j_disc_all,1,numtKernel);
	end
	nxi = size(GH{lin_idx}.xi,1);
	xiMat = kernel.xiMatXi{i,j,i,j};
	if size(xiMat,3)==1 && numtKernel > 1
		xiMat = repmat(xiMat,1,1,numtKernel);
	end
	etaMat = kernel.etaMatXi{i,j,i,j};
	if size(etaMat,3)==1 && numtKernel > 1
		etaMat = repmat(etaMat,1,1,numtKernel);
	end
	a_i_fine_eta = zeros(nxi,size(GH{lin_idx}.eta,1),numtKernel);
	a_j_fine_eta = zeros(nxi,size(GH{lin_idx}.eta,1),numtKernel);
	G_t = zeros(nxi,size(GH{lin_idx}.eta,1),numtKernel);
	G_xil_eta = zeros(1,size(GH{lin_idx}.eta,1),numtKernel);
	
	if numtL > 1
		error('Time dependent lambda not yet implemented here.')
	end
% 	for tIdx = 1:numtL
% 		if numtL == 1 % there may be time-dependency, but not in lambda!
% 			tIdxAll = 1:numtKernel;
% 			if size(mu,4) > 1
% 				tMu = 1:numtKernel;
% 			else
% 				tMu = 1;
% 			end
% 		else
% 			tIdxAll = tIdx; % there is time-dependency in lambda, everything needs to be done for each time
% 			if size(mu,4) > 1
% 				tMu = tIdx;
% 			else
% 				tMu = 1;
% 			end
% 		end
% 		
		xi_disc = GH{lin_idx}.xi(:,tIdx);
		eta_disc = GH{lin_idx}.eta(:,tIdx);
% 		etaDiscAcc = acc(eta_disc);
% 		xiDiscAcc = acc(xi_disc);
% 		z_etaeta = kem.zMatInterp{tIdx}(eta_disc(eta_disc>=0),eta_disc(eta_disc>=0));
% 		zeta_etaeta = kem.zetaMatInterp{tIdx}(eta_disc(eta_disc>=0),eta_disc(eta_disc>=0));
% 		HInterp = numeric.interpolant({xi_disc,eta_disc,1:numtKernel},Hij);
% 		
% 		etaMat(:,:,tIdxAll) = repmat(eta_disc,nxi,1,numel(tIdxAll));
% 		etaMatg0(:,:,tIdxAll) = repmat(eta_disc,nxi,1,numel(tIdxAll));
% 		xiMat(:,:,tIdxAll) = repmat(xi_disc.',1,length(eta_disc),numel(tIdxAll));
% 		
% 		% consider everything that only depends on eta fist, so this is the
% 		% outer loop
% % 		for eta_idx = 1:length(eta_disc)
% % % 			fprintf([reverseStr msg2]);
% % 			eta_ij=eta_disc(eta_idx);
% % 			etaijAcc = acc(eta_ij);
% % 
% % 			% xi_l for integration w.r.t. xi
% % 			if eta_ij >= 0 
% % 				xi_l = eta_ij;
% % 			else
% % 				xi_l = kem.xi_l(find(acc(kem.eta_l(:,tIdx)) <= etaijAcc,1),tIdx);
% % 			end
% % 			xi_lAcc = acc(xi_l);
% % 
% % 			for xi_idx = 1:nxi % now everything that depends on xi
% % 				xi_ij = xi_disc(xi_idx);
% % 				xiijAcc = acc(xi_ij);
% % 				a = (s+1)/2 *kem.phi_i(end,tIdx) - (s-1)/2*kem.phi_j(end,tIdx); % top of spatial area is 2a
% % 				eta_l = kem.eta_l(xi_idx,tIdx);
% % 				etaLAcc = acc(eta_l);
% % 				eta_s_idx_vec = find(etaDiscAcc>=etaLAcc & etaDiscAcc<=etaijAcc); 
% % 				eta_s_vec = eta_disc(eta_s_idx_vec);
% % 				xi_s_idx_vec = find(xiDiscAcc>=xi_lAcc & xiDiscAcc <= xiijAcc);
% % 				xi_s_vec = xi_disc(xi_s_idx_vec); % here it is better to provide the indices out of the loop!
% % 				if etaijAcc>=etaLAcc && etaijAcc<= min(xiijAcc,acc(2*a-xi_ij)) % only in valid spatial area
% % 					z = kem.zMat(xi_idx,eta_idx,tIdx);   % exact values
% % 					zeta = kem.zetaMat(xi_idx,eta_idx,tIdx);
% % 
% % 					G_t(xi_idx,eta_idx,tIdxAll) = lambda_jt_interp.evaluate(zeta,tIdx).*KInterp.evaluate(z,zeta,tIdxAll)...
% % 						+ lambda_j_interp.evaluate(zeta,tIdx).*KtInterp.evaluate(z,zeta,tIdxAll);
% % 
% % 			
% % 					xiEtaIdxLin = sub2ind([nxi,length(eta_disc)],xi_idx,eta_idx);
% % 					xiEtasIdxLin = sub2ind([nxi,length(eta_disc)],repmat(xi_idx,1,length(eta_s_vec)),eta_s_idx_vec);
% % 					xisEtaIdxLin = sub2ind([nxi,length(eta_disc)],xi_s_idx_vec,repmat(eta_idx,1,length(xi_s_vec)));
% % 					a_i_fine_eta(xi_idx,eta_idx,tIdxAll) = a_i_disc_all(xiEtaIdxLin,tIdxAll);
% % 					a_j_fine_eta(xi_idx,eta_idx,tIdxAll) = a_j_disc_all(xiEtaIdxLin,tIdxAll);
% % 					a_i_fine_etas = a_i_disc_all(xiEtasIdxLin,tIdxAll);
% % 					a_j_fine_etas = a_j_disc_all(xiEtasIdxLin,tIdxAll);
% % 					a_i_fine_xis = a_i_disc_all(xisEtaIdxLin,tIdxAll);
% % 					a_j_fine_xis = a_j_disc_all(xisEtaIdxLin,tIdxAll);
% % 					
% % 					Gij_xi_etas = permute(Gij(xi_idx,eta_s_idx_vec,tIdxAll),[3, 2, 1]); % argument could be eta_s_idx_vec, but logical indexing is faster
% % 					Hij_xi_etas = permute(Hij(xi_idx,eta_s_idx_vec,tIdxAll),[3, 2, 1]); % argument could be eta_s_idx_vec, but logical indexing is faster
% % 					Jij_xi_etas = permute(Jij(xi_idx,eta_s_idx_vec,tIdxAll),[3, 2, 1]); % argument could be eta_s_idx_vec, but logical indexing is faster
% % 					Hij_xis_eta = permute(Hij(xi_s_idx_vec,eta_idx,tIdxAll),[3, 1, 2]); % argument could be eta_s_idx_vec, but logical indexing is faster
% % 					Jij_xis_eta = permute(Jij(xi_s_idx_vec,eta_idx,tIdxAll),[3, 1, 2]); % argument could be eta_s_idx_vec, but logical indexing is faster
% % 					
% % 
% % 					% Dimensions are switched to used trapz_fast
% % 					Hik_sum = zeros(numtKernel,length(eta_s_vec)); %preallocate
% % 					Jik_sum = zeros(numtKernel,length(xi_s_vec)); %preallocate
% % 					Mu_sum = zeros(numtKernel,length(eta_s_vec)); %preallocate
% % 					Mu_sum_xis = zeros(numtKernel,length(xi_s_vec)); %preallocate
% % 					% z(xi,eta_s), zeta(xi,eta_s) and (xi_s,eta)
% % 					z_vec = kem.zMat(xi_idx,eta_s_idx_vec,tIdx);
% % 					z_vec_xis = kem.zMat(xi_s_idx_vec,eta_idx,tIdx);
% % 					zeta_vec = kem.zetaMat(xi_idx,eta_s_idx_vec,tIdx);
% % 					zeta_vec_xis = kem.zetaMat(xi_s_idx_vec,eta_idx,tIdx); 
% % 					zeta_idx_vec = get_idx(zeta_vec,zdisc,fast);
% % 					z_idx_vec = get_idx(z_vec,zdisc,fast);
% % 					zeta_idx_vec_xis = get_idx(zeta_vec_xis,zdisc,fast);
% % 					z_idx_vec_xis = get_idx(z_vec_xis,zdisc,fast);
% % 					if G_is_nonzero
% % 						for k=1:n % get sums
% % 							xi_ik_disc = GH{i,k}.xi(:,tIdx);
% % 							eta_ik_disc = GH{i,k}.eta(:,tIdx);
% % 							xi_kj_disc = GH{k,j}.xi(:,tIdx);
% % 							eta_kj_disc = GH{k,j}.eta(:,tIdx);
% % % 							[~,~,xi_ik_xiij_etas_vec, eta_ik_xiij_etas_vec] = GH{i,k}.get_xi(z_fine_vec,zeta_fine_vec);
% % 							% ACHTUNG: Durch interpolation von xi statt phi wird das ergebnis
% % 							% leicht ver�ndert. Ob das das Gesamtergebnis verschlechtert, sei
% % 							% abe dahingestellt. Durch Interpolation in xi ist es streng
% % 							% genommen keine lineare sondern eine verzerrte Interpolation.
% % 							% Das kann �ber lineare Indizes und vorheriges Abspeichern auch
% % 							% beschleunigt werden!
% % 							
% % 							% integration over eta
% % 							xi_ik_xiij_etas_vec = kernel.xiMatXi{i,j,i,k}(xi_idx,eta_s_idx_vec,tIdx);
% % 							eta_ik_xiij_etas_vec = kernel.etaMatXi{i,j,i,k}(xi_idx,eta_s_idx_vec,tIdx);
% % 							xi_kj_xiij_etas_vec = kernel.xiMatXi{i,j,k,j}(xi_idx,eta_s_idx_vec,tIdx);
% % 							eta_kj_xiij_etas_vec = kernel.etaMatXi{i,j,k,j}(xi_idx,eta_s_idx_vec,tIdx);
% % 							xi_ik_xiij_etas_idx_vec= get_idx(xi_ik_xiij_etas_vec,xi_ik_disc,fast);
% % 							eta_ik_xiij_etas_idx_vec = get_idx(eta_ik_xiij_etas_vec,eta_ik_disc,fast);
% % 							xi_kj_xiij_etas_idx_vec= get_idx(xi_kj_xiij_etas_vec,xi_kj_disc,fast);
% % 							eta_kj_xiij_etas_idx_vec = get_idx(eta_kj_xiij_etas_vec,eta_kj_disc,fast);
% % 							
% % 							% integration over xi
% % 							xi_ik_xis_etaij_vec = kernel.xiMatXi{i,j,i,k}(xi_s_idx_vec,eta_idx,tIdx);
% % 							eta_ik_xis_etaij_vec = kernel.etaMatXi{i,j,i,k}(xi_s_idx_vec,eta_idx,tIdx);
% % 							xi_kj_xis_etaij_vec = kernel.xiMatXi{i,j,k,j}(xi_s_idx_vec,eta_idx,tIdx);
% % 							eta_kj_xis_etaij_vec = kernel.etaMatXi{i,j,k,j}(xi_s_idx_vec,eta_idx,tIdx);
% % 							xi_ik_xis_etaij_idx_vec= get_idx(xi_ik_xis_etaij_vec,xi_ik_disc,fast);
% % 							eta_ik_xis_etaij_idx_vec = get_idx(eta_ik_xis_etaij_vec,eta_ik_disc,fast);
% % 							xi_kj_xis_etaij_idx_vec= get_idx(xi_kj_xis_etaij_vec,xi_kj_disc,fast);
% % 							eta_kj_xis_etaij_idx_vec = get_idx(eta_kj_xis_etaij_vec,eta_kj_disc,fast);
% % 														
% % 							Gikxi_ij_etas = mydiag(GH{i,k}.G(:,:,tIdxAll),xi_ik_xiij_etas_idx_vec,eta_ik_xiij_etas_idx_vec).';
% % 							Gkjxi_ij_etas = mydiag(GH{k,j}.G(:,:,tIdxAll),xi_kj_xiij_etas_idx_vec,eta_kj_xiij_etas_idx_vec).';
% % 							
% % 							Gikxi_s_etaij = mydiag(GH{i,k}.G(:,:,tIdxAll),xi_ik_xis_etaij_idx_vec,eta_ik_xis_etaij_idx_vec).';
% % 							Gkjxi_s_etaij = mydiag(GH{k,j}.G(:,:,tIdxAll),xi_kj_xis_etaij_idx_vec,eta_kj_xis_etaij_idx_vec).';
% % 				%%%%%%%%%%%%%%%
% % 				%%%% ACHTUNG: FUER ORTSABHAENGIGE A UND F NOCH NICHT AUF
% % 				%%%% INDEXINTERPOLATION UMGESTELLT!
% % 				% Kann grossen Fehler verursachen! Am besten noch mit vorab abgesoeicherten
% % 				% Interpolanten beheben. A kann vorher auf jeden beliebigen grid zeta(xi,eta)
% % 				% ausgewertet und abgespeichert werden!
% % 							Akjzeta = reshape(pars.A_disc(zeta_idx_vec,k,j,tIdxAll),[numel(zeta_idx_vec),numel(tIdxAll)]).';	
% % 							Akjzeta_xis = reshape(pars.A_disc(zeta_idx_vec_xis,k,j,tIdxAll),[numel(zeta_idx_vec_xis),numel(tIdxAll)]).';	
% % 							f = GH{k,j}.f(zeta_idx_vec,:);
% % 							f_xis = GH{k,j}.f(zeta_idx_vec_xis,:);
% % 							if numtL > 1
% % 								error('Not yet correctly implemented!')
% % 							end
% % 							Hik_sum = Hik_sum + ...
% % 											 f(:,tIdx).'/4/s.*...
% % 											 Gikxi_ij_etas.*...
% % 											 Akjzeta;
% % 							if any(isnan(Hik_sum(:)))
% % 								here=1;
% % 							end
% % 							Jik_sum = Jik_sum + ...
% % 											 f_xis(:,tIdx).'/4/s.*...
% % 											 Gikxi_s_etaij.*...
% % 											 Akjzeta_xis;
% % 							Mu_sum = Mu_sum + ...
% % 									 reshape(mu(z_idx_vec,i,k,tMu),[length(z_idx_vec),length(tMu)]).'.*GH{i,k}.varphi_j(z_idx_vec,tIdx).'./GH{i,k}.varphi_i(z_idx_vec,tIdx).'...
% % 									 .*Gkjxi_ij_etas/4/s;
% % 							Mu_sum_xis = Mu_sum_xis + ...
% % 									 reshape(mu(z_idx_vec_xis,i,k,tMu),[length(z_idx_vec_xis),length(tMu)]).'...
% % 									 .*Gkjxi_s_etaij/4/s;								 
% % 						end % Summen gebildet
% % 					end
% % 
% % 					% H is computed equal if robin or dirichlet
% % 					d = misc.mydiag(GH{lin_idx}.d,z_idx_vec,zeta_idx_vec);
% % 
% % 					if eliminate_convection
% % 						error('eliminate_convection not implemented for folding!')
% % 					else
% % % 						% Update H
% % 						delta_GH{lin_idx}.H(xi_idx,eta_idx,tIdxAll) = ... 
% % 						trapz_fast(eta_s_vec,...
% % 						Hik_sum+Mu_sum+...
% % 						 -1/4*(a_i_fine_etas.'+a_j_fine_etas.').*Hij_xi_etas ...
% % 						 -s/4*(a_i_fine_etas.'-a_j_fine_etas.').*Jij_xi_etas ...
% % 						);
% % 							
% % 						% Update J
% % 						if j<=n/2
% % 							delta_GH{lin_idx}.J(xi_idx,eta_idx,tIdxAll) = ... 
% % 							trapz_fast(xi_s_vec,...
% % 								Jik_sum+Mu_sum_xis+...
% % 								-1/4*(a_i_fine_xis.'+a_j_fine_xis.').*Hij_xis_eta ...
% % 								-s/4*(a_i_fine_xis.'-a_j_fine_xis.').*Jij_xis_eta);
% % 							if any(isnan(delta_GH{lin_idx}.J(:)))
% % 								here=1;
% % 							end
% % 						end
% % 					end
% % 				else % outside of the spatial area
% % 					% Important to write NaNs to detect the borders of the area! (if using
% % 					% misc.writeToOutside!
% % 					delta_GH{lin_idx}.G(xi_idx,eta_idx,tIdxAll) = NaN;
% % 					delta_GH{lin_idx}.H(xi_idx,eta_idx,tIdxAll) = NaN;
% % 					delta_GH{lin_idx}.J(xi_idx,eta_idx,tIdxAll) = NaN;
% % 					etaMat(xi_idx,eta_idx,tIdxAll) = NaN;
% % 					etaMatg0(xi_idx,eta_idx,tIdxAll) = NaN;
% % 					xiMat(xi_idx,eta_idx,tIdxAll) = NaN;
% % 				end
% % 				if eta_ij < 0
% % 					etaMatg0(xi_idx,eta_idx,tIdxAll) = NaN;
% % 				end
% % 			end % for xi
% % % 			fortschritt = round(((run/n^2)+eta_idx/length(eta_disc)/n^2)*100);
% % % 			reverseStr = repmat(sprintf('\b'), 1, length(msg2)-1);
% % % 			msg2 = sprintf('i=%u,j=%u: %u%%%% \n',i,j,fortschritt);
% % 		end % for eta
% 	end % for tIdx
	% everything that needs no coorindate loop:
	Giksum{i,j} = zeros(size(GH{i,j}.G));
	for k=1:n
		% compute sums
		Giksum{i,j} = Giksum{i,j} + lambdaZeta{j,i,j}./lambdaZeta{k,i,j}.*G{i,k,i,j}.*Azeta{k,j,i,j}...
			+ MuZ{i,k,i,j}.*G{k,j,i,j};
	end
	Hi = numeric.cumtrapz_fast_nDim(etaMat,...
		-1/4*a_delta.*Hij + s/4 * kem.a_ij_tilXiEta.*Gij + s/4*Giksum{i,j},2,'equal','ignoreNaN');
	
	Hii = zeros(size(Hij));
	if j<=n/2
		Hii = s/4*a_sigma.*numeric.cumtrapz_fast_nDim(etaMat,...
			Jij,2,'equal','ignoreNaN');
	end
	Hiii = zeros(size(Hij));
	if j>n/2
		Hiii = s/4*a_sigma.*numeric.cumtrapz_fast_nDim(xiMat,...
			Hij,1,'equal','ignoreNaN');
	end
	Hiiii = zeros(size(Hij));	
	
	Gi = zeros(size(Gij));
	Gii = zeros(size(Gij));
	Giii = zeros(size(Gij));
	
	if j>n/2 
		if GH{i,j-n/2}.lambda_i(1) >= GH{i,j-n/2}.lambda_j(1) && GH{i,j}.lambda_i(1) >= GH{i,j}.lambda_j(1)
			% H = int_etal^eta J_{ij-n}(xij-n(eta,eta),eta_s)d eta_s z0/(1-z0)
			% transform (eta,eta) to the j-n/2-the coordinate system after integration
			if numtL > 1
				error('Not yet correctly implemented here!')
			end
			etaMatjmn = kernel.etaMatXi{i,j-n/2,i,j-n/2}; % etaj-n(xi_j-n,eta_j-n)
			if size(etaMatjmn,3)==1 && numtKernel>1
				etaMatjmn = repmat(etaMatjmn,1,1,numtKernel);
			end
			% first integrate over all possible  values
			Jint = numeric.cumtrapz_fast_nDim(etaMatjmn,...
				GH{i,j-n/2}.J,2,'equal','ignoreNaN');
			JInterp = numeric.interpolant({GH{i,j-n/2}.xi,GH{i,j-n/2}.eta,1:numtKernel},Jint);
			% then evaluate at correct point
			% pairwise evaluation. xi(xi,eta), eta(xi,eta)
			xiMatXi = kernel.xiMatXi{i,j,i,j-n/2}; % xi_ij-n/2(xi_ij,eta_ij)
			etaMatXi = kernel.etaMatXi{i,j,i,j-n/2}; % eta_ij-n/2(xi_ij,eta_ij)
			Jijmn_all = JInterp.eval(...
				repmat(xiMatXi(:),numtKernel,1),...
				repmat(etaMatXi(:),numtKernel,1),...
				reshape(repmat(1:numtKernel,numel(xiMatXi),1),[numtKernel*numel(xiMatXi),1]));
			% reshape to correct format:
			% integral evaluated for all(xiij,etaij)
			Jijmn = reshape(Jijmn_all,size(xiMatXi,1),size(xiMatXi,2),numtKernel);
			% integration inserts NaNs because of the NaN-grid! so remove them egain before
			% next interpolation
			Jijmn = misc.writeToOutside(Jijmn);
			JijmnInterp = numeric.interpolant({xi_disc,eta_disc,1:numtKernel},Jijmn);
			JijmnEtaEta = JijmnInterp.eval(...
				repmat(eta_disc(eta_disc>=0),numtKernel,1),...
				repmat(eta_disc(eta_disc>=0),numtKernel,1),...
				reshape(repmat(1:numtKernel,numel(eta_disc(eta_disc>=0)),1),[numtKernel*numel(eta_disc(eta_disc>=0)),1]));
			Hiiii(:,eta_disc>=0,:) = 1/4*a_sigma(:,eta_disc>=0,:)*z0(j-n/2)/(1-z0(j-n/2)).*...
				reshape(JijmnEtaEta,1,length(eta_disc(eta_disc>=0)),numtKernel);
			Gi(:,eta_disc>=0,:) = repmat(reshape(JijmnEtaEta,1,length(eta_disc(eta_disc>=0)),numtKernel)...
					,numel(xi_disc),1,1)*z0(j-n/2)/(1-z0(j-n/2));
		end
		Gii = numeric.cumtrapz_fast_nDim(xiMat,Hij,1,'equal','ignoreNaN');
	else % j<=n/2
		Giii = numeric.cumtrapz_fast_nDim(etaMat,Jij,2,'equal','ignoreNaN');
	end
	delta_GH{i,j}.G = Gi + Gii + Giii;
	delta_GH{i,j}.G_t = diff_num(tDiscKernel,delta_GH{i,j}.G,3);
% 	delta_GH{lin_idx}.H = Hi+Hii+Hiii+Hiiii +...
% 		s/4*numeric.cumtrapz_fast_nDim(etaMat,delta_GH{i,j}.G_t,2,'equal','ignoreNaN');
	if ~ppide.ctrl_pars.quasiStatic
		delta_GH{lin_idx}.H = Hi+Hii+Hiii+Hiiii +...
			s/4*numeric.cumtrapz_fast_nDim(etaMat,kem.G_t,2,'equal','ignoreNaN');
	else
		delta_GH{lin_idx}.H = Hi+Hii+Hiii+Hiiii;
	end
% 		run = run+1;
end % parfor lin_idx

% continue values of the kernel into the area outside of the spatial
% domain. Is very important to get correct values at the borders!
% must be done before an interpolant of delta_GH is created.
% TODO: rewrite using writeToOutside is only possible if ensured that there are NaNs outside
% the domain

% to write the values into outside the spatial domain, NaNs are inserted first, so
% writeToOutside can be called. This is very efficient as it needs as little loops as possible
% Insert NaN into matrices! Important for easy integration.
% logical indexing, a lot faster than loops. For min_xi_diff = 0.001, it takes 2seconds
% compared to 110 seconds with the older method
varnames = {'G','H','J'};
for i=1:n
	for j=1:n
		s = GH{i,j}.s;
		a = (s+1)/2 *GH{i,j}.phi_i(end,:) - (s-1)/2*GH{i,j}.phi_j(end,:); % a Ortsbereich
		for nameIdx = 1:length(varnames)
			delta_GH{i,j}.(varnames{nameIdx})(~(acc(GH{i,j}.eta.')>=acc(GH{i,j}.eta_l) & acc(GH{i,j}.eta.') <= min(acc(GH{i,j}.xi),acc(2*a-GH{i,j}.xi))) & ones(1,1,numtKernel)) = NaN;
			delta_GH{i,j}.(varnames{nameIdx}) = misc.writeToOutside(delta_GH{i,j}.(varnames{nameIdx}));
		end
	end
end

% tic
% for i=1:n
% 	for j=1:n
% 		kem = GH{i,j};
% 		for tIdx=1:numtL
% 			if numtL == 1 % there may be time-dependency, but not in lambda!
% 				tIdxAll = 1:numtKernel;
% 			else
% 				tIdxAll = tIdx; % there is time-dependency in lambda, everything needs to be done for each time
% 			end  
% 			xi_disc = kem.xi(:,tIdx).';
% 			eta_disc = kem.eta(:,tIdx).';
% 			s = kem.s;
% 			a = (s+1)/2 *kem.phi_i(end,:) - (s-1)/2*kem.phi_j(end,:); % a Ortsbereich
% 			for xi_idx = 1:length(xi_disc)
% 				xi_ij = xi_disc(xi_idx);
% 				xiijAcc = acc(xi_ij);
% 				for eta_idx =1:length(eta_disc)
% 					eta_ij = eta_disc(eta_idx);
% 					etaijAcc = acc(eta_ij);
% 					% go from up to down:
% 					eta_inv_idx = length(eta_disc)-eta_idx+1;
% 					eta_inv_ij = eta_disc(eta_inv_idx);
% 					etaInvijAcc = acc(eta_inv_ij);
% 					% values "over" the top left or the top right border have
% 					% to be written in the outer area.
% 					if etaijAcc > min(xiijAcc,acc(2*a-xi_ij))
% 						delta_GH{i,j}.G(xi_idx,eta_idx,tIdxAll) = delta_GH{i,j}.G(xi_idx,eta_idx-1,tIdxAll);					
% 						delta_GH{i,j}.H(xi_idx,eta_idx,tIdxAll) = delta_GH{i,j}.H(xi_idx,eta_idx-1,tIdxAll);					
% 						delta_GH{i,j}.J(xi_idx,eta_idx,tIdxAll) = delta_GH{i,j}.J(xi_idx,eta_idx-1,tIdxAll);					
% 					end
% 					if etaInvijAcc<acc(kem.eta_l(xi_idx,tIdx))
% 					% values "under" the lower border are also written
% 						delta_GH{i,j}.G(xi_idx,eta_inv_idx,tIdxAll) = delta_GH{i,j}.G(xi_idx,eta_inv_idx+1,tIdxAll);				
% 						delta_GH{i,j}.H(xi_idx,eta_inv_idx,tIdxAll) = delta_GH{i,j}.H(xi_idx,eta_inv_idx+1,tIdxAll);				
% 						delta_GH{i,j}.J(xi_idx,eta_inv_idx,tIdxAll) = delta_GH{i,j}.J(xi_idx,eta_inv_idx+1,tIdxAll);				
% 					end
% 				end
% 			end
% 		end
% 	end
% end
% time = toc;
% disp(['Time2: ' num2str(time)]);

%  H needs to be calculated for each j in order to be able to implement J, thats why a new loop
%  is started. Would be avoided if the whole integral equations were inserted once more but this
%  is greater effort!
for lin_idx = 1:n^2
	[i,j] = ind2sub([n,n],lin_idx);
	kem = GH{lin_idx}; % abbrv.
	s = kem.s;	
	a_sigma = -(kem.aiZXieta-kem.ajZetaXieta);
	a_delta = kem.aiZXieta+kem.ajZetaXieta;
	
	xiMat = kernel.xiMatXi{i,j,i,j};
	if size(xiMat,3)==1 && numtKernel>1
		xiMat = repmat(xiMat,1,1,numtKernel);
	end
	eta_disc = GH{i,j}.eta;
	xi_disc = GH{i,j}.xi;
	
	Hij = GH{i,j}.H;
	Jij = GH{i,j}.J;
	Ji = zeros(size(Jij)); % H(eta,eta)
	Jii = zeros(size(Jij));  % H_{j+n}(eta,eta)
	Jiii = zeros(size(Jij)); % int F
	
	if j<=n/2
		if numtL > 1
			error('Time dependent lambda not yet implemented here!')
		end
		if s==1 && GH{i,j}.lambda_i(1) >= GH{i,j+n/2}.lambda_j(1)
			HInterp = numeric.interpolant({GH{i,j}.xi,GH{i,j}.eta,1:numtKernel},delta_GH{i,j}.H);
			Ji(:,eta_disc>=0,:) = ...
				repmat(reshape(-GH{i,j}.r{12}*...
					HInterp.eval(...
						repmat(eta_disc(eta_disc>=0),numtKernel,1),...
						repmat(eta_disc(eta_disc>=0),numtKernel,1),...
						reshape(repmat(1:numtKernel,numel(eta_disc(eta_disc>=0)),1),[numtKernel*numel(eta_disc(eta_disc>=0)),1]))...
				,1,numel(eta_disc(eta_disc>=0)),numtKernel),numel(xi_disc),1,1);
		
			xiMatXi = kernel.xiMatXi{i,j,i,j+n/2}; % xij+n(xi,eta)
			etaMatXi = kernel.etaMatXi{i,j,i,j+n/2}; % etaj+n(xi,eta)
			HijpnInterp = numeric.interpolant(...
				{GH{i,j+n/2}.xi,GH{i,j+n/2}.eta,1:numtKernel},delta_GH{i,j+n/2}.H);
			% pairwise evaluation. xi(xi,eta), eta(xi,eta)
			% 1. evaluate for all xi_ik, eta_ik that result from all xi_ij, eta_ij.
			% since the evaluation is only needed at eta,eta and eta_ij = eta_ij+n, this step
			% is obsolete and the interpolant could directly be evaluated at eta,eta!
			Hijpn_all = HijpnInterp.eval(...
				repmat(xiMatXi(:),numtKernel,1),repmat(etaMatXi(:),numtKernel,1),...
				reshape(repmat(1:numtKernel,numel(xiMatXi),1),[numtKernel*numel(xiMatXi),1]));
			% reshape to correct format:
			Hijpn = reshape(Hijpn_all,size(xiMatXi,1),size(xiMatXi,2),numtKernel);
			Hijpn = misc.writeToOutside(Hijpn);
			%dHijpn(xi,eta) where xi and eta are coordinates of ij
			% --> can be evaluated /interpolated for xi=eta.
			HijpnInterp = numeric.interpolant(...
				{GH{i,j}.xi,GH{i,j}.eta,1:numtKernel},Hijpn);
			Hijpn_eta_etaAll = ...
				HijpnInterp.eval(...
					repmat(GH{i,j}.eta(GH{i,j}.eta>=0),numtKernel,1),repmat(GH{i,j}.eta(GH{i,j}.eta>=0),numtKernel,1),...
					reshape(repmat(1:numtKernel,numel(GH{i,j}.eta(GH{i,j}.eta>=0)),1),[numtKernel*numel(GH{i,j}.eta(GH{i,j}.eta>=0)),1]));
			Hijpn_eta_eta = reshape(Hijpn_eta_etaAll,1,length(GH{i,j}.eta(GH{i,j}.eta>=0)),numtKernel);
			Jii(:,GH{i,j}.eta>=0,:) = ...
				2*GH{i,j+n/2}.s*GH{i,j}.r{13}*repmat(Hijpn_eta_eta,numel(GH{i,j}.xi),1,1);
		end
		
		Jiii = numeric.cumtrapz_fast_nDim(xiMat,...
			-a_delta/4.*Hij+s*a_sigma/4.*Jij+s/4*Giksum{i,j},1,'equal','ignoreNaN');
	end
% 	delta_GH{i,j}.J = Ji+Jii+Jiii...
% 		+ s/4*numeric.cumtrapz_fast_nDim(xiMat,delta_GH{i,j}.G_t,1,'equal','ignoreNaN');
	if ~ppide.ctrl_pars.quasiStatic
		delta_GH{i,j}.J = Ji+Jii+Jiii...
			+ s/4*numeric.cumtrapz_fast_nDim(xiMat,kem.G_t,1,'equal','ignoreNaN');
	else
		delta_GH{i,j}.J = Ji+Jii+Jiii;
	end
	
	% as the values have changed,  they need to be written into the outside area again!
	% cannot be done with writeToOutside because there are no NaNs because the values have
	% already been written to the outside
% 	GHNew(:,:,:,1) = delta_GH{i,j}.G;
% 	GHNew(:,:,:,2) = delta_GH{i,j}.H;
% 	GHNew(:,:,:,3) = delta_GH{i,j}.J;
% 	GHOut = misc.writeToOutside(GHNew);
% 	delta_GH{i,j}.G = GHOut(:,:,:,1);
% 	delta_GH{i,j}.H = GHOut(:,:,:,2);
% 	delta_GH{i,j}.J = GHOut(:,:,:,3);
end


% Insert NaN into matrices! Important for easy integration.
% logical indexing, a lot faster than loops
varnames = {'G','H','J'};
for i=1:n
	for j=1:n
		s = GH{i,j}.s;
		a = (s+1)/2 *GH{i,j}.phi_i(end,:) - (s-1)/2*GH{i,j}.phi_j(end,:); % a Ortsbereich
		% important to include 3rd dimension. Otherwise logical indexing does only affect first
		% entry of last dimension!
		for nameIdx = 1:length(varnames)
			delta_GH{i,j}.(varnames{nameIdx})(~(acc(GH{i,j}.eta.')>=acc(GH{i,j}.eta_l) & acc(GH{i,j}.eta.') <= min(acc(GH{i,j}.xi),acc(2*a-GH{i,j}.xi))) & ones(1,1,numtKernel)) = NaN;
			delta_GH{i,j}.(varnames{nameIdx}) = misc.writeToOutside(delta_GH{i,j}.(varnames{nameIdx}));
		end
	end
end

% tic
% for i=1:n
% 	for j=1:n
% 		kem = GH{i,j};
% 		for tIdx=1:numtL
% 			if numtL == 1 % there may be time-dependency, but not in lambda!
% 				tIdxAll = 1:numtKernel;
% 			else
% 				tIdxAll = tIdx; % there is time-dependency in lambda, everything needs to be done for each time
% 			end  
% 			xi_disc = kem.xi(:,tIdx).';
% 			eta_disc = kem.eta(:,tIdx).';
% 			s = kem.s;
% 			a = (s+1)/2 *kem.phi_i(end,:) - (s-1)/2*kem.phi_j(end,:); % a Ortsbereich
% 			for xi_idx = 1:length(xi_disc)
% 				xi_ij = xi_disc(xi_idx);
% 				xiijAcc = acc(xi_ij);
% 				for eta_idx =1:length(eta_disc)
% 					eta_ij = eta_disc(eta_idx);
% 					etaijAcc = acc(eta_ij);
% 					% go from up to down:
% 					eta_inv_idx = length(eta_disc)-eta_idx+1;
% 					eta_inv_ij = eta_disc(eta_inv_idx);
% 					etaInvijAcc = acc(eta_inv_ij);
% 					% values "over" the top left or the top right border have
% 					% to be written in the outer area.
% 					if etaijAcc > min(xiijAcc,acc(2*a-xi_ij))
% 						delta_GH{i,j}.G(xi_idx,eta_idx,tIdxAll) = delta_GH{i,j}.G(xi_idx,eta_idx-1,tIdxAll);					
% 						delta_GH{i,j}.H(xi_idx,eta_idx,tIdxAll) = delta_GH{i,j}.H(xi_idx,eta_idx-1,tIdxAll);					
% 						delta_GH{i,j}.J(xi_idx,eta_idx,tIdxAll) = delta_GH{i,j}.J(xi_idx,eta_idx-1,tIdxAll);					
% 					end
% 					if etaInvijAcc<acc(kem.eta_l(xi_idx,tIdx))
% 					% values "under" the lower border are also written
% 						delta_GH{i,j}.G(xi_idx,eta_inv_idx,tIdxAll) = delta_GH{i,j}.G(xi_idx,eta_inv_idx+1,tIdxAll);				
% 						delta_GH{i,j}.H(xi_idx,eta_inv_idx,tIdxAll) = delta_GH{i,j}.H(xi_idx,eta_inv_idx+1,tIdxAll);				
% 						delta_GH{i,j}.J(xi_idx,eta_inv_idx,tIdxAll) = delta_GH{i,j}.J(xi_idx,eta_inv_idx+1,tIdxAll);				
% 					end
% 				end
% 			end
% 		end
% 	end
% end
% time = toc;
% disp(['Time2: ' num2str(time)]);


msg2 = newline;
reverseStr = repmat(sprintf('\b'), 1, length(msg1)+length(msg2)-1);
fprintf(reverseStr); % alles wieder l�schen
% fprintf('\n')

tVec = linspace(0,1,numtKernel);
delta_kernel = kernel; % Datenuebernahme
delta_kernel.value = delta_GH;
% get original coordinates for G_t

for i=1:n
	for j=1:n
		if any(isnan(delta_kernel.value{i,j}.G(:)))
			here=1;
		end
		if any(isnan(delta_kernel.value{i,j}.H(:)))
			here=1;
		end
		if any(isnan(delta_kernel.value{i,j}.J(:)))
			here=1;
		end
		% transform each element. Needed in the next loop of the fixed-point iteration!
		% Attention: In the result of the fixed-point iteration, which is calculated as the sum
		% of the delta-kernels,, K and K_t are not preserved! They need to be computed again!
		delta_kernel.value{i,j}.K = delta_kernel.value{i,j}.transform_kernel_element(1);
		% Derivative in original coordinates
		delta_kernel.value{i,j}.K_t = diff_num(tVec,delta_kernel.value{i,j}.K,3);
	end
end
% delta_kernel.plot('G')
% delta_kernel.plot('H')
end



