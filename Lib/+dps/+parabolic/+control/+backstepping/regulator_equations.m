function [ pi, Kv, pi_z, pi_zz, D] = regulator_equations(system,sigMod,kernel)
%REGULATOR_EQUATIONS Solve regulator equations
%   pi = REGULATOR_EQUATIONS(system, sigMod, kernel) solves the regulator 
%       equations belonging to system system with respect to the signal
%       model sigMod
%   [pi,Kv] = REGULATOR_EQUATIONS(system, sigMod, kernel) also returns the
%       feedback gain for the signal model states u = Kv*v.
%   If system is of class ppide_sys (parabolic system), the backstepping
%   method is used, for which a ppide_kernel kernel is needed.
% 
%   INPUT PARAMETRS:
%     PPIDE_SYS      system   : plant
%     signalModel    sigMod   : signal model of the reference and
%                               disturbance
%     ppide_kernel   kernel   : kernel of the backstepping transformation
% 
%   OUTPUT PARAMETERS:
%
%     DOUBLE ARRAY    pi       : (z_idx,i,k+sum(length(jBlocks))) solution 
%                                 of the regulator equations pi_k(z). 
%                                 z_idx is the index of the spatial grid. 
%                                 The second index is the index of the 
%                                 vector and the third index represents the 
%                                 different generalized eigenvectors. 
%

% created on 13.03.2018 by Simon Kerschbaum
% TODO:
%  - Doku und verschiedene �bergabeklassen implementieren
%  - varargin erg�nzen, Variable 'silent' um Ausgabe im Command-Window zu
%    unterbinden. Au�erdem odeopt f�r den solver
% 
import misc.*
import numeric.*
import dps.parabolic.system.*
import dps.parabolic.control.backstepping.*

if ~isempty(sigMod.Sd)||~isempty(sigMod.Sr)
	opts = odeset('RelTol',1e-6,'AbsTol',1e-6,'vectorized','on');
	% opts = odeset();

	regtimer = tic;
	fprintf('Solving regulator equations: ...');
	if isa(system,'ppide_sys')
	%	1. case, ppide_sys.
	%   solve the regulator equations
	%   L(z)Pi'' - mu*Pi - A0_til(E1E1^T Pi'(0) + E2E2^T Pi(0))-Pi S + H1 = 0
	%        b_op_l[Pi}(0) = H2
	%     c_op Tc^(-1)[Pi] = pr - H4
	%   while S may be a non-diagonalizable matrix

		%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
		% 1. Get System parameters in target system representation and do all
		% necessary auxiliary stuff
		zdiscPlant = linspace(0,1,system.ndiscPars); % Aufloesung der Parameter
		ndiscPLant = system.ndiscPars; % Aufloesung der Parameter
		zdiscRegEq = system.ctrl_pars.zdiscRegEq;    % Aufloesung der Regulator Equations
		ndiscRegEq = length(zdiscRegEq);
		zdiscKernel = kernel.zdisc;                  % Aufloesung des Kerns
		ndiscKernel = length(zdiscKernel);
		n = system.n;
		m = system.c_op.m;
		nv = sigMod.nv; 
		IntPR = interp_mat(zdiscPlant,zdiscRegEq);
		IntKR = interp_mat(zdiscKernel,zdiscRegEq);
		% mu: may be a constant matrix, an anonymous matrix function or a
		% discretized matrix function!
		if ndiscRegEq ~= n
			if isa(system.ctrl_pars.mu,'function_handle') % function
				muSys = eval_pointwise(system.ctrl_pars.mu,zdiscRegEq);
			elseif isequal(size(system.ctrl_pars.mu,1),ndiscPLant) % is discretized
				muSys = zeros(ndiscRegEq,n,n);
				for i=1:n
					for j=1:n
						muSys(:,i,j) = IntPR*system.ctrl_pars.mu(:,i,j);
					end
				end
			elseif isequal(size(system.ctrl_pars.mu,1),n) % constant matrix, two dimensions
				muSys(1:ndiscRegEq,:,:) = repmat(shiftdim(system.ctrl_pars.mu,-1),ndiscRegEq,1,1);
			else % discretized matrix
				error('dimensions of mu not correct!')
			end
		else
			error('ndisc=n is a very bad choice!')
		end

	%	G1 = system.g_1; % anonymous function (z) dim: n x nd only used in
	%	discretized version
		G2 = system.g_2; % dim: n x nd % left disturbance input 
		G3 = system.g_3; % dim: n x nd % right disturbance input 
		G4 = system.g_4; % dim: m x nd % output disturbance input
		nd = size(G2,2); % length of disturbance vector
		% check number of Dirichlet/Robin BC
		numDir = 0; % number of Dirichlet BC
		nowRobin = 0; % first non-Dirichlet BC has been detected.
		for i=1:n
			if system.b_op1.b_n(i,i)==0
				if ~nowRobin
					numDir = numDir+1;
				else
					error('The BC on the left side are not sorted. First have to be Dirichlet, then Robin/Neumann!')
				end
			else
				nowRobin = 1;
			end
		end
		E1 = [eye(numDir);zeros(n-numDir,numDir)]; % selection matrix of DirBC
		E2 = [zeros(numDir,n-numDir); eye(n-numDir)]; % selectio matrix of RobBC
		T1 = [eye(n); zeros(n,n)]; % selection matrix of first solution state
		T2 = [zeros(n,n); eye(n,n)]; % selection matrix of second solution state
		Q0 = []; % get Robin matrix from BC
		mTN = system.ctrl_pars.makeTargetNeumann;
		% Q0 must come from target system BC
		if numDir<n
			if isa(system.b_op1.b_d,'function_handle')
				Q0 = zeros(n,n);
			else
				if any(system.b_op1.b_n(:)) % only if not Dirichlet!
					Q0 = ~mTN * system.b_op1.b_d(numDir+1:n,numDir+1:n);
				end
			end
		end
		% Store K and dzeta_K in Matrix for M1(z) and M2(z)
	% 	K(1:ndiscRegEq,1:ndiscRegEq,n,n) = 0;
		dzeta_K_z_0(1:ndiscRegEq,n,n)=0;


		if n>1
			lInterp = griddedInterpolant({zdiscPlant,1:n,1:n},system.l_disc);
			l_diffInterp = griddedInterpolant({zdiscPlant,1:n,1:n},system.l_diff_disc);
			a0Interp = griddedInterpolant({zdiscPlant,1:n,1:n},system.a_0_disc);
			l_disc = lInterp({zdiscRegEq,1:n,1:n});
			l_diff_disc = l_diffInterp({zdiscRegEq,1:n,1:n});
			a0_disc = a0Interp({zdiscRegEq,1:n,1:n});
		else
			l_disc = interp1(zdiscPlant,system.l_disc,zdiscRegEq(:));
			l_diff_disc = interp1(zdiscPlant,system.l_diff_disc,zdiscRegEq(:));
			a0_disc = interp1(zdiscPlant,system.a_0_disc,zdiscRegEq(:));
		end
		G1_disc = interp1(zdiscPlant,system.g_1,zdiscRegEq);

		a0_til = zeros(ndiscRegEq,n,n);
		KKern = kernel.get_K();
		if n>1
			KInterp = griddedInterpolant({zdiscKernel,zdiscKernel,1:n,1:n},KKern);
		else
			KInterp = griddedInterpolant({zdiscKernel,zdiscKernel},KKern);
		end
		if n>1
	% 		K_comp = permute(interp1(zdiscKernel,permute(interp1(zdiscKernel,KKern,zdiscRegEq),[2 1 3 4]),zdiscRegEq),[2 1 3 4]);
			K = KInterp({zdiscRegEq,zdiscRegEq,1:n,1:n});
		else
			K = KInterp({zdiscRegEq,zdiscRegEq});
		end

		for i=1:n
			for j=1:n
				dzeta_K_z_0(:,i,j) = IntKR*kernel.value{i,j}.dzeta_K_z_0;
				a0_til(:,i,j) =IntKR*kernel.a0_til(:,i,j);
			end
		end
		dzeta_K_z_0_Lambda(1:ndiscRegEq,n,n)=0;
		K_z_0_lambda_diff(1:ndiscRegEq,n,n)=0;
		K_z_0_lambda(1:ndiscRegEq,n,n)=0;
		for z_idx = 1:ndiscRegEq
			dzeta_K_z_0_Lambda(z_idx,:,:) =  squeeze(dzeta_K_z_0(z_idx,:,:))*squeeze(l_disc(1,:,:));
			K_z_0_lambda_diff(z_idx,:,:) = squeeze(K(z_idx,1,:,:))*squeeze(l_diff_disc(1,:,:));
			K_z_0_lambda(z_idx,:,:) = squeeze(K(z_idx,1,:,:))*squeeze(l_disc(1,:,:));
		end

		% Backstepping Trafo of matrix possible with a permutation of the
		% dimensions.
		A0_BS = permute(Tc(permute(a0_disc,[3 1 2]),KKern,zdiscRegEq,zdiscKernel),[2 3 1]);

		% FEHLT NOCH: AUCH BS TRAFO ABER ABHAENGIG VON nd!
		G1_BS = permute(Tc(permute(G1_disc,[3 1 2]),KKern,zdiscRegEq,zdiscKernel),[2 3 1]);
		M1_disc = A0_BS - dzeta_K_z_0_Lambda - K_z_0_lambda_diff;
		M2_disc = K_z_0_lambda;
		for i=1:n
			for j=1:n
				if system.b_op1.b_n(j,j) ~= 0 % Robin/Neumann left
					M1_disc(:,i,j) = M1_disc(:,i,j) + a0_til(:,i,j);
				else % dirichlet left
					M2_disc(:,i,j) = M2_disc(:,i,j) + a0_til(:,i,j);
				end
			end
		end


		% TODO:
		% CHECK if nd >1!
		G1_til(1:ndiscRegEq,1:n,1:nd) = 0;
		H1(1:ndiscRegEq,1:n,1:nv) = 0;
		for z_idx =1:ndiscRegEq
			G1_til(z_idx,1:n,1:nd) = reshape(G1_BS(z_idx,:,:),[n nd])+ ...
				(reshape(M1_disc(z_idx,:,:),[n n])*(E1*E1.') + reshape(M2_disc(z_idx,:,:),[n n])*(E2*E2.'))*G2;
			%   z  x n x nd
			H1(z_idx,:,:) = reshape(G1_til(z_idx,:,:),[n nd])*sigMod.pd;
		end
		H2 = G2*sigMod.pd;
		H3 = G3*sigMod.pd;
		H4 = G4*sigMod.pd;

		% preallocate and starting value
		% phi_til belonging to k=0 is zero and so is phi and pi.
		% phi(1:ndisc,1:2n,k)
		% initialize solution array
		%    ndisc,  n,  k+1 (k starts at zero, index at 1 for each block!)
		%    +1 index for k=0 for each block
		pi_j(1:ndiscRegEq,1:n,1:nv+length(sigMod.blockLength)) =0; 
		piz_j(1:ndiscRegEq,1:n,1:nv+length(sigMod.blockLength)) =0; 
		pizz_j(1:ndiscRegEq,1:n,1:nv+length(sigMod.blockLength)) =0; 

		% prepare a vector to only select k>=1 later
		selectionVector(1:nv+length(sigMod.blockLength))=0;

		D = zeros(1,nv);

		%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
		% 2. Calculate modal solution

		% loop through all jordan blocks:
		for jBlock = 1:length(sigMod.blockLength)
			gamma_for_vec(1:ndiscRegEq,1:(2*n)^2,1:(2*n)^2)=0; % preallocate
			gamma(1:ndiscRegEq,1:2*n,1:2*n) = 0; %preallocate
			Psi_mu_vec(1:ndiscRegEq,1:ndiscRegEq,1:(2*n)^2)=0; % preallocate
			Psi_mu(1:ndiscRegEq,1:ndiscRegEq,1:2*n,1:2*n) = 0; %preallocate
			try
				mu = sigMod.lambda(sum(sigMod.blockLength(1:jBlock-1))+1);
			catch err
				mythrow(err)
			end
			for z_idx = 1:ndiscRegEq
				% currently considered eigenvalue
				try
				gamma(z_idx,:,:) = ...
					[zeros(n,n)                                                                   eye(n);
					 reshape(l_disc(z_idx,:,:),[n n])\(reshape(muSys(z_idx,:,:),[n n])+mu*eye(n)) zeros(n,n)];
				catch err
					mythrow(err)
				end
				for i=1:2*n %number of columns in Psi
					% create diagonal matrix of gammas
					gamma_for_vec(z_idx,(i-1)*2*n+1:i*2*n,(i-1)*2*n+1:i*2*n) = gamma(z_idx,:,:);
				end
			end

			%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
			% calculate fundamental matrix:
			% as the solution of 
			%     dz Psi_mu(z,zeta) = gamma(z) Psi_mu(z,zeta) z in (0,1),zeta
			%     in (0,z)
			%     Psi_mu(zeta,zeta) = I
			if n>1
				gammaInterp = griddedInterpolant({zdiscRegEq,1:(2*n)^2,1:(2*n)^2},gamma_for_vec);
				gamma_gen = @(z) squeeze(gammaInterp({z(:),1:(2*n)^2,1:(2*n)^2}));
			else
				gammaInterp = griddedInterpolant({zdiscRegEq},gamma_for_vec);
				gamma_gen = @(z) squeeze(gammaInterp({z(:)}));
			end		
			Psi_0 = eye(2*n);
			Psi_vec_0 = Psi_0(:); % vectorized
			% Problem: matlab-solver only for vectors.
			% --> Matrix must be vectorized
			% --> Gamma blockdiagonalized
			for zeta_idx =1:ndiscRegEq-1
				% starting value for z
				z0 = zdiscRegEq(zeta_idx); 
				f=@(z,Psi_vec) gamma_gen(z)*Psi_vec;
				[z,Psi_z_zeta_vec]=ode15s(f,[z0 1],Psi_vec_0,opts);
				if n>1
					PsiInterp = griddedInterpolant({z,1:(2*n)^2},Psi_z_zeta_vec);
					Psi_z_zeta_vec_res = PsiInterp({zdiscRegEq(zeta_idx:ndiscRegEq),1:(2*n)^2});
				else
					PsiInterp = griddedInterpolant({z},Psi_z_zeta_vec);
					Psi_z_zeta_vec_res = PsiInterp({zdiscRegEq(zeta_idx:ndiscRegEq)});
				end
	% 			Psi_z_zeta_vec_res = interp1(z,Psi_z_zeta_vec,zdiscRegEq(zeta_idx:ndiscRegEq).');
				Psi_mu_vec(zeta_idx:ndiscRegEq,zeta_idx,:) = Psi_z_zeta_vec_res;
			end
			Psi_mu_vec(ndiscRegEq,ndiscRegEq,:) = Psi_vec_0; %#ok<AGROW>
			% put vector in matrix form:
			for j=1:2*n
				%      z,zeta,i,j            z,zeta,i
				Psi_mu(:,:,:,j) = Psi_mu_vec(:,:,(j-1)*2*n+1:j*2*n);
			end
			% fundamental matrix ready.
			%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

			%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
			% calculate unknown initial value
			% phi_til_k_0

			% loop through all generalized eigenvectors of the current
			% jordan block and calculate pi_j
			for k=1:sigMod.blockLength(jBlock)
				% k is index of genVec in current block
				% overall index of genVec:
				indexOfGenVec = sum(sigMod.blockLength(1:jBlock-1))+k;

				% index in pi:
				indexInPi = sum(sigMod.blockLength(1:jBlock-1)+1)+k+1;

				% current generalized eigenvector:
				try
					varphi_j_k = sigMod.genEig(:,indexOfGenVec);
				catch err
					mythrow(err)
				end
				% nv x 1
				h1_j_k(1:ndiscRegEq,1:n)=0; %preallocate and reset
				h1_j_k_til(1:ndiscRegEq,1:2*n)=0; %preallocate and reset
				H1_j_mu(1:ndiscRegEq,1:2*n) = 0; %preallocate and reset
				% nx1
				h2_j_k = H2*varphi_j_k;
				Hij_mu_til(1:ndiscRegEq,1:2*n)=0; % preallocate and reset
				% nx1
				T1_H_til(1:ndiscRegEq,1:n) = 0;% preallocate and reset
				T2_H_til(1:ndiscRegEq,1:n) = 0;% preallocate and reset
				R1_j_mu_km1(1:ndiscRegEq,1:2*n)=0;% preallocate and reset
				T1_R_mu(1:ndiscRegEq,1:n) = 0; % preallocate and reset
				T2_R_mu(1:ndiscRegEq,1:n) = 0; % preallocate and reset
				% nx1
				h4_j_k = (sigMod.pr-H4)*varphi_j_k;
				M0_mu(1:ndiscRegEq,1:2*n,1:n) = 0; % preallocate and reset
				M_mu(1:ndiscRegEq,1:2*n,1:n) = 0;  % preallocate and reset
				T1_M_mu(1:ndiscRegEq,1:n,1:n) = 0; %preallocate and reset
				for z_idx=1:ndiscRegEq %                   ndisc x n x nv*nv x 1
					h1_j_k(z_idx,:) = shiftdim(H1(z_idx,:,:),1)*varphi_j_k;
					h1_j_k_til(z_idx,:) = -T2/reshape(l_disc(z_idx,:,:),[n n])...
						*reshape(h1_j_k(z_idx,:),[n 1]);
					temp_prod=0; %reset
					temp_prod(1:z_idx,1:2*n)=0; % preallocate;
					for zeta_idx = 1:z_idx %                                        2n x 2n * 2n x 1
						temp_prod(zeta_idx,:) = (reshape(Psi_mu(z_idx,zeta_idx,:,:),[2*n 2*n])*reshape(h1_j_k_til(zeta_idx,:),[2*n 1]));
					end%     2n x 1
					H1_j_mu(z_idx,:) = trapz_fast(zdiscRegEq(1:z_idx),temp_prod.').';

					%%%%%       2n x 1
					Hij_mu_til(z_idx,:) = reshape(Psi_mu(z_idx,1,:,:),[2*n 2*n])...
						*(T1*(E1*E1.')+T2*(E2*E2.'))*h2_j_k...
						+ reshape(H1_j_mu(z_idx,:),[2*n 1]);
					T1_H_til(z_idx,:) = T1.'*reshape(Hij_mu_til(z_idx,:),[2*n 1]);
					T2_H_til(z_idx,:) = T2.'*reshape(Hij_mu_til(z_idx,:),[2*n 1]);

					%%%%%
					% get R1 from preceeding run:
					temp_prod_R = 0;
					temp_prod_R(1:z_idx,1:2*n) = 0;
					for zeta_idx=1:z_idx
						try
						temp_prod_R(zeta_idx,:) = ...
							reshape(Psi_mu(z_idx,zeta_idx,:,:),[2*n 2*n])...
							* T2/reshape(l_disc(zeta_idx,:,:),[n n]) ...
							* reshape(pi_j(zeta_idx,:,indexInPi-1),[n 1]);
												   % +1 for k=0 --> index 1
												   % -1 for k-1.
						catch err
							mythrow(err)
						end
					end
					R1_j_mu_km1(z_idx,:) = trapz_fast(zdiscRegEq(1:z_idx),temp_prod_R(:,:).').';
					T1_R_mu(z_idx,:) = T1.'*reshape(R1_j_mu_km1(z_idx,:),[2*n 1]);
					T2_R_mu(z_idx,:) = T2.'*reshape(R1_j_mu_km1(z_idx,:),[2*n 1]);

					% calculate M0_mu(z)
					temp_prod_M1=0;
					temp_prod_M1(1:z_idx,1:2*n,1:numDir)=0;
					temp_prod_M2=0;
					temp_prod_M2(1:z_idx,1:2*n,1:n-numDir)=0;
					for zeta_idx =1:z_idx
						temp_prod_M1(zeta_idx,1:2*n,1:numDir) = ...
							reshape(Psi_mu(z_idx,zeta_idx,:,:),[2*n 2*n])*T2...
							/reshape(l_disc(zeta_idx,:,:),[n n])...
							*reshape(a0_til(zeta_idx,:,:),[n n])*E1;
						temp_prod_M2(zeta_idx,1:2*n,1:n-numDir) = ...
							reshape(Psi_mu(z_idx,zeta_idx,:,:),[2*n 2*n])*T2...
							/reshape(l_disc(zeta_idx,:,:),[n n])...
							*reshape(a0_til(zeta_idx,:,:),[n n])*E2;
					end
					if z_idx>1 % first entry with int>0
						if numDir > 0 % if dirBC exist
							M0_mu(z_idx,1:2*n,1:numDir) = ...
								trapz(zdiscRegEq(1:z_idx),...
									temp_prod_M1,1);
						end
						if n-numDir > 0 % if Robin BC exist
							M0_mu(z_idx,1:2*n,numDir+1:n) = ...
								 trapz(zdiscRegEq(1:z_idx),...
									temp_prod_M2,1);
						end
					end
					% Problem: Psi ist untere Dreiecksmatrix! --> bei nur
					% Dirichlet-RB sind reg. eqns nicht l�sbar
					% pr�fen, warum! Mit entsprechendem Ausgang m�ssten diese
					% schon l�sbar sein, aebr hier ist immer schon M_mu gleich
					% 0! Ist Psi immer untere Dreiecksmatrix?
					M_mu(z_idx,1:2*n,1:n) = reshape(Psi_mu(z_idx,1,:,:),[2*n 2*n])...
						*[T2*E1, T1*E2-T2*E2*Q0] + shiftdim(M0_mu(z_idx,:,:),1); % hier kein reshape moeglich, weil numDir=0 sein kann!
					T1_M_mu(z_idx,1:n,1:n) = T1.'*reshape(M_mu(z_idx,:,:),[2*n n]);

				end
				% calculate unknown starting value:
				% phi_j_k_til_0 = (CTc^(-1)[T1'M_mu])^(-1)
				%                * (h4j-CTc^(-1)[T1^T R1km1] + CTc^(-1)[T1'Hij_mu_til])
				KI = kernel.KI;
				c_op = system.c_op;
				% Ausgangsoperator kann nur auf Vektor angewendet werden!
				% --> Auf jede Spalte von Tc T1 M_mu einzeln anwenden:
				TcT1M = permute(Tc(permute(T1_M_mu,[3 1 2]),-KI,zdiscRegEq,zdiscKernel),[2 3 1]);
				temp1 = c_op.use_output_operator(...
							TcT1M...
						,zdiscRegEq);
				temp1Comp(1:m,1:n) = 0; % preallocate and reset
				for j=1:n % #outputs = #states
					% Spalte von T1.'*M in Systemdarstellung bringen:
					for i=1:n
						TcT1M_vec((i-1)*ndiscRegEq+1:i*ndiscRegEq) = TcT1M(:,i,j);
					end
					temp1Comp(1:m,j) = c_op.use_output_operator(...
							TcT1M_vec...
						,zdiscRegEq);
				end
				% T1.'*R1 in Systemdarstellung bringen:
				% T1.'*H_til in Systemdarstellung bringen:
				TcT1_R = permute(Tc(permute(T1_R_mu,[3 1 2]),-KI,zdiscRegEq,zdiscKernel),[2 3 1]);
				TcT1_H = permute(Tc(permute(T1_H_til,[3 1 2]),-KI,zdiscRegEq,zdiscKernel),[2 3 1]);

				for i=1:n
					TcT1R_vec((i-1)*ndiscRegEq+1:i*ndiscRegEq) = TcT1_R(:,i);
					TcT1H_vec((i-1)*ndiscRegEq+1:i*ndiscRegEq) = TcT1_H(:,i);
				end
				if rank(temp1) < size(temp1,1)
					error('Regulator equations are not solvable!')
				end
				D(indexOfGenVec) = rank(temp1);
				phi_j_k_til_0 = temp1\...
					(...
						h4_j_k...
						+c_op.use_output_operator(...
							-TcT1H_vec-TcT1R_vec,zdiscRegEq...
						)...
					);

				% todo: phi_j ausrechnen. Muss aber geschrieben werden, damit
				% naechster Schritt gemacht werden kann!
				for z_idx =1:ndiscRegEq
					pi_j(z_idx,1:n,indexInPi) = T1.'*reshape(M_mu(z_idx,:,:),[2*n n])...
						* phi_j_k_til_0...
						+ reshape(T1_H_til(z_idx,:),[n 1])...
						+ reshape(T1_R_mu(z_idx,:),[n 1]);
					piz_j(z_idx,1:n,indexInPi) = T2.'*reshape(M_mu(z_idx,:,:),[2*n n])...
						* phi_j_k_til_0...
						+ reshape(T2_H_til(z_idx,:),[n 1])...
						+ reshape(T2_R_mu(z_idx,:),[n 1]);
					pizz_j(z_idx,1:n,indexInPi) = T2.'...
						* reshape(gamma(z_idx,:,:),[2*n 2*n])*[reshape(pi_j(z_idx,:,indexInPi),[n 1]); reshape(piz_j(z_idx,:,indexInPi),[n 1])]...
						+ T2.'*T2/reshape(l_disc(z_idx,:,:),[n n])*reshape(a0_til(z_idx,:,:),[n n])*[E1 E2]*phi_j_k_til_0...
						+ T2.'*T2/reshape(l_disc(z_idx,:,:),[n n])*reshape(pi_j(z_idx,:,indexInPi-1),[n 1])...
						+ T2.'*reshape(h1_j_k_til(z_idx,:),[2*n 1]);
				end
				selectionVector(indexInPi) = 1;
			end % gen eigenVec of current jBlock




		end % current jBlock

		% select pi for k>=1:
		pi_j_k_g_0 = pi_j(:,:,selectionVector==1);
		piz_j_k_g_0 = piz_j(:,:,selectionVector==1);
		pizz_j_k_g_0 = pizz_j(:,:,selectionVector==1);

		%%% modal solution computed
		%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

		%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
		% 3. Calculate original solution
		pi(1:ndiscRegEq,1:n,1:nv)=0; %preallocate
		piz(1:ndiscRegEq,1:n,1:nv)=0; %preallocate
		pizz(1:ndiscRegEq,1:n,1:nv)=0; %preallocate
		for z_idx=1:length(zdiscRegEq)
			pi(z_idx,:,:) = reshape(pi_j_k_g_0(z_idx,:,:),[n nv])/sigMod.genEig;
			piz(z_idx,:,:) = reshape(piz_j_k_g_0(z_idx,:,:),[n nv])/sigMod.genEig;
			pizz(z_idx,:,:) = reshape(pizz_j_k_g_0(z_idx,:,:),[n nv])/sigMod.genEig;
		end

		%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
		% 4. Calculate state feedback gain

	% 	grad_op = get_grad_op(zdisc);

		pi_z = piz;
		pi_zz = pizz;
		Kv = H3 - ~mTN*diag(diag(system.b_op2.b_d))*shiftdim(pi(end,:,:),1) ...
				- system.b_op2.b_n*shiftdim(pi_z(end,:,:),1);
		if any(imag(Kv)>1e-6)
			error('Solution of the regulator equations contains imaginary parts!')
		else
			Kv=real(Kv);
			pi = real(pi);
			pi_z = real(pi_z);
			pi_zz = real(pi_zz);
		end
	else
		error('regulator_equations currently only implemented for ppide_sys!')
	end
	fprintf('Finished! (%0.2fs) \n',toc(regtimer));
else
	pi = [];
	Kv = zeros(system.c_op.m,0);
	pi_z = [];
	pi_zz = [];
	D = [];
% 	warning('No signal model was passed. Regulator equations are empty').
end
end

