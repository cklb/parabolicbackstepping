function K = transform_kernel_element(kernel_element,disc_better)
%TRANSFORM_KERNEL transform the ppde kernel into its original koordinates

% created on 19.04.2017 by Simon Kerschbaum
import misc.*
import numeric.*
import dps.parabolic.control.backstepping.*
import dps.parabolic.system.*

zdisc = kernel_element.zdisc;
ndisc = length(zdisc);
xidisc = kernel_element.xi;
etadisc = kernel_element.eta;

xi_fine =  kernel_element.xi_fine;
eta_fine = kernel_element.eta_fine;
numtL = size(kernel_element.xi,2);
numtKernel = size(kernel_element.G,3);

% preallocate K:
K(1:ndisc,1:ndisc,1:numtKernel)=0;
% G_fine = zeros(size(xi_fine,1),size(eta_fine,1),numtKernel);
for tIdx = 1:numtL
	if numtL >1
		tIdxAll = tIdx;
	else
		tIdxAll = 1:numtKernel;
	end
% 	[XI,ETA] = meshgrid(xidisc(:,tIdx),etadisc(:,tIdx));
% 	[XIFINE, ETAFINE] = meshgrid(xi_fine(:,tIdx),eta_fine(:,tIdx));
	% mit tIdxAll machen!
	% interpolate G on fine grid for disc_better
% 		G_fine(:,:,tIdxAll) = misc.translate_to_grid(kernel_element.G(:,:,tIdxAll),{xi_fine(:,tIdx),eta_fine(:,tIdx)});
		GInterp = numeric.interpolant({xidisc(:,tIdx),etadisc(:,tIdx),tIdxAll},kernel_element.G(:,:,tIdxAll));
end

if disc_better % very performant without loops!
	K = 1./kernel_element.lambda_j.'...
		.* kernel_element.varphi_i...
		.* kernel_element.varphi_j.'...
		.* reshape(GInterp.eval(...
			repmat(kernel_element.xiMat(:),numtKernel,1),...
			repmat(kernel_element.etaMat(:),numtKernel,1),...
			reshape(repmat(1:numtKernel,numel(kernel_element.xiMat),1),[numtKernel*numel(kernel_element.xiMat),1]))...
		,[ndisc, ndisc, numtKernel]);	
else
	for z_idx = 1:ndisc
		z = zdisc(z_idx);
		for zeta_idx = 1:z_idx
			zeta = zdisc(zeta_idx);
			[xi, eta] = kernel_element.get_xi(z,zeta); % als vektor ausserhalb machen!
			for tIdx = 1:numtL
				if numtL >1
					tIdxAll = tIdx;
				else
					tIdxAll = 1:numtKernel;
				end
				if disc_better % use finer grid in G coordinates
					K(z_idx,zeta_idx,tIdxAll) = 1./kernel_element.lambda_j(zeta_idx,tIdx)...
						.* kernel_element.varphi_i(z_idx,tIdx)...
						.* kernel_element.varphi_j(zeta_idx,tIdx)...
						.* GInterp.evaluate(xi(tIdx),eta(tIdx),tIdxAll);
					if isnan(K(z_idx,zeta_idx,tIdx))
						warning('K(z,zeta)=NaN!')
					end
				else % use sparse grid in G coordinates
					K(z_idx,zeta_idx,tIdx) = 1/kernel_element.lambda_j(zeta_idx,tIdx)...
						* kernel_element.varphi_i(z_idx,tIdx)...
						* kernel_element.varphi_j(zeta_idx,tIdx)...
						* kernel_element.G(get_idx(xi(tIdx),xidisc(:,tIdx)),get_idx(eta(tIdx),etadisc(:,tIdx)),tIdx);
					if isnan(K(z_idx,zeta_idx,tIdx))
						warning('K(z,zeta)=NaN!')
					end
				end
			end
		end
		% do not write NaN into kernel because of interpolation problems!
		if z_idx<ndisc
			K(z_idx,z_idx+1:end,:) = repmat(K(z_idx,z_idx,:),1,ndisc-z_idx,1);% outside the area
		end
	end
end

	% K(z,zeta) = 1/lambda_j(zeta) * K_bar(u,v);
	% K_bar(z,zeta) = phi_i(z) * phi_j(zeta) K_til(z,zeta);
	% --> K(z,zeta) = 1/lambda_j(zeta) * phi_i(z) * phi_j(zeta) * K_til(z,zeta);

end

