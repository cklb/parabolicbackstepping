function [G, H, xi_disc, eta_disc] = limit_to_valid(kernel_element, samples)
%LIMIT_TO_VALID Select only valid values out of G
%   Should only be used in the end, before plotting.

%  modified on 18.04.2017 by SK: PPIDEs
%  modified on 18.07.2017 by SK:
%   * enable passing of samples



s=kernel_element.s;
% do not round in advance!
% a = numeric.acc((s+1)/2 *kernel_element.phi_i(end) - (s-1)/2*kernel_element.phi_j(end)); % top of spatial area
% a is the top value of the area
a = (s+1)/2 *kernel_element.phi_i(end) - (s-1)/2*kernel_element.phi_j(end); % top of spatial area
if nargin >1 && samples > 0
	xi_sparse = kernel_element.xi;
	eta_sparse = kernel_element.eta;
	% resample to number of samples
	ximin = min(xi_sparse);
	ximax = max(xi_sparse);
	etamin = min(eta_sparse);
	etamax = max(eta_sparse);
	% xi is discretized with samples. Not yet the correct grid.
	xi_disc = linspace(ximin,ximax,samples);
	diffXi = diff(xi_disc);
	xiInc = diffXi(1);
	
	% the top of the spatial area shall always be contained
	xi_disc = unique([xi_disc a]);
	aIdx = find(xi_disc==a,1);
	xiLeft = xi_disc(1:aIdx);
% 	xiRight = xi_disc(aIdx+1:end); %wont be used but replaced by fixed distance from aDiff
% 	after a
	aDiff = a-xiLeft(end-1);
	xi_disc = [xiLeft a+aDiff:xiInc:ximax ximax];
	
	% eta must have (exactly!) the same increment as xi, to be able to print the edges nicely
	
	% eta shall contain the point 0 and a
	eta_discPos = unique([0:xiInc:etamax etamax]);
	eta_discNeg = unique([-diffXi:-diffXi:etamin etamin]);
	eta_disc = unique([eta_discNeg eta_discPos]);

	% resample to new grid
	GInterp = numeric.interpolant({xi_sparse,eta_sparse,1:size(kernel_element.G,3)},kernel_element.G);
	HInterp = numeric.interpolant({xi_sparse,eta_sparse,1:size(kernel_element.H,3)},kernel_element.H);
	G = GInterp.evaluate(xi_disc,eta_disc,1:size(kernel_element.G,3));
	H = HInterp.evaluate(xi_disc,eta_disc,1:size(kernel_element.H,3));
	eta_l_fine = interp1(kernel_element.xi,kernel_element.eta_l,xi_disc);
else
	xi_disc = kernel_element.xi;
	eta_disc = kernel_element.eta;
	G = kernel_element.G;
	H = kernel_element.H;
	eta_l_fine = kernel_element.eta_l;
end

% remove all values outside the domain
min_dist = kernel_element.min_dist;

for xi_idx = 1:length(xi_disc)
	xi = xi_disc(xi_idx);
	eta_h  = min(numeric.acc(xi,1/min_dist),numeric.acc(2*a-xi,1/min_dist));
	for eta_idx = 1:length(eta_disc)
		eta = eta_disc(eta_idx);
		if numeric.acc(eta,1/min_dist) > numeric.acc(eta_h,1/min_dist) || numeric.acc(eta,1/min_dist) < numeric.acc(eta_l_fine(xi_idx),1/min_dist)
			G(xi_idx,eta_idx,1:end) = NaN;
			H(xi_idx,eta_idx,1:end) = NaN;
		end
	end
end

end

