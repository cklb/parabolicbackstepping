function [xi, eta, xi_fine, eta_fine] = get_xi(kernel_element,z,zeta)
%ppde_kernel_element.GET_XI calculate xi, eta belonging to z,zeta in the
%i,j-th kernel element

% Created on 03.04.2017 by Simon Kerschbaum
import misc.*
import numeric.*
phi_i = kernel_element.phi_i;
phi_j = kernel_element.phi_j;
phi_iInterp = kernel_element.phi_iInterp;
phi_jInterp = kernel_element.phi_jInterp;

z_idx = get_idx(z,kernel_element.zdisc);
zeta_idx = get_idx(zeta,kernel_element.zdisc);

s=kernel_element.s;

xi = s*phi_i(z_idx,:) +s*phi_j(zeta_idx,:) + 1/2*(1-s)*(phi_i(end,:) + phi_j(end,:));
eta = phi_i(z_idx,:) - phi_j(zeta_idx,:) - 1/2*(1-s)*(phi_i(end,:) - phi_j(end,:));

if nargout>2
% 	z_idx_fine = interp1(zdisc,1:ndisc,z);
% 	zeta_idx_fine = interp1(zdisc,1:ndisc,zeta);
% 	if any(isnan(z_idx_fine)) || any(isnan(zeta_idx_fine))
% 		warning(['Returning z/zeta_idx_fine = NaN for z: ' num2str(z)])
% 	end
% 	phi_i_z = interp1(1:ndisc,phi_i(1:ndisc,:),z_idx_fine);
% 	phi_j_zeta = interp1(1:ndisc,phi_j(1:ndisc,:),zeta_idx_fine);
	
	phi_i_z = phi_iInterp.eval(z);
	phi_j_zeta = phi_jInterp.eval(zeta);
	
	xi_fine = s*phi_i_z +s*phi_j_zeta + 1/2*(1-s)*(phi_i(end,:) + phi_j(end,:));
	eta_fine = phi_i_z - phi_j_zeta - 1/2*(1-s)*(phi_i(end,:) - phi_j(end,:));
end
end % function

