function [eta, xi] = get_eta(kernel_element,z,zeta)
%PPIDE_KERNEL_ELEMENT.GET_ETA convert z_coordinates to xi/eta coordinates

%created on 14.09.2016 by Simon Kerschbaum

[xi,eta] = kernel_element.get_xi(z,zeta);
end

