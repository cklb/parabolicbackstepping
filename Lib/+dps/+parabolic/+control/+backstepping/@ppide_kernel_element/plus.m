function obj = plus(a,b)
	% addition of two kernel_elements:
	obj = a;
	obj.G = a.G+b.G;
	obj.H = a.H+b.H;
	obj.J = a.J+b.J;
end
