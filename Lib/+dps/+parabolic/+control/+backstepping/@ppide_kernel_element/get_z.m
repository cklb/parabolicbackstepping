function [z, zeta, zFine, zetaFine] = get_z(obj,varargin)
% PPIDE_KERNEL_ELEMENT.GET_Z convert xi_ij, eta_ij coordinates into z,zeta representation
%
% xi, eta can be vectors of same length and z(i),zeta(i) are computed for
% xi(i),eta(i)
%
% xi and eta are passed as vectors. The corresponding time-index tIdx is passed separately.
% This is needed for a time-variant coordinate transformation.
%
% [z,zeta,z_fine,zeta_fine] = get_z(...)
%   z_fine, zeta_fine are the analytical values of z/zeta belongign to the
%   values of xi/eta.
%   z,zeta are the corresponding discretized values according to the grid
%   obj.zdisc
%
% varargin: (xi,eta,t) or ({xi,eta},t)

% history:
% created on 15.09.2016 by Simon Kerschbaum
%  * modified on 27.10.2016 by SK:
%    - changed to be able to pass vectors
%
%  * modified on 03.04.2017 by SK:
%  * implementation for PIDEs with spatially varying diffusion
import misc.*
import numeric.*


if iscell(varargin{1})
	% shall be evaluated as grid!
	xi = varargin{1}{1}(:);% column vector
	eta = varargin{1}{2}(:).'; % row vector
	if nargin>= 3
		tIdx = varargin{2};
	else
		tIdx = 1;
	end
	onGrid = 1;
else
	xi = varargin{1}(:); % make column vector
	eta = varargin{2}(:);
	if nargin>= 4
		tIdx = varargin{3};
	else
		tIdx = 1;
	end
	onGrid = 0;
end
phi_i = obj.phi_i(:,tIdx);
phi_iFine = obj.phi_iFine;
phi_j = obj.phi_j(:,tIdx);
phi_jFine = obj.phi_jFine;
s=obj.s;

if ~onGrid
	if size(xi,1) ~= size(eta,1)
		error('vectors xi, eta must be of same length!')
	end
end
	
% z = phi_i^(-1)(1/2(s*xi+eta)+1/2(1-s)phi_i(1))
% z and zeta is calculated everywhere and NaNs are inserted later!
xi_p_eta = 1/2*(s*xi+eta)+1/2*(1-s)*phi_i(end);
xi_eta_idx1 = get_idx(xi_p_eta, phi_i,1);
% zeta = phi_j^(-1)(1/2(s*xi-eta)+1/2(1-s)phi_j(1))
xi_m_eta = 1/2*(s*xi-eta)+1/2*(1-s)*phi_j(end);
xi_eta_idx2 = get_idx(xi_m_eta,phi_j,1);

z = obj.f_i_disc(xi_eta_idx1);
zeta = obj.f_j_disc(xi_eta_idx2);
% fine values need to be interpolated as phi_i is only discretized
% can be problematic at the border because of numeric!
if nargout > 2
	% Interpolants of phi_i^-1 and phi_j^-1
	zInterp = numeric.interpolant({phi_iFine},obj.zdiscFine.');
	zetaInterp = numeric.interpolant({phi_jFine},obj.zdiscFine.');
	zFine = numeric.acc(zInterp.eval(xi_p_eta));
	zetaFine = numeric.acc(zetaInterp.eval(xi_m_eta));

	if any(isnan(zFine(:))) || any(isnan(zetaFine(:)))
		warning(['Returning z/zeta_fine = NaN for xi: ' num2str(xi)])
	end
end


end

