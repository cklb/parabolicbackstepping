function obj = refine(kernel_element,factor)
%REFINE Refine the kernels_elements G,H with numpoints points in both directions
% (interpolate)

% created on 16.09.2016 by Simon Kerschbaum
% Datenuebernahme
obj = kernel_element;

z_orig = obj.zdisc;
zeta_orig = obj.zdisc;
xi_orig = kernel_element.xi;
eta_orig = kernel_element.eta;

xi_disc = linspace(xi_orig(1),xi_orig(end),factor*length(xi_orig));
eta_disc = linspace(eta_orig(1),eta_orig(end),factor*length(eta_orig));
z_disc =  linspace(z_orig(1),z_orig(end),factor*length(z_orig));
zeta_disc = z_disc;


[X_o,E_o] = meshgrid(xi_orig,eta_orig);
[Z_o,ZE_o] = meshgrid(z_orig,zeta_orig);
[X,E] = meshgrid(xi_disc,eta_disc);
[Z,ZE] = meshgrid(z_disc,zeta_disc);

obj.G = interp2(X_o,E_o,kernel_element.G.',X,E).';
obj.H = interp2(X_o,E_o,kernel_element.H.',X,E).';
obj.K = interp2(Z_o,ZE_o,kernel_element.K.',Z,ZE).';
obj.xi = xi_disc;
obj.eta = eta_disc;
obj.zdisc = z_disc;

end

