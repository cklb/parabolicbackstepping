function [zeta, z] = get_zeta(obj,xi,eta)
%GET_ZETA convert xi_ij, eta_ij coordinates into z,zeta representation

%created on 03.04.2017 by Simon Kerschbaum

[z,zeta] = obj.get_z(xi,eta);

end

