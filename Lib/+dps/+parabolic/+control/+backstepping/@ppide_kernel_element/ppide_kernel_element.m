classdef ppide_kernel_element
	% ppide_kernel_element the kernel for systems of parabolic pides in (xi-eta)
	% representation
	
	% created on 03.04.2017 by Simon Kerschbaum
	
	properties
		G     % values of the kernel_element, the values are written into the outside of the spatial domain
		H % H1, H in folding case
		J % J
		K % K
		K_t % time derivative of K
		G_t % time derivative of G
		dz_K_1_zeta % derivative w.r.t. z evaluated at (1,zeta)
		dz_K        % derivative w.r.t. z
		dzeta_K_z_0 % derivative w.r.t zeta, eval at (z,0)
		err  % error of the original PDE
		errG % error in G PDE
		errb % boundary error
		zdisc % discretization in original coordinates
		zdiscFine = linspace(0,1,151);   % a discretisation used to create high-res interpolants. Default: linspace(0,1,151).
		zMat  % z(xi,eta,t)
		zMatSparse  % z(xi,eta,t), containing only original z,zeta values
		zetaMat% zeta(xi,eta,t)
		zetaMatSparse 
		zMatInterp  % correspdonding interpolants
		zMatInterpNN % nearest neighbor interpolation
		zetaMatInterp 
		zetaMatInterpNN 
		xi    % discretization vector in xi coordinates
		eta  
		xiMat  % xi(z,zeta)
		etaMat % eta(z,zeta)
		xiMatInterp numeric.interpolant % corresponding interpolants
		etaMatInterp numeric.interpolant
		xiMat_xi % xi(xi,eta) (outside NaN, for multidimensional easy integration)
		etaMat_xi % eta(xi,eta) (outside NaN)
		xi_fine  % discretization needed to represent (nearly) all (z,zeta)-combinations.
		eta_fine % numerically too heavy
		i=1 % indices of the kernel element
		j=1 %
		lambda_i % diffusion coefficents, discretized
		lambda_iFine % discretized with zdiscFine
		lambda_i_t
		lambda_j
		lambda_jFine % discretized with zdiscFine
		lambda_j_t
		lambda_i_diff % respective derivatives
		lambda_j_diff
		lambda_i_diff2
		lambda_j_diff2
		c_i;      % convection coefficients, discretized
		c_j; 
		c_i_diff;
		c_j_diff;
		min_xi_diff = 0.05; % minimum distance of two points in xi-eta-coordintes
		maxNumXi = [];      % alternative: maximum number of points in xi-direction
		min_eta_diff = 0.05; % in the sparse grid, for points inside the domain
		min_xi_diff_border = 0; % minimum distance of two points in the sparse grid
		min_eta_diff_border = 0; % arising from boundary coordinates
		accur = 1e-12; % parameter towards all values are rounded
		min_dist=1e-6; %  distance below which two points are considered equal
		eliminate_convection=1;
	end
	
	properties (Hidden=false) % cannot be modified by name-value pair in constructor
		% if Hidden = true!
		s=1 % s=1 for i<=j, s=-1 otherwise
		phi_i % discretized phi_i
		phi_iFine % discretized phi_i with zdiscFine
		phi_iInterp % corresponding interpolant
		phi_j 
		phi_jFine 
		phi_jInterp % interpolant
		c % abbreviations for PDE solution
		d 
		e
		f
		g
		r 
		a_ij_til
		a_ij_tilXiEta  % a_ij_til(xi_ij,eta_ij)
		a_i
		a_iInterp
		aiZXieta     % ai(z(xi_ij,eta_ij))
		a_j
		a_jInterp
		ajZetaXieta  % aj(zeta(xi_ij,eta_ij))
		b_j
		varphi_i % (lambda_i/lambda_i(0))^(1/4)
		varphi_j % discretized
		varphi_i_diff % derivatives of varphi w.r.t. z/zeta
		varphi_j_diff 
		varphi_i_diff2
		varphi_j_diff2
		f_i_disc % f_i  = phi_i^(-1)(u) = zdisc
		f_j_disc % f_j = phi_j^(-1)(v) = zdisc
		f_i_diff % derivative of phi^(-1) w.r.t. u/v
		f_j_diff % = sqrt(lambda_i/j(z/zeta))
		eta_l % lower border of the spatial domain, discretized!
		eta_lInterp  % corresponding Interpolant
		xi_l % xi_l = eta_l^(-1) (on grid eta_l)
		xi_l_sparse % on grid eta
		xi_lInterp % Interpolant
		eta_l_diff % derivative of eta_l w.r.t xi
		eta_l_diffInterp % derivative of eta_l w.r.t xi
		eta_l_diff_sparse % derivative only for xi values which belong to the border
	end
	
	
	methods
		[xi,eta,xi_fine,eta_fine] = get_xi(obj,z,zeta);
		[eta,xi] = get_eta(obj,z,zeta);
		[z,zeta,z_fine,zeta_fine] = get_z(obj,xi,eta,tIdx);
		[zeta,z] = get_zeta(obj,eta,xi);
		
		[G, H, xi, eta] = limit_to_valid(obj,samples);
		[G, H] = refine(obj,factor);
		
		obj = plus(a,b);
		
		
		function obj= sparse_coords(obj)
			import misc.*
			import numeric.*
			% create a sparse grid
			xi_fine = obj.xi_fine; %#ok<*PROP>
			eta_fine = obj.eta_fine;
			% remove all values closer than min_xi_diff
			
			xi = unique_accur(xi_fine,obj.min_xi_diff);
			eta = unique_accur(eta_fine,obj.min_eta_diff);
			
			% add all the values which belong to boundaries to make sure they are included
			% Problem: in the time-dependent case, xi,eta may have more entries as xi_fine,
			% eta_fine, because the order is different and rows can only be removed in one!
			% --> unique accur must be written in a way, that elements of each column are
			% excluded separately....
			[xi_r1, eta_r1] = obj.get_xi(obj.zdisc,obj.zdisc); % z,z
			[xi_r2, eta_r2] = obj.get_xi(obj.zdisc,zeros(1,length(obj.zdisc))); % z,0
			[xi_r3, eta_r3] = obj.get_xi(ones(1,length(obj.zdisc)),obj.zdisc); % 1,zeta
			xi_r = [xi_r1; xi_r2; xi_r3];
			eta_r = [eta_r1; eta_r2; eta_r3];
			xi_r = unique_accur(xi_r,obj.min_xi_diff_border); % remove values closer as min_xi_diff_border
			eta_r = unique_accur(eta_r,obj.min_eta_diff_border); 
			
			% put the 4 vectors together
			% if lambda is constant, xi_r, eta_r need to be repeated for all times
			% Should be obsolete now!
			if size(xi,2)>1 && size(xi_r,2)==1
				xi_r = repmat(xi_r,1,size(xi,2));
				eta_r = repmat(eta_r,1,size(eta,2));
			end
			xi  = [xi; xi_r];
			eta  = [eta; eta_r];
			
			% sort and remove duplicates
			xi = sort(unique(acc(xi,obj.accur),'rows')); % round towards accur
			eta = sort(unique(acc(eta,obj.accur),'rows'));
			xi = unique_accur(xi,obj.min_dist); % remove values closer as min_dist
			eta = unique_accur(eta,obj.min_dist); % no rounding!
			xi = distributeDuplicates(xi);
			eta = distributeDuplicates(eta);
			
			% alternative way of creating the grid: ensure that the border points upper right and
			% left are on the grid:
			% only if not time-varying:
			% this considers maxNumXi, if given!
			if size(xi,2)==1
				% in that case, min_eta_diff is not active!
				xi_sparse = xi;
				eta_sparse = eta;
				% resample to number of samples
				ximin = min(xi_sparse);
				ximax = max(xi_sparse);
				etamin = min(eta_sparse);
				etamax = max(eta_sparse);
				a = etamax;
				% xi is discretized with samples. Not yet the correct grid.
				if ~isempty(obj.maxNumXi)
					samples = obj.maxNumXi;
				else
					samples = round((ximax-ximin)/obj.min_xi_diff);
				end
				xi_disc = (linspace(ximin,ximax,samples));
				diffXi = diff(xi_disc);
				xiInc = diffXi(1);

				% the top of the spatial area shall always be contained
				xi_disc = unique([xi_disc a]);
				aIdx = find(xi_disc==a,1);
				xiLeft = xi_disc(1:aIdx);
			% 	xiRight = xi_disc(aIdx+1:end); %wont be used but replaced by fixed distance from aDiff
			% 	after a
				aDiff = a-xiLeft(end-1);
				xi = unique([xiLeft a+aDiff:xiInc:ximax ximax]).';

				% eta must have (exactly!) the same increment as xi, to be able to print the edges nicely

				% eta shall contain the point 0 and a
				eta_discPos = unique([0:xiInc:etamax etamax]);
				eta_discNeg = unique([-diffXi:-diffXi:etamin etamin]);
				eta = unique([eta_discNeg eta_discPos]).';
			end			
			% sparse grid:
			obj.xi = xi; %
			obj.eta = eta;			
		end
		
		% Constructor:
		function obj = ppide_kernel_element(varargin)
			import misc.*
			import numeric.*
			% obj = PPIDE_KERNEL_ELEMENT(varargin)
			%   create new PPDE_KERNEL_ELEMENT object.
			%   properties can be passed by 'name', value syntax
			found=0;
			arglist = properties(obj);
			if ~isempty(varargin)
				if mod(length(varargin),2) % uneven number
					error('When passing additional arguments, you must pass them pairwise!')
				end
				for index = 1:2:length(varargin) % loop through all passed arguments:
					for arg = 1:length(arglist)
						if strcmp(varargin{index},arglist{arg})
							obj.(arglist{arg}) = varargin{index+1};
							found=1;
							break
						end 
					end % for arg
					% argument wasnt found in for-loop
					if ~found
						error([varargin{index} ' is not a valid property of class ppide_kernel_element!']);
					end
					found=0; % reset found
				end % for index
			end
			
			numtL = size(obj.xi_fine,2); % length of time vector
			tDisc = linspace(0,1,numtL);
			
			% sign parameter
			% TODO: Provide better check! there should be something that checks that functions
			% are not hitting in space or time!

            if isempty(obj.lambda_i)
                error('No "lambda_i" is provided!');
            end
            if isempty(obj.lambda_j)
                error('No "lambda_j" is provided!');
            end
            % check whether zdisc exists in consistent way
            if isempty(obj.zdisc)
                error('"zdisc" must be provided when calling ppide_kernel_element!');
            end
            
            % check dimensions of lambda_i and lambda_j
            if ~isequal(size(obj.lambda_i),size(obj.lambda_j))
                error('"lambda_i" and "lambda_j" have mismatching dimensions!');
            end
            
            % check consistency of zdisc and lambda
            if ~isequal(length(obj.zdisc),size(obj.lambda_i,1))
                error('Number of elements in first dimension of "lambda_i" doesn�t fit to "zdisc"!');
            end
            
			numtL = max(size(obj.lambda_i,2),size(obj.xi_fine,2)); % length of time vector
			tDisc = linspace(0,1,numtL);

			if obj.lambda_i(1)>= obj.lambda_j(1), obj.s=1; else, obj.s=-1; end
% 			obj.phi_i = cumtrapz_fast_nDim(obj.zdisc,1./sqrt(obj.lambda_i));
% 			obj.phi_j = cumtrapz_fast_nDim(obj.zdisc,1./sqrt(obj.lambda_j));
			
			if ~isempty(obj.lambda_iFine)
				obj.phi_iFine = cumtrapz_fast_nDim(obj.zdiscFine,1./sqrt(obj.lambda_iFine));
				obj.phi_jFine = cumtrapz_fast_nDim(obj.zdiscFine,1./sqrt(obj.lambda_jFine));
				obj.phi_iInterp = numeric.interpolant({obj.zdiscFine},obj.phi_iFine);
				obj.phi_jInterp = numeric.interpolant({obj.zdiscFine},obj.phi_jFine);
			else
				warning('Todo: init_kernel_elements auch lambda_iFine uebergeben wie bei folding!')
				obj.phi_iFine = cumtrapz_fast_nDim(obj.zdisc,1./sqrt(obj.lambda_i));
				obj.phi_jFine = cumtrapz_fast_nDim(obj.zdisc,1./sqrt(obj.lambda_j));
				obj.phi_iInterp = numeric.interpolant({obj.zdisc},obj.phi_iFine);
				obj.phi_jInterp = numeric.interpolant({obj.zdisc},obj.phi_jFine);
			end
			
			obj.phi_i = obj.phi_iInterp.eval({obj.zdisc});
			obj.phi_j = obj.phi_jInterp.eval({obj.zdisc});
			if size(obj.phi_i,2) < numtL
				obj.phi_i = repmat(obj.phi_i,1,numtL);
				obj.phi_j = repmat(obj.phi_j,1,numtL);
			end
			
			if size(obj.phi_i,2) < numtL
				obj.phi_i = repmat(obj.phi_i,1,numtL);
				obj.phi_j = repmat(obj.phi_j,1,numtL);
			end
			
			% calculate derivatives numerically, if not passed
			if isempty(obj.lambda_i_diff)
				obj.lambda_i_diff = diff_num(obj.zdisc,obj.lambda_i);
			elseif ~isequal(size(obj.lambda_i_diff),size(obj.lambda_i))
				error('Inconsistent number of elements. "lambda_i_diff" must have same size as "lambda_i"!');
			end
			if isempty(obj.lambda_i_t)
				obj.lambda_i_t = diff_num(tDisc,obj.lambda_i,2);
			elseif ~isequal(size(obj.lambda_i_t),size(obj.lambda_i))
				error('Inconsistent number of elements. "lambda_i_t" must have same size as "lambda_i"!');
			end
			if isempty(obj.lambda_j_diff)
				obj.lambda_j_diff = diff_num(obj.zdisc,obj.lambda_j);
			elseif ~isequal(size(obj.lambda_j_diff),size(obj.lambda_j))
				error('Inconsistent number of elements. "lambda_j_diff" must have same size as "lambda_j"!');
			end
			if isempty(obj.lambda_j_t)
				obj.lambda_j_t = diff_num(tDisc,obj.lambda_j,2);
			elseif ~isequal(size(obj.lambda_j_t),size(obj.lambda_j))
				error('Inconsistent number of elements. "lambda_j_t" must have same size as "lambda_j"!');
			end
			if isempty(obj.lambda_i_diff2)
				obj.lambda_i_diff2 = diff_num(obj.zdisc,obj.lambda_i_diff);
			elseif ~isequal(size(obj.lambda_i_diff2),size(obj.lambda_i))
				error('Inconsistent number of elements. "lambda_i_diff2" must have same size as "lambda_i"!');
			end
			if isempty(obj.lambda_j_diff2)
				obj.lambda_j_diff2 = diff_num(obj.zdisc,obj.lambda_j_diff);
            elseif ~isequal(size(obj.lambda_j_diff2),size(obj.lambda_j))
                error('Inconsistent number of elements. "lambda_j_diff2" must have same size as "lambda_j"!');
            end
            % Now check dimension of c and c_diff (must be the same as lambda I think)...
            if ~isequal(size(obj.c_i),size(obj.c_j))
                error('"c_i" and "c_j" have mismatching dimensions!');
            end
            if ~isequal(size(obj.c_i),size(obj.lambda_i))
				% They can be different if one is time dependent and the other is not!
				% note that currently, c_i is always repeatet for each timestep if its
				% constant, but lambda_i is not!
%                 error('Inconsistent number of elements. "c_i" has different dimensions than "lambda_i"!');
            end
            if isempty(obj.c_i_diff)
                obj.c_i_diff = diff_num(obj.zdisc,obj.c_i);
            elseif ~isequal(size(obj.c_i),size(obj.c_i_diff))
                error('Inconsistent number of elements. "c_i_diff" must have same size as "c_i"!');
            end
            if isempty(obj.c_j_diff)
                obj.c_j_diff = diff_num(obj.zdisc,obj.c_j);
            elseif ~isequal(size(obj.c_j),size(obj.c_j_diff))
                error('Inconsistent number of elements. "c_j_diff" must have same size as "c_j"!');
            end
            
            
			% TODO: adjust varphis and diffs, at first: numerically!
			if obj.eliminate_convection
				obj.varphi_i = (obj.lambda_i./obj.lambda_i(1,:)).^(1/4) ...
							.* exp(-cumtrapz_fast_nDim(obj.zdisc,(1/2*obj.c_i./obj.lambda_i)));
			else
				obj.varphi_i = ones(length(obj.zdisc),numtL);
			end
			% derivative of varphi w.r.t. u can be calculated analytically,
			% easily in dependence on varphi_i
			if obj.eliminate_convection
				if any(obj.c_i~=0)
					obj.varphi_i_diff = (obj.lambda_i_diff/2-obj.c_i)./2./sqrt(obj.lambda_i).*obj.varphi_i;
					obj.varphi_i_diff2 = (1/2*obj.lambda_i_diff2 - obj.c_i_diff)/2./obj.lambda_i.*obj.varphi_i...
										+ (1/4*obj.lambda_i_diff-1/2*obj.c_i-1./(2*sqrt(obj.lambda_i))).*obj.varphi_i_diff;
				else
					obj.varphi_i_diff = obj.lambda_i_diff/4./(obj.lambda_i(1,:).*obj.lambda_i).^(1/4);
					obj.varphi_i_diff2 = 1/4*obj.varphi_i.*obj.lambda_i_diff2 ...
									- 1/16./(obj.lambda_i(1,:).*obj.lambda_i.^3).^(1/4).*obj.lambda_i_diff.^2;
				end	
			end
			if obj.eliminate_convection
				obj.varphi_j = (obj.lambda_j./obj.lambda_j(1,:)) .^(1/4)...
						   .* exp(cumtrapz_fast_nDim(obj.zdisc,(1/2*obj.c_j./obj.lambda_j)));
			else
				obj.varphi_j = ones(length(obj.zdisc),numtL);
			end

			if obj.eliminate_convection
				if any(obj.c_j~=0)
					obj.varphi_j_diff = (obj.lambda_j_diff/2+obj.c_j)./2./sqrt(obj.lambda_j).*obj.varphi_j;
					obj.varphi_j_diff2 = (1/2*obj.lambda_j_diff2 + obj.c_j_diff)/2./obj.lambda_j.*obj.varphi_j...
										+ (1/4*obj.lambda_j_diff+1/2*obj.c_j-1./(2*sqrt(obj.lambda_j))).*obj.varphi_j_diff;
				else
					obj.varphi_j_diff = obj.lambda_j_diff/4./(obj.lambda_j(1,:).*obj.lambda_j).^(1/4);
					obj.varphi_j_diff2 = 1/4*obj.varphi_j.*obj.lambda_j_diff2 ...
						- 1/16./(obj.lambda_j(1,:).*obj.lambda_j.^3).^(1/4).*obj.lambda_j_diff.^2;
				end
			end
			
			obj.f_i_disc = obj.zdisc(:); %obj.f_i(obj.phi_i); obj.phi_i=phi_i(zdisc) 
			obj.f_j_disc = obj.zdisc(:); %obj.f_j(obj.phi_j);
			obj.f_i_diff = sqrt(obj.lambda_i); % derivative w.r.t u/v!
			obj.f_j_diff = sqrt(obj.lambda_j);
			
			obj = obj.sparse_coords;
			s=obj.s;
			if isempty(obj.eta_fine) || isempty(obj.xi_fine)
				warning('either xi or eta is not initialized and thus not eta_l(xi)!')
			else
				if obj.lambda_i(1)==obj.lambda_j(1)
					obj.eta_l = zeros(size(obj.xi,1),numtL);
					obj.eta_l_diff = zeros(size(obj.xi,1),numtL);
				else
					if numtL > 1
						error('This is not implemented for time-varying lambda!')
					end
					% take only values of xi, which belong to the lower
					% boundary (z,z)
% 					xi_zz = obj.get_xi(obj.zdisc,obj.zdisc); 
% 					[~,~,xi_zzFine,~] = obj.get_xi(obj.zdiscFine,obj.zdiscFine);
					xi_zzFine = s*obj.phi_iFine + s*obj.phi_jFine + 1/2*(1-s)*(obj.phi_iFine(end,:) + obj.phi_jFine(end,:));
% 					eta_l_sparse = obj.phi_i - obj.phi_j - 1/2*(1-s)*(obj.phi_i(end,:)-obj.phi_j(end,:));
					eta_l_fine = obj.phi_iFine - obj.phi_jFine - 1/2*(1-s)*(obj.phi_iFine(end,:)-obj.phi_jFine(end,:));
					obj.eta_lInterp = numeric.interpolant({xi_zzFine},eta_l_fine);
					% derivative w.r.t xi can be calculated analytically
					obj.eta_l_diff_sparse = s*(sqrt(obj.lambda_j)-sqrt(obj.lambda_i))./(sqrt(obj.lambda_i)+sqrt(obj.lambda_j));
					eta_l_diff_fine = s*(sqrt(obj.lambda_jFine)-sqrt(obj.lambda_iFine))./(sqrt(obj.lambda_iFine)+sqrt(obj.lambda_jFine));
					obj.eta_l_diffInterp = numeric.interpolant({xi_zzFine},eta_l_diff_fine);

					
					obj.eta_l = obj.eta_lInterp.eval({obj.xi});
					obj.eta_l_diff = obj.eta_l_diffInterp.eval(obj.xi);
					
					xi_l_fine = misc.inv_fun(eta_l_fine,xi_zzFine,eta_l_fine).';
					obj.xi_lInterp = numeric.interpolant({eta_l_fine},xi_l_fine);
					obj.xi_l = obj.xi_lInterp.eval(obj.eta_l);
					obj.xi_l_sparse = obj.xi_lInterp.eval(obj.eta(obj.eta<0));
					% get the lower boarder for each existing xi-value
% 					for tIdx=1:numtL
% 						obj.eta_l(:,tIdx) = interp1(acc(xi_zz(:,tIdx)),eta_l_sparse(:,tIdx),acc(obj.xi(:,tIdx)));
% 						obj.eta_l_diff(:,tIdx)  = interp1(acc(xi_zz(:,tIdx)),obj.eta_l_diff_sparse(:,tIdx),acc(obj.xi(:,tIdx)));
						% get the inverse function
% 					end
					
% 					for tIdx=1:numtL
% 						obj.xi_l(:,tIdx) = inv_fun(obj.eta_l(:,tIdx),obj.xi(:,tIdx),obj.eta_l(:,tIdx));
% 						obj.xi_l_sparse(:,tIdx) = interp1(acc(obj.eta_l(:,tIdx)),obj.xi_l(:,tIdx),flipud(acc(obj.eta(obj.eta(:,tIdx)<0,tIdx))));
% 					end
					% ATTENTION: xi_l is not discretized at each point eta,
					% but only at the points given by eta_l!
				end %else lambda_i == lambda=j
			end % else isempty(obj.eta_fine) || isempty(obj.xi_fine)
			
% 			zMat0 = zeros(size(obj.xi,1),size(obj.eta,1),numtL);
% 			zMatSparse0 = zeros(size(obj.xi,1),size(obj.eta,1),numtL);
% 			zetaMat0 = zeros(size(obj.xi,1),size(obj.eta,1),numtL);
% 			zetaMatSparse0 = zeros(size(obj.xi,1),size(obj.eta,1),numtL);
% 			zMat = NaN*zeros(size(obj.xi,1),size(obj.eta,1),numtL);
% 			zMatSparse = NaN*zeros(size(obj.xi,1),size(obj.eta,1),numtL);
% 			zetaMat = NaN*zeros(size(obj.xi,1),size(obj.eta,1),numtL);
% 			zetaMatSparse = NaN*zeros(size(obj.xi,1),size(obj.eta,1),numtL);
			xiMat_xi = zeros(size(obj.xi,1),size(obj.eta,1),numtL);
			etaMat_xi = zeros(size(obj.xi,1),size(obj.eta,1),numtL);
			if numtL > 1 
				error('Not yet implemented for time varying lambda!')
			end
			for tIdx=1:numtL
				a = (s+1)/2 *obj.phi_i(end,tIdx) - (s-1)/2*obj.phi_j(end,tIdx); %#ok<NASGU> % top of spatial area is 2a
				xiMat_xi(:,:,tIdx) = repmat(obj.xi(:,tIdx),1,size(obj.eta,1));
				etaMat_xi(:,:,tIdx) = repmat(obj.eta(:,tIdx).',size(obj.xi,1),1);
				
				[zMatSparse0,zetaMatSparse0,zMat0,zetaMat0] = obj.get_z({obj.xi,obj.eta},tIdx);
				zMatSparse = zMatSparse0;
				zetaMatSparse = zetaMatSparse0;
				zMat = zMat0;
				zetaMat = zetaMat0;
				
				% Insert NaN into matrices! Important for easy integration.
				% logical indexing, a lot faster than loops
				varnames = {'xiMat_xi','etaMat_xi','zMatSparse','zMat','zetaMatSparse','zetaMat'};
				% set values outside the domain to NaN
				for nameIdx = 1:length(varnames)
					eval([varnames{nameIdx} '(~(acc(obj.eta.'')>=acc(obj.eta_l) & acc(obj.eta.'') <= min(acc(obj.xi),acc(2*a-obj.xi)))) = NaN;']);
				end
				% for interpolation
				obj.zMatInterp{tIdx} = numeric.interpolant({obj.xi(:,tIdx),obj.eta(:,tIdx)},zMat0(:,:,tIdx));
				obj.zMatInterpNN{tIdx} = griddedInterpolant({obj.xi(:,tIdx),obj.eta(:,tIdx)},zMatSparse0(:,:,tIdx),'nearest');
				obj.zetaMatInterp{tIdx} = numeric.interpolant({obj.xi(:,tIdx),obj.eta(:,tIdx)},zetaMat0(:,:,tIdx));
				obj.zetaMatInterpNN{tIdx} = griddedInterpolant({obj.xi(:,tIdx),obj.eta(:,tIdx)},zetaMatSparse0(:,:,tIdx),'nearest');
			end
			obj.xiMat_xi = xiMat_xi;
			obj.etaMat_xi = etaMat_xi;
			% PROBLEM: MUESSTE VERMUTLICH MIT FEINEM GRID GEMACHT WERDEN, ABER eta_l AKTUELL
			% NUR FUER NORMALES GRID VERFUGBAR. KOENNTE INTERPOLIERT WERDEN, ABER FRAGLICH OB
			% DIE WERTE DANN SINNVOLL SIND. DAS HATTE ICH VERMUTLICH SCHON MAL GEMACHT.
			% NEAREST NEIGHBOR INTERPOLATION IS SEHR LANGSAM!
			obj.zMat = zMat;
			obj.zetaMat = zetaMat;
			obj.zMatSparse = zMatSparse;
			obj.zetaMatSparse = zetaMatSparse;
		end %constructor
		
		% phi_i^(-1) realized as functions for the optional arguments.
		% in discretized form, there should be no call necessary
		function f_i = f_i(obj,u,return_idx)
			if nargin < 3
				return_idx = false;
			end
			f_i = inv_fun(obj.phi_i,obj.zdisc,u,return_idx);
		end
		function f_j = f_j(obj,v,return_idx)
			if nargin < 3
				return_idx = false;
			end
			f_j = inv_fun(obj.phi_j,obj.zdisc,v,return_idx);
		end
	end
	
end

