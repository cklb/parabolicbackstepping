classdef ppide_controller
	%PPIDE_CONTROLLER Store ppide controller parameters
	%   Contains the parameters r(z) and the kernel of a backstepping-based
	%   state feedback controller
	
% created on 15.05.2018 by Simon Kerschbaum
	
	properties
		ppide        dps.parabolic.system.ppide_sys % corresponding ppide
% 		kernel       dps.parabolic.control.backstepping.ppide_kernel % controller kernel
		r            % distributed state feedback gain in system representation
		rAna         % analytical r for debug purpose
		rz
	end
	
	methods
		[r,r_alt,r_ana,rz] = compute_ppide_controller(obj,kernel,ppide,plotdzK,target_BC);
		[r,r_alt,r_ana,rz] = compute_ppide_controllerFolding(obj,kernel,ppide,plotdzK);
		% constructor
		function obj = ppide_controller(kernel,ppide,debug)
			if nargin<3 % it is love! <3
				debug=0;
			end
% 			obj.kernel=kernel;
			obj.ppide = ppide;
			
			if ppide.folded
				% analytical derivative not yet implemented
				[obj.r, r_comp, obj.rAna, obj.rz] = obj.compute_ppide_controllerFolding(kernel,ppide,debug);
			else
				if ppide.ctrl_pars.eliminate_convection
					[obj.r, r_comp, obj.rAna, obj.rz] = obj.compute_ppide_controller(kernel,ppide,debug);
				else
					[obj.r, r_comp, obj.rAna, obj.rz] = obj.compute_ppide_controller(kernel,ppide,debug);
				end
			end
			if ppide.folded
				n = ppide.n/2;
			else
				n = ppide.n;
			end
			if debug
				figure('Name','Regler mit analytischer und numerischer Ableitung')
				hold on
				ndisc = ppide.ndisc;
				num=1;
	% 			sub2ind([ppide.n ppide.n],i,j)
				p = size(ppide.b_1,2);
				for i=1:p
					for j=1:n
						subplot(p,n,num)
						title([num2str(i) num2str(j)])
						hold on
						if size(obj.r,3)>1
							tVec = 1:floor(size(obj.r,3)/2):size(obj.r,3);
						else
							tVec = 1;
						end
						for tIdx = tVec
							plot(linspace(0,1,ppide.ndisc),obj.r(i,(j-1)*ndisc+1:j*ndisc,tIdx),'DisplayName','used')
							plot(linspace(0,1,ppide.ndisc),r_comp(i,(j-1)*ndisc+1:j*ndisc,tIdx),'DisplayName','not used')
						end
						legend('-DynamicLegend')
						num=num+1;
					end
				end
			end
		end
		
		function plot(obj)
			%% plot surface of controller
			
			figure('Name','ControllerSurface')
			num=1;

			res_surf_t=31;
			res_surf_z=21;
			zdiscKernel = obj.ppide.ctrl_pars.zdiscCtrl;
			ndiscKernel = numel(zdiscKernel);
			zres = linspace(0,1,res_surf_z);
			n = obj.ppide.n;
			if size(obj.rz,3)>1
				% time dependent surface plot
				tres = linspace(0,1,res_surf_t);
				tDiscKernel = linspace(0,1,size(obj.rz,3));
				[Z,T] = meshgrid(zdiscKernel,tDiscKernel);
				[ZRES,TRES] = meshgrid(zres,tres);
				
				for i=1:n
					for j=1:n
						handles(num) = subplot(n,n,num);
						yres = interp2(Z,T,permute(obj.rz(i,(j-1)*ndiscKernel+1:j*ndiscKernel,:),[3 2 1]),ZRES,TRES);
						surf(TRES,ZRES,yres);
						hold on
						box on
						grid on
						set(gca,'Ydir','reverse')
						xlabel('$t$')
						ylabel('$z$')
						zlabel(['$R_{' num2str(i) num2str(j) '}(z,t)$'])
						num=num+1;
					end
				end
				Link = linkprop(handles,{'view'});
				setappdata(gcf, 'StoreTheLink', Link);
			else
				for i=1:n
					for j=1:n
						subplot(n,n,num);
						yres = interp1(zdiscKernel,permute(obj.rz(i,(j-1)*ndiscKernel+1:j*ndiscKernel),[2 1]),zres);
						plot(zres,yres);
						hold on
						box on
						grid on
						xlabel('$z$')
						zlabel(['$R_{' num2str(i) num2str(j) '}(z)$'])
						num=num+1;
					end
				end
			end
		end
	end
	
end

