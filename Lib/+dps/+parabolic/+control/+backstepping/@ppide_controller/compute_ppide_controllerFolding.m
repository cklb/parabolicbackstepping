function [controller,  controllerNumeric,  controllerAnalytical, distributedController] ...
			= compute_ppide_controllerFolding(obj, kernel, ppide, debug) %#ok<INUSL>
%compute_ppide_controllerFolding Compute state feedback controller for folded ppide_systems
%   
%	controller = ompute_ppide_controller(kernel, ppde)
%     compute the state feedback controller
%     K for usage in the control law
%       u = controller*x;
%     for coupled ppide_systems. The vector x contains the discretized values
%     of the different states as follows: x = [x1(0),  x1(h),  x1(2h), ... x2(0), 
%     x2(h), ...]^T,  where h is the discretization distance.
%
%   controller = compute_ppide_controller(kernel, ppde, debug) plots the comparison
%     of numerical and analytical derivatives
%
%   [~,  controllerNumeric] = compute_ppide_controller(kernel, ppde, ...) computes the
%     controller using a numerical derivative of the kernel
%
%   [~, ~, controllerAnalytical] = compute_ppide_controller(kernel, ppde, ...) gets the
%     analytical value of the state feedback (without turning it into a
%     matrix multiplication. (Attention: it also contains the boundary
%     feedback.)
%
%   [~,~,~,distributedController] = compute_ppide_controller(kernel,ppde,...) gets the
%     analytical value of the distributed state feedback (without boundary feedback)
%     ATTENTION: Only works correctly for Neumann Actuation! (TODO!)

% Created on 07.08.2019 by Simon Kerschbaum

%% Import and input checks
import misc.*
import numeric.*
import external.fem.*

if ~exist('debug', 'var')
	debug = 0;
end
n = size(kernel.value, 1);

%% Initialise parameter
% matrices of actuated boundary
tic
fprintf('Computing controller...')
Bd = ppide.b_op2.b_d;
Bn = ppide.b_op2.b_n;

Sd = ppide.b_op2.Sd;
Sr = ppide.b_op2.Sr;

Bd_til = [Sd*Sd.'];

K = kernel.get_K;
dzK_1_zeta = kernel.get_K('dz_K_1_zeta',2);
numtKernel = size(kernel.value{1,1}.G,3);

if numtKernel >1
	timeDependent = 1;
else
	timeDependent = 0;
end
if timeDependent
	numtPlant = ppide.ndiscTime;
	tDiscPlant = linspace(0,1,numtPlant);
else
	numtPlant = 1;
	tDiscPlant = 1;
end
if isa(Bd,'function_handle')
	BdPlant = permute(misc.eval_pointwise(Bd,tDiscPlant),[2 3 1]);
	BdKernel = permute(misc.eval_pointwise(Bd,linspace(0,1,numtKernel)),[2 3 1]);
else
% 	BdPlant = repmat(Bd,1,1,numTPlant);
% 	BdKernel = repmat(Bd,1,1,numTKernel);
	BdPlant = misc.translate_to_grid(Bd,{1:n,1:n,linspace(0,1,numtPlant)},'gridOld',...
		{1:n,1:n,linspace(0,1,size(Bd,3))});
	BdKernel = misc.translate_to_grid(Bd,{1:n,1:n,linspace(0,1,numtKernel)},'gridOld',...
		{1:n,1:n,linspace(0,1,size(Bd,3))});
end

zdisc = kernel.zdisc;
ndisc = length(zdisc);
ndiscPlant = ppide.ndisc;
zdiscPlant = linspace(0, 1, ndiscPlant);

KInterp = numeric.interpolant({zdisc,zdisc,1:n,1:n,linspace(0,1,numtKernel)},K);
dzK_1_zetaInterp = numeric.interpolant({zdisc,1:n,1:n,linspace(0,1,numtKernel)},dzK_1_zeta);
KPlant = KInterp.eval({zdiscPlant,zdiscPlant,1:n,1:n,linspace(0,1,numtPlant)});
dzK_1_zetaPlant = dzK_1_zetaInterp.eval({zdiscPlant,1:n,1:n,linspace(0,1,numtPlant)});

% calculate numerical derivative for comparison
dzK_ij(1:ndisc,1:ndisc,numtKernel) = 0;
dzetaK(1:ndisc,1:ndisc,numtKernel) = 0;
dzK1(1:ndisc,1:n,1:n,numtKernel) = 0;
dzetaK0(1:ndisc,1:n,1:n,numtKernel) = 0;
for it=1:n
	for jt=1:n
		for zeta_idx =1:ndisc
			if zeta_idx < ndisc
				dzK_ij(zeta_idx:end,zeta_idx,:) = diff_num(zdisc(zeta_idx:end),kernel.value{it,jt}.K(zeta_idx:end,zeta_idx,:));
			end
			if zeta_idx>1
				dzetaK(zeta_idx,1:zeta_idx,:) = diff_num(zdisc(1:zeta_idx),kernel.value{it,jt}.K(zeta_idx,1:zeta_idx,:),2);
			end
		end
		dzK_ij(end,end,:) = dzK_ij(end,end-1,:); %cannot be computed numerically
		dzetaK(1,1,:) = dzetaK(2,1,:); %cannot be computed numerically
		dzK1(:,it,jt,:) = dzK_ij(end,:,:);
		dzetaK0(:,it,jt,:) = dzetaK(:,1,:);
	end
end
dzK1Interp = numeric.interpolant({zdisc,1:n,1:n,linspace(0,1,numtKernel)},dzK1);
dzK1Plant = dzK1Interp.eval({zdiscPlant,1:n,1:n,tDiscPlant});

% get mass matrix to implement integral as matrix multiplication
massMatrix = get_lin_mass_matrix(zdiscPlant);

% to get correct size:
zeroSize = zeros(n,n,numtPlant);
% r1 = permute(...
% 	misc.multArray(zeroSize + Bd - Bd_til + misc.multArray(Bn,squeeze(K(end,end,:,:,:))),selZ1Plant,2,1)...
% 	,[1 3 2]); % i, (jzeta), t
r1 = zeroSize + BdPlant - Bd_til + misc.multArray(Bn,squeeze(KPlant(end,end,:,:,:))); % i, (j), t

% controller gain under integral
%												  i j z t
R = permute(misc.multArray(Bn,dzK_1_zetaPlant,2,2),[1 3 2 4])...    i j z t
	+ permute(misc.multArray(Bd_til,squeeze(KPlant(end,:,:,:,:)),2,2),[1 3 2 4]);
RNumeric = permute(misc.multArray(Bn,dzK1Plant,2,2),[1 3 2 4])...    i j z t
	+ permute(misc.multArray(Bd_til,squeeze(KPlant(end,:,:,:,:)),2,2),[1 3 2 4]);
%					% i z j t							 
% Kbar = misc.multArray(Bn,dzK_1_zetaPlant,2,2)...   
% 	+ misc.multArray(Bd_til,squeeze(KPlant(end,:,:,:,:)),2,2);

% Collapse second and third dimension. That is: first all zeta for j=1 then all zeta for j=2 and so on.                               
% KbarResh = reshape(Kbar,n,[],numTKernel);
% r2 = KbarResh;
% create diagonal matrix of MMs
% MMtot = kron(eye(n), massMatrix);
% r2 = permute(misc.multArray(KbarResh,MMtot,2,1),[1 3 2]); % i, (jzeta), t	   
	
% default values 0 for not yet calculated things
controllerAnalytical = zeros(n, n/2*ndisc,numtKernel);
distributedController = zeros(n, n/2*ndisc,numtKernel);

% apply unfolding, and apply mass matrix for the integration
z0 = ppide.ctrl_pars.foldingPoint;
if isscalar(z0)
	z0 = repmat(z0,1,n/2);
end
z0Left = zeros(n/2);
zRight = zeros(n/2,ppide.ndiscPars);
for i=1:n/2
	zRight(i,:) = linspace(z0(i),1,ppide.ndiscPars);
	dZ = diff(zRight(i,:));
	z0Left(i) = z0(i)-dZ(1);
end

% n rows for the n inputs. The first n/2 inputs are the left inputs and the second are the
% right ones.
c1 = zeros(n,n/2*ndiscPlant,numtPlant);
controllerTemp  = zeros(n,n/2*ndiscPlant,numtPlant);
controllerNumericTemp  = zeros(n,n/2*ndiscPlant,numtPlant);
controller = zeros(n,n/2*ndiscPlant,numtPlant);
controllerNumeric = zeros(n, n/2*ndiscPlant, numtPlant);
for j=1:n/2
	z0iIdx = find(zdiscPlant<z0(j),1,'last');
	
	% w1(1) = x(0)
	c1(:,(j-1)*ndiscPlant+1,:) = r1(:,j,:);
	% w2(1) = x(1)
	c1(:,j*ndiscPlant,:) = r1(:,j+n/2,:);
	
	% the left part of the spatial domain of x belongs to w1
	% xi(0:z0i) = w1i(1:0)
	controllerTemp(:,(j-1)*ndiscPlant+1:(j-1)*ndiscPlant+z0iIdx,:) = ...
				1/z0Left(j)*permute(interp1(linspace(zdiscPlant(z0iIdx),0,ndiscPlant),permute(R(:,j,:,:),[3 1 4 2]),zdiscPlant(1:z0iIdx)),...
				[2 1 3]);
	controllerNumericTemp(:,(j-1)*ndiscPlant+1:(j-1)*ndiscPlant+z0iIdx,:) = ...
		1/z0Left(j)*permute(interp1(linspace(zdiscPlant(z0iIdx),0,ndiscPlant),permute(RNumeric(:,j,:,:),[3 1 4 2]),zdiscPlant(1:z0iIdx)),...
		[2 1 3]);
	% xi(z0i:1) = w2i(0:1);
	controllerTemp(:,(j-1)*ndiscPlant+z0iIdx+1:j*ndiscPlant,:) = ...
				1/(1-z0(j))*permute(interp1(linspace(zdiscPlant(z0iIdx+1),1,ndiscPlant),permute(R(:,j+n/2,:,:),[3 1 4 2]),zdiscPlant(z0iIdx+1:end)),...
				[2 1 3]);
	controllerNumericTemp(:,(j-1)*ndiscPlant+z0iIdx+1:j*ndiscPlant,:) = ...
				1/(1-z0(j))*permute(interp1(linspace(zdiscPlant(z0iIdx+1),1,ndiscPlant),permute(RNumeric(:,j+n/2,:,:),[3 1 4 2]),zdiscPlant(z0iIdx+1:end)),...
				[2 1 3]);
	% the mass matrix can only be applied when the controller is created over the whole spatial
	% domain. 
	% If the controller has a 3rd (time) domain, it needs to be permuted after the
	% multiplication.
	if size(controller,3)>1
		controller(:,(j-1)*ndiscPlant+1:j*ndiscPlant,:) = ...
			permute(...
				misc.multArray(controllerTemp(:,(j-1)*ndiscPlant+1:j*ndiscPlant,:),...
				massMatrix,2,1)...
			,[1 3 2])...
		+c1(:,(j-1)*ndiscPlant+1:j*ndiscPlant,:);
		controllerNumeric(:,(j-1)*ndiscPlant+1:j*ndiscPlant,:) = ...
			permute(...
				misc.multArray(controllerNumericTemp(:,(j-1)*ndiscPlant+1:j*ndiscPlant,:),...
				massMatrix,2,1)...
			,[1 3 2])...
		+c1(:,(j-1)*ndiscPlant+1:j*ndiscPlant,:);
	else
		controller(:,(j-1)*ndiscPlant+1:j*ndiscPlant,:) = ...
				misc.multArray(controllerTemp(:,(j-1)*ndiscPlant+1:j*ndiscPlant,:),...
				massMatrix,2,1)...
		+c1(:,(j-1)*ndiscPlant+1:j*ndiscPlant,:);
		controllerNumeric(:,(j-1)*ndiscPlant+1:j*ndiscPlant,:) = ...
				misc.multArray(controllerNumericTemp(:,(j-1)*ndiscPlant+1:j*ndiscPlant,:),...
				massMatrix,2,1)...
		+c1(:,(j-1)*ndiscPlant+1:j*ndiscPlant,:);
	end
end



% debug
if debug
	figure('Name', 'Numerisches und analytisches dzK(1, z)')
	id = 1;
	for it = 1:n
		for jt = 1:n
			subplot(n, n, id)
			hold on
			grid on
			if size(kernel.value{it,jt}.dz_K_1_zeta,2)>1
				numplots = 3;
				idxDiff = round(size(kernel.value{it,jt}.dz_K_1_zeta,2)/numplots);
				for tIdx=1:idxDiff:size(kernel.value{it,jt}.dz_K_1_zeta,2)
					plot(zdisc,kernel.value{it,jt}.dz_K_1_zeta(:,tIdx),'displayName','analytisch')
					plot(zdisc,squeeze(dzK1(:,it,jt,tIdx)),'r','DisplayName','numerisch')
				end
			else
				plot(zdisc,kernel.value{it,jt}.dz_K_1_zeta(:),'displayName','analytisch')
				plot(zdisc,squeeze(dzK1(:,it,jt)),'r','DisplayName','numerisch')
			end
% 			plot(zdisc,kernel.value{it,jt}.dz_K_1_zeta(:,1),'displayName','analytisch')
% 			plot(zdisc,squeeze(dzK1(:,it,jt,1)),'r','DisplayName','numerisch')
% 			plot(zdisc,kernel.value{it,jt}.dz_K_1_zeta(:,end),'displayName','analytisch')
% 			plot(zdisc,squeeze(dzK1(:,it,jt,end)),'r','DisplayName','numerisch')
			legend('-dynamicLegend')
			id = id+1;
		end
	end
	figure('Name','Numerisches und analytisches dzetaK(z,0)')
	id =1;
	for it = 1:n
		for jt=1:n
			subplot(n,n,id)
			hold on
			grid on
			plot(zdisc,kernel.value{it,jt}.dzeta_K_z_0(:,1),'displayName','analytisch')
			plot(zdisc,squeeze(dzetaK0(:,it,jt,1)),'r','DisplayName','numerisch')
			plot(zdisc,kernel.value{it,jt}.dzeta_K_z_0(:,end),'displayName','analytisch')
			plot(zdisc,squeeze(dzetaK0(:,it,jt,end)),'r','DisplayName','numerisch')
			legend('-dynamicLegend')
			id = id+1;
		end
	end
end
time=toc;
fprintf('Finished! (%0.2fs)\n', time);

end

