function [controller,  controllerNumeric,  controllerAnalytical, distributedController] ...
			= compute_ppide_controller(obj, kernel, ppde, debug, target_BC) %#ok<INUSL>
%COMPUTE_PPIDE_CONTROLLER Compute state feedback controller for ppide_systems
%   
%	controller = ompute_ppide_controller(kernel, ppde)
%     compute the state feedback controller
%     K for usage in the control law
%       u = controller*x;
%     for coupled ppide_systems. The vector x contains the discretized values
%     of the different states as follows: x = [x1(0),  x1(h),  x1(2h), ... x2(0), 
%     x2(h), ...]^T,  where h is the discretization distance.
%
%   controller = compute_ppide_controller(kernel, ppde, debug) plots the comparison
%     of numerical and analytical derivatives
%
%   controller = compute_ppide_controller(kernel, ppde, debug, target_BC) uses the BC
%     stored in the boundary_operator target_BC for the target systems
%     actuated side
%
%   [~,  controllerNumeric] = compute_ppide_controller(kernel, ppde, ...) computes the
%     controller using a numerical derivative of the kernel
%
%   [~, ~, controllerAnalytical] = compute_ppide_controller(kernel, ppde, ...) gets the
%     analytical value of the state feedback (without turning it into a
%     matrix multiplication. (Attention: it also contains the boundary
%     feedback.)
%
%   [~,~,~,distributedController] = compute_ppide_controller(kernel,ppde,...) gets the
%     analytical value of the distributed state feedback (without boundary feedback)
%     ATTENTION: Only works correctly for Neumann Actuation! (TODO!)

% Created on 27.04.2017 by Simon Kerschbaum

%% Import and input checks
import misc.*
import numeric.*
import external.fem.*

if ~exist('debug', 'var')
	debug = 0;
end
n = size(kernel.value, 1);

%% Initialise parameter
% matrices of actuated boundary
tic
fprintf('Computing controller...')
Bd = ppde.b_op2.b_d;
Bn = ppde.b_op2.b_n;
Sd = ppde.b_op2.Sd;
Sr = ppde.b_op2.Sr;

if ~exist('target_BC','var')
	if isa(Bd,'function_handle')
		Bd_til = Sd*Sd.'; % ones for dirichlet states and zeros else.
	else
		if any(Bn(:)) % only if not Dirichlet!
			Bd_til = ~ppde.ctrl_pars.makeTargetNeumann * diag(diag(Bd))+Sd*Sd.';
		else
			Bd_til = Sd*Sd.';
		end
	end
	Bn_til = diag(diag(Bn));
else
	Bd_til = -target_BC.b_d;
	Bn_til = -target_BC.b_n;
end

numTKernel = size(kernel.value{1,1}.G,3);
if numTKernel >1
	timeDependent = 1;
else
	timeDependent = 0;
end
if timeDependent
	numTPlant = ppde.ndiscTime;
	tDiscPlant = linspace(0,1,numTPlant);
else
	numTPlant = 1;
end
if isa(Bd,'function_handle')
	BdPlant = permute(misc.eval_pointwise(Bd,tDiscPlant),[2 3 1]);
	BdKernel = permute(misc.eval_pointwise(Bd,linspace(0,1,numTKernel)),[2 3 1]);
else
% 	BdPlant = repmat(Bd,1,1,numTPlant);
% 	BdKernel = repmat(Bd,1,1,numTKernel);
	BdPlant = misc.translate_to_grid(Bd,{1:n,1:n,linspace(0,1,numTPlant)},'gridOld',...
		{1:n,1:n,linspace(0,1,size(Bd,3))});
	BdKernel = misc.translate_to_grid(Bd,{1:n,1:n,linspace(0,1,numTKernel)},'gridOld',...
		{1:n,1:n,linspace(0,1,size(Bd,3))});
end

zdisc = kernel.zdisc;
ndisc = length(zdisc);
ndiscPlant = ppde.ndisc;
zdiscPlant = linspace(0, 1, ndiscPlant);

% get mass matrix to implement integral as matrix multiplication
massMatrix = get_lin_mass_matrix(zdiscPlant);

% preallocations
rd(1:n,1:n*ndisc,numTKernel) = 0; 
rdPlant(1:n,1:n*ndiscPlant,numTPlant) = 0; 
rnPlant(1:n,1:n*ndiscPlant,numTPlant) = 0;
rnNumericPlant(1:n,1:n*ndiscPlant,numTPlant) = 0; 
rn(1:n,1:n*ndisc,numTKernel) = 0; 
rn2(1:n,1:n*ndisc,numTKernel) = 0; 
rnDist(1:n,1:n*ndisc,numTKernel) = 0;
rn2Plant(1:n,1:n*ndiscPlant,numTPlant) = 0; 
rn2Numeric(1:n,1:n*ndisc,numTKernel) = 0; 
rn2NumericPlant(1:n,1:n*ndiscPlant,numTPlant) = 0; 
rn3(1:n,1:n*ndisc,numTKernel) = 0; 
rn3Plant(1:n,1:n*ndiscPlant,numTPlant) = 0; 
rTemp(1:n,1:n*ndisc,numTKernel) = 0; 

controller = zeros(n, n*ndiscPlant, numTPlant);
controllerNumeric = zeros(n, n*ndiscPlant, numTPlant);
controllerAnalytical = zeros(n, n*ndisc,numTKernel);
% distributedController = zeros(n, n*ndisc,numTKernel);

% calculate numerical derivative for comparison
dzK_ij(1:ndisc,1:ndisc,numTKernel) = 0;
dzetaK(1:ndisc,1:ndisc,numTKernel) = 0;
dzK1(1:ndisc,1:n,1:n,numTKernel) = 0;
dzetaK0(1:ndisc,1:n,1:n,numTKernel) = 0;
for it=1:n
	for jt=1:n
		for zeta_idx =1:ndisc
			if zeta_idx < ndisc
				dzK_ij(zeta_idx:end,zeta_idx,:) = diff_num(zdisc(zeta_idx:end),kernel.value{it,jt}.K(zeta_idx:end,zeta_idx,:));
			end
			if zeta_idx>1
				dzetaK(zeta_idx,1:zeta_idx,:) = diff_num(zdisc(1:zeta_idx),kernel.value{it,jt}.K(zeta_idx,1:zeta_idx,:),2);
			end
		end
		dzK_ij(end,end,:) = dzK_ij(end,end-1,:); %cannot be computed numerically
		dzetaK(1,1,:) = dzetaK(2,1,:); %cannot be computed numerically
		dzK1(:,it,jt,:) = dzK_ij(end,:,:);
		dzetaK0(:,it,jt,:) = dzetaK(:,1,:);
	end
end

%% Calculate feedback gain
for it=1:n 
	for k=1:n
		rTemp(it,(k-1)*ndisc+1:k*ndisc,:) = kernel.value{it,k}.K(end,:,:);
% 		rTemp(it,k*ndisc) = rTemp(it,k*ndisc-1);
	end
end

% Dirichlet case -> rd
for it = 1:n % each state needs to be considered separately
	% calculate x_i(1) and dzx_i(1):
	if Bn_til(it,it) == 0 % dirichlet boundary condition
		for k=1:n
			rd(it,(k-1)*ndisc+1:k*ndisc,:) = rTemp(it,(k-1)*ndisc+1:k*ndisc,:);
			if timeDependent
				rdPlant(it,(k-1)*ndiscPlant+1:k*ndiscPlant,:) =  permute(multArray(translate_to_grid(permute(rd(it,(k-1)*ndisc+1:k*ndisc,:),[2 3 1]),{zdiscPlant,tDiscPlant}),massMatrix,1,1),[2 1]); % x_i(1,t)
			else
				rdPlant(it,(k-1)*ndiscPlant+1:k*ndiscPlant,:) =  permute(multArray(translate_to_grid(permute(rd(it,(k-1)*ndisc+1:k*ndisc,:),[2 3 1]),{zdiscPlant}),massMatrix,1,1),[2 1]); % x_i(1,t)
			end
		end
		rnPlant(it,1:n*ndiscPlant,:) = 0;
		rn(it,1:n*ndisc,:) = 0;
	else
		rdPlant(it,it*ndiscPlant,:) = 1; % x_i(1,t)
		rd(it,it*ndisc,:) = 1; % x_i(1,t)
	end
end
% Robin+Neumann case -> rn has to be calculated afterwards
for it=1:n
	if Bn_til(it,it) ~= 0 % robin or neumann boundary condition
		rd_sum = zeros(1,n*ndiscPlant,numTPlant);
		rd_sum_ana = zeros(1,n*ndisc,numTKernel);
		for k=1:n
			rn2(it,(k-1)*ndisc+1:k*ndisc,:) = kernel.value{it,k}.dz_K_1_zeta; % dzK(1,zeta)*x(zeta,t)d zeta
			rn2Numeric(it,(k-1)*ndisc+1:k*ndisc,:) = dzK1(:,it,k,:);
			rn3(it,(k-1)*ndisc+1:k*ndisc,:) = rTemp(it,(k-1)*ndisc+1:k*ndisc,:); %K(1,zeta)x(zeta,t)dzeta
			rd_sum_ana = rd_sum_ana + kernel.value{it,k}.K(end,end,:).*rd(k,:,:); %K(1,1)*x(1,t)
			if timeDependent
				rd_sum = rd_sum + permute(translate_to_grid(permute(kernel.value{it,k}.K(end,end,:),[3 1 2]),tDiscPlant),[2 3 1]).*rdPlant(k,:,:); %K(1,1)*x(1,t)
				rn2Plant(it,(k-1)*ndiscPlant+1:k*ndiscPlant,:) =  permute(multArray(translate_to_grid(permute(rn2(it,(k-1)*ndisc+1:k*ndisc,:),[2 3 1]),{zdiscPlant,tDiscPlant}),massMatrix,1,1),[2 1]);
				rn2NumericPlant(it,(k-1)*ndiscPlant+1:k*ndiscPlant,:) =  permute(multArray(translate_to_grid(permute(rn2Numeric(it,(k-1)*ndisc+1:k*ndisc,:),[2 3 1]),{zdiscPlant,tDiscPlant}),massMatrix,1,1),[2 1]);
				rn3Plant(it,(k-1)*ndiscPlant+1:k*ndiscPlant,:) =  permute(multArray(translate_to_grid(permute(rn3(it,(k-1)*ndisc+1:k*ndisc,:),[2 3 1]),{zdiscPlant,tDiscPlant}),massMatrix,1,1),[2 1]);
			else
				rd_sum = rd_sum + kernel.value{it,k}.K(end,end).*rdPlant(k,:,:); %K(1,1)*x(1,t)
				rn2Plant(it,(k-1)*ndiscPlant+1:k*ndiscPlant,:) =  permute(multArray(translate_to_grid(permute(rn2(it,(k-1)*ndisc+1:k*ndisc,:),[2 3 1]),{zdiscPlant}),massMatrix,1,1),[2 1]);
				rn2NumericPlant(it,(k-1)*ndiscPlant+1:k*ndiscPlant,:) =  permute(multArray(translate_to_grid(permute(rn2Numeric(it,(k-1)*ndisc+1:k*ndisc,:),[2 3 1]),{zdiscPlant}),massMatrix,1,1),[2 1]);
				rn3Plant(it,(k-1)*ndiscPlant+1:k*ndiscPlant,:) =  permute(multArray(translate_to_grid(permute(rn3(it,(k-1)*ndisc+1:k*ndisc,:),[2 3 1]),{zdiscPlant}),massMatrix,1,1),[2 1]);
			end
		end
		
		e_indiscT = zeros(1,n*ndiscPlant);
		e_indiscT(it*ndiscPlant) = 1; % x(1,t)
% 		e_indiscT_ana = zeros(1,n*ndisc);
% 		e_indiscT_ana(it*ndisc) = 1; % x(1,t)
		rnPlant(it,:,:) = rd_sum + rn2Plant(it,:,:)...
			- Bd_til(it,it)/Bn_til(it,it)*rdPlant(it,:,:)... % VORZEICHEN PRUEFEN!
			+ Bd_til(it,it)/Bn_til(it,it)*rn3Plant(it,:,:);
		rn(it,:,:) = rd_sum_ana + rn2(it,:,:)...
			- Bd_til(it,it)/Bn_til(it,it)*rd(it,:,:)... % VORZEICHEN PRUEFEN!
			+ Bd_til(it,it)/Bn_til(it,it)*rn3(it,:,:);
		rnNumericPlant(it,:,:) = rd_sum + rn2NumericPlant(it,:,:) - Bd_til(it,it)/Bn_til(it,it)*e_indiscT...
			+ Bd_til(it,it)/Bn_til(it,it)*rn3Plant(it,:,:);
		rnDist(it,:,:) = rn2(it,:,:)...
			+ Bd_til(it,it)/Bn_til(it,it)*rn3(it,:,:);
	end
end
%- Bd_til(it,it)/Bn_til(it,it)*rdPlant(it,:)...

% Combine Dirichlet and Robin+Neumann gains to one controller:
for it=1:n
	Bd_sum = zeros(1,n*ndiscPlant,numTPlant);
	Bd_sum_ana = zeros(1,n*ndisc,numTKernel);
	for k=1:n
		Bd_sum = Bd_sum + BdPlant(it,k,:).*rdPlant(k,:,:);
		Bd_sum_ana = Bd_sum_ana + BdKernel(it,k,:).*rd(k,:,:);
		% Only Bd requires a sum, because Bn must be diagonal!
	end
	controller(it,:,:) = Bn(it,it)*rnPlant(it,:,:) + Bd_sum; % total controller as matrix multiplication
	controllerAnalytical(it,:,:) = Bn(it,it)*rn(it,:,:) + Bd_sum_ana; %total controller analytical (without integral conversion)
% 	distributedController(it,:,:) = Bn(it,it)*rnDist(it,:,:); % distributed analytical controller
	controllerNumeric(it,:,:) = Bn(it,it)*rnNumericPlant(it,:,:) + Bd_sum; % with numeric derivtives
end
% actually, the whole controller calculation should be reworked with the new methos!
dzK1 = kernel.get_K('dz_K_1_zeta',2); % z i j t
K = kernel.get_K(); % z i j t             
K1 = shiftdim(K(end,:,:,:,:),1); 
								  %          i j * z i j t = i z j t
distributedControllerTemp = permute(misc.multArray(Bn,dzK1,2,2),[2 1 3 4])...
+ permute(misc.multArray(Bd_til,K1,2,2),[2 1 3 4]);
distributedController = reshape(permute(distributedControllerTemp,[2 1 3 4]),n, n*ndisc,[]);

% debug
if debug
	figure('Name', 'Numerisches und analytisches dzK(1, z)')
	id = 1;
	for it = 1:n
		for jt = 1:n
			subplot(n, n, id)
			hold on
			grid on
			for tIdx=1:size(kernel.value{it,jt}.dz_K_1_zeta,2)
				plot(zdisc,kernel.value{it,jt}.dz_K_1_zeta(:,tIdx),'displayName','analytisch')
				plot(zdisc,squeeze(dzK1(:,it,jt,tIdx)),'r','DisplayName','numerisch')
			end
% 			plot(zdisc,kernel.value{it,jt}.dz_K_1_zeta(:,1),'displayName','analytisch')
% 			plot(zdisc,squeeze(dzK1(:,it,jt,1)),'r','DisplayName','numerisch')
% 			plot(zdisc,kernel.value{it,jt}.dz_K_1_zeta(:,end),'displayName','analytisch')
% 			plot(zdisc,squeeze(dzK1(:,it,jt,end)),'r','DisplayName','numerisch')
			legend('-dynamicLegend')
			id = id+1;
		end
	end
	figure('Name','Numerisches und analytisches dzetaK(z,0)')
	id =1;
	for it = 1:n
		for jt=1:n
			subplot(n,n,id)
			hold on
			grid on
			plot(zdisc,kernel.value{it,jt}.dzeta_K_z_0(:,1),'displayName','analytisch')
			plot(zdisc,squeeze(dzetaK0(:,it,jt,1)),'r','DisplayName','numerisch')
			plot(zdisc,kernel.value{it,jt}.dzeta_K_z_0(:,end),'displayName','analytisch')
			plot(zdisc,squeeze(dzetaK0(:,it,jt,end)),'r','DisplayName','numerisch')
			legend('-dynamicLegend')
			id = id+1;
		end
	end
end
time=toc;
fprintf('Finished! (%0.2fs)\n', time);

end

