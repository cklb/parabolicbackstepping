function ppide = computeDualSystem(system,zdisc)
%computeDualSystem get the dual system needed for the observer kernel calculation
import misc.*
import numeric.*
import dps.parabolic.system.*
import dps.parabolic.control.backstepping.*
import dps.parabolic.tool.*

% locally store variables:
ndisc=length(zdisc);
zdisc_plant = linspace(0,1,system.ndiscPars);
ndisc_plant = system.ndiscPars;
% interpolate to correct resolution
% lambda_disc = interp1(zdisc_plant,system.l_disc,zdisc(:));
lambda_disc = system.l_disc;
% lambda_diff_disc = interp1(zdisc_plant,system.l_diff_disc,zdisc(:));
lambda_diff_disc = system.l_diff_disc;
lambda_obs = flip(flip(lambda_disc,1),4);
if size(lambda_obs,4)>1
	warning('Has not yet been checked, if lambda needs to be flipped in time for the observer!')
end
% a_disc = interp1(zdisc_plant,system.a_disc,zdisc(:));
a_disc = system.a_disc;
n = system.n;
% get discretized version of integral term
% F_disc = interp1(zdisc_plant,system.f_disc,zdisc(:));
% F_disc = permute(interp1(zdisc_plant,permute(F_disc,[2 1 3 4]),zdisc(:)),[2 1 3 4]);
F_disc = system.f_disc;
numtL = size(system.l_disc,4);
if numtL > 1
	% this can be removed, once implemented
	error('Implementation of time varying lambda needs implementation of time varying F for the dual kernel!')
end
numtA = size(system.a_disc,4);
numtNew = max(numtL, numtA);
if numtL>1 && numtA>1 && numtL ~= numtA
	% this case should never happen, because both parameters have resolution ndiscPars
	error('If time dependent, lambda and a must have the same resolution!')
end

% Hartwig edited
% if ndisc~= n
% 	if isa(system.ctrl_pars.mu_o,'function_handle') % function
% 		muHelp = eval_pointwise(system.ctrl_pars.mu_o,zdisc);
% 	elseif isequal(size(system.ctrl_pars.mu_o,1),ndisc) % is discretized 
% 		muHelp = system.ctrl_pars.mu_o;
% 	elseif isequal(size(system.ctrl_pars.mu_o,1),n) % constant matrix, two dimensions
% 		muHelp(1:ndisc,:,:) = repmat(shiftdim(system.ctrl_pars.mu_o,-1),ndisc,1,1);
% 	else % discretized matrix
% 		error('dimensions of mu_o not correct!')
% 	end
% else
% 	error('ndisc=n is a very bad choice!')
% end
tDisc = system.ctrl_pars.tDiscKernel;
if ndisc_plant~= n
	if isa(system.ctrl_pars.mu_o,'function_handle') % function
		if nargin(system.ctrl_pars.mu_o)==1
			muHelp = eval_pointwise(system.ctrl_pars.mu_o,zdisc_plant);
		else
			muHelp = zeros(ndisc_plant,n,n,length(tDisc));
			for z_idx=1:ndisc_plant
				muHelp(z_idx,:,:,:) = permute((eval_pointwise(@(t)system.ctrl_pars.mu_o(zdisc_plant(z_idx),t),tDisc)),[2 3 1]);
			end
		end
	elseif isequal(size(system.ctrl_pars.mu_o,1),length(system.ctrl_pars.zdiscCtrl)) % is discretized
		if length(size(system.ctrl_pars.mu_o))>3
			muInterpolant = interpolant({system.ctrl_pars.zdiscCtrl,1:n,1:n,linspace(0,1,size(system.ctrl_pars.mu_o,4))}, system.ctrl_pars.mu_o);
			muHelp = muInterpolant.evaluate(zdisc_plant,1:n,1:n,tDisc);
		else
			muInterpolant = interpolant({system.ctrl_pars.zdiscCtrl,1:n,1:n}, system.ctrl_pars.mu_o);
			muHelp = muInterpolant.evaluate(zdisc_plant,1:n,1:n);
		end
		%muHelp = ppide.ctrl_pars.mu;
	elseif isequal(size(system.ctrl_pars.mu_o,1),n) % constant matrix, two dimensions
		muHelp(1:ndisc_plant,:,:) = repmat(shiftdim(system.ctrl_pars.mu_o,-1),ndisc_plant,1,1);
	else % discretized matrix
		error('dimensions of mu not correct!')
	end
else
	error('ndisc_plant=n is a very bad choice!')
end
% mu_o = flip(muHelp,4);
numtMu = size(muHelp,4);
numtNewMu = max(numtL, numtMu);

% resample time to kernel resolution. needed because mu and a need same resolution.
% a_disc = misc.translate_to_grid(a_disc,{zdisc_plant,1:n,1:n,linspace(0,1,numtA)},'gridOld',{zdisc_plant,1:n,1:n,linspace(0,1,numtA)});
numtA = size(a_disc,4);
if size(F_disc,5)>1
	error('F noch nicht zeitabhaengig implementiert!')
	F_disc = misc.translate_to_grid(F_disc,{zdisc_plant,zdisc_plant,1:n,1:n,tdiscKernel},'gridOld',{zdisc_plant,zdisc_plant,1:n,1:n,linspace(0,1,size(F_disc,5))});
end

% mu_o = system.ctrl_pars.mu_o;
% Store system parameters in duality form:
% right dirichlet boundary matrix becomes left one
if isa(system.b_op2.b_d,'function_handle')
	% better: discretize here...
	BdrDisc = permute(misc.eval_pointwise(system.b_op2.b_d,tDisc),[2 3 1]);
	if any(misc.mydiag(BdrDisc-misc.makeDiag(misc.mydiag(BdrDisc,1:n,1:n)),1:n,1:n))
		error('Bdr needs to be a diagonal matrix for the observer!')
	end
	BdlObs = -flip(BdrDisc,3);
	for i=1:n
		if system.b_op2.b_n(i,i) == 0 % dirichlet
			BdlObs(i,i,:) = 1;
		end
	end
else
	BdlObs = zeros(n,n,size(system.b_op2.b_d,3));
	for i=1:n
		if system.b_op2.b_n(i,i) == 0 % dirichlet
			BdlObs(i,i,:) = 1;
		else
			BdlObs(i,i,:) = -flip(system.b_op2.b_d(i,i,:),3); % needs to be diagonal!
		end
	end
end
if size(BdlObs,3)>1
	BdlObs = misc.translate_to_grid(BdlObs,{1:n,1:n,tDisc},'gridOld',{1:n,1:n,linspace(0,1,size(BdlObs,3))});
end
if isa(system.b_op1.b_d,'function_handle')
	% better: discretize here...
	BdlDisc = permute(misc.eval_pointwise(system.b_op1.b_d,tDisc),[2 3 1]);
	if any(misc.mydiag(BdlDisc-misc.makeDiag(misc.mydiag(BdlDisc,1:n,1:n)),1:n,1:n))
		error('Bdr needs to be a diagonal matrix for the observer!')
	end
	BdrObs = -flip(BdlDisc,3);
	for i=1:n
		if system.b_op1.b_n(i,i) == 0 % dirichlet
			BdrObs(i,i,:) = 1;
		end
	end
else
	BdrObs = zeros(n,n,size(system.b_op1.b_d,3));
	for i=1:n
		if system.b_op1.b_n(i,i) == 0 % dirichlet
			BdrObs(i,i,:) = 1;
		else
			BdrObs(i,i,:) = -flip(system.b_op1.b_d(i,i,:),3); % needs to be diagonal!
		end
	end
end
if size(BdrObs,3)>1
	BdrObs = misc.translate_to_grid(BdrObs,{1:n,1:n,tDisc},'gridOld',{1:n,1:n,linspace(0,1,size(BdrObs,3))});
end


b_op1_obs = boundary_operator('z_b',0,'b_d',BdlObs,'b_n', system.b_op2.b_n);
b_op2_obs = boundary_operator('z_b',0,'b_d',BdrObs,'b_n', system.b_op1.b_n);
%b_op1_obs = boundary_operator('z_b',0,'b_d',Q0_obs,'b_n',eye(n));
% b_op2_obs = b_op1_obs; % right boundary operator wont be used but needs to be specified.
C0 =zeros(n,n);
Cd0=zeros(n,n);
for i=1:n
	C0(i,i) = system.b_op2.b_n(i,i);
	Cd0(i,i) = 1-system.b_op2.b_n(i,i);
end
Cm_op = dps.output_operator('c_0',C0,...
							'c_d0',Cd0);

% compute dual reaction matrix, not yet time dependent implemented!:
f_obs_temp = flip(flip(permute(F_disc,[2 1 4 3]),1),2);

a_obs = zeros(ndisc_plant,n,n,numtA); % preallocate
mu_obs = zeros(ndisc_plant,n,n,numtMu); % preallocate
f_obs = zeros(ndisc_plant,ndisc_plant,n,n); % preallocate
for z_idx  = 1:ndisc_plant
	for tIdx = 1:numtNew
		if numtA > 1
			tIdxA = tIdx;
		else
			tIdxA = 1;
		end
		if numtL > 1
			tIdxL = tIdx;
		else
			tIdxL = 1;
		end
		a_obs(z_idx,:,:,tIdx) = ...
			reshape(lambda_obs(z_idx,:,:,end-tIdxL+1),[n n])*...
			(reshape(a_disc(end-z_idx+1,:,:,end-tIdxA+1),[n n]).')...
			/squeeze(lambda_obs(z_idx,:,:,end-tIdxL+1));
	end
	for tIdx=1:numtNewMu
		if numtMu >1
			tIdxMu = tIdx;
		else
			tIdxMu = 1;
		end
		if numtL > 1
			tIdxL = tIdx;
		else
			tIdxL = 1;
		end
		mu_obs(z_idx,:,:,tIdxMu) = ...
			reshape(lambda_obs(z_idx,:,:,end-tIdxL+1),[n n])*...
			(reshape(muHelp(end-z_idx+1,:,:,end-tIdxMu+1),[n n]).')...
			/reshape(lambda_obs(z_idx,:,:,end-tIdxL+1),[n n]);
	end
	for zeta_idx=1:ndisc_plant
		f_obs(z_idx,zeta_idx,:,:) = squeeze(lambda_obs(z_idx))*squeeze(f_obs_temp(z_idx,zeta_idx,:,:)).'/squeeze(lambda_obs(zeta_idx,:,:));
	end
end

% store correct mu which is used by the kernel computation
obs_ctrl_pars = system.ctrl_pars;
% Hartwig edited
% mu_o_til = zeros(ndisc,n,n);
% for tidx = 1:size(mu_o,4)
% 	for it = 1:n
% 		for jt = 1:n
% 			mu_o_til(:,jt,it,tidx) = fliplr(reshape(mu_o(:,it,jt,tidx),[1,ndisc]));
% 		end
% 	end
% end
obs_ctrl_pars.mu = mu_obs;
obs_ctrl_pars.zdiscCtrl = zdisc;
% obs_ctrl_pars.mu = mu_o_til;

% create object to represent parameters for the determination of the
% kernel:
% Attention: Lambda needs to be flipped, if time dependent!
ppide = ppide_sys(...
				'l', lambda_obs,...
				'l_diff',-(flip(lambda_diff_disc,1)),...
				'a', a_obs,...
				'b_op1',b_op1_obs,...
				'b_op2',b_op2_obs,...
				'cm_op',Cm_op,...
				'b_1',zeros(n,n),... % Eing�nge sind egal!
				'b_2',eye(n,n),... % ein Eingang f�r jeden Zustand
				'ndiscPars',ndisc_plant,...
				'f',f_obs,...
				'a_0',@(z)zeros(n,n)+0*z,...
				'ctrl_pars',obs_ctrl_pars);

end

