function kern = check_pde( kern, ppde_sys )
%CHECK_PDE numerically check if the kernel fulfills the pde

% created on 19.09.2016 by Simon Kerschbaum
% modified on 26.04.2017 by SK:
%   - implementation for ppides
import misc.*
import numeric.*
import dps.parabolic.control.backstepping.*
if isa(kern,'ppde_kernel')
% old implementation
	n = size(kern.value,1);
	zdisc = kern.zdisc;
	ndisc = length(zdisc);
	% grad_op = get_grad_op(zdisc);

	% 1. K ueberpruefen:
	dz2K(n,n,ndisc,ndisc)=0; % preallocate
	dzeta2K(n,n,ndisc,ndisc)=0; % preallocate
	zdisc=acc(zdisc);
	for i =1:n
		for j=1:n
			alpha = kern.value{i,j}.alpha;
				for zeta_idx =1:ndisc				
					zeta = zdisc(zeta_idx);
					if i<j
						% Grenze: z = alpha*zeta;
						zgrenz = acc(alpha*zeta);
					elseif i>j
						% Grenze: z = alpha*zeta+1-alpha
						zgrenz = acc(alpha*zeta+1-alpha);
					else
						zgrenz = zeta; %existiert nur rechter Bereich
					end
					z1_idx_vec = zdisc >= zeta & zdisc < zgrenz - 1e-6; % linker Bereich, Abweichung wichtig, weil soinst vergleich auch mit acc nicht funktioniert..
					z1 = zdisc(z1_idx_vec); 
					z2_idx_vec = zdisc >= zgrenz + 1e-6; % rechter Bereich
					z2 = zdisc(z2_idx_vec);
					grad_op_z1 = get_grad_op(z1);
					grad_op_z2 = get_grad_op(z2);
					if zeta_idx == 3 && i==1 && j==2
						test=1;
					end
					dz2K(i,j,z1_idx_vec,zeta_idx) = grad_op_z1 * grad_op_z1 * kern.value{i,j}.K(z1_idx_vec,zeta_idx); % linker Bereich
					dz2K(i,j,z2_idx_vec,zeta_idx) = grad_op_z2 * grad_op_z2 * kern.value{i,j}.K(z2_idx_vec,zeta_idx); % rechter Bereich
				end
				for z_idx = 1:ndisc
					z = zdisc(z_idx);
					zetadisc = zdisc;
					if i<j
						zetagrenz = acc(z/alpha);
					elseif i>j
						zetagrenz = acc(1/alpha*(z-1+alpha));
					else
						zetagrenz = z; %nur unterer bereich.
					end
					zeta1_idx_vec = zetadisc <= z & zetadisc > zetagrenz + 1e-6;
					zeta1 = zetadisc(zeta1_idx_vec); % oberer Bereich
					zeta2_idx_vec = zetadisc <= zetagrenz -1e-6;
					zeta2 = zetadisc(zeta2_idx_vec); % unterer Bereich
					grad_op_zeta1 = get_grad_op(zeta1);
					grad_op_zeta2 = get_grad_op(zeta2);
					dzeta2K(i,j,z_idx,zeta1_idx_vec) = (grad_op_zeta1 * grad_op_zeta1 * kern.value{i,j}.K(z_idx,zeta1_idx_vec).').';
					dzeta2K(i,j,z_idx,zeta2_idx_vec) = (grad_op_zeta2 * grad_op_zeta2 * kern.value{i,j}.K(z_idx,zeta2_idx_vec).').';
				end		
			K(i,j,:,:) = kern.value{i,j}.K;
		end
	end
	errtemp(n,n,ndisc,ndisc)=0;
	for z_idx = 1:ndisc
		for zeta_idx =  1:z_idx
			errtemp(:,:,z_idx,zeta_idx) = ppde_sys.l*dz2K(:,:,z_idx,zeta_idx)...
					- dzeta2K(:,:,z_idx,zeta_idx)*ppde_sys.l ...
					- K(:,:,z_idx,zeta_idx)*ppde_sys.a(zdisc(zeta_idx)) ...
					- ppde_sys.ctrl_pars.mu*K(:,:,z_idx,zeta_idx);
			if max(max(abs(errtemp(:,:,z_idx,zeta_idx))))>1
				test=1;
			end
		end
	end

	% dem Kern zuweisen
	for i =1:n
		for j=1:n
			kern.value{i,j}.err = squeeze(errtemp(i,j,:,:));	
		end
	end


	% 2. G ueberpruefen:
	for i=1:n
		for j=1:n
			if j >= i
				s=1;
			else
				s=-1;
			end
			alpha = kern.value{i,j}.alpha;
			beta = (1-alpha)/(1+alpha);
			x2 = 1/2*(alpha-s*alpha)+1/2*(1+s); % = y2
			x3 = 1+alpha; 
			y3 = s*(1-alpha);
			xi_disc = kern.value{i,j}.xi;
			eta_disc = kern.value{i,j}.eta;
			dxietaG(1:length(xi_disc),1:length(eta_disc)) =0;
			% summe bilden
			Giksum = 0;
			Giksum(1:length(xi_disc),1:length(eta_disc)) = 0;

			for xi_idx =1:length(xi_disc)
				xi=xi_disc(xi_idx);
				for eta_idx =1:length(eta_disc)
					eta = eta_disc(eta_idx);
					if acc(eta)>=acc(s*beta*xi) && acc(eta)<= min(acc(xi),acc(x2 + (y3-x2)/(x3-x2)*(xi-x2)))
						zeta = kern.value{i,j}.get_zeta(xi,eta);
						Azeta = ppde_sys.a(zeta);
						for k =1:n
							Azeta_kj = Azeta(k,j);
							al_ik = kern.value{i,k}.alpha;
							xi_ik_disc = kern.value{i,k}.xi;
							eta_ik_disc = kern.value{i,k}.eta;
							[Xik,Eik] = meshgrid(xi_ik_disc,eta_ik_disc);
							[xi_ik, eta_ik] = kern.convert_xi(i,k,xi,eta,i,j,al_ik,alpha);
							Gik = interp2(Xik,Eik,kern.value{i,k}.G.',xi_ik,eta_ik);
							Giksum(xi_idx,eta_idx) = Giksum(xi_idx,eta_idx) + Gik*Azeta_kj;
						end
						rhs = 1/4/s/ppde_sys.l(i,i)*(Giksum + ppde_sys.ctrl_pars.mu(i,i)...
															*kern.value{i,j}.G(xi_idx,eta_idx));	
					else
						errG(xi_idx,eta_idx) = 0;
					end
				end
			end

			% Ableigun Gxieta bilden:
			for eta_idx = 1:length(eta_disc)
				eta= eta_disc(eta_idx);
				% Bereich eingrenzen:
				% xi l�uft von eta bis 2-eta
				xi_val = xi_disc(acc(xi_disc)>=acc(eta)&acc(xi_disc)<=acc(2-eta));
				xi_val_idx_vec = find(acc(xi_disc)>=acc(eta)&acc(xi_disc)<=acc(2-eta));
				grad_op_xi = get_grad_op(xi_val);
				if length(grad_op_xi)>1
					Gxi(xi_val_idx_vec,eta_idx) = grad_op_xi * kern.value{i,j}.G(xi_val_idx_vec,eta_idx);			
				else
					Gxi(xi_val_idx_vec,eta_idx) = 0;
				end
			end
			Gxieta = 0;
			for xi_idx = 1:length(xi_disc)
				xi = xi_disc(xi_idx);
				% eta l�uft von s*beta*xi bis eta
				eta_val = eta_disc(acc(eta_disc)>=acc(s*beta*xi)&acc(eta_disc)<=acc(eta));
				eta_val_idx_vec = find(acc(eta_disc)>=acc(s*beta*xi)&acc(eta_disc)<=acc(eta));
				grad_op_eta = get_grad_op(eta_val);
				if length(grad_op_eta)>1
					Gxieta(xi_idx,eta_val_idx_vec) = (grad_op_eta * Gxi(xi_idx,eta_val_idx_vec).').';			
				else
					Gxieta(xi_idx,eta_val_idx_vec) = 0;
				end
			end


			kern.value{i,j}.errG = Gxieta-rhs;
		end

	end

elseif isa(kern,'ppide_kernel')
	if kern.checked_PDE == 0
		timer = tic;
		fprintf('Checking PDE: ')
	% 	reverseStr = '';
	% 	msg2='';
	% 	run=0;
		% new implementation
		n = size(kern.value,1);
		zdisc = kern.zdisc;
		zdisc_plant = linspace(0,1,ppde_sys.ndiscPars);
		ndisc = length(zdisc);
		numT = size(kern.value{1,1}.xi,2);
		errtemp(n,n,ndisc,ndisc,numT)=0; % preallocate
		rhs_e(n,n,ndisc,ndisc,numT)=0; % preallocate
		% Hartwig edited
		if ndisc~= n
			if isa(ppde_sys.ctrl_pars.mu,'function_handle') % function
				muHelp = eval_pointwise(ppde_sys.ctrl_pars.mu,zdisc);
			elseif isequal(size(ppde_sys.ctrl_pars.mu,1),ndisc) % is discretized
				muHelp = ppde_sys.ctrl_pars.mu;
			elseif isequal(size(ppde_sys.ctrl_pars.mu,1),n) % constant matrix, two dimensions
				muHelp(1:ndisc,:,:) = repmat(shiftdim(ppde_sys.ctrl_pars.mu,-1),ndisc,1,1);
			else % discretized matrix
				error('dimensions of mu not correct!')
			end
		else
			error('ndisc=n is a very bad choice!')
		end
		mu = muHelp;
		if size(mu,4)>1
			error('check_pde only implemented for constant mu')
		end
		for tIdx = 1 %:numT
			varlist = {'c_disc','c_diff_disc','a_disc','l_disc'};
			for nameIdx = 1:length(varlist)
				if size(ppde_sys.(varlist{nameIdx}),4)>1
					pars.(varlist{nameIdx}) = interp1(zdisc_plant,ppde_sys.(varlist{nameIdx})(:,:,:,tIdx),zdisc(:));
				else
					pars.(varlist{nameIdx}) = interp1(zdisc_plant,ppde_sys.(varlist{nameIdx}),zdisc(:));
				end
				eval([varlist{nameIdx} '= pars.(varlist{nameIdx});']);
			end
			% get discretized version of integral term
			if nargin(ppde_sys.f) == 2
				% z zeta i j 
				f_disc = ppde_sys.f.on({zdisc,zdisc});
			elseif nargin(ppde_sys.f)==3
				% z zeta i j t
				error('Check_pde only implemented for constant F!')
				f_disc = permute(ppde_sys.f.on({zdisc,zdisc,tDisc}),[1 2 4 5 3]);
			end

			% 1. check K: % TODO: an Konvektionsterm anpassen!
			K(1:n,1:n,1:ndisc,1:ndisc)=0; % preallocate
			K_t(1:n,1:n,1:ndisc,1:ndisc)=0; % preallocate
			dz2K(1:n,1:n,1:ndisc,1:ndisc)=0; % preallocate
			dzK_temp(1:n,1:n,1:ndisc,1:ndisc)=0; % preallocate
			dzeta2K(1:n,1:n,1:ndisc,1:ndisc)=0; % preallocate
			dzetaK(1:n,1:n,1:ndisc,1:ndisc)=0; % preallocate
			for i =1:n
				for j=1:n
					kernel_element = kern.value{i,j};
					min_dist = kernel_element.min_dist;
					for zeta_idx =1:ndisc % calculate derivative in z-direction line-wise				
						zeta = zdisc(zeta_idx);				
						if kernel_element.lambda_i(1) > kernel_element.lambda_j(1)
							xi_grenz = 2*kernel_element.phi_j(zeta_idx,tIdx);
							if acc(1/2*xi_grenz,1/min_dist) >= acc(kernel_element.phi_i(end,tIdx),1/min_dist)
								% in this case, there is no sparation line
								zgrenz=1;
							else
								zgrenz = kernel_element.f_i_disc(get_idx(acc(1/2*xi_grenz,1/min_dist),acc(kernel_element.phi_i(:,tIdx),1/min_dist)));
							end
						elseif kernel_element.lambda_i(1) < kernel_element.lambda_j(1)
							xi_grenz = -2*kernel_element.phi_j(zeta_idx,tIdx)+2*kernel_element.phi_j(end,tIdx);
							% for i>j there is always a separating z!
							zgrenz = kernel_element.f_i_disc(get_idx(acc(-1/2*xi_grenz+kernel_element.phi_i(end,tIdx),1/min_dist),acc(kernel_element.phi_i(:,tIdx),1/min_dist)));
						else
							zgrenz = zeta; %there is only the "right-hand" area.
						end
						z1_idx_vec = acc(zdisc) >= acc(zeta) & acc(zdisc) < acc(zgrenz); % left area
						z2_idx_vec = acc(zdisc) >= acc(zeta) & acc(zdisc) >= acc(zgrenz); % right area
						% derivative in left area
						dzK_temp(i,j,z1_idx_vec,zeta_idx) = diff_num(zdisc(z1_idx_vec),kern.value{i,j}.K(z1_idx_vec,zeta_idx,tIdx));
						dz2K(i,j,z1_idx_vec,zeta_idx) = diff_num(zdisc(z1_idx_vec),squeeze(dzK_temp(i,j,z1_idx_vec,zeta_idx)));
						% derivative right area
						dzK_temp(i,j,z2_idx_vec,zeta_idx) = diff_num(zdisc(z2_idx_vec),kern.value{i,j}.K(z2_idx_vec,zeta_idx,tIdx));
						dz2K(i,j,z2_idx_vec,zeta_idx) = diff_num(zdisc(z2_idx_vec),squeeze(dzK_temp(i,j,z2_idx_vec,zeta_idx)));
					end
					for z_idx = 1:ndisc % calculate derivative in zeta-direction column-wise
						z = zdisc(z_idx);
						zetadisc = zdisc;
						if kernel_element.lambda_i(1) > kernel_element.lambda_j(1)
							% in this case, there is always a separating zeta!
							xi_grenz = 2*kernel_element.phi_i(z_idx,tIdx);
							zetagrenz = kernel_element.f_j_disc(get_idx(acc(1/2*xi_grenz,1/min_dist),kernel_element.phi_j(:,tIdx)));
						elseif kernel_element.lambda_i(1) < kernel_element.lambda_j(1)
							xi_grenz = -2*kernel_element.phi_i(z_idx,tIdx)+2*kernel_element.phi_i(end,tIdx);
							if acc(-1/2*xi_grenz+kernel_element.phi_j(end,tIdx),1/min_dist) <= acc(kernel_element.phi_j(1,tIdx),1/min_dist)
							% there is no separating zeta in this area:
								zetagrenz = 0; 
							else
								zetagrenz = kernel_element.f_j_disc(get_idx(acc(-1/2*xi_grenz+kernel_element.phi_j(end,tIdx),1/min_dist),kernel_element.phi_j(:,tIdx)));
							end
						else
							zetagrenz = z; % only the "lower" area is existing
						end
						zeta1_idx_vec = acc(zetadisc) <= acc(z) & acc(zetadisc) > acc(zetagrenz); % upper area
						zeta2_idx_vec = acc(zetadisc) <= acc(z) & acc(zetadisc) <= acc(zetagrenz); % lower area
						% derivative in upper area
						dzetaK(i,j,z_idx,zeta1_idx_vec) = diff_num(zdisc(zeta1_idx_vec),kern.value{i,j}.K(z_idx,zeta1_idx_vec,tIdx).').';
						dzeta2K(i,j,z_idx,zeta1_idx_vec) = diff_num(zdisc(zeta1_idx_vec),squeeze(dzetaK(i,j,z_idx,zeta1_idx_vec))).';
						% derivative in lower area
						dzetaK(i,j,z_idx,zeta2_idx_vec) = diff_num(zdisc(zeta2_idx_vec),kern.value{i,j}.K(z_idx,zeta2_idx_vec,tIdx).').';
						dzeta2K(i,j,z_idx,zeta2_idx_vec) = diff_num(zdisc(zeta2_idx_vec),squeeze(dzetaK(i,j,z_idx,zeta2_idx_vec))).';
					end %z_idx		
					K(i,j,:,:) = kern.value{i,j}.K(:,:,tIdx);
					K_t(i,j,:,:) = kern.value{i,j}.K_t(:,:,tIdx);
				end % j
			end % i


			for z_idx = 1:ndisc
				for zeta_idx =  1:z_idx
					zeta = zdisc(zeta_idx);
					zeta_plant_idx = get_idx(zeta,zdisc_plant);
					zeta_s_vec = zdisc(zeta_idx:z_idx);
					zeta_s_idx_vec = zeta_idx:z_idx;
					% calculate product for integration
					temp_prod = 0; % reset size
					temp_prod(1:n,1:n,1:length(zeta_s_vec)) = 0; % preallocate
					for zeta_s_idx =1:length(zeta_s_vec)
						temp_prod(:,:,zeta_s_idx) = K(:,:,z_idx,zeta_s_idx_vec(zeta_s_idx))*squeeze(f_disc(zeta_s_idx_vec(zeta_s_idx),zeta_idx,:,:));
					end
					integral = trapz(zdisc(zeta_idx:z_idx),temp_prod,3);
					try
					% calculate rhs separately, to get relative error
					rhs_e(:,:,z_idx,zeta_idx,tIdx) = - K(:,:,z_idx,zeta_idx)*squeeze(a_disc(zeta_idx,:,:)) ...
							- reshape(mu(z_idx,:,:),[n,n])*K(:,:,z_idx,zeta_idx)...
							+ squeeze(f_disc(z_idx,zeta_idx,:,:))...
							- integral...
							+ dzetaK(:,:,z_idx,zeta_idx)*squeeze(c_disc(zeta_idx,:,:)) ...
							+ K(:,:,z_idx,zeta_idx)*squeeze(c_diff_disc(zeta_idx,:,:)) ...
							+ squeeze(c_disc(z_idx,:,:))*dzK_temp(:,:,z_idx,zeta_idx)...
							- K_t(:,:,z_idx,zeta_idx);
					% calculate error = LHS-RHS
					% l_diff_disc must be resampled to kernel resolution
					% and l_diff2_disc
					errtemp(:,:,z_idx,zeta_idx,tIdx) = squeeze(l_disc(z_idx,:,:))*dz2K(:,:,z_idx,zeta_idx)...
							- dzeta2K(:,:,z_idx,zeta_idx)*squeeze(l_disc(zeta_idx,:,:)) ...
							- 2*dzetaK(:,:,z_idx,zeta_idx)*squeeze(ppde_sys.l_diff_disc(zeta_plant_idx,:,:))...
							- K(:,:,z_idx,zeta_idx)*squeeze(ppde_sys.l_diff2_disc(zeta_plant_idx,:,:))...
							+ rhs_e(:,:,z_idx,zeta_idx,tIdx);
					catch errmsg
						warning(mythrow(errmsg));
					end
				end % zeta_idx
			end % z_idx
		end
			% write relative error to kernel object
			for i =1:n
				for j=1:n
					for tIdx=1 %:numT
						kern.value{i,j}.err(:,:,tIdx) = squeeze(errtemp(i,j,:,:,tIdx))/max(max(abs(squeeze(rhs_e(i,j,:,:,tIdx)))));	
					end
				end
			end
% 		for tIdx=1:numT
% 			%%% end of checking K-PDE
% 			% 2. check G-PDE:
% 			for i=1:n
% 				for j=1:n
% 					kernel_element = kern.value{i,j};
% 					s = kernel_element.s;
% 					xi_disc = kern.value{i,j}.xi;
% 					eta_disc = kern.value{i,j}.eta;
% 					eta_fine = kern.value{i,j}.eta_fine;
% 					a = (s+1)/2 *kernel_element.phi_i(end) - (s-1)/2*kernel_element.phi_j(end); % top of area
% 					rhs = 0;
% 					rhs(1:length(xi_disc),1:length(eta_disc))=0;
% 					for xi_idx =1:length(xi_disc)
% 						xi=xi_disc(xi_idx);
% 						for eta_idx =1:length(eta_disc)
% 							eta = eta_disc(eta_idx);
% 							eta_l = kernel_element.eta_l(xi_idx);
% 							if acc(eta,1/min_dist)>=acc(eta_l,1/min_dist) && acc(eta,1/min_dist)<= min(acc(xi,1/min_dist),acc(2*a-xi,1/min_dist))
% 								[z,zeta] = kern.value{i,j}.get_z(xi,eta);
% 								zeta_idx = get_idx(zeta,zdisc);
% 								z_idx = get_idx(z,zdisc);
% 
% 								Fsum = 0; % reset size
% 								Giksum = 0;
% 								for k =1:n
% 									Azeta_kj = a_disc(zeta_idx,k,j);
% 									xi_ik_disc = kern.value{i,k}.xi;
% 									eta_ik_disc = kern.value{i,k}.eta;
% 									[xi_ik, eta_ik] = kern.convert_xi(i,k,xi,eta,i,j);
% 									Gik = kern.value{i,k}.G(get_idx(xi_ik,xi_ik_disc),get_idx(eta_ik,eta_ik_disc));
% 									Giksum = Giksum + kern.value{k,j}.f(zeta_idx)*Gik*Azeta_kj;
% 
% 									zeta_s_idx_vec = zeta_idx:z_idx;
% 									zeta_s_vec = zdisc(zeta_s_idx_vec);
% 									if ~isequal(f_disc,zeros(ndisc,ndisc,n,n))
% 										Fkj_disc = f_disc(:,:,k,j);
% 										Fkj_zetas_zeta = mydiag(Fkj_disc,zeta_s_idx_vec,repmat(zeta_idx,1,length(zeta_s_idx_vec)));
% 										try
% 											[xi_ik_z_zetas,eta_ik_z_zetas] =...
% 											kern.value{i,k}.get_xi(repmat(z,1,length(zeta_s_vec)),zeta_s_vec);
% 										catch errmsg
% 											warning(mythrow(errmsg));
% 										end
% 										xi_ik_z_zetas_idx = get_idx(xi_ik_z_zetas,xi_ik_disc);
% 										eta_ik_z_zetas_idx = get_idx(eta_ik_z_zetas,eta_ik_disc);
% 										try
% 											Fsum = Fsum + ...
% 											trapz_fast(...
% 											zeta_s_vec,...
% 											kern.value{k,j}.g(zeta_idx,zeta_s_idx_vec).'/4/s...
% 											.*mydiag(kern.value{i,k}.G,xi_ik_z_zetas_idx,eta_ik_z_zetas_idx)...
% 											.*Fkj_zetas_zeta);	
% 										catch errmsg
% 											warning(mythrow(errmsg));
% 										end
% 									end
% 								end
% 								% RHS of the PDE
% 								if ppde_sys.ctrl_pars.eliminate_convection
% 									rhs(xi_idx,eta_idx) = 1/4/s*(Giksum...
% 												+ ppde_sys.ctrl_pars.mu(i,i)*kern.value{i,j}.G(xi_idx,eta_idx)...
% 												- kern.value{i,j}.d(z_idx,zeta_idx)*kern.value{i,j}.G(xi_idx,eta_idx)...
% 												- kern.value{i,j}.e(z_idx,zeta_idx)*f_disc(z_idx,zeta_idx,i,j)...
% 												+ Fsum);	
% 								else
% 									rhs(xi_idx,eta_idx) = 1/4/s*(Giksum...
% 											+ ppde_sys.ctrl_pars.mu(i,i)*kern.value{i,j}.G(xi_idx,eta_idx)...
% 											- kern.value{i,j}.d(z_idx,zeta_idx)*kern.value{i,j}.G(xi_idx,eta_idx)...
% 											- kern.value{i,j}.e(zeta_idx)*f_disc(z_idx,zeta_idx,i,j)...
% 											+ Fsum);	
% 								end
% 							else
% 								rhs(xi_idx,eta_idx) = 0;						
% 							end % in spatial domain
% 						end % eta_idx
% 					end % xi_idx
% 
% 					% calculate derivatives of G
% 					Gxi = 0; % reset size
% 					Gxi(1:length(xi_disc),1:length(eta_disc)) = 0;
% 					for eta_idx = 1:length(eta_disc)
% 						eta= eta_disc(eta_idx);
% 						% take only valid area
% 						if eta>= 0
% 							% xi from eta to 2*a-eta
% 							xi_val = xi_disc(acc(xi_disc,1/min_dist)>=acc(eta,1/min_dist) & acc(xi_disc,1/min_dist)<=acc(2*a-eta,1/min_dist));
% 							xi_val_idx_vec = find(acc(xi_disc,1/min_dist)>=acc(eta,1/min_dist)&acc(xi_disc,1/min_dist)<=acc(2*a-eta,1/min_dist));
% 						else
% 							% xi from xi_l to 2*a-eta
% 							xi_l = kernel_element.xi_l(get_idx(eta,kernel_element.eta_l));
% 							xi_l_idx = get_idx(xi_l,xi_disc);
% 							xi_h_idx = find(acc(xi_disc,1/min_dist)<=acc(2*a-eta,1/min_dist),1,'last');
% 							xi_val = xi_disc(xi_l_idx:xi_h_idx);
% 							xi_val_idx_vec = xi_l_idx:xi_h_idx;
% 						end
% 						Gxi(xi_val_idx_vec,eta_idx) = diff_num(xi_val,kern.value{i,j}.G(xi_val_idx_vec,eta_idx));			 			
% 					end 
% 					Gxieta = 0; % reset size
% 					Gxieta(1:length(xi_disc),1:length(eta_disc)) = 0;
% 					for xi_idx = 1:length(xi_disc)
% 						xi = xi_disc(xi_idx);
% 						% eta from eta_l to min(xi,2*a-xi)
% 						% use fine grid for starting and end point! It may be that
% 						% starting and end point are not existing in sparse grid!
% 						eta_fine_val = eta_fine(acc(eta_fine,1/min_dist)>= acc(kernel_element.eta_l(xi_idx),1/min_dist) & acc(eta_fine,1/min_dist)<=acc(min(xi,2*a-xi),1/min_dist));
% 						eta_val_idx_vec = get_idx(eta_fine_val(1),eta_disc):get_idx(eta_fine_val(end),eta_disc);
% 						eta_val = eta_disc(eta_val_idx_vec);
% 						Gxieta(xi_idx,eta_val_idx_vec) = diff_num(eta_val,Gxi(xi_idx,eta_val_idx_vec).').';			
% 					end
% 					% write to object
% 					kern.value{i,j}.errG = (Gxieta-rhs)/max(max(abs(rhs)));
% 				end % j
% 			end % i
% 		
% 		end
		timeelapsed = toc(timer);
		fprintf(['...done.(' num2str(round(timeelapsed,2)) ' s)\n'])
		kern.checked_PDE = 1;
	else
		disp('kernel PDE already checked')
	end
else % ppide_kernel
	error('kern must be an object of class ppde_kernel or ppide_kernel')
end
end























