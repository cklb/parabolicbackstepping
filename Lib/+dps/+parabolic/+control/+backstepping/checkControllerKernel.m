function test = checkControllerKernel(ppide, controllerKernel, plotResult)
% checkControllerKernel inserts the previously calculated controller-kernel
% into its kernel equations and prints the residuals, to give some insight
% if they are correct or not. Note that the residual of the pde is usually
% relatively big, because numerical derivatives are used, which are very
% inaccurate. If one is uncertain about the interpretation of the numbers,
% having a look at the plots of the distributed residuals might be helpful.
	if nargin < 3
		plotResult = false;
	end
	%% Read parameter
	n = ppide.n;
	disc.n = ppide.ndiscPars;
	disc.z = linspace(0,1,disc.n);
	disc.dz = disc.z(2) - disc.z(1);
	disc.Lambda = ppide.l;
	disc.Lambda_dz = ppide.l_diff;
	disc.Lambda_ddz = ppide.l_diff2;
	disc.A = ppide.a;
	
	K = controllerKernel.get_K;
	for it = 1:n
		for jt = 1:n
			K(:,:,it,jt) = inpaint_nans(K(:,:,it,jt));
		end
	end
	test.K.value = misc.translate_to_grid(K, disc.z, 'zdim', 2, 'method', 'spline');
	[test.K.dzeta, test.K.dz] = numeric.gradient_on_2d_triangular_domain(test.K.value , disc.dz);
	[test.K.ddzeta, ~] = numeric.gradient_on_2d_triangular_domain(test.K.dzeta, disc.dz);
	[~, test.K.ddz] = numeric.gradient_on_2d_triangular_domain(test.K.dz, disc.dz);
	% Hartwig edited
	if disc.n~= n
		zdisc = linspace(0,1,disc.n);
		if isa(ppide.ctrl_pars.mu,'function_handle') % function
			muHelp = misc.eval_pointwise(ppide.ctrl_pars.mu,zdisc);
		elseif isequal(size(ppide.ctrl_pars.mu,1),disc.n) % is discretized 
			muHelp = ppide.ctrl_pars.mu;
		elseif isequal(size(ppide.ctrl_pars.mu,1),n) % constant matrix, two dimensions
			muHelp(1:disc.n,:,:) = repmat(shiftdim(ppide.ctrl_pars.mu,-1),disc.n,1,1);
		else % discretized matrix
			error('dimensions of mu not correct!')
		end
	else
		error('ndiscPars=n is a very bad choice!')
	end
	mu = muHelp;
	%mu = ppide.ctrl_pars.mu;
	
	%% PDE
	% Lambda(z) K_ddz(z, zeta) - (K(z, zeta) Lambda(zeta))_ddzeta - K(z, zeta) (A(zeta) + mu I) = 0
	test.K.pde = zeros(disc.n, disc.n, n, n); 
	for zIdx = 2:disc.n-1
		for zetaIdx = 2:zIdx-1
			test.K.pde(zIdx, zetaIdx, :, :) = ...
				reshape(disc.Lambda(zIdx,:,:), [n, n]) * reshape(test.K.ddz(zIdx, zetaIdx, :, :), [n, n]) ...
				- reshape(test.K.ddzeta(zIdx, zetaIdx, :, :), [n, n]) * reshape(disc.Lambda(zetaIdx,:,:), [n, n]) ...
				- 2 * reshape(test.K.dzeta(zIdx, zetaIdx, :, :), [n, n]) * reshape(disc.Lambda_dz(zetaIdx,:,:), [n, n]) ...
				- reshape(test.K.value(zIdx, zetaIdx, :, :), [n, n]) * reshape(disc.Lambda_ddz(zetaIdx,:,:), [n, n]) ...
				- reshape(test.K.value(zIdx, zetaIdx, :, :), [n, n]) * (reshape(disc.A(zetaIdx,:,:), [n, n]) + reshape(mu(zIdx,:,:),[n,n]) * eye(n));
		end
	end
	
	%% BC at z=0
	% K(z, 0) Lambda(0) + A0_tilde(z) = 0
	A0_tilde = misc.translate_to_grid(controllerKernel.a0_til, disc.z);
	test.K.bc.zeta0 = zeros(disc.n, n, n);
	for jt = 1:n
		test.K.bc.zeta0(:,:,jt) = reshape(test.K.value(:,1,:,jt), [disc.n, n]) * disc.Lambda(1,jt,jt);
	end
	test.K.bc.zeta0 = test.K.bc.zeta0 + A0_tilde;		
	
	%% Is A0_tilde lower triangular?
	test.A0_tilde_upper = 0;
	numberOfA0Elements = 1;
	for it = 1:n
		for jt = it:n
			test.A0_tilde_upper = test.A0_tilde_upper + mean(abs(A0_tilde(:, it, jt)));
			numberOfA0Elements = numberOfA0Elements+1;
		end
	end
	test.A0_tilde_upper = test.A0_tilde_upper / numberOfA0Elements;
	
	%% BC at z=zeta (algebraic)
	% K(z, z) Lambda(z) - Lambda(z) K(z, z) = 0
	test.K.zEqualZeta = misc.diag_nd(test.K.value);
	test.K.bc.zzeta.algebraic = zeros(disc.n, n, n);
	for zIdx = 1:disc.n
		test.K.bc.zzeta.algebraic(zIdx, :, :) = ...
			reshape(test.K.zEqualZeta(zIdx, :, :), [n, n]) * reshape(disc.Lambda(zIdx, :, :), [n, n]) ...
			- reshape(disc.Lambda(zIdx, :, :), [n, n]) * reshape(test.K.zEqualZeta(zIdx, :, :), [n, n]);
	end
	
	%% BC at z=zeta (pde)
	% Lambda(z) (dz K(z, z) + K_dz(z, z)) + K_dzeta(z, z) Lambda(z) ...
	%		+ K(z, z) Lambda_dz(z) + A(z) + mu I = 0
	if n == 1
		test.K.zEqualZetaTotalDiff = numeric.diff_num(disc.z.', test.K.zEqualZeta);
	else
		[~, test.K.zEqualZetaTotalDiff] = gradient(test.K.zEqualZeta, disc.dz);
	end
	test.K.zEqualZeta_dz = misc.diag_nd(test.K.dz);
	test.K.zEqualZeta_dzeta = misc.diag_nd(test.K.dzeta);
	test.K.bc.zzeta.pde = zeros(disc.n, n, n);
	for zIdx = 1:disc.n
		test.K.bc.zzeta.pde(zIdx, :, :) = ...
			reshape(disc.Lambda(zIdx, :, :), [n, n]) * ...
				(reshape(test.K.zEqualZetaTotalDiff(zIdx, :, :), [n, n]) ...
				+ reshape(test.K.zEqualZeta_dz(zIdx, :, :), [n, n])) ...
			+ reshape(test.K.zEqualZeta_dzeta(zIdx, :, :), [n, n]) * ...
				reshape(disc.Lambda(zIdx, :, :), [n, n]) ...
			+ reshape(test.K.zEqualZeta(zIdx, :, :), [n, n]) * ...
				reshape(disc.Lambda_dz(zIdx, :, :), [n, n]) ...
			+ reshape(disc.A(zIdx, :, :), [n, n]) ...
			+ reshape(mu(zIdx,:,:),[n,n]) * eye(n);
	end
	
	%% BC at z=zeta (solution of pde) for i = 1, 2, ..., n
	% K_ii(z, z) = K_ii(0, 0) * sqrt(lambda_i(0)/lambda_i(z)) ...
	%		- 1/sqrt(lambda_i(z)) * ...
	%			int_0^z (A_ii(zeta) + mu_c) / (2 * sqrt(lambda_i(zeta))) dzeta
	test.K.bc.zzeta.integralSolution = zeros(disc.n, n);
	test.K.bc.zzeta.numericSolution = zeros(disc.n, n);
	for it = 1:n
		test.K.bc.zzeta.numericSolution(:, it) = test.K.zEqualZeta(:, it, it);
		test.K.bc.zzeta.integralSolution(:, it) = K(1, 1, it, it) *sqrt(disc.Lambda(1, it, it) ./ disc.Lambda(:, it, it) )...
			- (1./sqrt(disc.Lambda(:, it, it))) .* ...
			numeric.cumtrapz_fast(disc.z, reshape((disc.A(:, it, it) + mu(:,it, it)) ./ (2*sqrt(disc.Lambda(:, it, it))), [1, disc.n])).';
	end
	
	fprintf(['\nAbsolut average of K-PDE-error: ', ...
		num2str(mean(abs(test.K.pde(:)))), '\n']);
	fprintf(['Absolut average of K-BC-error at zeta=0: ', ...
		num2str(mean(abs(test.K.bc.zeta0(:)))), '\n']);
	fprintf(['Absolut average of upper triangular elements of A0_tilde: ', ...
		num2str(test.A0_tilde_upper), '\n']);
	fprintf(['Absolut average of K-BC-error at z=zeta of algebraic bc: ', ...
		num2str(mean(abs(test.K.bc.zzeta.algebraic(:)))), '\n']);
	fprintf(['Absolut average of K-BC-error at z=zeta of pde bc: ', ...
		num2str(mean(abs(test.K.bc.zzeta.pde(:)))), '\n']);
	fprintf(['Absolut average of K-BC-error at z=zeta of solution of pde and\n', ...
		'        successive approximation are compared: ', ...
		num2str(mean(abs(test.K.bc.zzeta.numericSolution(:)-test.K.bc.zzeta.integralSolution(:)))), '\n\n']);
	
	%% plots?
	if plotResult
		dps.hyperbolic.plot.matrix_4D(test.K.pde, 'hide_upper_triangle', false);
		dps.hyperbolic.plot.matrix_3D(test.K.bc.zeta0, 'title', 'K-BC at zeta = 0');
		dps.hyperbolic.plot.matrix_3D(test.K.bc.zzeta.algebraic, 'title', 'K-BC algebraic at zeta = z');
		dps.hyperbolic.plot.matrix_3D(test.K.bc.zzeta.pde, 'title', 'K-BC pde at zeta = z');
		dps.hyperbolic.plot.matrix_3D(test.K.bc.zzeta.integralSolution-test.K.bc.zzeta.numericSolution, 'title', 'K-BC integral-fixpoint solution at zeta = z');
	end
end