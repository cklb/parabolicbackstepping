function checkObserverKernel(ppide, observerKernel, plotResult)
% checkObserverKernel inserts the previously calculated observer-kernel
% into its kernel equations and prints the residuals, to give some insight
% if they are correct or not. Note that the residual of the pde is usually
% relatively big, because numerical derivatives are used, which are very
% inaccurate. If one is uncertain about the interpretation of the numbers,
% having a look at the plots of the distributed residuals might be helpful.

% history:
% * Created on 21.11.2018 by Jakob Gabriel
% * Modified on 05.06.2019 by Simon Kerschbaum:
%   - include time dependent case
%   - include ppides

	if nargin < 3
		plotResult = false;
	end
	%% Read parameter
	n = ppide.n;
	disc.z = ppide.ctrl_pars.zdiscObsKernel;
	disc.n = length(disc.z);
	disc.nPlant = ppide.ndiscPars;
	disc.zPlant = linspace(0,1,disc.nPlant);
	disc.tPlant = linspace(0,1,ppide.ndiscTime);
	disc.dz = disc.z(2) - disc.z(1);
	disc.t = linspace(0,1,size(observerKernel.value{1,1}.K,5));
	disc.nt = length(disc.t);
	
	% Parameters need to be resampled to kernel resolution. Resampling the kernel to plant
	% resolution makes no sense.
	% no problem if there is no time dimension, translate_to_grid wont create one
	disc.Lambda = misc.translate_to_grid(ppide.l,{disc.z,1:n,1:n,disc.t},'gridOld',...
		{disc.zPlant,1:n,1:n,linspace(0,1,size(ppide.l,4))});
	disc.Lambda_dz = misc.translate_to_grid(ppide.l_diff,{disc.z,1:n,1:n,disc.t},'gridOld',...
		{disc.zPlant,1:n,1:n,linspace(0,1,size(ppide.l_diff,4))});
	disc.Lambda_ddz = misc.translate_to_grid(ppide.l_diff2,{disc.z,1:n,1:n,disc.t},'gridOld',...
			{disc.zPlant,1:n,1:n,linspace(0,1,size(ppide.l_diff2,4))});
	disc.A = misc.translate_to_grid(ppide.a,{disc.z,1:n,1:n,disc.t},'gridOld',...
			{disc.zPlant,1:n,1:n,linspace(0,1,size(ppide.a,4))});
	disc.F = misc.translate_to_grid(ppide.f,{disc.z,disc.z,1:n,1:n,disc.t},'gridOld',...
			{disc.zPlant,disc.zPlant,1:n,1:n,linspace(0,1,size(ppide.f,5))});
		
	PI = observerKernel.get_K;
	% get_K should not produce NaN outside the valid area. So no inpaint_nans is required!
% 	for it = 1:n
% 		for jt = 1:n
% 			for tIdx = 1:length(disc.t)
% 				PI(:,:,it,jt,tIdx) = inpaint_nans(PI(:,:,it,jt,tIdx));
% 			end
% 		end
% 	end
	
	
	test.PI.value = PI;
% 	test.PI.value = misc.translate_to_grid(PI, disc.z, 'zdim', 2, 'method', 'spline');
	[test.PI.dzeta, test.PI.dz] = numeric.gradient_on_2d_triangular_domain(test.PI.value , disc.dz);
	[test.PI.ddzeta, ~] = numeric.gradient_on_2d_triangular_domain(test.PI.dzeta, disc.dz);
	[~, test.PI.ddz] = numeric.gradient_on_2d_triangular_domain(test.PI.dz, disc.dz);
	test.PI.dt = numeric.diff_num(disc.t,PI,5);
	if length(disc.t)>1
		test.PIInt = test.PI.value(:,:,:,:,1) + numeric.cumtrapz_fast_nDim(disc.t,test.PI.dt,5);
		errPI = test.PI.value-test.PIInt;
	% 	figure('Name','Fehler')
	% 	surf(squeeze(errPI(:,5,1,1,:)).')
	% 	figure('Name','Zeitableitung')
	% 	surf(squeeze(test.PI.dt(:,5,1,1,:)).')
	% 	figure('Name','PI normal')
	% 	surf(squeeze(test.PI.value(:,5,1,1,:)).')
	% 	figure('Name','PI integriert')
	% 	surf(squeeze(test.PIInt(:,5,1,1,:)).')
	% 	xlabel('z'),ylabel('t')
	end
	mu_o = ppide.ctrl_pars.mu_o;
	
	% discretize mu_o with all possibilities
	if disc.n~= n
		if isa(mu_o,'function_handle') % function
			if nargin(mu_o)==1
				mu = eval_pointwise(mu_o,zdisc);
			else
				mu = zeros(ndisc,n,n,length(disc.t));
				for z_idx=1:disc.n
					mu(z_idx,:,:,:) = permute((eval_pointwise(@(z)mu_o(zdisc(z_idx),z),disc.t)),[2 3 1]);
				end
			end
		elseif isequal(size(mu_o,1),disc.n) % is discretized
% 			if length(size(mu))>3
				mu = mu_o; % dimension must be correct
% 			else
% 				muInterpolant = interpolant({system.ctrl_pars.zdiscCtrl,1:n,1:n}, mu);
% 				muHelp = muInterpolant.evaluate(zdisc,1:n,1:n);
% 			end
			%muHelp = ppide.ctrl_pars.mu;
		elseif isequal(size(mu_o,1),n) % constant matrix, two dimensions
			mu(1:disc.n,:,:) = repmat(shiftdim(mu_o,-1),disc.n,1,1);
		else % discretized matrix
			error('dimensions of mu not correct!')
		end
	else
		error('ndisc=n is a very bad choice!')
	end

	%% PDE
	% Lambda(z) PI_ddz(z, zeta) - (PI(z, zeta) Lambda(zeta))_ddzeta 
	%    + A(z)PI(z,zeta) + PI(z, zeta)MU(zeta) - F(z,zeta) 
	%    + \int_{zeta}^zF(z,ozeta)PI(ozeta,zeta)d zeta - PI_t = 0
	test.PI.pde = zeros(disc.n, disc.n, n, n,disc.nt); 
	for zIdx = 2:disc.n-1
		for zetaIdx = 2:zIdx-1
			for tIdx = 1:disc.nt
				if size(disc.Lambda,4)>1
					tIdxL = tIdx;
				else
					tIdxL = 1;
				end
				if size(disc.A,4)>1
					tIdxA = tIdx;
				else
					tIdxA = 1;
				end
				if size(mu,4)>1
					tIdxMu = tIdx;
				else
					tIdxMu = 1;
				end
				if size(disc.F,5)>1
					tIdxF = tIdx;
				else
					tIdxF = 1;
				end
				
				test.PI.pde(zIdx, zetaIdx, :, :,tIdx) = ...
					reshape(disc.Lambda(zIdx,:,:,tIdxL), [n, n]) * reshape(test.PI.ddz(zIdx, zetaIdx, :, :,tIdx), [n, n]) ...
					- reshape(test.PI.ddzeta(zIdx, zetaIdx, :, :,tIdx), [n, n]) * reshape(disc.Lambda(zetaIdx,:,:,tIdxL), [n, n]) ...
					- 2 * reshape(test.PI.dzeta(zIdx, zetaIdx, :, :,tIdx), [n, n]) * reshape(disc.Lambda_dz(zetaIdx,:,:,tIdxL), [n, n]) ...
					- reshape(test.PI.value(zIdx, zetaIdx, :, :,tIdx), [n, n]) * reshape(disc.Lambda_ddz(zetaIdx,:,:,tIdxL), [n, n]) ...
					+ reshape(disc.A(zIdx,:,:,tIdxA), [n, n]) * reshape(test.PI.value(zIdx, zetaIdx, :, :,tIdx), [n, n])...
					+ reshape(test.PI.value(zIdx, zetaIdx, :, :,tIdx), [n, n])*reshape(mu(zetaIdx,:,:,tIdxMu), [n, n])...
					- reshape(disc.F(zIdx,zetaIdx,:,:,tIdxF),[n n])...
					+ reshape(numeric.trapz_fast_nDim(disc.z(zetaIdx:zIdx),...
						misc.multArray(reshape(disc.F(zIdx,zetaIdx:zIdx,:,:,tIdxF),...
										[zIdx-zetaIdx+1 n n])...
						,reshape(test.PI.value(zetaIdx:zIdx,zetaIdx,:,:,tIdx),...
										[zIdx-zetaIdx+1,n,n])...
						,3,2,1)),...
					[n n]);
					-reshape(test.PI.dt(zIdx,zetaIdx,:,:,tIdx),[n n]);
			end
		end
	end

	%% BC at z=1
	% PI(1, zeta) + A0_bar(z) = 0
	A0_bar = observerKernel.a0_bar;
	Bdr = ppide.b_op2.b_d;
	Bnr = ppide.b_op2.b_n;
	if isa(Bdr,'function_handle')
		BdlDisc = permute(misc.eval_pointwise(Bdr,disc.t),[2 3 1]);
	else
		BdlDisc = misc.translate_to_grid(Bdr,{1:n,1:n,disc.t},'gridOld',...
			{1:n,1:n,linspace(0,1,size(Bdr,3))});
	end
	
	for tIdx=1:disc.nt
		if size(BdlDisc,3)>1
			tIdxBdl = tIdx;
		else
			tIdxBdl = 1;
		end
		for zIdx = 1:disc.n
		test.PI.bc.z1(zIdx,:,:,tIdx) = ...
			reshape(BdlDisc(:,:,tIdxBdl),[n n])...
				*reshape(test.PI.value(end,zIdx,:,:,tIdx),[n, n])...
			+ Bnr * reshape(test.PI.dz(end,zIdx,:,:,tIdx),[n, n])...
			+ reshape(A0_bar(zIdx,:,:,tIdx),[n n]);
		end
	end
	
	%% Is A0_bar upper triangular?
	test.A0_bar_lower = 0;
	numberOfA0Elements = 0;
	for it = 1:n
		for jt = 1:it
			test.A0_bar_lower = test.A0_bar_lower + mean(mean(abs(A0_bar(:, it, jt,:)),4));
			numberOfA0Elements = numberOfA0Elements+1;
		end
	end
	test.A0_tilde_upper = test.A0_bar_lower / numberOfA0Elements;
	
	%% BC at z=zeta (algebraic)
	% PI(z, z) Lambda(z) - Lambda(z) PI(z, z) = 0
	test.PI.zEqualZeta = misc.diag_nd(test.PI.value);
	test.PI.bc.zzeta.algebraic = zeros(disc.n, n, n,disc.nt);
	for zIdx = 1:disc.n
		for tIdx =1:disc.nt
			if size(disc.Lambda,4)>1
				tIdxL = tIdx;
			else
				tIdxL = 1;
			end
			test.PI.bc.zzeta.algebraic(zIdx, :, :,tIdx) = ...
				reshape(test.PI.zEqualZeta(zIdx, :, :,tIdx), [n, n]) * reshape(disc.Lambda(zIdx, :, :,tIdxL), [n, n]) ...
				- reshape(disc.Lambda(zIdx, :, :,tIdxL), [n, n]) * reshape(test.PI.zEqualZeta(zIdx, :, :,tIdx), [n, n]);
		end
	end

	%% BC at z=zeta (pde)
	% Lambda(z) (dz PI(z, z) + PI_dz(z, z)) + PI_dzeta(z, z) Lambda(z) ...
	%		+ PI(z, z) Lambda_dz(z) - A(z) - MU = 0
	if n == 1
		test.PI.zEqualZetaTotalDiff = numeric.diff_num(disc.z.', test.PI.zEqualZeta);
	else
		[~, test.PI.zEqualZetaTotalDiff] = gradient(test.PI.zEqualZeta, disc.dz);
	end
	test.PI.zEqualZeta_dz = misc.diag_nd(test.PI.dz);
	test.PI.zEqualZeta_dzeta = misc.diag_nd(test.PI.dzeta);
	test.PI.bc.zzeta.pde = zeros(disc.n, n, n, disc.nt);
	for zIdx = 1:disc.n
		for tIdx = 1:disc.nt
			if size(disc.Lambda,4)>1, tIdxL = tIdx;	else, tIdxL = 1; end
			if size(disc.A,4)>1, tIdxA = tIdx;	else,tIdxA = 1; end
			if size(mu,4)>1, tIdxMu = tIdx; else, tIdxMu = 1; end
			test.PI.bc.zzeta.pde(zIdx, :, :,tIdx) = ...
				reshape(disc.Lambda(zIdx, :, :,tIdxL), [n, n]) * ...
					(reshape(test.PI.zEqualZetaTotalDiff(zIdx, :, :,tIdx), [n, n]) ...
					+ reshape(test.PI.zEqualZeta_dz(zIdx, :, :,tIdx), [n, n])) ...
				+ reshape(test.PI.zEqualZeta_dzeta(zIdx, :, :,tIdx), [n, n]) * ...
					reshape(disc.Lambda(zIdx, :, :,tIdxL), [n, n]) ...
				+ reshape(test.PI.zEqualZeta(zIdx, :, :,tIdx), [n, n]) * ...
					reshape(disc.Lambda_dz(zIdx, :, :,tIdxL), [n, n]) ...
				- reshape(disc.A(zIdx, :, :,tIdxA), [n, n]) ...
				- reshape(mu(zIdx, :, :,tIdxMu), [n, n]);
		end
	end

	%% BC at z=zeta (solution of pde) for i = 1, 2, ..., n
	% PI_ii(z, zeta) = PI_ii(0, 0) * sqrt(lambda_i(0)/lambda_i(z)) ...
	%		+ 1/sqrt(lambda_i(z)) * ...
	%			int_0^z (A_ii(zeta) + mu) / (2 * sqrt(lambda_i(zeta))) dzeta
	
	% is only computet on the diagonal!
	test.PI.bc.zzeta.integralSolution = zeros(disc.n, n,disc.nt);
	test.PI.bc.zzeta.numericSolution = zeros(disc.n, n,disc.nt);
	for it = 1:n
		for tIdx=1:disc.nt
		if size(disc.Lambda,4)>1, tIdxL = tIdx;	else, tIdxL = 1; end
		if size(disc.A,4)>1, tIdxA = tIdx;	else,tIdxA = 1; end
		if size(mu,4)>1, tIdxMu = tIdx; else, tIdxMu = 1; end
		test.PI.bc.zzeta.numericSolution(:, it,tIdx) = test.PI.zEqualZeta(:, it, it,tIdx);
		test.PI.bc.zzeta.integralSolution(:, it,tIdx) = PI(1, 1, it, it,tIdx) * ...
					sqrt(disc.Lambda(1, it, it,tIdxL) ./ disc.Lambda(:, it, it,tIdxL) )...
			+ (1./sqrt(disc.Lambda(:, it, it,tIdxL))) .* ...
			numeric.cumtrapz_fast(disc.z, reshape((disc.A(:, it, it,tIdxA) + mu(:,it, it,tIdxMu)) ./ (2*sqrt(disc.Lambda(:, it, it,tIdxL))), [1, disc.n])).';
		end
	end
	
	%% BC PI(0,0):
	Bdr = ppide.b_op2.b_d;
	Bnr = ppide.b_op2.b_n;
	if isa(Bdr,'function_handle')
		BdrDisc = permute(misc.eval_pointwise(Bdr,disc.t),[2 3 1]);
	else
		BdrDisc = misc.translate_to_grid(Bdr,{1:n,1:n,disc.t},'gridOld',...
			{1:n,1:n,linspace(0,1,size(Bdr,3))});
	end
	test.PI.bc.oneOne = zeros(n,n,disc.nt);
	for it=1:n
		for jt=1:n
			% check all elements though should only appear on the diagonal!
			if Bnr(it,it)==1 && Bnr(jt,jt)==1
				for tIdx=1:disc.nt
					if size(BdrDisc,3)>1, tIdxBdR = tIdx; else tIdxBdR = 1; end
					test.PI.bc.oneOne(it,jt,tIdx) = squeeze(test.PI.value(end,end,it,jt,tIdx)) - BdrDisc(it,jt,tIdxBdR);
				end
			else
				% is zero anyway by initialization!
			end
		end
	end
	
	fprintf(['\nAbsolut average of PI-PDE-error: ', ...
		num2str(mean(abs(test.PI.pde(:)))), '\n']);
	fprintf(['Absolut average of PI-BC-error at z=1: ', ...
		num2str(mean(abs(test.PI.bc.z1(:)))), '\n']);
	fprintf(['Absolut average of lower triangular elements of A0_bar: ', ...
		num2str(test.A0_bar_lower), '\n']);
	fprintf(['Absolut average of PI-BC-error at z=zeta of algebraic bc: ', ...
		num2str(mean(abs(test.PI.bc.zzeta.algebraic(:)))), '\n']);
	fprintf(['Absolut average of PI-BC-error at z=zeta of pde bc: ', ...
		num2str(mean(abs(test.PI.bc.zzeta.pde(:)))), '\n']);
	fprintf(['Absolut average of PI-BC-error at z=zeta of solution of pde and\n', ...
		'        successive approximation are compared: ', ...
		num2str(mean(abs(test.PI.bc.zzeta.numericSolution(:)-test.PI.bc.zzeta.integralSolution(:)))), '\n']);
	fprintf(['\nAbsolut average of PI(1,1)-error: ', ...
		num2str(mean(abs(test.PI.bc.oneOne(:)))), '\n\n']);

	if plotResult
		dps.hyperbolic.plot.matrix_4D(test.PI.pde, 'hide_upper_triangle', false);
		dps.hyperbolic.plot.matrix_3D(test.PI.bc.z1, 'title', 'PI-BC at z = 1');
		dps.hyperbolic.plot.matrix_3D(test.PI.bc.zzeta.algebraic, 'title', 'PI-BC algebraic at zeta = z');
		dps.hyperbolic.plot.matrix_3D(test.PI.bc.zzeta.pde, 'title', 'PI-BC pde at zeta = z');
		if size(test.PI.bc.zzeta.numericSolution,3)>1
			figure('Name','K-BC pde solution-fixpoint solution at zeta = z')
			for it=1:n
				subplot(n,1,it)
				surf(squeeze(test.PI.bc.zzeta.integralSolution(:,it,:)-test.PI.bc.zzeta.numericSolution(:,it,:)).')
				xlabel('z')
				ylabel('t')
			end
		else
			dps.hyperbolic.plot.matrix_3D(test.PI.bc.zzeta.integralSolution-test.PI.bc.zzeta.numericSolution, 'title', 'K-BC pde solution-fixpoint solution at zeta = z');
		end
	end
end