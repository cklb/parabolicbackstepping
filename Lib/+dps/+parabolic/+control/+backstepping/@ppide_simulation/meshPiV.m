function varargout = meshPiV(obj)
	import misc.*
	if ~obj.simulated
		obj = obj.simulate();
	end
	if ~ obj.simulatedTarget
		obj = obj.simTarget;
	end 

	zdisc = obj.ppide.ctrl_pars.zdiscRegEq;
	ndisc = length(zdisc);
	zdisc_plant = linspace(0,1,obj.ppide.ndisc);
	ndisc_plant=length(zdisc_plant);
	zdiscKernel = obj.controllerKernel.zdisc;
	zdiscTarget = obj.ppide.ctrl_pars.zdiscTarget;
	ndiscTarget = length(zdiscTarget);
	n = obj.ppide.n;
	plot_pars = obj.plotPars;
	for t_idx =1:length(obj.t)
		for z_idx = 1:length(zdisc)
			piV(t_idx,z_idx,:)= sd(obj.Pi(z_idx,:,:))*sd(obj.v(t_idx,:));
		end
	end
	for i=1:obj.ppide.n
		piVVec(:,(i-1)*ndisc+1:i*ndisc) = interp1(obj.t,piV(:,:,i),obj.tTransformed);
	end
	for i=1:obj.ppide.n
		figure('Name',['Pi*v: ' num2str(i)])
		mesh_sparse(obj.tTransformed,zdisc,piVVec(:,(i-1)*ndisc+1:i*ndisc).',20,plot_pars.plot_res_2D_num_lines,plot_pars.plot_res_2D_lines,plot_pars.plot_res_2D_lines);
		hold on
		set(gca,'Ydir','reverse')
		xlabel('$t$')
		ylabel('$z$')
		box on
	end

	for i=1:obj.ppide.n
		yBS(:,:,i) = obj.xTransformed(:,(i-1)*ndisc_plant+1:i*ndisc_plant);
	end



	% x auf zdisc resamplen:
	xTransformed = zeros(length(obj.tTransformed),length(zdisc),obj.ppide.n);
	xTargetVec = obj.ppide.sim2Sys(obj.xETarget(:,1:n*ndiscTarget));
	xTargetInt = griddedInterpolant({obj.tTarget,zdiscTarget,1:n},xTargetVec);
	xTargetResVec = xTargetInt({obj.tTransformed,zdisc,1:n});
	for i=1:obj.ppide.n
		xTransformed(:,:,i) = permute(interp1(zdisc_plant,permute(yBS(:,:,i),[2 1 3]),zdisc),[2 1 3]);
		figure('Name',['xTransformed-Pi*v: ' num2str(i)])
		mesh_sparse(obj.tTransformed,zdisc,xTransformed(:,:,i).'-piVVec(:,(i-1)*ndisc+1:i*ndisc).',20,plot_pars.plot_res_2D_num_lines,plot_pars.plot_res_2D_lines,plot_pars.plot_res_2D_lines);
		hold on 
		box on
		set(gca,'Ydir','reverse')
		xlabel('$t$')
		ylabel('$z$')
	end
	for i=1:obj.ppide.n
		figure('Name',['xTarget-Pi*v: ' num2str(i)])
		mesh_sparse(obj.tTransformed,zdisc,xTargetResVec(:,:,i).'-piVVec(:,(i-1)*ndisc+1:i*ndisc).',20,plot_pars.plot_res_2D_num_lines,plot_pars.plot_res_2D_lines,plot_pars.plot_res_2D_lines);
		hold on 
		box on
		set(gca,'Ydir','reverse')
		xlabel('$t$')
		ylabel('$z$')
	end

	for i=1:obj.ppide.n
		figure('Name',['x_transformed: ' num2str(i)])
		mesh_sparse(obj.tTransformed,zdisc,xTransformed(:,:,i).',20,plot_pars.plot_res_2D_num_lines,plot_pars.plot_res_2D_lines,plot_pars.plot_res_2D_lines);
		hold on 
		box on
		set(gca,'Ydir','reverse')
		xlabel('$t$')
		ylabel('$z$')
	end			
	for i=1:obj.ppide.n
		figure('Name',['x_target: ' num2str(i)])
		mesh_sparse(obj.tTransformed,zdisc,xTargetResVec(:,:,i).',20,plot_pars.plot_res_2D_num_lines,plot_pars.plot_res_2D_lines,plot_pars.plot_res_2D_lines);
		hold on 
		box on
		set(gca,'Ydir','reverse')
		xlabel('$t$')
		ylabel('$z$')
	end


	figure('Name','Ausgang bei Pi*v:')
	C = obj.ppide.c_op.output_matrix(zdisc);
	hold on
	for t_idx=1:length(obj.tTransformed)
		out(t_idx,:) = C*sd(piVVec(t_idx,:));
	end
	for i=1:obj.ppide.c_op.m
		plot(obj.tTransformed,out(:,i),'DisplayName',num2str(i));
	end
	legend('-DynamicLegend')
	if nargout>0
		varargout{1} = obj;
	end
end