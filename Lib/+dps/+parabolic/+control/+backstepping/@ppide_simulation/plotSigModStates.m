function obj = plotSigModStates(obj)
	if ~obj.simulated
		obj = obj.simulate;
	end
	figure('name','Signalmodellzustände')
	hold on
	plot(obj.t,obj.v);
	figure('name','Signalmodellzustände beobachtet')
	hold on
	plot(obj.t,obj.vHat);

end
