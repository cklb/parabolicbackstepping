function varargout = plotU(obj)
	if ~obj.simulated
		obj = obj.simulate();
	end
	n = obj.ppide.n;
	m = obj.ppide.c_op.m;
	ndiscPlant = obj.ppide.ndisc;
	zdiscPlant = linspace(0,1,ndiscPlant);
	sM = obj.sM; %#ok<*PROP>
	zdiscObs = obj.ppide.ctrl_pars.zdiscObs;
	ndiscObs = length(zdiscObs);
	no = n; % observer order. Will be set to 2n later if folded observer is used
	
	p = size(obj.ppide.b_2,2); 
	
	if p > n
		folding=1;
		name = ['Stellsignal, \check{z}_0 = ' num2str(obj.ppide.ctrl_pars.foldingPoint)]; 
	else
		folding=0;
		name = 'Stellsignal';
	end

	r = obj.controller.r;
	numtPlant = size(r,3);
% 	end
	
	if numtPlant>1
		% get the interpolant via translate_to_grid
		rInterpPerm = numeric.interpolant({linspace(0,1,numtPlant),1:p,1:n*ndiscPlant},permute(r,[3 1 2]));
	end
	if ~isempty(obj.Kv)
		Kv = obj.Kv;
	else
		Kv = zeros(p,sM.nv);
	end
	KvHat = obj.KvHat;
	

% 	if ~isempty(obj.observer)
% 		L = obj.observer.L;
% 		L_res_obs = L; %interp1(zdisc_obs_kernel,L,zdisc_obs(:));
% 	% 			L_res_plant = interp1(zdisc_obs,L,zdisc_plant(:));
% 		% Transform L(z) in system represenatation:
% 		L_obs = obj.ppide.sim2Sys(L_res_obs);
% 		L_obs(n*ndisc_obs,n)=0; % preallocate
% 		for i=1:n
% 			for j=1:n
% 				L_obs((i-1)*ndisc_obs+1:i*ndisc_obs,j) = L_res_obs(:,i,j);
% 	% 					L_plant((i-1)*ndisc_plant+1:i*ndisc_plant,j) = L_res_plant(:,i,j);
% 			end
% 		end
% 	end
	% Create dynamics of simulation:
	% Most important: closed-loop system.
	uRO = obj.useReferenceObserver;
	uDO = obj.useDisturbanceObserver;
	uSO = obj.useStateObserver;
	aSO = obj.useObserverInFeedback;
	uOR = obj.useOutputRegulation;
	uSFB = obj.useStateFeedback;
	uIM = obj.useIntMod;
	
	if uIM
		KvQPlant = obj.KvQPlant;
	else
		KvQPlant = zeros(p, n*ndiscPlant);
	end
	
	% interpolate observer states to controller resolution by using a
	% matrix
	if uSO && aSO
		if any(obj.ppide.cm_op.c_m(:))  || any(obj.ppide.cm_op.c_dm(:)) % folding observer
			zm = obj.ppide.cm_op.z_m;
			no = 2*n;
			zmIdx = find(zdiscPlant<zm,1,'last');
			% the actual state must be calculated via unfolding from the folded observer
			% states:
			% x(z,t) = 
			%    w((zm-z)/zm) for z<zm states of the first observer
			%    w((z-zm)/(1-zm)) for z>= zm  states of the second observer
			% expressed as matrix multiplication with discretized x and w:
			% x = IOPTot*w
			% Interpolation matrix to regard for different resolutions:
			% calling x the plant state and xl/xr the states of the 2 observers.
			% In the plant (where to go) it is:
			% x_1(0..zm..1)
			% x_2(0..zm..1)
			% ....
			% corresponding to 
			% xl_1(0..1)xr_1(0..1)
			% xl_2(0..1)xr_2(0..1)
			% ...
			% But the observer states are ordered as
			% xl_1(0..1)
			% xl_2(0..1)
			% ...
			% xr_1(0..1)
			% xr_2(0..1)
			% Therefore the corresponding columns need to be swapped.
			
			% x_i(0..zm) = IOP1*xl_i(0..1)
			IOP1 = flipud(misc.interp_mat(zdiscObs, linspace(0,1,zmIdx))); % from observer to plant
			% x_i(zm..1) = IOP2*xr_i(0..1)
			IOP2 = misc.interp_mat(zdiscObs, linspace(0,1,ndiscPlant-zmIdx)); % from observer to plant
			IOPTot = zeros(n*ndiscPlant,2*n*ndiscObs);
			nL = size(IOP1,1);
			nR = size(IOP2,1);
			nP = ndiscPlant;
			nO = ndiscObs;
			for i=1:n
				IOPTot((i-1)*nP+1:(i-1)*nP+nL,(i-1)*nO+1:i*nO) = IOP1;
				IOPTot(i*nL+(i-1)*nR+1:i*nP,n*nO+(i-1)*nO+1:n*nO+(i-1)*nO+nO) = IOP2;
			end
		else
			IOP = interp_mat(zdiscObs, zdiscPlant); % from observer to plant
			% add to diagonal matrix to interpolate each state. Using kron is
			% faster than doing it manually for large n.
			IOPTot = kron(eye(n), IOP);
		end
	else % no state observer
		IOPTot = eye(n*ndiscPlant);
	end % if
	
	if uSO
		ndiscO = ndiscObs;
	else
		ndiscO = ndiscPlant;
	end
	nvHatInt = uIM*m*sM.nv;
	selectXHat = [(1-(uSO&&aSO))*eye(n*ndiscPlant,no*ndiscO); zeros(sM.nv,no*ndiscO);[(uSO&&aSO)*eye(no*ndiscObs) zeros(no*ndiscObs,no*ndiscO-no*ndiscObs)];zeros(sM.nd,no*ndiscO);zeros(sM.nr,no*ndiscO);zeros(nvHatInt,no*ndiscO)]; % selection matrix of reconstructed states
	selectVHat = [zeros(n*ndiscPlant,sM.nv); blkdiag((1-uDO)*eye(sM.nd,sM.nd),(1-uRO)*eye(sM.nr,sM.nr)) ; zeros(no*ndiscObs,sM.nv) ; blkdiag(uDO*eye(sM.nd),uRO*eye(sM.nr,sM.nr));zeros(nvHatInt,sM.nv)]; % selection matrix of reference states and reconstructed disturbance states
	selectVHatInt = [zeros(n*ndiscPlant,nvHatInt); zeros(sM.nv,nvHatInt) ; zeros(no*ndiscObs,nvHatInt) ; zeros(sM.nv,nvHatInt);eye(nvHatInt)]; % selection matrix of internal model states
% 			u  = @(t,x_e) uSFB*r*IOPTot*selectXHat.'*x_e - uOR*Kv*selectVHat.'*x_e;

	u_orig = zeros(length(obj.t),p);
	u_kv = zeros(length(obj.t),p);
	for t_idx = 1:length(obj.t)
		if numtPlant>1
			u_orig(t_idx,:) = uSFB*reshape(rInterpPerm.evaluate(obj.t(t_idx),1:p,1:n*ndiscPlant),[p n*ndiscPlant])*IOPTot*selectXHat.'*obj.xE(t_idx,:).' - uOR*Kv*selectVHat.'*obj.xE(t_idx,:).'...
		          - uIM*KvHat*selectVHatInt.'*obj.xE(t_idx,:).' + uIM*KvQPlant*IOPTot*selectXHat.'*obj.xE(t_idx,:).';
		else
			u_orig(t_idx,:) = uSFB*r*IOPTot*selectXHat.'*obj.xE(t_idx,:).' - uOR*Kv*selectVHat.'*obj.xE(t_idx,:).'...
				- uIM*KvHat*selectVHatInt.'*obj.xE(t_idx,:).' + uIM*KvQPlant*IOPTot*selectXHat.'*obj.xE(t_idx,:).';
		end
		u_kv(t_idx,:) = - uOR*Kv*selectVHat.'*obj.xE(t_idx,:).';
	end
			  
	figure('Name','ControlEffort')
	hold on
	title(name)
	if folding
		for i=1:n
			plot(obj.t,u_orig(:,i),'r','displayName',['u_0_' num2str(i)])
		end
		for i=n+1:p
			plot(obj.t,u_orig(:,i),'b','displayName',['u_1_' num2str(i-n)])
		end
	else
		for i=1:p
			plot(obj.t,u_orig(:,i),'r','displayName',['u_' num2str(i)])
			if uOR
				plot(obj.t,u_kv(:,i),'b','displayName',['-Kv*v_' num2str(i)])
			end
		end	
	end
	legend('-dynamicLegend');
	
	obj.u = u_orig;
	if nargout>0
		varargout{1} = obj;
	end
end