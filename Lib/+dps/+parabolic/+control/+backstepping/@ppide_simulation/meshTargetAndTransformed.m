function varargout = meshTargetAndTransformed(obj,varargin)
	import misc.*
	if ~obj.simulated
		obj = obj.simulate();
		if ~ obj.simulatedTarget
			obj = obj.simTarget();
		end
	end
	if ~ obj.simulatedTarget
		obj = obj.simTarget();
		if ~obj.simulated
			obj = obj.simulate();
		end
	end
	
	found=0;
	passed = [];
	arglist = {'numpointsT','numpointsZ','zLim','tLim','plotPars'};
	if ~isempty(varargin)
		if mod(length(varargin),2) % uneven number
			error('When passing additional arguments, you must pass them pairwise!')
		end
		for index = 1:2:length(varargin) % loop through all passed arguments:
			for arg = 1:length(arglist)
				if strcmp(varargin{index},arglist{arg})
					passed.(arglist{arg}) = varargin{index+1};
					found=1;
					break
				end 
			end % for arg
			% argument wasnt found in for-loop
			if ~found
				error([varargin{index} ' is not a valid property to pass to meshObserverError!']);
			end
			found=0; % reset found
		end % for index
	end 
	if ~isfield(passed,'numpointsT')
		passed.numpointsT = obj.plotPars.plot_res_2D;
	end
	if ~isfield(passed,'numpointsZ')
		passed.numpointsZ = obj.plotPars.plot_res_2D;
	end
	if ~isfield(passed,'zLim')
		zPlot = linspace(0,1,passed.numpointsZ);
	else
		zPlot = linspace(passed.zLim(1),pssed.zLim(2),passed.numpointsZ);
	end
	if ~isfield(passed,'tLim')
		tPlot = linspace(0,1,passed.numpointsT);
	else
		tPlot = linspace(passed.tLim(1),passed.tLim(2),passed.numpointsT);
	end
	if ~isfield(passed,'plotPars')
		passed.plotPars = {};
	else
		if ~iscell(passed.plotPars) 
			error('plotPars must be passed as cell array of strings representing name-value-pairs!')
		end
	end
	
	for i=1:obj.ppide.n
		figure('Name',['TargetAndTransformed3D_' num2str(i)])
		mesh(tPlot,zPlot,obj.xETargetQuantity(i).on({tPlot,zPlot},{'t','z'}).',passed.plotPars{:})
		hold on
		set(gca,'yDir','reverse')
		mesh(tPlot,zPlot,obj.xTransformedQuantity(i).on({tPlot,zPlot},{'t','z'}).',passed.plotPars{:})
		xlabel('t')
		ylabel('z')
		legend('Target','Transformed')
	end
	
	if nargout>0
		varargout{1} = obj;
	end
end