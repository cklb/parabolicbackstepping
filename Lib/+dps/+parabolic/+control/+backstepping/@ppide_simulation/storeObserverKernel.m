function obj = storeObserverKernel(obj)
% Funktion zur Kernberechnung auslagern, damit Objekt danach
% abgespeichert werden kann.
	if obj.useStateObserver
		n = obj.ppide.n;
		% in domain-measurement: folding
		if any(obj.ppide.cm_op.c_m(:))  || any(obj.ppide.cm_op.c_dm(:))
			if ~isequal(obj.ppide.cm_op.c_m,[zeros(n,n);eye(n)]) || ~isequal(obj.ppide.cm_op.c_dm,[eye(n);zeros(n,n)])
				error('When the measurement is in-domain, both the state and the derivative must be measured.')
			end
			% in this case, a collocated in-domain measuement at zm is assumed. This
			% splits the problem into two clasical subploblems.
			% create folded system:
			zm = obj.ppide.cm_op.z_m;
			obj.observerFoldedPpide = obj.foldPpide('zF',zm,'preserve',1);

			zdisc = linspace(0,1,obj.observerFoldedPpide.ndiscPars);
			ndisc = obj.observerFoldedPpide.ndiscPars;
			zLeft = zeros(n,ndisc);
			zRight = zeros(n,ndisc);
			zRight = linspace(zm,1,ndisc);
			dZ = diff(zRight);
			zLeft = linspace(0,zm-dZ(1),ndisc);
			% the folding point seen from the left side of the domain. Is not exactly the
			% folding point due to discretisation.
			zmLeft = zLeft(end);
			for i=1:2

				Ared = obj.observerFoldedPpide.a(:,(i-1)*n+1:i*n,(i-1)*n+1:i*n,:);
				muRed = obj.observerFoldedPpide.ctrl_pars.mu(:,(i-1)*n+1:i*n,(i-1)*n+1:i*n,:);
				mu_oRed = obj.observerFoldedPpide.ctrl_pars.mu_o(:,(i-1)*n+1:i*n,(i-1)*n+1:i*n,:);
				g_strichRed = obj.observerFoldedPpide.ctrl_pars.g_strich((i-1)*n+1:i*n,(i-1)*n+1:i*n);
				ctrl_pars_red = obj.observerFoldedPpide.ctrl_pars;
				ctrl_pars_red.mu = muRed;
				ctrl_pars_red.mu_o = mu_oRed;
				ctrl_pars_red.g_strich = g_strichRed;
				Lred = obj.observerFoldedPpide.l(:,(i-1)*n+1:i*n,(i-1)*n+1:i*n);
				LDiffRed = obj.observerFoldedPpide.l_diff(:,(i-1)*n+1:i*n,(i-1)*n+1:i*n);
				if i==1
					% in this case, using zm would cause a small error. See foldPpide, the
					% left part of the domain only reaches to zdiscObs(zmIdx), why the
					% derivatives are related with this factor exactly.
					if isa(obj.observerFoldedPpide.b_op2.b_d,'function_handle')
						BdRed = @(t) -zmLeft*[eye(n) zeros(n,n)]*obj.observerFoldedPpide.b_op2.b_d(t)*[eye(n);zeros(n,n)];
					else
						BdRed  = -zmLeft*obj.observerFoldedPpide.b_op2.b_d((i-1)*n+1:i*n,(i-1)*n+1:i*n);
					end
					BnRed  = -zmLeft*obj.observerFoldedPpide.b_op2.b_n((i-1)*n+1:i*n,(i-1)*n+1:i*n);
					b2 = -zmLeft*eye(n);
				else
					if isa(obj.observerFoldedPpide.b_op2.b_d,'function_handle')
						BdRed = @(t) (1-zm)*[zeros(n,n) eye(n)]*obj.observerFoldedPpide.b_op2.b_d(t)*[zeros(n,n);eye(n)];
					else
						BdRed  = (1-zm)*obj.observerFoldedPpide.b_op2.b_d((i-1)*n+1:i*n,(i-1)*n+1:i*n);
					end
					BnRed  = (1-zm)*obj.observerFoldedPpide.b_op2.b_n((i-1)*n+1:i*n,(i-1)*n+1:i*n);
					b2 = (1-zm)*eye(n);
				end
				% the single observers need left and right input.
				% not necessary to define 2n inputs, because Br and Bl are calculated
				% separately in the approximation
				b1 = [eye(n)];
				% for the caluclation of the observer gains, the system only has a neumann
				% measurement at the left boundary. This is injected via L(z) distributed
				% over the domain.
				cmRed = dps.output_operator('c_d0',eye(n));
				b_opRight = dps.parabolic.system.boundary_operator(...
					'z_b',1,...
					'b_d',BdRed,...
					'b_n',BnRed);
				b_opLeft = dps.parabolic.system.boundary_operator(...
					'z_b',0,...
					'b_d',eye(n),...
					'b_n',zeros(n));
				ppide{i} = dps.parabolic.system.ppide_sys(...
					'n',n,...
					'l',Lred,...
					'l_diff',LDiffRed,...
					'a',Ared,...
					'b_op1',b_opLeft,...
					'b_op2',b_opRight,...
					'b_1',b1,...
					'b_2',b2,...
					'name',['sub_ppide_' num2str(i)],...
					'ndiscPars',obj.ppide.ndiscPars,...
					'ctrl_pars',ctrl_pars_red,...
					'cm_op',cmRed);
			end
			obj.observerPpide1 = ppide{1};
			obj.observerPpide2 = ppide{2};
			if ~obj.observerKernelFlag 
				fprintf('Storing observer kernel 1:...\n')
				[obj.observerKernel1, obj.dualKernel1] = obj.compute_observer_kernel(ppide{1},obj.ppide.ctrl_pars.zdiscObsKernel);
				fprintf('Storing observer kernel 2:...\n')
				[obj.observerKernel2, obj.dualKernel2] = obj.compute_observer_kernel(ppide{2},obj.ppide.ctrl_pars.zdiscObsKernel);
				obj.observerKernelFlag =1;
				obj.observerFlag = 0;
				obj.GammaFlag = 0;
			end
		else % normal boundary measurement
			if ~obj.observerKernelFlag 
				fprintf('Computing observer kernel:...\n')
				to = tic;
				[obj.observerKernel, obj.dualKernel] = obj.compute_observer_kernel(obj.ppide,obj.ppide.ctrl_pars.zdiscObsKernel);
				fprintf('finished observer kernel. (%0.2fs)\n',toc(to));
				obj.observerKernelFlag =1;
				obj.observerFlag = 0;
				obj.GammaFlag = 0;
			end
		end
	end
end

