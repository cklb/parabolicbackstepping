function varargout = plotTargetAndTransformed(obj,zdiscPlot)
	import misc.*
	if ~obj.simulated
		obj = obj.simulate();
		if ~ obj.simulatedTarget
			obj = obj.simTarget();
		end
	end
	if ~ obj.simulatedTarget
		obj = obj.simTarget();
		if ~obj.simulated
			obj = obj.simulate();
		end
	end
	plot_pars = obj.plotPars;
	zdiscTarget = obj.ppide.ctrl_pars.zdiscTarget;
	ndiscTarget = length(zdiscTarget);
	zdiscObs = obj.ppide.ctrl_pars.zdiscObs;
	ndiscObs = length(zdiscObs);
	ndiscTargetObs = ndiscTarget;
	zdiscTargetObs = zdiscTarget;
	ndiscPlant = obj.ppide.ndisc;
	zdiscPlant = linspace(0,1,ndiscPlant);
	zdiscKernel = obj.controllerKernel.zdisc;
	ndiscKernel = length(zdiscKernel);
	if nargin<2
		zdiscPlot = zdiscTarget;
	end
	n=obj.ppide.n;
	ppide=obj.ppide;

	y_target = obj.xETarget(:,1:n*ndiscTarget);
	
	% Veranschaulichen der maximalen Abklingrate
	% F�r abklingverhalten ist mu_max mit \tilde{A}_* = 0 und mu = 0
	% erforderlich:
	% Zielsystem in Objekt zusammenfassen
	% x0_target_norm = sqrt(sum(x0_target.^2));
% 			x0_target_quad_sum = 0;
% 			for i=1:n
% 				x0_target_quad_sum = x0_target_quad_sum + x0_target((i-1)*ndiscTarget+1:i*ndiscTarget).^2;
% 			end
% 			x0_target_L2_norm = sqrt(trapz_fast(zdiscTarget,x0_target_quad_sum));
% 			x_target_soll = exp((mu_max-ppide.ctrl_pars.mu(1,1))*t_target);

	% Norm plotten
% 			if plotten2.norm
% 				% ||h|| = (int_0^1 ||h||^2_Cn dz)^1/2
% 				y_quad_sum = 0;
% 				y_target_quad_sum = 0;
% 				for i=1:n
% 					y_quad_sum = y_quad_sum + y(:,(i-1)*ndisc_plant+1:i*ndisc_plant).^2;
% 					y_target_quad_sum = y_target_quad_sum + y_target(:,(i-1)*ndiscTarget+1:i*ndiscTarget).^2;
% 				end
% 				y_2_norm_quad = (y_quad_sum);
% 			% 	for t_idx = 1:length(t)
% 				y_L2_norm = sqrt(trapz_fast(zdisc_plant,y_2_norm_quad));
% 				y_target_L2_norm = sqrt(trapz_fast(zdisc_plant,y_target_quad_sum));
% 			% 	end
% 			% 	y_norm_sum(t_idx) = y_norm_sum(t_idx) + sqrt(sum(y(t_idx,(i-1)*ndisc_plant+1:i*ndisc_plant).^2))/ndisc_plant;
% 			% 	y_norm_ges0 = sqrt(sum(y_norm(1,:).^2));
% 				figure('Name','Norm_geregeltes_System')
% 				hold on
% 				plot(t,y_L2_norm,'DisplayName','ist');
% 				plot(t,3*max(y_L2_norm)*exp((mu_max-ppide.ctrl_pars.mu(1,1))*t),'lineWidth',2,'Color','green','DisplayName','soll')
% 				plot(t_target,y_target_L2_norm,'b:','LineWidth',2,'DisplayName','target_ist');	
% 				plot(t_target,1.1*max(y_target_L2_norm)*x_target_soll,'g:','lineWidth',2,'DisplayName','target_soll')
% 				legend('-DynamicLegend');
% 			end

% 	% Anfangsverteilung in Zielkoordinaten plotten
% 	x0_target_vec = ppide.sim2Sys(obj.xETarget(1,1:1*n*ndiscTarget));
% 	
% % 			if plotten2.x0_target
% 	figure('Name','Anfangsverteilung_Zielkoordinaten')
% 	hold on 
% 	grid on
% 	plot(zdiscTarget,misc.reduce_dimension(x0_target_vec));
	
	% Zielsystem und transformierter Verlauf plotten
% 			if plotten2.target_and_transformed1D
	% Transformiertes System auf Aufl�sung Zielsystem resamplen:
	xRes = zeros(length(obj.tTransformed),n*ndiscTarget);
% 	xResTarget = zeros(length(obj.tTarget),n*ndiscKernel);
	for i=1:n
		xInterp = griddedInterpolant({obj.tTransformed,zdiscPlant},obj.xTransformed(:,(i-1)*ndiscPlant+1:i*ndiscPlant));
% 		xInterpTarget = griddedInterpolant({obj.tTarget,zdiscTarget},obj.xETarget(:,(i-1)*ndiscTarget+1:i*ndiscTarget));
		xRes(:,(i-1)*ndiscTarget+1:i*ndiscTarget) = xInterp({obj.tTransformed,zdiscTarget});
% 		xResTarget(:,(i-1)*ndiscKernel+1:i*ndiscKernel) = xInterpTarget({obj.tTarget,zdiscKernel});
	end
	% entf�llt bei Beobachter


% 	figure('name','Zielsystem_und_transformierter_Verlauf')
% 	hold on
% 	legendtext = '';
% 	p1 = plot(obj.tTarget,obj.xETarget(:,1:n*ndiscTarget),'r--','LineWidth',2);
% % 	p1 = plot(obj.tTarget,xResTarget,'r--','LineWidth',2);
% 	legendtext{1} = 'Zielsystem';
% 	p2 = plot(obj.tTransformed,xRes,'b','LineWidth',2);
% % 	p2 = plot(obj.tTransformed,obj.xTransformed,'b','LineWidth',2);
% 	legendtext{2} = 'transformiert';
% 	% p3 = plot(t_target,x_target_soll,'g','LineWidth',2.5);
% 	% legendtext{3} = 'Abklingrate';
% 	legend([p1(1),p2(1)], legendtext);

	% figure('name','Zielsystem und transformierter Verlauf Simulink')
	% hold on
	% plot(t_target,y_target,'r--','DisplayName','Zielsystem','LineWidth',2)
	% plot(t_simu,y_transformed_simu,'b','DisplayName','transformiert','LineWidth',2)
	% plot(t_target,x_target_soll,'g','DisplayName','Abklingrate','LineWidth',2.5)
% 			end

	% Zielsystem und transformierter Verlauf 3D 
% 			if plotten2.target_and_transformed3D
	xTransformedVec = ppide.sim2Sys(obj.xTransformed);
	xTransformedInt = numeric.interpolant({obj.tTransformed,zdiscPlant,1:n},xTransformedVec);
	xTransformedResVec = xTransformedInt.evaluate(obj.tTransformed,zdiscPlot,1:n);
% 	xTransformedVec = ppide.sim2Sys(obj.xTransformed);
% 	xTransformedInt = numeric.interpolant({obj.tTransformed,zdiscKernel,1:n},xTransformedVec);
% 	xTransformedResVec = xTransformedInt.evaluate(obj.tTransformed,zdiscPlot,1:n);

	xTargetVec = ppide.sim2Sys(y_target);
	xTargetInt = numeric.interpolant({obj.tTarget,zdiscTarget,1:n},xTargetVec);
	xTargetResVec = xTargetInt.evaluate(obj.tTransformed,zdiscPlot,1:n);
% 	xTargetVec = ppide.sim2Sys(y_target);
% 	xTargetInt = numeric.interpolant({obj.tTarget,zdiscTarget,1:n},xTargetVec);
% 	xTargetResVec = xTargetInt.evaluate(obj.tTransformed,zdiscPlot,1:n);
	for i=1:n
		figure('name',['Zielsystem_und_transformierter_Verlauf_3D_' num2str(i)])
		hold on
		box on
		set(gca,'Ydir','reverse')
		xlabel('$t$')
		ylabel('$z$')
		mesh(obj.tTransformed,zdiscPlot,xTransformedResVec(:,:,i).','DisplayName','transformiert')
		mesh(obj.tTransformed,zdiscPlot,xTargetResVec(:,:,i).','DisplayName','Zielsystem')
		legend('-dynamicLegend')
% 			end
	% plot(t_target,y_target,'r--','DisplayName','Zielsystem','LineWidth',2)
	% plot(t,y_transformed,'b','DisplayName','transformiert','LineWidth',2)
	end
	 % scheint zu gehen!
	 
	%%%%%%%%%%%% Beobachter
	if obj.useStateObserver
		y_targetObsOrig = obj.xETargetObserver(:,1:n*ndiscTargetObs);
		xTargetVecObsOrig = ppide.sim2Sys(y_targetObsOrig);
		xTargetVecObs = permute(misc.translate_to_grid(permute(xTargetVecObsOrig,[2 1 3]),zdiscObs),[2 1 3]);
		y_targetObs  = ppide.sys2Sim(xTargetVecObs);
		x0_target_vecObs = ppide.sim2Sys(obj.xETargetObserver(1,1:1*n*ndiscTargetObs));
		
		figure('Name','Anfangsverteilung_Zielkoordinaten Beobachter')
		hold on 
		grid on
		plot(zdiscTargetObs,misc.reduce_dimension(x0_target_vecObs));
		eTransformedVec = ppide.sim2Sys(obj.eTransformedObserver);
		plot(linspace(0,1,size(eTransformedVec,2)),misc.reduce_dimension(eTransformedVec(1,:,:)));
		
% 		figure('name','Zielsystem_und_transformierter_Verlauf_Beobachter')
% 		hold on
% 		legendtext = '';
% 		p1 = plot(obj.tTargetObserver,y_targetObs,'r--','LineWidth',2);
% 		legendtext{1} = 'Zielsystem';
% 		p2 = plot(obj.tTransformed,obj.eTransformedObserver,'b','LineWidth',2);
% 		legendtext{2} = 'transformiert';
% 		% p3 = plot(t_target,x_target_soll,'g','LineWidth',2.5);
% 		% legendtext{3} = 'Abklingrate';
% 		legend([p1(1),p2(1)], legendtext);

		% figure('name','Zielsystem und transformierter Verlauf Simulink')
		% hold on
		% plot(t_target,y_target,'r--','DisplayName','Zielsystem','LineWidth',2)
		% plot(t_simu,y_transformed_simu,'b','DisplayName','transformiert','LineWidth',2)
		% plot(t_target,x_target_soll,'g','DisplayName','Abklingrate','LineWidth',2.5)
	% 			end

		% Zielsystem und transformierter Verlauf Beobachter
	% 			if plotten2.target_and_transformed3D
		xTransformedVecObs = ppide.sim2Sys(obj.eTransformedObserver);
% 		warning('changed here')
		if (any(obj.ppide.cm_op.c_m(:))  || any(obj.ppide.cm_op.c_dm(:))) && obj.useStateObserver % folding 
			xTransformedIntObs = numeric.interpolant({obj.tTransformed,zdiscPlant,1:n},xTransformedVecObs);
			xTransformedResVecObs = xTransformedIntObs.evaluate(obj.tTransformed,zdiscPlot,1:n);
		else
			xTransformedIntObs = numeric.interpolant({obj.tTransformed,obj.observerKernel.zdisc,1:n},xTransformedVecObs);
			xTransformedResVecObs = xTransformedIntObs.evaluate(obj.tTransformed,zdiscPlot,1:n);
		end
		
		xTargetIntObs = numeric.interpolant({obj.tTargetObserver,zdiscTargetObs,1:n},xTargetVecObsOrig);
		xTargetResVecObs = xTargetIntObs.evaluate(obj.tTransformed,zdiscPlot,1:n);
		for i=1:n
			figure('name',['Zielsystem_und_transformierter_Verlauf_3D_' num2str(i) ', Beobachter'])
			hold on
			box on
			set(gca,'Ydir','reverse')
			xlabel('$t$')
			ylabel('$z$')
			mesh(obj.tTransformed,zdiscPlot,xTransformedResVecObs(:,:,i).','DisplayName','transformiert')
			mesh(obj.tTransformed,zdiscPlot,xTargetResVecObs(:,:,i).','DisplayName','Zielsystem')
			legend('-dynamicLegend')
	% 			end
		% plot(t_target,y_target,'r--','DisplayName','Zielsystem','LineWidth',2)
		% plot(t,y_transformed,'b','DisplayName','transformiert','LineWidth',2)
		end
% 		figure('Name','Anfangswerte nach resample auf zdiscPlot mit t_transformed')
% 		hold on
% 		for i=1:n
% 			plot(zdiscPlot,xTransformedResVecObs(1,:,i),'DisplayName',['transformiert' num2str(i)]);
% 			plot(zdiscPlot,xTargetResVecObs(1,:,i),'DisplayName',['Ziel' num2str(i)]);
% 		end
	end

	% Abweichung System und Zielsystem
	% macht auch nur an den Stellen Sinn, an denen Backstepping-Trafo definiert
	% ist!
% 			if plotten2.diff_orig_target
		err_x = xTransformedResVec - xTargetResVec;
		for i=1:n		
			if max(max(abs(xTargetResVec(:,:,i))))~= 0
				err_x_rel = err_x/max(max(abs(xTargetResVec(:,:,i))));
			else
				err_x_rel = zeros(size(xTargetResVec));
			end

			figure('name',['Abweichung_3D_' num2str(i)])
			hold on
			title('relativer Fehler bezogen auf maximale Zielsystem-Auslenkung')
	% 		mesh(t,zdisc,err_x.','DisplayName','Fehler')
			mesh_sparse(obj.tTransformed,zdiscPlot,err_x_rel(:,:,i).',plot_pars.plot_res_2D_num_lines,plot_pars.plot_res_2D_num_lines,plot_pars.plot_res_2D_lines,plot_pars.plot_res_2D_lines);
			xlabel('t')
			ylabel('z')
	% 		legend('-dynamicLegend')
			[~,It] = max(abs(err_x(:,:,i)));
			[M,Iz] = max(max(abs(err_x(:,:,i))));
			disp(['Max. Abweichung: ' num2str(i) ' = ' num2str(M) ' at (t,z)=(' num2str(obj.tTransformed(It(Iz))) ',' num2str(zdiscPlot(Iz)) ')'])
		end
		% Beobachter
		if obj.useStateObserver
		err_xObs = xTransformedResVecObs - xTargetResVecObs;
		for i=1:n		
			if max(max(abs(xTargetResVecObs(:,:,i))))~= 0
				warning('Changed!')
				err_x_relObs = err_xObs/max(max(abs(xTargetResVecObs(:,:,i))));
			else
				err_x_relObs = zeros(size(xTargetResVecObs));
			end

			figure('name',['Abweichung_3D_' num2str(i) '_Beobachter'])
			hold on
			title('relativer Fehler bezogen auf maximale Zielsystem-Auslenkung Beobachter')
	% 		mesh(t,zdisc,err_x.','DisplayName','Fehler')
			mesh_sparse(obj.tTransformed,zdiscPlot,err_x_relObs(:,:,i).',plot_pars.plot_res_2D_num_lines,plot_pars.plot_res_2D_num_lines,plot_pars.plot_res_2D_lines,plot_pars.plot_res_2D_lines);
			xlabel('t')
			ylabel('z')
	% 		legend('-dynamicLegend')
			disp(['Max. Abweichung Beobachter: ' num2str(i) ' = ' num2str(max(max(abs(err_xObs(:,:,i)))))])
		end
	end
% 			end
	if nargout>0
		varargout{1} = obj;
	end
end