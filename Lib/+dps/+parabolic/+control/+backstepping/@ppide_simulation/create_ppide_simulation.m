function obj = create_ppide_simulation(obj,ppide,varargin)
% create_ppide_simulation(ppide,varargin) helper function for the ppide_simulation constructor
%   obj = create_ppide_simulation(obj,ppide,varargin) sets the parameters
%     stored in varargin and stores them into the ppide_simulation object
%     obj.

% created on 04/2018 by Simon Kerschbaum

	ds = datestr(now,'yy-mm-dd__HH-MM-ss');			
	if isa(varargin,'cell') % when varargin was passed as a variable
		if isa (varargin{1},'cell')
			varargin = varargin{1};
		end
	end
	% store ppide
	obj.ppide=ppide;
	% my input parser:
	found=0;
	arglist = properties(obj);
	if ~isempty(varargin)
		if mod(length(varargin),2) % uneven number
			error('When passing additional arguments, you must pass them pairwise!')
		end
		for index = 1:2:length(varargin) % loop through all passed arguments:
			for arg = 1:length(arglist)
				if strcmp(varargin{index},arglist{arg})
					obj.(arglist{arg}) = varargin{index+1};
					found=1;
					break
				end 
			end % for arg
			% argument wasnt found in for-loop
			if ~found
				error([varargin{index} ' is not a valid property of class ppide_simulation!']);
			end
			found=0; % reset found
		end % for index
	end 
	% create folded system if required
	if size(ppide.b_1,2)== 2*ppide.n % bilateral system
		obj.designPpide = obj.foldPpide();
	end
	
	% store approximations
	if isempty(obj.ppideApproximation)
		obj.ppideApproximation = dps.parabolic.system.ppide_approximation(obj.ppide);
 		obj.checkEigenvalues();
	end
	if isempty(obj.simPpideApproximation) && ~isempty(obj.simPpide)
		obj.simPpideApproximation = dps.parabolic.system.ppide_approximation(obj.simPpide);
	end
	if isempty(obj.observerApproximation) && ~isempty(obj.observer)
		obj.observerApproximation = dps.parabolic.system.ppide_approximation(obj.observer.ppide);
	end
	
	if isempty(obj.sM)
		% default signal model is non-existent, but number of reference
		% signals must fit to system! This is ensured by the constructor of
		% the signalModel
		obj.sM = dps.parabolic.control.outputRegulation.signalModel(...
			double.empty(0,0),double.empty(0,0),double.empty(0,0),...
			zeros(obj.ppide.c_op.m,0));
	end
	
	% Store the saveStr to be used for savings
	obj.saveStr = ...
		[obj.path... % path
		ds...                    % new date
		obj.name...                 % new name
		'_n'...		% num States
		num2str(obj.ppide.n)...
		'_nc'...
		num2str(length(obj.ppide.ctrl_pars.zdiscCtrl))... % ndiscCtrl
		'_noK'... % rest unchanged
		num2str(length(obj.ppide.ctrl_pars.zdiscObsKernel))];
end % function