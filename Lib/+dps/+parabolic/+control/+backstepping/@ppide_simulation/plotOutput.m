function varargout = plotOutput(obj)
	if ~obj.simulated
		obj = obj.simulate();
	end
	if ~isempty(obj.simPpide)
		output = (obj.simPpideApproximation.C*obj.x.').';
	else
		output = (obj.ppideApproximation.C*obj.x.').';
	end
	reference = (obj.sM.pr*obj.v.').';
	err_y = output-reference;

	figure('Name','Outputs')
% 			plot(t_BS,output_BS,'b','DisplayName','State feedback')
	hold on
	plot(obj.t,output,'DisplayName','y');
	plot(obj.t,reference,'DisplayName','r');

% 			yl = ylim();
% 			plot(t_uncontrolled,output_uncontrolled,'r','DisplayName','Uncontrolled');
% 			ylim(yl);
	legend('-dynamicLegend')

	figure('Name','OutputError')
% 			plot(t_BS,err_y_BS,'b','DisplayName','State feedback');
	hold on
	plot(obj.t,err_y,'DisplayName','Output regulation');
% 			yl=ylim();
% 			plot(t_uncontrolled,err_y_uncontrolled,'r','DisplayName','Uncontrolled');
% 			ylim(yl);
	legend('-dynamicLegend')
	if nargout>0
		varargout{1} = obj;
	end
end