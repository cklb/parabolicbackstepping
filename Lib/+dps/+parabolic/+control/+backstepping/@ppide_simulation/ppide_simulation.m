classdef ppide_simulation
	%PPIDE_SIMULATION Simlation of a ppide_system
	%   Simulation of a ppide. Store parameters and simulation results and
	%   contains plot functions.
	%   TODO: DOKU WEITER!
	
% created on 09.05.2018 by Simon Kerschbaum
	
	properties
		debug = 0;                              % show plots for debugging purpose
	end

	properties(Dependent=true)
		simPars								    % simulation paramters
		ppide dps.parabolic.system.ppide_sys    % corresponding original system
		simPpide dps.parabolic.system.ppide_sys % ppide for simulation (can differ from design ppide)
		sM dps.parabolic.control.outputRegulation.signalModel   % signal model
		x0                                      % initial state (discretized or function)
		v0                                      % initial state of signal model
		tv                                      % times belongign to the initial state of signal model
		xHat0                                   % initial state observer (discretized or function)
		vHat0                                   % initial state disturbance observer
		referenceSignal                         % reference input, only used for internal model so far
		plotPars
		
		% input options:
		useStateFeedback                        % use backstepping-based state feedback controller
		useOutputRegulation                     % Use Signal model states feedback to achieve output tracking and disturbance rejection
		useStateObserver 				    	% activate state observing
		useObserverInFeedback                   % use the observed states for feedback
		useDisturbanceObserver 			    	% use the disturbance observer or directly use the signal model states
		useReferenceObserver					% use a reference observer or directly use the signal model states
		useIntMod                               % use internal model controller
		useStateFeedbackForInputDelay			% use controller considering input delays
		useStateObserverForOutputDelay			% use observer considering output delays
		
		% meta data:
		name                                    % string representing the name of the simulation
		path                                    % string for the save path
	end
	properties  %(SetAccess = private)
		% simulation results
		designPpide dps.parabolic.system.ppide_sys % system used for the design. (important for folding)
		observerFoldedPpide dps.parabolic.system.ppide_sys % system used for the observer design. (important for folding)
		observerPpide1 dps.parabolic.system.ppide_sys % first subsystem of folding observer
		observerPpide2 dps.parabolic.system.ppide_sys % second subsystem of folding observer
		simulated = 0;                          % If simulation has already been computed for each mode.
		simulatedTarget = 0;                    % If target System simulation has already been performed
		t                                       % Simulation time
		tTransformed                            % Time belonging to transformed state
		tTarget									% Time of the simulation of the target system
		tTargetObserver                         % Time of the simulation of the observer target system
		xE                                      % Extended simulation state
		x                                       % Plant states
		xQuantity quantity.Discrete				% simulation result as quantity
		xNorm                                   % Norm of the states
		xTransformed                            % Simulation result transformed to target system
		xTransformedQuantity quantity.Discrete  % Simulation result transformed to target system as quantity
		xTransformedNorm                        % Norm of transformed state
		eTransformedObserver                    % Simulation result transformed to target system
		eTransformedObserverQuantity quantity.Discrete  % Simulation result transformed to target system as quantity
		xETarget                                % States of the simulated target system
		xETargetQuantity quantity.Discrete      % States of the simulated target system
		xETargetObserver                        % States of the simulated observer target system 
		xETargetObserverQuantity quantity.Discrete      % States of the simulated observer target system 
		xTargetNorm								% Norm of target system states
		xHat                                    % Observed plant sates
		xHatQuantity quantity.Discrete          % Observed plant sates
		xHatFolded                              % folded observer states
		v                                       % Signal model states
		u                                       % Input of system
		uDelayed                                % Input after the input delay, acting at ppide-input
		ym                                      % Measurement of the system
		ymDelayed                               % Output after the output delay, acting at observer input
		wHat                                    % Observed states of measurement delay model
		vHat                                    % Observed signal model states	
		vHatInt                                 % States of the internal model 
		ppideApproximation dps.parabolic.system.ppide_approximation   % Approximation of the ppide
		simPpideApproximation                   % Approximation of the simulation ppide
		observerKernel dps.parabolic.control.backstepping.ppide_kernel  % observerKernel
		observerKernel1 dps.parabolic.control.backstepping.ppide_kernel  % in the folding case, two kernels with order n=n/2 are stored.
		observerKernel2 dps.parabolic.control.backstepping.ppide_kernel
		observerKernelFlag = 0;                 % Shows if observerKernel was computed
		dualKernel                              % dual Kernel as a temporary result of the kernel computation
		dualKernel1                              % dual Kernel as a temporary result of the kernel computation
		dualKernel2                             % dual Kernel as a temporary result of the kernel computation
		observer                                % ppide_observer
		observer1                               % store single observers separately for folding
		observer2                               
		observerFlag = 0;                       % True if observer has been computed
		observerApproximation                   % Approximation of the observer
		observerApproximation1                  % Approximation of the observer
		observerApproximation2                  % Approximation of the observer
		controllerKernel dps.parabolic.control.backstepping.ppide_kernel   % ppide_kernel
		controllerKernelFlag = 0;               % True if controllerKernel was computed already
		controller                              % ppide_controller
		controllerFlag = 0;                     % True if controller is computed already
		controllerForInputDelayFlag = 0;		% True if considers input delay
		observerForOutputDelayFlag = 0;			% True if considers output delay
		Kv                                      % Signal model state feedback gain
		KvFlag = 0;								% True if regulator equations are already solved
		Pi                                      % Solution of the regulator equations
		Piz                                     % Derivative of solution of regulator equations
		Pizz                                    % 2nd derivative
		KvHat                                   % feedback gain of internal model states
		KvQPlant                                % state feedback part of internal model control
		KvHatFlag = 0;                          % Gain of internal model was computed
		Q                                       % Solution of decoupling equations (robust) in original coordinates
		QTil                                    % Solution of decoupling equations (robust)
		D									    % Determinant of regulator equations
		DRobust                                 % Determinant of transfer function u->y (robust)
		Gamma                                   % Solution of the decoupling equations (observer)
		GammaFlag = 0;                          % Shows if gamma was computed
		saveStr;                                % path and filename to store object
	end
	properties(Access = private)
     	simParsPriv = [];
     	ppidePriv = [];
		simPpidePriv = [];
     	sMPriv = [];
     	x0Priv = [];
     	v0Priv = [];
		referenceSignalPriv = [];
     	tvPriv = 0; 
     	xHat0Priv = [];
     	vHat0Priv = [];
     	plotParsPriv = struct('plot_res_1D', 50, 'plot_res_2D_lines', ...
										150', 'plot_res_2D_num_lines', 20);
     	useStateFeedbackPriv  = 0;
     	useOutputRegulationPriv = 0;
     	useStateObserverPriv = 0;
		useObserverInFeedbackPriv = 0;
     	useDisturbanceObserverPriv = 0;
     	useReferenceObserverPriv = 0;
		useIntModPriv = 0;
		useStateFeedbackForInputDelayPriv = 0;
		useStateObserverForOutputDelayPriv = 0;
		namePriv = 'sim';
		pathPriv = './'; 
	end
	
	methods % Constructor and setter: stored in external function create_ppide_simulation
		function obj = ppide_simulation(ppide, varargin)
			% ppide_simulation create new ppide_simulation object.
			%   obj = ppide_simulation(ppide, 'paramete_name', value...)
			%     creates a new ppide_simulation object. 
			%     The object is used to perform the simulation of the
			%     backstepping based control and output regulation of
			%     coupled parabolic systems.			
			if nargin > 1 && isa(varargin, 'cell') % when varargin was passed as a variable
				if isa (varargin{1}, 'cell')
					varargin = varargin{1};
				end
			end
			obj = obj.create_ppide_simulation(ppide, varargin);
		end % function
		
		obj = create_ppide_simulation(obj, ppide, varargin); % Helper function for constructor
		obj = setPars(obj, ppide, varargin);                 % Change parameters of ppide_simulation
		designPpide = foldPpide(obj,varargin);			 % create parameters of folded system
		ranges = findAppropriateFoldingPoint(obj,lambdaInterp,n,ndisc,ndiscT); % get possible folding points
		res = checkEigenvalues(obj);
	end % methods 1
	
	methods % Simulation
		obj = initializeSimulation(obj)  % Make sure all computations are done before simulating
		obj = simulate(obj)              % Proceed the simulation
		obj = simTarget(obj)             % Simulate the target system
		obj = deleteSimulationResults(obj, parameterList)	% delets data from previous simulations.
	end
		
	methods % Computation of controller etc.
		obj = computeControllerKernel(obj)                                     % Compute state feedback kernel
		obj = storeObserverKernel(obj)                                         % Compute observer kernel in its final form if it hasnt been done yet and set flags
		[GH_obs, GH_dual] = compute_observer_kernel(obj, system, zdisc, GH_dual)  % Compute the observer kernel explicitly
		GH_dual = compute_dual_kernel(obj, system, zdisc)                        % Compute the dual kernel which is needed for the observer kernel
	end
	
	methods % Plot functions
		obj = plotOutput(obj)                          % Plot outputs of the system
		obj = plotSigModStates(obj)                    % Plot signal model states
		obj = plotDistEstimation(obj)                  % Plot estimation of the disturbance
		obj = meshState(obj, varargin)                 % 3D-Plot of the system states, varargin is passed to mesh
		obj = meshStateSparse(obj, varargin)           % 3D-Plot of the system states, varargin is passed to mesh_sparse
		obj = meshObserverError(obj,numT,numZ,varargin)% 3D-Plot of the state observer error
		obj = meshObserverErrorSparse(obj)             % 3D-Plot of the state observer error
		obj = meshPiV(obj)                             % 3D-Plot of the decoupling target
		obj = plotTargetAndTransformed(obj, zdiscPlot)  % Plot original system states and transformed ones
		obj = meshTargetAndTransformed(obj, numT,numZ,varargin)  % Plot original system states and transformed ones
		obj = plotDZetaK(obj)	                       % Plot analytical derivatives of the kernel solution (TODO!)
		obj = plotU(obj)                               % Plot input signal
		obj = plotRegEq(obj)                           % Analyse solution of the regulator equations
		obj = plotNorm(obj,relative)				   % Analyse norm of solution
		obj = plotObserverNorm(obj,factor)			   % Analyse norm of solution
	end
	
	methods %post production of simulation data
		%SPLITSTATES extracts the states resulting from simulations which have the
		%dimenions x \in R^{numel(t), ppide.n * ndisc} into the individual states
		%x_i combined in xSplitted \in R^{numel(t), ndisc, ppide.n}.
		xSplitted = splitStates(obj, x)
	end
	
	methods % Check Differences
		function result = ppideDiffControllerKernel(obj, ppide)
			import misc.*
			 % Check if the new ppide contains modifications that require a
			 % new computation of thecontroller kernel.
			 % TODO: add c_disc, not yet implemented!
			parlist_ppide = {'l_disc','a_disc','b_op1'...
				'b_1','a_0_disc','c_disc','f'};
			parlist_ctrl_pars = {...
				'mu', 'zdiscCtrl', 'it_max', 'it_min', 'tol', 'g_strich_disc','g_strich',...
				'min_xi_diff', 'makeTargetNeumann', 'eliminate_convection', 'quasiStatic',...
				'tDiscKernel','foldingPoint','maxNumXi'};
			result1 = isAnyDifferent(obj.ppide, ppide, parlist_ppide);
			result2 = isAnyDifferent(obj.ppide.ctrl_pars, ppide.ctrl_pars, parlist_ctrl_pars);
			% treat special because quantity is used!
			if ~isempty(result1) && ~isempty(result2)
				result = [result1 ', ' result2];
% 			elseif ~isempty(result1) && ~isempty(result3)
% 				result = [result1 ', ' result3];
% 			elseif ~isempty(result2) && ~isempty(result3)
% 				result = [result2 ', ' result3];
% 			elseif ~isempty(result2) && ~isempty(result2) && ~isempty(result3)
% 				result = [result1 ', ' result2 ', ' result3];
			else
				result = [result1 result2];
			end
		end
		function result = ppideDiffObserverKernel(obj, ppide)
			import misc.*
			% Todo: change when moving to a generalized observer!
			parlist_ppide = {'l_disc', 'a_disc', 'b_op2'...
				'f_disc'};
			parlist_ctrl_pars = {...
				'mu_o', 'zdiscObsKernel', 'it_max', 'it_min', 'tol', 'g_strich_disc', 'min_xi_diff'};
			result1 = isAnyDifferent(obj.ppide, ppide, parlist_ppide);
			result2 = isAnyDifferent(obj.ppide.ctrl_pars, ppide.ctrl_pars, parlist_ctrl_pars);
			if ~isempty(result1) && ~isempty(result2)
				result = [result1 ', ' result2];
			else
				result = [result1 result2];
			end
		end
		function result = ppideDiffKv(obj, ppide)
			import misc.*
			parlist_ppide = {'c_op', 'g_1_disc', 'g_2', 'g_4'};
			parlistCtrlPars = {'zdiscRegEq'};
			result1 = isAnyDifferent(obj.ppide, ppide, parlist_ppide);
			result2 = isAnyDifferent(obj.ppide.ctrl_pars, ppide.ctrl_pars, parlistCtrlPars);
			if ~isempty(result1) && ~isempty(result2)
				result = [result1 ', ' result2];
			else
				result = [result1 result2];
			end
		end
		function result = ppideDiffKvHat(obj, ppide)
			import misc.*
			parlist_ppide = {'c_op', 'b_op2'};
			parlistCtrlPars = {'zdiscRegEq', 'By', 'eigInt'};
			result1 = isAnyDifferent(obj.ppide, ppide, parlist_ppide);
			result2 = isAnyDifferent(obj.ppide.ctrl_pars, ppide.ctrl_pars, parlistCtrlPars);
			if ~isempty(result1) && ~isempty(result2)
				result = [result1 ', ' result2];
			else
				result = [result1 result2];
			end
		end
		function result = ppideDiffController(obj, ppide)
			import misc.*
			% Todo: change when moving to a generalized observer!
			parlist_ppide = {'b_op2', 'ndisc'};
			if isa(ppide, 'dps.parabolic.system.Delay')
				parlist_ppide{end+1} = 'inputDelay';
			end
			result = isAnyDifferent(obj.ppide, ppide, parlist_ppide);
		end
		function result = ppideDiffObserver(obj, ppide)
			import misc.*
			% Todo: change when moving to a generalized observer!
			parlist_ppide = properties(ppide);
			if isa(ppide, 'dps.parabolic.system.Delay')
				parlist_ppide{end+1} = 'outputDelay';
			end
			result = isAnyDifferent(obj.ppide, ppide, parlist_ppide);
		end
		function result = ppideDiffGamma(obj, ppide)
			import misc.*
			% Todo: change when moving to a generalized observer!
			warning('Hier fehlt noch �nderung in Eigenwerten!')
			parlist_ppide = {'b_op1', 'g_1_disc', 'g_2', 'g_3'};
			result = isAnyDifferent(obj.ppide, ppide, parlist_ppide);
		end
		function result = sMDiffKv(obj, sM)
			import misc.*
			% Todo: change when moving to a generalized observer!
			parlist = {'S', 'pd', 'pr'};
			result = isAnyDifferent(obj.sM, sM, parlist);
		end
		function result = sMDiffKvHat(obj, sM)
			import misc.*
			% Todo: change when moving to a generalized observer!
			parlist = {'S'};
			result = isAnyDifferent(obj.sM, sM, parlist);
		end
		function result = sMDiffGamma(obj, sM)
			import misc.*
			% Todo: change when moving to a generalized observer!
			parlist = {'Sd', 'pd_til'};
			result = isAnyDifferent(obj.sM, sM, parlist);
		end
	end % methods: diff functions
	
	methods % setters/getters
		% Setters/Getters are created with the function createSetters.
		function obj = set.simPars(obj, value)
			if ~isequal(obj.simPars, value)
				if obj.simulated
					fprintf('simPars have changed, needs new simulation.\n')
					obj.simulated = 0;
				end
				if obj.simulatedTarget
					obj.simulatedTarget = 0;
				end
			end
			obj.simParsPriv = value;
		end
		function value = get.simPars(obj)
			value = obj.simParsPriv;
		end
			
		function obj = set.ppide(obj, value)		
			import misc.*
% 			% Flags zur�cksetzen
			% TODO: Wenn sich n ge�ndert hat alles zur�cksetzen!
			if isa(obj.ppide, 'dps.parabolic.system.ppide_sys')
				% only if there was a ppide assigned already
				if obj.controllerKernelFlag
					% only if kernel has already been computed
					chged = obj.ppideDiffControllerKernel(value);
					if ~isempty(chged)
						if isa(value, 'dps.parabolic.system.ppide_sys')
							% if resolution has changed, a new saveStr is
							% needed!
							if isAnyDifferent(obj.ppide.ctrl_pars, value.ctrl_pars, {'zdiscCtrl'})
								ds = datestr(now, 'yy-mm-dd__HH-MM-ss');
								obj.saveStr = ...
									[obj.path...             % path
									ds...                    % new date
									obj.name...              % name
									'_n'...		% num States
									num2str(obj.ppide.n)...
									'_nc'...
									num2str(length(value.ctrl_pars.zdiscCtrl))... % ndiscCtrl
									'_noK'... % rest unchanged
									num2str(length(value.ctrl_pars.zdiscObsKernel))];
							end
						end
						obj.controllerKernelFlag = 0;
						fprintf(['Controller kernel needs to be computed again!\n\t' ...
							'Changed: ' chged '\n']);
						obj.simulated = 0;
						obj.simulatedTarget = 0;
					end
				end
				if obj.observerKernelFlag
					% only if kernel has already been computed
					chged = obj.ppideDiffObserverKernel(value);
					if ~isempty(chged)
						if isa(value, 'dps.parabolic.system.ppide_sys')
							% if resolution has changed, a new saveStr is
							% needed!
							if isAnyDifferent(obj.ppide.ctrl_pars, value.ctrl_pars, {'zdiscObsKernel'})
								ds = datestr(now, 'yy-mm-dd__HH-MM-ss');
								obj.saveStr = ...
									[obj.path...             % path
									ds...                    % new date
									obj.name...              % name
									'_n'...		% num States
									num2str(obj.ppide.n)...
									'_nc'...
									num2str(length(value.ctrl_pars.zdiscCtrl))... % ndiscCtrl
									'_noK'... % rest unchanged
									num2str(length(value.ctrl_pars.zdiscObsKernel))];
							end
						end
						obj.observerKernelFlag = 0;
						fprintf(['Observer kernel needs to be computed again!\n\t' ...
							'Changed: ' chged '\n']);
						obj.simulated = 0;
						obj.simulatedTarget = 0;
					end
				end
				% TODO: KvHat Flag!
				if obj.KvFlag
					chged = obj.ppideDiffKv(value);
					if ~isempty(chged)
						obj.KvFlag = 0;
						fprintf(['Regulator equations need to be solved again!\n\t' ...
							'Changed: ' chged '\n']);
						obj.simulated = 0;
						obj.simulatedTarget = 0;
					end
				end
				if obj.KvHatFlag
					chged = obj.ppideDiffKvHat(value);
					if ~isempty(chged)
						obj.KvHatFlag = 0; 
						fprintf(['Robust decoupling equations need to be solved again!\n\t' ...
							'Changed: ' chged '\n']);
						obj.simulated = 0;
						obj.simulatedTarget = 0;
					end
				end
				if obj.controllerFlag
					chged = obj.ppideDiffController(value);
					if ~isempty(chged)
						obj.controllerFlag = 0;
						fprintf(['Controller needs to be computed again!\n\t' ...
							'Changed: ' chged '\n']);
						obj.simulated = 0;
						obj.simulatedTarget = 0;
					end
				end
				if obj.GammaFlag
					chged = obj.ppideDiffGamma(value);
					if ~isempty(chged)
						obj.GammaFlag = 0;
						fprintf(['Decoupling equations need to be solved again!\n\t' ...
							'Changed: ' chged '\n']);
						obj.simulated = 0;
						obj.simulatedTarget = 0;
					end
				end
				if obj.observerFlag
					chged = obj.ppideDiffObserver(value);
					if ~isempty(chged)
						obj.observerFlag = 0;
						fprintf(['Observer needs to be computed again!\n\t' ...
							'Changed: ' chged '\n']);
						obj.simulated = 0;
						obj.simulatedTarget = 0;
					end
				end
				if ~isequalF(obj.ppide, value)
					% needs new approximation
					obj.ppideApproximation=dps.parabolic.system.ppide_approximation.empty;
					% and new simulation
					if obj.simulated
						obj.simulated = 0;
						fprintf('Ppide has changed, needs new simulation.\n')
					end
					obj.simulatedTarget = 0;
				end
			end
			obj.ppidePriv = value;
		 end
		 function value = get.ppide(obj)
			value = obj.ppidePriv;
		 end
		 function obj = set.simPpide(obj, value)		
			import misc.*
% 			% Flags zur�cksetzen
			if isa(obj.ppide, 'dps.parabolic.system.ppide_sys')
				% only if there was a ppide assigned already
				if ~isequalF(obj.simPpide, value)
					% needs new approximation
					obj.simPpideApproximation=[];
					% and new simulation
					if obj.simulated
						obj.simulated = 0;
						fprintf('SimPpide has changed, needs new simulation.\n')
					end
					obj.simulatedTarget = 0;
				end
			end
			obj.simPpidePriv = value;
		 end
		 function value = get.simPpide(obj)
			value = obj.simPpidePriv;
		 end

		 function obj = set.sM(obj, value)
			% Flags zur�cksetzen
			if ~isempty(value) && ~isa(value, 'dps.parabolic.control.outputRegulation.signalModel')
				error('sM must be of type dps.parabolic.control.outputRegulation.signalModel.')
			end
			if isa(obj.sM, 'dps.parabolic.control.outputRegulation.signalModel')
				% only if there was a signal model assigned already
				if obj.KvFlag
					chged = obj.sMDiffKv(value);
					if ~isempty(chged)
						obj.KvFlag = 0;
						fprintf(['Regulator equations need to be solved again!\n\t' ...
							'Changed: ' chged '\n']);
					end
				end
				if obj.KvHatFlag
					chged = obj.sMDiffKvHat(value);
					if ~isempty(chged)
						obj.KvHatFlag = 0;
						fprintf(['Robust decoupling equations need to be solved again!\n\t' ...
							'Changed: ' chged '\n']);
					end
				end
				if obj.GammaFlag
					chged = obj.sMDiffGamma(value);
					if ~isempty(chged)
						obj.GammaFlag = 0;
						fprintf(['Decpupling equations need to be solved again!\n\t' ...
							'Changed: ' chged '\n']);
					end
				end
				if obj.observerFlag
					if ~isequal(obj.sM, value)
						obj.observerFlag = 0;
						fprintf(['Observer needs to be computed again!\n\t' ...
							'Changed: sM \n']);
					end
				end
				if obj.KvHatFlag
					if ~isequal(obj.sM, value)
						obj.KvHatFlag = 0;
						fprintf(['Robust gain needs to be computed again!\n\t' ...
							'Changed: sM \n']);
					end
				end
			else % set a new signalModel
				obj.observerFlag = 0;
				obj.GammaFlag = 0;
				obj.KvFlag = 0;
				obj.KvHatFlag = 0;
				obj.simulated = 0;
				obj.simulatedTarget = 0;
			end
			if ~isequal(obj.sM, value)
				if obj.simulated
					fprintf('Signal Model has changed, needs new simulation.\n')
				end
				obj.simulated = 0;
				obj.simulatedTarget = 0;
			end
			obj.sMPriv = value;
		 end
		 function value = get.sM(obj)
			value = obj.sMPriv;
		 end

		 function obj = set.x0(obj, value)
			if ~misc.isequalF(obj.x0, value)
				if obj.simulated
					fprintf('x0 has changed, needs new simulation.\n')
				end
				obj.simulated = 0;
				obj.simulatedTarget = 0;
			end
			obj.x0Priv = value;
		 end
		 function value = get.x0(obj)
			value = obj.x0Priv;
		 end

		 function obj = set.v0(obj, value)
			if ~isequal(size(obj.v0), size(value)) || max(any(obj.v0-value))
				% Here one could insert a dimension check of v0, but it
				% might be set before the signal model ist set. So it wont
				% work.
				if obj.simulated
					fprintf('v0 has changed, needs new simulation.\n')
				end
				obj.simulated = 0;
				obj.simulatedTarget = 0;
			end
			obj.v0Priv = value;
		 end
		 function value = get.v0(obj)
			value = obj.v0Priv;
		 end

		 function obj = set.tv(obj, value)
			if ~isequal(obj.tv, value)
				if obj.simulated
					fprintf('tv has changed, needs new simulation.\n')
				end
				obj.simulated = 0;
				obj.simulatedTarget = 0;
			end
			obj.tvPriv = value;
		 end
		 function value = get.tv(obj)
			value = obj.tvPriv;
		 end
		 
		 function obj = set.referenceSignal(obj, value)
			if ~misc.isequalF(obj.referenceSignal, value)
				if obj.simulated
					fprintf('referenceSignal has changed, needs new simulation.\n')
				end
				obj.simulated = 0;
				obj.simulatedTarget = 0;
			end
			if isempty(value) || isa(value, 'function_handle') 
				obj.referenceSignalPriv = value;
			else
				obj.referenceSignalPriv = @(t) value+0*t;
			end
		 end
		 function value = get.referenceSignal(obj)
			value = obj.referenceSignalPriv;
		 end

		 function obj = set.xHat0(obj, value)
			if ~isequal(obj.xHat0, value)
				if obj.simulated
					fprintf('xHat0 has changed, needs new simulation.\n')
				end
				obj.simulated = 0;
				obj.simulatedTarget = 0;
			end
			obj.xHat0Priv = value;
		 end
		 function value = get.xHat0(obj)
			value = obj.xHat0Priv;
		 end

		 function obj = set.vHat0(obj, value)
			if ~isequal(obj.vHat0, value)
				if obj.simulated
					fprintf('vHat0 has changed, needs new simulation.\n')
				end
				obj.simulated = 0;
				obj.simulatedTarget = 0;
			end
			obj.vHat0Priv = value;
		 end
		 function value = get.vHat0(obj)
			value = obj.vHat0Priv;
		 end

		 function obj = set.plotPars(obj, value)
			obj.plotParsPriv = value;
		 end
		 function value = get.plotPars(obj)
			value = obj.plotParsPriv;
		 end

		 function obj = set.useStateFeedback(obj, value)
			if ~isequal(obj.useStateFeedback, value)
				if obj.simulated
					fprintf('USFB has changed, needs new simulation.\n')
				end
				obj.simulated = 0;
				obj.simulatedTarget = 0;
			end
			obj.useStateFeedbackPriv = value;
		 end
		 function value = get.useStateFeedback(obj)
			value = obj.useStateFeedbackPriv;
		 end

		 function obj = set.useOutputRegulation(obj, value)
			if ~isequal(obj.useOutputRegulation, value)
				if obj.simulated
					fprintf('UOR has changed, needs new simulation.\n')
				end
				obj.simulated = 0;
				obj.simulatedTarget = 0;
			end
			obj.useOutputRegulationPriv = value;
		 end
		 function value = get.useOutputRegulation(obj)
			value = obj.useOutputRegulationPriv;
		 end

		 function obj = set.useStateObserver(obj, value)
			if ~isequal(obj.useStateObserver, value)
				if obj.simulated
					fprintf('USO has changed, needs new simulation.\n')
				end
				obj.simulated = 0;
				obj.simulatedTarget = 0;
			end
			obj.useStateObserverPriv = value;
		 end
		 function value = get.useStateObserver(obj)
			value = obj.useStateObserverPriv;
		 end
		 
		 function obj = set.useObserverInFeedback(obj, value)
			if ~isequal(obj.useObserverInFeedback, value)
				if obj.simulated
					fprintf('ASO has changed, needs new simulation.\n')
				end
				obj.simulated = 0;
				obj.simulatedTarget = 0;
			end
			obj.useObserverInFeedbackPriv = value;
		 end
		 function value = get.useObserverInFeedback(obj)
			value = obj.useObserverInFeedbackPriv;
		 end

		function obj = set.useDisturbanceObserver(obj, value)
			if ~isequal(obj.useDisturbanceObserver, value)
				if obj.simulated
					fprintf('UDO has changed, needs new simulation.\n')
				end
				obj.simulated = 0;
				obj.simulatedTarget = 0;
			end
			obj.useDisturbanceObserverPriv = value;
		 end
		function value = get.useDisturbanceObserver(obj)
			value = obj.useDisturbanceObserverPriv;
		end

		function obj = set.useReferenceObserver(obj, value)
			if ~isequal(obj.useReferenceObserver, value)
				if obj.simulated
					fprintf('URO has changed, needs new simulation.\n')
				end
				obj.simulated = 0;
				obj.simulatedTarget = 0;
			end
			obj.useReferenceObserverPriv = value;
		end
		function value = get.useReferenceObserver(obj)
			value = obj.useReferenceObserverPriv;
		end
		
		function obj = set.useIntMod(obj, value)
			if ~isequal(obj.useIntMod, value)
				if obj.simulated
					fprintf('UIM has changed, needs new simulation.\n')
				end
				obj.simulated = 0;
				obj.simulatedTarget = 0;
			end
			obj.useIntModPriv = value;
		end
		function value = get.useIntMod(obj)
			value = obj.useIntModPriv;
		end
		
		function obj = set.useStateFeedbackForInputDelay(obj, value)
			if ~isequal(obj.useStateFeedbackForInputDelay, value)
				if obj.simulated
					fprintf('useStateFeedbackForInputDelay has changed, needs new simulation.\n')
				end
				obj.simulated = 0;
				obj.simulatedTarget = 0;
				obj.controllerFlag = 0;
			end
			obj.useStateFeedbackForInputDelayPriv = value;
		end
		function value = get.useStateFeedbackForInputDelay(obj)
			value = obj.useStateFeedbackForInputDelayPriv;
		end
		
		function obj = set.useStateObserverForOutputDelay(obj, value)
			if ~isequal(obj.useStateObserverForOutputDelay, value)
				if obj.simulated
					fprintf('useStateObserverForOutputDelay has changed, needs new simulation.\n')
				end
				obj.simulated = 0;
				obj.simulatedTarget = 0;
				obj.observerFlag = 0;
			end
			obj.useStateObserverForOutputDelayPriv = value;
		end
		function value = get.useStateObserverForOutputDelay(obj)
			value = obj.useStateObserverForOutputDelayPriv;
		end 
		
		
		function obj = set.path(obj, value)
			if find((value == '_'), 1)
				error('Path must not contain an underscore!')
			else
				if value(end)~='/'
					value = [value '/'];
				end
				if ~strcmp(obj.path, value)
					obj.pathPriv = value;
					% Savestring contains:
					% path dd-mm__HH-MM-SS_ name _nc ndiscKernel _noK
					% ndiscObserverKernel
					ds = datestr(now, 'yy-mm-dd__HH-MM-ss');
					obj.saveStr = ...
						[value... % path
						ds...                    % new date
						obj.name...                 % new name
						'_n'...		% num States
						num2str(obj.ppide.n)...
						'_nc'...
						num2str(length(obj.ppide.ctrl_pars.zdiscCtrl))... % ndiscCtrl
						'_noK'... % rest unchanged
						num2str(length(obj.ppide.ctrl_pars.zdiscObsKernel))];
				end
			end
		end
		function value = get.path(obj)
			value = obj.pathPriv;
		end
		
		function obj = set.name(obj, value)
			if ~strcmp(obj.name, value)
				% new name has been stored
				obj.namePriv = value;
				% Savestring contains:
				% path dd-mm__HH-MM-SS_ name _nc ndiscKernel _noK
				% ndiscObserverKernel
				ds = datestr(now, 'yy-mm-dd__HH-MM-ss');
				obj.saveStr = ...
					[obj.path... % path
					ds...                    % new date
					value...                 % new name
					'_n'...		% num States
					num2str(obj.ppide.n)...
					'_nc'...
					num2str(length(obj.ppide.ctrl_pars.zdiscCtrl))... % ndiscCtrl
					'_noK'... % rest unchanged
					num2str(length(obj.ppide.ctrl_pars.zdiscObsKernel))];
			end			
		end
		function value = get.name(obj)
			value = obj.namePriv;
		end		
	end
	
end




