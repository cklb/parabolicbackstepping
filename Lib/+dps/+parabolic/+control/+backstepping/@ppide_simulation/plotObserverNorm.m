function varargout = plotObserverNorm(obj,factor)
% plotObserverNorm plots the norms of observer error, transformed and traget system together
% with the desired decay rate.
% 
%   plotObserverNorm(factor) uses a fixed factor for decay = factor*e^{(mumax-mu) t}

if ~obj.simulated
	obj = obj.simulate();
end
if ~obj.simulatedTarget
	obj=obj.simTarget();
end

n=obj.ppide.n;
p = size(obj.ppide.b_2,2); 
if p > n
	folding=1;
	name = ['NormClosedLoopObserver, \check{z}_0 = ' num2str(obj.ppide.ctrl_pars.foldingPoint)]; 
else
	folding=0;
	name = 'NormClosedLoopObserver';
end


figure('Name','NormClosedLoopObserver')
hold on
title(name);
box on
t = obj.t;
t_target = obj.tTarget;
t_transformed = obj.tTransformed;
zdiscObs = obj.ppide.ctrl_pars.zdiscObs;
ndiscObs = length(zdiscObs);
zdiscTarget = obj.ppide.ctrl_pars.zdiscTarget;
ndiscTarget = length(zdiscTarget);
if (any(obj.ppide.cm_op.c_m(:))  || any(obj.ppide.cm_op.c_dm(:))) && obj.useStateObserver 
	% observerFolding
	zdiscKernel = obj.observerKernel1.zdisc;
else
	zdiscKernel = obj.observerKernel.zdisc;
end
ndiscKernel = length(zdiscKernel);
e = obj.xQuantity - obj.xHatQuantity;
% use same discretization everywhere!
eDiscSim = obj.ppide.sys2Sim(permute(e.on({zdiscObs,t},{'z','t'}),[2 3 1]));
eTransformedSim = obj.ppide.sys2Sim(permute(obj.eTransformedObserverQuantity.on({zdiscObs,t_transformed},{'z','t'}),[2 3 1]));
eTargetSim = obj.ppide.sys2Sim(permute(obj.xETargetObserverQuantity.on({zdiscObs,t_target},{'z','t'}),[2 3 1]));

e_quad_sum = 0;
e_transformed_quad_sum = 0;
e_target_quad_sum = 0;
lObs = misc.translate_to_grid(obj.ppide.l_disc,{zdiscObs});
lKernel =  misc.translate_to_grid(obj.ppide.l_disc,{zdiscKernel});
lTarget =  misc.translate_to_grid(obj.ppide.l_disc,{zdiscTarget});
for i=1:obj.ppide.n
	e_quad_sum = e_quad_sum + (permute(lObs(:,i,i,:),[4,1,2,3]).^(-1/2).*eDiscSim(:,(i-1)*ndiscObs+1:i*ndiscObs)).^2;
	e_transformed_quad_sum = e_transformed_quad_sum + (permute(lObs(:,i,i,:),[4,1,2,3]).^(-1/2).*eTransformedSim(:,(i-1)*ndiscObs+1:i*ndiscObs)).^2;
	e_target_quad_sum = e_target_quad_sum + (permute(lObs(:,i,i,:),[4,1,2,3]).^(-1/2).*eTargetSim(:,(i-1)*ndiscObs+1:i*ndiscObs)).^2;
% 		y_transformed_quad_sum = y_transformed_quad_sum + (permute(lKernel(:,i,i,:),[4,1,2,3]).^(-1/2).*y_transformed(:,(i-1)*ndiscKernel+1:i*ndiscKernel)).^2;
end

y_L2_norm = sqrt(numeric.trapz_fast(zdiscObs,e_quad_sum));
y_transformed_L2_norm = sqrt(numeric.trapz_fast(zdiscObs,e_transformed_quad_sum));
y_target_L2_norm = sqrt(numeric.trapz_fast(zdiscObs,e_target_quad_sum));


h1 = plot(t,y_L2_norm,'DisplayName','$\|e(t)\|$');
h2 = plot(t_transformed,y_transformed_L2_norm,'r','LineWidth',2,'DisplayName','transformiert');
yl = get(gca,'ylim');
set(gca,'ylim',yl);
if ~isempty(obj.xETarget)
	h3 = plot(t_target,y_target_L2_norm,'b--','LineWidth',2,'DisplayName','target_ist');	
end
too_small = 1;
savetyCounter = 1;
fac = y_transformed_L2_norm(1);
if folding 
	% achtung: geht natuerlich nur fuer konstante mu!
	muCur = obj.ppide.ctrl_pars.mu(1);
	if nargin > 1
		too_small=0;
		fac = factor;
		decay_t = fac*exp(-(muCur)*t);
	end
	while too_small
		decay_t = fac*exp(-(muCur)*t);
		if ~isempty(obj.xETarget)
			decay_t_target = fac*exp((muCur)*t_target);
		end
		decay_t_transformed = fac*exp((muCur)*obj.tTransformed.');
		if ~isempty(obj.xETarget)
			if ~any( (decay_t - y_L2_norm) < 0) && ~any( (decay_t_target - y_target_L2_norm) < 0) && ~any( (decay_t_transformed - y_transformed_L2_norm) < 0)
				too_small=false;
			end
		else
			if ~any( (decay_t - y_L2_norm) < 0) && ~any( (decay_t_transformed - y_transformed_L2_norm) < 0)
				too_small=false;
			end
		end
		muCur = muCur*0.99;
		if savetyCounter > 300
			warning('Could not find sufficiently large decay with 300 tries.')
			break
		end
		savetyCounter = savetyCounter + 1;
	end
	h4 = plot(t,decay_t,'lineWidth',1.5,'Color','green');
	if ~isempty(obj.xETarget)
		legend([h2 h3 h4],{'$\|\T[e(t),t]\|$','$\|\tilde{e}(t)\|$',['$\e^{-' sprintf('%.2f',muCur) 't}$']},'interpreter','none');
	else
		legend([h2 h4],{'$\|\T[e(t),t]\|$',['$\e^{-' sprintf('%.2f',muCur) 't}$']},'interpreter','none');
	end
else
	mu_max = max(real(obj.ppide.eigTarget(obj.ppide.ctrl_pars.mu_o)));
% 	plot(t_target,1.5*max(y_target_L2_norm)*x_target_soll,'g:','lineWidth',2,'DisplayName','target_soll')
	if nargin > 1
		too_small=0;
		fac = factor;
		decay_t = fac*exp((mu_max)*t);
	end
	while too_small
		decay_t = fac*exp((mu_max)*t);
		if ~isempty(obj.xETarget)
			decay_t_target = fac*exp((mu_max)*t_target);
		end
		decay_t_transformed = fac*exp((mu_max)*obj.tTransformed.');
		if ~isempty(obj.xETarget)
			if ~any( (decay_t - y_L2_norm) < 0) && ~any( (decay_t_target - y_target_L2_norm) < 0) && ~any( (decay_t_transformed - y_transformed_L2_norm) < 0)
				too_small=false;
			end
		else
			if ~any( (decay_t - y_L2_norm) < 0) && ~any( (decay_t_transformed - y_transformed_L2_norm) < 0)
				too_small=false;
			end
		end
		fac = fac*1.01;
		if savetyCounter > 300
			warning('Could not find sufficiently large decay with 300 tries.')
			break
		end
		savetyCounter = savetyCounter + 1;
	end
	h4 = plot(t,decay_t,'lineWidth',1.5,'Color','green');
% legend([h1 h2 h3],{'$\|x(t)\|$','$\|\T[x(t)]\|$','$\|\tilde{x}(t)\|$'});
	if ~isempty(obj.xETarget)
		legend([h2 h3 h4],{'$\|\T[e(t),t]\|$','$\|\tilde{e}(t)\|$',['$' sprintf('%.2f',fac) '\e^{\mymucmd}$']},'interpreter','none');
	else
		legend([h2 h4],{'$\|\T[e(t),t]\|$',['$' sprintf('%.2f',fac) '\e^{\mymucmd}$']},'interpreter','none');
	end
end
% ylim([0 1.1*max(obj.xNorm(:))])
xlabel('$t$')
if nargout>0
	varargout{1} = obj;
end

end % function