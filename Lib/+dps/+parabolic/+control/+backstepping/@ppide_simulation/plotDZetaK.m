function varargout=plotDZetaK(obj)
	if ~obj.simulated
		obj = obj.simulate();
	end
	ndiscPlant = obj.ppide.ndisc;
	zdiscPlant = linspace(0,1,ndiscPlant);
	zdisc = obj.controllerKernel.zdisc;
	ndisc = length(zdisc);
	numtKernel = size(obj.controllerKernel.value{1,1}.K,3);
	figure('name','Analytisches_und_numerisches_dzeta_K_z_0')
	hold on
	id = 1;
	n = obj.ppide.n;
	for i=1:n
		for j=1:n
			dzeta_K_z_0_plant_ana{i,j} = interp1(zdisc,obj.controllerKernel.value{i,j}.dzeta_K_z_0,zdiscPlant);
			for z_idx = 1:ndisc
				grad_op =  numeric.get_grad_op(zdisc(1:z_idx));
				for t_idx = 1:numtKernel
				dzeta_K_plant(z_idx,1:z_idx,t_idx) = (grad_op*obj.controllerKernel.value{i,j}.K(z_idx,1:z_idx,t_idx).').';
				dzeta_K_z_0(z_idx,t_idx) = dzeta_K_plant(z_idx,1,t_idx);
				end
			end
			dzeta_K_z_0_plant{i,j} = interp1(zdisc,dzeta_K_z_0,zdiscPlant);
			subplot(n,n,id)
			hold on
			if numtKernel > 1
				numPlots = 3;
				inc = (numtKernel-1)/(numPlots-1);
				for plotIdx = 1:numPlots
					tIdx = 1 + floor((plotIdx-1)*inc);
					plot(zdiscPlant,dzeta_K_z_0_plant{i,j}(:,tIdx),'g','DisplayName','Num')
					plot(zdiscPlant,dzeta_K_z_0_plant_ana{i,j}(:,tIdx),'r','DisplayName','An.')
				end
			else
				plot(zdiscPlant,dzeta_K_z_0_plant{i,j},'g','DisplayName','Num')
				plot(zdiscPlant,dzeta_K_z_0_plant_ana{i,j},'r','DisplayName','An.')
			end
			legend('-DynamicLegend')
			id = id+1;
		end
	end
	if nargout>0
		varargout{1} = obj;
	end
end