function obj = deleteSimulationResults(obj, parameterList)
%deleteSimulationResults sets properties named in parameterList to empty.
	for it = 1 : numel(parameterList)
		obj.(parameterList{it}) = [];
	end	
end