function obj = plotDistEstimation(obj)
	if ~obj.simulated
		obj = obj.simulate;
	end
	figure('name','RealAndObservedDisturbance')
	hold on
	plot(obj.t,(obj.sM.pd*obj.v.').','DisplayName','$d$');
	plot(obj.t,(obj.sM.pd*obj.vHat.').','DisplayName','$\hat{d}$');
	xlabel('$t$')

end
