function obj=simTarget(obj)			
	import dps.parabolic.system.*
	import misc.*
	import numeric.*
	import dps.parabolic.control.backstepping.*
	obj = initializeSimulation(obj);
	obj = obj.deleteSimulationResults({'tTarget','xETarget'});
	
	if any(obj.tv>obj.simPars.tend)
		error('obj.tv contains times greater than simPars.tend!')
	end
	
	n=obj.ppide.n; % normal n;
	if size(obj.ppide.b_2,2)> n
		folding=1;
		ppide = obj.ppide;
		n = ppide.n;
		designPpide = obj.ppide;
		nF = designPpide.n;
	else
		folding=0;
		ppide = obj.ppide;
	end
	
	ndiscPlant = ppide.ndisc;
	zdiscPlant = linspace(0,1,ndiscPlant);
	
	ndiscPars = ppide.ndiscPars;
	zdiscPars = linspace(0,1,ndiscPars);
		
	tDisc = linspace(0,1,ppide.ndiscTime);
	% Raender: Randbedingungen werden entkoppelt bzw. eliminiert
	% evaluate time-dependent BC
	Bdl = ppide.b_op1.b_d;
	Bdr = ppide.b_op2.b_d;
	if isa(Bdl,'function_handle')
		BdlDisc = permute(misc.eval_pointwise(Bdl,tDisc),[2 3 1]);
	else
		BdlDisc = Bdl;
	end
	if isa(Bdr,'function_handle')
		BdrDisc = permute(misc.eval_pointwise(Bdr,tDisc),[2 3 1]);
	else
		BdrDisc = Bdr;
	end
	% may be 1 or numT but all the same!
	numTBdl = size(BdlDisc,3);
	numTBdr = size(BdrDisc,3);
	Bnl =  ppide.b_op1.b_n;
	Bnr =  ppide.b_op2.b_n;
	
	% Anfangsbedingung in Zielkoordinaten transformieren
	% 1. Interpolieren, falls Aufl�sungen unterschiedliche 
	x0_fine = 0;
	if ~isempty(obj.x0)
		if isa(obj.x0,'function_handle')
			x0_disc_temp = eval_pointwise(obj.x0,zdiscPlant);
			x0_disc = x0_disc_temp(:);
			if length(x0_disc)~=obj.ppide.n*ndiscPlant
				error('Size of x0 did not fit!')
			end
		else
			x0_disc = obj.x0;
			if size(x0_disc,1)~= obj.ppide.n*ndiscPlant 
				error(['size(x0_disc,1)=' num2str(size(x0_disc,1)) ' ~= n*ndiscPlant=' num2str(obj.ppide.n*ndiscPlant)])
			elseif size(x0_disc,2)~= 1
				error('size(x0) must be [n*ndiscPlant,1], or x0 must be an anonymous function.');
			end
		end
	else % if no initial value is set, it is assumed as zero.
		x0_disc = zeros(obj.ppide.n*ndiscPlant,1);
	end
	
	% left BC of target system:
	% State feedback control: left BC needs to be diagonal anyway.
	% Right BC can be arbitrary but is made diagonal or eliminated
	mTN = ppide.ctrl_pars.makeTargetNeumann;
	Bdl_til = zeros(n);
	Bdr_til = zeros(n);
	if numTBdl > 1
		for i=1:n
			if Bnl(i,i) ~= 0
				Bdl_til(i,i) = 0; % Robin eliminated if time dependent
			else
				Bdl_til(i,i) = 1; % Dirichlet is Dirichlet
			end
		end
	else
		for i=1:n
			if Bnl(i,i) ~= 0 
				Bdl_til(i,i) = ~mTN * Bdl(i,i);
			else
				Bdl_til(i,i) = 1;
			end
		end
	end
	Bnl_til = Bnl;
	b_op1T = boundary_operator('z_b',0,'b_d',Bdl_til,'b_n',Bnl_til);
	if numTBdr > 1
		for i=1:n
			if Bnr(i,i) ~= 0
				Bdr_til(i,i) = 0; % Robin eliminated if time dependent
			else
				Bdr_til(i,i) = 1; % Dirichlet is Dirichlet
			end
		end
	else
		for i=1:n
			if Bnr(i,i) ~= 0 
				Bdr_til(i,i) = ~mTN * Bdr(i,i);
			else
				Bdr_til(i,i) = 1;
			end
		end
	end
	Bnr_til = Bnr;
	b_op2T = boundary_operator('z_b',1,'b_d',Bdr_til,'b_n',Bnr_til);
	
	% Beobachterzielsystem simulieren:
	
	if obj.useStateObserver
		no = n;
		if any(obj.ppide.cm_op.c_m(:))  || any(obj.ppide.cm_op.c_dm(:)) % folding 
			observerFolding = 1;
			z_m = obj.ppide.cm_op.z_m;
			zdiscObsKernel = obj.observerKernel1.zdisc; % easier
			no = 2*n;
		else
			observerFolding = 0;
			zdiscObsKernel = obj.observerKernel.zdisc; % easier
		end
		
		if observerFolding
			nFold = 2;
			a0_barVec{1} = obj.observerKernel1.a0_bar;
			a0_barVec{2} = obj.observerKernel2.a0_bar;
			obsPpide{1} = obj.observerPpide1;
			obsPpide{2} = obj.observerPpide2;
						
			b_opLeft{1} = obj.observerPpide1.b_op1;
			b_opLeft{2} = obj.observerPpide2.b_op1;
			% right BC: depends on mTN
			if ~obj.ppide.ctrl_pars.makeTargetNeumann
				b_opRight{1} = obj.observerPpide1.b_op2;
				b_opRight{2} = obj.observerPpide2.b_op2;
			else
				E1 = ppide.b_op1.Sd;
				E2 = ppide.b_op1.Sr;
				Sd = ppide.b_op2.Sd;
				Sr = ppide.b_op2.Sr;
				Bd1br = E1*E1.';
				Bd2br = Sd*Sd.';					  
				Bn1br = E2*E2.';
				Bn2br = Sr*Sr.';
				b_opRight{1} = dps.parabolic.system.boundary_operator(...
					'z_b',1,'b_d',Bd1br,'b_n',Bn1br);
				b_opRight{2} = dps.parabolic.system.boundary_operator(...
					'z_b',1,'b_d',Bd2br,'b_n',Bn2br);
	
			end
			
			obsKern{1} = obj.observerKernel1;
			obsKern{2} = obj.observerKernel2;
			zdiscKernel = obj.observerKernel1.zdisc;
		else
			nFold = 1;
			a0_barVec{1} = obj.observerKernel.a0_bar;
			obsPpide{1} = obj.ppide;
			b_opLeft{1} = b_op1T;
			b_opRight{1} = b_op2T;
			obsKern{1} = obj.observerKernel;
			zdiscKernel = obj.observerKernel.zdisc;
		end
		tic;
		fprintf('Starting Simulation of target system (observer)...\n');
		% Observer Kernel resolution
		
		ndiscKernel = length(zdiscKernel);
		% Observer realisation resolution
		ndiscObs = length(ppide.ctrl_pars.zdiscObs);
		zdiscObs = linspace(0,1,ndiscObs);
		% Observer target system resolution 
		zdiscTarget = ppide.ctrl_pars.zdiscTarget;
		ndiscTarget = length(zdiscTarget);
		
		% Anfangsbedingung in Zielkoordinaten transformieren
		% 1. Interpolieren, falls Aufl�sungen unterschiedliche 
		xHat0_fine = 0;
		if ~isempty(obj.xHat0)
			if isa(obj.xHat0,'function_handle')
				xHat0_disc_temp = eval_pointwise(obj.xHat0,zdiscObs);
				xHat0_disc = xHat0_disc_temp(:);
				if length(xHat0_disc)~=no*ndiscObs
					error('Size of x0 did not fit!')
				end
			else
				xHat0_disc = obj.x0;
				if size(xHat0_disc,1)~= n*ndiscObs 
					error(['size(x0_disc,1)=' num2str(size(xHat0_disc,1)) ' ~= n*ndiscObs=' num2str(n*ndiscObs)])
				elseif size(xHat0_disc,2)~= 1
					error('size(x0_obs) must be [n*ndiscObs,1], or x0 must be an anonymous function.');
				end
			end
		else % if no initial value is set, it is assumed as zero.
			xHat0_disc = zeros(n*ndiscObs,1);
		end
		% Attention: Backstepping transformation with upsampled IC gives strange result
		for i=1:no
			xHat0_fine((i-1)*ndiscKernel+1:i*ndiscKernel) = interp1(zdiscObs,xHat0_disc((i-1)*ndiscObs+1:i*ndiscObs),zdiscKernel);
		end

		% IC is passed for the folded observers!
		if observerFolding
			x0Vec = ppide.sim2Sys(x0_disc.');
			x0_vecObs = misc.fold(x0Vec,z_m,2,'nFolded',ndiscObs,'preserve',true);
			
% 			xResZ1 = x0_vecObs(:,:,:,1); % these are the values until nearly the folding point.
			xHat0_vec = ppide.sim2Sys(xHat0_disc.',no);
% 			xHat1 = xHat0_vec(:,:,1:n);
% 			xHat1Only = permute(interp1(zdiscObs(2:end),permute(xHat1(:,2:end,:),[2 1 3]),linspace(zdiscObs(2),1,ndiscObs)),[2 1 3]);
% 			e1 = permute(interp1([0 linspace(zdiscObs(2),1,ndiscObs)],...
% 				permute(cat(2,zeros(size(xResZ1,1),1,n),(xResZ1 - xHat1Only)),[2 1 3]),...
% 					zdiscObs),[2 1 3]);
% 			e2 = x0_vecObs(:,:,:,2) - xHat0_vec(:,:,n+1:end);
% 			e0Folded = [];
% 			e0Folded(:,:,:,1) = e1;
% 			e0Folded(:,:,:,2) = e2;
% 			e0Folded = permute(misc.translate_to_grid(...
% 				permute(e0Folded,[2 1 3 4]),zdiscKernel),[2 1 3 4]);
			e0FoldedRes = x0_vecObs-reshape(xHat0_vec,size(xHat0_vec,1),size(xHat0_vec,2),n,2);
			e0Folded = permute(misc.translate_to_grid(...
				permute(e0FoldedRes,[2 1 3 4]),zdiscKernel),[2 1 3 4]);
		else
			xHat0_vec = ppide.sim2Sys(xHat0_fine,no);
			for i=1:n
				x0ResZ(1,(i-1)*ndiscKernel+1:i*ndiscKernel) = misc.translate_to_grid(x0_disc((i-1)*ndiscPlant+1:i*ndiscPlant),zdiscKernel);
			end
			x0_vecObs = ppide.sim2Sys(x0ResZ);
			e0Folded = x0_vecObs - xHat0_vec;
		end
		
		for observerNum = 1:nFold
			a0_bar = a0_barVec{observerNum};
			ppideObs = obsPpide{observerNum};
			
			A0_barInterpolant = numeric.interpolant({linspace(0, 1, size(a0_bar, 1)), 1:n, 1:n, linspace(0,1,size(a0_bar,4))}, a0_bar);
			massMatrix = get_lin_mass_matrix(zdiscTarget);
			A0_barFeedback = zeros(n, n*ndiscTarget, size(a0_bar,4));
			for it = 1:n
				for jt = 1:n
					A0_barFeedback(it, (jt-1)*ndiscTarget + (1:ndiscTarget),:) =  ...
						(squeeze(A0_barInterpolant.evaluate(zdiscTarget, it, jt,linspace(0,1,size(a0_bar,4)))).' * massMatrix).';
				end
			end

			sM = obj.sM;


			% Zielsystem in Objekt zusammenfassen
			% Hartwig edited
			if ndiscPars~= n
				if isa(ppide.ctrl_pars.mu_o,'function_handle') % function
					if nargin(ppide.ctrl_pars.mu_o)==1
						muHelpObs = eval_pointwise(ppide.ctrl_pars.mu_o,zdiscPars);
					else
						muHelpObs = zeros(ndisc,n,n,length(tDisc));
						for z_idx=1:ndiscPars
							muHelpObs(z_idx,:,:,:) = permute((eval_pointwise(@(z)ppide.ctrl_pars.mu_o(zdiscPars(z_idx),z),tDisc)),[2 3 1]);
						end
					end
				elseif size(ppide.ctrl_pars.mu_o,1) > n % is discretized
					if length(size(ppide.ctrl_pars.mu_o))>3
						muInterpolant = interpolant({ppide.ctrl_pars.zdiscCtrl,1:n,1:n,ppide.ctrl_pars.tDiscKernel}, ppide.ctrl_pars.mu_o);
						muHelpObs = muInterpolant.evaluate(zdiscPars,1:n,1:n,tDisc);
					else
						muInterpolant = interpolant({ppide.ctrl_pars.zdiscCtrl,1:n,1:n}, ppide.ctrl_pars.mu_o);
						muHelpObs = muInterpolant.evaluate(zdiscPars,1:n,1:n);
					end
					%muHelp = ppide.ctrl_pars.mu;
				elseif isequal(size(ppide.ctrl_pars.mu_o,1),n) % constant matrix, two dimensions
					muHelpObs(1:ndiscPars,:,:) = repmat(shiftdim(ppide.ctrl_pars.mu_o,-1),ndiscPars,1,1);
				else % discretized matrix
					error('dimensions of mu_o not correct!')
				end
			else
				error('ndisc=n is a very bad choice!')
			end
			mu_o = muHelpObs;
			if observerFolding
				mu_oFolded = misc.fold(mu_o,z_m);
				muOFoldedResh = reshape(mu_oFolded,[],2);
				mu_o = reshape(muOFoldedResh(:,observerNum),size(mu_o));
			end
			target = ppide_sys('n',n,...
							'l',ppideObs.l_disc,...
							'c',ppideObs.c,...
							'b',@(z) 0*z + zeros(n),...
							'a',-mu_o,...
							'b_op1',b_opLeft{observerNum},...
							'b_op2',b_opRight{observerNum},...
							'b_1',zeros(n),...
							'b_2',eye(n),...
							'ndisc',ndiscTarget,...
							'ndiscPars', ndiscPars,...
							'ndiscTime',ppide.ndiscTime,...
							'name','Observer target');

			% 

			% Zielsystem approximieren
			target_appr = ppide_approximation(target);
			A_target = target_appr.A;
			if size(A_target,3)>1
				% get interpolant
				[~, AInterpPerm] = translate_to_grid(permute(A_target,[3 1 2]),linspace(0,1,size(A_target,3)));
			end
			if size(A0_barFeedback,3)>1
				[~, A0bInterpPerm] = translate_to_grid(permute(A0_barFeedback,[3 1 2]),linspace(0,1,size(A0_barFeedback,3)));
			end
			
			B_target = target_appr.Br;

			if size(B_target,3)>1
				[~, BInterpPerm] = translate_to_grid(permute(B_target,[3 1 2]),linspace(0,1,size(B_target,3)));
			end
			if size(A_target,3)>1
				Afun = @(t) reshape(AInterpPerm.evaluate(t,1:n*ndiscTarget,1:n*ndiscTarget),[n*ndiscTarget n*ndiscTarget]);
			else
				Afun = @(t) A_target;
			end
			if size(A0_barFeedback,3)>1
				A0bfun = @(t) reshape(A0bInterpPerm.evaluate(t,1:n,1:n*ndiscTarget),[n n*ndiscTarget]);
			else
				A0bfun = @(t) A0_barFeedback;
			end
			if size(B_target,3)>1
				Bfun = @(t) reshape(BInterpPerm.evaluate(t,1:n*ndiscTarget,1:n),[n*ndiscTarget n]);
			else
				Bfun = @(t) B_target;
			end

			% Zielsystem simulieren (signalmodell mitsimulieren f�r
			% Eingriff)
			uOR = obj.useOutputRegulation;
			Kv = obj.Kv;
			if isempty(Kv)
				Kv = zeros(n,0);
			end

											% Noch pr�fen, was passiert mit Beobachter!?
			f = @(t,x) [Afun(t)-Bfun(t)*A0bfun(t)         -0*Bfun(t)*uOR*Kv;...
						zeros(obj.sM.nv,size(A_target,2)) obj.sM.S]*x;
			
			
			P = -obsKern{observerNum}.KI;
			x0_target_vecRes = Tc(e0Folded(:,:,:,observerNum),P(:,:,:,:,1),zdiscKernel,zdiscKernel);
			x0_target_vec = permute(misc.translate_to_grid(permute(x0_target_vecRes,[2 1 3]),zdiscTarget),[2 1 3]);

			% 2. in andere Darstellung bringen
			x0_target = ppide.sys2Sim(x0_target_vec);
			if isempty(obj.v0)
				v0Temp = zeros(sM.nv,1);
			else
				% Check if v0 has appropriate dimensions:
				if size(obj.v0,2)~= sM.nv
					error(['The initial value of the signal model (v0) must be an'...
					' array containing row vectors at the different timestamps tv'....
					' So size(v0,2) mus equal sM.nv.'])
				end
				v0Temp = obj.v0(1,:);
			end
			x0Tot = [x0_target v0Temp]; %Anfangswert nehmen

			odeopts = odeset('relTol',obj.simPars.frel,'absTol',obj.simPars.fabs,'MaxStep',obj.simPars.tsmax);
			x0Sim = x0Tot;
			t = [];
			y = [];
			for block_idx =1:length(obj.tv)
				t_start = obj.tv(block_idx);
				if block_idx == length(obj.tv)
					tEnd = obj.simPars.tend;
				else
					tEnd = obj.tv(block_idx+1);
				end
				[ti,yi] = ode15s(f,[t_start tEnd],x0Sim,odeopts);
				if any(diff(ti))<= 0
					warning('Solver returned non-increasing ti. Probably there is an error with the times!')
				end
				if block_idx<length(obj.tv)
					x0Sim = yi(end,:).'; % End values are new starting values
					for i = 1:sM.nv
						if ~isnan(obj.v0(block_idx+1,i))
							x0Sim(n*ndiscObs+i) = obj.v0(block_idx+1,i); % Values of the signal model are given at the timestamps tv
						end
					end
				end
				t = [t(1:end-1);ti];
				y = [y(1:end-1,:);yi];
			end
			xObs{observerNum} = y;
			tObs{observerNum} = t;
		end
		if observerFolding
			% reample everyting to t1
			xObs2 = misc.translate_to_grid(xObs{2},tObs{1},'gridOld',tObs{2});
			xObsGes(:,:,1) = xObs{1};
			xObsGes(:,:,2) = xObs2;
			
			xObsVec(:,:,1:n) = obsPpide{1}.sim2Sys(xObsGes(:,:,1));
			xObsVec(:,:,n+1:2*n) = obsPpide{2}.sim2Sys(xObsGes(:,:,2));
			xObsVecForUnfold(:,:,:,1) = obsPpide{1}.sim2Sys(xObsGes(:,:,1));
			xObsVecForUnfold(:,:,:,2) = obsPpide{2}.sim2Sys(xObsGes(:,:,2));
			xObsUnfolded = misc.unfold(xObsVecForUnfold,z_m,2);
			PI1 = obsKern{1}.get_K();
			PI2 = obsKern{2}.get_K();
			warning('todo: Wird auch im zeitunabh. Fall mit zeitabh. transformiert?'); 
			ToInvXObsVec1 = Tc(xObsVec(:,:,1:n),permute(translate_to_grid(permute(PI1,[5 1 2 3 4]),tObs{1}),[2 3 4 5 1]),zdiscTarget,obsKern{observerNum}.zdisc);
			ToInvXObsVec2 = Tc(xObsVec(:,:,n+1:2*n),permute(translate_to_grid(permute(PI2,[5 1 2 3 4]),tObs{1}),[2 3 4 5 1]),zdiscTarget,obsKern{observerNum}.zdisc);
			
			ToInvXObsVec(:,:,:,1) = ToInvXObsVec1;
			ToInvXObsVec(:,:,:,2) = ToInvXObsVec2;
			% unfold:
			ToInvXObsUnfolded = misc.unfold(ToInvXObsVec,z_m,2);
			ToInvXObsSim = obj.ppide.sys2Sim(ToInvXObsUnfolded);
			ToInvXObsInterp = numeric.interpolant({tObs{1},1:n*ndiscTarget},ToInvXObsSim);
			ToInvXObsFun = @(t) ToInvXObsInterp.evaluate(t,1:n*ndiscTarget);
		else
			xObs = y(:,1:n*ndiscTarget);
			xObsVec = ppide.sim2Sys(xObs);
			xObsUnfolded = xObsVec;
			PI = obsKern{1}.get_K();
% 			warning('changed here!')
			% it seems a little bit better to transform onto the kernel resulution and resample
			% afterwards than to upsample and transform to the target system resolution.
% 			ToInvXObs = Tc(xObsVec,permute(translate_to_grid(permute(PI,[5 1 2 3 4]),t),[2 3 4 5 1]),zdiscTarget,zdiscObsKernel);
			ToInvXObs = Tc(permute(translate_to_grid(permute(xObsVec,[2 1 3]),zdiscObsKernel),[2 1 3]),permute(translate_to_grid(permute(PI,[5 1 2 3 4]),t),[2 3 4 5 1]),zdiscObsKernel,zdiscObsKernel);
			ToInvXObsSim = obj.ppide.sys2Sim(ToInvXObs);
% 			ToInvXObsInterp = numeric.interpolant({t,1:n*ndiscTarget},ToInvXObsSim);
			ToInvXObsInterp = numeric.interpolant({t,1:n*length(zdiscObsKernel)},ToInvXObsSim);
% 			ToInvXObsFun = @(t) ToInvXObsInterp.evaluate(t,1:n*ndiscTarget);
			ToInvXObsFun = @(t) ToInvXObsInterp.evaluate(t,1:n*length(zdiscObsKernel));
		end
		obj.tTargetObserver = tObs{1};
		obj.xETargetObserver = xObsUnfolded;
		obj.xETargetObserverQuantity = quantity.Discrete(xObsUnfolded,'gridName',{'t','z'},...
			'grid',{tObs{1},zdiscTarget});
		
		
		time=toc;
		fprintf('Finished sim target observer! (%0.2fs) \n',time);
	end % observer
	tic;
	fprintf('Starting Simulation of target system...\n');
	zdiscKernel = obj.controllerKernel.zdisc;
	ndiscKernel = length(zdiscKernel);
	zdiscTarget = ppide.ctrl_pars.zdiscTarget;
	ndiscTarget = length(zdiscTarget);
	ndiscPars = ppide.ndiscPars;
	zdiscPars = linspace(0,1,ndiscPars);

	% Anfangswert in Zielkoordinaten taransformieren
	for i=1:n
		x0_fine((i-1)*ndiscTarget+1:i*ndiscTarget) = interp1(zdiscPlant,x0_disc((i-1)*ndiscPlant+1:i*ndiscPlant),zdiscTarget);
	end
	
	x0_vec=zeros(1,ndiscTarget,n);
	for i=1:n %t,z,n
		x0_vec(1,1:ndiscTarget,i) = x0_fine((i-1)*ndiscTarget+1:i*ndiscTarget);
	end
	K = obj.controllerKernel.get_K;
	if folding
		z0 = ppide.ctrl_pars.foldingPoint;
		if isscalar(z0) % scalar
			z0 = ones(1,n)*z0;
		end
		zLeft = zeros(n,ndiscTarget);
		zRight = zeros(n,ndiscTarget);
		zRightPars = zeros(n,ndiscPars);
		for i=1:n
			zRight(i,:) = linspace(z0(i),1,ndiscTarget);
			zRightPars(i,:) = linspace(z0(i),1,ndiscPars);
			dZ = diff(zRight(i,:));
% 			dZ = diff(zRightPars(i,:));
			zLeft(i,:) = linspace(0,z0(i)-dZ(1),ndiscTarget);
		end
		% transorm initial condition
		x0Interp = numeric.interpolant({1,zdiscTarget,1:n},x0_vec);
		x0New = zeros(1,ndiscTarget,2*n);
		for i=1:n
			x0New(1,:,i) = x0Interp.evaluate(1,fliplr(zLeft(i,:)),i);
			x0New(1,:,i+n) = x0Interp.evaluate(1,zRight(i,:),i);
		end
		x0VecTrans = x0New;
	else
		x0VecTrans = x0_vec;
	end
	x0TargetVecFolded = Tc(x0VecTrans,K(:,:,:,:,1),zdiscTarget,zdiscKernel);
	x0TargetVecUnfolded = zeros(1,ndiscTarget,n);
	if folding
	% unfold
		for i=1:n
			z0iIdx = find(zdiscTarget<z0(i),1,'last');
			x0TargetVecUnfolded(:,1:z0iIdx,i) = permute(...
				interp1(...
					linspace(zdiscPlant(z0iIdx),0,ndiscPlant),permute(x0TargetVecFolded(:,:,i),[2 1 3]),...
				zdiscPlant(1:z0iIdx))...
				,[2 1]);
			x0TargetVecUnfolded(:,z0iIdx+1:end,i) = permute(...
				interp1(...
					linspace(zdiscPlant(z0iIdx+1),1,ndiscPlant),permute(x0TargetVecFolded(:,:,i+n),[2 1 3]),...
				zdiscPlant(z0iIdx+1:end))...
				,[2 1]);
		end
	else
		x0TargetVecUnfolded = x0TargetVecFolded;
	end
	
	
	
	r = obj.controller.r;
	
	
	p = size(ppide.b_2,2); 
	numtKernel = size(obj.controllerKernel.value{1,1}.K,3);
	
	a0_til = obj.controllerKernel.a0_til;
	a0TilInterp  = numeric.interpolant({zdiscKernel,1:size(a0_til,2),1:size(a0_til,3),1:size(a0_til,4)},a0_til);
	A0_til_fine = translate_to_grid(a0_til,zdiscPars);

	if folding
		a0_neu = obj.controllerKernel.a0_neu;
		a0NeuInterp = numeric.interpolant({zdiscKernel,1:size(a0_neu,2),1:size(a0_neu,3),1:size(a0_neu,4)},a0_neu);
		A0_neu_fine = translate_to_grid(a0_neu,zdiscPars);
	end
	E1 = ppide.b_op1.Sd;
	E2 = ppide.b_op1.Sr;
		
	A0Dir = zeros(ndiscTarget,n,n,numtKernel);
	A0DirSparse = zeros(ndiscKernel,n,n,numtKernel);
	A0Neu = zeros(ndiscTarget,n,n,numtKernel);
	A0DirComp = zeros(ndiscTarget,n,n,numtKernel);
	A0NeuComp = zeros(ndiscTarget,n,n,numtKernel);
	if folding
% 		a(zIdx,i,j,1:size(ppide.a,4)) = aInterp.evaluate(zLeft(i,end-zIdx+1),i,j,1:size(ppide.a,4));
		for i=1:n
			z0iIdx = find(zdiscTarget<z0(i),1,'last');
			z0iIdxKernel = find(zdiscKernel<z0(i),1,'last');
			A0Dir(1:z0iIdx,i,:,:) = a0TilInterp.eval({linspace(1,0,z0iIdx),i,1:n,1:size(a0_til,4)})....
								   + a0TilInterp.eval({linspace(1,0,z0iIdx),i,n+1:2*n,1:size(a0_til,4)});
			A0DirSparse(1:z0iIdxKernel,i,:,:) = interp1(linspace(z0(i),0,ndiscKernel),a0_til(:,i,1:n,:),linspace(0,z0(i),z0iIdxKernel))....
									+ interp1(linspace(z0(i),0,ndiscKernel),a0_til(:,i,n+1:end,:),linspace(0,z0(i),z0iIdxKernel));
			A0DirComp(1:z0iIdx,i,:,:) = interp1(linspace(z0(i),0,ndiscPars),A0_til_fine(:,i,1:n,:),linspace(0,z0(i),z0iIdx))....
									+ interp1(linspace(z0(i),0,ndiscPars),A0_til_fine(:,i,n+1:end,:),linspace(0,z0(i),z0iIdx));
			A0Dir(z0iIdx+1:end,i,:,:) = a0TilInterp.eval({linspace(0,1,ndiscTarget - z0iIdx),i+n,1:n,1:size(a0_til,4)})....
								    + a0TilInterp.eval({linspace(0,1,ndiscTarget - z0iIdx),i+n,n+1:2*n,1:size(a0_til,4)});
			A0DirComp(z0iIdx+1:end,i,:,:) = interp1(linspace(z0(i),1,ndiscPars),A0_til_fine(:,i+n,1:n,:),linspace(z0(i),1,ndiscTarget - z0iIdx))....
									+ interp1(linspace(z0(i),1,ndiscPars),A0_til_fine(:,i+n,n+1:end,:),linspace(z0(i),1,ndiscTarget  - z0iIdx));
			A0DirSparse(z0iIdxKernel+1:end,i,:,:) = interp1(linspace(zdiscKernel(z0iIdxKernel+1),1,ndiscKernel),a0_til(:,i+n,1:n,:),linspace(zdiscKernel(z0iIdxKernel+1),1,ndiscKernel - z0iIdxKernel))....
									+ interp1(linspace(zdiscKernel(z0iIdxKernel+1),1,ndiscKernel),a0_til(:,i+n,n+1:end,:),linspace(zdiscKernel(z0iIdxKernel+1),1,ndiscKernel  - z0iIdxKernel));
			A0Neu(1:z0iIdx,i,:,:) = -a0NeuInterp.eval({linspace(1,0,z0iIdx),i,1:n,1:size(a0_til,4)}).*reshape(z0,1,1,[])...
								   + a0NeuInterp.eval({linspace(1,0,z0iIdx),i,n+1:2*n,1:size(a0_til,4)}).*reshape(1-z0,1,1,[]);
			A0NeuComp(1:z0iIdx,i,:,:) = - interp1(linspace(z0(i),0,ndiscPars),A0_neu_fine(:,i,1:n,:),linspace(0,z0(i),z0iIdx)).*reshape(z0,1,1,[])....
									+ interp1(linspace(z0(i),0,ndiscPars),A0_neu_fine(:,i,n+1:end,:),linspace(0,z0(i),z0iIdx)).*reshape(1-z0,1,1,[]);
			A0Neu(z0iIdx+1:end,i,:,:) = -a0NeuInterp.eval({linspace(0,1,ndiscTarget - z0iIdx),i+n,1:n,1:size(a0_til,4)}).*reshape(z0,1,1,[])....
								   + a0NeuInterp.eval({linspace(0,1,ndiscTarget - z0iIdx),i+n,n+1:2*n,1:size(a0_til,4)}).*reshape(1-z0,1,1,[]);
			A0NeuComp(z0iIdx+1:end,i,:,:) = -interp1(linspace(z0(i),1,ndiscPars),A0_neu_fine(:,i+n,1:n,:),linspace(z0(i),1,ndiscTarget - z0iIdx)).*reshape(z0,1,1,[])....
									+ interp1(linspace(z0(i),1,ndiscPars),A0_neu_fine(:,i+n,n+1:end,:),linspace(z0(i),1,ndiscTarget  - z0iIdx)).*reshape(1-z0,1,1,[]);
		end
	else
		if ndims(A0_til_fine)>3 % time dependent
			A0Dir = permute(misc.multArray(A0_til_fine,(E2*E2.'),3,1),[1 2 4 3]);
			A0Neu = permute(misc.multArray(A0_til_fine,(E1*E1.'),3,1),[1 2 4 3]);
		else
			A0Dir = misc.multArray(A0_til_fine,(E2*E2.'),3,1);
			A0Neu = misc.multArray(A0_til_fine,(E1*E1.'),3,1);
		end
	end
% 	A0Dir = misc.translate_to_grid(A0DirSparse,zdiscPars);
	sM = obj.sM;
	
	% Zielsystem in Objekt zusammenfassen
	% Hartwig edited
	if ndiscPlant~= n
		if isa(ppide.ctrl_pars.mu,'function_handle') % function
			if nargin(ppide.ctrl_pars.mu)==1
				muHelp = eval_pointwise(ppide.ctrl_pars.mu,zdiscPlant);
			else
				muHelp = zeros(ndiscPlant,n,n,length(tDisc));
				for z_idx=1:ndiscPlant
					muHelp(z_idx,:,:,:) = permute((eval_pointwise(@(t)ppide.ctrl_pars.mu(zdiscPlant(z_idx),t),tDisc)),[2 3 1]);
				end
			end
		elseif size(ppide.ctrl_pars.mu,1) > n % is discretized
			if length(size(ppide.ctrl_pars.mu))>3
				muInterpolant = interpolant({ppide.ctrl_pars.zdiscCtrl,1:n,1:n,linspace(0,1,size(ppide.ctrl_pars.mu,4))}, ppide.ctrl_pars.mu);
				muHelp = muInterpolant.evaluate(zdiscPlant,1:n,1:n,tDisc);
			else
				muInterpolant = interpolant({ppide.ctrl_pars.zdiscCtrl,1:n,1:n}, ppide.ctrl_pars.mu);
				muHelp = muInterpolant.evaluate(zdiscPlant,1:n,1:n);
			end
			%muHelp = ppide.ctrl_pars.mu;
		elseif isequal(size(ppide.ctrl_pars.mu,1),ndiscPars) % is discretized
			if length(size(ppide.ctrl_pars.mu))>3
				muInterpolant = interpolant({zdiscPars,1:n,1:n,linspace(0,1,size(ppide.ctrl_pars.mu,4))}, ppide.ctrl_pars.mu);
				muHelp = muInterpolant.evaluate(zdiscPlant,1:n,1:n,tDisc);
			else
				muInterpolant = interpolant({zdiscPars,1:n,1:n}, ppide.ctrl_pars.mu);
				muHelp = muInterpolant.evaluate(zdiscPlant,1:n,1:n);
			end
			%muHelp = ppide.ctrl_pars.mu;
		elseif isequal(size(ppide.ctrl_pars.mu,1),n) % constant matrix, two dimensions
			muHelp(1:ndiscPlant,:,:) = repmat(shiftdim(ppide.ctrl_pars.mu,-1),ndiscPlant,1,1);
		else % discretized matrix
			error('dimensions of mu not correct!')
		end
	else
		error('ndiscPlant=n is a very bad choice!')
	end
	

	mu = muHelp;
	if folding
		target = ppide_sys('n',n,...
						'l',ppide.l,...
						'l_diff',ppide.l_diff,...
						'c',ppide.c,...
						'a_z0',-A0Dir,...
						'c_z0',-A0Neu,...
						'z0',z0,...
						'b',@(z) 0*z + zeros(n,2*n),...
						'a',-mu,...
						'b_op1',b_op1T,...
						'b_op2',b_op2T,...
						'b_1',[eye(n) zeros(n,n)],...
						'b_2',[zeros(n,n) eye(n)],...
						'ndisc',ndiscTarget,...
						'ndiscPars', ndiscPars,...
						'ndiscTime',obj.ppide.ndiscTime,...
						'name','target sys folding');
	else
		target = ppide_sys('n',n,...
						'l',ppide.l,...
						'l_diff',ppide.l_diff,...
						'c',ppide.c,...
						'a_0',-A0Dir,...
						'c_0',-A0Neu,...
						'b',@(z) 0*z + zeros(n),...
						'a',-mu,...
						'b_op1',b_op1T,...
						'b_op2',b_op2T,...
						'b_1',zeros(n),...
						'b_2',eye(n),...
						'ndisc',ndiscTarget,...
						'ndiscPars', ndiscPars,...
						'ndiscTime',obj.ppide.ndiscTime,...
						'name','target sys');
	end

	% 

	% Zielsystem approximieren
	target_appr = ppide_approximation(target);
	A_target = target_appr.A;
	numtPlant = size(A_target,3);
	if numtPlant>1
		% get interpolant
		[~, AInterpPerm] = translate_to_grid(permute(A_target,[3 1 2]),linspace(0,1,numtPlant));
	end
	if folding
		B_target = target_appr.Br + target_appr.Bl;
	else
		B_target = target_appr.Br;
	end
	if size(B_target,3)>1
		[~, BInterpPerm] = translate_to_grid(permute(B_target,[3 1 2]),linspace(0,1,numtPlant));
	end
	if size(A_target,3)>1
		Afun = @(t) reshape(AInterpPerm.evaluate(t,1:n*ndiscTarget,1:n*ndiscTarget),[n*ndiscTarget n*ndiscTarget]);
	else
		Afun = @(t) A_target;
	end
	if size(B_target,3)>1
		Bfun = @(t) reshape(BInterpPerm.evaluate(t,1:n*ndiscTarget,1:n),[n*ndiscTarget n]);
	else
		Bfun = @(t) B_target;
	end

	% Zielsystem simulieren (signalmodell mitsimulieren f�r
	% Eingriff)
	uOR = obj.useOutputRegulation;
	Kv = obj.Kv;
	if isempty(Kv)
		if folding
			Kv = zeros(2*n,0);
		else
			Kv = zeros(n,0);
		end
	end
	% Simulate input of observer error dynamics in target system
	uSFB = obj.useStateFeedback;
	
	if numtKernel>1
		% get the interpolant via translate_to_grid
		rInterpPerm = numeric.interpolant({linspace(0,1,size(r,3)),1:p,1:size(r,2)},permute(r,[3 1 2]));
	end
	uSO = obj.useStateObserver;
	aSO = obj.useObserverInFeedback;
	if uSO && aSO
% 		warning('changed here')
		if observerFolding
			IOP = interp_mat(zdiscTarget, zdiscPlant); % from observer to plant
		else
			IOP = interp_mat(zdiscObsKernel, zdiscPlant); % from observer to plant
		end
		% add to diagonal matrix to interpolate each state. Using kron is
		% faster than doing it manually for large n.
		IOPTot = kron(eye(n), IOP);
	else % no state observer
		IOPTot = eye(n*ndiscPlant);
	end % if
	if uSO && aSO
		% coupling of observer error into traget system
		if numtKernel>1
			kT = @(t) uSFB*(reshape(rInterpPerm.evaluate(t,1:p,1:n*ndiscPlant),[p n*ndiscPlant])*IOPTot*ToInvXObsFun(t).');
		else
			kT = @(t) uSFB*r*IOPTot*ToInvXObsFun(t).';
		end
		f = @(t,x) [Afun(t) -Bfun(t)*uOR*Kv;...
					zeros(obj.sM.nv,size(A_target,2)) obj.sM.S]*x...
					- [Bfun(t);zeros(obj.sM.nv,p)]*kT(t);
	else
		f = @(t,x) [Afun(t) -Bfun(t)*uOR*Kv;...
				zeros(obj.sM.nv,size(A_target,2)) obj.sM.S]*x;
	end
		
	% 2. in Simulations-Darstellung bringen
	x0_target = obj.ppide.sys2Sim(x0TargetVecUnfolded);
	
	if isempty(obj.v0)
		v0Temp = zeros(sM.nv,1);
	else
		% Check if v0 has appropriate dimensions:
		if size(obj.v0,2)~= sM.nv
			error(['The initial value of the signal model (v0) must be an'...
			' array containing row vectors at the different timestamps tv'....
			' So size(v0,2) mus equal sM.nv.'])
		end
		v0Temp = obj.v0(1,:);
	end
	x0Tot = [x0_target v0Temp]; %Anfangswert nehmen

	odeopts = odeset('relTol',obj.simPars.frel,'absTol',obj.simPars.fabs,'MaxStep',obj.simPars.tsmax);
	x0Sim = x0Tot;
	t = [];
	y = [];
	for block_idx =1:length(obj.tv)
		t_start = obj.tv(block_idx);
		if block_idx == length(obj.tv)
			tEnd = obj.simPars.tend;
		else
			tEnd = obj.tv(block_idx+1);
		end
		[ti,yi] = ode15s(f,[t_start tEnd],x0Sim,odeopts);
		if any(diff(ti))<= 0
			warning('Solver returned non-increasing ti. Probably there is an error with the times!')
		end
		if block_idx<length(obj.tv)
			x0Sim = yi(end,:).'; % End values are new starting values
			for i = 1:sM.nv
				if ~isnan(obj.v0(block_idx+1,i))
					x0Sim(n*ndiscTarget+i) = obj.v0(block_idx+1,i); % Values of the signal model are given at the timestamps tv
				end
			end
		end
		t = [t(1:end-1);ti];
		y = [y(1:end-1,:);yi];
	end
	obj.tTarget = t;
% 	if folded % unfold result
% 		xETargetVec = zeros(1,ndiscTarget,n);
% 		yVec = ppide.sim2sys(y);
% 		for i=1:n
% 			z0iIdx = find(zdiscTarget<z0(i),1,'last');
% 			xETargetVec(:,1:z0iIdx,i) = permute(...
% 				interp1(...
% 					linspace(zdiscTarget(z0iIdx),0,ndiscTarget),permute(yVec(:,:,i),[2 1 3]),...
% 				zdiscTarget(1:z0iIdx))...
% 				,[2 1 3]);
% 			xETargetVec(:,z0iIdx+1:end,i) = permute(...
% 				interp1(...
% 					linspace(zdiscTarget(z0iIdx),1,ndiscTarget),permute(yVec(:,:,i+n),[2 1 3]),...
% 				zdiscTarget(z0iIdx+1:end))...
% 				,[2 1 3]);
% 		end
% 		obj.xETarget = sys2sim(xETargetVec);
% 	else % not folded
	obj.xETarget = y;
	obj.xETargetQuantity = quantity.Discrete(obj.ppide.sim2Sys(y),'gridName',{'t','z'},...
		'grid',{t,zdiscTarget});
% 	end
	
	%% calculate norm
	y_target_quad_sum = 0;
	lTarget = misc.translate_to_grid(obj.ppide.l_disc,{zdiscTarget});
	y_target = obj.xETarget(:,1:n*ndiscTarget);
	for i=1:obj.ppide.n
		y_target_quad_sum = y_target_quad_sum + (permute(lTarget(:,i,i,:),[4,1,2,3]).^(-1/2).*y_target(:,(i-1)*ndiscTarget+1:i*ndiscTarget)).^2;
	end
	y_target_L2_norm = sqrt(numeric.trapz_fast(zdiscTarget,y_target_quad_sum));
	if y_target_L2_norm(1) ~= 0
		obj.xTargetNorm = y_target_L2_norm;%/y_target_L2_norm(1);
	else
		obj.xTargetNorm = y_target_L2_norm;
	end
	obj.simulatedTarget =1;

	
	time=toc;
	fprintf('Finished simulation of target system! (%0.2fs) \n',time);
		
	
end