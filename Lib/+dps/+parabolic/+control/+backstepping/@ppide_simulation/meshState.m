function varargout = meshState(obj,varargin)
	import misc.*
	if ~obj.simulated
		obj = obj.simulate();
	end
	found=0;
	passed = [];
	arglist = {'numpointsT','numpointsZ','zLim','tLim','plotPars','extraZ','extraT','maxDiffT'};
	if ~isempty(varargin)
		if mod(length(varargin),2) % uneven number
			error('When passing additional arguments, you must pass them pairwise!')
		end
		for index = 1:2:length(varargin) % loop through all passed arguments:
			for arg = 1:length(arglist)
				if strcmp(varargin{index},arglist{arg})
					passed.(arglist{arg}) = varargin{index+1};
					found=1;
					break
				end 
			end % for arg
			% argument wasnt found in for-loop
			if ~found
				error([varargin{index} ' is not a valid property to pass to meshObserverError!']);
			end
			found=0; % reset found
		end % for index
	end 
	if ~isfield(passed,'numpointsT')
		if isfield(obj.plotPars,'plot_res_2D')
			passed.numpointsT = obj.plotPars.plot_res_2D;
		else
			passed.numpointsT = 21;
		end
	end
	if ~isfield(passed,'numpointsZ')
		if isfield(obj.plotPars,'plot_res_2D')
			passed.numpointsZ = obj.plotPars.plot_res_2D;
		else
			passed.numpointsZ = 21;
		end
	end
	if ~isfield(passed,'extraZ')
		passed.extraZ = [];
	end
	if ~isfield(passed,'extraT')
		passed.extraT = [];
	end
	if ~isfield(passed,'maxDiffT')
		passed.maxDiffT = 0;
	end
	if ~isfield(passed,'zLim')
		zPlot = linspace(0,1,passed.numpointsZ);
	else
		zPlot = linspace(passed.zLim(1),pssed.zLim(2),passed.numpointsZ);
	end
	if ~isfield(passed,'tLim')
		tPlot = linspace(0,1,passed.numpointsT);
	else
		tPlot = linspace(passed.tLim(1),passed.tLim(2),passed.numpointsT);
	end
	if ~isfield(passed,'plotPars')
		passed.plotPars = {};
	else
		if ~iscell(passed.plotPars) 
			error('plotPars must be passed as cell array of strings representing name-value-pairs!')
		end
	end
	zPlot = sort([zPlot passed.extraZ]);
	tPlot = sort([tPlot passed.extraT]);

	ndisc_plant = obj.ppide.ndisc;
	zdisc_plant = linspace(0,1,ndisc_plant);
	n=obj.ppide.n;
	c_op = obj.ppide.c_op;
	
	for i=1:n
		figure('name',['x' num2str(i)])
		if passed.maxDiffT ~= 0
			needsRefinement =1;
			while needsRefinement
				xTemp = obj.xQuantity(i).on({tPlot,zPlot},{'t','z'});
				xDiffT = abs(diff(xTemp,1,1));
				idxVec = zeros(size(xTemp,1),1);
				idxVec(max(xDiffT,[],2)>passed.maxDiffT) = 1;
				curIdx = 1;
				tAdd = [];
				needsRefinement = 0;
				for idx = 1:length(idxVec)
					if idxVec(idx) 
						tAdd(curIdx) = tPlot(idx)+(tPlot(idx+1)-tPlot(idx))/2; % insert point in the middle
						curIdx = curIdx+1;
						needsRefinement =1;
					end
				end
				tPlot = sort([tPlot tAdd]);
			end
			xPlot = obj.xQuantity(i).on({tPlot,zPlot},{'t','z'});
		else
			xPlot = obj.xQuantity(i).on({tPlot,zPlot},{'t','z'});
		end
		mesh(tPlot,zPlot,xPlot.',passed.plotPars{:})
		hold on
		box on
		objT = obj.t(:); % column vector!
		shortT = objT(objT>=tPlot(1)&objT<=tPlot(end));
		if size(obj.ppide.b_1,2)>n % folding
			xFoldingPoint = obj.xQuantity(i).on({shortT,obj.ppide.ctrl_pars.foldingPoint},{'t','z'}).';
			plot3(shortT,repmat(obj.ppide.ctrl_pars.foldingPoint,length(shortT),1),xFoldingPoint,'r','linewidth',2);
		end
		% ausgaenge
		if any(obj.ppide.c_op.c_m(:,i)) % Messung Punkt
			plot3(shortT,repmat(obj.ppide.c_op.z_m,length(shortT),1),obj.x((objT>=tPlot(1)&objT<=tPlot(end)),(i-1)*ndisc_plant+get_idx(c_op.z_m,zdisc_plant)),'r','linewidth',2);
		end
		if any(c_op.c_0(:,i)) % Messung links
			plot3(shortT,zeros(length(shortT),1),obj.x((objT>=tPlot(1)&objT<=tPlot(end)),(i-1)*ndisc_plant+1),'r','linewidth',2);
		end
		if any(c_op.c_1(:,i)) % Messung rechts
			plot3(shortT,zeros(length(shortT),1),obj.x((objT>=tPlot(1)&objT<=tPlot(end)),(i)*ndisc_plant),'r','linewidth',2);
		end
		if obj.useStateObserver
			cm_op = obj.ppide.cm_op;
			% messungen f Beobachter
			if any(obj.ppide.cm_op.c_m(:,i)) % Messung Punkt
				plot3(shortT,repmat(obj.ppide.cm_op.z_m,length(shortT),1),obj.x((objT>=tPlot(1)&objT<=tPlot(end)),(i-1)*ndisc_plant+get_idx(cm_op.z_m,zdisc_plant)),'r','linewidth',2);
			end
			if any(cm_op.c_0(:,i)) % Messung links
				plot3(shortT,zeros(length(shortT),1),obj.x((objT>=tPlot(1)&objT<=tPlot(end)),(i-1)*ndisc_plant+1),'r','linewidth',2);
			end
			if any(cm_op.c_1(:,i)) % Messung rechts
				plot3(shortT,zeros(length(shortT),1),obj.x((objT>=tPlot(1)&objT<=tPlot(end)),(i)*ndisc_plant),'r','linewidth',2);
			end
		end
		title(['$x_' num2str(i) '(z,t)$'])
		set(gca,'Ydir','reverse')
		xlabel('$t$')
		ylabel('$z$')
	end
	if nargout>0
		varargout{1} = obj;
	end
end