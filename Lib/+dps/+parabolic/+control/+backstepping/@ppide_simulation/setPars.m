function obj = setPars(obj, ppide, varargin)
% setPars set parameters to a ppide_simulation
%   obj = setPars(obj,ppide,varargin)  changes the parameters of a
%     ppide_simulation object. It can be used like the constructor, as it
%     gets the same parameters.

%created on 04/2018 by Simon Kerschbaum


	obj.ppide = ppide;
	if size(ppide.b_1,2)== 2*ppide.n % bilateral system
		obj.designPpide = obj.foldPpide();
	end
	if isa(varargin,'cell') % when varargin was passed as a variable
		if isa (varargin{1},'cell')
			varargin = varargin{1};
		end
	end
	found = 0;
	arglist = properties(obj);
	if ~isempty(varargin)
		if mod(length(varargin),2) % uneven number
			error('When passing additional arguments, you must pass them pairwise!')
		end
		for index = 1:2:length(varargin) % loop through all passed arguments:
			for arg = 1:length(arglist)
				if strcmp(varargin{index},arglist{arg})
					obj.(arglist{arg}) = varargin{index+1};
					found=1;
					break
				end 
			end % for arg
			% argument wasnt found in for-loop
			if ~found
				error([varargin{index} ' is not a valid property of class ppide_simulation!']);
			end
			found=0; % reset found
		end % for index
	end % if nargin > numfix
end