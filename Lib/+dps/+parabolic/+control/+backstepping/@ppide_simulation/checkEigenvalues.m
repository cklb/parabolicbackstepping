function res = checkEigenvalues(obj)
% check if plant is stable or not and if mu and mu_o are set properly.

	if ~isempty(obj.ppideApproximation)
		tic
		fprintf('  Checking eigenvalues...\n')
		ppide_appr = obj.ppideApproximation;
		A = ppide_appr.A;
		eigVals=[];
		for tIdx = 1:size(A,3)
			eigVals = [eigVals;eig(A(:,:,tIdx))];
		end

		% only real and imaginary parts
		[meigreal, ind] = max(real(eigVals));
		[meigimag, indImag] = max(imag(eigVals));
		% corresponding eigenvalues
		meig = eigVals(ind);
		meigImag = eigVals(indImag);
		% noch verbessern:
		if meig>0
			fprintf(['    Plant is unstable: ' num2str(round(meig,2)) '\n'])
		else
			fprintf(['    Plant is stable: ' num2str(round(meig,2)) '\n'])
		end
		ppide=obj.ppide;
		if obj.useStateFeedback
			ew_target = ppide.eigTarget(ppide.ctrl_pars.mu);
			if max(real(ew_target)) > 0
				error(['    Largest eigenvalue of the target system: ' num2str(round(max(real(ew_target)),2)) '!'])
			end
		end
		if obj.useStateObserver
			ew_obs = ppide.eigTarget(ppide.ctrl_pars.mu_o);
			if max(real(ew_obs)) > 0
				error(['Largest eigenvalue of the observer target system: ' num2str(round(max(real(ew_obs)),2)) '!'])
			end
		end
% 		fprintf(['Positivster Eigenwert des Zielsystems (ohne mu) = ' num2str(max(ppide.eigTarget(zeros(n,n)))) '\n']);
		fprintf(['  Finished eigenvalue check. (' num2str(round(toc,2)) ')\n'])
	else
		fprintf('Cannot check eigenvalues, ppideApproximation is empty!')
	end
	
end % function