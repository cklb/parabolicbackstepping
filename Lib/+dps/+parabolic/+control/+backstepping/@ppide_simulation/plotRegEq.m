function varargout = plotRegEq(obj)
	import numeric.*
	import misc.*
	obj = obj.computeControllerKernel;
	if ~obj.KvFlag
		[obj.Pi, obj.Kv, obj.Piz, obj.Pizz] = regulator_equations(obj.ppide,obj.sM,obj.controller.kernel);
		obj.KvFlag=1;
	end
	zdiscRegEq = obj.ppide.ctrl_pars.zdiscRegEq;
	ndiscRegEq = length(zdiscRegEq);
	Pi = obj.Pi;
	Pi_z = obj.Piz;
	Pi_zz = obj.Pizz;

	n = obj.ppide.n;
	nv = obj.sM.nv;
	m = obj.ppide.c_op.m;

	pi_z_comp(1:ndiscRegEq,1:n,1:nv)=0; %preallocate
	pi_zz_comp(1:ndiscRegEq,1:n,1:nv)=0; %preallocate
	for i=1:n
		for j=1:nv	
			pi_z_comp(:,i,j) = diff_num(zdiscRegEq,Pi(:,i,j));
			pi_zz_comp(:,i,j) = diff_num(zdiscRegEq,Pi_z(:,i,j));
		end
	end
	% check regulator equations for validity:
	figure('Name','Abweichung numerische analytische Ableitung: pi_zz')
	hold on
	for i=1:n
		for j=1:nv
			firstline = plot(zdiscRegEq,Pi_zz(:,i,j),'DisplayName',['an ' num2str(i) num2str(j)]);
			curCol = get(firstline, 'Color');
			plot(zdiscRegEq,pi_zz_comp(:,i,j),'--','color',curCol,'DisplayName',['num ' num2str(i) num2str(j)]);
		end
	end
	legend('-DynamicLegend')

	figure('Name','Abweichung numerische analytische Ableitung: pi_z')
	hold on
	for i=1:n
		for j=1:nv
			firstline = plot(zdiscRegEq,Pi_z(:,i,j),'DisplayName',['an ' num2str(i) num2str(j)]);
			curCol = get(firstline, 'Color');
			plot(zdiscRegEq,pi_z_comp(:,i,j),'--','color',curCol,'DisplayName',['num ' num2str(i) num2str(j)]);
		end
	end
	legend('-DynamicLegend')
	kernel = obj.controllerKernel;
	zdiscKernel = kernel.zdisc;
% 	ndiscKernel = length(zdiscKernel);
	zdiscPlant = linspace(0,1,obj.ppide.ndiscPars); % Aufloesung der Parameter
	ndiscPlant = obj.ppide.ndiscPars;
	IntPR = interp_mat(zdiscPlant,zdiscRegEq);
	IntKR = interp_mat(zdiscKernel,zdiscRegEq);
	if ndiscRegEq ~= n
		if isa(obj.ppide.ctrl_pars.mu,'function_handle') % function
			muSys = eval_pointwise(obj.ppide.ctrl_pars.mu,zdiscRegEq);
		elseif isequal(size(obj.ppide.ctrl_pars.mu,1),ndiscPlant) % is discretized
			muSys = zeros(ndiscRegEq,n,n);
			for i=1:n
				for j=1:n
					muSys(:,i,j) = IntPR*obj.ppide.ctrl_pars.mu(:,i,j);
				end
			end
		elseif isequal(size(obj.ppide.ctrl_pars.mu,1),n) % constant matrix, two dimensions
			muSys(1:ndiscRegEq,:,:) = repmat(shiftdim(obj.ppide.ctrl_pars.mu,-1),ndiscRegEq,1,1);
		else % discretized matrix
			error('dimensions of mu not correct!')
		end
	else
		error('ndisc=n is a very bad choice!')
	end
	if n>1
		lInterp = griddedInterpolant({zdiscPlant,1:n,1:n},obj.ppide.l_disc);
		l_diffInterp = griddedInterpolant({zdiscPlant,1:n,1:n},obj.ppide.l_diff_disc);
		a0Interp = griddedInterpolant({zdiscPlant,1:n,1:n},obj.ppide.a_0_disc);
		l_disc = lInterp({zdiscRegEq,1:n,1:n});
		l_diff_disc = l_diffInterp({zdiscRegEq,1:n,1:n});
		a0_disc = a0Interp({zdiscRegEq,1:n,1:n});
	else
		l_disc = interp1(zdiscPlant,obj.ppide.l_disc,zdiscRegEq(:));
		l_diff_disc = interp1(zdiscPlant,obj.ppide.l_diff_disc,zdiscRegEq(:));
		a0_disc = interp1(zdiscPlant,obj.ppide.a_0_disc,zdiscRegEq(:));
	end
	G1_disc = misc.translate_to_grid(obj.ppide.g_1,zdiscRegEq); % dim: ndisc x n x nd
	G2 = obj.ppide.g_2; % dim: n x nd % left disturbance input
	nd = size(G2,2); % length of disturbance vector
	G4 = obj.ppide.g_4; % dim: m x nd % output disturbance input
	sigMod = obj.sM;
	H2 = G2*sigMod.pd;
	H4 = G4*sigMod.pd;


	KKern = kernel.get_K();
	if n>1
		KInterp = griddedInterpolant({zdiscKernel,zdiscKernel,1:n,1:n},KKern);
	else
		KInterp = griddedInterpolant({zdiscKernel,zdiscKernel},KKern);
	end
	if n>1
% 		K_comp = permute(interp1(zdiscKernel,permute(interp1(zdiscKernel,KKern,zdiscRegEq),[2 1 3 4]),zdiscRegEq),[2 1 3 4]);
		K = KInterp({zdiscRegEq,zdiscRegEq,1:n,1:n});
	else
		K = KInterp({zdiscRegEq,zdiscRegEq});
	end

	dzeta_K_z_0 = zeros(ndiscRegEq,n,n);
	a0_til = zeros(ndiscRegEq,n,n);
	for i=1:n
		for j=1:n
			dzeta_K_z_0(:,i,j) = IntKR*kernel.value{i,j}.dzeta_K_z_0;
			a0_til(:,i,j) =IntKR*kernel.a0_til(:,i,j);
		end
	end
	dzeta_K_z_0_Lambda(1:ndiscRegEq,n,n)=0;
	K_z_0_lambda_diff(1:ndiscRegEq,n,n)=0;
	K_z_0_lambda(1:ndiscRegEq,n,n)=0;
	for z_idx = 1:ndiscRegEq
		dzeta_K_z_0_Lambda(z_idx,:,:) =  squeeze(dzeta_K_z_0(z_idx,:,:))*squeeze(l_disc(1,:,:));
		K_z_0_lambda_diff(z_idx,:,:) = squeeze(K(z_idx,1,:,:))*squeeze(l_diff_disc(1,:,:));
		K_z_0_lambda(z_idx,:,:) = squeeze(K(z_idx,1,:,:))*squeeze(l_disc(1,:,:));
	end

	% Backstepping Trafo of matrix possible with a permutation of the
	% dimensions.
	A0_BS = permute(dps.parabolic.control.backstepping.Tc(permute(a0_disc,[3 1 2]),KKern,zdiscRegEq,zdiscKernel),[2 3 1]);

	% FEHLT NOCH: AUCH BS TRAFO ABER ABHAENGIG VON nd!
	G1_BS = permute(dps.parabolic.control.backstepping.Tc(permute(G1_disc,[3 1 2]),KKern,zdiscRegEq,zdiscKernel),[2 3 1]);
	M1_disc = A0_BS - dzeta_K_z_0_Lambda - K_z_0_lambda_diff;
	M2_disc = K_z_0_lambda;
	for i=1:n
		for j=1:n
			if obj.ppide.b_op1.b_n(j,j) ~= 0 % Robin/Neumann left
				M1_disc(:,i,j) = M1_disc(:,i,j) + a0_til(:,i,j);
			else % dirichlet left
				M2_disc(:,i,j) = M2_disc(:,i,j) + a0_til(:,i,j);
			end
		end
	end

	numDir = 0; % number of Dirichlet BC
	nowRobin = 0; % first non-Dirichlet BC has been detected.
	for i=1:n
		if obj.ppide.b_op1.b_n(i,i)==0
			if ~nowRobin
				numDir = numDir+1;
			else
				error('The BC on the left side are not sorted. First have to be Dirichlet, then Robin/Neumann!')
			end
		else
			nowRobin = 1;
		end
	end
	E1 = [eye(numDir);zeros(n-numDir,numDir)]; % selection matrix of DirBC
	E2 = [zeros(numDir,n-numDir); eye(n-numDir)]; % selectio matrix of RobBC

	% TODO:
	% CHECK if nd >1!
	G1_til(1:ndiscRegEq,1:n,1:nd) = 0;
	H1(1:ndiscRegEq,1:n,1:nv) = 0;
	for z_idx =1:ndiscRegEq
		G1_til(z_idx,1:n,1:nd) = reshape(G1_BS(z_idx,:,:),[n nd])+ ...
			(reshape(M1_disc(z_idx,:,:),[n n])*(E1*E1.') + reshape(M2_disc(z_idx,:,:),[n n])*(E2*E2.'))*G2;
		%   z  x n x nd
		H1(z_idx,:,:) = reshape(G1_til(z_idx,:,:),[n nd])*sigMod.pd;
	end
	lhs = zeros(ndiscRegEq,n,nv);
	rhs = zeros(ndiscRegEq,n,nv);
	err = zeros(ndiscRegEq,n,nv);
	for z_idx=1:ndiscRegEq
		lhs(z_idx,:,:) = sd(l_disc(z_idx,:,:))*sd(Pi_zz(z_idx,:,:))...
			- sd(muSys(z_idx,:,:))*sd(Pi(z_idx,:,:))...
			- sd(Pi(z_idx,:,:))*sigMod.S-sd(a0_til(z_idx,:,:))*sd(Pi(1,:,:));
		rhs(z_idx,:,:) = -sd(H1(z_idx,:,:));
		err(z_idx,:,:) = sd(lhs(z_idx,:,:)) - sd(rhs(z_idx,:,:));
	end
	KI = kernel.KI;
	mTN = obj.ppide.ctrl_pars.makeTargetNeumann;
	errbl = obj.ppide.b_op1.b_n*sd(Pi_z(1,:,:))...
		+~mTN*obj.ppide.b_op1.b_d*sd(Pi(1,:,:)) - H2;
	TcInvPi = permute(dps.parabolic.control.backstepping.Tc(permute(Pi,[3 1 2]),-KI,zdiscRegEq,zdiscKernel),[2 3 1]);
	CTcInvPi(1:m,1:nv) = 0;
	for j=1:nv
		CTcInvPi(:,j) = obj.ppide.c_op.use_output_operator(TcInvPi(:,:,j),zdiscRegEq);
	end
	errbc = CTcInvPi - sigMod.pr + H4;


	figure('Name','Fehler Regulator Equations')
	hold on
	for i=1:n
		for j=1:nv
			firstline = plot(zdiscRegEq,err(:,i,j),'DisplayName',[num2str(i) num2str(j)]);
			curCol = get(firstline, 'Color');
			plot(0,errbl(i,j),'x','DisplayName',[num2str(i) num2str(j)],'Color',curCol)
			if i<=m
				plot(1,errbc(i,j),'x','DisplayName',[num2str(i) num2str(j)],'Color',curCol)
			end
	% 		plot(zdisc,cumtrapz(zdisc,err(:,i,j).'),'DisplayName',['Integral:' num2str(i) num2str(j)]);
		end
	end
	legend('-DynamicLegend')

	figure('Name','Pi, Pi_z')
	hold on
	for i=1:n
		for j=1:nv
			firstline = plot(zdiscRegEq,Pi(:,i,j),'DisplayName',['pi ' num2str(i) num2str(j)]);
			curCol = get(firstline, 'Color');
			plot(zdiscRegEq,Pi_z(:,i,j),'--','DisplayName',['pi_z ' num2str(i) num2str(j)],'color',curCol);
		end
	end
	legend('-DynamicLegend')
	if nargout>0
		varargout{1} = obj;
	end
end