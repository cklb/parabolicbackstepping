function varargout = plotNorm(obj,~)
% plotNorm plot Norms of system, transformed and target system
% obj = plotNorm(rel) plots the norms normalized wrt their initial value.

if ~obj.simulated
	obj = obj.simulate();
end
if ~obj.simulatedTarget
	obj=obj.simTarget();
end

n=obj.ppide.n;
p = size(obj.ppide.b_2,2); 
if p > n
	folding=1;
	name = ['NormClosedLoop, \check{z}_0 = ' num2str(obj.ppide.ctrl_pars.foldingPoint)]; 
else
	folding=0;
	name = 'NormClosedLoop';
end

if nargin < 2
	x = obj.xNorm;
	xTransformed = obj.xTransformedNorm;
	xTarget = obj.xTargetNorm;
else
	x = obj.xNorm/obj.xNorm(1);
	xTransformed = obj.xTransformedNorm/obj.xTransformedNorm(1);
	xTarget = obj.xTargetNorm/obj.xTargetNorm(1);
end


figHandle = figure('Name','NormClosedLoop');
hold on
title(name);
box on
t = obj.t;
t_target = obj.tTarget;
h1 = plot(t,x,'DisplayName','$\|x(t)\|$');
h2 = plot(obj.tTransformed,xTransformed,'r','LineWidth',2,'DisplayName','transformiert');
yl = get(gca,'ylim');
set(gca,'ylim',yl);
if ~isempty(obj.xETarget)
	h3 = plot(t_target,xTarget,'b--','LineWidth',2,'DisplayName','target_ist');	
end
too_small = 1;
savetyCounter = 1;
fac = xTransformed(1);
if folding 
	% achtung: geht natuerlich nur fuer konstante mu!
	muCur = obj.ppide.ctrl_pars.mu(1);
	while too_small
		decay_t = fac*exp(-(muCur)*t);
		if ~isempty(obj.xETarget)
			decay_t_target = fac*exp((muCur)*t_target);
		end
		decay_t_transformed = fac*exp((muCur)*obj.tTransformed.');
		if ~isempty(obj.xETarget)
			if ~any( (decay_t - x) < 0) && ~any( (decay_t_target - xTarget) < 0) && ~any( (decay_t_transformed - xTransformed) < 0)
				too_small=false;
			end
		else
			if ~any( (decay_t - y_L2_norm) < 0) && ~any( (decay_t_transformed - xTransformed) < 0)
				too_small=false;
			end
		end
		muCur = muCur*0.99;
		if savetyCounter > 300
			warning('Could not find sufficiently large decay with 300 tries.')
			break
		end
		savetyCounter = savetyCounter + 1;
	end
	h4 = plot(t,decay_t,'lineWidth',1.5,'Color','green');
	if ~isempty(obj.xETarget)
		legend([h2 h3 h4],{'$\|\T[x(t),t]\|$','$\|\tilde{x}(t)\|$',['$\e^{-' sprintf('%.2f',muCur) 't}$']},'interpreter','none');
	else
		legend([h2 h4],{'$\|\T[x(t),t]\|$',['$\e^{-' sprintf('%.2f',muCur) 't}$']},'interpreter','none');
	end
else
	mu_max = max(real(obj.ppide.eigTarget(obj.ppide.ctrl_pars.mu)));
% 	plot(t_target,1.5*max(y_target_L2_norm)*x_target_soll,'g:','lineWidth',2,'DisplayName','target_soll')
	while too_small
		decay_t = fac*exp((mu_max)*t);
		if ~isempty(obj.xETarget)
			decay_t_target = fac*exp((mu_max)*t_target);
		end
		decay_t_transformed = fac*exp((mu_max)*obj.tTransformed.');
		if ~isempty(obj.xETarget)
			if ~any( (decay_t - x) < 0) && ~any( (decay_t_target - xTarget) < 0) && ~any( (decay_t_transformed - xTransformed) < 0)
				too_small=false;
			end
		else
			if ~any( (decay_t - y_L2_norm) < 0) && ~any( (decay_t_transformed - xTransformed) < 0)
				too_small=false;
			end
		end
		fac = fac*1.01;
		if savetyCounter > 300
			warning('Could not find sufficiently large decay with 300 tries.')
			break
		end
		savetyCounter = savetyCounter + 1;
	end
	h4 = plot(t,decay_t,'lineWidth',1.5,'Color','green');
% legend([h1 h2 h3],{'$\|x(t)\|$','$\|\T[x(t)]\|$','$\|\tilde{x}(t)\|$'});
	if ~isempty(obj.xETarget)
		legend([h2 h3 h4],{'$\|\T[x(t),t]\|$','$\|\tilde{x}(t)\|$',['$' sprintf('%.2f',fac) '\e^{\mymucmd}$']},'interpreter','none');
	else
		legend([h2 h4],{'$\|\T[x(t),t]\|$',['$' sprintf('%.2f',fac) '\e^{\mymucmd}$']},'interpreter','none');
	end
end
% ylim([0 1.1*max(x(:))])
xlabel('$t$')
if nargout>0
	varargout{1} = obj;
end
if nargout > 1
	varargout{2} = figHandle;
end

end % function