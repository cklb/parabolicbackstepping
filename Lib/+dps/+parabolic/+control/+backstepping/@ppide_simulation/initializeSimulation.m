function obj = initializeSimulation(obj)
% initializeSimulation initialize ppide_simulation to execute simulation
%   obj = initinitializeSimulation(obj) computes approximations, 
%   controllers and observers to be ready to perform a simulation.

% created on 05/2018 by Simon Kerschbaum

% packages:
import dps.parabolic.system.*
import dps.parabolic.control.backstepping.*
%   Approximation should be computed in constructor but make sure it is:
	if isempty(obj.ppideApproximation)
		obj.ppideApproximation = ppide_approximation(obj.ppide);
		obj.checkEigenvalues();
	end
	if isempty(obj.simPpideApproximation) && ~isempty(obj.simPpide)
		obj.simPpideApproximation = ppide_approximation(obj.simPpide);
	end
	% Otherwise resampling doesnt work in its current form:
	if length(obj.ppide.ctrl_pars.zdiscObs) > obj.ppide.ndisc
		error('Plant approximation resolution must always be greater than observer approximation resolution!')
	end

	% Compute controllerKernel. Internally checks if kernel was already computed
	% TODO: add default values so no controller needs to be computed if not used!
% 	if obj.useStateFeedback
	timer = tic;
		obj = obj.computeControllerKernel;
	time = toc(timer);
% 	disp(['elapsed time for controller kernel: ' num2str(round(time*100)/100) 's']);
% 	end
	if ~obj.controllerFlag %&& obj.useStateFeedback
		if ~obj.useStateFeedbackForInputDelay
			if ~isempty(obj.designPpide)
				obj.controller = ppide_controller(obj.controllerKernel, obj.designPpide, obj.debug);
			else
				obj.controller = ppide_controller(obj.controllerKernel, obj.ppide, obj.debug);
			end
			obj.controllerFlag = 1;
			obj.simulated  = 0;
			obj.simulatedTarget = 0;
		else
			obj.controller = ControllerForInputDelay(obj.controllerKernel, obj.ppide, obj.debug);
			obj.controllerFlag = 1;
			obj.controllerForInputDelayFlag = 1;
		end
	end
	
	% Calculate reference and disturbance feedforward control
	if ~obj.KvFlag
		[obj.Pi, obj.Kv, obj.Piz, obj.Pizz, obj.D] = dps.parabolic.control.backstepping.regulator_equations(obj.ppide, obj.sM, obj.controllerKernel);
		obj.KvFlag = 1;
	end
	if ~obj.KvHatFlag 
		if obj.useIntMod
			% Attention: There is no check, whether the observer configuration
			% fits to the internal model! Always assumes that Output is
			% measured!
			[obj.KvHat, obj.KvQPlant, obj.Q, obj.QTil, obj.DRobust] = robustDecouplingEquations(obj.ppide, obj.sM, obj.controllerKernel);
			obj.KvHatFlag = 1;
		else
			p = size(obj.ppide.b_2,2);
			obj.KvHat = zeros(p, obj.ppide.n*obj.sM.nv); 		
		end
	end
	
	% Compute observerKernel. Internally checks if kernel was already computed
	if obj.useStateObserver || obj.useDisturbanceObserver
		obj = storeObserverKernel(obj);

		% Compute observer gains and approximation
		if ~obj.observerFlag 
			if ~obj.GammaFlag
				obj.Gamma = dps.parabolic.control.backstepping.decoupling_equations(obj.ppide, obj.sM, obj.observerKernel);
				% compare solution with solution gained with generic BVP
				% Solution
	% 			import misc.*
	% 			zdiscPlant = linspace(0, 1, obj.ppide.ndisc);
	% 			zdisc = obj.ppide.ctrl_pars.zdiscObsKernel;
	% 			ndisc = length(zdisc);
	% 			n = obj.ppide.n;
	% 			l_disc = interp1(zdiscPlant, obj.ppide.l_disc, zdisc(:));
	% 			M = -obj.ppide.ctrl_pars.mu_o;
	% 			S = -obj.sM.Sd;
	% 			A0 = zeros(length(zdisc), n, n);
	% 			nv = obj.sM.nd;
	% 			G2 = obj.ppide.g_2; % dim: n x nd % left disturbance input 
	% 			G3 = obj.ppide.g_3; % dim: n x nd % right disturbance input 
	% 			nd = size(G2, 2); % length of disturbance vector
	% 			% calculate H_bar
	% 			G2_bar = G2*obj.sM.pd_til;
	% 			G3_bar = G3*obj.sM.pd_til;
	% 
	% 			p = obj.observerKernel.KI;
	% 			g1_disc = eval_pointwise(obj.ppide.g_1, zdisc);
	% 			ToG1 = permute(Tc(permute(g1_disc, [3 1 2]), -p, zdisc, zdisc), [2 3 1]);
	% 			H1_bar = zeros(ndisc, n, nd);
	% 			H = zeros(ndisc, n, nv);
	% 			for z_idx = 1:ndisc
	% 				H1_bar(z_idx, 1:n, 1:nd) = -squeeze(p(z_idx, 1, :, :))*squeeze(l_disc(1, :, :))...
	% 									 *G2 + sd(ToG1(z_idx, :, :));
	% 				H(z_idx, :, :) = -reshape(H1_bar(z_idx, :, :), [n nd])*obj.sM.pd_til;
	% 			end
	% 			
	% 			% define left boundary operator as output operator:
	% 			% theta_o = B1:
	% 			Bn0 = obj.ppide.b_op1.b_n;
	% 			Bd0 = obj.ppide.b_op1.b_d;
	% 			Bn1 = obj.ppide.b_op2.b_n;
	% 			Bd1 = obj.ppide.b_op2.b_d;
	% 			B1 = dps.output_operator('c_0', Bd0, 'c_d0', Bn0);
	% 			
	% 			% distributed output cannot be passed discretized at the
	% 			% moment!
	% 			A0_bar = obj.observerKernel.a0_bar;
	% 			a0_gen = @(z) reshape(interp1(zdisc, A0_bar, z), [n n]);
	% 			B2 = dps.output_operator('c_1', Bd1, 'c_d1', Bn1, 'c_c', a0_gen);
	% 			Gamma_comp = misc.solveGenericBVP(l_disc, M, S, A0, H, B1, G2_bar, B2, G3_bar, zdisc);

				obj.GammaFlag = 1;
	% 			for i = 1:n
	% 				for j = 1:n
	% 					figure('Name', [num2str(i) num2str(j)])
	% 					hold on
	% 					plot(zdisc, Gamma_comp(:, i, j))
	% 					plot(zdisc, obj.Gamma(:, i, j))
	% 				end
	% 			end
			end
			
			if ~obj.useStateObserverForOutputDelay
				% in domain-measurement: folding
				n = obj.ppide.n;
				if any(obj.ppide.cm_op.c_m(:))  || any(obj.ppide.cm_op.c_dm(:)) % folding
					if ~isequal(obj.ppide.cm_op.c_m,[zeros(n,n);eye(n)]) || ~isequal(obj.ppide.cm_op.c_dm,[eye(n);zeros(n,n)])
						error('When the measurement is in-domain, both the state and the derivative must be measured.')
					end
					obj.observer1 = dps.parabolic.control.backstepping.ppide_observer(...
									obj.observerKernel1, obj.observerPpide1, obj.Gamma, obj.sM);
					obj.observer2 = dps.parabolic.control.backstepping.ppide_observer(...
									obj.observerKernel2, obj.observerPpide2, obj.Gamma, obj.sM);
					obj.observerPpide1.ndisc = length(obj.ppide.ctrl_pars.zdiscObs);
					obj.observerPpide2.ndisc = length(obj.ppide.ctrl_pars.zdiscObs);
					obj.observerApproximation1 = ppide_approximation(obj.observerPpide1);
					obj.observerApproximation2 = ppide_approximation(obj.observerPpide2);
					
				else
					obj.observer = dps.parabolic.control.backstepping.ppide_observer(...
									obj.observerKernel, obj.ppide, obj.Gamma, obj.sM);
					obj.observerApproximation = ppide_approximation(obj.observer.ppide);
				end
			else
				obj.observer = dps.parabolic.control.backstepping.ObserverForOutputDelay(...
								obj.observerKernel, obj.ppide, obj.Gamma, obj.sM);
				obj.observerApproximation = ppide_approximation(obj.observer.ppide);
				obj.observerForOutputDelayFlag = 1;
			end
			obj.observerFlag = 1;
			obj.simulated = 0;
		end
		if isempty(obj.observerApproximation) && ~(any(obj.ppide.cm_op.c_m(:))  || any(obj.ppide.cm_op.c_dm(:)))
			% only if not folding
			obj.observerApproximation = ppide_approximation(obj.observer.ppide);
		end
	end
end

