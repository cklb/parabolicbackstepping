function GH_dual = compute_dual_kernel(~,system,zdisc)
%COMPUTE_DUAL_KERNEL compute the backstepping observer kernel using duality to state feedback kernel
%  GH_dual = ppide_simulation.compute_dual_kernel(system,zdisc)
%    system: ppide_sys of the observer
%    zdisc:  spatial discretization
%    GH_dual still needs to be transformed to get the observer kernel!

% created on 23.04.2018 by Simon Kerschbaum

import misc.*
import numeric.*
import dps.parabolic.system.*
import dps.parabolic.control.backstepping.*
import dps.parabolic.tool.*

% locally store variables:
ndisc=length(zdisc);
zdisc_plant = linspace(0,1,system.ndiscPars);
ndisc_plant = system.ndiscPars;
% interpolate to correct resolution
% lambda_disc = interp1(zdisc_plant,system.l_disc,zdisc(:));
lambda_disc = system.l_disc;
% lambda_diff_disc = interp1(zdisc_plant,system.l_diff_disc,zdisc(:));
lambda_diff_disc = system.l_diff_disc;
lambda_obs = flip(flip(lambda_disc,1),4);
if size(lambda_obs,4)>1
	warning('Has not yet been checked, if lambda needs to be flipped in time for the observer!')
end
% a_disc = interp1(zdisc_plant,system.a_disc,zdisc(:));
a_disc = system.a_disc;
n = system.n;
% get discretized version of integral term
% F_disc = interp1(zdisc_plant,system.f_disc,zdisc(:));
% F_disc = permute(interp1(zdisc_plant,permute(F_disc,[2 1 3 4]),zdisc(:)),[2 1 3 4]);
% permute t to last dimension to be compatible to old implementation. Copuld be reworked!
if nargin(system.f) > 2
	% time dependent
	F_disc = permute(system.f.on(),[1 2 4 5 3]);
else
	% time constant
	F_disc = system.f.on();
end
numtL = size(system.l_disc,4);
if numtL > 1
	% this can be removed, once implemented
	error('Implementation of time varying lambda needs implementation of time varying F for the dual kernel!')
end
numtA = size(system.a_disc,4);
numtNew = max(numtL, numtA);
if numtL>1 && numtA>1 && numtL ~= numtA
	% this case should never happen, because both parameters have resolution ndiscPars
	error('If time dependent, lambda and a must have the same resolution!')
end

% Hartwig edited
% if ndisc~= n
% 	if isa(system.ctrl_pars.mu_o,'function_handle') % function
% 		muHelp = eval_pointwise(system.ctrl_pars.mu_o,zdisc);
% 	elseif isequal(size(system.ctrl_pars.mu_o,1),ndisc) % is discretized 
% 		muHelp = system.ctrl_pars.mu_o;
% 	elseif isequal(size(system.ctrl_pars.mu_o,1),n) % constant matrix, two dimensions
% 		muHelp(1:ndisc,:,:) = repmat(shiftdim(system.ctrl_pars.mu_o,-1),ndisc,1,1);
% 	else % discretized matrix
% 		error('dimensions of mu_o not correct!')
% 	end
% else
% 	error('ndisc=n is a very bad choice!')
% end
tDisc = system.ctrl_pars.tDiscKernel;
if ndisc_plant~= n
	if isa(system.ctrl_pars.mu_o,'function_handle') % function
		if nargin(system.ctrl_pars.mu_o)==1
			muHelp = eval_pointwise(system.ctrl_pars.mu_o,zdisc_plant);
		else
			muHelp = zeros(ndisc_plant,n,n,length(tDisc));
			for z_idx=1:ndisc_plant
				muHelp(z_idx,:,:,:) = permute((eval_pointwise(@(t)system.ctrl_pars.mu_o(zdisc_plant(z_idx),t),tDisc)),[2 3 1]);
			end
		end
	elseif isequal(size(system.ctrl_pars.mu_o,1),length(system.ctrl_pars.zdiscObsKernel)) % is discretized
		if length(size(system.ctrl_pars.mu_o))>3
			muInterpolant = interpolant({system.ctrl_pars.zdiscObsKernel,1:n,1:n,linspace(0,1,size(system.ctrl_pars.mu_o,4))}, system.ctrl_pars.mu_o);
			muHelp = muInterpolant.evaluate(zdisc_plant,1:n,1:n,tDisc);
		else
			muInterpolant = interpolant({system.ctrl_pars.zdiscObsKernel,1:n,1:n}, system.ctrl_pars.mu_o);
			muHelp = muInterpolant.evaluate(zdisc_plant,1:n,1:n);
		end
		%muHelp = ppide.ctrl_pars.mu;
	elseif isequal(size(system.ctrl_pars.mu_o,1),n) % constant matrix, two dimensions
		muHelp(1:ndisc_plant,:,:) = repmat(shiftdim(system.ctrl_pars.mu_o,-1),ndisc_plant,1,1);
	else % discretized matrix
		if size(system.ctrl_pars.mu_o,2)==n && size(system.ctrl_pars.mu_o,3)==n % probably correct dimension but differnt discretization
			muInterpolant = interpolant({linspace(0,1,size(system.ctrl_pars.mu_o,1)),1:n,1:n,linspace(0,1,size(system.ctrl_pars.mu_o,4))}, system.ctrl_pars.mu_o);
			muHelp = muInterpolant.evaluate(zdisc_plant,1:n,1:n,tDisc);
		else
			error('dimensions of mu not correct!')
		end
	end
else
	error('ndisc_plant=n is a very bad choice!')
end
% mu_o = flip(muHelp,4);
numtMu = size(muHelp,4);
numtNewMu = max(numtL, numtMu);

% resample time to kernel resolution. needed because mu and a need same resolution.
% a_disc = misc.translate_to_grid(a_disc,{zdisc_plant,1:n,1:n,linspace(0,1,numtA)},'gridOld',{zdisc_plant,1:n,1:n,linspace(0,1,numtA)});
numtA = size(a_disc,4);
numtF = size(F_disc,5);

% mu_o = system.ctrl_pars.mu_o;
% Store system parameters in duality form:
% right dirichlet boundary matrix becomes left one
if isa(system.b_op2.b_d,'function_handle')
	% better: discretize here...
	BdrDisc = permute(misc.eval_pointwise(system.b_op2.b_d,tDisc),[2 3 1]);
	if any(misc.mydiag(BdrDisc-misc.makeDiag(misc.mydiag(BdrDisc,1:n,1:n)),1:n,1:n))
		error('Bdr needs to be a diagonal matrix for the observer!')
	end
	BdlObs = -flip(BdrDisc,3);
	for i=1:n
		if system.b_op2.b_n(i,i) == 0 % dirichlet
			BdlObs(i,i,:) = 1;
		end
	end
else
	BdlObs = zeros(n,n,size(system.b_op2.b_d,3));
	for i=1:n
		if system.b_op2.b_n(i,i) == 0 % dirichlet
			BdlObs(i,i,:) = 1;
		else
			BdlObs(i,i,:) = -flip(system.b_op2.b_d(i,i,:),3); % needs to be diagonal!
		end
	end
end
if size(BdlObs,3)>1
	BdlObs = misc.translate_to_grid(BdlObs,{1:n,1:n,tDisc},'gridOld',{1:n,1:n,linspace(0,1,size(BdlObs,3))});
end
b_op1_obs = boundary_operator('z_b',0,'b_d',BdlObs,'b_n', system.b_op2.b_n);
%b_op1_obs = boundary_operator('z_b',0,'b_d',Q0_obs,'b_n',eye(n));
b_op2_obs = b_op1_obs; % right boundary operator wont be used but needs to be specified.
b_op2_obs.z_b = 1; % just for correctness

% compute dual F: first change z,zeta, transpose and make 1-x for x in z,zeta,t
f_obs_temp = flip(flip(flip(permute(F_disc,[2 1 4 3 5]),1),2),5);

a_obs = zeros(ndisc_plant,n,n,numtA); % preallocate
mu_obs = zeros(ndisc_plant,n,n,numtMu); % preallocate
for z_idx  = 1:ndisc_plant
	for tIdx = 1:numtNew
		if numtA > 1
			tIdxA = tIdx;
		else
			tIdxA = 1;
		end
		if numtL > 1
			tIdxL = tIdx;
		else
			tIdxL = 1;
		end
		a_obs(z_idx,:,:,tIdx) = ...
			reshape(lambda_obs(z_idx,:,:,end-tIdxL+1),[n n])*...
			(reshape(a_disc(end-z_idx+1,:,:,end-tIdxA+1),[n n]).')...
			/squeeze(lambda_obs(z_idx,:,:,end-tIdxL+1));
	end
	for tIdx=1:numtNewMu
		if numtMu >1
			tIdxMu = tIdx;
		else
			tIdxMu = 1;
		end
		if numtL > 1
			tIdxL = tIdx;
		else
			tIdxL = 1;
		end
		mu_obs(z_idx,:,:,tIdxMu) = ...
			reshape(lambda_obs(z_idx,:,:,end-tIdxL+1),[n n])*...
			(reshape(muHelp(end-z_idx+1,:,:,end-tIdxMu+1),[n n]).')...
			/reshape(lambda_obs(z_idx,:,:,end-tIdxL+1),[n n]);
	end
end
% get F in observer representatin without for-loops because 3 for loops are very inefficient!
if numtF > 1
	permVec = [2 1 3 7 4 5 6];
else
	permVec = [2 1 3 6 4 5];
end
% lambda is a diagonal matrix so inversion can be performed elementwise.
lambda_obsInvTemp = 1./lambda_obs;
lambda_obsInv = lambda_obsInvTemp;
lambda_obsInv(lambda_obsInv==Inf) = 0;
f_obs = permute(...
	misc.multArray(...
		misc.multArray(...
			lambda_obs,f_obs_temp,3,3,1 ... % z i j * z zeta i j t
		), ... % = z i zeta j t
		reshape(lambda_obsInv,[1 1 ndisc_plant n n]),4,4,3 ... % * 1 1 zeta i j
	), ... % = zeta z i t 1 1 j
	permVec... % = z zeta i j t
);

% store correct mu which is used by the kernel computation
obs_ctrl_pars = system.ctrl_pars;
% Hartwig edited
% mu_o_til = zeros(ndisc,n,n);
% for tidx = 1:size(mu_o,4)
% 	for it = 1:n
% 		for jt = 1:n
% 			mu_o_til(:,jt,it,tidx) = fliplr(reshape(mu_o(:,it,jt,tidx),[1,ndisc]));
% 		end
% 	end
% end
obs_ctrl_pars.mu = mu_obs;
obs_ctrl_pars.zdiscCtrl = zdisc;
% obs_ctrl_pars.mu = mu_o_til;

% create object to represent parameters for the determination of the
% kernel:
% Attention: Lambda needs to be flipped, if time dependent!
ppide_obs = ppide_sys(...
				'l', lambda_obs,...
				'l_diff',-(flip(lambda_diff_disc,1)),...
				'a', a_obs,...
				'b_op1',b_op1_obs,...
				'b_op2',b_op2_obs,...
				'b_1',zeros(n,n),... % Eing�nge sind egal!
				'b_2',eye(n,n),... % ein Eingang f�r jeden Zustand
				'ndiscPars',ndisc_plant,...
				'f',f_obs,...
				'a_0',@(z)zeros(n,n)+0*z,...
				'ctrl_pars',obs_ctrl_pars,...
				'name','Dual System');

% create the dual kernel
kern_dual = ppide_kernel(ppide_obs,zdisc);
use_F_handle = @(ppidek) use_F_ppides(ppidek,ppide_obs);
% Caluclate solution of the dual kernel problem.
[GH_dual, ~] = fixpoint_iteration(kern_dual,use_F_handle,ppide_obs.ctrl_pars.it_min,ppide_obs.ctrl_pars.it_max,ppide_obs.ctrl_pars.tol);
% Get in original z/zeta-coordinates
GH_dual = GH_dual.transform_kernel(ppide_obs);
% This is the dual solution.
% (which still needs transformations, see
% ppide_simulation.compute_observer_kernel)
% dps.parabolic.control.backstepping.checkControllerKernel(ppide_obs,GH_dual,1)

end

