function xSplitted = splitStates(obj, x)
%SPLITSTATES extracts the states resulting from simulations which have the
%dimenions x \in R^{numel(t), ppide.n * ndisc} into the individual states
%x_i combined in xSplitted \in R^{numel(t), ndisc, ppide.n}.
xSplitted = reshape(x, [size(x, 1), size(x, 2) / obj.ppide.n, obj.ppide.n]);
end