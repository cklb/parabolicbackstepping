function varargout = meshObserverErrorSparse(obj)
	if ~obj.useStateObserver
		warning('Cannot mesh observer error, observer is deactivated!')
		if nargout>0
			varargout{1} = obj;
		end
		return
	end
	if ~obj.simulated
		obj = obj.simulate();
	end
	ndisc_plant = obj.ppide.ndisc;
	zdisc_plant = linspace(0,1,ndisc_plant);
	zdisc_obs = obj.ppide.ctrl_pars.zdiscObs;
	ndisc_obs = length(zdisc_obs);
	plot_pars = obj.plotPars;
	n=obj.ppide.n;

	for i=1:n
		figure('name',['xHat' num2str(i)])
% 		mesh(t,zdisc_plant,y(:,(i-1)*ndisc_plant+1:i*ndisc_plant).')
		misc.mesh_sparse(obj.t,zdisc_obs,obj.xHat(:,(i-1)*ndisc_obs+1:i*ndisc_obs).',20,plot_pars.plot_res_2D_num_lines,plot_pars.plot_res_2D_lines,plot_pars.plot_res_2D_lines);
		hold on
		box on
		title(['$xHat_' num2str(i) '(z,t)$'])
		set(gca,'Ydir','reverse')
		xlabel('$t$')
		ylabel('$z$')
	end
	for i=1:n
		res_x(:,(i-1)*ndisc_obs+1:i*ndisc_obs) = interp1(zdisc_plant,obj.x(:,(i-1)*ndisc_plant+1:i*ndisc_plant).',zdisc_obs(:)).';
	end
	err_xQ = obj.xQuantity - obj.xHatQuantity;
	for i=1:n
		err_x = obj.ppide.sys2Sim(err_xQ(i).on({obj.t,zdisc_plant}));
		figure('name',['ex' num2str(i)])
% 		mesh(t,zdisc_plant,y(:,(i-1)*ndisc_plant+1:i*ndisc_plant).')
		misc.mesh_sparse(obj.t,zdisc_plant,err_x.',20,plot_pars.plot_res_2D_num_lines,plot_pars.plot_res_2D_lines,plot_pars.plot_res_2D_lines);
		hold on
		box on
		title(['$ex_' num2str(i) '(z,t)$'])
		set(gca,'Ydir','reverse')
		xlabel('$t$')
		ylabel('$z$')
	end
	if nargout>0
		varargout{1} = obj;
	end
end