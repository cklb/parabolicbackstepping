function varargout = meshStateSparse(obj,varargin)
	import misc.*
	if ~obj.simulated
		obj = obj.simulate();
	end
	ndisc_plant = obj.ppide.ndisc;
	zdisc_plant = linspace(0,1,ndisc_plant);
	plot_pars = obj.plotPars;
	c_op = obj.ppide.c_op;
	n=obj.ppide.n;
% 	figure('Name','Verlauf_geregeltes_System')
% 	hold on
% % 	plot(t,2*exp((-ppide.ctrl_pars.mu(1,1))*t),'lineWidth',2,'Color','green')
% 	plot(obj.t,obj.x)
	for i=1:n
		figure('name',['x' num2str(i)])
% 		mesh(t,zdisc_plant,y(:,(i-1)*ndisc_plant+1:i*ndisc_plant).')
		misc.mesh_sparse(obj.t, zdisc_plant, obj.x(:,(i-1)*ndisc_plant+1:i*ndisc_plant).',...
			20,plot_pars.plot_res_2D_num_lines, plot_pars.plot_res_2D_lines, ...
			plot_pars.plot_res_2D_lines, varargin);
		hold on
		box on
		if size(obj.ppide.b_1,2)>n % folding
			xFoldingPoint = interp1(zdisc_plant,obj.x(:,(i-1)*ndisc_plant+1:i*ndisc_plant).',obj.ppide.ctrl_pars.foldingPoint).';
			plot3(obj.t,repmat(obj.ppide.ctrl_pars.foldingPoint,length(obj.t),1),xFoldingPoint,'r','linewidth',2);
		end
		if any(obj.ppide.c_op.c_m(:,i)) % Messung Punkt
			plot3(obj.t,repmat(obj.ppide.c_op.z_m,length(obj.t),1),obj.x(:,(i-1)*ndisc_plant+get_idx(c_op.z_m,zdisc_plant)),'r','linewidth',2);
		end
		if any(c_op.c_0(:,i)) % Messung links
			plot3(obj.t,zeros(length(obj.t),1),obj.x(:,(i-1)*ndisc_plant+1),'r','linewidth',2);
		end
		if any(c_op.c_1(:,i)) % Messung rechts
			plot3(obj.t,zeros(length(obj.t),1),obj.x(:,(i)*ndisc_plant),'r','linewidth',2);
		end
		title(['$x_' num2str(i) '(z,t)$'])
		set(gca,'Ydir','reverse')
		xlabel('$t$')
		ylabel('$z$')
	end
	if nargout>0
		varargout{1} = obj;
	end
end