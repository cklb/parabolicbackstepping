function [GH_obs, GH_dual] = compute_observer_kernel(obj,system,zdisc,GH_dual)
%COMPUTE_OBSERVER_KERNEL Compute kernel of observer backstepping transformation using duality.
%  GH_obs = ppide_simulation.compute_observer_kernel(system,zdisc)
%    computes the kernel of the observer backstepping transformation 
%      ex = ex_til - int_0^z P(z,zeta) ex_til(zeta) d zeta
%    using the duality to the state feedback transformation
%      x_til = x - int_0^z k(z,zeta) x (zeta) d zeta.
%  [GH_obs, GH_dual] = ...
%    also returns the dual kernel, which is the solution of the dual observer kernel equations
%  [...] = ppide_simulation.compute_observer_kernel(system,zdisc,GH_dual)
%    only performs the required transformations using the already computed
%    dual kernel GH_dual.
%    

% created on 19.04.2018 by Simon Kerschbaum

% 1. Get system in representation to compute kernel via usual methods.
import misc.*
import numeric.*
import dps.parabolic.control.backstepping.*


ndisc=length(zdisc);
zdisc_plant = linspace(0,1,system.ndiscPars);
lambda_disc = interp1(zdisc_plant,system.l_disc,zdisc(:));
lambda_diff_disc = interp1(zdisc_plant,system.l_diff_disc,zdisc(:));
n = system.n;
numtL = size(lambda_disc,4);

% get dual kernel if not yet passed:
if nargin<4
	GH_dual = obj.compute_dual_kernel(system,zdisc);
end
numtKernel = size(GH_dual.value{1,1}.K,3);

% Transform to original solution:
GH_obs = GH_dual; % overtake parameters
for i=1:n
	for j=1:n
		for z_idx = 1:ndisc
			for zeta_idx = 1:ndisc
				% only solution K(z,zeta) in the original coordinates is
				% transfomed, of course.
				GH_obs.value{i,j}.K(z_idx,zeta_idx,:) = ...
					reshape(lambda_disc(z_idx,i,i,:),[1, numtL])...
					.*fliplr(reshape(GH_dual.value{j,i}.K(end-zeta_idx+1,end-z_idx+1,:),[1,numtKernel]))...
					./reshape(lambda_disc(zeta_idx,j,j,:),[1,numtL]);
% 				GH_obs.value{i,j}.K(z_idx,zeta_idx,:) = ...
% 					reshape(lambda_disc(z_idx,i,i,:),[1, numtL])...
% 					.*(reshape(GH_dual.value{j,i}.K(end-zeta_idx+1,end-z_idx+1,:),[1,numtKernel]))...
% 					./reshape(lambda_disc(zeta_idx,j,j,:),[1,numtL]);
			end
		end
		% the analytical derivatives at the boundaries need to be
		% transformed separatedly, too.
		GH_obs.value{i,j}.dzeta_K_z_0 =...
			-reshape(lambda_disc(:,i,i,:),[ndisc,numtL])...
			.*fliplr(reshape(flip(GH_dual.value{j,i}.K(end,:,:),2),[ndisc,numtKernel]))...
			./reshape(lambda_disc(1,j,j,:).^2,[1,numtL]) ...
			.*reshape(lambda_diff_disc(1,j,j),[1 numtL])...
			- reshape(lambda_disc(:,i,i,:),[ndisc,numtL])...
			.* fliplr(flip(GH_dual.value{j,i}.dz_K_1_zeta,1))... 
			./ reshape(lambda_disc(1,j,j,:),[1,numtL]);
		GH_obs.value{i,j}.dz_K_1_zeta =...
			reshape(lambda_diff_disc(end,i,i,:),[1,numtL])...
			*fliplr(reshape(flip(GH_dual.value{j,i}.K(:,1,:),1),[ndisc,numtKernel]))...
			./reshape(lambda_disc(:,j,j,:),[ndisc,numtL])...
			- reshape(lambda_disc(end,i,i,:),[1,numtL])...
			* fliplr(flip(GH_dual.value{j,i}.dzeta_K_z_0,1))...
			./reshape(lambda_disc(:,j,j,:),[ndisc numtL]);
% 		GH_obs.value{i,j}.dzeta_K_z_0 =...
% 			-reshape(lambda_disc(:,i,i,:),[ndisc,numtL])...
% 			.*(reshape(flip(GH_dual.value{j,i}.K(end,:,:),2),[ndisc,numtKernel]))...
% 			./reshape(lambda_disc(1,j,j,:).^2,[1,numtL]) ...
% 			.*reshape(lambda_diff_disc(1,j,j),[1 numtL])...
% 			- reshape(lambda_disc(:,i,i,:),[ndisc,numtL])...
% 			.* (flip(GH_dual.value{j,i}.dz_K_1_zeta,1))... 
% 			./ reshape(lambda_disc(1,j,j,:),[1,numtL]);
% 		GH_obs.value{i,j}.dz_K_1_zeta =...
% 			reshape(lambda_diff_disc(end,i,i,:),[1,numtL])...
% 			*(reshape(flip(GH_dual.value{j,i}.K(:,1,:),1),[ndisc,numtKernel]))...
% 			./reshape(lambda_disc(:,j,j,:),[ndisc,numtL])...
% 			- reshape(lambda_disc(end,i,i,:),[1,numtL])...
% 			* (flip(GH_dual.value{j,i}.dzeta_K_z_0,1))...
% 			./reshape(lambda_disc(:,j,j,:),[ndisc numtL]);
% 		for z_idx=1:ndisc
% 			grad_op = get_grad_op(zdisc(z_idx:end));
% 			test_dz_K_1_zeta_i_jz = ...
% 				grad_op*GH_obs.value{i,j}.K(z_idx:end,z_idx);
% 			test_dz_K_1_zeta_i_j(z_idx) = test_dz_K_1_zeta_i_jz(end);
% 		end
% 		errdzK = test_dz_K_1_zeta_i_j - GH_obs.value{i,j}.dz_K_1_zeta;
	end
end

% Calculate inverse observer kernel using reciprocit.
GH_obs.KI = GH_obs.invert_kernel;

% A0_bar needs to be stored. 
GH_obs = GH_obs.get_a0_bar(system);







end

