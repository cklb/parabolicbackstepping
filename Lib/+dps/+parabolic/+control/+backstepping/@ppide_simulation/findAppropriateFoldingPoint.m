function ranges = findAppropriateFoldingPoint(~,lambdaInterp,n,ndisc,ndiscT)
%findAppropriateFoldingPoint find ranges, where folding point is ok.


zdisc = linspace(0,1,ndisc);
nF = 2*n;

% check every point as a folding point. Boundaries cannot be folding points
ranges = [];
rangesRow = 1;
rangesCol = 1;
for zIdx = 2:ndisc-1
	z0 = zdisc(zIdx);
	zRight = linspace(z0,1,ndisc);
	dZ = diff(zRight);
	zLeft = linspace(0,z0-dZ(1),ndisc);

	lambda = zeros(ndisc,n,n,ndiscT);
	lambda(:,1:n,1:n,1:ndiscT) = lambdaInterp.evaluate(fliplr(zLeft),1:n,1:n,1:ndiscT)/(z0.^2);
	lambda(:,n+1:nF,n+1:nF,1:ndiscT) = lambdaInterp.evaluate(zRight,1:n,1:n,1:ndiscT)/((1-z0).^2);
	
	ok=1;
	for i=1:nF-1
		for j=i+1:nF
			dG = diff(lambda(:,i,i,:)>lambda(:,j,j,:));
			dL = diff(lambda(:,i,i,:)<lambda(:,j,j,:));
			dE = diff(lambda(:,i,i,:)==lambda(:,j,j,:));
			if any(dG(:)) || any(dL(:)) || any(dE(:)) % folding point is not ok
				ok = 0;
			end
		end
	end
	if ~ok
		if rangesCol == 2
			ranges(rangesRow,rangesCol) = zdisc(zIdx-1); % last one was still ok.
			rangesCol = 1;
			rangesRow = rangesRow+1; % search for next range
		end
	else % point was ok
		% want the beginning of a range
		if rangesCol==1
			ranges(rangesRow,rangesCol) = z0;
			rangesCol =2; % now look for the end of a range
		end
	end
end % for
if rangesCol == 2 % did not find an end of the last range
	ranges(rangesRow,rangesCol) = zdisc(end-1);
end


end % function

