function designPpide = foldPpide(ppideSimulation,varargin)
%foldPpide Create parameters of folded system for bilateral control

ppide = ppideSimulation.ppide;
obj = ppideSimulation;

found=0;
passed = [];
arglist = {'preserve','zF'};
% preserve: insert the folding point at both left and right part. Important for observer error!
% zF: folding Point
if ~isempty(varargin)
	if mod(length(varargin),2) % uneven number
		error('When passing additional arguments, you must pass them pairwise!')
	end
	for index = 1:2:length(varargin) % loop through all passed arguments:
		for arg = 1:length(arglist)
			if strcmp(varargin{index},arglist{arg})
				passed.(arglist{arg}) = varargin{index+1};
				found=1;
				break
			end 
		end % for arg
		% argument wasnt found in for-loop
		if ~found
			error([varargin{index} ' is not a valid property to pass to fold!']);
		end
		found=0; % reset found
	end % for index
end 
if ~isfield(passed,'preserve')
	passed.preserve = 0;
end
if ~isfield(passed,'zF')
	z0 = ppide.ctrl_pars.foldingPoint;
else
	z0 = passed.zF;
end

% check if the system has parameters which cant be handled by folding:
if misc.anyn(ppide.f.on)
	error('The integral term must be zero for folding.')
end
if any(ppide.a_0(:))
	error('The local term must be zero for folding.')
end
n = ppide.n;
ndisc = ppide.ndiscPars;
zdisc = linspace(0,1,ndisc);
nF = 2*n;

if isscalar(z0) % scalar
	z0 = ones(1,n)*z0;
else
% 	if length(z0)~= n
% 		error(['When passing the folding point as a vector, it must contain n=' num2str(n) ' elements.'])
% 	end
	error('The folding point must be a scalar!')
end

	
lambdaInterp = numeric.interpolant({zdisc,1:n,1:n,1:size(ppide.l,4)},ppide.l);
lambdaDiffInterp = numeric.interpolant({zdisc,1:n,1:n,1:size(ppide.l_diff,4)},ppide.l_diff);
aInterp = numeric.interpolant({zdisc,1:n,1:n,1:size(ppide.a,4)},ppide.a);
lambda = zeros(ndisc,nF,nF,size(ppide.l,4));
lambdaDiff = zeros(ndisc,nF,nF,size(ppide.l,4));
a = zeros(ndisc,nF,nF,size(ppide.a,4));
% left and right part of z for each state
zLeft = zeros(n,ndisc);
zRight = zeros(n,ndisc);
if ~passed.preserve
	for i=1:n
		zRight(i,:) = linspace(z0(i),1,ndisc);
		dZ = diff(zRight(i,:));
		zLeft(i,:) = linspace(0,z0(i)-dZ(1),ndisc);
	end
else
	for i=1:n
		zRight(i,:) = linspace(z0(i),1,ndisc);
		zLeft(i,:) = linspace(0,z0(i),ndisc);
	end
end
for i=1:n
	lambda(:,i,i,1:size(ppide.l,4)) = lambdaInterp.evaluate(fliplr(zLeft(i,:)),i,i,1:size(ppide.l,4))/(zLeft(i,end).^2);
	lambdaDiff(:,i,i,1:size(ppide.l,4)) = -lambdaDiffInterp.evaluate(fliplr(zLeft(i,:)),i,i,1:size(ppide.l,4))/(zLeft(i,end));
	lambda(:,n+i,n+i,1:size(ppide.l,4)) = lambdaInterp.evaluate(zRight(i,:),i,i,1:size(ppide.l,4))/((1-z0(i)).^2);
	lambdaDiff(:,n+i,n+i,1:size(ppide.l,4)) = lambdaDiffInterp.evaluate(zRight(i,:),i,i,1:size(ppide.l,4))/((1-z0(i)));
end

tDisc = linspace(0,1,ppide.ndiscPars);
varlist = {'mu','mu_o'};
for i=1:length(varlist)
	mu = varlist{i};
	if ndisc~= n
		if isa(ppide.ctrl_pars.(mu),'function_handle') % function
			if nargin(ppide.ctrl_pars.(mu))==1
				muHelp = eval_pointwise(ppide.ctrl_pars.(mu),zdisc);
			else % time dependent mu
				muHelp = zeros(ndisc,n,n,length(tDisc));
				for z_idx=1:ndisc
					muHelp(z_idx,:,:,:) = permute((eval_pointwise(@(z)ppide.ctrl_pars.(mu)(zdisc(z_idx),z),tDisc)),[2 3 1]);
				end
			end
		elseif size(ppide.ctrl_pars.(mu),1) > length(ppide.ctrl_pars.zdiscCtrl) % is discretized
			if length(size(ppide.ctrl_pars.(mu)))>3
				muInterpolant = interpolant({linspace(0,1,size(ppide.ctrl_pars.(mu),1)),1:n,1:n,tDisc}, ppide.ctrl_pars.(mu));
				muHelp = muInterpolant.evaluate(zdisc,1:n,1:n,tDisc);
			else
				muInterpolant = interpolant({linspace(0,1,size(ppide.ctrl_pars.(mu),1)),1:n,1:n}, ppide.ctrl_pars.(mu));
				muHelp = muInterpolant.evaluate(zdisc,1:n,1:n);
			end
			%muHelp = ppide.ctrl_pars.mu;
		elseif isequal(size(ppide.ctrl_pars.(mu),1),n) % constant matrix, two dimensions
			muHelp(1:ndisc,:,:) = repmat(shiftdim(ppide.ctrl_pars.(mu),-1),ndisc,1,1);
		else % discretized matrix
			error('dimensions of mu not correct!')
		end
	else
		error('ndisc=n is a very bad choice!')
	end
	muTemp{i} = zeros(ndisc,nF,nF,size(muHelp,4));
	muInterpTemp{i} = numeric.interpolant({zdisc,1:n,1:n,1:size(muHelp,4)},muHelp);
end
mu = muTemp{1};
muInterp = muInterpTemp{1};
mu_o = muTemp{2};
mu_oInterp = muInterpTemp{2};

for i=1:n
	for j=1:n
		for zIdx = 1:ndisc
% 			if i<= n
				if zLeft(i,zIdx) < z0(j)
					a(zIdx,i,j,1:size(ppide.a,4)) = aInterp.evaluate(zLeft(i,end-zIdx+1),i,j,1:size(ppide.a,4));
					mu(zIdx,i,j,1:size(mu,4)) = muInterp.evaluate(zLeft(i,end-zIdx+1),i,j,1:size(mu,4));
					mu_o(zIdx,i,j,1:size(mu_o,4)) = mu_oInterp.evaluate(zLeft(i,end-zIdx+1),i,j,1:size(mu_o,4));
				else % zLeft >= z0(j)
					a(zIdx,i,j+n,1:size(ppide.a,4)) = aInterp.evaluate(zLeft(i,end-zIdx+1),i,j,1:size(ppide.a,4));
					mu(zIdx,i,j+n,1:size(mu,4)) = muInterp.evaluate(zLeft(i,end-zIdx+1),i,j,1:size(mu,4));
					mu_o(zIdx,i,j+n,1:size(mu_o,4)) = mu_oInterp.evaluate(zLeft(i,end-zIdx+1),i,j,1:size(mu_o,4));
				end
% 			else % i>n
				if zRight(i,zIdx) < z0(j)
					a(zIdx,i+n,j,1:size(ppide.a,4)) = aInterp.evaluate(zRight(i,zIdx),i,j,1:size(ppide.a,4));
					mu(zIdx,i+n,j,1:size(mu,4)) = muInterp.evaluate(zRight(i,zIdx),i,j,1:size(mu,4));
					mu_o(zIdx,i+n,j,1:size(mu_o,4)) = mu_oInterp.evaluate(zRight(i,zIdx),i,j,1:size(mu_o,4));
				else % zRight >= z0(j)
					a(zIdx,i+n,j+n,1:size(ppide.a,4)) = aInterp.evaluate(zRight(i,zIdx),i,j,1:size(ppide.a,4));
					mu(zIdx,i+n,j+n,1:size(mu,4)) = muInterp.evaluate(zRight(i,zIdx),i,j,1:size(mu,4));
					mu_o(zIdx,i+n,j+n,1:size(mu_o,4)) = mu_oInterp.evaluate(zRight(i,zIdx),i,j,1:size(mu_o,4));
				end
% 			end
		end
	end
end

if obj.debug
	% for debug:plot all lambda
	figure('Name','Lambda after folding')
	hold on
	for i=1:nF
		plot(zdisc,lambda(:,i,i,1),'displayName',num2str(i));
	end
	legend('-dynamicLegend')

	figure('Name','mu after folding')
	hold on
	for i=1:nF
		for j=1:nF
		plot(zdisc,mu(:,i,j,1),'displayName',[num2str(i) ',' num2str(j)]);
		end
	end
	legend('-dynamicLegend')


	figure('Name','A after folding')
	hold on
	for i=1:nF
		for j=1:nF
		plot(zdisc,a(:,i,j,1),'displayName',[num2str(i) ',' num2str(j)]);
		end
	end
	legend('-dynamicLegend')
end

% check whether all lambda are unequal:
% think about letting matlab try a lot of choices to find an appropriate z0 or give the ranges
% where z0 is ok
ranges =  ppideSimulation.findAppropriateFoldingPoint(lambdaInterp,n,ndisc,size(ppide.l,4));
if obj.debug
	fprintf(['Possible single folging points must be within the rows of\n']);
	disp(num2str(ranges));
end

for i=1:nF-1
	for j=i+1:nF
		dG = diff(lambda(:,i,i,:)>lambda(:,j,j,:));
		dL = diff(lambda(:,i,i,:)<lambda(:,j,j,:));
		dE = diff(lambda(:,i,i,:)==lambda(:,j,j,:));
		if any(dG(:)) || any(dL(:)) || any(dE(:))
			fprintf(['Possible single folging points must be within the rows of\n']);
			disp(num2str(ranges));
			error('There are intersections in lambda after folding! Choose a different folding point!')
		end
		
	end
end

% boundary matrices:
% 1. right boundary matrix
E1 = ppide.b_op1.Sd;
E2 = ppide.b_op1.Sr;
Sd = ppide.b_op2.Sd;
Sr = ppide.b_op2.Sr;
if isa(ppide.b_op1.b_d,'function_handle')
	Q0 = @(t) E2.'*ppide.b_op1.b_d(t)*E2;
else
	Q0 = E2.'*ppide.b_op1.b_d*E2;
end
if isa(ppide.b_op2.b_d,'function_handle')
	Q1 = @(t) Sr.'*ppide.b_op2.b_d(t)*Sr;
else
	Q1 = Sr.'*ppide.b_op2.b_d*Sr;
end
% all combinations of function handle and no function handle
if isa(ppide.b_op1.b_d,'function_handle')
	if isa(ppide.b_op2.b_d,'function_handle')
		Bd1br = @(t) [(E1*E1.')+E2*Q0(t)*E2.', zeros(n,n);
					  zeros(n,n)               (Sd*Sd.')+Sr*Q1(t)*Sr.'];
	else
		Bd1br = @(t) [(E1*E1.')+E2*Q0(t)*E2.', zeros(n,n);
					  zeros(n,n)               (Sd*Sd.')+Sr*Q1*Sr.'];
	end
else
	if isa(ppide.b_op2.b_d,'function_handle')
		Bd1br = @(t) [(E1*E1.')+E2*Q0*E2.', zeros(n,n);
					  zeros(n,n)            (Sd*Sd.')+Sr*Q1(t)*Sr.'];
	else
		Bd1br = [(E1*E1.')+E2*Q0*E2.', zeros(n,n);
			      zeros(n,n)            (Sd*Sd.')+Sr*Q1*Sr.'];
	end
end
Bn1br = [diag(-1./zLeft(:,end))*(E2*E2.') zeros(n);
	     zeros(n)               diag(1./(1-z0))*(Sr*Sr.')];

% 2. left boundary matrix (probably wont be used)
Bd0br = [eye(n),  -eye(n);
		 zeros(n), zeros(n)];

Bn0br = [zeros(n),    zeros(n)
		 diag(1./zLeft(:,end)), diag(1./(1-z0))];
b_op1br = dps.parabolic.system.boundary_operator('z_b',0,'b_n',Bn0br,'b_d',Bd0br);
b_op2br = dps.parabolic.system.boundary_operator('z_b',1,'b_n',Bn1br,'b_d',Bd1br);

% transform initial condition
if ~isempty(ppide.x0)
	if isa(ppide.x0,'function_handle')
		x0Disc = misc.eval_pointwise(ppide.x0,zdisc);
	else
		x0Disc =  x0;
	end
else
	x0Disc = zeros(ndisc,n);
end

zLeftFull = zeros(n,ndisc);
for i=1:n
	zLeftFull(i,:) = linspace(0,z0(i),ndisc);
end
x0Interp = numeric.interpolant({zdisc,1:n},x0Disc);
x0New = zeros(ndisc,nF);
for i=1:n
	x0New(:,i) = x0Interp.evaluate(fliplr(zLeftFull(i,:)),i);
	x0New(:,i+n) = x0Interp.evaluate(zRight(i,:),i);
end

if obj.debug
	figure('Name','x0 before folding')
	hold on
	for i=1:n
		plot(zdisc,x0Disc(:,i),'displayName',num2str(i));
	end
	legend('-dynamicLegend')
	figure('Name','x0 after folding')
	hold on
	for i=1:nF
		plot(zdisc,x0New(:,i),'displayName',num2str(i));
	end
	legend('-dynamicLegend')
end

ctrlPars = ppide.ctrl_pars;
ctrlPars.mu = mu;
ctrlPars.mu_o = mu_o;
for i=1:nF
	for j=1:nF %                                                 geht so      oder so genau gleich
		ctrlPars.g_strich{i,j} = @(eta) ppide.ctrl_pars.g_strich{1+mod(i+1,n),n-mod(j,n)}(eta);
	end
end

% g_strich wird aktuell einfach yu 0 gesetzt!
designPpide = dps.parabolic.system.ppide_sys(...
				'n',nF,...
				'name','PlantDesign',...
				'l',lambda,... % lambda diff numerically
				'l_diff',lambdaDiff,... % lambda analytically
				'a', a,...
				'b_op1',b_op1br,...
				'b_op2',b_op2br,...
				'b_1',zeros(nF,nF),...  % n Eing�nge auf jeder Seite
				'b_2',eye(nF,nF),... % 
				'ndisc',ppide.ndisc,...
				'ndiscTime',ppide.ndiscTime,...
				'ndiscPars',ppide.ndiscPars,...
				'x0',x0New,...
				'folded',1,...
				'ctrl_pars',ctrlPars);

%




end

