function obj = computeControllerKernel(obj)
	% Compute the kernel for the backstepping transformation of the state feedback controller.
	%   obj = computeControllerKernel(obj)
	if ~obj.controllerKernelFlag
		import dps.parabolic.control.backstepping.*
		fprintf('Computing controller kernel...\n')
		ct = tic;
		% hier alles anpassen!
		% Frage, ob man gleich use_F_ppides Kopie erzeugt f�r folding und dann entsprechendes
		% aufruft, statt in der Funktion noch eine Fallunterscheidung einzubauen. Vermutlich
		% sinnvoller, dann auch von anfang an anst�ndig programmierbar. Gleiches gilt f�r die
		% Initialisierung des Kerns.
		if ~isempty(obj.designPpide)
			if obj.designPpide.n == 2*obj.ppide.n % folding
				use_F_handle = @(ppidek) use_F_ppidesFolding(ppidek,obj.designPpide);
			else % no folding but designPpide exists
				use_F_handle = @(ppidek) use_F_ppides(ppidek,obj.designPpide);
			end
			ppidek = ppide_kernel(obj.designPpide,obj.ppide.ctrl_pars.zdiscCtrl);
		else % normal
			ppidek = ppide_kernel(obj.ppide,obj.ppide.ctrl_pars.zdiscCtrl);
			use_F_handle = @(ppidek) use_F_ppides(ppidek,obj.ppide);
		end
		
		[obj.controllerKernel,~,avgTime] = dps.parabolic.tool.fixpoint_iteration(ppidek,use_F_handle,obj.ppide.ctrl_pars.it_min,obj.ppide.ctrl_pars.it_max,obj.ppide.ctrl_pars.tol);
		obj.controllerKernel.avgTime =avgTime;
		
		if ~isempty(obj.designPpide)
			obj.controllerKernel = obj.controllerKernel.transform_kernel(obj.designPpide);
		else
			obj.controllerKernel = obj.controllerKernel.transform_kernel(obj.ppide);
		end
		% TODO!!
% 		obj.controllerKernel= check_pde(obj.controllerKernel,obj.ppide);
% 		obj.controllerKernel = check_ppde_boundaries(obj.controllerKernel,obj.ppide);
		fprintf('finished controller kernel. (%0.2fs)\n',toc(ct));
		obj.controllerKernelFlag=1;
		obj.controllerFlag = 0; 
		obj.KvFlag = 0;
	end
end