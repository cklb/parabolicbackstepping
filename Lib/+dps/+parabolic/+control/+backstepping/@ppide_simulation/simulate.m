function obj = simulate(obj)
% simulate perform a simulation of a ppide sytem
%   obj = simulate(obj) peforms the simulation with the current settings
%   and stores the result in the appropriate properties.

% created on 05/2018 by Simon Kerschbaum

%% packages and input checks
	import misc.*
	import numeric.*
	import dps.parabolic.system.*
	import dps.parabolic.control.backstepping.*
	import dps.parabolic.control.outputRegulation.*
	
	% Make sure object is initialized
	obj = initializeSimulation(obj);
	obj = obj.deleteSimulationResults({'t', 'tTransformed', ...
		'xE', 'x', 'xTransformed', 'xHat', 'v', 'u', ...
		'uDelayed', 'ym', 'ymDelayed', 'vHat', 'vHatInt', 'wHat'});
	% set default simulation parameters if not set
	if isempty(obj.simPars)
		simPars = misc.simulation_parameters();
	else
		simPars = obj.simPars;
	end

	% Some checks about the initial values
	if any(obj.tv>simPars.tend)
		error('obj.tv contains times greater than simPars.tend!')
	end
	if size(obj.v0, 2)~=obj.sM.nv
		error(['v0 has ' num2str(size(obj.v0, 2)) ' entries, but nv=' num2str(obj.sM.nv) '!'])
	end
	
	if ~isempty(obj.simPpide)
		ppide = obj.simPpide;
		ppide_appr = obj.simPpideApproximation;
	else
		ppide = obj.ppide;
		ppide_appr = obj.ppideApproximation;
	end
	
	simstart = tic;
	fprintf('Starting Simulation...\n');

%% Store parameters in local variables
	n = ppide.n;
	ndiscPlant = ppide.ndisc;
	zdiscPlant = linspace(0, 1, ndiscPlant);
	ndiscKernel = length(obj.controllerKernel.zdisc);
	zdiscKernel = obj.controllerKernel.zdisc;
	ndiscPars = obj.ppide.ndiscPars;
	zdiscPars = linspace(0,1,ndiscPars);
	sM = obj.sM; %#ok<*PROP>
	zdiscObs = obj.ppide.ctrl_pars.zdiscObs;
	ndiscObs = length(zdiscObs);
	if ndiscObs > ndiscPlant
		% otherwise interpolation doesnt work in its current way
		error('Plant approximation resolution must always be greater than observer approximation resolution!')
	end
	
	folding=0;
	observerFolding = 0;
	% number of inputs
	p = size(ppide.b_2,2); 
	no = n;
	if p> n % folding
		folding=1;
		z0 = ppide.ctrl_pars.foldingPoint;
		if isscalar(z0)
			z0 = repmat(z0,1,n);
		end
% 			
% 		r1 = obj.controller.r(1:n,:,:);
% 		r2 = obj.controller.r(n+1:end,:,:);
% 		numtPlant = size(r1,3);
	end
		r = obj.controller.r;
		numtPlant = size(r,3);
% 	end
	
	if numtPlant>1
		% get the interpolant via translate_to_grid
		rInterpPerm = numeric.interpolant({linspace(0,1,numtPlant),1:p,1:n*ndiscPlant},permute(r,[3 1 2]));
	end
	Kv = obj.Kv;
	if isempty(Kv)
		Kv = zeros(p, sM.nv);
	end
	
	ppide_appr_obs = obj.observerApproximation;
% 	if any(ppide.cm_op.c_m(:)) || any(ppide.cm_op.c_dm(:))
% 		% folding = indomain collocated measurement
% 		Cm_op = obj.observerPpide1.cm_op;
% 		Cm_plantSingle = Cm_op.output_matrix(zdiscPlant);
% 		Cm_plant = [Cm_plantSingle, zeros(size(Cm_plantSingle))];
% 	else
% 		Cm_op = ppide.cm_op;
% 	end

	Cm_op = ppide.cm_op;
	% in the folding case, the output is at the left boundary, where both state and derivative
	% are measured.
	Cm_opObs = dps.output_operator(...
		'c_0',[zeros(n,n);eye(n)],...
		'c_d0',[eye(n,n);zeros(n,n)]);
	cOp = ppide.c_op;	
	m =  cOp.m; % # number of outputs
		
	CPlant = cOp.output_matrix(zdiscPlant);
	% in the folding case,
	% y = [x'(z_m);x(z_m)]!
	Cm_plant = Cm_op.output_matrix(zdiscPlant);
	
	
	% Create needed variables:
	A = ppide_appr.A;
	if size(A,3)>1
		AInterpPerm = numeric.interpolant({linspace(0,1,numtPlant),1:n*ndiscPlant,1:n*ndiscPlant},permute(A,[3 1 2]));
	end
	G = ppide_appr.G;
	if folding
		B = ppide_appr.Br+ppide_appr.Bl;
	else
		B = ppide_appr.Br;
	end
	if size(B,3)>1
		BInterpPerm = numeric.interpolant({linspace(0,1,numtPlant),1:n*ndiscPlant,1:p},permute(B,[3 1 2]));
	end
	if size(G,3)>1
		if size(G,2)>0
			GInterpPerm = numeric.interpolant({linspace(0,1,numtPlant),1:n*ndiscPlant,1:size(G,2)},permute(G,[3 1 2]));
		end
	end

	% observer
	if ~isempty(ppide_appr_obs) && obj.useStateObserver && isempty(obj.observerApproximation1)
		% klassischer Fall
		A_obs = ppide_appr_obs.A;
		G_obs = ppide_appr_obs.G;
		B_obs = ppide_appr_obs.Br;
		Bl_obs = ppide_appr_obs.Bl;
		Cm_obs = Cm_op.output_matrix(zdiscObs);
		CObs = cOp.output_matrix(zdiscObs);
		mObs  = Cm_op.m; % # of measurements
		zdiscObsKernel = obj.observerKernel.zdisc;
	elseif obj.useStateObserver && ~isempty(obj.observerApproximation1)
		% folding
		no = 2*n;
		mObs  = 2*Cm_op.m; % # of measurements
		A_obs = zeros(2*n*ndiscObs,2*n*ndiscObs,size(obj.observerApproximation1.A,3));
		A_obs(1:n*ndiscObs,1:n*ndiscObs,:) = obj.observerApproximation1.A;
		A_obs(n*ndiscObs+1:end,n*ndiscObs+1:end,:) = obj.observerApproximation2.A;
		G_obs = zeros(2*n*ndiscObs,2*size(obj.observerApproximation1.G,2),size(obj.observerApproximation1.G,3));
		G_obs(1:n*ndiscObs,1:size(obj.observerApproximation1.G,2),:) = obj.observerApproximation1.G;
		G_obs(n*ndiscObs+1:end,size(obj.observerApproximation1.G,2)+1:end,:) = obj.observerApproximation2.G;
		B_obs = zeros(2*n*ndiscObs,2*size(obj.observerApproximation1.Br,2),size(obj.observerApproximation1.Br,3));
		B_obs(1:n*ndiscObs,1:size(obj.observerApproximation1.Br,2),:) = obj.observerApproximation1.Br;
		B_obs(n*ndiscObs+1:end,size(obj.observerApproximation1.Br,2)+1:end,:) = obj.observerApproximation2.Br;
		Bl_obs = zeros(2*n*ndiscObs,2*size(obj.observerApproximation1.Bl,2),size(obj.observerApproximation1.Bl,3));
		Bl_obs(1:n*ndiscObs,1:size(obj.observerApproximation1.Bl,2),:) = obj.observerApproximation1.Bl;
		Bl_obs(n*ndiscObs+1:end,size(obj.observerApproximation1.Bl,2)+1:end,:) = obj.observerApproximation2.Bl;
		Cm_obsSingle = Cm_opObs.output_matrix(zdiscObs);
		Cm_obs = blkdiag(Cm_obsSingle,Cm_obsSingle); % each observer has its output matrix
		CObs = cOp.output_matrix(zdiscObs);
		zdiscObsKernel = obj.observerKernel1.zdisc;
	else % no observer
		mObs  = Cm_op.m; % # of measurements
		A_obs = zeros(n*ndiscObs);
		G_obs = zeros(n*ndiscObs, size(G, 2));
		B_obs = zeros(n*ndiscObs, size(B, 2));
		Bl_obs = zeros(n*ndiscObs, size(ppide_appr.Bl, 2));
		Cm_obs = Cm_op.output_matrix(zdiscObs);
		CObs = cOp.output_matrix(zdiscObs);
		zdiscObsKernel = [];
	end
	if (any(obj.ppide.cm_op.c_m(:))  || any(obj.ppide.cm_op.c_dm(:))) && obj.useStateObserver % folding 
		observerFolding = 1;
		zm = obj.ppide.cm_op.z_m;
		% measurement from first and second state is equal so it is suffiient to evaluate first
		% state. Beacuse of neumann measurement used for observer injection, needs correction
		% in sign and magnitude due to folding point.
		% passt nicht, erster Beobachter braucht Ableitung von erstem Zustand, zweiter
		% Beobacher braucht Ableitung von seinen Zustšnden.  Kann zwar ineinander umgerechnet
		% werden, aber besser gleich die richtgen Zustšnde nehmen!
		% observer is doubled but values at left border are the same, so sufficient to feed
		% back states of first observer.
		% CObs is not implemented, this is just for appropriate dimensions!
		CObs = [CObs, zeros(size(CObs))];
	end
	
	if size(A_obs,3)>1
		A_obsInterpPerm = numeric.interpolant({linspace(0,1,numtPlant),1:no*ndiscObs,1:no*ndiscObs},permute(A_obs,[3 1 2]));
	end
	if size(B_obs,3)>1
		B_obsInterpPerm = numeric.interpolant({linspace(0,1,numtPlant),1:no*ndiscObs,1:size(B_obs, 2)},permute(B_obs,[3 1 2]));
	end
	if size(Bl_obs,3)>1
		Bl_obsInterpPerm = numeric.interpolant({linspace(0,1,numtPlant),1:no*ndiscObs,1:size(ppide_appr.Bl, 2)},permute(Bl_obs,[3 1 2]));
	end
	if size(G_obs,3)>1
		if size(G_obs,2)>0 % no disturbance
			G_obsInterpPerm = numeric.interpolant({linspace(0,1,numtPlant),1:no*ndiscObs,1:size(G_obs,2)},permute(G_obs,[3 1 2]));
		end
	end
	% internal model
	uIM = obj.useIntMod;
	if uIM
		KvHat = obj.KvHat;
		By = obj.ppide.ctrl_pars.By;
		for i=1:m
			STil((i-1)*sM.nv+1:i*sM.nv, (i-1)*sM.nv+1:i*sM.nv) = sM.S;
		end
	else
		KvQPlant = 0; %#ok<NASGU>
		KvHat = zeros(p, 0);
		By = zeros(0, m);
		STil = zeros(0, 0); % empty 0x0 matrix
	end

	numtL = 1;
	
	if ~isempty(obj.observer) && obj.useStateObserver
		numtL = size(obj.observer.L,4);
		L = obj.observer.L;
		L0 = obj.observer.L1;
		L0Obs = L0;
		Ld = obj.observer.Ld;
		LdPlant = Ld;
		Lr = obj.observer.Lr;
		% Transform L(z) in system represenatation: 
		L_obs = reshape(L, [n*ndiscObs, n, numtL]);
		L_obsPlant = L_obs;
	elseif ~isempty(obj.observer1) && obj.useStateObserver 
		% folding = collocated in-domain measurement and two single observers
		% preallocate
		numtL = size(obj.observer1.L,4);
		L = zeros(ndiscObs, no, mObs,numtL);
		LPlant = zeros(ndiscObs, no, 2*n,numtL);
		% injection at left boundary: w_hat(0) = w(0). corresponds to second measurement
		L0 = zeros(p, 2*n);
		L0(:,n+1:end) = [eye(n);eye(n)];
		% In the folding case, the left boundary incection is 
		% w_hat(0) = w(0). That means the injection of the observer itself is zero.
		L0Obs = zeros(p,mObs);
		Ld = zeros(sM.nd, mObs);
		LdPlant = zeros(sM.nd,2*n);
		Lr = zeros(sM.nr, size(sM.pr,1));
		% Transform L(z) in system represenatation: 
		% for L(z), only w'(z) is fed back. (1:n in 3rd dim)
		L(:,1:n,1:n,:) = obj.observer1.L;
		L(:,n+1:end,2*n+1:3*n,:) = obj.observer2.L;
		L_obs = reshape(L, [no*ndiscObs, mObs, numtL]);	
		% corrected L to account for w'(0) ~= x'(zm)
		LPlant(:,1:n,1:n,:) = obj.observer1.L*(-zm);
		LPlant(:,n+1:end,1:n,:) = obj.observer2.L*(1-zm);
		L_obsPlant = reshape(LPlant, [no*ndiscObs, 2*n, numtL]);	
	else
		L = zeros(ndiscObs, n, mObs);
		L0 = zeros(p, mObs);
		L0Obs = L0;
		Ld = zeros(sM.nd, mObs);
		LdPlant = Ld;
		Lr = zeros(sM.nr, size(sM.pr,1));
		% Transform L(z) in system represenatation: 
		L_obs = reshape(L, [n*ndiscObs, mObs, numtL]);
		L_obsPlant = L_obs;
	end
	if numtL>1
		% get the interpolant via translate_to_grid
		L_obsInterpPerm = numeric.interpolant({linspace(0,1,numtL),1:size(L_obs,1),1:size(L_obs,2)},permute(L_obs,[3 1 2]));
		L_obsInterpPermPlant = numeric.interpolant({linspace(0,1,numtL),1:size(L_obsPlant,1),1:size(L_obsPlant,2)},permute(L_obsPlant,[3 1 2]));
		L0InterpPerm = numeric.interpolant({linspace(0,1,numtL),1:size(L0,1),1:size(L0,2)},permute(L0,[3 1 2]));
		L0ObsInterpPerm = numeric.interpolant({linspace(0,1,numtL),1:size(L0Obs,1),1:size(L0Obs,2)},permute(L0Obs,[3 1 2]));
	end
	if size(A,3)>1
		Afun = @(t) reshape(AInterpPerm.evaluate(t,1:n*ndiscPlant,1:n*ndiscPlant),[n*ndiscPlant n*ndiscPlant]);
	else
		Afun = @(t) A;
	end
	if size(B,3)>1
		Bfun = @(t) reshape(BInterpPerm.evaluate(t,1:n*ndiscPlant,1:p),[n*ndiscPlant p]);
	else
		Bfun = @(t) B;
	end
	if size(A_obs,3)>1
		A_obsfun = @(t) reshape(A_obsInterpPerm.evaluate(t,1:size(A_obs,1),1:size(A_obs,2)),[size(A_obs,1) size(A_obs,2)]);
	else
		A_obsfun = @(t) A_obs;
	end
	if size(B_obs,3)>1
		B_obsfun = @(t) reshape(B_obsInterpPerm.evaluate(t,1:size(B_obs,1),1:size(B_obs,2)),[size(B_obs,1) size(B_obs,2)]);
	else
		B_obsfun = @(t) B_obs;
	end
	if size(G,3)>1 
		if size(G,2)>0
			G_fun = @(t) reshape(G_InterpPerm.evaluate(t,1:n*ndiscPlant,1:size(G,2)),[n*ndiscPlant size(G,2)]);
		else
			G_fun = @(t) zeros(n*ndiscPlant,0);
		end
	else
		G_fun = @(t) G;
	end
	if size(G_obs,3)>1 
		if size(G_obs,2)>0
			G_obsfun = @(t) reshape(G_obsInterpPerm.evaluate(t,1:size(G_obs,1),1:size(G_obs,2)),[size(G_obs,1) size(G_obs,2)]);
		else
			G_obsfun = @(t) zeros(size(G_obs,1),0);
		end
	else
		G_obsfun = @(t) G_obs;
	end
	if size(L0,3)>1
		L0_fun = @(t) reshape(L0InterpPerm.evaluate(t,1:size(L0,1),1:size(L0,2)),[size(L0,1) size(L0,2)]);
		L0_fun_obs = @(t) reshape(L0ObsInterpPerm.evaluate(t,1:size(L0Obs,1),1:size(L0Obs,2)),[size(L0Obs,1) size(L0Obs,2)]);
	else
		L0_fun = @(t) L0;
		L0_fun_obs = @(t) L0Obs;
	end
	if size(Bl_obs,3)>1
		Bl_obsfun = @(t) reshape(Bl_obsInterpPerm.evaluate(t,1:size(Bl_obs,1),1:size(Bl_obs,2)),[size(Bl_obs,1) size(Bl_obs,2)]);
	else
		Bl_obsfun = @(t) Bl_obs;
	end
	if size(L_obs,3)>1
		L_obsfun = @(t) reshape(L_obsInterpPerm.evaluate(t,1:size(L_obs,1),1:size(L_obs,2)),[size(L_obs,1) size(L_obs,2)]);
		L_obsfunPlant = @(t) reshape(L_obsInterpPermPlant.evaluate(t,1:size(L_obsPlant,1),1:size(L_obsPlant,2)),[size(L_obsPlant,1) size(L_obsPlant,2)]);
	else
		L_obsfun = @(t) L_obs;
		L_obsfunPlant = @(t) L_obsPlant;
	end
	
	% delay: by default no delay
	inputDelay = zeros(obj.ppide.n, 1);
	outputDelay = zeros(obj.ppide.n, 1);
	useInputDelaySimulation = false;
	useOutputDelaySimulation = false;
	if isa(obj.ppide, 'dps.parabolic.system.Delay')
		% ppide is a delay-system -> consider delays
		inputDelay = obj.ppide.inputDelay;
		outputDelay = obj.ppide.outputDelay;
		useInputDelaySimulation = logical(any(abs(inputDelay(:))));
		useOutputDelaySimulation = logical(any(abs(outputDelay(:))));
	end

	% Store user switches:
	uRO = obj.useReferenceObserver;
	uDO = obj.useDisturbanceObserver;
	uSO = obj.useStateObserver;
	aSO = obj.useObserverInFeedback;
	uODS = useOutputDelaySimulation;
	if any([uRO uDO]) && isempty({Ld Lr})
		error('Trying to use refrence/disturbance observer without having calculated them.')
	end
	uOR = obj.useOutputRegulation;
	uSFB = obj.useStateFeedback;
	if uOR && ~uSFB
		warning('Using output regulation without state feedback. This might not work!')
	end
	% interpolate observer states to controller resolution by using a
	% matrix
	if uSO && aSO
		if any(obj.ppide.cm_op.c_m(:))  || any(obj.ppide.cm_op.c_dm(:)) % folding 
			zmIdx = find(zdiscPlant<zm,1,'last');
			% the actual state must be calculated via unfolding from the folded observer
			% states:
			% x(z,t) = 
			%    w((zm-z)/zm) for z<zm states of the first observer
			%    w((z-zm)/(1-zm)) for z>= zm  states of the second observer
			% expressed as matrix multiplication with discretized x and w:
			% x = IOPTot*w
			% Interpolation matrix to regard for different resolutions:
			% calling x the plant state and xl/xr the states of the 2 observers.
			% In the plant (where to go) it is:
			% x_1(0..zm..1)
			% x_2(0..zm..1)
			% ....
			% corresponding to 
			% xl_1(0..1)xr_1(0..1)
			% xl_2(0..1)xr_2(0..1)
			% ...
			% But the observer states are ordered as
			% xl_1(0..1)
			% xl_2(0..1)
			% ...
			% xr_1(0..1)
			% xr_2(0..1)
			% Therefore the corresponding columns need to be swapped.
			
			% Due to the measurement the folding point is available as boundary point of both
			% obervers and needs to be ignored from one observer. The left point of xl needs to
			% be ignored, since zl ist the last point before the folding point
			% x_i(0..zm) = IOP1*xl_i(0..1)
			warning('this should be improved! Like for the unfolding Function!')
% 			IOP1 = flipud(misc.interp_mat(zdiscObs, linspace(0,1,zmIdx+1))); % from observer to plant
			IOP1 = flipud(misc.interp_mat(zdiscObs, linspace(0,1,zmIdx))); % from observer to plant
			% this interpolation matrix changes the number of points from zdiscObs(0,1) to the 
			% right part of z but uses one point more there to be able to ignore the last one.
% 			IOP1 = IOP1(1:end-1,:);
			% x_i(zm..1) = IOP2*xr_i(0..1)
			IOP2 = interp_mat(zdiscObs, linspace(0,1,ndiscPlant-zmIdx)); % from observer to plant

			IOPTot = zeros(n*ndiscPlant,2*n*ndiscObs);
			nL = size(IOP1,1);
			nR = size(IOP2,1);
			nP = ndiscPlant;
			nO = ndiscObs;
			for i=1:n
				IOPTot((i-1)*nP+1:(i-1)*nP+nL,(i-1)*nO+1:i*nO) = IOP1;
				IOPTot(i*nL+(i-1)*nR+1:i*nP,n*nO+(i-1)*nO+1:n*nO+(i-1)*nO+nO) = IOP2;
			end
		else
			IOP = interp_mat(zdiscObs, zdiscPlant); % from observer to plant
			% add to diagonal matrix to interpolate each state. Using kron is
			% faster than doing it manually for large n.
			IOPTot = kron(eye(n), IOP);
		end
	else % no state observer
		IOPTot = eye(n*ndiscPlant);
	end % if
	% Store resolution of the fed back states depending on the usage of the
	% observer
	if uSO && aSO
		ndiscO = ndiscObs;
	else
		ndiscO = ndiscPlant;
	end
	if uIM
		KvQPlant = obj.KvQPlant;
	else
		KvQPlant = zeros(p, n*ndiscPlant);
	end
	
	% Create matrices that select some states out of the extended
	% simulation vector (depending on settings):
	nvHatInt = uIM*m*sM.nv;
	selectXHat = [(1-(uSO&&aSO))*eye(n*ndiscPlant,no*ndiscO); zeros(sM.nv,no*ndiscO);[(uSO&&aSO)*eye(no*ndiscObs) zeros(no*ndiscObs,no*ndiscO-no*ndiscObs)];zeros(sM.nd,no*ndiscO);zeros(sM.nr,no*ndiscO);zeros(nvHatInt,no*ndiscO)]; % selection matrix of reconstructed states
	selectVHat = [zeros(n*ndiscPlant,sM.nv); blkdiag((1-uDO)*eye(sM.nd,sM.nd),(1-uRO)*eye(sM.nr,sM.nr)) ; zeros(no*ndiscObs,sM.nv) ; blkdiag(uDO*eye(sM.nd),uRO*eye(sM.nr,sM.nr));zeros(nvHatInt,sM.nv)]; % selection matrix of reference states and reconstructed disturbance states
	selectVHatInt = [zeros(n*ndiscPlant,nvHatInt); zeros(sM.nv,nvHatInt) ; zeros(no*ndiscObs,nvHatInt) ; zeros(sM.nv,nvHatInt);eye(nvHatInt)]; % selection matrix of internal model states
	
	% Control law:
	if numtPlant>1
		u  = @(t,x_e) uSFB*reshape(rInterpPerm.evaluate(t,1:p,1:n*ndiscPlant),[p n*ndiscPlant])*IOPTot*selectXHat.'*x_e - uOR*Kv*selectVHat.'*x_e...
		          - uIM*KvHat*selectVHatInt.'*x_e + uIM*KvQPlant*IOPTot*selectXHat.'*x_e;
	else
		u  = @(t,x_e) uSFB*r*IOPTot*selectXHat.'*x_e - uOR*Kv*selectVHat.'*x_e...
		          - uIM*KvHat*selectVHatInt.'*x_e + uIM*KvQPlant*IOPTot*selectXHat.'*x_e;
	end
	
	% create zeros in appropriate sizes:
	nvnp0 = zeros(sM.nv, n*ndiscPlant);
	nvHnp0 = zeros(nvHatInt, n*ndiscPlant); %#ok<NASGU>
	ndnv0 = zeros(size(sM.Sd, 1), size(sM.S, 2));
	npno0 = zeros(size(A, 1), size(A_obs, 2));
	nvno0 = zeros(sM.nv, size(A_obs, 2));
	nvHno0 = zeros(nvHatInt, size(A_obs, 2));
	npnd0 = zeros(size(A, 1), size(sM.Sd, 2));
	nvnd0 = zeros(sM.nv, size(sM.Sd, 2));
	nvHnd0 = zeros(nvHatInt, size(sM.Sd, 2));
	nrnp0 = zeros(sM.nr, n*ndiscPlant);
	nrnv0 = zeros(sM.nr, sM.nv);
	nrno0 = zeros(sM.nr, no*ndiscObs);
	nrnd0 = zeros(sM.nr, sM.nd);
	nvHnv0 = zeros(nvHatInt, sM.nv);
	nvHnr0 = zeros(nvHatInt, sM.nr);
	npnvH0  = zeros(size(A, 1), nvHatInt);
	
	if isempty(obj.referenceSignal)
		refSig = @(t) zeros(m, 1);
	else
		refSig = obj.referenceSignal;
	end

	% System matrix of simulation:
	% The extended state consists of:
	% Plant, Signal model, State observer, Disturbance observer, Reference
	% observer, internal model
	A_f = @(t) [Afun(t),													     G_fun(t)*sM.pd,				npno0,																	    npnd0,							nrnp0.',						npnvH0;... % Plant
				nvnp0,														     sM.S,							nvno0,																	    nvnd0,							nrnv0.',						nvHnv0.';... % Signal model
				(1-uODS)*uSO*(L_obsfunPlant(t)+Bl_obsfun(t)*L0_fun(t))*Cm_plant, uSO*(1-uDO)*G_obsfun(t)*sM.pd,	uSO*(A_obsfun(t)-(1-uODS)*(L_obsfun(t)+Bl_obsfun(t)*L0_fun_obs(t))*Cm_obs),	uSO*uDO*G_obsfun(t)*sM.pd_til,	nrno0.',						nvHno0.';... % State observer
				(1-uODS)*uDO*LdPlant*Cm_plant,							  	     ndnv0,							-uDO*Ld*Cm_obs,															    sM.Sd,							nrnd0.',						nvHnd0.';... % Disturbance observer
				nrnp0,															 uRO*Lr*sM.pr,					nrno0,																	    nrnd0,							sM.Sr-uRO*Lr*obj.sM.pr_til,		nvHnr0.';... % Reference observer
				(1-(uSO&&aSO))*By*CPlant										 nvHnv0,						(uSO&&aSO)*By*CObs,														    nvHnd0,							nvHnr0,							STil]; % internal model
	B_f = @(t) [Bfun(t); zeros(sM.nv, p); uSO*B_obsfun(t); zeros(sM.nv, p); zeros(nvHatInt, p)];
	B_f_refSig = [zeros(n*ndiscPlant, m);zeros(sM.nv, m);zeros(no*ndiscObs, m);zeros(sM.nv, m);-By];
	if numtPlant>1
		kT = @(t) uSFB*reshape(rInterpPerm.evaluate(t,1:p,1:n*ndiscPlant),[p n*ndiscPlant])*IOPTot*selectXHat.' - uOR*Kv*selectVHat.'...
			- uIM*KvHat*selectVHatInt.' + uIM*KvQPlant*IOPTot*selectXHat.';
	else
		kT = @(t) uSFB*r*IOPTot*selectXHat.' - uOR*Kv*selectVHat.' ...
			- uIM*KvHat*selectVHatInt.' + uIM*KvQPlant*IOPTot*selectXHat.';
	end
		
	% TODO: referenceSignal auch bei Fuehrungs- und
	% Stoergroessenaufschaltung verwenden!
%% Transform initial value to correct representation
	if ~isempty(obj.x0)
		if isa(obj.x0, 'function_handle')
			x0_disc_temp = eval_pointwise(obj.x0, zdiscPlant);
			x0_disc = x0_disc_temp(:);
			if length(x0_disc)~=n*ndiscPlant
				error('Size of x0 did not fit!')
			end
		else
			x0_disc = obj.x0;
			if size(x0_disc, 1)~= n*ndiscPlant 
				error(['size(x0_disc, 1)=' num2str(size(x0_disc, 1)) ' ~= n*ndisc_plant=' num2str(n*ndiscPlant)])
			elseif size(x0_disc, 2)~= 1
				error('size(x0) must be [n*ndisc_plant, 1], or x0 must be an anonymous function.');
			end
		end
	else % if no initial value is set, it is assumed as zero.
		x0_disc = zeros(n*ndiscPlant, 1);
	end
	% Get initial value of observer in correct representation
	if ~isempty(obj.xHat0)
		if isa(obj.xHat0, 'function_handle')
			x0_hat_disc_temp = eval_pointwise(obj.xHat0, zdiscObs);
			x0_hat_disc = x0_hat_disc_temp(:);
		else
			x0_hat_disc = obj.xHat0;
			if size(x0_hat_disc, 1)~= n*ndiscObs 
				error(['size(x0_hat_disc, 1)=' num2str(size(x0_hat_disc, 1)) ' ~= n*ndisc_obs=' num2str(n*ndiscObs)])
			elseif size(x0_hat_disc, 2)~= 1
				error('size(xHat0) must be [n*ndisc_obs, 1], or xHat0 must be an anonymous function.');
			end
		end
	else % if no initial value is set, it is assumed as zero.
		x0_hat_disc = zeros(no*ndiscObs, 1);
	end
	% Is no initial value of the signal model is set, it is assumed as zero
	if isempty(obj.v0)
		v0Temp = zeros(sM.nv, 1);
	else
		% Check if v0 has appropriate dimensions:
		if size(obj.v0, 2)~= sM.nv
			error(['The initial value of the signal model (v0) must be an'...
			' array containing row vectors at the different timestamps tv'....
			' So size(v0, 2) mus equal sM.nv.'])
		end
		v0Temp = obj.v0(1, :).';
	end
	x0 = [x0_disc;v0Temp;x0_hat_disc;obj.vHat0;zeros(nvHatInt, 1)];
	x0Sim = x0;
	
%% System dynamics
% matrices used only for delay-simulations: 
	if useInputDelaySimulation || useOutputDelaySimulation
		CmNoDelay = zeros(n, size(A_f(0), 1)); CmNoDelay(1:n, 1:(ndiscPlant*n)) = Cm_plant;
		% Set time discretization for simulation
		timeData = misc.Time('stepSize', simPars.ts, ...
			'start', 0, 'duration', simPars.tend);
		ti = timeData.array;
		yi = zeros(timeData.numberOfTimeSteps, numel(x0Sim));
		yi(1, :) = x0Sim;
		if useInputDelaySimulation
			obj.uDelayed = zeros(timeData.numberOfTimeSteps, n);
			obj.u = zeros(timeData.numberOfTimeSteps, n);
			vTempTimeFrameSync = linspace(-max(inputDelay(:)), 0, round(max(inputDelay(:))/simPars.ts));
			vTempTimeFrameSyncIdx = -round(max(inputDelay(:))/simPars.ts):1:-1;
			selectPlantInput = flipud(interp_mat(vTempTimeFrameSync, flipud(-inputDelay)));
								% double flip because interp_mat requires
								% ascending inputs
								
			if isa(obj.controller, 'dps.parabolic.control.backstepping.ControllerForInputDelay')
				feedbackInputDelayedSync = obj.controller.getFeedbackInputDelayed(ppide, simPars.ts);
				feedbackState = obj.controller.feedbackState;
				obj.u(2, :) = - uSFB * feedbackState*IOPTot * selectXHat.' * x0Sim;
			end
			f = @(t, x_e, uExternal) A_f(t) * x_e + B_f(t) * uExternal(t);
		end	
		if useOutputDelaySimulation
			obj.ymDelayed = zeros(timeData.numberOfTimeSteps, n);
			obj.ym = zeros(timeData.numberOfTimeSteps, n);
			wTempTimeFrameSync = linspace(-max(outputDelay(:)), 0, 1+round(max(outputDelay(:))/simPars.ts));
			wTempTimeFrameSyncIdx = -round(max(outputDelay(:))/simPars.ts):1:0;
			selectPlantOutput = flipud(interp_mat(wTempTimeFrameSync, flipud(-outputDelay)));
								% double flip because interp_mat requires
								% ascending inputs
								
			% LExternal is basicly the first column of A_f but only 
			% considering the measurement (terms ending with Cm_plant)
			LExternal = [zeros(size([A; nvnp0], 1), n); ... % plant and signal model
							uSO*(L_obs+Bl_obs*L0); ...		% observer simulation dynamics
							uDO*Ld; ...						% observer disturbance model
							zeros(size([nrnp0; 0*By*CPlant], 1), n)]; % internal model + reference
	
			if isa(obj.observer, 'dps.parabolic.control.backstepping.ObserverForOutputDelay')
				hyperbolicObserverSubsystem = ...
					obj.observer.hyperbolicSubsystem.finite_difference_approximation(...
					'dt', simPars.ts);
				dummy.d = zeros(hyperbolicObserverSubsystem.q, 1);
				dummy.f = zeros(hyperbolicObserverSubsystem.r, 1);
				dummy.wHat = zeros(timeData.numberOfTimeSteps, sum([hyperbolicObserverSubsystem.approx.cfl.ndisc{:}]));
				dummy.wObsz0 = zeros(timeData.numberOfTimeSteps, n);
				dummy.u = zeros(n, 1);
				dummy.ymHat = zeros(timeData.numberOfTimeSteps, n);
			end
			selectYmHat = Cm_obs * selectXHat.';
			f = @(t, x_e, ymExternal) (A_f(t) + B_f(t) * kT(t)) * x_e + LExternal * ymExternal(t);
		end
		if useInputDelaySimulation && useOutputDelaySimulation
			f = @(t, x_e, uExternal, ymExternal) A_f(t) * x_e + B_f(t) * uExternal(t) + LExternal * ymExternal(t);
		end
	else % Default case: No delay
		f = @(t, x_e) (A_f(t) + B_f(t) * kT(t)) * x_e + B_f_refSig * refSig(t);
	end

%% Solver options
	odeopts = odeset('relTol', simPars.frel, 'absTol', simPars.fabs, 'MaxStep', simPars.tsmax);
	t = [];
	y = [];
	
%% Run simulations
	for block_idx = 1:length(obj.tv)
		% Simulate each timestep between the values of tv
		t_start = obj.tv(block_idx);
		if block_idx == length(obj.tv)
			tEnd = simPars.tend;
		else
			tEnd = obj.tv(block_idx+1);
		end
		
		% The following if-conditions are implemented, since the simulation
		% considering input or output delays costs a lot of computational
		% effort. Because the delay-systems are implemented as fixed-step
		% simulation, since simulations of hyperbolic systems, i.e., for 
		% instance the delay sub-system in the state observer, work best for
		% fixed-step solvers. Still, between that timesteps, the parabolic
		% system is simulated with an variable-step solver in order to
		% avoid further implementation work. To sum up, the mix of
		% fixed-step and variable-step costs a lot of resources, but yields
		% satisfactory results. 
		% Still, not in every simulation input delays and, at the same
		% time, output delays are present. Furthermore, there are differend
		% controller dynamics for controllers considering output delay and
		% those which do not. Therefore, there are different simulation
		% implementations for the cases:
		% 1. input and output delays are present
		%	a) controller considering delays
		%	b) feedback considers delay, observer does not consider delay
		%	c) observer considers delay, feedback does not.
		%	d) controller and observer do not consider delay
		%	e) open loop
		% 2. input delays are present (no output delay)
		%	a) feedback considers delay
		%	b) feedback does not consider delay
		% 3. output delays are present (no input delay)
		%	a) observer considers delay
		%	b) observer does not consider delay
		% 4. No delays (default case, by far best performance)
		%
		% To avoid redundand comments, only case 1.) input and output
		% delays are commented.
		if useInputDelaySimulation && useOutputDelaySimulation
			%% If-conditions choose different solver if input and/or output 
			% delays are considered or not.
			if obj.useStateFeedbackForInputDelay && obj.useStateObserverForOutputDelay
			%% Simulation with input and output delay and controller and observer for delays
				timeData.reset();
 				while(~timeData.isOver)
					tt = double(timeData.nowIdx);
					% implementation of delays: 
					% vTempTimeFrameSyncIdx and wTempTimeFrameSyncIdx
					% select the input-delay states (v) and the
					% output-delay states from the past input and output
					% signals. diag(selectPlantInput (.)) and
					% diag(selectPlantOutput (.)) select the delayed
					% signals from v and w afterwards.
					vTempSync = reshape(obj.u(max(1, 1+tt+vTempTimeFrameSyncIdx), :), [], 1);
					obj.uDelayed(tt, :) = diag(selectPlantInput * obj.u(max(1, 1+tt+vTempTimeFrameSyncIdx), :));
					obj.ymDelayed(tt, :) = diag(selectPlantOutput * obj.ym(max(1, tt+wTempTimeFrameSyncIdx), :));
					
					% Simulation of the hyperbolic sub-systems observer
					% dynamics using delayed input and delayed measurement
					[dummy.wHat(tt, :), ~, dummy.wObsz0(tt, :)] = ...
						hyperbolicObserverSubsystem.sim_one_timestep(...
							dummy.wHat(tt-1, :).',	...		% state in last timestep
							dummy.d, ...					% disturbance
							[dummy.ymHat(tt-1, :).'; ...	% boundary input x_z^hat(0, t)
							obj.ymDelayed(tt, :).'], ...	% distributed input y(t)
							dummy.f, timeData);				% fault, time
						
					% Simulation of plant and parabolic observer sub-system
					% using delayed input and delayed measurement
					[~, y_temp] = ode15s(f, [timeData.atIdxRelative(-1), timeData.now], yi(tt-1, :), odeopts, ...
						@(t) (t-timeData.atIdxRelative(-1))/simPars.ts * obj.uDelayed(tt, :).'...
								+ (timeData.now-t)/simPars.ts * obj.uDelayed(tt-1, :).', ...
						@(t) (1+0*t)*(obj.ymDelayed(tt, :) - dummy.wObsz0(tt, :)).');
					% since y_temp contains many timesteps in ti(tt-1:tt),
					% only the last one is saved as state with the fixed
					% step discretization as the input, measurement and the
					% hyperbolic system.
					yi(tt, :) = y_temp(end, :);
					
					% get measurements from recent states
					obj.ym(tt, :) = CmNoDelay * yi(tt, :).';
					dummy.ymHat(tt, :) = selectYmHat * yi(tt, :).';
					
					% evaluation of feedback law
					obj.u(tt+1, :) = - uSFB * feedbackState*IOPTot * selectXHat.' * yi(tt, :).' ...
									- feedbackInputDelayedSync * vTempSync;
					
					% Iterate to next timestep
					timeData.nextStep();
				end
				% The states of the hyperbolic observer subsystem are
				% translated to a uniform grid for every state:
				wHatCell = hyperbolicObserverSubsystem.stateVecToCell(dummy.wHat);
				obj.wHat = zeros(timeData.numberOfTimeSteps, ndiscPlant, n);
				for it = 1:n
					obj.wHat(:, :, it) = misc.translate_to_grid(wHatCell{it}.', linspace(0, 1, ndiscPlant)).';
				end
				
			elseif obj.useStateFeedbackForInputDelay
			%% Simulation with input and output delay and controller for input delay but usual observer
			% Comments see general case above: 
			%	"Simulation with input and output delay and controller and observer for delays"
				timeData.reset();
 				while(~timeData.isOver)
					tt = double(timeData.nowIdx);
					obj.uDelayed(tt, :) = diag(selectPlantInput * obj.u(max(1, 1+tt+vTempTimeFrameSyncIdx), :));
					obj.ymDelayed(tt, :) = diag(selectPlantOutput * obj.ym(max(1, tt+wTempTimeFrameSyncIdx), :));
					[~, y_temp] = ode15s(f, [ti(tt-1) ti(tt)], yi(tt-1, :), odeopts, ...
						@(t) (t-ti(tt-1))/simPars.ts * obj.uDelayed(tt, :).'...
								+ (ti(tt)-t)/simPars.ts * obj.uDelayed(tt-1, :).', ...
						@(t) (t-ti(tt-1))/simPars.ts * obj.ymDelayed(tt, :).'...
								+ (ti(tt)-t)/simPars.ts * obj.ymDelayed(tt-1, :).');
					yi(tt, :) = y_temp(end, :);
					vTempSync = reshape(obj.u(max(1, 1+tt+vTempTimeFrameSyncIdx), :), [], 1);
					obj.u(tt+1, :) = - uSFB * feedbackState*IOPTot * selectXHat.' * yi(tt, :).' ...
									- feedbackInputDelayedSync * vTempSync;
					obj.ym(tt, :) = CmNoDelay * yi(tt, :).';
					timeData.nextStep();
				end
				
			elseif obj.useStateObserverForOutputDelay
			%% Simulation with input and output delay and observer for output delays but usual controller
			% Comments see general case above: 
			%	"Simulation with input and output delay and controller and observer for delays"
				timeData.reset();
 				while(~timeData.isOver)
					tt = double(timeData.nowIdx);
					obj.uDelayed(tt, :) = diag(selectPlantInput * obj.u(max(1, tt+vTempTimeFrameSyncIdx), :));
					obj.ymDelayed(tt, :) = diag(selectPlantOutput * obj.ym(max(1, tt+wTempTimeFrameSyncIdx), :));
					[dummy.wHat(tt, :), ~, dummy.wObsz0(tt, :)] = ...
						hyperbolicObserverSubsystem.sim_one_timestep(...
							dummy.wHat(tt-1, :).',	...		% state in last timestep
							dummy.d, ...					% disturbance
							[dummy.ymHat(tt-1, :).'; ...	% boundary input x_z^hat(0, t)
							obj.ymDelayed(tt, :).'], ...	% distributed input y(t)
							dummy.f, timeData);				% fault, time
					[~, y_temp] = ode15s(f, [ti(tt-1) ti(tt)], yi(tt-1, :), odeopts, ...
						@(t) (t-ti(tt-1))/simPars.ts * obj.uDelayed(tt, :).'...
								+ (ti(tt)-t)/simPars.ts * obj.uDelayed(tt-1, :).', ...
						@(t) (1+0*t)*(obj.ymDelayed(tt, :) - dummy.wObsz0(tt, :)).');
					yi(tt, :) = y_temp(end, :);
					obj.u(tt, :) = kT * yi(tt, :).';
					obj.ym(tt, :) = CmNoDelay * yi(tt, :).';
					dummy.ymHat(tt, :) = selectYmHat * yi(tt, :).';
					timeData.nextStep();
				end
				wHatCell = hyperbolicObserverSubsystem.stateVecToCell(dummy.wHat);
				obj.wHat = zeros(timeData.numberOfTimeSteps, ndiscPlant, n);
				for it = 1:n
					obj.wHat(:, :, it) = misc.translate_to_grid(wHatCell{it}.', linspace(0, 1, ndiscPlant)).';
				end
			else
			%% Simulation with input delay and output delay but usual controller and observer
			% Comments see general case above: 
			%	"Simulation with input and output delay and controller and observer for delays"
				timeData.reset();
 				while(~timeData.isOver)
					tt = double(timeData.nowIdx);
					obj.uDelayed(tt, :) = diag(selectPlantInput * obj.u(max(1, tt+vTempTimeFrameSyncIdx), :));
					obj.ymDelayed(tt, :) = diag(selectPlantOutput * obj.ym(max(1, tt+wTempTimeFrameSyncIdx), :));
					[~, y_temp] = ode15s(f, [ti(tt-1) ti(tt)], yi(tt-1, :), odeopts, ...
						@(t) (t-ti(tt-1))/simPars.ts * obj.uDelayed(tt, :).'...
								+ (ti(tt)-t)/simPars.ts * obj.uDelayed(tt-1, :).', ...
						@(t) (t-ti(tt-1))/simPars.ts * obj.ymDelayed(tt, :).'...
								+ (ti(tt)-t)/simPars.ts * obj.ymDelayed(tt-1, :).');
					yi(tt, :) = y_temp(end, :);
					obj.u(tt, :) = kT * yi(tt, :).';
					obj.ym(tt, :) = CmNoDelay * yi(tt, :).';
					timeData.nextStep();
				end
			end

		elseif useInputDelaySimulation 
			if obj.useStateFeedbackForInputDelay
			%% Simulation with input delay and controller for input delay
			% Comments see general case above: 
			%	"Simulation with input and output delay and controller and observer for delays"
				timeData.reset();
 				while(~timeData.isOver)
					tt = double(timeData.nowIdx);
					obj.uDelayed(tt, :) = diag(selectPlantInput * obj.u(max(1, 1+tt+vTempTimeFrameSyncIdx), :));
					[~, y_temp] = ode15s(f, [ti(tt-1) ti(tt)], yi(tt-1, :), odeopts, ...
						@(t) (t-ti(tt-1))/simPars.ts * obj.uDelayed(tt, :).'...
								+ (ti(tt)-t)/simPars.ts * obj.uDelayed(tt-1, :).');
					yi(tt, :) = y_temp(end, :);
					vTempSync = reshape(obj.u(max(1, 1+tt+vTempTimeFrameSyncIdx), :), [], 1);
					obj.u(tt+1, :) = - uSFB * feedbackState*IOPTot * selectXHat.' * yi(tt, :).' ...
									- feedbackInputDelayedSync * vTempSync;
					timeData.nextStep();
				end
			else
			%% Simulation with input delay but usual controller
			% Comments see general case above: 
			%	"Simulation with input and output delay and controller and observer for delays"
				timeData.reset();
 				while(~timeData.isOver)
					tt = double(timeData.nowIdx);
					obj.uDelayed(tt, :) = diag(selectPlantInput * obj.u(max(1, tt+vTempTimeFrameSyncIdx), :));
					[~, y_temp] = ode15s(f, [ti(tt-1) ti(tt)], yi(tt-1, :), odeopts, ...
						@(t) (t-ti(tt-1))/simPars.ts * obj.uDelayed(tt, :).'...
								+ (ti(tt)-t)/simPars.ts * obj.uDelayed(tt-1, :).');
					yi(tt, :) = y_temp(end, :);
					obj.u(tt, :) = kT * yi(tt, :).';
					timeData.nextStep();
				end
			end
			
		elseif useOutputDelaySimulation
			if obj.useStateObserverForOutputDelay
			%% Simulation with output delay and observer for output delays
			% Comments see general case above: 
			%	"Simulation with input and output delay and controller and observer for delays"
				timeData.reset();
 				while(~timeData.isOver)
					tt = double(timeData.nowIdx);
					obj.ymDelayed(tt, :) = diag(selectPlantOutput * obj.ym(max(1, tt+wTempTimeFrameSyncIdx), :));
					[dummy.wHat(tt, :), ~, dummy.wObsz0(tt, :)] = ...
						hyperbolicObserverSubsystem.sim_one_timestep(...
							dummy.wHat(tt-1, :).',	...		% state in last timestep
							dummy.d, ...					% disturbance
							[dummy.ymHat(tt-1, :).'; ...	% boundary input x_z^hat(0, t)
							obj.ymDelayed(tt, :).'], ...	% distributed input y(t)
							dummy.f, timeData);				% fault, time
					[~, y_temp] = ode15s(f, [ti(tt-1) ti(tt)], yi(tt-1, :), odeopts, ...
						@(t) (1+0*t)*(obj.ymDelayed(tt, :) - dummy.wObsz0(tt, :)).');
					yi(tt, :) = y_temp(end, :);
					obj.ym(tt, :) = CmNoDelay * yi(tt, :).';
					dummy.ymHat(tt, :) = selectYmHat * yi(tt, :).';
					timeData.nextStep();
				end
				wHatCell = hyperbolicObserverSubsystem.stateVecToCell(dummy.wHat);
				obj.wHat = zeros(timeData.numberOfTimeSteps, ndiscPlant, n);
				for it = 1:n
					obj.wHat(:, :, it) = misc.translate_to_grid(wHatCell{it}.', linspace(0, 1, ndiscPlant)).';
				end

			else
			%% Simulation with output delay but usual observer
			% Comments see general case above: 
			%	"Simulation with input and output delay and controller and observer for delays"
				timeData.reset();
 				while(~timeData.isOver)
					tt = double(timeData.nowIdx);
					obj.ymDelayed(tt, :) = diag(selectPlantOutput * obj.ym(max(1, tt+wTempTimeFrameSyncIdx), :));
					[~, y_temp] = ode15s(f, [ti(tt-1) ti(tt)], yi(tt-1, :), odeopts, ...
						@(t) (t-ti(tt-1))/simPars.ts * obj.ymDelayed(tt, :).'...
								+ (ti(tt)-t)/simPars.ts * obj.ymDelayed(tt-1, :).');
					yi(tt, :) = y_temp(end, :);
					obj.ym(tt, :) = CmNoDelay * yi(tt, :).';
					timeData.nextStep();
				end
			end
			
		else
		%% Default case: No delay
			% As no delay needs to be considered, the simulation of the
			% parabolic system runs from the start to end with one call of
			% a solver.
			[ti, yi] = ode15s(f, [t_start tEnd], x0Sim, odeopts);
			if any(diff(ti))<= 0
				warning('Solver returned non-increasing ti. Probably the system is unstable!')
			end
		end
		if block_idx<length(obj.tv)
			x0Sim = yi(end, :).'; % End values are new starting values
			for i = 1:sM.nv
				if ~isnan(obj.v0(block_idx+1, i))
					x0Sim(n*ndiscPlant+i) = obj.v0(block_idx+1, i); % Values of the signal model are given at the timestamps tv
				end
			end
		end
		t = [t(1:end-1); ti];
		y = [y(1:end-1, :); yi];
	end
	
%% Store simulation results
	obj.x = y(:, 1:n*ndiscPlant);
	xVec = obj.ppide.sim2Sys(obj.x);
	obj.xQuantity = quantity.Discrete(xVec,'gridName',{'t','z'},...
		'grid',{t,zdiscPlant});
	
	obj.v = y(:, n*ndiscPlant+1:n*ndiscPlant+obj.sM.nv);
	obj.t = t;
	obj.xE = y;
	
	if observerFolding
		xHatFolded = y(:, n*ndiscPlant+obj.sM.nv+1:n*ndiscPlant+obj.sM.nv+size(A_obs, 1));
		obj.xHatFolded = xHatFolded;
		xHatFoldedVec = ppide.sim2Sys(xHatFolded,2*n);
		xHatForUnfold = reshape(xHatFoldedVec,size(xHatFoldedVec,1),[],n,2);
		xHatFoldedQuantity = quantity.Discrete(ppide.sim2Sys(xHatFolded,2*n),...
			'gridName',{'t','z'},'grid',{obj.t,zdiscObs});
		xHatUnfolded = misc.unfold(xHatForUnfold,zm,2,ndiscPlant);
% 		xHatUnfolded = zeros(size(y,1),n*ndiscPlant);
% 		zmIdx = find(zdiscPlant<zm,1,'last');
% 		for i=1:n
% % 			xHatUnfolded(:,(i-1)*ndiscPlant+1:(i-1)*ndiscPlant+zmIdx) = permute(...
% % 				interp1(...
% % 					linspace(zdiscObs(zmIdx),0,ndiscObs),...
% % 						permute(xHatFolded(:,(i-1)*ndiscObs+1:i*ndiscObs),[2 1]),...
% % 				zdiscObs(1:zmIdx))...
% % 				,[2 1]);
% 			% ignore first point of xl because folding point is already included in xr!
% 			xHatUnfolded(:,(i-1)*ndiscPlant+1:(i-1)*ndiscPlant+zmIdx) = ...
% 				xHatFoldedQuantity(i).on({obj.t,linspace(1,zdiscPlant(2),zmIdx)});			
% % 			xHatUnfolded(:,(i-1)*ndiscObs+zmIdx+1:i*ndiscObs) =	permute(...
% % 				interp1(...
% % 					linspace(zdiscObs(zmIdx+1),1,ndiscObs),...
% % 						permute(xHatFolded(:,(i+n-1)*ndiscObs+1:(i+n)*ndiscObs),[2 1]),...
% % 				zdiscObs(zmIdx+1:end))...
% % 				,[2 1]);
% 			xHatUnfolded(:,(i-1)*ndiscPlant+zmIdx+1:i*ndiscPlant) =	...
% 				xHatFoldedQuantity(i+n).on({obj.t,linspace(0,1,ndiscPlant-zmIdx)});
% 			
% 		end
		obj.xHat = xHatUnfolded;
	else
		obj.xHat = y(:, n*ndiscPlant+obj.sM.nv+1:n*ndiscPlant+obj.sM.nv+size(A_obs, 1));
	end
	if observerFolding
% 		obj.xHatQuantity = quantity.Discrete(ppide.sim2Sys(obj.xHat),'gridName',{'t','z'},...
% 			'grid',{obj.t,zdiscPlant});
		obj.xHatQuantity = quantity.Discrete(obj.xHat,'gridName',{'t','z'},...
			'grid',{obj.t,zdiscPlant});
	else
		obj.xHatQuantity = quantity.Discrete(ppide.sim2Sys(obj.xHat),'gridName',{'t','z'},...
			'grid',{obj.t,zdiscObs});
	end
	obj.vHat = y(:, n*ndiscPlant+obj.sM.nv+size(A_obs, 1)+1:n*ndiscPlant+obj.sM.nv+size(A_obs, 1)+sM.nv);
	obj.vHatInt = y(:, n*ndiscPlant+obj.sM.nv+size(A_obs, 1)+sM.nv+1:n*ndiscPlant+obj.sM.nv+size(A_obs, 1)+sM.nv+nvHatInt);

	% Transform solution to target system
	% 1. resample time
	% Use plot_pars.plot_res_2D_lines as parameter, as it will probably
	% be plotted with this resolution.
	plot_pars = obj.plotPars;
	if isempty(plot_pars)
		% set default plot parameters
		plot_pars.plot_res_2D_lines = 400;
	end
	% Time must be second argument in resample_time
	if ~observerFolding
		for i=1:n
			xResZ(:,(i-1)*ndiscObs+1:i*ndiscObs) = misc.translate_to_grid(obj.x(:,(i-1)*ndiscPlant+1:i*ndiscPlant),{t, zdiscObs},'gridOld',{t,zdiscPlant});
		end
		[eRes_temp, ~] = resample_time((xResZ.'-obj.xHat.'), obj.t, plot_pars.plot_res_2D_lines);
	else
		% in the observer folding case, observer states are sampled with ndiscPlant!
		xResZ = obj.x(:,1:n*ndiscPlant);
		xResZVec = ppide.sim2Sys(obj.x(:,1:n*ndiscPlant));
		xResZFolded = misc.fold(xResZVec,zm,2,'nFolded',ndiscObs,'preserve',true);
% 		xResZFolded = misc.fold(xResZVec,zm,2,'nFolded',ndiscObs);
		% Attention: the folding function always removes the folding point from the first
		% state! For the observer error this must be respected! 
		% But the distance is not one tick of the observer resolution but just the ticks in
		% zRight!!! Therefore, perhaps it is though ok to leave it... Test!
% 		xResZ1 = xResZFolded(:,:,:,1); % these are the values until nearly the folding point.
		xHatFoldedVec = ppide.sim2Sys(obj.xHatFolded,4);
% 		xHat1 = xHatFoldedVec(:,:,1:n);
% 		xHat1Only = permute(interp1(zdiscObs(2:end),permute(xHat1(:,2:end,:),[2 1 3]),linspace(zdiscObs(2),1,ndiscObs)),[2 1 3]);
% 		e1 = permute(interp1([0 linspace(zdiscObs(2),1,ndiscObs)],...
% 			permute(cat(2,zeros(size(xResZ1,1),1,n),(xResZ1 - xHat1Only)),[2 1 3]),...
% 			zdiscObs),[2 1 3]);
% 		e2 = xResZFolded(:,:,:,2) - xHatFoldedVec(:,:,n+1:end);
% 		eFolded = cat(3,e1,e2);
		eFolded = reshape(xResZFolded,size(xResZFolded,1),[],2*n)-xHatFoldedVec;
		% reshape to remove last dimension
		[eRes_temp,~] = resample_time(reshape(eFolded,size(eFolded,1),[]).', obj.t, plot_pars.plot_res_2D_lines);
	end
	[xRes_temp, t_res] = resample_time(obj.x.', obj.t, plot_pars.plot_res_2D_lines);
	% TODO: Unfold observer states for observer error
	
	xRes = xRes_temp.';
	eRes = eRes_temp.';
	
	% Transform to vector (., ., .) form and use backstepping transformation
	x_vec = ppide.sim2Sys(xRes);
	e_vec = ppide.sim2Sys(eRes,no);
	
	if uSO %&& ~observerFolding
		e_vec = permute(interp1(zdiscObs,permute(e_vec,[2 1 3]),zdiscObsKernel),[2 1 3]);
	end
	sizEVec = size(e_vec);
	tic
	fprintf('  Transforming Simulation result to target system...')
	if folding
		zLeft = zeros(n,ndiscPlant);
		zRight = zeros(n,ndiscPlant);
		for i=1:n
			zRight(i,:) = linspace(z0(i),1,ndiscPlant);
			dZ = diff(zRight(i,:));
			zLeft(i,:) = linspace(0,z0(i)-dZ(1),ndiscPlant);
		end
		% transform solution back to folded representation
		xInterp = numeric.interpolant({t_res,zdiscPlant,1:n},x_vec);
		xNew = zeros(length(t_res),ndiscPlant,2*n);
		for i=1:n
			xNew(:,:,i) = xInterp.evaluate(t_res,fliplr(zLeft(i,:)),i);
			xNew(:,:,i+n) = xInterp.evaluate(t_res,zRight(i,:),i);
		end
		
		x_vecTrans = xNew;
% 		x_vecTrans = permute(interp1(zdiscPlant,permute(xNew,[2 1 3]),obj.controllerKernel.zdisc),[2 1 3]);
	else
		x_vecTrans = x_vec;
% 		x_vecTrans =  permute(interp1(zdiscPlant,permute(x_vec,[2 1 3]),obj.controllerKernel.zdisc),[2 1 3]);
	end
	if numtPlant>1
		yBSTemp =  Tc(x_vecTrans,permute(translate_to_grid(permute(obj.controllerKernel.get_K(),[5 1 2 3 4]),t_res),[2 3 4 5 1]),zdiscPlant,obj.controllerKernel.zdisc);
% 		yBSTemp =  Tc(x_vecTrans,permute(translate_to_grid(permute(obj.controllerKernel.get_K(),[5 1 2 3 4]),t_res),[2 3 4 5 1]),obj.controllerKernel.zdisc,obj.controllerKernel.zdisc);
		if obj.useStateObserver
			if observerFolding
				% observer error can only be transformed in folding coordinates:
				eVecFolded = reshape(e_vec,sizEVec(1),sizEVec(2),n,2);
				yBSObsFoldedLeft = Tc(eVecFolded(:,:,:,1),permute(translate_to_grid(permute(-obj.observerKernel1.KI,[5 1 2 3 4]),t_res),[2 3 4 5 1]),obj.observerKernel1.zdisc,obj.observerKernel1.zdisc);
				yBSObsFoldedRight = Tc(eVecFolded(:,:,:,2),permute(translate_to_grid(permute(-obj.observerKernel2.KI,[5 1 2 3 4]),t_res),[2 3 4 5 1]),obj.observerKernel2.zdisc,obj.observerKernel2.zdisc);
				yBSObsFolded = zeros(size(eVecFolded));
				yBSObsFolded(:,:,:,1) = yBSObsFoldedLeft;
				yBSObsFolded(:,:,:,2) = yBSObsFoldedRight;
				yBSObsFolded = permute(translate_to_grid(permute(yBSObsFolded,[2 1 3 4]),zdiscPlant),[2 1 3 4]);
				% only unfold with high resolution!
				yBSObs = misc.unfold(yBSObsFolded,zm,2);
			else
% 				yBSObs = Tc(e_vec,permute(translate_to_grid(permute(-obj.observerKernel.KI,[5 1 2 3 4]),t_res),[2 3 4 5 1]),zdiscObs,zdiscObsKernel);
				yBSObs = Tc(e_vec,permute(translate_to_grid(permute(-obj.observerKernel.KI,[5 1 2 3 4]),t_res),[2 3 4 5 1]),zdiscObsKernel,zdiscObsKernel);
			end
		end
	else
		yBSTemp =  Tc(x_vecTrans,obj.controllerKernel.get_K(),zdiscPlant,obj.controllerKernel.zdisc);
% 		yBSTemp =  Tc(x_vecTrans,obj.controllerKernel.get_K(),obj.controllerKernel.zdisc,obj.controllerKernel.zdisc);
		if obj.useStateObserver
			if observerFolding
				warning('Backstepping trafo of observer error not yet implemented!')
			else
				yBSObs =  Tc(e_vec,-obj.observerKernel.KI,zdiscObsKernel,zdiscObsKernel);
			end
		end
	end
% 	yBSTemp = permute(interp1(obj.controllerKernel.zdisc,permute(yBSTemp,[2 1 3]),zdiscPlant),[2 1 3]);
	
	if folding
		% unfold back to original system
		yBS = zeros(length(t_res),ndiscPlant,n);
% 		yBS = zeros(length(t_res),ndiscKernel,n);
		for i=1:n
			z0iIdx = find(zdiscPlant < z0(i),1,'last');
% 			z0iIdx = find(zdiscKernel<z0(i),1,'last');
			z0iIdxPars = find(zdiscPars < z0(i),1,'last');
			yBS(:,1:z0iIdx,i) = permute(...
				interp1(...
					linspace(zdiscPlant(z0iIdx),0,ndiscPlant),permute(yBSTemp(:,:,i),[2 1 3]),...
				zdiscPlant(1:z0iIdx))...
				,[2 1]);
% 			yBS(:,1:z0iIdx,i) = permute(...
% 				interp1(...
% 					linspace(zdiscPars(z0iIdxPars),0,ndiscKernel),permute(yBSTemp(:,:,i),[2 1 3]),...
% 				zdiscKernel(1:z0iIdx))...
% 				,[2 1]);
			yBS(:,z0iIdx+1:end,i) = permute(...
				interp1(...
					linspace(zdiscPlant(z0iIdx+1),1,ndiscPlant),permute(yBSTemp(:,:,i+n),[2 1 3]),...
				zdiscPlant(z0iIdx+1:end))...
				,[2 1]);
% 			yBS(:,z0iIdx+1:end,i) = permute(...
% 				interp1(...
% 					linspace(zdiscPars(z0iIdxPars+1),1,ndiscKernel),permute(yBSTemp(:,:,i+n),[2 1 3]),...
% 				zdiscKernel(z0iIdx+1:end))...
% 				,[2 1]);
		
		end
	else
		yBS = yBSTemp;
	end
	
	time=toc;
	fprintf('  Finished transformation! (%0.2fs)\n', time);
	% transform to simulation form
	obj.xTransformed = ppide.sys2Sim(yBS);
	obj.xTransformedQuantity = quantity.Discrete(yBS,'gridName',{'t','z'},...
		'grid',{t_res,zdiscPlant});
	if obj.useStateObserver
		obj.eTransformedObserver = ppide.sys2Sim(yBSObs);
% 		warning('Changed this')
		if observerFolding
			obj.eTransformedObserverQuantity = quantity.Discrete(yBSObs,'gridName',{'t','z'},...
				'grid',{t_res,zdiscPlant});
		else
			obj.eTransformedObserverQuantity = quantity.Discrete(yBSObs,'gridName',{'t','z'},...
				'grid',{t_res,zdiscObsKernel});
		end
	end
	obj.tTransformed = t_res;
	
	%% Calculate Norms and save
	y_transformed = obj.xTransformed;
	y = obj.x;
	ndiscPlant = obj.ppide.ndisc;
	zdiscPlant = linspace(0,1,ndiscPlant);
	y_quad_sum = 0;
	y_transformed_quad_sum = 0;
	lPlant = misc.translate_to_grid(obj.ppide.l_disc,{zdiscPlant});
	lKernel =  misc.translate_to_grid(obj.ppide.l_disc,{zdiscKernel});
	for i=1:obj.ppide.n
		y_quad_sum = y_quad_sum + (permute(lPlant(:,i,i,:),[4,1,2,3]).^(-1/2).*y(:,(i-1)*ndiscPlant+1:i*ndiscPlant)).^2;
		y_transformed_quad_sum = y_transformed_quad_sum + (permute(lPlant(:,i,i,:),[4,1,2,3]).^(-1/2).*y_transformed(:,(i-1)*ndiscPlant+1:i*ndiscPlant)).^2;
% 		y_transformed_quad_sum = y_transformed_quad_sum + (permute(lKernel(:,i,i,:),[4,1,2,3]).^(-1/2).*y_transformed(:,(i-1)*ndiscKernel+1:i*ndiscKernel)).^2;
	end
	y_2_norm_quad = (y_quad_sum);
	y_L2_norm = sqrt(numeric.trapz_fast(zdiscPlant,y_2_norm_quad));
	y_transformed_L2_norm = sqrt(numeric.trapz_fast(zdiscPlant,y_transformed_quad_sum));
% 	y_transformed_L2_norm = sqrt(numeric.trapz_fast(zdiscKernel,y_transformed_quad_sum));
	if y_L2_norm(1) ~= 0
		obj.xNorm = y_L2_norm;%/y_L2_norm(1);
	else
		obj.xNorm = y_L2_norm;
	end
	if y_transformed_L2_norm(1) ~= 0
		obj.xTransformedNorm = y_transformed_L2_norm;%/y_transformed_L2_norm(1);
	else
		obj.xTransformedNorm = y_transformed_L2_norm;
	end
	
	obj.simulated = 1;
	time=toc(simstart);
	fprintf('Finished simulation! (%0.2fs) \n', time);
end %function