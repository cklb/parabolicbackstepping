function h0 = get_h0_pideFolding(ppide,kern_elem)
%GET_H0_PIDE compute the starting value H0 for fixpoint iteration for systems of
%parabolic pides.
%
%   INPUT PARAMETERS:
%     PPIDE_SYS            ppide     : system of class ppide_sys
%     PPIDE_KERNEL_ELEMENT kern_elem : ppide_kernel_element
%
%   OUTPUT PARAMETERS:
%     MATRIX      h0    : start-value of fixpoint iteration


% Created on 31.07.2019 by Simon Kerschbaum based on a copy of get_h0_pide
import misc.*
import numeric.*
import dps.parabolic.control.backstepping.*
import dps.parabolic.system.*
if ~isa(ppide,'ppide_sys')
	error('ppide must be of class ppide_sys!')
end

xi_disc =kern_elem.xi;
eta_disc = kern_elem.eta;
% eta_fine = kern_elem.eta_fine;
i = kern_elem.i;
j = kern_elem.j;

zdisc = kern_elem.zdisc;
% zdisc_plant = linspace(0,1,ppide.ndiscPars);
% Hartwig edited
ndisc = length(zdisc);
n = ppide.n;
tDisc = ppide.ctrl_pars.tDiscKernel;
if ndisc~= n
	if isa(ppide.ctrl_pars.mu,'function_handle') % function
		if nargin(ppide.ctrl_pars.mu)==1
			muHelp = eval_pointwise(ppide.ctrl_pars.mu,zdisc);
		else
			muHelp = zeros(ndisc,n,n,length(tDisc));
			for z_idx=1:ndisc
				muHelp(z_idx,:,:,:) = permute((eval_pointwise(@(z)ppide.ctrl_pars.mu(zdisc(z_idx),z),tDisc)),[2 3 1]);
			end
		end
	elseif size(ppide.ctrl_pars.mu,1) > n % is discretized
		if length(size(ppide.ctrl_pars.mu))>3
			muInterpolant = interpolant({linspace(0,1,size(ppide.ctrl_pars.mu,1)),1:n,1:n,tDisc}, ppide.ctrl_pars.mu);
			muHelp = muInterpolant.evaluate(zdisc,1:n,1:n,tDisc);
		else
			muInterpolant = interpolant({linspace(0,1,size(ppide.ctrl_pars.mu,1)),1:n,1:n}, ppide.ctrl_pars.mu);
			muHelp = muInterpolant.evaluate(zdisc,1:n,1:n);
		end
		%muHelp = ppide.ctrl_pars.mu;
	elseif isequal(size(ppide.ctrl_pars.mu,1),n) % constant matrix, two dimensions
		muHelp(1:ndisc,:,:) = repmat(shiftdim(ppide.ctrl_pars.mu,-1),ndisc,1,1);
	else % discretized matrix
		error('dimensions of mu not correct!')
	end
else
	error('ndisc=n is a very bad choice!')
end
if length(size(muHelp)) > 3
	mu = reshape(muHelp(:,i,j,:),[ndisc,length(tDisc)]);
else
	mu = reshape(muHelp(:,i,j),[ndisc,1]);
end
%mu = ppide.ctrl_pars.mu(i,j);
% get discretized version of integral term
numtKernel = size(kern_elem.G,3);
numtL = size(kern_elem.lambda_i,2);
if ndims(ppide.a_disc)<4
	% Interpolation geht nicht, wenn A zeitkonstant ist, weil nur ein Punkt!
	A_disc = permute(repmat(translate_to_grid(ppide.a_disc,zdisc),1,1,1,numtKernel),[1 4 2 3]);
else
	A_disc = translate_to_grid(permute(ppide.a_disc,[1 4 2 3]),{zdisc, linspace(0,1,numtKernel)},'zdim',2);
end
A_ij_disc = A_disc(:,:,i,j);
s = kern_elem.s;


h01(size(xi_disc,1),size(eta_disc,1),numtKernel) = 0; %preallocate
r6 = kern_elem.r{6};
lambda_i = kern_elem.lambda_i;
lambda_j = kern_elem.lambda_j;

r6_disc = r6;
xi_fik = linspace(0,1,401).'*xi_disc(end,:);
for tIdx = 1:numtKernel
	if numtL >1
		tIdxL = tIdx;
	else
		tIdxL = 1;
	end
	if size(r6_disc,2)>1
		tIdxr6 = tIdx;
	else
		tIdxr6 = 1;
	end
	if size(mu,2)>1
		tIdxMu = tIdx;
	else
		tIdxMu = 1;
	end
	% TODO: Performance mit Interpolanten!
	eta_l_diff_fik(:,tIdxL) = interp1(xi_disc(:,tIdxL),kern_elem.eta_l_diff(:,tIdxL),xi_fik(:,tIdxL));
	eta_l_fik(:,tIdxL) = interp1(xi_disc(:,tIdxL),kern_elem.eta_l(:,tIdxL),xi_fik(:,tIdxL));
	[~,~,z_fine_vec(:,tIdxL),~] = kern_elem.get_z(xi_fik(:,tIdxL),eta_l_fik(:,tIdxL),tIdxL);
	A_ij_fine(:,tIdx) = interp1(zdisc,A_ij_disc(:,tIdx),z_fine_vec(:,tIdxL));
	lambda_i_fine(:,tIdxL) = interp1(zdisc,lambda_i(:,tIdxL),z_fine_vec(:,tIdxL));
	lambda_i_diff_fine(:,tIdxL) = interp1(zdisc,kern_elem.lambda_i_diff(:,tIdxL),z_fine_vec(:,tIdxL));
	r6_fine(:,tIdxr6) = interp1(zdisc,r6_disc(:,tIdxr6),z_fine_vec(:,tIdxL));
	% Hartwig edited
	% mu is repeated for all times
	mu_ij(:,tIdxMu) = interp1(zdisc,mu(:,tIdxMu),z_fine_vec(:,tIdxL));  %#ok<*AGROW>
end
if size(z_fine_vec,2)==1 && numtKernel > 1
	z_fine_vec = repmat(z_fine_vec,1,numtKernel);
end

if ~ppide.ctrl_pars.eliminate_convection
	if lambda_i(1)==lambda_j(1)	
		h01_vec = -1/4*sqrt(lambda_i_fine).*(A_ij_fine+mu_ij)... 
					-lambda_i_diff_fine/4 ...
					.*cumtrapz_fast_nDim(z_fine_vec,(A_ij_fine+mu_ij)/2./sqrt(lambda_i_fine),1,'equal');			
		
	else
		h01_vec = r6_fine.*eta_l_diff_fik./(eta_l_diff_fik*s-1);
		if size(h01_vec,2) == 1
			h01_vec = repmat(h01_vec,1,numtKernel);
		end
	end
	% % interpolate on complete grid and repeat for all eta values
	for tIdx = 1:numtL%                               h01(xi,t) 
		if numtL >1
			tIdxAll = tIdx;
		else
			tIdxAll = 1:numtKernel;
		end
% 		h01(:,:,tIdxAll) = repmat(reshape(interp1(xi_fik(:,tIdx),h01_vec(:,tIdxAll),xi_disc(:,tIdx)),[size(xi_disc,1),1,numtKernel]),1,size(eta_disc,1),1);
		% Hartwig edited
		h01(:,:,tIdxAll) = repmat(reshape(interp1(xi_fik(:,tIdx),h01_vec(:,tIdxAll),xi_disc(:,tIdx)),[size(xi_disc,1),1,length(tIdxAll)]),1,size(eta_disc,1),1);
% 		warning('Nicht sicher ob korrekt berechnet');
	end
else
	error('Folding not implemented for eliminate_convection')
end

h0 = h01;


end

