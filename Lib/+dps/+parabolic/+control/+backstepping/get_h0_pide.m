function h0 = get_h0_pide(ppide,kern_elem)
%GET_H0_PIDE compute the starting value H0 for fixpoint iteration for systems of
%parabolic pides.
%
%   INPUT PARAMETERS:
%     PPIDE_SYS            ppide     : system of class ppide_sys
%     PPIDE_KERNEL_ELEMENT kern_elem : ppide_kernel_element
%
%   OUTPUT PARAMETERS:
%     MATRIX      h0    : start-value of fixpoint iteration


% Created on 03.04.2017 by Simon Kerschbaum
import misc.*
import numeric.*
import dps.parabolic.control.backstepping.*
import dps.parabolic.system.*
if ~isa(ppide,'ppide_sys')
	error('ppide must be of class ppide_sys!')
end

xi_disc =kern_elem.xi;
eta_disc = kern_elem.eta;
% eta_fine = kern_elem.eta_fine;
i = kern_elem.i;
j = kern_elem.j;

zdisc = kern_elem.zdisc;
% zdisc_plant = linspace(0,1,ppide.ndiscPars);
% Hartwig edited
ndisc = length(zdisc);
n = ppide.n;
tDisc = ppide.ctrl_pars.tDiscKernel;
if ndisc~= n
	if isa(ppide.ctrl_pars.mu,'function_handle') % function
		if nargin(ppide.ctrl_pars.mu)==1
			muHelp = eval_pointwise(ppide.ctrl_pars.mu,zdisc);
		else
			muHelp = zeros(ndisc,n,n,length(tDisc));
			for z_idx=1:ndisc
				muHelp(z_idx,:,:,:) = permute((eval_pointwise(@(z)ppide.ctrl_pars.mu(zdisc(z_idx),z),tDisc)),[2 3 1]);
			end
		end
	elseif size(ppide.ctrl_pars.mu,1) > n % is discretized
		if length(size(ppide.ctrl_pars.mu))>3
			muInterpolant = interpolant({linspace(0,1,size(ppide.ctrl_pars.mu,1)),1:n,1:n,tDisc}, ppide.ctrl_pars.mu);
			muHelp = muInterpolant.evaluate(zdisc,1:n,1:n,tDisc);
		else
			muInterpolant = interpolant({linspace(0,1,size(ppide.ctrl_pars.mu,1)),1:n,1:n}, ppide.ctrl_pars.mu);
			muHelp = muInterpolant.evaluate(zdisc,1:n,1:n);
		end
		%muHelp = ppide.ctrl_pars.mu;
	elseif isequal(size(ppide.ctrl_pars.mu,1),n) % constant matrix, two dimensions
		muHelp(1:ndisc,:,:) = repmat(shiftdim(ppide.ctrl_pars.mu,-1),ndisc,1,1);
	else % discretized matrix
		error('dimensions of mu not correct!')
	end
else
	error('ndisc=n is a very bad choice!')
end
if length(size(muHelp)) > 3
	mu = reshape(muHelp(:,i,j,:),[ndisc,length(tDisc)]);
else
	mu = reshape(muHelp(:,i,j),[ndisc,1]);
end
%mu = ppide.ctrl_pars.mu(i,j);
% get discretized version of integral term
% F_disc = translate_to_grid(ppide.f_disc,zdisc,'zdim',2);
% F_ij_disc = F_disc(:,:,i,j);
if nargin(ppide.f)==2
	F_ij_disc = ppide.f(i,j).on({zdisc,zdisc});
else
	F_ij_disc = ppide.f(i,j).on({zdisc,zdisc,tDisc});
end
numtKernel = size(kern_elem.G,3);
numtL = size(kern_elem.lambda_i,2);
if ndims(ppide.a_disc)<4
	% Interpolation geht nicht, wenn A zeitkonstant ist, weil nur ein Punkt!
	A_disc = permute(repmat(translate_to_grid(ppide.a_disc,zdisc),1,1,1,numtKernel),[1 4 2 3]);
else
	A_disc = translate_to_grid(permute(ppide.a_disc,[1 4 2 3]),{zdisc, linspace(0,1,numtKernel)},'zdim',2);
end

A0_disc = translate_to_grid(ppide.a_0_disc,zdisc);
if size(A0_disc,4)>1
	A0_disc = permute(translate_to_grid(permute(A0_disc,[4 1 2 3]),linspace(0,1,numtKernel)),[2 3 4 1]);
end
A_ij_disc = A_disc(:,:,i,j);
A0_ij_disc = permute(A0_disc(:,i,j,:),[1 4 2 3]);
min_dist = kern_elem.min_dist;
s = kern_elem.s;
a = (s+1)/2 *kern_elem.phi_i(end,:) - (s-1)/2*kern_elem.phi_j(end,:); % a Ortsbereich


a_i_disc = kern_elem.a_i;
a_j_disc = kern_elem.a_j;

% get xis belonging to the lower border
xi_vec = kern_elem.get_xi(zdisc,zdisc);
% h01_sparse(1:length(xi_vec)) = 0;

h01(size(xi_disc,1),size(eta_disc,1),numtKernel) = 0; %preallocate
h02(size(xi_disc,1),size(eta_disc,1),numtKernel) = 0; %preallocate
h03(size(xi_disc,1),size(eta_disc,1),numtKernel) = 0; %preallocate
h04(size(xi_disc,1),size(eta_disc,1),numtKernel) = 0; %preallocate

% r1 = kern_elem.r{1};
r2 = kern_elem.r{2};
r6 = kern_elem.r{6};
% c_i = kern_elem.c_i;
% c_j = kern_elem.c_j;
lambda_i = kern_elem.lambda_i;
lambda_j = kern_elem.lambda_j;

E2 = ppide.b_op1.Sr;
if isa(ppide.b_op1.b_d,'function_handle')
	Bd = misc.multArray(E2*E2.',permute(misc.eval_pointwise(ppide.b_op1.b_d,linspace(0,1,numtKernel)),[2 3 1]));
else
	% can have a 3rd dimension.
	Bd = misc.multArray(E2*E2.',ppide.b_op1.b_d,2,1);
	if size(Bd,3)>1
		if size(Bd,3)~=numtKernel
			Bd = permute(misc.translate_to_grid(permute(Bd,[3 1 2]),linspace(0,1,numtKernel)),[2 3 1]);
		end
	end
end

mTN = ppide.ctrl_pars.makeTargetNeumann;
z_idx_vec = 1:length(zdisc);
r2_disc = r2;
r6_disc = r6;
xi_fik = linspace(0,1,401).'*xi_disc(end,:);
for tIdx = 1:numtKernel
	if numtL >1
		tIdxL = tIdx;
	else
		tIdxL = 1;
	end
	if size(r2_disc,2)>1
		tIdxr2 = tIdx;
	else
		tIdxr2 = 1;
	end
	if size(r6_disc,2)>1
		tIdxr6 = tIdx;
	else
		tIdxr6 = 1;
	end
	if size(mu,2)>1
		tIdxMu = tIdx;
	else
		tIdxMu = 1;
	end
	% TODO: Performance mit Interpolanten!
	eta_l_diff_fik(:,tIdxL) = interp1(xi_disc(:,tIdxL),kern_elem.eta_l_diff(:,tIdxL),xi_fik(:,tIdxL));
	eta_l_fik(:,tIdxL) = interp1(xi_disc(:,tIdxL),kern_elem.eta_l(:,tIdxL),xi_fik(:,tIdxL));
	[~,~,z_fine_vec(:,tIdxL),~] = kern_elem.get_z(xi_fik(:,tIdxL),eta_l_fik(:,tIdxL),tIdxL);
	A_ij_fine(:,tIdx) = interp1(zdisc,A_ij_disc(:,tIdx),z_fine_vec(:,tIdxL));
	lambda_i_fine(:,tIdxL) = interp1(zdisc,lambda_i(:,tIdxL),z_fine_vec(:,tIdxL));
	lambda_i_diff_fine(:,tIdxL) = interp1(zdisc,kern_elem.lambda_i_diff(:,tIdxL),z_fine_vec(:,tIdxL));
	r2_fine(:,tIdxr2) = interp1(zdisc,r2_disc(:,tIdxr2),z_fine_vec(:,tIdxL));
	r6_fine(:,tIdxr6) = interp1(zdisc,r6_disc(:,tIdxr6),z_fine_vec(:,tIdxL));
	% Hartwig edited
	% mu is repeated for all times
	mu_ij(:,tIdxMu) = interp1(zdisc,mu(:,tIdxMu),z_fine_vec(:,tIdxL)); 
end
if size(z_fine_vec,2)==1 && numtKernel > 1
	z_fine_vec = repmat(z_fine_vec,1,numtKernel);
end

if ~ppide.ctrl_pars.eliminate_convection
	if lambda_i(1)==lambda_j(1)	
		if i~= j && mTN
% 			warning('Hier k�nnte bei gleichen Diffusionskoeffizienten noch ein Fehler sein, Ergebnisse pr�fen!')
		    % in this case, the value K(0,0) is a degree of freedom which is set to zero.
		end
		% This value is different from eliminate_convection, because the K_tilde coordinates BC
		% on u,u is different.
		% -A18b-, 24.10.17
		% FEHLER!
		h01_vec = -1/4*sqrt(lambda_i_fine).*(A_ij_fine+mu_ij)... 
					-lambda_i_diff_fine/4 ...
					.*cumtrapz_fast_nDim(z_fine_vec,(A_ij_fine+mu_ij)/2./sqrt(lambda_i_fine),1,'equal')...
					-mTN.*lambda_i_diff_fine.*1/4.*sqrt(lambda_i_fine(1,:)).*reshape(Bd(i,j,:),[1 size(Bd,3)]);			
		
	else
		h01_vec = r6_fine.*eta_l_diff_fik./(eta_l_diff_fik*s-1);
		if size(h01_vec,2) == 1
			h01_vec = repmat(h01_vec,1,numtKernel);
		end
	end
	% % interpolate on complete grid and repeat for all eta values
	for tIdx = 1:numtL%                               h01(xi,t) 
		if numtL >1
			tIdxAll = tIdx;
		else
			tIdxAll = 1:numtKernel;
		end
% 		h01(:,:,tIdxAll) = repmat(reshape(interp1(xi_fik(:,tIdx),h01_vec(:,tIdxAll),xi_disc(:,tIdx)),[size(xi_disc,1),1,numtKernel]),1,size(eta_disc,1),1);
		% Hartwig edited
		h01(:,:,tIdxAll) = repmat(reshape(interp1(xi_fik(:,tIdx),h01_vec(:,tIdxAll),xi_disc(:,tIdx)),[size(xi_disc,1),1,length(tIdxAll)]),1,size(eta_disc,1),1);
% 		warning('Nicht sicher ob korrekt berechnet');
	end
else
% 	c_i_fine = translate_to_grid(c_i,z_fine_vec);
% 	if size(c_i_fine,2)>1 
% 		c_i_fine = permute(translate_to_grid(permute(c_i,[2 1]),linspace(0,1,numtL)),[2 1]);
% 	end
	if lambda_i(1)==lambda_j(1)
		% ACHTUNG: HIER WAR FEHLER VORGELEGEN! (20.02.18)
		% -c_i_fine...-Teil m�sste entfallen, siehe s.-A11b)-
		% korrigiert am 05.10.18, aber noch nicht getestet! Mit anderem Fall vergleichen!
		h01_vec = -1/4*r2_fine.*(A_ij_fine+mu_ij);%...
		  %-c_i_fine/2./sqrt(lambda_i_fine)...
		  %.*cumtrapz_fast(z_fine_vec,(A_ij_fine.'+mu)/2./sqrt(lambda_i_fine.')).'...
		  %.*r2_fine;
	else
		h01_vec = r6_fine.*eta_l_diff_fik./(eta_l_diff_fik*s-1);
	end

	% interpolate on complete grid and repeat for all eta values
	for tIdx = 1:numtKernel%                               h01(xi,t) 
		h01(:,:,tIdx) = repmat(interp1(xi_fik(:,tIdx),h01_vec(:,tIdx),xi_disc(:,tIdx)),1,size(eta_disc,1));
	end
end


% second part of h0, dependent on eta, borders dependend on xi, only exists
% if integral part is existent
if ~isequal(F_ij_disc,zeros(length(zdisc),length(zdisc)))
	if numtL>1
		error('Part not yet implemented!')
	end
	for xi_idx =  1:length(xi_disc)
			xi = xi_disc(xi_idx);
			% lower border
			eta_l_idx = find(acc(eta_disc,1/min_dist)>=acc(kern_elem.eta_l(xi_idx),1/min_dist),1);
			% upper border
			eta_h_idx = find(acc(eta_disc,1/min_dist)<=min(acc(xi,1/min_dist),acc(2*a-xi,1/min_dist)),1,'last'); % letzter Index im Ortsbereich
			% number of eta points
			n_eta = eta_h_idx - eta_l_idx+1;
			if n_eta > 0
			% it may be that eta_l > eta_h because of the different
			% discretizations of eta_disc and xi_disc
				try
					[z_vec, zeta_vec] = kern_elem.get_z(repmat(xi,1,n_eta),eta_disc(eta_l_idx:eta_h_idx));
				catch errmsg
					warning(mythrow(errmsg));
				end
				z_idx_vec = get_idx(z_vec,zdisc).';
				zeta_idx_vec = get_idx(zeta_vec,zdisc).';
				if ppide.ctrl_pars.eliminate_convection
					h02(xi_idx,eta_l_idx:eta_h_idx) = ...
					- cumtrapz_fast(eta_disc(eta_l_idx:eta_h_idx),...
								 kern_elem.e(z_idx_vec,zeta_idx_vec).'/4/s...
								 .*diag(F_ij_disc(z_idx_vec,zeta_idx_vec)).');
				else
					h02(xi_idx,eta_l_idx:eta_h_idx,:) = ...
					zeros(1,eta_h_idx-eta_l_idx+1,numtKernel)... % allows to handle dimensions easier
					- reshape(cumtrapz_fast_nDim(eta_disc(eta_l_idx:eta_h_idx),...
								 kern_elem.lambda_j(zeta_idx_vec)/4/s...
								 .*misc.mydiag(F_ij_disc,z_idx_vec,zeta_idx_vec)),1,eta_h_idx-eta_l_idx+1,[]);
				end
			end
	end
end

if ~ppide.ctrl_pars.eliminate_convection
	% third and 4th part of h0, dependent on xi,eta
	if lambda_i(1)==lambda_j(1)
		if ~isequal(A0_ij_disc,zeros(length(zdisc),1))
			if numtL > 1
				error('Part not yet implemented!')
			end
			for xi_idx = 1:length(xi_disc)
				xi = xi_disc(xi_idx);
				eta_l_idx = 1;
				% upper border
				eta_h_idx = find(acc(eta_disc,1/min_dist)<=min(acc(xi,1/min_dist),acc(2*a-xi,1/min_dist)),1,'last'); % letzter Index im Ortsbereich
				[z_eta_s,~] = kern_elem.get_z(eta_disc(eta_disc>=0),eta_disc(eta_disc>=0));
				z_eta_s_idx_vec = get_idx(z_eta_s,zdisc);
				for eta_idx = 1:eta_h_idx %nur in gueltigem Ortsbereich
					[~,~,z_fine,zeta_fine] = kern_elem.get_z(xi_disc(xi_idx),eta_disc(eta_idx));
					%%% TODO: HIER NOCH INTERPOLIEREN FUER INTEGRAL
					h03(xi_idx,eta_idx,:) = ...
						zeros(1,1,numtKernel)... % handle dimensions
						+1/4/s*(interp1(zdisc,a_i_disc,z_fine)-interp1(zdisc,a_j_disc,zeta_fine))...
						.*sqrt(kern_elem.lambda_j(1))*reshape(trapz_fast_nDim(eta_disc(1:eta_idx),A0_ij_disc(z_eta_s_idx_vec(1:eta_idx),:)),1,1,[]);
				end
			end
		end
	end	
	if lambda_i(1) == lambda_j(1)
		clearvars lambda_i_fine A_fine mu_ij
		for tIdx = 1:numtL
			if numtL >1
				tIdxAll = tIdx;
				tMuIdx = tIdx;
				if (size(mu,2)) > 1
					tMu = tIdx;
				else
					tMu = 1;
				end
			else
				tIdxAll = 1:numtKernel;
				if (size(mu,2)) > 1
					tMu = 1:numtKernel;
					tMuIdx = tMu;
				else
					tMu = 1;
					tMuIdx = tIdx;
				end
			end
			[~,~,z_fine_l_vec(:,tIdx),zeta_fine_l_vec(:,tIdx)] = kern_elem.get_z(xi_disc(:,tIdx),zeros(1,size(xi_disc,1)),tIdx);
			A_fine(:,tIdxAll) = interp1(zdisc,A_ij_disc(:,tIdxAll),z_fine_l_vec(:,tIdx));
			lambda_i_fine(:,tIdx) = interp1(zdisc,lambda_i(:,tIdx),z_fine_l_vec(:,tIdx));
			a_i_fine(:,tIdx) = interp1(zdisc,a_i_disc(:,tIdx),z_fine_l_vec(:,tIdx));
			a_j_fine(:,tIdx) = interp1(zdisc,a_j_disc(:,tIdx),zeta_fine_l_vec(:,tIdx));
			% Hartwig edited
			% Problem: weil hier "einfach Implementierung" wird mu im
			% zeitkonstanten Fall als Vektor �bergeben, weshalb ein
			% zus�tzlicher Index tMuIDx ben�tigt wird!
			mu_ijRes(:,tMuIdx) = interp1(zdisc,mu(:,tMu),z_fine_l_vec(:,tIdx)); % Muss f�r Zeitinvariante mu angepasst werden
		end
		h04 = repmat(permute(-1/4/s*(a_i_fine-a_j_fine)...
			.* (sqrt(lambda_i_fine)...
			.*cumtrapz_fast_nDim(z_fine_l_vec,(A_fine+mu_ijRes)/2./sqrt(lambda_i_fine),1,'equal')...
			+ mTN.*sqrt(lambda_i_fine.*lambda_i_fine(1,:)).*reshape(Bd(i,j,:),[1 size(Bd,3)])),[1 3 2]),1,size(eta_disc,1),1);
	end
	if s==1 % as Bd is diagonal, this only exists for lambda_i = lambda_j in fact
		clearvars lambda_i_fine A_fine z_fine_vec zeta_fine_vec a_i_fine a_j_fine
		for tIdx = 1:numtL
			if numtL >1
				tIdxAll = tIdx;
			else
				tIdxAll = 1:numtKernel;
			end
			eta = eta_disc(:,tIdx);
			% lower border
			eta_l_idx = find(acc(eta,1/min_dist)>=0,1);
			for eta_idx = eta_l_idx:numel(eta)
				if eta(eta_idx)>=0
					a_i_fine(:,eta_idx,tIdx) = reshape(kern_elem.a_iInterp.evaluate(kern_elem.zMat(:,eta_idx,tIdx),tIdx),[size(xi_disc,1),1,numtL]);
					a_j_fine(:,eta_idx,tIdx) = reshape(kern_elem.a_jInterp.evaluate(kern_elem.zetaMat(:,eta_idx,tIdx),tIdx),[size(xi_disc,1),1,numtL]);
				end
			end
		end
		h03 = h03 + 1/4/s*(a_i_fine-a_j_fine)...
			*mTN.*reshape(lambda_j(1,:),1,1,[]).*Bd(i,j,:);
		h03 = writeToOutside(h03);
	end
	if s==-1
		for tIdx = 1:numtL
			if numtL >1
				tIdxAll = tIdx;
			else
				tIdxAll = 1:numtKernel;
			end
			for xi_idx = 1:size(xi_disc,1)
				xi = xi_disc(xi_idx,tIdx);
				eta = eta_disc(:,tIdx);
				% lower border
				eta_l_idx = find(acc(eta,1/min_dist)>=acc(kern_elem.eta_l(xi_idx,tIdx),1/min_dist),1);
				% upper border
				eta_h_idx = find(acc(eta,1/min_dist)<=min(acc(xi,1/min_dist),acc(2*a(tIdx)-xi,1/min_dist)),1,'last'); % letzter Index im Ortsbereich
				for eta_idx = eta_l_idx:eta_h_idx
					if eta(eta_idx)>=0
						[~,~,z_fine,zeta_fine] = kern_elem.get_z(xi_disc(xi_idx,tIdx),eta(eta_idx),tIdx);
						h03(xi_idx,eta_idx,tIdxAll) = repmat(-1/4/s*(interp1(zdisc,a_i_disc(:,tIdx),z_fine)-interp1(zdisc,a_j_disc(:,tIdx),zeta_fine))...
							.*ppide.ctrl_pars.g_strich{i,j}(eta(eta_idx)),1,1,numel(tIdxAll));
					end
				end
			end
		end
	end
end

h0 = h01 + h02 + h03 + h04;


end

