function varargout = get_backstep_adjoint_function(kappa,f_til,zdisc,dz_f_til,dzeta_kappa)
% f = GET_BACKSTEP_ADJOINT_FUNCTION(kappa,f_til, zdisc)
%     computes the adjoint backstepping function
%         f(zeta) = f_til(z)-int_z^1 f_til(zeta)kappa(zeta,z)d zeta
%     which is adjoint in the sense of
%         int_0^1 f(z)g(z)dz = int_0^1 f_til(z)g_til(z)dz , 
%     in the case that f_til = Tc(f) is the backstepping transformed
%     function of f.
%
% [f dz_f] = GET_BACKSTEP_ADJOINT_FUNCTION(kappa,f_til, zdisc, dz_f_til, dzeta_kappa)
%     also computes the spatial derivative of the adjoint backstepping
%     function. Therefor, the spatial derivative of the kernel in the
%     second variable is needed.
%
%   INPUT PARAMETRS:
%     MATRIX       kappa       : discretized kernel kappa(z,zeta,i,j)
%     MATRIX       f_til       : discretized matrix function f, evaluated at
%                                discretisation points. Contains matrices
%                                for each discretisation points.
%     VECTOR       zdisc       : underlying spatial discretisation
%
%   OPTIONAL:
%     VECTOR       dz_f_til    : spatial derivative of the function
%     MATRIX       dzeta_kappa : spatial derivative of the kernel in the
%                                second variable
%
%   OUTPUT PARAMETRS:
%     VECTOR       f           : adjoint function
%
%   OPTIONAL:
%     VECTOR       dz_f    : spatial derivative of the adjoint function
 
% history:
%   created on 14.01.2016 by Simon Kerschbaum
%   modified on 21.06.2018 by Simon Kerschbaum
%     - implemented solution for kernel matrices and f_til matrices

% benötigte Unterprogramme:
% - trapz_fast.m

%%%%%%%%%%%%%%%%%%%%%
% 0. Voraussetzungen:
%%%%%%%%%%%%%%%%%%%%%

if nargin == 5
	if length(dz_f_til) ~= length(zdisc)
		error('dz_f_til must be discretized like zdisc')
	end
	if size(dzeta_kappa) ~= size(kappa)
		error('size(dzeta_kappa) must be equal size(kappa)')
	end
end


if size(kappa,1)~=size(kappa,2) || size(kappa,1)~= length(f_til)
	error('Kappa must be square and of same discretisation as f_til!')
end

%%%%%%%%%%%%%%%%%%%%%
% 1. Transformation:
%%%%%%%%%%%%%%%%%%%%%
n =  size(kappa,3);
ndisc = size(kappa,1);
numRows = size(f_til,2);
% matrix product for each point z,zeta
f = zeros(ndisc,numRows,n);
for z_idx = 1:ndisc
	temp_prod = zeros(ndisc,numRows,n);
	for zeta_idx=z_idx:ndisc
		temp_prod(zeta_idx,:,:) = reshape(f_til(zeta_idx,:,:),[numRows n])*reshape(kappa(zeta_idx,z_idx,:,:),[n n]);
	end
	f(z_idx,:,:) = f_til(z_idx,:,:) - trapz(zdisc(z_idx:end),temp_prod(z_idx:end,:,:),1);
end
varargout{1}=f;

if nargin == 5
	warning('Implementation not corrected for calculating derivative!')
%%%%%%%%%%%%%%%%%%%%%
% 2. Transformation:
%%%%%%%%%%%%%%%%%%%%%
	for z_idx = 1:length(zdisc)
		for zeta_idx=z_idx:length(zdisc)
			temp_prod(:,zeta_idx) = dzeta_kappa(zeta_idx,z_idx)*f_til(:,zeta_idx);
		end
		dz_f(:,z_idx) = dz_f_til(:,z_idx)+ f_til(z_idx)*kappa(z_idx,z_idx) - trapz_fast(zdisc(z_idx:end),temp_prod(:,z_idx:end));
	end
	varargout{2} = dz_f;
end
