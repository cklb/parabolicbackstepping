function [  KvHat,KvQPlant, Q, QTil, D] = robustDecouplingEquations(system,sigMod,kernel)
%robustDecouplingEquations Solve decoupling equations for robust output regulation
%   [ KvHat,KvQPlant, Q, Q_til, D] = robustDecouplingEquations(system,sigMod,kernel) solves 
%        the decoupling equations belonging to system system with respect 
%        to the signal model sigMod and the backstepping kernel kernel.
% 
%   INPUT PARAMETRS:
%     PPIDE_SYS      system   : plant
%     signalModel    sigMod   : signal model of the reference and
%                               disturbance
%     ppide_kernel   kernel   : kernel of the backstepping transformation
% 
%   OUTPUT PARAMETERS:
%     MATRIX          KvHat    : feedback gain of internal model states
%     MATRIX          KvQPlant : Distributed feedback gain of system state.
%                                u_IM = -KvHat vHat + int_0^1 KvHatQ(z)
%                                x(z) dz. Is approximated to be used like
%                                the backstepping controller in the form  
%                                u_IM = -KvHat vHat + KvQPlant*x
%     DOUBLE ARRAY    Q        : (z_idx,i,j) transformed solution of the
%                                 decoupling equations Q(z). 
%                                 z_idx is the index of the spatial grid. 
%     DOUBLE ARRAY    QTil     : (z_idx,i,j) solution of the
%                                 decoupling equations QTil(z). 
%                                 z_idx is the index of the spatial grid. 
%

% created on 21.06.2018 by Simon Kerschbaum


import misc.*
import numeric.*
import dps.parabolic.system.*
import dps.parabolic.control.backstepping.*

% 1. Perform required caluclations and transformations:
ndiscPlant = system.ndisc; 
zdiscPlant = linspace(0,1,ndiscPlant);
zdiscPars = linspace(0,1,system.ndiscPars); % Aufloesung der Parameter
ndiscPars = system.ndiscPars; % Aufloesung der Parameter
zdiscRegEq = system.ctrl_pars.zdiscRegEq;    % Aufloesung der Regulator Equations
ndiscRegEq = length(zdiscRegEq);
zdiscKernel = kernel.zdisc;                  % Aufloesung des Kerns
ndiscKernel = length(zdiscKernel);
n = system.n;
m = system.c_op.m;
nv = sigMod.nv; 
IntPR = interp_mat(zdiscPars,zdiscRegEq);
IntKR = interp_mat(zdiscKernel,zdiscRegEq);

% mu: may be a constant matrix, an anonymous matrix function or a
% discretized matrix function!
if ndiscRegEq ~= n
	if isa(system.ctrl_pars.mu,'function_handle') % function
		muSys = eval_pointwise(system.ctrl_pars.mu,zdiscRegEq);
	elseif isequal(size(system.ctrl_pars.mu,1),ndiscPars) % is discretized
		muSys = zeros(ndiscRegEq,n,n);
		for i=1:n
			for j=1:n
				muSys(:,i,j) = IntPR*system.ctrl_pars.mu(:,i,j);
			end
		end
	elseif isequal(size(system.ctrl_pars.mu,1),n) % constant matrix, two dimensions
		muSys(1:ndiscRegEq,:,:) = repmat(shiftdim(system.ctrl_pars.mu,-1),ndiscRegEq,1,1);
	else % discretized matrix
		error('dimensions of mu not correct!')
	end
else
	error('ndisc=n is a very bad choice!')
end

a0_til = zeros(ndiscRegEq,n,n);
for i=1:n
	for j=1:n
% 		dzeta_K_z_0(:,i,j) = IntKR*kernel.value{i,j}.dzeta_K_z_0.';
		a0_til(:,i,j) =IntKR*kernel.a0_til(:,i,j);
	end
end
KI = kernel.KI;
if n>1
	lInterp = griddedInterpolant({zdiscPars,1:n,1:n},system.l_disc);
	l_disc = lInterp({zdiscRegEq,1:n,1:n});
else
	l_disc = interp1(zdiscPars,system.l_disc,zdiscRegEq(:));
end
L = l_disc;
M = zeros(ndiscRegEq,n,n);
for z_idx=1:ndiscRegEq   % -lambda * M^T * lambda^-1
	M(z_idx,:,:) = -sd(l_disc(z_idx,:,:))*permute(muSys(z_idx,:,:),[3 2 1])/sd(l_disc(z_idx,:,:));
end
S = -sigMod.S.';
A0 = zeros(ndiscRegEq,n,n);
By = system.ctrl_pars.By;
BccDisc = zeros(ndiscRegEq,n,n);
for z_idx=1:ndiscRegEq
	BccDisc(z_idx,:,:) = -sd(a0_til(z_idx,:,:)).'/sd(l_disc(z_idx,:,:));
end
Bcc = @(z) reshape(interp1(zdiscRegEq,BccDisc,z),[n n]);
B1 = dps.output_operator('c_d0',eye(n),'c_c',Bcc);
B2 = dps.output_operator('c_d1',eye(n));
cDist = system.c_op.c_c;
cM = system.c_op.c_m;
zM = system.c_op.z_m;
zMIdx = get_idx(zM,zdiscKernel);
zMRegIdx = get_idx(zM,zdiscRegEq);
cDisc = misc.eval_pointwise(cDist,zdiscKernel);

% create transformed distributed output functino c_til:
% 1. transform distributed output to BS coordinates: cBreve1
cBreve1 = get_backstep_adjoint_function(-KI,cDisc,zdiscKernel);
% 2. + Cm*KI = cBreve
cBreve2 = zeros(ndiscKernel,m,n);
for z_idx=1:zMIdx
	cBreve2(z_idx,:,:) = cM*reshape(KI(zMIdx,z_idx,:,:),[n n]);
end
% resampling must be done here, because cBreve2 and the rest need different
% resampling!
cBreve1Res = interp1(zdiscKernel,cBreve1,zdiscRegEq(:));
cBreve2Res = zeros(ndiscRegEq,m,n);
if zMIdx>1
	cBreve2Res(1:zMRegIdx,:,:) = interp1(zdiscKernel,cBreve2(:,:,:),zdiscRegEq(1:zMRegIdx));
else
	cBreve2Res(1,:,:) = cBreve2(1,:,:);
end

% cBreveRes = zeros(ndiscRegEq,n,n);
% for z_idx=1:ndiscRegEq
% 	if z_idx <= zMIdx
% 		cBreveRes(z_idx,:,:) = cBreve1Res(z_idx,:,:) + cBreve2Res(z_idx,:,:);
		cBreveRes = cBreve1Res+ cBreve2Res;
% 	else
% 		cBreveRes(z_idx,:,:) = reshape(cBreve1(z_idx,:,:),[n n]); % KI is only taken until zi. So it can be seen as zero for greater z!
% 	end
% end
% add Cd1K_I(1,z): cTil
cTil1Ker = zeros(ndiscKernel,m,n);
for z_idx=1:ndiscKernel
	cTil1Ker(z_idx,:,:) = system.c_op.c_1*reshape(KI(end,z_idx,:,:),[n,n]);
end
cTil1 = interp1(zdiscKernel,cTil1Ker,zdiscRegEq(:));
cTil = cTil1 + cBreveRes;

% Problem: 0 darf nicht mit rein interpoliert werden! Nur von 0 bis zi
% interpolieren!
% Resample to ndiscRegEq
% cTilTemp = zeros(ndiscRegEq,n,n);
% if zMRegIdx >1 % special case zm=0 is when no zm is passed!
% 	cTilTemp(1:zMRegIdx,:,:) = interp1(zdiscKernel(1:zMIdx),cTilKer(1:zMIdx,:,:),zdiscRegEq(1:zMRegIdx)); 
% else
% 	cTilTemp(1,:,:) = cTilKer(1,:,:); 
% end

% Add the approximated diracs for point measurement in BS coordinates
dz = 1/(ndiscRegEq-1);
IMzm1 = misc.interp_mat(zdiscRegEq,zM)./dz; % realized by an interpolation matrix
% scaled by the spatial step such that trapz(0,1,IMzm1)=1! Discrete
% approximation of a delta function!
IMzm = zeros(ndiscRegEq,n,n);
for i=1:n
	IMzm(:,i,i) = IMzm1.'; % select point of each state
end
cmTil = zeros(ndiscRegEq,m,n);
for z_idx=1:ndiscRegEq
	cmTil(z_idx,:,:) = cM*reshape(IMzm(z_idx,:,:),[n n]);
end

cTil = cTil + cmTil ;
% calculate each Ri
R = zeros(ndiscRegEq,m*nv,n);
STil = zeros(m*nv,m*nv);
for i=1:m
	H = zeros(ndiscRegEq,n,nv);
	try
		byiT = By((i-1)*nv+1:i*nv,i).';
	catch 
		here=1;
	end
	ciTil = permute(cTil(:,i,:),[1 3 2]);
	c0i = system.c_op.c_0(i,:).';
	c1i = system.c_op.c_1(i,:).';
	for z_idx=1:ndiscRegEq
		H(z_idx,:,:) = sd(l_disc(z_idx,:,:))*sd(ciTil(z_idx,:))*byiT;
	end
	G1 = c0i*byiT;
	G2 = -c1i*byiT;
	
	RiT = misc.solveGenericBVP(L,M,S,A0,H,B1,G1,B2,G2,zdiscRegEq);
	% RiT passt! QTil nicht!
	Ri = permute(RiT,[1 3 2]);
	R(:,(i-1)*nv+1:i*nv,:) = Ri;
	STil((i-1)*nv+1:i*nv,(i-1)*nv+1:i*nv) = sigMod.S;
end
QTil = zeros(ndiscRegEq,m*nv,n);
for z_idx=1:ndiscRegEq
	QTil(z_idx,:,:) = sd(R(z_idx,:,:))/sd(l_disc(z_idx,:,:));
end
QTilRes = interp1(zdiscRegEq,QTil,zdiscKernel(:));
K = kernel.get_K;

QRes = get_backstep_adjoint_function(K,QTilRes,zdiscKernel);
Q = interp1(zdiscKernel,QRes,zdiscRegEq(:));

%calculate determinant/rank of numerator of transfer matrix at mu:
opts = odeset('RelTol',1e-6,'AbsTol',1e-6,'vectorized','on');
D = zeros(1,length(sigMod.blockLength));
T1 = [eye(n); zeros(n,n)]; % selection matrix of first solution state
T2 = [zeros(n,n); eye(n,n)]; % selection matrix of second solution state
for jBlock=1:length(sigMod.blockLength)
	gamma_for_vec(1:ndiscRegEq,1:(2*n)^2,1:(2*n)^2)=0; % preallocate
	gamma(1:ndiscRegEq,1:2*n,1:2*n) = 0; %preallocate
	Psi_mu_vec(1:ndiscRegEq,1:ndiscRegEq,1:(2*n)^2)=0; % preallocate
	Psi_mu(1:ndiscRegEq,1:ndiscRegEq,1:2*n,1:2*n) = 0; %preallocate
	try
		mu = sigMod.lambda(sum(sigMod.blockLength(1:jBlock-1))+1);
	catch err
		mythrow(err)
	end
	for z_idx = 1:ndiscRegEq
		% currently considered eigenvalue
		try
		gamma(z_idx,:,:) = ...
			[zeros(n,n)                                                               eye(n);
			 reshape(l_disc(z_idx,:,:),[n n])\(reshape(muSys(z_idx,:,:),[n n])+mu*eye(n)) zeros(n,n)];
		catch err
			mythrow(err)
		end
		for i=1:2*n %number of columns in Psi
			% create diagonal matrix of gammas
			gamma_for_vec(z_idx,(i-1)*2*n+1:i*2*n,(i-1)*2*n+1:i*2*n) = gamma(z_idx,:,:);
		end
	end
	
	%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
	% calculate fundamental matrix:
	% as the solution of 
	%     dz Psi_mu(z,zeta) = gamma(z) Psi_mu(z,zeta) z in (0,1),zeta
	%     in (0,z)
	%     Psi_mu(zeta,zeta) = I
% 	if n>1
		gammaInterp = griddedInterpolant({zdiscRegEq,1:(2*n)^2,1:(2*n)^2},gamma_for_vec);
		gamma_gen = @(z) squeeze(gammaInterp({z(:),1:(2*n)^2,1:(2*n)^2}));
% 	else
% 		gammaInterp = griddedInterpolant({zdiscRegEq},gamma_for_vec);
% 		gamma_gen = @(z) squeeze(gammaInterp({z(:)}));
% 	end		
	Psi_0 = eye(2*n);
	Psi_vec_0 = Psi_0(:); % vectorized
	% Problem: matlab-solver only for vectors.
	% --> Matrix must be vectorized
	% --> Gamma blockdiagonalized
	for zeta_idx =1:ndiscRegEq-1
		% starting value for z
		z0 = zdiscRegEq(zeta_idx); 
		f=@(z,Psi_vec) gamma_gen(z)*Psi_vec;
		[z,Psi_z_zeta_vec]=ode15s(f,[z0 1],Psi_vec_0,opts);
% 		if n>1
			PsiInterp = griddedInterpolant({z,1:(2*n)^2},Psi_z_zeta_vec);
			Psi_z_zeta_vec_res = PsiInterp({zdiscRegEq(zeta_idx:ndiscRegEq),1:(2*n)^2});
% 		else
% 			PsiInterp = griddedInterpolant({z},Psi_z_zeta_vec);
% 			Psi_z_zeta_vec_res = PsiInterp({zdiscRegEq(zeta_idx:ndiscRegEq)});
% 		end
% 			Psi_z_zeta_vec_res = interp1(z,Psi_z_zeta_vec,zdiscRegEq(zeta_idx:ndiscRegEq).');
		Psi_mu_vec(zeta_idx:ndiscRegEq,zeta_idx,:) = Psi_z_zeta_vec_res;
	end
	Psi_mu_vec(ndiscRegEq,ndiscRegEq,:) = Psi_vec_0; %#ok<AGROW>
	% put vector in matrix form:
	for j=1:2*n
		%      z,zeta,i,j            z,zeta,i
		Psi_mu(:,:,:,j) = Psi_mu_vec(:,:,(j-1)*2*n+1:j*2*n);
	end
	% fundamental matrix ready.
	%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
	
	prod1 = zeros(ndiscRegEq,m,n);
	prod2 = zeros(ndiscRegEq,m,n);
	prod3 = zeros(ndiscRegEq,m,n);
	% calculated fundamental matrix is Phi of the paper! so no transpose is
	% used and zeta and z is exchanged!
	for z_idx=1:ndiscRegEq
		prod1(z_idx,:,:) = sd(cTil(z_idx,:,:))*T1.'*reshape(Psi_mu(z_idx,1,:,:),[2*n 2*n])*T1;
		prod2(z_idx,:,:) =  system.c_op.c_1*T1.'*reshape(Psi_mu(end,z_idx,:,:),[2*n 2*n])*T2/sd(l_disc(z_idx,:,:))*sd(a0_til(z_idx,:,:));
		tempProd = zeros(ndiscRegEq,m,n);
		for zeta_idx =z_idx:ndiscRegEq
			tempProd(zeta_idx,:,:) = sd(cTil(zeta_idx,:,:))*T1.'*reshape(Psi_mu(zeta_idx,z_idx,:,:),[2*n 2*n])...
				*T2;
		end
		prod3(z_idx,:,:) = sd(trapz(zdiscRegEq(z_idx:end),tempProd(z_idx:end,:,:),1))/sd(l_disc(z_idx,:,:))*sd(a0_til(z_idx,:,:));
	end
	D(jBlock) =  rank(system.c_op.c_0 +  system.c_op.c_1*T1.'*reshape(Psi_mu(end,1,:,:),[2*n 2*n])*T1...
		+ sd(trapz(zdiscRegEq,prod1,1)) + sd(trapz(zdiscRegEq,prod2,1)) + sd(trapz(zdiscRegEq,prod3,1)));
	if D(jBlock) < m
		error(['rank(N(mu))=' num2str(D(jBlock)) '< m= ' num2str(m) '. (STil,Bv) not controllable!'])
	end
end


% calculate KvHat:
Bv = -sd(QTil(end,:,:))*sd(l_disc(end,:,:));

% KvHat = misc.placeLQR(STil,Bv,system.ctrl_pars.eigInt);
KvHat = place(STil,Bv,system.ctrl_pars.eigInt);
MM = get_lin_mass_matrix(zdiscPlant);
KvQ = zeros(ndiscRegEq,n,n);
for z_idx=1:ndiscRegEq
	KvQ(z_idx,:,:) = KvHat*sd(Q(z_idx,:,:));
end
KvQRes = interp1(zdiscRegEq,KvQ,zdiscPlant(:));
KvQPlant = zeros(n,n*ndiscPlant);
for i=1:n
	for j=1:n
		KvQPlant(i,(j-1)*ndiscPlant+1:j*ndiscPlant) = reshape(KvQRes(:,i,j),[1 ndiscPlant])*MM;
	end
end

% L�sung der Entkopplungsgleichungen numerisch verifizieren:
% grad_op = numeric.get_grad_op(zdiscRegEq);
% lhs = zeros(ndiscRegEq,n*nv,n);
% R_zz = zeros(ndiscRegEq,n*nv,n);
% for i=1:n*nv
% 	for j=1:n
% 		R_zz(:,i,j) = grad_op*grad_op*R(:,i,j);
% 	end
% end
% for z_idx=1:ndiscRegEq
% 	lhs(z_idx,:,:) = STil*sd(R(z_idx,:,:))/sd(L(z_idx,:,:))...
% 		+sd(R(z_idx,:,:))/sd(L(z_idx,:,:))*sd(muSys(z_idx,:,:))...
% 		+ By*sd(cTil(z_idx,:,:));
% end
% rhs = R_zz;
% err = lhs-rhs;
% figure('Name','Fehler robuste Entkopplungsgleichungen')
% hold on
% for i=1:n*nv
% 	for j=1:n
% 		plot(zdiscRegEq,err(:,i,j),'DisplayName',[num2str(i) num2str(j)])
% 	end
% end
% legend('-DynamicLegend')






end % function

