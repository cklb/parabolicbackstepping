function GH = init_kernel_elementsFolding(~,ppide,zdisc)
%INIT_KERNEL_ELEMENTS initialize kernel elements for ppide system

% created on 14.09.2016 by Simon Kerschbaum
% modified on 03.04.2017 by SK:
%  - implementation of PIDEs with spatially varying diffusion
import misc.*
import numeric.*
import dps.parabolic.control.backstepping.*
ndisc = length(zdisc);
ndiscPars = ppide.ndiscPars;
zdiscPars = linspace(0,1,ndiscPars);
ndiscTime = length(ppide.ctrl_pars.tDiscKernel);

%%% Interpolate system parameters to kernel resolution 
pars.lambda_disc = translate_to_grid(ppide.l_disc,zdisc);
pars.lambda_t_disc = translate_to_grid(ppide.l_t,zdisc);
pars.lambda_diff_disc = translate_to_grid(ppide.l_diff_disc,zdisc);
pars.lambda_diff2_disc = translate_to_grid(ppide.l_diff2_disc,zdisc);
pars.c_disc = translate_to_grid(ppide.c,zdisc);
pars.c_diff_disc = translate_to_grid(ppide.c_diff,zdisc);
pars.a0_disc = translate_to_grid(ppide.a_0_disc,zdisc(:));
pars.A_disc = translate_to_grid(ppide.a_disc,zdisc(:));


% all time-dependent coefficients need to be resampled in time-direction
varlist = {'lambda_disc','lambda_t_disc','lambda_diff_disc','lambda_diff2_disc',...
	'c_disc','c_diff_disc','A_disc'};
dims = 4*ones(length(varlist),1);
tDisc = ppide.ctrl_pars.tDiscKernel;
timeDependent=0;
for i=1:length(varlist)
	dim = dims(i);
	if size(pars.(varlist{i}),dim)>1
		pars.(varlist{i}) = permute(misc.translate_to_grid(permute(pars.(varlist{i}),[dim 1:dim-1]),tDisc),[2:dim 1]);
		timeDependent=1;
	end
end

n = size(pars.lambda_disc,2);


% Hartwig edited
if ndisc~= n
	if isa(ppide.ctrl_pars.mu,'function_handle') % function
		if nargin(ppide.ctrl_pars.mu)==1
			muHelp = eval_pointwise(ppide.ctrl_pars.mu,zdisc);
			muHelpPars = eval_pointwise(ppide.ctrl_pars.mu,zdiscPars);
		else
			muHelp = zeros(ndisc,n,n,length(tDisc));
			muHelpPars = zeros(ndiscPars,n,n,ndiscPars);
			for z_idx=1:ndisc
				muHelp(z_idx,:,:,:) = permute((eval_pointwise(@(z)ppide.ctrl_pars.mu(zdisc(z_idx),z),tDisc)),[2 3 1]);
			end
			for z_idx=1:ndiscPars
				muHelpPars(z_idx,:,:,:) = permute((eval_pointwise(@(z)ppide.ctrl_pars.mu(zdiscPars(z_idx),z),linspace(0,1,ndiscPars))),[2 3 1]);
			end
		end
	elseif size(ppide.ctrl_pars.mu,1) > n % is discretized
		if length(size(ppide.ctrl_pars.mu))>3
			muInterpolant = interpolant({linspace(0,1,size(ppide.ctrl_pars.mu,1)),1:n,1:n,tDisc}, ppide.ctrl_pars.mu);
			muHelp = muInterpolant.evaluate(zdisc,1:n,1:n,tDisc);
			muHelpPars = muInterpolant.evaluate(zdiscPars,1:n,1:n,linspace(0,1,ndiscPars));
		else
			muInterpolant = interpolant({linspace(0,1,size(ppide.ctrl_pars.mu,1)),1:n,1:n}, ppide.ctrl_pars.mu);
			muHelp = muInterpolant.evaluate(zdisc,1:n,1:n);
			muHelpPars = muInterpolant.evaluate(zdiscPars,1:n,1:n);
		end
		%muHelp = ppide.ctrl_pars.mu;
	elseif isequal(size(ppide.ctrl_pars.mu,1),n) % constant matrix, two dimensions
		muHelp(1:ndisc,:,:) = repmat(shiftdim(ppide.ctrl_pars.mu,-1),ndisc,1,1);
		muHelpPars(1:ndiscPars,:,:) = repmat(shiftdim(ppide.ctrl_pars.mu,-1),ndiscPars,1,1);
	else % discretized matrix
		error('dimensions of mu not correct!')
	end
else
	error('ndisc=n is a very bad choice!')
end
if size(muHelp,4) > 1
	timeDependent = 1;
end
for i=1:n
	for j=1:n
		if any(any(muHelp(:,i,j,:))) && any(any(pars.lambda_disc(:,i,i,:)>pars.lambda_disc(:,j,j,:)))
			error('mu must only have elements for lambda_i \leq lambda_j!')
		end
	end
end


% time dimension of the kernel is discretized with ndiscTime, if one of the parameters is
% time-dependent
if timeDependent
	numtL = size(pars.lambda_disc,4); % number of timesteps in lambda
	numtKernel = ndiscTime; % number of timesteps in kernel
else
	numtL = 1;
	numtKernel = 1;
end

% numerical parameters:
accur = 1/ppide.ctrl_pars.min_z_res; % accuracy towards all values are rounded
min_xi_diff = ppide.ctrl_pars.min_xi_diff; % minimum distance that points inside the xi/eta domain have to vary
maxNumXi = ppide.ctrl_pars.maxNumXi;
min_eta_diff = ppide.ctrl_pars.min_eta_diff; % in sparse grid
min_xi_diff_border = ppide.ctrl_pars.min_xi_diff_border; % minimum distance that points wich arise from a boundary have to vary
min_eta_diff_border = ppide.ctrl_pars.min_eta_diff_border; % in sparse grid
min_dist = ppide.ctrl_pars.min_dist; % distance below which points are considered as equal. Should be higher as min_z_res
% (needed if points are the result of a math operation, e.g. 2*a-xi)

GH{n,n} = 0; %preallocate

z0 = ppide.ctrl_pars.foldingPoint;
if isscalar(z0) % scalar
	z0 = ones(1,n/2)*z0;
end
z0Left = zeros(n);
zRight = zeros(n,ndiscPars);
for i=1:n/2
	zRight(i,:) = linspace(z0(i),1,ppide.ndiscPars);
	dZ = diff(zRight(i,:));
	z0Left(i) = z0(i)-dZ(1);
end
for i=1:n
	for j=1:n
		%%%%%%%%%% create grid %%%%%%%%%
		xi = zeros(sum(1:(ndisc)),numtL); % preallocate 
		eta = zeros(sum(1:(ndisc)),numtL);% at first, all possible combinations are loaded
		xiMat = zeros(ndisc,ndisc,numtL);
		etaMat = zeros(ndisc,ndisc,numtL);
		elementPars.lambda_i = reshape(pars.lambda_disc(:,i,i,:),ndisc,[]); % reshape is needed if there is a time-dimension
		elementPars.lambda_j = reshape(pars.lambda_disc(:,j,j,:),ndisc,[]);
		elementPars.lambda_i_t = reshape(pars.lambda_t_disc(:,i,i,:),ndisc,[]); % reshape is needed if there is a time-dimension
		elementPars.lambda_j_t = reshape(pars.lambda_t_disc(:,j,j,:),ndisc,[]);
		elementPars.c_i = reshape(pars.c_disc(:,i,i,:),ndisc,[]);
		elementPars.c_j = reshape(pars.c_disc(:,j,j,:),ndisc,[]);
		elementPars.lambda_i_diff = reshape(pars.lambda_diff_disc(:,i,i,:),ndisc,[]);
		elementPars.lambda_j_diff = reshape(pars.lambda_diff_disc(:,j,j,:),ndisc,[]);
		elementPars.lambda_i_diff2 = reshape(pars.lambda_diff2_disc(:,i,i,:),ndisc,[]);
		elementPars.lambda_j_diff2 = reshape(pars.lambda_diff2_disc(:,j,j,:),ndisc,[]);
		elementPars.c_i_diff = reshape(pars.c_diff_disc(:,i,i,:),ndisc,[]);
		elementPars.c_j_diff = reshape(pars.c_diff_disc(:,j,j,:),ndisc,[]);
		
		k=1; % running index for xi, eta. 
		% important: compute phi with high-res because value of integral is changed!
		phi_iFine = cumtrapz(zdiscPars,(1./sqrt(reshape(ppide.l(:,i,i,:),ndiscPars,[]))),1);
		phi_iFineInterp = numeric.interpolant({zdiscPars},phi_iFine);
% 		phi_i = cumtrapz(zdisc,(1./sqrt(elementPars.lambda_i)),1);
		phi_i = phi_iFineInterp.eval({zdisc});
% 		[~,phi_i_interp] = translate_to_grid(phi_i,{zdisc,1:numtL},'zdim',2);
% 		[~,phi_i_interpNN] = translate_to_grid(phi_i,{zdisc,1:numtL},'zdim',2,'method','nearest');
% 		phi_j = cumtrapz(zdisc,(1./sqrt(elementPars.lambda_j)),1);
		phi_jFine = cumtrapz(zdiscPars,(1./sqrt(reshape(ppide.l(:,j,j,:),ndiscPars,[]))),1);
		phi_jFineInterp = numeric.interpolant({zdiscPars},phi_jFine);
		phi_j = phi_jFineInterp.eval({zdisc});
% 		[~,phi_j_interp] = translate_to_grid(phi_j,{zdisc,1:numtL},'zdim',2);
% 		[~,phi_j_interpNN] = translate_to_grid(phi_j,{zdisc,1:numtL},'zdim',2,'method','nearest');

% 		if ~ismatrix(ppide.ctrl_pars.mu) % more than 2 dimensions
% 			error('The implementation of of the kernel is still restricted to constant mu!')
% 		end
% 		mu_ij = ppide.ctrl_pars.mu(i,j);
		% Hartwig edited
% 		if ndisc~= n
% 			if isa(ppide.ctrl_pars.mu,'function_handle') % function
% 				muHelp = eval_pointwise(ppide.ctrl_pars.mu,zdisc);
% 			elseif isequal(size(ppide.ctrl_pars.mu,1),ndisc) % is discretized 
% 				muHelp = ppide.ctrl_pars.mu;
% 			elseif isequal(size(ppide.ctrl_pars.mu,1),n) % constant matrix, two dimensions
% 				muHelp(1:ndisc,:,:) = repmat(shiftdim(ppide.ctrl_pars.mu,-1),ndisc,1,1);
% 			else % discretized matrix
% 				error('dimensions of mu_o not correct!')
% 			end
% 		else
% 			error('ndisc=n is a very bad choice!')
% 		end
		% not redundant because tDisc is a vector even if mu is not time dependent!
		if size(muHelp,4) > 1
			mu_ij = reshape(muHelp(:,i,j,:),[ndisc,length(tDisc)]);
			mu_ijPars = reshape(muHelpPars(:,i,j,:),[ndiscPars,ndiscPars]);
		else
			mu_ij = reshape(muHelp(:,i,j),[ndisc,1]);
			mu_ijPars = reshape(muHelpPars(:,i,j),[ndiscPars,1]);
		end
		
		repvars = {'c_i_diff','c_j_diff','c_i','c_j'};
		% convection is repeated to have same time resolution as lambda
		for varIdx=1:length(repvars)
			if size(elementPars.(repvars{varIdx}),2)<numtL
				elementPars.(repvars{varIdx}) = repmat(elementPars.(repvars{varIdx}),1,numtL);
			end
		end
		repvars2 = {'lambda_i','lambda_j','lambda_i_t','lambda_j_t','lambda_i_diff',...
			'lambda_j_diff','lambda_i_diff2','lambda_j_diff2'};
		% diffusion is repeated to have same time resolution as c_i
		for varIdx=1:length(repvars2)
			if size(elementPars.(repvars2{varIdx}),2)<size(elementPars.c_i,2)
				warning('When lambda is constant and convection is time dependent, the implementation is inefficient, because lambda is treated as time dependent!')
				elementPars.(repvars2{varIdx}) = repmat(elementPars.(repvars2{varIdx}),1,numtL);
			end
		end
		
		for z_idx = 1:length(zdisc)
			for zeta_idx = 1:z_idx	
				if elementPars.lambda_i(1)>= elementPars.lambda_j(1) % still no different check is performed! 
					% This check should be drastically improved!
					xi(k,:) = phi_i(z_idx,:) + phi_j(zeta_idx,:); 
					eta(k,:) = phi_i(z_idx,:) - phi_j(zeta_idx,:);
				else
					xi(k,:) = phi_i(end,:)+phi_j(end,:)-phi_i(z_idx,:)-phi_j(zeta_idx,:);
					eta(k,:) = -phi_i(end,:)+phi_j(end,:)+phi_i(z_idx,:)-phi_j(zeta_idx,:);
				end
				xiMat(z_idx,zeta_idx,1:numtL) = xi(k,:);
				etaMat(z_idx,zeta_idx,1:numtL) = eta(k,:);
				k=k+1;
			end
			% TODO: keine For-Schleife noetig!
			for zeta_idx = z_idx+1:length(zdisc)
				xiMat(z_idx,zeta_idx,1:numtL) = xiMat(z_idx,z_idx,1:numtL); % take the diagonal value into the not defined area to prevent interpolation errors
				etaMat(z_idx,zeta_idx,1:numtL) = etaMat(z_idx,z_idx,1:numtL); % take the diagonal value into the not defined area to prevent interpolation errors
			end
		end
		xiMatInterp = numeric.interpolant({zdisc,zdisc,1:numtL},xiMat);
		etaMatInterp = numeric.interpolant({zdisc,zdisc,1:numtL},etaMat);
		% remove duplicate entries after rounding to accur, has to be
		% higher resolution than min_dist!
		% The sort command is neccessary beacause the unique command does not sort each column, 
		% when 'rows' is passed.
		xi_fine = sort(unique(acc(xi,accur),'rows')); 
		eta_fine = sort(unique(acc(eta,accur),'rows'));
		% remove all entries, which are closer than min_dist, but no
		% rounding!
		xi_fine = unique_accur(xi_fine,min_dist); 
		eta_fine = unique_accur(eta_fine,min_dist); 
		% In case of time-dependent coordinates, the different columns of xi,eta may contain
		% duplicates, because a row can only be removed, if all the elements' deviation is
		% small.
		% The following function removes the duplicates.
		xi_fine = distributeDuplicates(xi_fine); 
		eta_fine = distributeDuplicates(eta_fine);
		
		% create kernel_element. The sparse grid is created by the
		% constructor
		GH{i,j} = dps.parabolic.control.backstepping.ppide_kernel_element('lambda_i',elementPars.lambda_i,...
									  'lambda_j',elementPars.lambda_j,...
									  'lambda_iFine',reshape(ppide.l(:,i,i,:),ndiscPars,[]),...
									  'lambda_jFine',reshape(ppide.l(:,j,j,:),ndiscPars,[]),...
									  'zdiscFine',linspace(0,1,ndiscPars),...
									  'c_i',elementPars.c_i,...
									  'c_j',elementPars.c_j,...
									  'c_i_diff',elementPars.c_i_diff,...
									  'c_j_diff',elementPars.c_j_diff,...
									  'lambda_i_t',elementPars.lambda_i_t,...
									  'lambda_j_t',elementPars.lambda_j_t,...
									  'lambda_i_diff',elementPars.lambda_i_diff,...
									  'lambda_j_diff',elementPars.lambda_j_diff,...
									  'lambda_i_diff2',elementPars.lambda_i_diff2,...
									  'lambda_j_diff2',elementPars.lambda_j_diff2,...
									  'i',i,'j',j,...
									  'xi_fine',xi_fine,'eta_fine',eta_fine,...
									  'xiMat',xiMat,...
									  'etaMat',etaMat,...
									  'xiMatInterp',xiMatInterp,...
									  'etaMatInterp',etaMatInterp,...
									  'zdisc',zdisc,...
									  'min_xi_diff',min_xi_diff,...
									  'maxNumXi',maxNumXi,...
									  'min_eta_diff',min_eta_diff,...
									  'min_xi_diff_border',min_xi_diff_border,...
									  'min_eta_diff_border',min_eta_diff_border,...
									  'accur',accur,...
									  'min_dist',min_dist,...
									  'eliminate_convection',ppide.ctrl_pars.eliminate_convection);	
		xi = GH{i,j}.xi; % get the sparse grid
		eta = GH{i,j}.eta; 
		
		parlist = {'lambda_i','lambda_j','lambda_i_diff','lambda_i_diff2','lambda_j_diff',...
					'lambda_j_diff2','c_i','c_j','lambda_i_t','lambda_j_t'};
		
		% Store repeated values in time, wich are needed for multiple arguments
		elementParsTime = elementPars;
		for parIdx =1:length(parlist)
			if size(elementPars.(parlist{parIdx}),2)<numtKernel
				elementParsTime.(parlist{parIdx}) = repmat(elementPars.(parlist{parIdx}),1,numtKernel);
			end
		end
								
		%%%%%%%%%% abbreviations %%%%%%%%%
		obj = GH{i,j}; % short form
		% abbreviations from BC
		GH{i,j}.a_i = (-1/2*elementPars.lambda_i_diff + elementPars.c_i)./sqrt(elementPars.lambda_i);
		aidisc = GH{i,j}.a_i;
		GH{i,j}.a_iInterp = interpolant({zdisc,1:numtL},aidisc);
		a_iz_xietaTemp = GH{i,j}.a_iInterp.eval(GH{i,j}.zMat(:),1:numtL);
		GH{i,j}.aiZXieta = reshape(a_iz_xietaTemp,size(GH{i,j}.xi,1),size(GH{i,j}.eta,1),numtL);
		GH{i,j}.a_j = (1/2*elementPars.lambda_j_diff + elementPars.c_j)./sqrt(elementPars.lambda_j);
		ajdisc = GH{i,j}.a_j;
		GH{i,j}.a_jInterp = interpolant({zdisc,1:numtL},ajdisc);
		a_jzeta_xietaTemp = GH{i,j}.a_jInterp.eval(GH{i,j}.zetaMat(:),1:numtL);
		GH{i,j}.ajZetaXieta = reshape(a_jzeta_xietaTemp,size(GH{i,j}.xi,1),size(GH{i,j}.eta,1),numtL);
		% used in get_G_eta0 and for d
		GH{i,j}.b_j = elementPars.lambda_j_diff.*elementPars.c_j./elementPars.lambda_j...
			-elementPars.c_j_diff-(1-ppide.ctrl_pars.quasiStatic)*elementPars.lambda_j_t./elementPars.lambda_j;		
		% used for d:
		GH{i,j}.a_ij_til = ...
			-1/2*(...
				1/2*reshape(elementPars.lambda_i_diff2,[ndisc,1,numtL])...
				-reshape(elementPars.c_i_diff,[ndisc,1,numtL]))...
			+1/4*reshape(elementPars.lambda_i_diff,[ndisc,1,numtL])...
				./reshape(elementPars.lambda_i,[ndisc,1,numtL])....
				.*(1/2*reshape(elementPars.lambda_i_diff,[ndisc,1,numtL])...
				-reshape(elementPars.c_i,[ndisc,1,numtL]))...
			+1/2*(...
				1/2*reshape(elementPars.lambda_j_diff2,[1,ndisc,numtL])...
				+reshape(elementPars.c_j_diff,[1,ndisc,numtL]))...
			-1/4*reshape(elementPars.lambda_j_diff,[1,ndisc,numtL])...
				./reshape(elementPars.lambda_j,[1,ndisc,numtL])...
				.*(...
				1/2*reshape(elementPars.lambda_j_diff,[1,ndisc,numtL])...
				+reshape(elementPars.c_j,[1,ndisc,numtL]));
		aijInterp = numeric.interpolant({zdisc,zdisc,1:numtL},GH{i,j}.a_ij_til);
		aijXiEtaTemp = aijInterp.eval(repmat(GH{i,j}.zMat(:),numtL,1),repmat(GH{i,j}.zetaMat(:),numtL,1),...
			reshape(repmat(1:numtL,numel(GH{i,j}.zMat),1),[numtL*numel(GH{i,j}.zMat),1]));
		GH{i,j}.a_ij_tilXiEta = reshape(aijXiEtaTemp,size(GH{i,j}.xi,1),size(GH{i,j}.eta,1),numtL);
		if ppide.ctrl_pars.eliminate_convection
			error('For folding, eliminate_convection must be 0!')
		else
			% TODO: Schauen, ob/wo die r �behaupt noch verwendet werden!
			r{2} = sqrt(elementPars.lambda_i); % used in get_h0_ppides
% 			r{3} = sqrt(obj.lambda_j(1,:)).*reshape(Bd(j,j,:),1,size(Bd,3))...
% 					- obj.c_j(1,:)./sqrt(obj.lambda_j(1,:)); %used in use_F_ppides
			r{4} = sqrt(elementPars.lambda_j(1,:)); % used in init_kernel_elements
			A_ij_disc = permute(pars.A_disc(:,i,j,:),[1 4 2 3]);
			% used in get_h0_ppides:
			% TODO: Kann noch deutlich verbessert werden! Interpolant sollte nicht �ber
			% sparse-diskretisiertem A erzeugt werden!

			r{6} = elementPars.lambda_j.* sqrt(elementPars.lambda_i)./(elementPars.lambda_j-elementPars.lambda_i).* (A_ij_disc+mu_ij);
			r6fine = ppide.l(:,j,j) .* sqrt(ppide.l(:,i,i)) ./ (ppide.l(:,j,j)-ppide.l(:,i,i)) ...
				.* (squeeze(ppide.a(:,i,j,:))+mu_ijPars);
			r{7} = numeric.interpolant({zdiscPars,linspace(0,1,size(r6fine,2))},r6fine);

			% new constants for folding are stored in r{8-23}
			% c2 and c4. Exists in the dissertation without folding, but is never used in the integral
			% equations as a whole constant. Here this gets easier with this.
			% c2(z):
			r{8} = -(A_ij_disc+mu_ij).*sqrt(elementPars.lambda_i)/4 - numeric.cumtrapz_fast_nDim(...
				zdisc.', (A_ij_disc+mu_ij)/8./sqrt(elementPars.lambda_i)).*elementPars.lambda_i_diff;
			r8fine = -(squeeze(ppide.a(:,i,j,:))+mu_ijPars).*sqrt(ppide.l(:,i,i))/4 - numeric.cumtrapz_fast_nDim(...
				zdiscPars.', (squeeze(ppide.a(:,i,j,:))+mu_ijPars)/8./sqrt(ppide.l(:,i,i))).*ppide.l_diff(:,i,i);
			r{9} = numeric.interpolant({zdiscPars,linspace(0,1,size(r8fine,2))},r8fine);
			
			% c4(z):
			r{10} = numeric.cumtrapz_fast_nDim(zdisc.',...
				-(A_ij_disc+mu_ij)/2./sqrt(elementPars.lambda_j)).*sqrt(elementPars.lambda_i);
			r10fine = numeric.cumtrapz_fast_nDim(zdiscPars.',...
				-(squeeze(ppide.a(:,i,j,:))+mu_ijPars)/2./sqrt(ppide.l(:,j,j))).*sqrt(ppide.l(:,i,i));
			r{11} = numeric.interpolant({zdiscPars,linspace(0,1,size(r10fine,2))},r10fine);
			
			% c1st
			if j <= n/2
				z0j = z0(j);
				% c1st
				r{12} = (z0Left(j)*sqrt(elementPars.lambda_j(1,:))-sqrt(reshape(pars.lambda_disc(1,j+n/2,j+n/2,:),1,[]))*(1-z0j))...
						./(z0Left(j)*sqrt(elementPars.lambda_j(1,:))+sqrt(reshape(pars.lambda_disc(1,j+n/2,j+n/2,:),1,[]))*(1-z0j));
				% c2st
				r{13} = (1-z0j)*sqrt(elementPars.lambda_j(1,:))...
						./(z0Left(j)*sqrt(elementPars.lambda_j(1,:))+sqrt(reshape(pars.lambda_disc(1,j+n/2,j+n/2,:),1,[]))*(1-z0j));
			else
				r{12} = [];
				r{13} = [];
			end
			
			
			GH{i,j}.r = r; 


			% abbreviations from PDE:
% 			GH{i,j}.e = elementPars.lambda_j;
			
			% used in use_F_ppides (all 3)
			GH{i,j}.d = -GH{i,j}.a_ij_til - reshape(GH{i,j}.b_j,[1,ndisc,numtL]);
			GH{i,j}.f = elementPars.lambda_j./elementPars.lambda_i;
			GH{i,j}.g = ...
				reshape(elementPars.lambda_j,[ndisc,1,numtL])...
				./reshape(elementPars.lambda_i,[1,ndisc,numtL]);
		end % ~eliminate_convection
		
		%%%%%%%%%% initial values of fixpoint iteration %%%%%%%%%
		GH{i,j}.G = zeros(size(xi,1),size(eta,1),numtKernel); % preallocate
		GH{i,j}.H = zeros(size(xi,1),size(eta,1),numtKernel); % preallocate
		GH{i,j}.J = zeros(size(xi,1),size(eta,1),numtKernel); % preallocate
		
		% Problem: eta_s ist f�r jeden Zeitpunkt unterschiedlich lang! --> In dieser Struktur
		% nicht m�glich! 	
% 		if elementPars.lambda_i(1) == elementPars.lambda_j(1) 
% 			for tIdx = 1:numtKernel
% 				if numtL > 1, tIdxL = tIdx;	else, tIdxL = 1; end
% 				if j <= n/2
% 					GH{i,j}.G(:,:,tIdx) = repmat(r{11}.evaluate(GH{i,j}.zMat(:,1,tIdxL),tIdx),1,length(eta));
% 				else
% 					GH{i,j}.G(:,:,tIdx) = repmat(GH{i,j-n/2}.r{11}.evaluate(GH{i,j}.zMatInterp{tIdxL}.eval({eta,0}),tIdx).'*z0j/(1-z0j),length(xi),1);
% 				end
% 			end
% 		end
% 		
% 		GH{i,j}.zdisc = zdisc;
% 		GH{i,j}.H = get_h0_pideFolding(ppide,GH{i,j});
		
	end % for j
% first need to calculate all j because j+n is needed!
	for j=1:n	
		obj=GH{i,j};
		J1 = zeros(size(obj.xi,1),size(obj.eta,1),numtKernel);	
		r6Interp = numeric.interpolant({zdisc,1:size(GH{i,j}.r{6},2)},GH{i,j}.r{6});
		if j <= n/2
			eta_g_0 = GH{i,j}.eta(GH{i,j}.eta>=0);
			h01_xi = GH{i,j}.H(:,1,:);
			h01Interp = numeric.interpolant({GH{i,j}.xi,1,1:numtKernel},h01_xi);
			h01_eta = permute(h01Interp.evaluate(eta_g_0,1,1:numtKernel),[2 1 3]);
			h01jpn_xi = GH{i,j+n/2}.H(:,1,:);
			h01jpnInterp = numeric.interpolant({GH{i,j+n/2}.xi,1,1:numtKernel},h01jpn_xi);
			if numtL>1
				error('Hier nicht weiter mit zeitvarianz implementiert!')
			else
				z_etaeta = GH{i,j}.zMatInterp{1}.eval(eta_g_0,eta_g_0);
				% zeta must be zero
				zeta_etaeta = zeros(length(eta_g_0),1);
			
				xijpn_etaeta = GH{i,j+n/2}.xiMatInterp.eval(z_etaeta,zeta_etaeta);
				h01jpn_xi = permute(h01jpnInterp.evaluate(xijpn_etaeta,1,1:numtKernel),[2 1 3]);
				if GH{i,j}.lambda_i(1) ~= GH{i,j}.lambda_j(1)
					if numtL>1
						error('Hier nicht weiter mit zeitvarianz implementiert!')
					else
						eta_l_diffInterp = numeric.interpolant({obj.xi},obj.eta_l_diff);
					end
					for tIdx = 1:numtKernel
						% c1_tilde
						J1(:,obj.eta < 0,tIdx) = repmat(r6Interp.evaluate(GH{i,j}.zMatInterp{1}.eval(GH{i,j}.xi_l_sparse,obj.eta(obj.eta<0)),tIdx).'...
							.*1 ...
							./(1-GH{i,j}.s*eta_l_diffInterp.evaluate(GH{i,j}.xi_l_sparse)).',length(obj.xi),1);
					end
				end
				if GH{i,j}.lambda_i(1) >= GH{i,j}.lambda_j(1)
					J1(:,obj.eta >= 0,:) = repmat(...
						- GH{i,j}.r{12}*h01_eta + 2*GH{i,j+n/2}.s*GH{i,j}.r{13}*h01jpn_xi...
					,size(J1,1),1,1);
				else
					% DOF J(eta,eta)
					J1(:,obj.eta >= 0,:) = ...
						repmat(ppide.ctrl_pars.g_strich{i,j}(eta_g_0.'),length(GH{i,j}.xi),1,numtKernel);
				end
			end % numtL == 1
		end % j<=n/2
			
		% time-derivative
% 		G_eta = zeros(size(GH{i,j}.xi,1),size(GH{i,j}.eta,1),numtKernel);
% 		for tIdx=1:numtKernel
% 			if numtL >1
% 				tIdxL = tIdx;
% 			else
% 				tIdxL = 1;
% 			end
% 			etaVec = GH{i,j}.eta(:,tIdxL);
% 			G_eta(:,:,tIdx) = numeric.diff_num(etaVec,GH{i,j}.G(:,:,tIdx),2);
% 		end
% 		tVec = linspace(0,1,numtKernel);
% 		xi_t = diff_num(tVec,GH{i,j}.xi,2).';
% 		eta_t = diff_num(tVec,GH{i,j}.eta,2).';
% 		G_t1 = zeros(size(xi_t,1),size(eta_t,1),numtL);
% 		G_t2 = zeros(size(xi_t,1),size(eta_t,1),numtL);
% 		for tIdx=1:size(xi_t,2)
% 			for xi_idx=1:size(xi_t,1)
% % 				G_t1(xi_idx,:,tIdx) = GH{i,j}.H(xi_idx,:,tIdx)*xi_t(tIdx);
% 			end
% 			for eta_idx=1:size(eta_t,1)
% 				G_t2(:,eta_idx,tIdx) = G_eta(:,eta_idx,tIdx)*eta_t(tIdx);
% 			end
% 		end
        % time-derivative of G must be calculated via K!!!
% 		dtG = diff_num(tVec,GH{i,j}.G,3);
% 		if numtL == 1
% 			GH{i,j}.G_t = diff_num(tVec,GH{i,j}.G,3);
% 		else
% 			% must be computed via K. 
% 			% not yet implemented!
% 			error('Time dependent lambda not yet implemented for folding!')
% 		end
		% this is the total derivative because the values at the timestamps are explicitly
		% known!
		% directly calculate K
% 		GH{i,j}.K = GH{i,j}.transform_kernel_element(1);
		% Derivative in original coordinates
% 		GH{i,j}.K_t = diff_num(tVec,GH{i,j}.K,3);
% 		if size(GH{i,j}.xiMat_xi,3) == 1 && numtKernel > 1
% 			xiMat_xi = repmat(GH{i,j}.xiMat_xi,1,1,numtKernel);
% 		else
% 			xiMat_xi = GH{i,j}.xiMat_xi;
% 		end
% 		if size(GH{i,j}.etaMat_xi,3) == 1 && numtKernel > 1
% 			etaMat_xi = repmat(GH{i,j}.etaMat_xi,1,1,numtKernel);
% 		else
% 			etaMat_xi = GH{i,j}.etaMat_xi;
% 		end
% 		GH{i,j}.J = J1 + GH{i,j}.s/4*numeric.cumtrapz_fast_nDim(xiMat_xi,GH{i,j}.G_t,1,'equal','ignoreNaN');
% 		GH{i,j}.H = GH{i,j}.H + GH{i,j}.s/4*numeric.cumtrapz_fast_nDim(etaMat_xi,GH{i,j}.G_t,2,'equal','ignoreNaN');
% 		if any(isnan(GH{i,j}.G(:))) || any(isnan(GH{i,j}.G(:))) || any(isnan(GH{i,j}.J(:)))
% 			here=1;
% 		end
	end
end


end



