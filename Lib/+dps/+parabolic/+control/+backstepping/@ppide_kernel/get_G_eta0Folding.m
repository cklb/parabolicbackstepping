function G_eta0 = get_G_eta0Folding(obj,ppide)
%get_G_eta0 get starting value of the fixed point iteration to determine G_eta
warning('Todo: Performance! Ist sicher noch sehr schlecht!')
import misc.*
import numeric.*
zdisc = obj.zdisc;

ndisc = length(zdisc);
% time resolution is same for each kernel element
numtL = size(obj.value{1,1}.xi,2);
numtKernel = size(obj.value{1,1}.G,3);

A_disc = translate_to_grid(ppide.a_disc,zdisc);
if numtKernel > 1
	A_disc = permute(translate_to_grid(permute(A_disc,[4 1 2 3]),linspace(0,1,numtKernel)),[2 3 4 1]);
end
n = size(obj.value,1);
G_eta0 = myCell(n,n);
% warning('mu must be constant matrix')
tDisc = linspace(0,1,numtKernel);
% Hartwig edited
if ndisc~= n
	if isa(ppide.ctrl_pars.mu,'function_handle') % function
		if nargin(ppide.ctrl_pars.mu)==1
			muHelp = eval_pointwise(ppide.ctrl_pars.mu,zdisc);
		else
			muHelp = zeros(ndisc,n,n,length(tDisc));
			for z_idx=1:ndisc
				muHelp(z_idx,:,:,:) = permute((eval_pointwise(@(z)ppide.ctrl_pars.mu(zdisc(z_idx),z),tDisc)),[2 3 1]);
			end
		end
	elseif size(ppide.ctrl_pars.mu,1) > n % is discretized
		if length(size(ppide.ctrl_pars.mu))>3
			muInterpolant = interpolant({linspace(0,1,size(ppide.ctrl_pars.mu,1)),1:n,1:n,tDisc}, ppide.ctrl_pars.mu);
			muHelp = muInterpolant.evaluate(zdisc,1:n,1:n,tDisc);
		else
			muInterpolant = interpolant({linspace(0,1,size(ppide.ctrl_pars.mu,1)),1:n,1:n}, ppide.ctrl_pars.mu);
			muHelp = muInterpolant.evaluate(zdisc,1:n,1:n);
		end
		%muHelp = ppide.ctrl_pars.mu;
	elseif isequal(size(ppide.ctrl_pars.mu,1),n) % constant matrix, two dimensions
		muHelp(1:ndisc,:,:) = repmat(shiftdim(ppide.ctrl_pars.mu,-1),ndisc,1,1);
	else % discretized matrix
		error('dimensions of mu not correct!')
	end
else
	error('ndisc=n is a very bad choice!')
end
M = muHelp;
%M = ppide.ctrl_pars.mu;
% AInterp = interpolant({zdisc,1:n,1:n,1:numtL},A_disc);
for i=1:n
	for j=1:n
		kem = obj.value{i,j};
		ai_disc = kem.a_i;
		% attention: time-direction is not interpolated!
		aiInterp = interpolant({zdisc,1:numtL},ai_disc);
		aj_disc = kem.a_j;
		ajInterp = interpolant({zdisc,1:numtL},aj_disc);
		s = kem.s;
		r6_disc = kem.r{6};
		r6_Interp = interpolant({zdisc,1:size(r6_disc,2)},r6_disc);
		nxi = size(kem.xi,1);
		neta = size(kem.eta,1);
		xiMat = kem.xiMat_xi;
		etaMat = kem.etaMat_xi;
		if size(xiMat,3)==1
			xiMat = repmat(xiMat,1,1,numtKernel);
		end
		if size(etaMat,3)==1
			etaMat = repmat(etaMat,1,1,numtKernel);
		end
		
		lambda_jt_disc = kem.lambda_j_t;
		lambda_j_disc = kem.lambda_j;
		lambda_jt_interp = numeric.interpolant({zdisc,1:numtL},lambda_jt_disc);
		lambda_j_interp = numeric.interpolant({zdisc,1:numtL},lambda_j_disc);
		KInterp = numeric.interpolant({zdisc,zdisc,1:numtKernel},kem.K);
		KtInterp = numeric.interpolant({zdisc,zdisc,1:numtKernel},kem.K_t);
		
		
		MG = zeros(nxi,neta,numtKernel);
		GA = zeros(nxi,neta,numtKernel);
		rG = zeros(nxi,neta,numtKernel);
		G_t = zeros(nxi,neta,numtKernel);
		G_tComp = zeros(nxi,neta,numtKernel);
		Getaeta = zeros(1,neta,numtKernel);
		cH = zeros(nxi,neta,numtKernel);
		quasiStatic = ppide.ctrl_pars.quasiStatic;

		for tIdx =1:numtL
			if numtL>1
				tIdxAll = tIdx;
				if size(r6_disc,2)>1
					tIdxr6 = tIdx;
				else
					tIdxr6 = 1;
				end
				if size(M,4) > 1
					tIdxMu = tIdx;
				else
					tIdxMu = 1;
				end
				tIdxAi = tIdx;
			else
				tIdxAll = 1:numtKernel;
				if size(r6_disc,2)>1
					tIdxr6 = 1:numtKernel;
				else
					tIdxr6 = 1;
				end
				if size(M,4) > 1
					tIdxMu = tIdxAll;
				else
					tIdxMu = 1;
				end
				tIdxAi = 1;
			end
			
			xi_disc = kem.xi(:,tIdx).';
			eta_disc = kem.eta(:,tIdx).';
			H1Interp = numeric.interpolant({xi_disc,eta_disc,tIdxAll},kem.H(:,:,tIdxAll));
			
			
			Gt = kem.G(:,:,tIdxAll);
% 			Gt(isnan(Gt)) = 0;
			GInterp = numeric.interpolant({xi_disc,eta_disc,tIdxAll},Gt);
			A0Interp = numeric.interpolant({zdisc,1:n,1:n,1:numtKernel},A0_disc);
			g_diff = diff_num(eta_disc,eval_pointwise(ppide.ctrl_pars.g_strich{i,j},eta_disc));
			for eta_idx = 1:length(eta_disc)
				eta_ij=eta_disc(eta_idx);
				etaijAcc = acc(eta_ij);
				for xi_idx=1:nxi
					xi_ij = xi_disc(xi_idx);
					xiijAcc = acc(xi_ij);
					a = (s+1)/2 *kem.phi_i(end,tIdx) - (s-1)/2*kem.phi_j(end,tIdx); % top of spatial area is 2a
					eta_l = kem.eta_l(xi_idx,tIdx);
					etaLAcc = acc(eta_l);
					
					if etaijAcc>=etaLAcc && etaijAcc<= min(xiijAcc,acc(2*a-xi_ij)) % only in valid spatial area
						z = kem.zMat(xi_idx,eta_idx,tIdx);   % exact values
						z_idx = get_idx(z,zdisc);
						zeta = kem.zetaMat(xi_idx,eta_idx,tIdx);
						zeta_idx = get_idx(zeta,zdisc); % A is not yet interpolated!
						
						% WARUM GIBT SICH HIER EINE ABWEICHUNG!						
% 						G_tComp(xi_idx,eta_idx,tIdx) = lambda_jt_interp.evaluate(zeta,tIdx).*KInterp.evaluate(z,zeta,tIdx)...
% 						+ lambda_j_interp.evaluate(zeta,tIdx).*KtInterp.evaluate(z,zeta,tIdx);
						G_t(xi_idx,eta_idx,tIdxAll) = kem.G_t(xi_idx,eta_idx,tIdxAll);
						
						rG(xi_idx,eta_idx,tIdxAll) = reshape(bjInterp.evaluate(zeta,tIdxbj),[1,1,numel(tIdxbj)]).*kem.G(xi_idx,eta_idx,tIdxAll);
						cH(xi_idx,eta_idx,tIdxAll) = reshape(s*(aiInterp.evaluate(z,tIdxAi)+ajInterp.evaluate(zeta,tIdxAi)),[1,1,numel(tIdxAi)]).*(kem.H(xi_idx,eta_idx,tIdxAll)+H2(xi_idx,eta_idx,tIdxAll));
						for k=1:n
							% kj
							xikj = obj.xiMatXi{i,j,k,j}(xi_idx,eta_idx,tIdx);
							etakj = obj.etaMatXi{i,j,k,j}(xi_idx,eta_idx,tIdx);
							xikjIdx = get_idx(xikj,obj.value{k,j}.xi(:,tIdx));
							etakjIdx = get_idx(etakj,obj.value{k,j}.eta(:,tIdx));
% 							Akjzeta = AInterp.evaluate(zeta,k,j,tIdx);	
							Akjzeta = reshape(A_disc(zeta_idx,k,j,tIdxAll),[1,1,numel(tIdxAll)]);	
							% ik
							xiik = obj.xiMatXi{i,j,i,k}(xi_idx,eta_idx,tIdx);
							etaik = obj.etaMatXi{i,j,i,k}(xi_idx,eta_idx,tIdx);
							xiikIdx = get_idx(xiik,obj.value{i,k}.xi(:,tIdx));
							etaikIdx = get_idx(etaik,obj.value{i,k}.eta(:,tIdx));
							
							% Hier ist interpolation schwierig, weil f�r jedes k noch ein
							% Interpolant abgelegt werden m�sste. Am Besten w�re es, wenn man
							% die Inerpolanten f�r G, H, lambda_i, lambda_j, A mit im Kernelement
							% ablegen w�rde, dann k�nnte man hier einfach zugreifen!
							MG(xi_idx,eta_idx,tIdxAll) = MG(xi_idx,eta_idx,tIdxAll) + reshape(M(z_idx,i,k,tIdxMu),[1,1,length(tIdxMu)]).*obj.value{k,j}.G(xikjIdx,etakjIdx,tIdxAll);
							GA(xi_idx,eta_idx,tIdxAll) = GA(xi_idx,eta_idx,tIdxAll) + kem.lambda_j(zeta_idx,tIdx)/obj.value{i,k}.lambda_j(zeta_idx,tIdx)*obj.value{i,k}.G(xiikIdx,etaikIdx,tIdxAll).*Akjzeta;
						end
					else
						rG(xi_idx,eta_idx,tIdxAll) = NaN;
						cH(xi_idx,eta_idx,tIdxAll) = NaN;
						MG(xi_idx,eta_idx,tIdxAll) = NaN;
						GA(xi_idx,eta_idx,tIdxAll) = NaN;
						G_t(xi_idx,eta_idx,tIdxAll) = NaN;
					end
				end
			% xi_l for integration w.r.t. xi
				if eta_ij >= 0 
					xi_l = eta_ij;
				else
					xi_l = kem.xi_l(find(acc(kem.eta_l(:,tIdx)) <= etaijAcc,1),tIdx);
				end
				xi_lAcc = acc(xi_l);
% 				xi_l_idx = get_idx(xi_lAcc,kem.xi(:,tIdx));
				% ALTERNATIV: ERSTEN EINTRAG IN DER AKTUELLEN ZEILE DER XI-MAT FINDEN, DER
				% UNGLEICH NAN IST!
				zl = kem.zMatInterp{tIdx}({xi_lAcc,eta_ij});
% 				zlIdx = get_idx(zl,zdisc);
				if eta_ij >= 0
					if kem.s == 1
% 						xi_eta_idx = get_idx(eta_ij,kem.xi(:,tIdx));
						if ppide.b_op1.b_n(j,j) ~= 0 % it is zero in the Dirichlet Case!
							Getaeta(1,eta_idx,tIdxAll) = ...
								H1Interp.evaluate(eta_ij,eta_ij,tIdxAll)+H2Interp.evaluate(eta_ij,eta_ij,tIdxAll)...
								- s*GInterp.evaluate(eta_ij,eta_ij,tIdxAll)...
								- reshape(ljA0(get_idx(eta_ij,xi_disc),eta_idx,tIdxA0),[1,1,numel(tIdxA0)])...
								+ reshape(GA0(get_idx(eta_ij,xi_disc),eta_idx,tIdxAll),[1,1,numel(tIdxAll)]);
						else % dirichlet Case
							Getaeta(1,eta_idx,tIdxAll) = -(H1Interp.evaluate(eta_ij,eta_ij,tIdxAll)+H2Interp.evaluate(eta_ij,eta_ij,tIdxAll));
						end
					else
						Getaeta(1,eta_idx,tIdxAll) =...
							g_diff(eta_idx)-(H1Interp.evaluate(eta_ij,eta_ij,tIdxAll)...
							+H2Interp.evaluate(eta_ij,eta_ij,tIdxAll));
					end
				else
					Getaeta(1,eta_idx,tIdxAll) = ...
						reshape(r6_Interp.evaluate(zl,tIdxr6),[1,1,numel(tIdxr6)]) - s*(H1Interp.evaluate(xi_l,eta_ij,tIdxAll)...
						+H2Interp.evaluate(xi_l,eta_ij,tIdxAll));
					% H2=0 at the lower boarder so it is basically not needed here.
				end
			end
		end
		G_eta0.value{i,j} = 1/4/s*(...
			numeric.cumtrapz_fast_nDim(xiMat,(...
				rG+MG+GA+(1-quasiStatic)*G_t-cH...
				),1,'equal','ignoreNaN')...	
			)...
			+ Getaeta;
	end
end

end

