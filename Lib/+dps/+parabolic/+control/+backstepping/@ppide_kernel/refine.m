function kernel_ref = refine(kernel, factor)
%PPDE_KERNEL/REFINE refine the kernel elements

n = size(kernel.value,1);
kernel_ref = kernel; %Datenübernahme
z_orig = kernel.zdisc;
kernel_ref.zdisc =  linspace(z_orig(1),z_orig(end),factor*length(z_orig));
for i=1:n
	for j=1:n
		kernel_ref.value{i,j} = kernel.value{i,j}.refine(factor);
	end
end

end

