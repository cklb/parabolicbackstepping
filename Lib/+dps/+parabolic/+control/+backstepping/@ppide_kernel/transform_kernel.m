function kernel_new = transform_kernel(kernel,ppide)
%TRANSFORM_KERNEL transform ppide kernel into original z-zeta koordinates
% and calculate derivatives analytically

% created on 16.09.2016 by Simon Kerschbaum
% Should always be called, when kernel values are changed in the kernel;

import misc.*
import numeric.*
import dps.parabolic.control.backstepping.*
if kernel.transformed == 0
	timer = tic;
	fprintf('  Transforming kernel:\n')
	reverseStr = '';
	msg2='';
	run=0;

	n = size(kernel.value,1);
	kernel_new = kernel;
	if ~isa(kernel.value{1,1},'ppide_kernel_element')
		warning('Kernel not yet initialized!')
		return
	end
	zdisc = kernel.zdisc;
	ndisc = length(zdisc);
	zdisc_plant=linspace(0,1,ppide.ndiscPars);
	numtL = size(kernel.value{1,1}.lambda_i,2);
	numtKernel = size(kernel.value{1,1}.G,3);
	tVec = linspace(0,1,numtKernel);
	% Hartwig edited
	tDisc = tVec;
	% Hartwig edited
	if ndisc~= n
		if isa(ppide.ctrl_pars.mu,'function_handle') % function
			if nargin(ppide.ctrl_pars.mu)==1
				muHelp = eval_pointwise(ppide.ctrl_pars.mu,zdisc);
			else
				muHelp = zeros(ndisc,n,n,length(tDisc));
				for z_idx=1:ndisc
					muHelp(z_idx,:,:,:) = permute((eval_pointwise(@(z)ppide.ctrl_pars.mu(zdisc(z_idx),z),tDisc)),[2 3 1]);
				end
			end
		elseif size(ppide.ctrl_pars.mu,1) > n % is discretized
			if length(size(ppide.ctrl_pars.mu))>3
				muInterpolant = interpolant({linspace(0,1,size(ppide.ctrl_pars.mu,1)),1:n,1:n,tDisc}, ppide.ctrl_pars.mu);
				muHelp = muInterpolant.evaluate(zdisc,1:n,1:n,tDisc);
			else
				muInterpolant = interpolant({linspace(0,1,size(ppide.ctrl_pars.mu,1)),1:n,1:n}, ppide.ctrl_pars.mu);
				muHelp = muInterpolant.evaluate(zdisc,1:n,1:n);
			end
			%muHelp = ppide.ctrl_pars.mu;
		elseif isequal(size(ppide.ctrl_pars.mu,1),n) % constant matrix, two dimensions
			muHelp(1:ndisc,:,:) = repmat(shiftdim(ppide.ctrl_pars.mu,-1),ndisc,1,1);
		else % discretized matrix
			error('dimensions of mu not correct!')
		end
	else
		error('ndisc=n is a very bad choice!')
	end
	% transform each element and get time-derivative!
	for i =1:n
		for j=1:n
			nxi = size(kernel.value{i,j}.xi,1);
			neta = size(kernel.value{i,j}.eta,1);
			kernel_new.value{i,j}.K = kernel.value{i,j}.transform_kernel_element(1);
			kernel_new.value{i,j}.K_t = diff_num(tVec,kernel_new.value{i,j}.K,3);
			%
			kem = kernel_new.value{i,j};
			
			if numtL == 1
				s = kem.s;
				a = (s+1)/2 *kem.phi_i(end,:) - (s-1)/2*kem.phi_j(end,:); % a Ortsbereich
				kernel_new.value{i,j}.G_t = diff_num(tVec,kernel.value{i,j}.G,3);
				% write values outside the spatial area
				kernel_new.value{i,j}.G_t(~(acc(kem.eta.')>=acc(kem.eta_l) & acc(kem.eta.') <= min(acc(kem.xi),acc(2*a-kem.xi)))) = NaN;
				kernel_new.value{i,j}.G_t = misc.writeToOutside(kernel_new.value{i,j}.G_t);
			else % numtL > 1 not yet reworked
				lambda_jt_disc = kem.lambda_j_t;
				lambda_j_disc = kem.lambda_j;
				lambda_jt_interp = numeric.interpolant({zdisc,1:numtL},lambda_jt_disc);
				lambda_j_interp = numeric.interpolant({zdisc,1:numtL},lambda_j_disc);
				KInterp = numeric.interpolant({zdisc,zdisc,1:numtKernel},kem.K);
				KtInterp = numeric.interpolant({zdisc,zdisc,1:numtKernel},kem.K_t);
				s = kem.s;
				for tIdx =1:numtL
					if numtL >1
						tIdxAll = tIdx;
					else
						tIdxAll = 1:numtKernel;
					end
					xi_disc = kem.xi(:,tIdx).';
					eta_disc = kem.eta(:,tIdx).';
					for eta_idx = 1:neta
						eta_ij=eta_disc(eta_idx);
						etaijAcc = acc(eta_ij);
						for xi_idx=1:nxi
							xi_ij = xi_disc(xi_idx);
							xiijAcc = acc(xi_ij);
							a = (s+1)/2 *kem.phi_i(end,tIdx) - (s-1)/2*kem.phi_j(end,tIdx); % top of spatial area is 2a
							eta_l = kem.eta_l(xi_idx,tIdx);
							etaLAcc = acc(eta_l);
							if etaijAcc>=etaLAcc && etaijAcc<= min(xiijAcc,acc(2*a-xi_ij)) % only in valid spatial area
								z = kem.zMat(xi_idx,eta_idx,tIdx);   % exact values
								zeta = kem.zetaMat(xi_idx,eta_idx,tIdx);

								kernel_new.value{i,j}.G_t(xi_idx,eta_idx,tIdxAll) = lambda_jt_interp.evaluate(zeta,tIdx).*KInterp.evaluate(z,zeta,tIdxAll)...
								+ lambda_j_interp.evaluate(zeta,tIdx).*KtInterp.evaluate(z,zeta,tIdxAll);
							end
						end
					end
					% write values outside the spatial area
					for xi_idx = 1:length(xi_disc)
						xi_ij = xi_disc(xi_idx);
						xiijAcc = acc(xi_ij);
						for eta_idx =1:length(eta_disc)
							eta_ij = eta_disc(eta_idx);
							etaijAcc = acc(eta_ij);
							% values "over" the top left or the top right border have
							% to be written in the outer area.
							if etaijAcc > min(xiijAcc,acc(2*a-xi_ij))
								kernel_new.value{i,j}.G_t(xi_idx,eta_idx,tIdxAll) = kernel_new.value{i,j}.G_t(xi_idx,eta_idx-1,tIdxAll);					
							end 
							eta_inv_idx = length(eta_disc)-eta_idx+1;
							eta_inv_ij = eta_disc(eta_inv_idx);
							etaInvijAcc = acc(eta_inv_ij);
							if etaInvijAcc<acc(kem.eta_l(xi_idx,tIdx))
								kernel_new.value{i,j}.G_t(xi_idx,eta_inv_idx,tIdxAll) = kernel_new.value{i,j}.G_t(xi_idx,eta_inv_idx+1,tIdxAll);				
							end
						end
					end
				end
			end
		end
	end

% 	F_disc = interp1(zdisc_plant,ppide.f_disc,zdisc(:));
% 	F_disc = permute(interp1(zdisc_plant,permute(F_disc,[2 1 3 4]),zdisc(:)),[2 1 3 4]);
	if nargin(ppide.f) == 2
		% z zeta i j 
		F_disc = ppide.f.on({zdisc,zdisc});
	elseif nargin(ppide.f)==3
		% z zeta i j t
		F_disc = permute(ppide.f.on({zdisc,zdisc,tDisc}),[1 2 4 5 3]);
	end

	
	a0_disc = interp1(zdisc_plant,ppide.a_0_disc,zdisc(:));
	a_disc = translate_to_grid(ppide.a_disc,zdisc);
	min_dist = ppide.ctrl_pars.min_dist;
	
	% derivatives:
	if ~ppide.ctrl_pars.eliminate_convection 	
		% calculate G_eta by fixed-point iteration 
% 		if ~ppide.folded
			G_eta0 = kernel_new.get_G_eta0(ppide);
			use_F_handle = @kernel_new.use_F_Geta;
			G_eta = dps.parabolic.tool.fixpoint_iteration(G_eta0,use_F_handle,1,100,1e-8);
			if ppide.folded
				for i=1:n
					for j=1:n/2
						G_eta.value{i,j} = kernel.value{i,j}.J;
					end
				end
			end
% 		else
% 			for i=1:n
% 				for j=1:n
% 					kem = kernel_new.value{i,j};
% 					G_eta.value{i,j} = zeros(size(kem.xi,1),size(kem.eta,1),numtKernel);
% 					for tIdx = 1:numtL
% 						if numtL > 1
% 							tIdxAll = tIdx;
% 						else
% 							tIdxAll = 1:numtKernel;
% 						end
% 					G_eta.value{i,j}(:,:,tIdxAll) = numeric.diff_num(kem.eta(:,tIdx),kem.G(:,:,tIdxAll),2);
% 					end
% 				end
% 			end
% 		end
		% comment in for debug purposes!
		% checked after implementation of integral term. Fits (at least time constant)
% 		GH=kernel.value;
% 			for i=1:n
% 				for j=1:n
% 					s = GH{i,j}.s;
% 					a = (s+1)/2 *GH{i,j}.phi_i(end,:) - (s-1)/2*GH{i,j}.phi_j(end,:); % a Ortsbereich
% 					G_eta_fin{i,j} = zeros(size(kernel.value{i,j}.xi,1),size(kernel.value{i,j}.eta,1));
% 					for xi_idx=1:size(kernel.value{i,j}.xi,1)
% 						G_eta_fin{i,j}(xi_idx,:) = numeric.diff_num(kernel.value{i,j}.eta(:,1),kernel.value{i,j}.G(xi_idx,:,1),2);
% 					end
% 					G_etaNaN = G_eta;
% 					delta{i,j} = G_eta.value{i,j}(:,:,1)-G_eta_fin{i,j};
% 					
% 					delta{i,j}(~(acc(GH{i,j}.eta.')>=acc(GH{i,j}.eta_l) & acc(GH{i,j}.eta.') <= min(acc(GH{i,j}.xi),acc(2*a-GH{i,j}.xi)))) = NaN;
% 					G_eta_fin{i,j}(~(acc(GH{i,j}.eta.')>=acc(GH{i,j}.eta_l) & acc(GH{i,j}.eta.') <= min(acc(GH{i,j}.xi),acc(2*a-GH{i,j}.xi)))) = NaN;
% 					G_etaNaN.value{i,j}(~(acc(GH{i,j}.eta.')>=acc(GH{i,j}.eta_l) & acc(GH{i,j}.eta.') <= min(acc(GH{i,j}.xi),acc(2*a-GH{i,j}.xi)))) = NaN;
% % 						delta_GH{i,j}.(varnames{nameIdx}) = misc.writeToOutside(delta_GH{i,j}.(varnames{nameIdx}));
% 					
% 					figure('Name','G')
% 					surf(kernel_new.value{i,j}.G(:,:,1).')
% 					figure('Name','H')
% 					surf(kernel_new.value{i,j}.H(:,:,1).')
% 					figure('Name',['G_eta ' num2str(i) num2str(j)])
% 					subplot(1,3,1)
% 					surf(G_etaNaN.value{i,j}(:,:,1).','edgeColor','none')
% 					subplot(1,3,2)
% 					surf(G_eta_fin{i,j}.','LineStyle', 'none')
% 					subplot(1,3,3)
% 					surf(delta{i,j}.','edgeColor','none')
% 				end
% 			end
		for i=1:n
			for j=1:n
				el = kernel_new.value{i,j};
				%%% numerical calucation of derivatives. For comparison. Comment in if needed
	% 					dzetaK = zeros(ndisc,ndisc,numtKernel);
	% 					for zIdx = 2:ndisc
	% 						dzetaK(zIdx,1:zIdx,:) = diff_num(zdisc(1:zIdx),kernel_new.value{i,j}.K(zIdx,1:zIdx,:),2);
	% 					end
	% 					dzetaK(1,1,:) = dzetaK(2,1,:);
	% 					dzK = zeros(ndisc,ndisc,numtKernel);
	% 					for zetaIdx = 1:ndisc-1
	% 						dzK(zetaIdx:end,zetaIdx,:) = diff_num(zdisc(zetaIdx:end),kernel_new.value{i,j}.K(zetaIdx:end,zetaIdx,:),1);
	% 					end
	% 					dzK(end,end,:) = dzK(end,end-1,:);

				%%% analytical calculation
				% Kz = s/sqrt(l_i(z))/l_j(zeta)*H +
				%   1/sqrt(l_i(z))/l_j(zeta)*G_eta
				% Kzeta = -l_j_diff(zeta)/l_j(zeta)*K + s/l_j(zeta)^(3/2)*H -
				%   1/l_j(zeta)^(3/2)*G_eta
				dzK_an = zeros(ndisc,ndisc,numtKernel);
				dzetaK_an = zeros(ndisc,ndisc,numtKernel);
				etaMat = el.etaMat_xi;
				if size(etaMat,3)==1 && numtKernel>1
					etaMat = repmat(etaMat,1,1,numtKernel);
				end
				quasiStatic = ppide.ctrl_pars.quasiStatic;
				H2 = (1-quasiStatic)*1/4/el.s*cumtrapz_fast_nDim(etaMat,el.G_t,2,'equal','ignoreNan');

				for tIdx=1:numtL
					if numtL>1
						tIdxAll = tIdx;
					else
						tIdxAll = 1:numtKernel;
					end
					HInterp = numeric.interpolant({el.xi(:,tIdx),el.eta(:,tIdx),tIdxAll},el.H(:,:,tIdxAll));
					H2Interp = numeric.interpolant({el.xi(:,tIdx),el.eta(:,tIdx),tIdxAll},H2(:,:,tIdxAll));
					G_etaInterp = numeric.interpolant({el.xi(:,tIdx),el.eta(:,tIdx),tIdxAll},G_eta.value{i,j}(:,:,tIdxAll));
					if ~ppide.folded % H2 must be added because H in kernel is only H1
						for z_idx=1:ndisc
							for zeta_idx=1:z_idx
								% doesnt need the t-loop!
								[~,~,xi, eta] = el.get_xi(zdisc(z_idx),zdisc(zeta_idx));
								dzK_an(z_idx,zeta_idx,tIdxAll) = ...
									+ el.s/sqrt(el.lambda_i(z_idx,tIdx))/el.lambda_j(zeta_idx,tIdx)*(HInterp.evaluate(xi(tIdx),eta(tIdx),tIdxAll)+H2Interp.evaluate(xi(tIdx),eta(tIdx),tIdxAll))...
									+ 1/sqrt(el.lambda_i(z_idx,tIdx))/el.lambda_j(zeta_idx,tIdx)*G_etaInterp.evaluate(xi(tIdx),eta(tIdx),tIdxAll);
								dzetaK_an(z_idx,zeta_idx,tIdxAll) = -el.lambda_j_diff(zeta_idx,tIdx)/el.lambda_j(zeta_idx,tIdx)*el.K(z_idx,zeta_idx,tIdxAll)...
									+ el.s/el.lambda_j(zeta_idx,tIdx)^(3/2)*(HInterp.evaluate(xi(tIdx),eta(tIdx),tIdxAll)+H2Interp.evaluate(xi(tIdx),eta(tIdx),tIdxAll))...
									- 1/el.lambda_j(zeta_idx,tIdx)^(3/2)*G_etaInterp.evaluate(xi(tIdx),eta(tIdx),tIdxAll);
							end
						end
					else % H in kernel is total H
						% TODO: rework without look using xiMat, etaMat!
						for z_idx=1:ndisc
							for zeta_idx=1:z_idx
								% doesnt need the t-loop!
								[~,~,xi, eta] = el.get_xi(zdisc(z_idx),zdisc(zeta_idx));
								dzK_an(z_idx,zeta_idx,tIdxAll) = ...
									+ el.s/sqrt(el.lambda_i(z_idx,tIdx))/el.lambda_j(zeta_idx,tIdx)*(HInterp.evaluate(xi(tIdx),eta(tIdx),tIdxAll))...
									+ 1/sqrt(el.lambda_i(z_idx,tIdx))/el.lambda_j(zeta_idx,tIdx)*G_etaInterp.evaluate(xi(tIdx),eta(tIdx),tIdxAll);
								dzetaK_an(z_idx,zeta_idx,tIdxAll) = -el.lambda_j_diff(zeta_idx,tIdx)/el.lambda_j(zeta_idx,tIdx)*el.K(z_idx,zeta_idx,tIdxAll)...
									+ el.s/el.lambda_j(zeta_idx,tIdx)^(3/2)*(HInterp.evaluate(xi(tIdx),eta(tIdx),tIdxAll))...
									- 1/el.lambda_j(zeta_idx,tIdx)^(3/2)*G_etaInterp.evaluate(xi(tIdx),eta(tIdx),tIdxAll);
							end
						end
					end
				end
				kernel_new.value{i,j}.dzeta_K_z_0 = permute(dzetaK_an(:,1,:),[1 3 2]);
				kernel_new.value{i,j}.dz_K_1_zeta = permute(dzK_an(end,:,:),[2 3 1]);
				kernel_new.value{i,j}.dz_K = dzK_an;
				% Plot comparison (comment in with calculation for debug purposes!)
% 				diffZ = dzK - dzK_an;
% 				diffZeta = dzetaK - dzetaK_an;
% 				figure('Name',['Diff dzK:' num2str(i) num2str(j)])
% 				subplot(2,1,1)
% 				surf(diffZ(:,:,1).');
% 				subplot(212)
% 				hold on
% 				surf(dzK(:,:,1).')
% 				surf(dzK_an(:,:,1).')
% 				figure('Name',['Diff dzetaK:' num2str(i) num2str(j)])
% 				subplot(211)
% 				surf(diffZeta(:,:,1).');
% 				subplot(212)
% 				hold on
% 				surf(dzetaK(:,:,1).')
% 				surf(dzetaK_an(:,:,1).')
			end % for j=1:n
		end % for i=1:n
	else % now eliminate_convection=1
		for dir = 1:2 % dzK(1,zeta) and dzetaK(z,0)
			for i=1:n
				for j=1:n
					if ~ismatrix(kernel_new.value{1,1}.G)
						error('Eliminate_convection=1 not implemented for time-varying coefficients!')
					end
					element = kernel_new.value{i,j};
					s = element.s; 
					Fij_disc = permute(F_disc(:,:,i,j,:),[1 2 5 3 4]);
					detaG = 0; % reset size
					detaG(1:ndisc)=0; % preallocate

					xidisc = kernel_new.value{i,j}.xi;
					xifine = kernel_new.value{i,j}.xi_fine;
					etadisc = kernel_new.value{i,j}.eta;
					etafine = kernel_new.value{i,j}.eta_fine;
					g_diff = diff_num(etadisc,ppide.ctrl_pars.g_strich{i,j}(etadisc));

					% get those points on the boundary (eta,eta), which are
					% existent in the grid!
					% Problem: if the grid is sparse, it may be that there is no
					% further point (eta,eta) existent. 
					% To avoid this, the fine grid is used here.
					id = 1;
					eta_eq_xi_idx_vec = 0;
					xi_eq_eta_idx_vec = 0;
					for xi_idx = 1:length(xifine)
						eta_eq_xi_idx = find(acc(etafine)==acc(xifine(xi_idx)));
						if ~isempty(eta_eq_xi_idx) % this xi exists in eta
							eta_eq_xi_idx_vec(id) = eta_eq_xi_idx;      %#ok<AGROW>
							xi_eq_eta_idx_vec(id) = xi_idx;             %#ok<AGROW>
							id = id+1;
						end
					end 
					eta_eq_xi_vec = etafine(eta_eq_xi_idx_vec);
					xi_eq_eta_vec = xifine(xi_eq_eta_idx_vec); %should be the same!
					H_eta_eq_xi = mydiag(kernel_new.value{i,j}.H,get_idx(xi_eq_eta_vec,xidisc),get_idx(eta_eq_xi_vec,etadisc));
					G_eta_eq_xi = mydiag(kernel_new.value{i,j}.G,get_idx(xi_eq_eta_vec,xidisc),get_idx(eta_eq_xi_vec,etadisc));
					% 1D Interpolating on the complete eta grid
					H_eta_eta_vec = interp1(acc(eta_eq_xi_vec),H_eta_eq_xi,acc(etafine));
					G_eta_eta_vec = interp1(acc(eta_eq_xi_vec),G_eta_eq_xi,acc(etafine));

					if dir==1 % dzK(1,zeta)
						[xi_vec, eta_vec] = kernel_new.value{i,j}.get_xi(1,zdisc);
					else % dzetaK(z,0)
						[xi_vec, eta_vec] = kernel_new.value{i,j}.get_xi(zdisc,0);
					end

					H_vec = 0; %reset size
					H_vec(1:ndisc)=0; %preallocate
					for zeta_idx = 1:ndisc 
						fprintf([reverseStr msg2]);
						xi = xi_vec(zeta_idx);
						eta = eta_vec(zeta_idx);
						xi_idx = get_idx(acc(xi),acc(xidisc));
						eta_idx = get_idx(acc(eta),acc(etadisc));
						% used for H(eta,eta)
						eta_idx_fine = get_idx(acc(eta),acc(etafine));

						% H(xi,eta)
						H_vec(zeta_idx) = kernel_new.value{i,j}.H(xi_idx,eta_idx);
						% areas of integration
						if eta >= 0
							xi_s_vec = xidisc(acc(xidisc,1/min_dist)>=acc(eta,1/min_dist) & acc(xidisc,1/min_dist) <= acc(xi,1/min_dist));
							xi_s_idx_vec = find(acc(xidisc,1/min_dist)>=acc(eta,1/min_dist) & acc(xidisc,1/min_dist) <= acc(xi,1/min_dist));
						else % cant be implemented by get_idx as it would lead to points outside the domain
							xi_s_vec = xidisc(acc(xidisc,1/min_dist)>= acc(element.xi_l(find(element.eta_l <= eta,1)),1/min_dist) & acc(xidisc,1/min_dist) <= acc(xi,1/min_dist));
							xi_s_idx_vec = find(acc(xidisc,1/min_dist)>= acc(element.xi_l(find(element.eta_l <= eta,1)),1/min_dist) & acc(xidisc,1/min_dist) <= acc(xi,1/min_dist));
						end
						if ~isempty(xi_s_vec)
							[z_vec, zeta_vec] = element.get_z(xi_s_vec,repmat(eta,1,length(xi_s_vec)));
							z_idx_vec  = get_idx(z_vec,zdisc);
							zeta_idx_vec = get_idx(zeta_vec,zdisc);

							Gik_sum1 = 0; % reset size
							Gik_sum1(1:length(xi_s_vec)) = 0; % preallocate
							Mu_sum = 0; % reset size
							Mu_sum(1:length(xi_s_vec)) = 0; % preallocate
							Fsum = 0;
							Fsum(1:length(xi_s_vec)) = 0;
							for k=1:n
								% Hartwig edited
								mu_ik = muHelp(:,i,k);
								%mu_ik = ppide.ctrl_pars.mu(i,k);
								xi_ik_disc = kernel_new.value{i,k}.xi;
								eta_ik_disc = kernel_new.value{i,k}.eta;
								[X,E] = meshgrid(xi_ik_disc,eta_ik_disc);
								xi_kj_disc = kernel_new.value{k,j}.xi;
								eta_kj_disc = kernel_new.value{k,j}.eta;
								[X_kj,E_kj] = meshgrid(xi_kj_disc,eta_kj_disc);
								[z_xis,zeta_xis] = element.get_z(xi_s_vec,repmat(eta,1,length(xi_s_vec)));
								zeta_xis_idx_vec = get_idx(zeta_xis,zdisc);
								z_xis_idx_vec = get_idx(z_xis,zdisc);
								[xi_ik_xis_eta, eta_ik_xis_eta] = kernel_new.value{i,k}.get_xi(z_xis,zeta_xis);
								[xi_kj_xis_eta, eta_kj_xis_eta] = kernel_new.value{k,j}.get_xi(z_xis,zeta_xis);
								Gikxi_s_eta  = interp2(acc(X),acc(E),kernel_new.value{i,k}.G.',acc(xi_ik_xis_eta),acc(eta_ik_xis_eta));
								Gkjxi_s_eta  = interp2(acc(X_kj),acc(E_kj),kernel_new.value{k,j}.G.',acc(xi_kj_xis_eta),acc(eta_kj_xis_eta));
								Akjzeta = a_disc(zeta_xis_idx_vec,k,j);
								fkjzeta = kernel_new.value{k,j}.f(zeta_xis_idx_vec);

								Fkj_disc = F_disc(:,:,k,j);
								for xi_s_idx = 1:length(xi_s_vec)		
									Gik_sum1(xi_s_idx) = Gik_sum1(xi_s_idx) + ...
												 fkjzeta(xi_s_idx) * Gikxi_s_eta(xi_s_idx)*...
												 Akjzeta(xi_s_idx);
									Mu_sum(xi_s_idx) = Mu_sum(xi_s_idx) + ...
												mu_ik(z_idx_vec(xi_s_idx)) * kernel_new.value{i,k}.varphi_j(z_xis_idx_vec(xi_s_idx))...
													  /kernel_new.value{i,j}.varphi_i(z_xis_idx_vec(xi_s_idx))...
													  * Gkjxi_s_eta(xi_s_idx);

									% implementation of integral part F
									if ~isequal(F_disc,zeros(ndisc,ndisc,n,n))
										zeta_s_idx_vec = zeta_xis_idx_vec(xi_s_idx):z_xis_idx_vec(xi_s_idx);
										zeta_s_vec = zdisc(zeta_s_idx_vec);


										Fkj_zetas_zeta = Fkj_disc(zeta_s_idx_vec,zeta_xis_idx_vec(xi_s_idx)).';
										try
										[xi_ik_z_xis_eta_zetas,eta_ik_z_xis_eta_zetas] =...
											kernel_new.value{i,k}.get_xi(repmat(z_xis(xi_s_idx),1,length(zeta_s_vec)),zeta_s_vec);
										catch errmsg
											warning(mythrow(errmsg));
										end
										xi_ik_z_xis_eta_zetas_idx = get_idx(xi_ik_z_xis_eta_zetas,xi_ik_disc);
										eta_ik_z_xis_eta_zetas_idx = get_idx(eta_ik_z_xis_eta_zetas,eta_ik_disc);
										try
											Fsum(xi_s_idx) = Fsum(xi_s_idx) + ...
											trapz_fast(...
											zeta_s_vec,...
											kernel_new.value{k,j}.g(zeta_xis_idx_vec(xi_s_idx),zeta_s_idx_vec).'...
											.*mydiag(kernel_new.value{i,k}.G,xi_ik_z_xis_eta_zetas_idx,eta_ik_z_xis_eta_zetas_idx).'...
											.*Fkj_zetas_zeta);	
										catch errmsg
											warning(mythrow(errmsg));
										end
									end %TODO: DEBUG VARIABLE BEIM KERNEL EINFUEHREN, DIE NACHRICHTEN AUSSPUCKT
								end
							end
							try
							integral = trapz_fast(xi_s_vec,...
											(-element.d(z_idx_vec,zeta_idx_vec).').*element.G(xi_s_idx_vec,eta_idx).'...
											- element.e(z_idx_vec,zeta_idx_vec).'.*mydiag(Fij_disc,z_idx_vec,zeta_idx_vec).'...
											+ Mu_sum...
											+ Gik_sum1...
											+ Fsum);
							catch errmsg
								warning(mythrow(errmsg));
							end
						else
							integral=0;
						end

						if eta>= 0
							H_eta_eta = H_eta_eta_vec(eta_idx_fine);
							G_eta_eta = G_eta_eta_vec(eta_idx_fine);
							[z_eta_eta, ~] = element.get_z(eta,eta);
							z_eta_eta_idx = get_idx(z_eta_eta,zdisc);

							a0sum = 0; % if no boundary feedback provided
							zeta_s_idx_vec = 1:z_eta_eta_idx;
							zeta_s_vec = zdisc(zeta_s_idx_vec);
							% if boundary feedback a0 is active
							if ~isequal(a0_disc,zeros(ndisc,n,n))
								for k=1:n
									[xi_ik_z_eta_eta_zeta_s, eta_ik_z_eta_eta_zeta_s] = kernel_new.value{i,k}.get_xi(repmat(z_eta_eta,1,length(zeta_s_vec)),zeta_s_vec);
									xi_ik_z_eta_eta_zeta_idx = get_idx(xi_ik_z_eta_eta_zeta_s,kernel_new.value{i,k}.xi);
									eta_ik_z_eta_eta_zeta_idx = get_idx(eta_ik_z_eta_eta_zeta_s,kernel_new.value{i,k}.eta);

									a0sum = a0sum ...
											+ trapz_fast(zeta_s_vec,...
												kernel_new.value{i,j}.varphi_j(zeta_s_idx_vec).'...
												.* mydiag(kernel_new.value{i,k}.G,xi_ik_z_eta_eta_zeta_idx,eta_ik_z_eta_eta_zeta_idx).'...
												./ kernel_new.value{i,k}.lambda_j(zeta_s_idx_vec).'...
												.*squeeze(a0_disc(zeta_s_idx_vec,k,j)).');
								end
							end
							if s==1 % lambda_i >=  lambda_j
								if ppide.b_op1.b_n(j,j) ~= 0 % Rob/Neu
									detaG(zeta_idx) = H_eta_eta...
											  + element.r{3}*G_eta_eta...
											  - element.r{4}(z_eta_eta_idx)*a0_disc(z_eta_eta_idx,i,j)...
											  + element.r{5} * a0sum ...
											  + 1/4/s*integral;		
								else % Dir.
									detaG(zeta_idx) = -H_eta_eta + 1/4/s*integral;
								end
							else % lambda_i < lambda_j
								detaG(zeta_idx) = g_diff(eta_idx) - H_eta_eta + 1/4/s*integral;
							end
						else % eta < 0
							% ATTENTION: can be a problem, xi_l is always left of
							% the boundary and the value taken there lies out of
							% the domain. eta_l and xi_l are not defined on a fine
							% grid.

							H_bord = kernel_new.value{i,j}.H(get_idx(element.xi_l(find(element.eta_l <= eta,1)),xidisc),eta_idx);
							% does not bring better results:
		% 					H_bord = kernel_new.value{i,j}.H(get_idx(element.xi_l(find(element.eta_l >= eta,1,'last')),xidisc),eta_idx);

							xi_l = element.xi_l(find(element.eta_l <= eta,1));
							% doenst bring better results:
		% 					xi_l = element.xi_l(find(element.eta_l >= eta,1,'last'));
							z_xil_eta = element.get_z(xi_l,eta);
							z_xil_eta_idx = get_idx(z_xil_eta,zdisc);

							detaG(zeta_idx) = element.r{6}(z_xil_eta_idx) - ...
											  s*H_bord + 1/4/s*integral;
						end % if eta >= 1

					fortschritt = round(((run/(2*n^2))+zeta_idx/ndisc/(2*n^2))*100);
					reverseStr = repmat(sprintf('\b'), 1, length(msg2)-1);
					msg2 = sprintf(' %u%%%%',fortschritt);
					end % for zeta_idx = 1:ndisc
					if dir==1
						kernel_new.value{i,j}.dz_K_1_zeta = ...
						(element.varphi_i_diff(end)/sqrt(element.lambda_i(end))/element.varphi_i(end).*element.K(end,:)...
						+ (element.varphi_i(end).*element.varphi_j./element.lambda_j./sqrt(element.lambda_i(end))).'.*(...
							s*H_vec + detaG...
						)).';
					else
						kernel_new.value{i,j}.dzeta_K_z_0 = ...
							(-element.lambda_j_diff(1)/element.lambda_j(1).*element.K(:,1).'...
							+1/element.lambda_j(1)*(...
								element.varphi_j_diff(1)*element.lambda_j(1)/sqrt(element.lambda_j(1))/element.varphi_j(1)*element.K(:,1).'...
								+ element.varphi_i.' .* element.varphi_j(1) .* (...
									s/sqrt(element.lambda_j(1))*H_vec ...
									-1/sqrt(element.lambda_j(1)) * detaG...
								)...
							)).';
					end
					run = run+1;
				end  % j
			end % i
		end % dir
	end % eliminate_convection =1
	timeelapsed = toc(timer);
	if ~ppide.ctrl_pars.eliminate_convection 
		fprintf(['  finished transformation! (' num2str(round(timeelapsed,2)) ' s)\n'])
	else
		fprintf([reverseStr msg2 '(' num2str(round(timeelapsed,2)) ' s)\n'])
	end
	% reverseStr = repmat(sprintf('\b'), 1, length(msg1)+length(msg2)-2);
	% fprintf(reverseStr); % alles wieder l�schen
	% fprintf('\n')
	kernel_new.transformed = 1;
	kernel_new = kernel_new.get_a0_til(ppide);
	kernel_new.KI = kernel_new.invert_kernel;
	kernel_new.inverted=1;
else
	disp('kernel already transformed')
	kernel_new=kernel;
end
	



end % function

