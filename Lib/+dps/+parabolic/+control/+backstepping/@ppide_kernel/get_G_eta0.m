function G_eta0 = get_G_eta0(obj,ppide)
%get_G_eta0 get starting value of the fixed point iteration to determine G_eta
import misc.*
import numeric.*
zdisc = obj.zdisc;

tic
fprintf('    Calculating G_eta0...')

ndisc = length(zdisc);
% time resolution is same for each kernel element
numtL = size(obj.value{1,1}.xi,2);
numtKernel = size(obj.value{1,1}.G,3);
Bn = ppide.b_op1.b_n;
kernel = obj;
n = size(obj.value,1);
zdiscPars = linspace(0,1,ppide.ndiscPars);
ndiscPars = numel(zdiscPars);


A0_disc = translate_to_grid(ppide.a_0,zdisc);
if numtKernel > 1
	A0_disc = permute(translate_to_grid(permute(A0_disc,[4 1 2 3]),linspace(0,1,numtKernel)),[2 3 4 1]);
end

G_eta0 = myCell(n,n);
% warning('mu must be constant matrix')
tDisc = linspace(0,1,numtKernel);
if nargin(ppide.f) == 2
	% z zeta i j 
	F_disc = ppide.f.on({zdisc,zdisc});
	numtF = 1;
elseif nargin(ppide.f)==3
	% z zeta i j t
	F_disc = permute(ppide.f.on({zdisc,zdisc,tDisc}),[1 2 4 5 3]);
	numtF = size(F_disc,5);
end


if ~ppide.folded
	GH = obj.value;
	% preallocate
	Gzzeta = cell(n,n);
	G = cell(n,n,n,n);
	Giksum = cell(n,n);
	GInterp = cell(n,n);
	% store G on z,zeta-grid
	for i = 1:n
		for j=1:n
			GInterp{i,j} = numeric.interpolant({kernel.value{i,j}.xi,kernel.value{i,j}.eta,1:numtKernel},kernel.value{i,j}.G);
			xiMat = kernel.value{i,j}.xiMat;
			etaMat = kernel.value{i,j}.etaMat;
			GijAll = GInterp{i,j}.eval(repmat(xiMat(:),numtKernel,1),repmat(etaMat(:),numtKernel,1),...
						reshape(repmat(1:numtKernel,numel(xiMat),1),[numtKernel*numel(xiMat),1]));
			% fuer F benoetigt.
			Gzzeta{i,j} = reshape(GijAll,size(xiMat,1),size(xiMat,2),numtKernel);

			for k=1:n			
				for l=1:n				
					% store interpolations of G
					xiMatXi = kernel.xiMatXi{k,l,i,j}; % xi_ij(xi_kl,eta_kl)
					etaMatXi = kernel.etaMatXi{k,l,i,j}; % eta_ij(xi_kl,eta_kl)
					GAll = GInterp{i,j}.eval(repmat(xiMatXi(:),numtKernel,1),repmat(etaMatXi(:),numtKernel,1),...
						reshape(repmat(1:numtKernel,numel(xiMatXi),1),[numtKernel*numel(xiMatXi),1]));
					% G_ij(xi_kl,eta_kl)
					G{i,j,k,l} = reshape(GAll,size(xiMatXi,1),size(xiMatXi,2),numtKernel);
				end
			end
		end

	end
	Azeta = kernel.Azeta;      % Azeta{i,j,k,j} = Aij(zeta(xi_kj,eta_kj))
	MuZ = kernel.MuZ;		   % MuZ{i,j,i,k} =  muij(z(xi_ik,eta_ik))
	lambdaZeta = kernel.lambdaZeta;  % lambdaZeta{k,i,j} = lambda_k(zeta(xi_ij,eta_ij))
	K = obj.get_K;		
	zDiscVec = zdisc(:);
	zetaDiscVec = zDiscVec.';
	zetaBarVec = reshape(zdisc,1,1,ndisc);
	zMatExt = repmat(reshape(zdisc,1,1,ndisc),ndisc,ndisc,1);
	% the trick is that zeta_bar is only valid in the area, where it is between z and
	% zeta! That means the integration is only perfomed in that area. Therefore, no
	% cumtrapz is needed because the valid area direcly is the range zeta--> z
	zMatExt( (zetaBarVec < zetaDiscVec) | (zetaBarVec > zDiscVec) ) = NaN;
	% account for the time dependency
	zMatExt = repmat(zMatExt,1,1,1,numtKernel);
	for i=1:n
		for j=1:n
			kem = obj.value{i,j};
			xiDisc = kem.xi;
			etaDisc = kem.eta;
			a_delta = kem.aiZXieta+kem.ajZetaXieta;
			if any(kem.b_j(:))
				error('Current implementation does not include kem.bj! (needed for convection)')
			end
			s = kem.s;

			if any(etaDisc<0)
				r6_disc = kem.r{6};
				r6_Interp = interpolant({zdisc,1:size(r6_disc,2)},r6_disc);
				zXilEta = reshape(kem.zMatInterp{1}.eval(kem.xi_l_sparse,etaDisc(etaDisc<0)),[],1);
				r6All = r6_Interp.eval(repmat(zXilEta,size(r6_disc,2),1),...
					reshape(repmat(1:size(r6_disc,2),numel(zXilEta),1),[size(r6_disc,2)*numel(zXilEta),1]));
				r6xilEta = reshape(r6All,1,numel(zXilEta),size(r6_disc,2));
			end
			
			xiMat = kem.xiMat_xi;
			etaMat = kem.etaMat_xi;
			if size(xiMat,3)==1
				xiMat = repmat(xiMat,1,1,numtKernel);
			end
			if size(etaMat,3)==1
				etaMat = repmat(etaMat,1,1,numtKernel);
			end
			
			GA0 = zeros(ndisc,ndisc,numtKernel);
			for k=1:n
				%lambda_k(ozeta) (3rd dim.)
				% 2nd dimension is integrated
				lambda_kZeta = reshape(GH{i,k}.lambda_j,1,ndisc);
				GikzzetaA0 = Gzzeta{i,k}(:,:,:);
				GA0 = GA0 + 1./lambda_kZeta .* GikzzetaA0 .* reshape(A0_disc(:,k,j,:),1,ndisc,[]);
			end		
			zMatA0 = repmat(zetaDiscVec,ndisc,1,numtKernel);
			zMatA0( zetaDiscVec > zDiscVec & ones(1,1,numtKernel) ) = NaN;
			GA0Int = reshape(numeric.trapz_fast_nDim(zMatA0,GA0,2,'equal','ignoreNaN'),[ndisc,numtKernel]);
			GA0IntInterp = numeric.interpolant({zdisc,1:numtKernel},GA0Int);
			zEtaEta = reshape(kem.zMatInterp{1}.eval(etaDisc(etaDisc>=0),etaDisc(etaDisc>=0)),[],1);
			GA0All = GA0IntInterp.eval(repmat(zEtaEta,numtKernel,1),...
				reshape(repmat(1:numtKernel,numel(zEtaEta),1),[numtKernel*numel(zEtaEta),1]));
			GA0EtaEta = reshape(GA0All,1,numel(zEtaEta),numtKernel);
			
			A0Interp = numeric.interpolant({zdiscPars,linspace(0,1,size(ppide.a_0,4))},reshape(ppide.a_0(:,i,j,:),ndiscPars,[]));
			A0All = A0Interp.eval(repmat(zEtaEta,numtKernel,1),...
				reshape(repmat(linspace(0,1,numtKernel),numel(zEtaEta),1),[numtKernel*numel(zEtaEta),1]));
			A0EtaEta = reshape(A0All,1,numel(zEtaEta),numtKernel);

			Giksum = zeros(size(GH{i,j}.G));
			for k=1:n
				% compute sums
				Giksum = Giksum + lambdaZeta{j,i,j}./lambdaZeta{k,i,j}.*G{i,k,i,j}.*Azeta{k,j,i,j}...
					+ MuZ{i,k,i,j}.*G{k,j,i,j};
			end
			
			
			%    z, zeta, t. --> lambda(zeta) must be row vector!
			lF = reshape(GH{i,j}.lambda_j,1,ndisc).'.*reshape(F_disc(:,:,i,j,:),ndisc,ndisc,numtF);
			lFInterp = numeric.interpolant({zdisc,zdisc,1:numtF},lF);
			lFXiEtaAll = lFInterp.eval(repmat(kem.zMat(:),numtF,1),repmat(kem.zetaMat(:),numtF,1),...
				reshape(repmat(1:numtF,numel(kem.zMat),1),[numtF*numel(kem.zMat),1]));
			lFXiEta = reshape(lFXiEtaAll,size(kem.zMat,1),size(kem.zMat,2),numtF);
			G_t = kem.G_t;
			quasiStatic = ppide.ctrl_pars.quasiStatic;
			H1 = kem.H;
			H2 = (1-quasiStatic)*1/4/kem.s*cumtrapz_fast_nDim(etaMat,kem.G_t,2,'equal','ignoreNan');
			
			% Permute F so that 
			% Fperm(zeta,zeta_bar) = F(zeta_bar,zeta)
			% create array (z,zeta,zeta_bar) so that integration can be performed with
			% cumtrapzFastnDim			
			% z zeta zeta_bar t
			KF = zeros(ndisc,ndisc,ndisc,numtKernel);
			for k=1:n
				% z zeta zeta_bar t
				FkjPerm = shiftdim(permute(F_disc(:,:,k,j,:),[2 1 5 3 4]),-1);
				Kik = reshape(K(:,:,i,k,:),[ndisc,1,ndisc,numtKernel]);
				% lambda_j_disc depends on zeta --> row vector!
				KF = KF + reshape(GH{i,j}.lambda_j,1,ndisc).*Kik.*FkjPerm;
			end
			% z zeta 1 t to z zeta t
			KFInt = permute(numeric.trapz_fast_nDim(zMatExt,KF,3,'equal','ignoreNaN'),[1 2 4 3]);
			KFInterp = numeric.interpolant({zdisc,zdisc,1:numtKernel},KFInt);
			KFAll = KFInterp.eval(repmat(kem.zMat(:),numtKernel,1),repmat(kem.zetaMat(:),numtKernel,1),...
				reshape(repmat(1:numtKernel,numel(kem.zMat),1),[numtKernel*numel(kem.zMat),1]));
			KFXiEta = reshape(KFAll,size(kem.zMat,1),size(kem.zMat,2),numtKernel);
			
			% Integration over A0:
			% int_0^z sum_k=1^n 1/lambda_k(zeta) Gik(z,zeta)A0kj(zeta) d zeta
			% = int_0^z sum_k=1^n Kik(z,zeta)A0kj(zeta) d zeta
			% with z  = z(eta,eta)
			KA0 = zeros(ndisc,ndisc,numtKernel);
			for k=1:n
				% z, zeta, (t)
				KikForA0 = reshape(K(:,:,i,k,:),[ndisc,ndisc,numtKernel]);
				% depends on zeta, (t)
				A0kjPerm = shiftdim(permute(A0_disc(:,k,j,:),[1 4 2 3]),-1);
				KA0 = KA0 + KikForA0.*A0kjPerm;
			end
			zetaMat = repmat(zetaDiscVec,ndisc,1);
			zetaMat(zetaDiscVec>zDiscVec) = NaN;
			zetaMat = repmat(zetaMat,1,1,numtKernel);
			KA0Int = permute(numeric.trapz_fast_nDim(zetaMat,KA0,2,'equal','ignoreNaN'),[1 3 2]);
			KA0Interp = numeric.interpolant({zdisc,1:numtKernel},KA0Int);
			zVec = kem.get_z(kem.eta(kem.eta>=0),kem.eta(kem.eta>=0));
			KA0All = KA0Interp.eval(repmat(zVec,numtKernel,1),...
				reshape(repmat(1:numtKernel,numel(zVec),1),[numtKernel*numel(zVec),1]));
			KA0EtaEta = sqrt(GH{i,j}.lambda_j(1))*reshape(KA0All,1,size(zVec,1),numtKernel);
			
			cH = -s*a_delta.*(H1+H2);
			
			g_diff = diff_num(etaDisc(etaDisc>=0),eval_pointwise(ppide.ctrl_pars.g_strich{i,j},etaDisc(etaDisc>=0)));

	
					G_xil_eta = zeros(size(xiMat));
					HInterp = numeric.interpolant({GH{i,j}.xi,GH{i,j}.eta,1:numtKernel},H1+H2);
					Hetaeta = ...
							reshape(...
								HInterp.eval(...
									repmat(etaDisc(etaDisc>=0),numtKernel,1),...
									repmat(etaDisc(etaDisc>=0),numtKernel,1),...
									reshape(repmat(1:numtKernel,numel(etaDisc(etaDisc>=0)),1),[numtKernel*numel(etaDisc(etaDisc>=0)),1])...
								)...
								,1,numel(etaDisc(etaDisc>=0)),numtKernel...
							)...
						;
					if s==1 
						if Bn(j,j) ~= 0
							Getaeta = ...
								reshape(...
									GInterp{i,j}.eval(...
										repmat(etaDisc(etaDisc>=0),numtKernel,1),...
										repmat(etaDisc(etaDisc>=0),numtKernel,1),...
										reshape(repmat(1:numtKernel,numel(etaDisc(etaDisc>=0)),1),[numtKernel*numel(etaDisc(etaDisc>=0)),1]))...
								,1,numel(etaDisc(etaDisc>=0)),numtKernel);

							G_xil_eta(:,etaDisc>=0,:) = ...
								zeros(size(xiMat(:,etaDisc>=0,:))) + Hetaeta...
									+ reshape(kem.r{3},1,1,[]).*Getaeta ...
									+ sqrt(kem.lambda_j(1))*(0*GA0EtaEta - A0EtaEta);
						else % dirichlet
							G_xil_eta(:,etaDisc>=0,:) = ...
								zeros(size(xiMat(:,etaDisc>=0,:))) - Hetaeta;
						end
					else % s=-1
						G_xil_eta(:,etaDisc>=0,:) = ...
							zeros(size(xiMat(:,etaDisc>=0,:))) + reshape(g_diff,1,[]) ...
							 - Hetaeta;
					end
					if any(etaDisc<0)
						HxilEta = ...
							reshape(...
									HInterp.eval(...
										repmat(kem.xi_l_sparse,numtKernel,1),...
										repmat(etaDisc(etaDisc < 0),numtKernel,1),...
										reshape(repmat(1:numtKernel,numel(etaDisc(etaDisc<0)),1),[numtKernel*numel(etaDisc(etaDisc<0)),1])...
									)...
									,1,numel(etaDisc(etaDisc<0)),numtKernel...
								)...
							;
						G_xil_eta(:,etaDisc<0,:) = ...
							zeros(size(xiMat(:,etaDisc<0,:))) + ...
							r6xilEta - s*HxilEta;
					end
	
% 					if eta_ij >= 0
% 						if kem.s == 1
% 	% 						xi_eta_idx = get_idx(eta_ij,kem.xi(:,tIdx));
% 							if ppide.b_op1.b_n(j,j) ~= 0 % it is zero in the Dirichlet Case!
% 								Getaeta(1,eta_idx,tIdxAll) = ...
% 									H1Interp.evaluate(eta_ij,eta_ij,tIdxAll)+H2Interp.evaluate(eta_ij,eta_ij,tIdxAll)...
% 									+ reshape(kem.r{3}(tIdxR3),[1,1,numel(tIdxR3)]).*GInterp.evaluate(eta_ij,eta_ij,tIdxAll)...
% 									- reshape(ljA0(get_idx(eta_ij,xi_disc),eta_idx,tIdxA0),[1,1,numel(tIdxA0)])...									+ reshape(GA0(get_idx(eta_ij,xi_disc),eta_idx,tIdxAll),[1,1,numel(tIdxAll)])...
% 									;
% 							else % dirichlet Case
% 								Getaeta(1,eta_idx,tIdxAll) = -(H1Interp.evaluate(eta_ij,eta_ij,tIdxAll)+H2Interp.evaluate(eta_ij,eta_ij,tIdxAll));
% 							end
% 						else
% 							Getaeta(1,eta_idx,tIdxAll) =...
% 								g_diff(eta_idx)-(H1Interp.evaluate(eta_ij,eta_ij,tIdxAll)...
% 								+H2Interp.evaluate(eta_ij,eta_ij,tIdxAll));
% 						end
% 					else
% 						Getaeta(1,eta_idx,tIdxAll) = ...
% 							reshape(r6_Interp.evaluate(zl,tIdxr6),[1,1,numel(tIdxr6)]) - s*(H1Interp.evaluate(xi_l,eta_ij,tIdxAll)...
% 							+H2Interp.evaluate(xi_l,eta_ij,tIdxAll));
% 						% H2=0 at the lower boarder so it is basically not needed here.
% 					end
				
				
				
			if kem.s==1 && ppide.b_op1.b_n(j,j) ~= 0
				% KA0 calculated with new methods so less loops required!
				G_xil_eta(:,kem.eta>=0,:) = G_xil_eta(:,kem.eta>=0,:) + KA0EtaEta;
			end
			
			G_eta0.value{i,j} = 1/4/s*(...
				numeric.cumtrapz_fast_nDim(xiMat,(...
					Giksum+(1-quasiStatic)*G_t+cH-lFXiEta+KFXiEta...
					),1,'equal','ignoreNaN')...	
				)...
				+ G_xil_eta;
		end
	end
else % now folded
	Giksum = cell(n,n);
	GInterp = cell(n,n);
	HInterp = cell(n,n);
	JInterp = cell(n,n);
	G = cell(n,n,n,n);
	kernel = obj;
	for i = 1:n
		for j=1:n
			GInterp{i,j} = numeric.interpolant({kernel.value{i,j}.xi,kernel.value{i,j}.eta,1:numtKernel},kernel.value{i,j}.G);
			HInterp{i,j} = numeric.interpolant({kernel.value{i,j}.xi,kernel.value{i,j}.eta,1:numtKernel},kernel.value{i,j}.H);
			JInterp{i,j} = numeric.interpolant({kernel.value{i,j}.xi,kernel.value{i,j}.eta,1:numtKernel},kernel.value{i,j}.J);
			for k=1:n						
				% store interpolations of J
% 				xiMatXi = kernel.xiMatXi{i,k,i,j}; % xi_ij(xi_ik,eta_ik)
% 				etaMatXi = kernel.etaMatXi{i,k,i,j}; % eta_ij(xi_ik,eta_ik)
% 	% 			JAll = misc.mydiag(JInterp.evaluate(xiMatXi(:),etaMatXi(:),1:numtKernel));
% 				JAll = JInterp.eval(repmat(xiMatXi(:),numtKernel,1),repmat(etaMatXi(:),numtKernel,1),...
% 						reshape(repmat(1:numtKernel,numel(xiMatXi),1),[numtKernel*numel(xiMatXi),1]));
% 				% J_ij(xi_ik,eta_ik)
% 				J{i,j,i,k} = reshape(JAll,size(xiMatXi,1),size(xiMatXi,2),numtKernel);
% 				% store interpolations of H
% 	% 			HAll = misc.mydiag(HInterp.evaluate(xiMatXi(:),etaMatXi(:),1:numtKernel),1:numel(xiMatXi),1:numel(xiMatXi));
% 				HAll = HInterp.eval(repmat(xiMatXi(:),numtKernel,1),repmat(etaMatXi(:),numtKernel,1),...
% 						reshape(repmat(1:numtKernel,numel(xiMatXi),1),[numtKernel*numel(xiMatXi),1]));
% 				% H_ij(xi_ik,eta_ik)
% 				H{i,j,i,k} = reshape(HAll,size(xiMatXi,1),size(xiMatXi,2),numtKernel);
				for l=1:n				
					% store interpolations of G
					xiMatXi = kernel.xiMatXi{k,l,i,j}; % xi_ij(xi_kl,eta_kl)
					etaMatXi = kernel.etaMatXi{k,l,i,j}; % eta_ij(xi_kl,eta_kl)
					% Important! Do not evaluate Interpolant with a grid! This creates far too many
					% data points!
					GAll = GInterp{i,j}.eval(repmat(xiMatXi(:),numtKernel,1),repmat(etaMatXi(:),numtKernel,1),...
						reshape(repmat(1:numtKernel,numel(xiMatXi),1),[numtKernel*numel(xiMatXi),1]));
					% G_ij(xi_kl,eta_kl)
					G{i,j,k,l} = reshape(GAll,size(xiMatXi,1),size(xiMatXi,2),numtKernel);
				end
			end
		end
	end

	Azeta = kernel.Azeta;      % Azeta{i,j,k,j} = Aij(zeta(xi_kj,eta_kj))
	MuZ = kernel.MuZ;		   % MuZ{i,j,i,k} =  muij(z(xi_ik,eta_ik))
	lambdaZeta = kernel.lambdaZeta;  % lambdaZeta{k,i,j} = lambda_k(zeta(xi_ij,eta_ij))
	
	z0 = ppide.ctrl_pars.foldingPoint;
	if isscalar(z0)
		z0 = ones(n/2,1)*z0;
	end
	for i=1:n
		for j=n/2+1:n
			kem = obj.value{i,j};
			GH = obj.value;
			Hij = kem.H;
			s = kem.s;
			xiMat = kernel.xiMatXi{i,j,i,j};
			if size(xiMat,3)==1
				xiMat = repmat(xiMat,1,1,numtKernel);
			end
% 			etaMat = kernel.etaMatXi{i,j,i,j};
% 			if size(etaMat,3)==1
% 				etaMat = repmat(etaMat,1,1,numtKernel);
% 			end
			xiDisc = kem.xi;
			etaDisc = kem.eta;
			G_eta0.value{i,j} = zeros(size(xiMat));
			
% 			a_sigma = -(kem.aiZXieta-kem.ajZetaXieta);
			a_delta = kem.aiZXieta+kem.ajZetaXieta;
			
			% preallocate all
			Ji = zeros(size(xiMat));
			Jii = zeros(size(xiMat));
			Jiii = zeros(size(xiMat));
			
			if  kem.lambda_i(1) >= GH{i,j}.lambda_j(1) && kem.lambda_i(1) >= GH{i,j-n/2}.lambda_j(1)
				% interpolate on (eta,eta)
				Ji(:,etaDisc>=0,:) = Ji(:,etaDisc>=0,:)...
					+(2*GH{i,j-n/2}.r{13}*z0(j-n/2)/(1-z0(j-n/2))-1)*reshape(HInterp{i,j}.eval(...
						repmat(etaDisc(etaDisc>=0),numtKernel,1),...
						repmat(etaDisc(etaDisc>=0),numtKernel,1),...
						reshape(repmat(1:numtKernel,numel(etaDisc(etaDisc>=0)),1),numtKernel*numel(etaDisc(etaDisc>=0)),1)),...
					1,[],numtKernel);
				
				% xi_j-n(eta,eta) und eta_j-n(eta,eta)
				xiMatXi = obj.xiMatXi{i,j,i,j-n/2}; % xij-n(xi,eta)
				xiMatXiFull = misc.writeToOutside(xiMatXi);
				xiMatInterp = numeric.interpolant({xiDisc,etaDisc,1:numtL},xiMatXiFull);
				etaMatXi = obj.etaMatXi{i,j,i,j-n/2}; % xij-n(xi,eta)
				etaMatXiFull = misc.writeToOutside(etaMatXi);
				
% 				% then evaluate at correct point
% 				% pairwise evaluation. xi(xi,eta), eta(xi,eta)
% 				xiMatXi = kernel.xiMatXi{i,j,i,j-n/2}; % xi_ij-n/2(xi_ij,eta_ij)
% 				etaMatXi = kernel.etaMatXi{i,j,i,j-n/2}; % eta_ij-n/2(xi_ij,eta_ij)
% 				Hijmn_all = HInterp{i,j-n/2}.eval(...
% 					repmat(xiMatXi(:),numtKernel,1),...
% 					repmat(etaMatXi(:),numtKernel,1),...
% 					reshape(repmat(1:numtKernel,numel(xiMatXi),1),[numtKernel*numel(xiMatXi),1]));
% 				% reshape to correct format:
% 				% integral evaluated for all(xiij,etaij)
% 				Hijmn = reshape(Hijmn_all,size(xiMatXi,1),size(xiMatXi,2),numtKernel);
% 				% integration inserts NaNs because of the NaN-grid! so remove them egain before
% 				% next interpolation
% 				Hijmn = misc.writeToOutside(Hijmn);
% 				HijmnInterp = numeric.interpolant({kem.xi,kem.eta,1:numtKernel},Hijmn);
% 				eta_disc = kem.eta;
% 				HijmnEtaEtaComp = HijmnInterp.eval(...
% 					repmat(eta_disc(eta_disc>=0),numtKernel,1),...
% 					repmat(eta_disc(eta_disc>=0),numtKernel,1),...
% 					reshape(repmat(1:numtKernel,numel(eta_disc(eta_disc>=0)),1),[numtKernel*numel(eta_disc(eta_disc>=0)),1]));
				
				etaMatInterp = numeric.interpolant({xiDisc,etaDisc,1:numtL},etaMatXiFull);
				xiEtaEtaTemp = xiMatInterp.eval(repmat(etaDisc(etaDisc>=0),numtL,1),repmat(etaDisc(etaDisc>=0),numtL,1),...
					reshape(repmat(1:numtL,numel(etaDisc(etaDisc>=0)),1),numtL*numel(etaDisc(etaDisc>=0)),1));
				xiEtaEta = reshape(xiEtaEtaTemp,numel(etaDisc(etaDisc>=0)),numtL);
				etaEtaEtaTemp = etaMatInterp.eval(repmat(etaDisc(etaDisc>=0),numtL,1),repmat(etaDisc(etaDisc>=0),numtL,1),...
					reshape(repmat(1:numtL,numel(etaDisc(etaDisc>=0)),1),numtL*numel(etaDisc(etaDisc>=0)),1));
				etaEtaEta = reshape(etaEtaEtaTemp,numel(etaDisc(etaDisc>=0)),numtL);
			
% 				HijmnEtaEta = reshape(HInterp{i,j-n/2}.eval(repmat(xiEtaEta,numtKernel,1),repmat(etaEtaEta,numtKernel,1),...
% 						reshape(repmat(1:numtKernel,numel(xiEtaEta),1),numtKernel*numel(xiEtaEta),1)),...
% 					1,[],numtKernel);
				Jii(:,etaDisc>=0,:) = Jii(:,etaDisc>=0,:)...
					+ (1- GH{i,j-n/2}.r{12})*z0(j-n/2)/(1-z0(j-n/2))*...
					reshape(HInterp{i,j-n/2}.eval(repmat(xiEtaEta,numtKernel,1),repmat(etaEtaEta,numtKernel,1),...
						reshape(repmat(1:numtKernel,numel(xiEtaEta),1),numtKernel*numel(xiEtaEta),1)),...
					1,[],numtKernel);
			end
			
			if kem.lambda_i(1) < GH{i,j}.lambda_j(1) || kem.lambda_i(1) < GH{i,j-n/2}.lambda_j(1)
				g_diff = diff_num(etaDisc(etaDisc>=0),eval_pointwise(ppide.ctrl_pars.g_strich{i,j},etaDisc(etaDisc>=0)));
				Jiii(:,etaDisc>=0,:) = Jiii(:,etaDisc>=0,:)...
					+ reshape(g_diff,1,length(etaDisc(etaDisc>=0)))...  
					- reshape(HInterp{i,j}.eval(...
						repmat(etaDisc(etaDisc>=0),numtKernel,1),...
						repmat(etaDisc(etaDisc>=0),numtKernel,1),...
						reshape(repmat(1:numtKernel,numel(etaDisc(etaDisc>=0)),1),numtKernel*numel(etaDisc(etaDisc>=0)),1)),...
					1,[],numtKernel);
			end
			
			if kem.lambda_i(1) ~= kem.lambda_j(1)
				xilEta = kem.xi_l_sparse;
				zVec = kem.zMatInterp{1}.eval(xilEta,etaDisc(etaDisc<0));
				eta_l_diffInterp = kem.eta_l_diffInterp;
				% avoid repmat by Ji+
				Ji(:,etaDisc<0,:) = Ji(:,etaDisc<0,:) + reshape(kem.r{7}.eval({zVec,linspace(0,1,numtKernel)})...
					*1./(1-s*eta_l_diffInterp.eval(xilEta)),1,[],numtKernel);
			end
			Giksum{i,j} = zeros(size(xiMat));
			for k=1:n
				% compute sums
				Giksum{i,j} = Giksum{i,j} + lambdaZeta{j,i,j}./lambdaZeta{k,i,j}.*G{i,k,i,j}.*Azeta{k,j,i,j}...
					+ MuZ{i,k,i,j}.*G{k,j,i,j};
			end
			Jiiii = numeric.cumtrapz_fast_nDim(xiMat,...
				-1/4*a_delta.*Hij + s/4*Giksum{i,j} + s/4*kem.G_t,1,'equal','ignoreNaN');
			
			G_eta0.value{i,j} = Ji + Jii + Jiii + Jiiii;
		end % j=n/2+1:n
		for j=1:n/2 % here, J is given from the kernel, so no fixedPoint-iteration is applied!
			% set to zero so that the fixedpoint iteration can be applied for all i,j so no
			% case distinction is needed, because kernel does not know if he is folded.
			G_eta0.value{i,j} = zeros(size(obj.value{i,j}.G));
		end
	end % i=1:n
	

end % folded	

fprintf([' (' num2str(round(toc,2)) 's)'])
end % function

