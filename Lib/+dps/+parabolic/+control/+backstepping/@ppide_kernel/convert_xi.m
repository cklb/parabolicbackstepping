function [xi, eta] = convert_xi(kernel,i,j,xi_kl,eta_kl,k,l)
%ppide_kernel.CONVERT_XI convert xi_coordinates from kl to ij representation

% history:
% created on 11.04.2016 by Simon Kerschbaum


xi_kl = xi_kl(:);
eta_kl = eta_kl(:);

if size(xi_kl,1) ~= size(eta_kl,1)
	error('vectors xi, eta must be of same length!')
end

[z,zeta] = kernel.value{k,l}.get_z(xi_kl,eta_kl);	
[xi,eta] = kernel.value{i,j}.get_xi(z,zeta);

% WEGEN PERFORMANCE EVTL LIEBER SO MACHEN

% if i<= j
% 	xi = phi_i(z) + phi_j(zeta);
% 	eta = phi_i(z) - phi_j(zeta);
% else
% 	xi = phi_i(1)
% 	eta = -1+lambda_i+z-lambda_i*zeta;
% end
end

