function plot(obj,var,varargin)
%PLOT plot kernel Entries
% var can be 'G','H','K','err','errG','errb','dz_K_1_zeta','dzeta_K_z_0'
% plot(ppide_kernel,var,...) plots the variable named var
% plot(ppide_kernel,var,'samples',samples,...) uses a maximum of 'samples' data points
% plot(ppide_kernel,var,'parent',parent,...) plots into a given figure 'parent'
% plot(ppide_kernel,var,'showCharacteristics',1,...) plot the lines eta_const
% plot(ppide_kernel,var,'plotPars',plotPars,...) 
% further options:
%  - contour (boolean) show contour lines (only for G/H)
%   passes the comma separated list of plotPars to the plot/surf/whatever used function.
%   Specify plotPars as a cell array of strings, as you would pass the stings to surf/etc.
%   Example: plotPars = {'edgeColor','none'}

% created on 15.09.2016 by Simon Kerschbaum
% modified on 18.04.2017 by SK:
%  - implementation for ppides

% TODO: Auf Matlab input-parser umstellen.
found=0;
passed = [];
arglist = {'samples','parent','showCharacteristics','plotPars','contour','numPlots','showBoundaries','showGrid'};
if ~isempty(varargin)
	if mod(length(varargin),2) % uneven number
		error('When passing additional arguments, you must pass them pairwise!')
	end
	for index = 1:2:length(varargin) % loop through all passed arguments:
		for arg = 1:length(arglist)
			if strcmp(varargin{index},arglist{arg})
				passed.(arglist{arg}) = varargin{index+1};
				found=1;
				break
			end 
		end % for arg
		% argument wasnt found in for-loop
		if ~found
			error([varargin{index} ' is not a valid property to pass to plot!']);
		end
		found=0; % reset found
	end % for index
end 
if ~isfield(passed,'samples')
	passed.samples=0;
end
if ~isfield(passed,'showCharacteristics')
	passed.showCharacteristics = 0;
end
if ~isfield(passed,'contour')
	passed.contour = 0;
end
if ~isfield(passed,'parent')
	passed.parent = [];
end
if ~isfield(passed,'numPlots')
	passed.numPlots = 1;
end
if ~isfield(passed,'showGrid')
	passed.showGrid = 0;
end
if ~isfield(passed,'plotPars')
	passed.plotPars = {};
else
	if ~iscell(passed.plotPars) 
		error('plotPars must be passed as cell array of strings representing name-value-pairs!')
	end
end
if ~isfield(passed,'showBoundaries')
	passed.showBoundaries = 1;
end

if ~isa(var,'char')
	error('var must be a string!')
end
% if passed.contour > 0 && passed.numPlots>1
% 	warning('not plotting contours in time varying case!')
% end
% if passed.samples>0 && ~(strcmp(var,'G') || strcmp(var,'H'))
% 	warning('the samples option is only active for G or H plots.')
% end
import misc.*

n = size(obj.value,1);
arglist={'G','H','K','KI','err','errG','errb','dz_K_1_zeta','dzeta_K_z_0','growthAssumption','a0_til','a0_neu'};
isin = 0;
for a = 1:length(arglist)
	if strcmp(var,arglist{a})
		isin=1;
	end
end
if ~isin
	warning([var ' cannot be plotted!'])
	return
end

if strcmp(var,'a0_til')
	if size(obj.a0_neu,4)>1
		dps.hyperbolic.plot.matrix_4D(permute(obj.a0_til,[1 4 2 3]),'title','A0_neu');
	else
		dps.hyperbolic.plot.matrix_3D(obj.a0_til,'title','A0_til');
	end
	return
end
if strcmp(var,'a0_neu')
	if size(obj.a0_neu,4)>1
		dps.hyperbolic.plot.matrix_4D(permute(obj.a0_neu,[1 4 2 3]),'title','A0_neu');
	else
		dps.hyperbolic.plot.matrix_3D(obj.a0_neu,'title','A0_neu');
	end
	return
end

for i=1:n
	for j=1:n
		if strcmp(var,'G') || strcmp(var,'H') || strcmp(var,'errG') || strcmp(var,'J')
			coords = '(xi,eta)';
			texcoords{i,j} = ['$(\xi_{' num2str(i) num2str(j) '}' ',\eta_{' num2str(i) num2str(j) '})$'];
		else
		coords = '(z,zeta)';
		texcoords{i,j} = '$(z,\zeta)$';
		end
	end
end
if isempty(passed.parent)
	figure('Name',[var coords])
end
id = 1;
KIGes = obj.KI;
for i = 1:n
	for j=1:n
		zdisc = obj.value{i,j}.zdisc;
		if isempty(passed.parent)
			subplot(n,n,id)
		else
% 			op = get(fig,'outerposition');
			ax = subplot(n,n,id,'parent',passed.parent);
			axes(ax);
		end
		s = obj.value{i,j}.s;
		hold on
		if strcmp(var,'G') || strcmp(var,'H') || strcmp(var,'errG') || strcmp(var,'J')
			xlabel(['$\xi_{' num2str(i) num2str(j) '}$'],'color','k')
			ylabel(['$\eta_{' num2str(i) num2str(j) '}$'],'color','k')
		elseif strcmp(var,'dz_K_1_zeta')
			xlabel('$\zeta$','color','k')
			ylabel('$\partial_z K(1,zeta)$','color','k')
		elseif strcmp(var,'dzeta_K_z_0')
			xlabel('$z$','color','k')
			ylabel('$\partial_{\zeta} K(z,0)$','color','k')
		elseif strcmp(var,'errb')
			xlabel('$z$','color','k')
			ylabel('$err_b(z)$','color','k')
		else
			xlabel('$z$','color','k')
			ylabel('$\zeta$','color','k')
		end
		
		if strcmp(var,'G') || strcmp(var,'H') || strcmp(var,'errG') || strcmp(var,'J')
			xidisc = obj.value{i,j}.xi;
			etadisc = obj.value{i,j}.eta;
			[X,E] = meshgrid(xidisc,etadisc);
			if strcmp(var,'G') || strcmp(var,'H') || strcmp(var,'J')
				[G, H, xidisc, etadisc] = obj.value{i,j}.limit_to_valid(passed.samples);
				[X_RES,E_RES] = meshgrid(xidisc,etadisc);
			end
% 			G = obj.value{i,j}.G; %#ok<NASGU>
% 			H = obj.value{i,j}.H; %#ok<NASGU>
		elseif strcmp(var,'dz_K_1_zeta') || strcmp(var,'dzeta_K_z_0')
%  			zdisc = obj.value{i,j}.zdisc;
		elseif strcmp(var,'K') || strcmp(var,'KI')
			xidisc = obj.value{i,j}.zdisc;
			etadisc = obj.value{i,j}.zdisc;
			[X,E] = meshgrid(xidisc,etadisc);
			if passed.samples > 0
				x_res = linspace(0,1,passed.samples);
				e_res = x_res;
				[X_RES,E_RES] = meshgrid(x_res,e_res);
			else
				
				X_RES = X;
				E_RES = E;
			end
		else
			xidisc = obj.value{i,j}.zdisc;
			etadisc = obj.value{i,j}.zdisc;
			[X,E] = meshgrid(xidisc,etadisc);
			X_RES = X;
			E_RES = E;
		end
		K = obj.value{i,j}.K;
		numtKernel = size(obj.value{i,j}.K,3);
		KInterp = numeric.interpolant({zdisc,zdisc,linspace(0,1,numtKernel)},K); %#ok<NASGU>
		if strcmp(var,'KI')
			KI = squeeze(KIGes(:,:,i,j,:));%#ok<NASGU>
		end
		err = obj.value{i,j}.err;%#ok<NASGU>
		errG = obj.value{i,j}.errG;%#ok<NASGU>
		errb = obj.value{i,j}.errb;%#ok<NASGU>
		dz_K_1_zeta = obj.value{i,j}.dz_K_1_zeta;%#ok<NASGU>
		dzeta_K_z_0 = obj.value{i,j}.dzeta_K_z_0;%#ok<NASGU>
		ndisc = length(obj.value{i,j}.zdisc);
		if strcmp(var,'dz_K_1_zeta') || strcmp(var,'dzeta_K_z_0')
			dzetaK = zeros(ndisc,ndisc,numtKernel);
			for zIdx = 2:ndisc
				dzetaK(zIdx,1:zIdx,:) = numeric.diff_num(zdisc(1:zIdx),obj.value{i,j}.K(zIdx,1:zIdx,:),2);
			end
			dzetaK(1,1,:) = dzetaK(2,1,:);
			dzK = zeros(ndisc,ndisc,numtKernel);
			for zetaIdx = 1:ndisc-1
				dzK(zetaIdx:end,zetaIdx,:) = numeric.diff_num(zdisc(zetaIdx:end),obj.value{i,j}.K(zetaIdx:end,zetaIdx,:),1);
			end
			dzK(end,end,:) = dzK(end,end-1,:);
		end
		
	
		if strcmp(var,'growthAssumption')
			gamma = 0.95;
			Z = zdisc - gamma*zdisc.';
		else
			vn = matlab.lang.makeValidName(var);
			eval(['Z =' vn ';']); % whichever will be plotted is now stored in Z	
		end
		if passed.numPlots>1 && size(Z,3)==1
			warning('Requested numPlots=3, but object is time-invariant.')
			passed.numPlots=1;
		end
			
		if passed.samples > 0
			if strcmp(var,'K')||strcmp(var,'KI')
				ZInterp = numeric.interpolant({obj.zdisc,obj.zdisc,linspace(0,1,size(Z,3))},Z);
				Z = ZInterp.evaluate(xidisc,etadisc,linspace(0,1,size(Z,3)));
				ndisc = length(xidisc);
			end
		end
		
		
		if strcmp(var,'errb')
			hold on
			title(['$' var '_{' num2str(i) num2str(j) '}$' texcoords{i,j}])
			if size(Z,2)>1
				coldiff = 255/(size(Z,2)-1);
			else
				coldiff=255;
			end
			for k = 1:size(Z,2)
				blue=(k-1)*coldiff;
				green = (k-1)*coldiff;
				red = 255-(k-1)*coldiff;
				plot(xidisc,Z{k},'Color',[red/255 green/255 blue/255]);
			end
		elseif strcmp(var,'dz_K_1_zeta') 
			if passed.samples==0
				plot(zdisc,Z,'DisplayName','analytic');
				hold on
				plot(zdisc,squeeze(dzK(end,:,:)),'DisplayName','numeric');
			else
				plot_res(zdisc,Z,passed.samples,'DisplayName','analytic');
				hold on
				plot_res(zdisc,squeeze(dzK(end,:,:)),'DisplayName','numeric');
			end
			legend('-dynamicLegend')
		elseif strcmp(var,'dzeta_K_z_0')
			if passed.samples==0
				plot(zdisc,Z,'DisplayName','analytic');
				hold on
				plot(zdisc,squeeze(dzetaK(:,1,:)),'DisplayName','numeric');
			else
				plot_res(zdisc,Z,passed.samples,'DisplayName','analytic');
				hold on
				plot_res(zdisc,squeeze(dzetaK(:,1,:)),'DisplayName','numeric');
			end
			legend('-dynamicLegend')
		else
			sortIdc = [];
% 			Z = Z.';
			hold on
			if ~isempty(passed.parent)
				set(gca,'NextPlot','replace')
				% hier werden dann die Achsenbeschriftungen entfernt!
				% M�ssten an einer sinnvollen Stelle noch wieder eingef�gt
				% werden!
			end
			if passed.samples==0
				if ~strcmp(var,'growthAssumption')					
					if size(Z,3)>1
						hold on
						if passed.numPlots>1 
							jDisc = linspace(0,1,passed.numPlots);
						else
							jDisc = 0; % in that case, t=0 shall be evaluated
						end
						tDisc = linspace(0,1,size(Z,3));
						jIdx = get_idx(jDisc,linspace(0,1,size(Z,3)));
						opacityVec = 1/(passed.numPlots)*[1:passed.numPlots];
						for k=1:passed.numPlots
							meanVec(k) = nanmean(nanmean(Z(:,:,jIdx(k))));
						end
						[~,sortIdc] = sort(meanVec,'descend');
						opacityVec = opacityVec(sortIdc);
						for k=1:passed.numPlots
							opacity = opacityVec(k);
							if strcmp(var,'K')
%					 ATTENTION:
% normally I would simply use FaceColor interp witout eliminating the edgeColor but this would
% lead to shader=faceted interp in tikz. This, however, creates huge files which are way larger
% that plotting the grid over the surf.
								if passed.showGrid
									handles(k) = misc.plotLowerTriangular(zdisc,Z(:,:,jIdx(k)),passed.plotPars{:}...
										,'faceAlpha',opacity,'edgeColor','none'...
										);
									misc.plotLowerTriangular(zdisc,Z(:,:,jIdx(k)),passed.plotPars{:}...
										,'faceColor','none'...
										);
								else
									handles(k) = misc.plotLowerTriangular(zdisc,Z(:,:,jIdx(k)),passed.plotPars{:}...
										,'faceAlpha',opacity,'edgeColor','none'...
										);
								end
								str{k} = ['t = ' sprintf('%.1f',tDisc(jIdx(k)))];
							else
								handles(k) = misc.surfSparse(xidisc,etadisc,Z(:,:,jIdx(k)),passed.plotPars{:}...
									,'faceAlpha',opacity...
									);
								str{k} = ['t = ' sprintf('%.1f',tDisc(jIdx(k)))];
							end
							view(45,30)
						end
						% jet is used because it will not create a colormap on matlab2tikz
							% export!
% 						if passed.contour && strcmp(var,'K')
% 							ZNew = Z;
% 							for zIdx=1:ndisc
% 								ZNew(zIdx,zIdx+1:end,1) = NaN;
% 							end
% 							if ~isempty(SortIdc)
% 								[~, h] = contour(zdisc,zdisc,ZNew(:,:,jIdx(SortIdc(1))).',6,'k');
% 							else
% 								[~, h] = contour(zdisc,zdisc,ZNew(:,:,1).',6,'k');
% 							end
% 							h.ContourZLevel = max(ZNew(:));
% 						end
					else
						if strcmp(var,'K')
							plotLowerTriangular(zdisc,Z(:,:),passed.plotPars{:});
% 							misc.surfSparse(zdisc,zdisc,Z(:,:),passed.plotPars{:});
% 							if passed.contour
% 								ZNew = Z;
% 								for zIdx=1:ndisc
% 									ZNew(zIdx,zIdx+1:end) = NaN;
% 								end
% 								[~, h] = contour(zdisc,zdisc,ZNew.',6,'k');
% 								h.ContourZLevel = max(ZNew(:));
% 							end
						else
% 							surf(X,E,Z(:,:).',passed.plotPars{:});
							misc.surfSparse(xidisc,etadisc,Z(:,:),passed.plotPars{:});
% 							surf(X,E,Z(:,:).',passed.plotPars{:});
						end
						view(45,30)
					end
					colormap jet % viridis
					if ~isempty(passed.parent)
						set(gca,'NextPlot','add')
					end
					if strcmp(var,'errG')|| strcmp(var,'err')
						caxis([-1 1]);
						zlim([-1 1])
						colormap jet % default
						% jet is used because it will not create a colormap on matlab2tikz
						% export!
					end
				end
			else
% 				[Z_res,xi_res] = resample_time(Z,xidisc,passed.samples);
% 				[Z_res,eta_res] = resample_time(Z_res.',etadisc,passed.samples);
% 				Z_res = Z_res.';
% 				[X_res,E_res] = meshgrid(xi_res,eta_res);
% 				surf(X_res,E_res,Z_res,'edgeColor','none');
% 				shading interp;
				if strcmp(var,'errG') || strcmp(var,'err')
					caxis([-1 1]);
					zlim([-1 1])
					colormap default
				end
% 				strcmp(var,'G') || strcmp(var,'H') || strcmp(var,'K')
				if ~strcmp(var,'growthAssumption')					
					if size(Z,3)>1
						hold on
						if passed.numPlots>1 
							jDisc = linspace(0,1,passed.numPlots);
						else
							jDisc = 0; % in that case, t=0 shall be evaluated
						end
						tDisc = linspace(0,1,size(Z,3));
						jIdx = get_idx(jDisc,tDisc);
						opacityVec = 1/(passed.numPlots)*[1:passed.numPlots];
						for k=1:passed.numPlots
							meanVec(k) = nanmean(nanmean(Z(:,:,jIdx(k))));
						end
						[~,sortIdc] = sort(meanVec,'descend');
						opacityVec = opacityVec(sortIdc);
						for k=1:passed.numPlots
							opacity = opacityVec(k);
% 							mesh_sparse(xidisc,etadisc,Z(:,:,jIdx(k)).',20,20,passed.samples,passed.samples,'color',1,'grid',0,'plotPars',passed.plotPars);
							if strcmp(var,'K')
								if passed.showGrid
									handles(k) = misc.plotLowerTriangular(xidisc,Z(:,:,jIdx(k)),passed.plotPars{:}...
										,'faceAlpha',opacity,'edgeColor','none'...
										);
									misc.plotLowerTriangular(xidisc,Z(:,:,jIdx(k)),passed.plotPars{:}...
										,'faceColor','none'...
										);
								else
									handles(k) = misc.plotLowerTriangular(xidisc,Z(:,:,jIdx(k)),passed.plotPars{:}...
										,'faceAlpha',opacity,'edgeColor','none'...
										);
								end
								str{k} = ['t = ' sprintf('%.1f',tDisc(jIdx(k)))];
							else
								if passed.showGrid
									handles(k) = misc.surfSparse(xidisc,etadisc,Z(:,:,jIdx(k)),passed.plotPars{:}...
										,'faceAlpha',opacity...
										);
								else
									handles(k) = misc.surfSparse(xidisc,etadisc,Z(:,:,jIdx(k)),passed.plotPars{:}...
										,'faceAlpha',opacity...
										,'edgeColor','none'...
										);
								end
								str{k} = ['t = ' sprintf('%.1f',tDisc(jIdx(k)))];
							end
							colormap jet% viridis
							% jet is used because it will not create a colormap on matlab2tikz
							% export!
						end
					else
% 						mesh_sparse(xidisc,etadisc,Z.',20,20,passed.samples,passed.samples,'color',1,'grid',0,'plotPars',passed.plotPars);
						if strcmp(var,'K')
							plotLowerTriangular(xidisc,Z(:,:),passed.plotPars{:})
						else
							misc.surfSparse(xidisc,etadisc,Z(:,:),passed.plotPars{:});
						end
						colormap jet%  viridis
						% jet is used because it will not create a colormap on matlab2tikz
						% export!
					end
				end
				hold on
				view(2)
			end
			if passed.contour
				if strcmp(var,'G') || strcmp(var,'H')
					if ~isempty(sortIdc)
						[~, h] = contour(X_RES,E_RES,Z(:,:,jIdx(sortIdc(1))).',6,'k');
					else
						[~, h] = contour(X_RES,E_RES,Z(:,:,1).',6,'k');
					end
					h.ContourZLevel = max(Z(:));
				elseif strcmp(var,'K')
					ZNew = Z;
					for zIdx=1:ndisc
						ZNew(zIdx,zIdx+1:end,:) = NaN;
					end
					if ~isempty(sortIdc)
						[~, h] = contour(xidisc,etadisc,ZNew(:,:,jIdx(sortIdc(1))).',6,'k');
					else
						[~, h] = contour(xidisc,etadisc,ZNew(:,:,1).',6,'k');
					end
					h.ContourZLevel = max(ZNew(:));
				end
			end
% 			shading inter
			box on
% 			grid on
			title(['$' var '_{' num2str(i) num2str(j) '}$' texcoords{i,j}])
			if strcmp(var,'growthAssumption')
				ZGES = Z.';
			else
				if ~strcmp(var,'KI')
					if ~isempty(sortIdc)
						ZGES = obj.value{i,j}.(vn)(:,:,jIdx(sortIdc(1))).';
					else
						ZGES = obj.value{i,j}.(vn)(:,:,1).';
					end
				end
			end
			if ~isempty(passed.parent)
				view(3)
			end
		end
		if isempty(passed.parent)
% 			axis equal
		end
		colormap jet
		% plot borders in xi/eta-coordinates
		if passed.showBoundaries
			if strcmp(var,'G') || strcmp(var,'H')
				zdisc = obj.value{i,j}.zdisc;
				xidisc = obj.value{i,j}.xi;
				a = (s+1)/2 *obj.value{i,j}.phi_i(end) - (s-1)/2*obj.value{i,j}.phi_j(end); % a Ortsbereich
				b = s*(obj.value{i,j}.phi_i(end)-obj.value{i,j}.phi_j(end));
				c = obj.value{i,j}.phi_i(end) + obj.value{i,j}.phi_j(end);
				xlim([0 c]);

				if obj.value{i,j}.lambda_i(1)<obj.value{i,j}.lambda_j(1)
					a_str = ['$\phi_' num2str(j) '(1)$'];
	% 				b_str = ['$\phi_' num2str(j) '(1)-\phi_' num2str(i) '(1)$'];
				else
					a_str = ['$\phi_' num2str(i) '(1)$'];
	% 				b_str = ['$\phi_' num2str(i) '(1)-\phi_' num2str(j) '(1)$'];
				end			
				if obj.value{i,j}.lambda_i(1)~=obj.value{i,j}.lambda_j(1)
					c_str = '$c$';
				else
					c_str = ['\makebox[0pt]{$2\phi_' num2str(i) '(1)$}'];
				end
				% add tick: phi_i(1) or phi_j(1)
				if isempty(passed.parent)
					b_str = '$b$';

					%ticks
					oldtick = get(gca,'YTick');
					oldxtick= get(gca,'XTick');

					% diejenigen normalen Ticks die �bernommen werden sollen
					newytick = 0:1:oldtick(end);
	% 				newxtickold = 0:1:oldxtick(end);
					newxtickold = 0;
					set(gca, 'YTick',newytick);
					set(gca, 'XTick',newxtickold);

					yticklabelvec = get(gca, 'YTickLabel'); % alte labels speichern
					for idx = 1:length(yticklabelvec)
						yticklabelvec{idx} = ['$' yticklabelvec{idx} '$'];
					end
					xticklabelvec = get(gca, 'XTickLabel'); % alte labels speichern
					[newxtick, Ix] = unique([a c newxtickold]);
					if obj.value{i,j}.lambda_i(1)~= obj.value{i,j}.lambda_j(1)
						[ytickvecsorted, I] = unique([b, a, newytick, 1]);	
					else
						[ytickvecsorted, I] = unique([a, newytick]);
					end
		% 			[tickvecsorted,I] = sort(newtickvec);
					set(gca, 'YTick',ytickvecsorted);
					set(gca, 'XTick',newxtick);

					% labels
					for idx = 1:length(xticklabelvec)
						xticklabelvec{idx} = ['$' xticklabelvec{idx} '$'];
					end
					newxticklabelvec = [a_str; c_str; xticklabelvec].';
					if obj.value{i,j}.lambda_i(1)~=obj.value{i,j}.lambda_j(1)
						newyticklabelvec = [b_str; a_str;yticklabelvec;'1'].';			
					else
						newyticklabelvec = [a_str;yticklabelvec].';			
					end
					yticklabelssorted = newyticklabelvec(I).';
					xticklabelssorted = newxticklabelvec(Ix).';		
					set(gca, 'YTicklabels', yticklabelssorted);
					set(gca, 'XTicklabels', xticklabelssorted);
				end
				xlim([0 1.1*c]);
				% 1. line: (xi,xi)
				res_line= 50; % each line is plottet with 100 points
				% z0: xi,0
				for l_idx =1:res_line 
					x_0(l_idx) = 2*a*(l_idx-1)/(res_line-1); %#ok<*AGROW>
					y_0(l_idx) = 0;
				end
				z_0 = interp2(X,E,ZGES,x_0,y_0); % get height, to plot "above" the plot
				z_0 = z_0 + 0.01; % shift the line a bit to be surely "above"
				% z1: eta,eta
				for l_idx =1:res_line 
					x_1(l_idx) = a*(l_idx-1)/(res_line-1); %#ok<*AGROW>
					y_1(l_idx) = a*(l_idx-1)/(res_line-1);
				end
				z_1 = interp2(X,E,ZGES,x_1,y_1); % get height, to plot "above" the plot
				z_1 = z_1 + 0.01; % shift the line a bit to be surely "above"
				% 2. line: 2a-xi
				for l_idx =1:res_line 
					x_2(l_idx) = a + (obj.value{i,j}.phi_i(end)+obj.value{i,j}.phi_j(end)-a)*(l_idx-1)/(res_line-1);
					y_2(l_idx) = a - (obj.value{i,j}.phi_i(end)+obj.value{i,j}.phi_j(end)-a)*(l_idx-1)/(res_line-1); % obere Grenze
				end
				z_2 = interp2(X,E,ZGES,x_2,y_2);
				z_2 = z_2 + 0.01*abs(max(ZGES(:))-min(ZGES(:)));
				% 3. line: eta_l 
				for l_idx =1:res_line 
					x_3(l_idx) = (obj.value{i,j}.phi_i(end)+obj.value{i,j}.phi_j(end))*(l_idx-1)/(res_line-1);
					y_3(l_idx) = interp1(xidisc,obj.value{i,j}.eta_l.',numeric.acc(x_3(l_idx)));
				end
				z_3 = interp2(X,E,ZGES,x_3,y_3);
				z_3 = z_3 + 0.01;

				% use-userDefined colors for BCs so that can easily be changed in 
				% plot 3 boundaries separately
				mycol{1} = [255 10 10]/255; % red
				mycol{2} = [20 10 255]/255; %
				mycol{3} = [125 125 125]/255;
				mycol{4} = [10 255 100]/255;
				hold on
				if obj.value{i,j}.lambda_i(1)~=obj.value{i,j}.lambda_j(1)
					plot3(x_0,y_0,z_0,'color',mycol{1});
				end
				if obj.value{i,j}.s == -1
					% well-Posed-BC
					plot3(x_1,y_1,z_1,'color','white');
					plot3(x_1,y_1,z_1,':','color',mycol{4});				
				else
					plot3(x_1,y_1,z_1,'color',mycol{2});
				end
	% 			plot3(x_2,y_2,z_2,'color','white'); % no BC->
				if obj.value{i,j}.s == -1
					plot3(x_2,y_2,z_2,'color',mycol{2});
				else
					plot3(x_2,y_2,z_2,'color',mycol{4});
				end
				plot3(x_3,y_3,z_3,'color',mycol{3});
	% 			if obj.value{i,j}.lambda_i(1)~=obj.value{i,j}.lambda_j(1)
	% 				plot3(x_3,y_3-0.075*(max(etadisc)-min(etadisc)),z_3,'color',mycol{3});
	% 			end
			end
		end
		
		% plot separating line in z/zeta coordinates
		if passed.showBoundaries
			if strcmp(var,'K')||strcmp(var,'growthAssumption')
				zdisc = obj.value{i,j}.zdisc;
				xidisc = obj.value{i,j}.xi;
				etadisc = obj.value{i,j}.eta;
				ZInterp = numeric.interpolant({obj.zdisc,obj.zdisc,linspace(0,1,size(Z,3))},Z);
				xiMatXi = misc.writeToOutside(obj.xiMatXi{i,j,i,j});
				xiMatInterp = numeric.interpolant({obj.value{i,j}.xi,obj.value{i,j}.eta},xiMatXi);
				etaMatXi = misc.writeToOutside(obj.etaMatXi{i,j,i,j});
				etaMatInterp = numeric.interpolant({obj.value{i,j}.xi,obj.value{i,j}.eta},etaMatXi);
				clear z zeta z_val
				res_line=length(zdisc);
				res_line_sep=51;
	% 			if strcmp(var,'growthAssumption')
					% plot multiple lines
				numLinesEta = 10;
				eta_vals = interp1(etadisc,etadisc,linspace(etadisc(1),etadisc(end),numLinesEta));
	% 			end
				mycol{1} = [255 10 10]/255; % red
				mycol{2} = [20 10 255]/255; %
				mycol{3} = [125 125 125]/255;
				mycol{4} = [10 255 100]/255;
				if obj.value{i,j}.lambda_i(1)~=obj.value{i,j}.lambda_j(1)
					s = obj.value{i,j}.s;
					a = (s+1)/2 *obj.value{i,j}.phi_i(end) - (s-1)/2*obj.value{i,j}.phi_j(end);
					xiH = xiMatInterp.eval(2*a,0); % for eta=0
					% reset
					xi_l_idx_eta = 0;
					xi_h_eta = 0;
					xi_l_eta = 0;
					xiLInterp = numeric.interpolant({obj.value{i,j}.eta_l},obj.value{i,j}.xi_l);
					xiHEta = xiMatInterp.eval(2*a-eta_vals,eta_vals);
					xiLEta(eta_vals>=0) = xiMatInterp.eval(eta_vals(eta_vals>=0),eta_vals(eta_vals>=0));
					xiLEta(eta_vals < 0) = xiLInterp.eval(eta_vals(eta_vals<0));
					for etaIdx =1:length(eta_vals)
						xi_h_idx_eta(etaIdx) = find(numeric.acc(xidisc)<=(numeric.acc(2*a-eta_vals(etaIdx))),1,'last'); % highes index of xi for eta
						xi_h_eta(etaIdx) = xidisc(xi_h_idx_eta(etaIdx));
						if eta_vals(etaIdx) >= 0
							xi_l_idx_eta(etaIdx) = find(numeric.acc(xidisc)>=(eta_vals(etaIdx)),1); % highes index of xi for eta
						else
							eta_l_idx = get_idx(eta_vals(etaIdx),obj.value{i,j}.eta_l);
							try
								xi_l_idx_eta(etaIdx) = find(numeric.acc(xidisc)>=numeric.acc(obj.value{i,j}.xi_l(eta_l_idx)),1); % highes index of xi for eta
							catch 
								here=1;
							end
						end
						xi_l_eta(etaIdx) = xidisc(xi_l_idx_eta(etaIdx));
					end
					% plot separating lines, resp. lines for constant eta in original
					% coordinates
					xiVec = linspace(0,xiH,res_line_sep);
					z = obj.value{i,j}.zMatInterp{1}.eval({xiVec,0});
					zeta = obj.value{i,j}.zetaMatInterp{1}.eval({xiVec,0});
					try
						if ~isempty(sortIdc)
							zVal = KInterp.eval(z,zeta,repmat(tDisc(jIdx(sortIdc(1))),length(z),1));
						else
							zVal = KInterp.eval(z,zeta,0);
						end
						
					catch errmsg
						warning(mythrow(errmsg));
					end
						
					xiVecEta = zeros(res_line_sep,length(eta_vals));
					for etaIdx =1:length(eta_vals)
						xiVecEta(:,etaIdx) = linspace(xiLEta(etaIdx),xiHEta(etaIdx),res_line_sep);
					end
					zEta = reshape(...
							obj.value{i,j}.zMatInterp{1}.eval(xiVecEta(:),reshape(repmat(eta_vals,size(xiVecEta,1),1),[],1))...
						,size(xiVecEta,1),[]);
					zetaEta = reshape(...
							obj.value{i,j}.zetaMatInterp{1}.eval(xiVecEta(:),reshape(repmat(eta_vals,size(xiVecEta,1),1),[],1))...
						,size(xiVecEta,1),[]);
					if ~isempty(sortIdc)
						zValEta = reshape(KInterp.eval(zEta(:),zetaEta(:),...
							repmat(tDisc(jIdx(sortIdc(1))),numel(zEta),1))...
							,size(zEta,1),[]);
					else
						zValEta = reshape(KInterp.eval(zEta(:),zetaEta(:),...
							zeros(numel(zEta),1))...
							,size(zEta,1),[]);
					end
								
					zVal = zVal + 0.2*abs((max(zVal)-min(zVal)));
					zValEta = zValEta + 0.2*abs((max(zValEta)-min(zValEta)));
					hold on
					plot3(z,zeta,zVal,'color',mycol{1},'lineWidth',2);
					if passed.showCharacteristics
						for etaIdx =1:length(eta_vals)
							plot3(zEta(:,etaIdx),zetaEta(:,etaIdx),zValEta(:,etaIdx),'color',mycol{1});
						end
					end
				end
				for l_idx =1:res_line
					x_1(l_idx) = (l_idx-1)/(res_line-1); %#ok<*AGROW>
					y_1(l_idx) = (l_idx-1)/(res_line-1); % K(z,z)
					x_2(l_idx) = (l_idx-1)/(res_line-1); %#ok<*AGROW>
					y_2(l_idx) = 0; % K(z,0)
					x_3(l_idx) = 1;
					y_3(l_idx) = (l_idx-1)/(res_line-1); %#ok<*AGROW>
				end
				% (z,z)
				z_1 = interp2(X,E,ZGES(:,:,1),x_1,y_1); % get height, to plot "above" the plot
				z_1 = z_1 + 0.01*abs(max(ZGES(:))-min(ZGES(:))); % shift the line a bit to be surely "above"
				% (z,0)
				z_2 = interp2(X,E,ZGES(:,:,1),x_2,y_2); % get height, to plot "above" the plot
				z_2 = z_2 + 0.01*abs(max(ZGES(:))-min(ZGES(:))); % shift the line a bit to be surely "above"
				% (1,zeta)
				z_3 = interp2(X,E,ZGES(:,:,1),x_3,y_3); % get height, to plot "above" the plot
				z_3 = z_3 + 0.01*abs(max(ZGES(:))-min(ZGES(:))); % shift the line a bit to be surely "above"
				hold on
				plot3(x_1,y_1,z_1,'color',mycol{4});
	% 			if obj.value{i,j}.lambda_i(1)~=obj.value{i,j}.lambda_j(1)
	% 				plot3(x_1,y_1+0.06,z_1,'color','black');
	% 			end
				if obj.value{i,j}.s==-1
					plot3(x_3,y_3,z_3,'color','white');
					plot3(x_3,y_3,z_3,':','color',mycol{3});
				else
					plot3(x_3,y_3,z_3,'color',mycol{3});
				end
				plot3(x_2,y_2,z_2,'color',mycol{2});
	% 			xlim([0 1.2])
			end
		end
		if passed.numPlots>1
			legend(handles,str);
		end
		id = id+1;
	end
end

end

