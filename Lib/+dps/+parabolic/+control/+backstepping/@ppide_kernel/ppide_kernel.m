classdef ppide_kernel
	%PPIDE_KERNEL ppide_kernel
	%   only contains the cell array of the kernel elements but is needed
	%   to define the plus function for the cell array.
	
	% created on 03.04.2017 by Simon Kerschbaum
	% modified on 14.03.2018 by Simon Kerschbaum:
	%   * include a0_til in kernel
	
	
	properties
		value = cell(2,2); % kernel elements K,G,H
		zdisc; % spatial discretization
		tDisc; % temporal discretization
		iteration=1; % number of iterations (plus one) used for fixed-point iteration
		transformed = 0;
		checked_PDE = 0;
		checked_boundaries = 0;
		inverted = 0;
		a0_til;                 % coupling matrix state feedback
		a0_neu;					% neumann coupling matrix needed for folding
		KI;
		a0_bar;                 % coupling matrix observer
		zInterp    % cell arrarys zInterp{i,j,k,l} wich contain the interpolants to interpolate 
		zetaInterp % zMat, which is defined over xi_ij, eta_ij over xi_kl, eta_kl.
% 		xiInterp
% 		etaInterp
		xiMatXi    % cell array xiMatXi{i,j,k,l} stores the transformation of the coordinates 
				   % xi_kl(xi_ij,eta_ij)
		etaMatXi   % xi_ij,eta_ij into the coordinates xi_kl,eta_kl.
				   % eta_kl(xi_ij,eta_ij)
		aimjMat    % cell array that stores a_i-a_j discretized over xi,eta
		Azeta      % Azeta{i,j,k,j} = Aij(zeta(xi_kj,eta_kj))
		MuZ		   % MuZ{i,j,i,k} =  muij(z(xi_ik,eta_ik))
		lambdaZeta % lambdaZeta{k,i,j} = lambda_k(zeta(xi_ij,eta_ij))
		a0Zeta     % a0(zeta,i,j,t)
		
		avgTime    % average time required for one fixed-point iteration
	end
	
	methods
		plot(obj,var,varargin);
	end
	
	methods
		% [xi,eta] = convert_xi(kernel,i,j,xi_kl,eta_kl,k,l); % Evtl noch Eigenschaften des Objekts �bergeben f�r Speed
		%CONVERT_XI convert xi_coordinates from kl to ij representation
		% [eta,xi] = convert_eta(kernel,i,j,xi_kl,eta_kl,k,l);
		%CONVERT_ETA convert eta_coordinates from kl to ij representation
		GH = init_kernel_elements(obj,ppde,zdisc);
		GH = init_kernel_elementsFolding(obj,ppde,zdisc);
		kernel = refine(obj,factor);
		obj = transform_kernel(obj,ppde);
		KI = invert_kernel(obj, allowNan);
		G_eta0 = get_G_eta0(obj,ppide);
		G_eta0 = get_G_eta0Folding(obj,ppide);
		GH = getFixedPointStartFolding(obj,ppide);
		test = checkControllerKernel(controllerKernel, ppide, plotResult)
		
		function G_eta = use_F_Geta(obj,GetaOld)
			% integral term of the fixed-point iteration for G_eta(xi,eta)
			n = size(obj.value,1);
			G_eta = misc.myCell(n,n); 
			numT = size(obj.value{1,1}.G,3);
			for i=1:n
				for j=1:n
					xiMat = obj.xiMatXi{i,j,i,j};
					aijMat = obj.value{i,j}.aiZXieta-obj.value{i,j}.ajZetaXieta;
					if size(xiMat,3) == 1 && numT > 1
						xiMat = repmat(xiMat,1,1,numT);
					end
					if size(aijMat,3) == 1 && numT > 1
						aijMat = repmat(aijMat,1,1,numT);
					end
					xi_disc = obj.value{i,j}.xi;
					nxi = size(xi_disc,1);
					eta_disc = obj.value{i,j}.eta;
					neta = size(eta_disc,1);
					G_eta.value{i,j} = zeros(nxi,neta,numT);
					% Problem: keine NaNs in xiMatXi!
					G_eta.value{i,j} = -1/4/obj.value{i,j}.s*...
						numeric.cumtrapz_fast_nDim(xiMat,aijMat.*GetaOld.value{i,j},1,'equal','ignoreNaN');
				end
			end
			
		end
		
		function obj = plus(a,b)
			% Elementwise addition of kernel elements
			obj = a; % Datenuebernahme
			for i=1:size(obj.value,1)
				for j=1:size(obj.value,2)
					obj.value{i,j} =  a.value{i,j}+b.value{i,j};
					obj.iteration = a.iteration+b.iteration;
				end
			end
		end
		
		function obj = abs(kernel)
			% Elementwise abs of the kernel elements
			obj = kernel; %Datenuebernahme
			for i=1:size(obj.value,1)
				for j=1:size(obj.value,2)
					obj.value{i,j}.G =  abs(kernel.value{i,j}.G);
					obj.value{i,j}.H =  abs(kernel.value{i,j}.H);
					obj.value{i,j}.J =  abs(kernel.value{i,j}.J);
				end
			end			
		end
		
		function m = max(kernel)
			% max of the kernel values
			mG = 0;
			mH = 0;
			mJ = 0;
			for i=1:size(kernel.value,1)
				for j=1:size(kernel.value,2)
					mGij =  max(kernel.value{i,j}.G(:));
					mHij =  max(kernel.value{i,j}.H(:));
					mJij =  max(kernel.value{i,j}.J(:));
					if mGij >= mG
						mG = mGij;
					end
					if mHij >= mH
						mH = mHij;
					end
					if mJij >= mJ
						mJ = mJij;
					end
				end
			end
			m = [mG mH mJ];
		end
		
		
		% constructor:
		function obj = ppide_kernel(ppide,zdisc)
			tic;
			fprintf('  Initializing ppide_kernel...')
			% kernel = ppide_kernel(ppide,zdisc) 
			% create new ppide_kernel object for the object ppide of class
			% ppide_sys with the spatial discretisation zdisc
			import numeric.*
			import misc.*
% 			userView = memory;
% 			disp(['Memory used: ' num2str(round(userView.MemUsedMATLAB/1e6)) 'MB'])
			if ppide.folded % folding
				obj.value = obj.init_kernel_elementsFolding(ppide,zdisc);
			else
				obj.value = obj.init_kernel_elements(ppide,zdisc);
			end
% 			userView = memory;
% 			disp(['Memory used: ' num2str(round(userView.MemUsedMATLAB/1e6)) 'MB'])
			obj.zdisc = zdisc;
			
			n = size(ppide.l,2);
			zInterp = cell(n,n,n,n,size(ppide.l,4));
			zetaInterp = cell(n,n,n,n,size(ppide.l,4));
% 			xiInterp = cell(n,n,n,n,size(ppide.l,4));
% 			etaInterp = cell(n,n,n,n,size(ppide.l,4));
			xiMatXi = cell(n,n,n,n);
			etaMatXi = cell(n,n,n,n);
			numtL = size(obj.value{1,1}.xi,2);
			for i=1:n
				for j=1:n
% 					obj.aimjMat{i,j} = zeros(size(obj.value{i,j}.xi,1),size(obj.value{i,j}.eta,1),numtL);
% 					for tIdx=1:numtL
% 						for xiIdx=1:length(obj.value{i,j}.xi(:,tIdx))
% 							for etaIdx = 1:length(obj.value{i,j}.eta(:,tIdx))
% 								z = obj.value{i,j}.zMat(xiIdx,etaIdx,tIdx);
% 								zeta = obj.value{i,j}.zetaMat(xiIdx,etaIdx,tIdx);
% % 								zIdx = get_idx(z,zdisc);
% % 								zetaIdx = get_idx(zeta,zdisc);
% 								ai = obj.value{i,j}.a_iInterp.evaluate(z,tIdx);
% 								aj = obj.value{i,j}.a_jInterp.evaluate(zeta,tIdx);
% 								obj.aimjMat{i,j}(xiIdx,etaIdx,tIdx) = ai - aj;
% 							end
% 						end
% 					end
					for k=1:n
						for l=1:n
							xiMatXiTemp = obj.value{k,l}.xiMatInterp.eval(repmat(obj.value{i,j}.zMat(:),numtL,1),repmat(obj.value{i,j}.zetaMat(:),numtL,1),...
								reshape(repmat(1:numtL,numel(obj.value{i,j}.zMat),1),[numtL*numel(obj.value{i,j}.zMat),1]));
							% xi_kl(xi_ij,eta_ij)
							xiMatXi{i,j,k,l} = reshape(xiMatXiTemp,size(obj.value{i,j}.zMat,1),size(obj.value{i,j}.zMat,2),numtL);
							etaMatXiTemp = obj.value{k,l}.etaMatInterp.eval(repmat(obj.value{i,j}.zMat(:),numtL,1),repmat(obj.value{i,j}.zetaMat(:),numtL,1),...
								reshape(repmat(1:numtL,numel(obj.value{i,j}.zMat),1),[numtL*numel(obj.value{i,j}.zMat),1]));
							% eta_kl(xi_ij,eta_ij)
							etaMatXi{i,j,k,l} = reshape(etaMatXiTemp,size(obj.value{i,j}.zMat,1),size(obj.value{i,j}.zMat,2),numtL);
						end
					end
				end
			end
			% PROBLEM: INTERPOLATIONEN UEBER DIE SPARSE-COORDINATEN MACHT VLLT WEGNI SINN, VA.
			% WEGEN DEN NaNs...
			obj.zInterp = zInterp;
			obj.zetaInterp = zetaInterp;
			obj.xiMatXi = xiMatXi;
			obj.etaMatXi = etaMatXi;
			% store discretized versions of A, Lambda, and Mu evaluated over each grid xi, eta:
			% resample time resolution:
			%%% Interpolate system parameters to kernel resolution
			pars.lambda_disc = ppide.l_disc;  %translate_to_grid(,zdisc);
			pars.A_disc = ppide.a_disc; %translate_to_grid(,zdisc);
			pars.a0_disc = ppide.a_0; %translate_to_grid(,zdisc);
			% all time-dependent coefficients need to be resampled in time-direction
			varlist = {'lambda_disc','A_disc','a0_disc'};
			obj.tDisc = ppide.ctrl_pars.tDiscKernel;
			for i=1:length(varlist)
				if size(pars.(varlist{i}),4)>1
					pars.(varlist{i}) = permute(misc.translate_to_grid(permute(pars.(varlist{i}),[4 1 2 3]),obj.tDisc),[2 3 4 1]);
				end
			end
			
			ndisc = length(zdisc);
			if ndisc~= n
				if isa(ppide.ctrl_pars.mu,'function_handle') % function
					if nargin(ppide.ctrl_pars.mu)==1
						muHelp = eval_pointwise(ppide.ctrl_pars.mu,zdisc);
					else
						muHelp = zeros(ndisc,n,n,length(obj.tDisc));
						for z_idx=1:ndisc
							muHelp(z_idx,:,:,:) = permute((eval_pointwise(@(z)ppide.ctrl_pars.mu(zdisc(z_idx),z),obj.tDisc)),[2 3 1]);
						end
					end
				elseif size(ppide.ctrl_pars.mu,1) > n % is discretized
					if length(size(ppide.ctrl_pars.mu))>3
						muInterpolant = interpolant({linspace(0,1,size(ppide.ctrl_pars.mu,1)),1:n,1:n,obj.tDisc}, ppide.ctrl_pars.mu);
						muHelp = muInterpolant.evaluate(zdisc,1:n,1:n,obj.tDisc);
					else
						muInterpolant = interpolant({linspace(0,1,size(ppide.ctrl_pars.mu,1)),1:n,1:n}, ppide.ctrl_pars.mu);
						muHelp = muInterpolant.evaluate(zdisc,1:n,1:n);
					end
					%muHelp = ppide.ctrl_pars.mu;
				elseif isequal(size(ppide.ctrl_pars.mu,1),n) % constant matrix, two dimensions
					muHelp(1:ndisc,:,:) = repmat(shiftdim(ppide.ctrl_pars.mu,-1),ndisc,1,1);
				else % discretized matrix
					error('dimensions of mu not correct!')
				end
			else
				error('ndisc=n is a very bad choice!')
			end
			mu = muHelp;
			
			
			
			% preallocate
			obj.Azeta = cell(n,n,n,n);
			obj.MuZ = cell(n,n,n,n);
			obj.lambdaZeta = cell(n,n,n);
			% store interpolants of A for integrations in different coordinate systems
			AInterp = numeric.interpolant({linspace(0,1,ppide.ndiscPars),1:n,1:n,1:size(pars.A_disc,4)},pars.A_disc);
			LambdaInterp = numeric.interpolant({linspace(0,1,ppide.ndiscPars),1:n,1:n,1:size(pars.lambda_disc,4)},pars.lambda_disc);
			muInterp = numeric.interpolant({zdisc,1:n,1:n,linspace(0,1,size(mu,4))},mu);
			% Evaluate Interpolant to get A on each k,l grid
			kernel = obj;
			for i = 1:n
				for j=1:n
					for k=1:n
						% for A, the column index is constant
						AzetaTemp = AInterp.eval({kernel.value{k,j}.zetaMat(:),i,j,1:size(pars.A_disc,4)});
						% Aij(zeta(xi_kj,eta_kj))
						obj.Azeta{i,j,k,j} = reshape(AzetaTemp,size(kernel.value{k,j}.xi,1),size(kernel.value{k,j}.eta,1),size(pars.A_disc,4));

						
						% for mu the row index is constant
						muZTemp = muInterp.eval({kernel.value{i,k}.zMat(:),i,j,linspace(0,1,size(mu,4))});
						% muij(z(xi_ik,eta_ik))
						obj.MuZ{i,j,i,k} = reshape(muZTemp,size(kernel.value{i,k}.xi,1),size(kernel.value{i,k}.eta,1),size(mu,4));
			
						% lambda needs to be evaluated for a single index
						lambda_zeta = LambdaInterp.eval({kernel.value{i,j}.zetaMat(:),k,k,1:size(pars.lambda_disc,4)});
						% lambda_k(zeta(xi_ij,eta_ij))
						obj.lambdaZeta{k,i,j} = reshape(lambda_zeta,size(kernel.value{i,j}.xi,1),size(kernel.value{i,j}.eta,1),size(pars.lambda_disc,4));
					end
				end
			end
			% a0 needs to be integrated over zeta in use_F_ppides o no interpolation on the
			% xi-eta-grid is required here. Instead, it is simply stored so the resampling isnt
			% needed in each iteration step.
			obj.a0Zeta = misc.translate_to_grid(pars.a0_disc,zdisc);
			
% 			obj.xiInterp = xiInterp;
% 			obj.etaInterp = etaInterp;
			if ppide.folded
				obj.value = obj.getFixedPointStartFolding(ppide);
			end

			time=toc;
			fprintf('Finished! (%0.2fs)\n',time);
		end
		
		% return K as matrix
		function K = get_K(obj,varname,tIdx)
			% K = get_K(obj,varname,tIdx)
			% tIdx give the dimension of the value of the kernel element that shall be put to
			% the end.
			import misc.*
			import numeric.*
			import dps.parabolic.control.backstepping.*
			% just get K(z,zeta,i,j,t)
			if nargin<2
				K=0;
				n = size(obj.value,1);
				K(1:length(obj.zdisc),1:length(obj.zdisc),n,n,1:size(obj.value{1,1}.G,3)) = 0;
				for i=1:n
					for j=1:n % create empty dimensions for i,j and move time to end
						K(:,:,i,j,:) = permute(obj.value{i,j}.K,[1 2 4 5 3]);
					end
				end
			else % get a different variable:
				if ~ischar(varname)
					error('varname must be a string!')
				end
				n = size(obj.value,1);
				if ~isprop(obj.value{1,1},varname)
					error([varname ' is no property of ppide_kernel_element!'])
				end
				% get size of the variable
				sizVar = sizeOf(reduce_dimension(obj.value{1,1}.(varname)));
				while sizVar(end)==1
					sizVar = sizVar(1:end-1);
				end
				% number of dimensions of var.
				% Problem: Shall always be z, Rest, n, n, t
				% it is not possible to decide where the variable has its t-dimension, so it
				% must be given in the argument. Should better be implemented with quantity.
				% may be that the time-dimension is one, so it doesnt exit anymore.
				idxVec = 1:length(sizVar);
				if nargin>2 && length(sizVar)>=tIdx
					sizK = [sizVar(idxVec~=tIdx) sizVar(tIdx+1:end) n n sizVar(tIdx)];
				else
					sizK = [sizVar n n];
				end
				K = zeros(sizK);
				if nargin>2 && length(sizVar)>=tIdx
					K_vec = reshape(K,prod(sizVar(idxVec~=tIdx)),n,n,sizVar(tIdx));
				else
					K_vec = reshape(K,[],n,n);
				end
				if nargin>2 && length(sizVar)>=tIdx
					for i=1:n
						for j=1:n
							K_vec(:,i,j,:) = reshape(obj.value{i,j}.(varname),prod(sizVar(idxVec~=tIdx)),1,1,sizVar(tIdx));
						end
					end
				else
					for i=1:n
						for j=1:n
							K_vec(:,i,j,:) = reshape(obj.value{i,j}.(varname),[],1);
						end
					end
				end
				K = reshape(K_vec,sizK);
			end
		end
		
	function obj = get_a0_til(obj,ppide) % is called with the designPpide
		import misc.*
		import numeric.*
		import dps.parabolic.control.backstepping.*
		if obj.transformed
			K = obj.get_K; % kernel in matrix form
			zdisc=obj.zdisc; %#ok<PROPLC> % spatial discretization of kernel
			ndisc = length(zdisc); %#ok<PROPLC>
			n = size(obj.value,1); % system order
			numtKernel = size(K,5);
			lambdaDisc = translate_to_grid(ppide.l_disc,obj.zdisc);
			lambdaDiffDisc = translate_to_grid(ppide.l_diff,obj.zdisc);
			if ppide.folded % folding
				% calculate numerical derivative
				z0 = ppide.ctrl_pars.foldingPoint;
				if isscalar(z0)
					z0 = ones(n,1)*z0;
				end
				z0Left = zeros(n/2);
				zRight = zeros(n/2,ppide.ndiscPars);
				for i=1:n/2
					zRight(i,:) = linspace(z0(i),1,ppide.ndiscPars);
					dZ = diff(zRight(i,:));
					z0Left(i) = z0(i)-dZ(1);
				end
% 				warning('Calculate analytical kernel derivative and implement in a0_til')
% 				dzetaK = zeros(ndisc,ndisc,n,n,numtKernel);
% 	% 					for i=1:n
% 	% 						for j=1:n
% 						for zIdx = 2:ndisc
% 							dzetaK(zIdx,1:zIdx,:,:,:) = numeric.diff_num(zdisc(1:zIdx),K(zIdx,1:zIdx,:,:,:),2); %#ok<PROPLC>
% 						end
% 	% 						end
% 	% 					end
% 				dzetaK(1,1,:,:,:) = dzetaK(2,1,:,:,:);
% 				dzetaK_z_0 = reshape(dzetaK(:,1,:,:,:),[ndisc,n,n,numtKernel]);
				dzetaK_z_0 = obj.get_K('dzeta_K_z_0',2);
				% Dirichlet Coupling
				A0Til = zeros(ndisc,n,n,numtKernel);
				% Neumann Coupling
				A0Neu = zeros(ndisc,n,n,numtKernel);
				% upper left part of A0Til
	% 					A0Til(:,1:n/2,1:n/2,:) = -permute(misc.multArray(dzetaK(:,1,1:n/2,1:n/2,:),lambdaDisc(1,1:n/2,1:n/2),4,2),[1 3 6 4 2 5])...
	% 						- permute(misc.multArray(dzetaK(:,1,1:n/2,n/2+1:end,:),lambdaDisc(1,n/2+1:end,n/2+1:end),4,2),[1 3 6 4 2 5])...
	% 						- permute(misc.multArray(K(:,1,1:n/2,1:n/2,:),lambdaDiffDisc(1,1:n/2,1:n/2),4,2),[1 3 6 4 2 5])...
	% 						- permute(misc.multArray(K(:,1,1:n/2,n/2+1:end,:),lambdaDiffDisc(1,n/2+1:end,n/2+1:end),4,2),[1 3 6 4 2 5]); 
	% 					A0Til(:,n/2+1:end,n/2+1:end,:) = -permute(misc.multArray(dzetaK(:,1,n/2+1:end,1:n/2,:),lambdaDisc(1,1:n/2,1:n/2),4,2),[1 3 6 4 2 5])...
	% 						- permute(misc.multArray(dzetaK(:,1,n/2+1:end,n/2+1:end,:),lambdaDisc(1,n/2+1:end,n/2+1:end),4,2),[1 3 6 4 2 5])...
	% 						- permute(misc.multArray(K(:,1,n/2+1:end,1:n/2,:),lambdaDiffDisc(1,1:n/2,1:n/2),4,2),[1 3 6 4 2 5])...
	% 						- permute(misc.multArray(K(:,1,n/2+1:end,n/2+1:end,:),lambdaDiffDisc(1,n/2+1:end,n/2+1:end),4,2),[1 3 6 4 2 5]); 
	% 					%     z i     j     t
	% 					A0Neu(:,1:n/2,1:n/2,:) = -permute(...
	% 						misc.multArray(K(:,1,1:n/2,1:n/2,:),lambdaDisc(1,1:n/2,1:n/2),4,2),[1 3 6 4 2 5])...
	% 						+ permute(misc.multArray(K(:,1,1:n/2,n/2+1:end,:),lambdaDisc(1,n/2+1:end,n/2+1:end),4,2),[1 3 6 4 2 5])...
	% 						.*reshape((1-z0)./z0,[1 1 n/2]);
	% 					% the multiplication with diag(z0) can be applied as an element-wise
	% 					% multiplication where z0 is a vector expanded over the j-dimension (3) of K*L
	% 					A0Neu(:,n/2+1:end,n/2+1:end,:) = permute(...
	% 						misc.multArray(K(:,1,n/2+1:end,1:n/2,:),lambdaDisc(1,1:n/2,1:n/2),4,2),[1 3 6 4 2 5])...
	% 						.*reshape(z0./(1-z0),[1 1 n/2])...
	% 						- permute(misc.multArray(K(:,1,n/2+1:end,n/2+1:end,:),lambdaDisc(1,n/2+1:end,n/2+1:end),4,2),[1 3 6 4 2 5]);
				for i=1:n/2
					for j=1:n
						if j<= n/2
							% only the first condition would be sufficient because in the other
							% case, it should be zero. Important: It is not possible to omit
							% the condition as in the monolateral case, since the DEFINITION of
							% A0Til = 0 in the other cases is essential. (it is not
							% automatically 0!)
							% activate the uncommented cases to compute the kernel residuum for
							% the coupling BC in the Coupling matrix. But careful: Due to the
							% coupling inof the jth element with the j+n/2th element, the
							% residuum is A0Neu*S2 or A0Til*S1!
							if ppide.l(1,i,i,1) < ppide.l(1,j,j,1)% || ...
% 								ppide.l(1,i,i,1) >= ppide.l(1,j,j,1) ...
% 							 && ppide.l(1,i,i,1) >= ppide.l(1,j+n/2,j+n/2,1)
								A0Til(:,i,j,:) = reshape(...
									dzetaK_z_0(:,i,j,:)*lambdaDisc(1,j,j)...
									+dzetaK_z_0(:,i,j+n/2,:)*lambdaDisc(1,j+n/2,j+n/2)...
									+reshape(K(:,1,i,j,:)*lambdaDiffDisc(1,j,j),[ndisc,1,1,numtKernel])...
									+reshape(K(:,1,i,j+n/2,:)*lambdaDiffDisc(1,j+n/2,j+n/2),[ndisc,1,1,numtKernel])...
								,[],1,1,numtKernel);
								A0Neu(:,i,j,:) = reshape(...
									-K(:,1,i,j,:)*lambdaDisc(1,j,j)...
									+K(:,1,i,j+n/2,:)*lambdaDisc(1,j+n/2,j+n/2)*(1-z0(j))/z0Left(j)...
								,[],1,1,numtKernel);
							end
						else % j>n/2
							if ppide.l(1,i,i,1) < ppide.l(1,j,j,1) %|| ...
% 									ppide.l(1,i,i,1) >= ppide.l(1,j,j,1) &&...
% 									ppide.l(1,i,i,1) >= ppide.l(1,j-n/2,j-n/2,1)
								A0Til(:,i,j,:) = -A0Til(:,i,j-n/2,:)...
									+reshape(...
										dzetaK_z_0(:,i,j-n/2,:)*lambdaDisc(1,j-n/2,j-n/2)...
										+dzetaK_z_0(:,i,j,:)*lambdaDisc(1,j,j)...
										+reshape(K(:,1,i,j-n/2,:)*lambdaDiffDisc(1,j-n/2,j-n/2),[ndisc,1,1,numtKernel])...
										+reshape(K(:,1,i,j,:)*lambdaDiffDisc(1,j,j),[ndisc,1,1,numtKernel])...
									,[],1,1,numtKernel);
								A0Neu(:,i,j,:) = A0Neu(:,i,j-n/2,:)*z0Left(j-n/2)/(1-z0(j-n/2))...
									+reshape(...
									-K(:,1,i,j,:)*lambdaDisc(1,j,j)...
									+K(:,1,i,j-n/2,:)*lambdaDisc(1,j-n/2,j-n/2)/(1-z0(j-n/2))*z0Left(j-n/2)...
								,[],1,1,numtKernel);
							end
						end
					end
				end
				for i=n/2+1:n
					for j=n:-1:1 % count backwards here, beacuse right elements need to be calculated first!
						if j<= n/2
							if ppide.l(1,i,i,1) < ppide.l(1,j,j,1) %|| ...
% 								ppide.l(1,i,i,1) >= ppide.l(1,j,j,1) && ...
% 								ppide.l(1,i,i,1) >= ppide.l(1,j+n/2,j+n/2,1)
								A0Til(:,i,j,:) = -A0Til(:,i,j+n/2,:)...
									+ reshape(...
										+dzetaK_z_0(:,i,j,:)*lambdaDisc(1,j,j)...
										+dzetaK_z_0(:,i,j+n/2,:)*lambdaDisc(1,j+n/2,j+n/2)...
										+reshape(K(:,1,i,j,:)*lambdaDiffDisc(1,j,j),[ndisc,1,1,numtKernel])...
										+reshape(K(:,1,i,j+n/2,:)*lambdaDiffDisc(1,j+n/2,j+n/2),[ndisc,1,1,numtKernel])...
									,[],1,1,numtKernel);
								A0Neu(:,i,j,:) = A0Neu(:,i,j+n/2,:)/z0Left(j)*(1-z0(j))...
									+reshape(...
									-K(:,1,i,j,:)*lambdaDisc(1,j,j)...
									+K(:,1,i,j+n/2,:)*lambdaDisc(1,j+n/2,j+n/2)*(1-z0(j))/z0Left(j)...
								,[],1,1,numtKernel);
							end
						else % j>n/2
							if ppide.l(1,i,i,1) < ppide.l(1,j,j,1) %|| ...
% 								ppide.l(1,i,i,1) >= ppide.l(1,j,j,1) && ...
% 								ppide.l(1,i,i,1) >= ppide.l(1,j-n/2,j-n/2,1)
								A0Til(:,i,j,:) = ...
									+reshape(...
										+dzetaK_z_0(:,i,j-n/2,:)*lambdaDisc(1,j-n/2,j-n/2)...
										+dzetaK_z_0(:,i,j,:)*lambdaDisc(1,j,j)...
										+reshape(K(:,1,i,j-n/2,:)*lambdaDiffDisc(1,j-n/2,j-n/2),[ndisc,1,1,numtKernel])...
										+reshape(K(:,1,i,j,:)*lambdaDiffDisc(1,j,j),[ndisc,1,1,numtKernel])...
									,[],1,1,numtKernel);
								A0Neu(:,i,j,:) = reshape(...
									-K(:,1,i,j,:)*lambdaDisc(1,j,j)...
									+K(:,1,i,j-n/2,:)*lambdaDisc(1,j-n/2,j-n/2)/(1-z0(j-n/2))*z0Left(j-n/2)...
								,[],1,1,numtKernel);
							end
						end
					end
				end

				% All values of A0Til where lambda_i geq lambda_j or lambda_i>= lambda_j+n/2 are set to zero,
				% which is important to be able to check the BC. Normally this should be
				% automatically fulfilled. (except the kernel solution deviation)
				obj.a0_til = A0Til;
				obj.a0_neu = A0Neu;
			else  % no more folding
			% Untere Dreiecksverkopplung berechnen:
				if isa(ppide.b_op1.b_d,'function_handle')
					Q0 = -permute(misc.eval_pointwise(ppide.b_op1.b_d,linspace(0,1,numtKernel)),[2 3 1]);
				else
					Q0 = misc.translate_to_grid(-ppide.b_op1.b_d,{1:n,1:n,linspace(0,1,numtKernel)},'gridOld',...
						{1:n,1:n,linspace(0,1,size(ppide.b_op1.b_d,3))}); %Q0 in original representation
				end
				lambdaDisc = translate_to_grid(ppide.l_disc,obj.zdisc);
				% Hartwig edited
				if size(ppide.l_disc,4)>1
					lambdaDisc = permute(translate_to_grid(permute(lambdaDisc,[4,2,3,1]),obj.zdisc),[4,2,3,1]);
				end

				c_disc = translate_to_grid(ppide.c,obj.zdisc);
				if size(c_disc,4)>1
					c_disc = permute(translate_to_grid(permute(c_disc,[4 1 2 3]),linspace(0,1,numtKernel)),[2 3 4 1]);
				end
				A0_til(1:length(zdisc),1:n,1:n,1:numtKernel) = 0; %#ok<PROPLC> % preallocate
	% 				A0_til_comp(1:length(zdisc),1:n,1:n) = 0; %#ok<PROPLC> % preallocate
				a0_disc = translate_to_grid(ppide.a_0_disc,obj.zdisc);
				if size(a0_disc,4)>1 % time dependent a0
					a0_disc = permute(translate_to_grid(permute(a0_disc,[4 1 2 3]),linspace(0,1,numtKernel)),[2 3 4 1]);
				end
				A0_BS = permute(Tc(permute(a0_disc,[4 1 2 3]),K,zdisc,zdisc),[2 3 4 1]); %#ok<PROPLC>

				for i=1:n
					for j=1:n
						% a0_til haengt von Kern ab und damit wird es auch
						% nur in Kern-Aufloesung berechnet!
						lambda_j_diff = obj.value{i,j}.lambda_j_diff;
						K_z_0 = permute(K(:,1,i,j,:),[1 5 2 3 4]);
						if lambdaDisc(1,i,i,1)<lambdaDisc(1,j,j,1)
							if ppide.b_op1.b_n(j,j)~= 0 % Robin	
								A0_til(:,i,j,:) = -permute(A0_BS(:,i,j,:),[1 4 2 3])...
									+repmat(reshape(lambdaDisc(1,j,j,:),1,[]),ndisc,1,1).*obj.value{i,j}.dzeta_K_z_0...
									+repmat(+lambda_j_diff(1,:) - permute(lambdaDisc(1,j,j,:),[1 4 2 3]).*reshape(Q0(j,j,:),1,[]) - reshape(c_disc(1,j,j,:),1,[]),ndisc,1,1).*K_z_0;
							else
								% TODO: VOrzeichen pr�fen!
								A0_til(:,i,j,:) = -K_z_0.*permute(lambdaDisc(1,j,j,:),[4 1 2 3]);
							end
	% 						else
	% 							A0_til_comp(:,i,j) = 0;
						end
					end
				end
				obj.a0_til = A0_til;
			end
		else
			warning('kernel has not yet been transformed, no ao_til has been computed!')
		end
	end
		
		function obj = get_a0_bar(obj,ppide)
			import misc.*
			import numeric.*
			import dps.parabolic.control.backstepping.*
			if obj.transformed
				K = obj.get_K; % kernel in matrix form
				numtKernel = size(K,5);
				zdisc=obj.zdisc; %#ok<PROPLC> % spatial discretization of kernel
				n = size(obj.value,1); % system order
				
				Bn = ppide.b_op2.b_n;
				if isa(ppide.b_op2.b_d,'function_handle')
					BdDisc = permute(misc.eval_pointwise(ppide.b_op2.b_d,linspace(0,1,numtKernel)),[2 3 1]);
				else
					if size(ppide.b_op2.b_d,3)==1
						% constant
						BdDisc = repmat(ppide.b_op2.b_d,1,1,numtKernel);
					else
						% discretized
						BdDisc = misc.translate_to_grid(ppide.b_op2.b_d,{1:n,1:n,linspace(0,1,numtKernel)},'gridOld',...
							{1:n,1:n,linspace(0,1,size(ppide.b_op2.b_d,3))});
					end
				end
				dz_K_1_zeta = obj.get_K('dz_K_1_zeta',2); 
				
				A0_bar = zeros(length(zdisc),n,n,numtKernel); %#ok<PROPLC>
				for z_idx=1:length(zdisc) %#ok<PROPLC>
					for tIdx=1:numtKernel
						% Caution: the degree of freedom 
						% Sr^t P(1,1,t) Sd is set to zero in the implementation.
						A0_bar(z_idx,:,:,tIdx) = -Bn*squeeze(dz_K_1_zeta(z_idx,:,:,tIdx))...
							- BdDisc(:,:,tIdx)*squeeze(K(end,z_idx,:,:,tIdx));
					end
				end
				% normally not needed cause numerical errors are so small!
				% however, if the numerical parameters are bad conditioned, the error can be
				% visible!
				for i=1:n
					for j=1:n
						if ppide.l(1,i,i,1) <= ppide.l(1,j,j,1)
							A0_bar(:,i,j,:) = zeros(length(zdisc),1,1,numtKernel); %#ok<PROPLC>
						end
					end
				end
				obj.a0_bar = A0_bar;
			else
				warning('kernel has not yet been transformed, no ao_bar has been computed!')
			end
		end
		
	end
	
end

