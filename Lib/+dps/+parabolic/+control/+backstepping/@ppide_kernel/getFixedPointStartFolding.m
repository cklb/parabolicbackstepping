function GH = getFixedPointStartFolding(obj,ppide)
%getFixedPointStartFolding get starting values of fixed point iteration
%
% GH: cell array of kernel_elements

% created on 07.08.2019 by Simon Kerschbaum

% overtake old values
GH = obj.value;

n = ppide.n;
numtKernel = size(GH{1,1}.G,3);
numtL = size(ppide.l,4);
if numtKernel > 1
	tDisc = obj.tDisc;
else
	tDisc = 1;
end
zdisc = obj.zdisc;
ndisc = length(zdisc);
z0 = ppide.ctrl_pars.foldingPoint;
if isscalar(z0)
	z0 = z0*ones(n,1);
end

if ndisc~= n
	if isa(ppide.ctrl_pars.mu,'function_handle') % function
		if nargin(ppide.ctrl_pars.mu)==1
			muHelp = eval_pointwise(ppide.ctrl_pars.mu,zdisc);
		else
			muHelp = zeros(ndisc,n,n,length(tDisc));
			for z_idx=1:ndisc
				muHelp(z_idx,:,:,:) = permute((eval_pointwise(@(z)ppide.ctrl_pars.mu(zdisc(z_idx),z),tDisc)),[2 3 1]);
			end
		end
	elseif size(ppide.ctrl_pars.mu,1) > length(ppide.ctrl_pars.zdiscCtrl) % is discretized
		if length(size(ppide.ctrl_pars.mu))>3
			muInterpolant = numeric.interpolant({linspace(0,1,size(ppide.ctrl_pars.mu,1)),1:n,1:n,tDisc}, ppide.ctrl_pars.mu);
			muHelp = muInterpolant.evaluate(zdisc,1:n,1:n,tDisc);
		else
			muInterpolant = numeric.interpolant({linspace(0,1,size(ppide.ctrl_pars.mu,1)),1:n,1:n}, ppide.ctrl_pars.mu);
			muHelp = muInterpolant.evaluate(zdisc,1:n,1:n);
		end
		%muHelp = ppide.ctrl_pars.mu;
	elseif isequal(size(ppide.ctrl_pars.mu,1),n) % constant matrix, two dimensions
		muHelp(1:ndisc,:,:) = repmat(shiftdim(ppide.ctrl_pars.mu,-1),ndisc,1,1);
	else % discretized matrix
		error('dimensions of mu not correct!')
	end
else
	error('ndisc=n is a very bad choice!')
end

if size(ppide.a,4)>1 || size(ppide.l,4)>1 || size(muHelp,4)>1
	numtKernel = length(tDisc);
end

for i=1:n
	for j=1:n
		kem = obj.value{i,j};
		s = kem.s;
		if size(GH{i,j}.xiMat_xi,3) == 1 && numtKernel > 1
			xiMat = repmat(GH{i,j}.xiMat_xi,1,1,numtKernel);
		else
			xiMat = GH{i,j}.xiMat_xi;
		end
		if size(GH{i,j}.etaMat_xi,3) == 1 && numtKernel > 1
			etaMat = repmat(GH{i,j}.etaMat_xi,1,1,numtKernel);
		else
			etaMat = GH{i,j}.etaMat_xi;
		end
		a_sigma = -(kem.aiZXieta-kem.ajZetaXieta);
		a_delta = kem.aiZXieta+kem.ajZetaXieta;
		etaDisc = kem.eta;
		xiDisc = kem.xi;
		
		% H0 and G0
		Hi = zeros(size(xiMat));
		Hii = zeros(size(xiMat));
		Hiii = zeros(size(xiMat));
		Hiiii = zeros(size(xiMat));
		Hiiiii = zeros(size(xiMat));
		Hiiiiii = zeros(size(xiMat));
		Gi = zeros(size(xiMat));
		Gii = zeros(size(xiMat));
		Giii = zeros(size(xiMat));
		
		if kem.lambda_i(1) == kem.lambda_j(1) && j<= n/2
			% c4(z(xi,0))
			Gi = reshape(kem.r{11}.eval({kem.zMat(:,1),linspace(0,1,numtKernel)}),[],1,numtKernel);
			Hi = s/4*a_sigma.*Gi;
		end
		if j>n/2 && (kem.lambda_i(1) < GH{i,j-n/2}.lambda_j(1) || kem.lambda_i(1) < GH{i,j}.lambda_j(1))
			% gf(eta)
			Gii(:,etaDisc>=0,:) = repmat(...
					reshape(ppide.ctrl_pars.g_strich{i,j}(etaDisc(etaDisc>=0)),1,[],1)...
				,length(xiDisc),1,numtKernel);
			Hii(:,etaDisc>=0,:) = Hii(:,etaDisc>=0,:)+...
				s/4*a_sigma(:,etaDisc>=0,:).*Gii(:,etaDisc>=0,:);
		end
		if kem.lambda_i(1) == kem.lambda_j(1)
			Hiii = -s/4*a_sigma(:,1,:).*reshape(kem.r{11}.eval({kem.zMat(:,1),linspace(0,1,numtKernel)}),[],1,numtKernel);
		end
		
		if j > n/2 && kem.lambda_i(1) == GH{i,j-n/2}.lambda_j(1) && kem.lambda_i(1) >= kem.lambda_j(1)
			% for i,j lambda_i ~= lambda_j! but lambda_i == lambda_j-n!
			% c4_j-n
			xiMatXi = obj.xiMatXi{i,j,i,j-n/2}; % xij-n(xi,eta)
			xiMatXiFull = misc.writeToOutside(xiMatXi);
			xiMatInterp = numeric.interpolant({xiDisc,etaDisc,1:numtL},xiMatXiFull);
			xiEtaEtaTemp = xiMatInterp.eval(repmat(etaDisc(etaDisc>=0),numtL,1),repmat(etaDisc(etaDisc>=0),numtL,1),...
				reshape(repmat(1:numtL,numel(etaDisc(etaDisc>=0)),1),numtL*numel(etaDisc(etaDisc>=0)),1));
			xiEtaEta = reshape(xiEtaEtaTemp,numel(etaDisc(etaDisc>=0)),numtL);
			zVec  = kem.zMatInterp{1}.eval({xiEtaEta(:),0});

			Giii(:,etaDisc>=0,:) = zeros(size(xiMat,1),length(etaDisc(etaDisc>=0)),numtKernel)+...
				z0(j-n/2)/(1-z0(j-n/2)).*reshape(GH{i,j-n/2}.r{11}.eval({zVec,linspace(0,1,numtKernel)}),1,[],numtKernel);
			Hiiii(:,etaDisc>=0,:) = s/4*a_sigma(:,etaDisc>=0,:).*Giii(:,etaDisc>=0,:);
		end
		
		if kem.lambda_i(1) == kem.lambda_j(1)
			zVec = kem.zMat(:,1);
			Hiiiii = reshape(kem.r{9}.eval({zVec,linspace(0,1,numtKernel)}),[],1,numtKernel);
		end
		if kem.lambda_i(1) ~= kem.lambda_j(1)
			% Problem: eta_l anders diskretisiert, als eta!
			% --> kommt egal wie, wenn einfach nur zMat interpoliert wird, zu gro�en
			% Interpolationsfehlern. Eigentlich muss genau zwischen den Punkten interpoliert
			% werden, die in eta und eta_l enthalten sind. Diese sind xi_l_sparse!
			% also
			zVecSparse = kem.zMatInterp{1}.eval(kem.xi_l_sparse,kem.eta(kem.eta<0));
			zVecInterp = numeric.interpolant({kem.xi_l_sparse},zVecSparse);
			zVec = zVecInterp.eval({xiDisc});
			Hiiiiii = reshape(kem.r{7}.eval({zVec,linspace(0,1,numtKernel)}).*kem.eta_l_diff./(s*kem.eta_l_diff-1),[],1,numtKernel);
		end
		
		GH{i,j}.G = Gi + Gii + Giii;
		% in order to be able to interpolate, which will be needed for J in order to not implement
		% everything again, the values must be written into the outside-domain!
		GH{i,j}.H = misc.writeToOutside(Hi+Hii+Hiii+Hiiii++Hiiiii++Hiiiiii);
	end % for j % need to finish all H in this row to be able to calculate J!
		
	for j=1:n
		% J0:
		kem = obj.value{i,j};
		s = kem.s;
		if size(GH{i,j}.xiMat_xi,3) == 1 && numtKernel > 1
			xiMat = repmat(GH{i,j}.xiMat_xi,1,1,numtKernel);
		else
			xiMat = GH{i,j}.xiMat_xi;
		end
		if size(GH{i,j}.etaMat_xi,3) == 1 && numtKernel > 1
			etaMat = repmat(GH{i,j}.etaMat_xi,1,1,numtKernel);
		else
			etaMat = GH{i,j}.etaMat_xi;
		end
		etaDisc = kem.eta;
		xiDisc = kem.xi;
		
		Ji = zeros(size(xiMat));
		Jii = zeros(size(xiMat));
		Jiii = zeros(size(xiMat));
		Jiiii = zeros(size(xiMat));
		
		if j<=n/2
			if kem.lambda_i(1) ~= kem.lambda_j(1)
				xilEta = kem.xi_l_sparse;
				zVec = kem.zMatInterp{1}.eval(xilEta,etaDisc(etaDisc<0));
				eta_l_diffInterp = kem.eta_l_diffInterp;
				% avoid repmat by Ji+
				Ji(:,etaDisc<0,:) = Ji(:,etaDisc<0,:) + reshape(kem.r{7}.eval({zVec,linspace(0,1,numtKernel)})...
					*1./(1-s*eta_l_diffInterp.eval(xilEta)),1,[],numtKernel);
			end
			if kem.lambda_i(1) < kem.lambda_j(1) || kem.lambda_i(1) < GH{i,j+n/2}.lambda_j(1)
				Jii(:,etaDisc>=0,:) = Jii(:,etaDisc>=0,:)+...
					reshape(ppide.ctrl_pars.g_strich{i,j}(etaDisc(etaDisc>=0)),1,[],1);
			end
			if s==1 && kem.lambda_i(1) >= GH{i,j+n/2}.lambda_j(1)
				% interpolate on (eta,eta)
				HInterpij = numeric.interpolant({GH{i,j}.xi,GH{i,j}.eta,1:numtKernel},...
					GH{i,j}.H);
				Jiii(:,etaDisc>=0,:) = Jiii(:,etaDisc>=0,:)...
					- kem.r{12}*reshape(HInterpij.eval(...
						repmat(etaDisc(etaDisc>=0),numtKernel,1),...
						repmat(etaDisc(etaDisc>=0),numtKernel,1),...
						reshape(repmat(1:numtKernel,numel(etaDisc(etaDisc>=0)),1),numtKernel*numel(etaDisc(etaDisc>=0)),1)),...
					1,[],numtKernel);
				
				% xi_j+n(eta,eta) und eta_j+n(eta,eta)
				xiMatXi = obj.xiMatXi{i,j,i,j+n/2}; % xij+n(xi,eta)
				xiMatXiFull = misc.writeToOutside(xiMatXi);
				xiMatInterp = numeric.interpolant({xiDisc,etaDisc,1:numtL},xiMatXiFull);
				etaMatXi = obj.etaMatXi{i,j,i,j+n/2}; % xij+n(xi,eta)
				etaMatXiFull = misc.writeToOutside(etaMatXi);
				etaMatInterp = numeric.interpolant({xiDisc,etaDisc,1:numtL},etaMatXiFull);
				xiEtaEtaTemp = xiMatInterp.eval(repmat(etaDisc(etaDisc>=0),numtL,1),repmat(etaDisc(etaDisc>=0),numtL,1),...
					reshape(repmat(1:numtL,numel(etaDisc(etaDisc>=0)),1),numtL*numel(etaDisc(etaDisc>=0)),1));
				xiEtaEta = reshape(xiEtaEtaTemp,numel(etaDisc(etaDisc>=0)),numtL);
				etaEtaEtaTemp = etaMatInterp.eval(repmat(etaDisc(etaDisc>=0),numtL,1),repmat(etaDisc(etaDisc>=0),numtL,1),...
					reshape(repmat(1:numtL,numel(etaDisc(etaDisc>=0)),1),numtL*numel(etaDisc(etaDisc>=0)),1));
				etaEtaEta = reshape(etaEtaEtaTemp,numel(etaDisc(etaDisc>=0)),numtL);
				HInterp = numeric.interpolant({GH{i,j+n/2}.xi,GH{i,j+n/2}.eta,1:numtKernel},...
					GH{i,j+n/2}.H);
			
				Jiiii(:,etaDisc>=0,:) = Jiiii(:,etaDisc>=0,:)...
					+ 2*GH{i,j+n/2}.s*kem.r{13}*...
					reshape(HInterp.eval(repmat(xiEtaEta,numtKernel,1),repmat(etaEtaEta,numtKernel,1),...
						reshape(repmat(1:numtKernel,numel(xiEtaEta),1),numtKernel*numel(xiEtaEta),1)),...
					1,[],numtKernel);
			end
		end
		GH{i,j}.J = Ji + Jii + Jiii + Jiiii;
		
		
		
		if numtL == 1
			GH{i,j}.G_t = numeric.diff_num(tDisc,GH{i,j}.G,3);
		else
			% must be computed via K. 
			% not yet implemented!
			error('Time dependent lambda not yet implemented for folding!')
		end
		
		GH{i,j}.K = GH{i,j}.transform_kernel_element(1);
		GH{i,j}.K_t = numeric.diff_num(tDisc,GH{i,j}.K,3);
		
		% only in the end! Must be used for fixed-point iteration, but not for starting values!
% 		GH{i,j}.J = GH{i,j}.J + GH{i,j}.s/4*numeric.cumtrapz_fast_nDim(xiMat,GH{i,j}.G_t,1,'equal','ignoreNaN');
% 		GH{i,j}.H = GH{i,j}.H + GH{i,j}.s/4*numeric.cumtrapz_fast_nDim(etaMat,GH{i,j}.G_t,2,'equal','ignoreNaN');
		if any(isnan(GH{i,j}.G(:))) || any(isnan(GH{i,j}.G(:))) || any(isnan(GH{i,j}.J(:)))
			here=1;
		end
	end
end

end

