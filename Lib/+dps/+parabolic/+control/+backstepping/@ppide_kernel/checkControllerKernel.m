function test = checkControllerKernel(controllerKernel, ppide, plotResult)
% checkControllerKernel inserts the previously calculated controller-kernel
% into its kernel equations and prints the residuals, to give some insight
% if they are correct or not. Note that the residual of the pde is usually
% relatively big, because numerical derivatives are used, which are very
% inaccurate. If one is uncertain about the interpretation of the numbers,
% having a look at the plots of the distributed residuals might be helpful.
	if nargin < 3
		plotResult = false;
	end
	%% Read parameter
	n = ppide.n;
	disc.n = ppide.ndiscPars;
	disc.z = linspace(0,1,disc.n);
	disc.dz = disc.z(2) - disc.z(1);
	disc.Lambda = ppide.l;
	if size(disc.Lambda,4)>1
		error('Not yet implemented for time-varying lambda!')
	end
	disc.Lambda_dz = ppide.l_diff;
	disc.Lambda_ddz = ppide.l_diff2;
	AInterp = numeric.interpolant({disc.z,1:n,1:n,linspace(0,1,size(ppide.a,4))},ppide.a);
	K = controllerKernel.get_K;
	K_t = controllerKernel.get_K('K_t',3);
	disc.ntKernel = size(K,5);
	disc.A = AInterp.eval({disc.z,1:n,1:n,linspace(0,1,disc.ntKernel)});
	
	disc.nKernel = size(K,1);
	disc.zKernel = linspace(0,1,disc.nKernel);
	dzetaK = zeros(disc.nKernel,disc.n,n,n,disc.ntKernel);
	for zIdx = 2:disc.nKernel
		dzetaK(zIdx,1:zIdx,:,:,:) = numeric.diff_num(disc.zKernel(1:zIdx),K(zIdx,1:zIdx,:,:,:),2); 
	end
	dzetaK(1,1,:,:,:) = dzetaK(2,1,:,:,:);
	
	% K is already printed to outside of the valid spatial domain,
% 	for it = 1:n
% 		for jt = 1:n
% 			K(:,:,it,jt) = inpaint_nans(K(:,:,it,jt));
% 		end
% 	end
	
	test.K.value = misc.translate_to_grid(K, disc.z, 'zdim', 2);%, 'method', 'spline');
	test.Kt.value = misc.translate_to_grid(K_t, disc.z, 'zdim', 2);%, 'method', 'spline');
	[test.K.dzeta, test.K.dz] = numeric.gradient_on_2d_triangular_domain(test.K.value , disc.dz);
	[test.K.ddzeta, ~] = numeric.gradient_on_2d_triangular_domain(test.K.dzeta, disc.dz);
	% use normal derivative for first derivative
	test.K.dzetaNum = misc.translate_to_grid(dzetaK, disc.z, 'zdim', 2);%, 'method', 'spline');
	[~, test.K.ddz] = numeric.gradient_on_2d_triangular_domain(test.K.dz, disc.dz);
	% Hartwig edited
	if disc.n~= n
		zdisc = linspace(0,1,disc.n);
		if isa(ppide.ctrl_pars.mu,'function_handle') % function
			muHelp = eval_pointwise(ppide.ctrl_pars.mu,zdisc);
		elseif isequal(size(ppide.ctrl_pars.mu,1),disc.n) % is discretized 
			muHelp = ppide.ctrl_pars.mu;
		elseif isequal(size(ppide.ctrl_pars.mu,1),n) % constant matrix, two dimensions
			muHelp(1:disc.n,:,:) = repmat(shiftdim(ppide.ctrl_pars.mu,-1),disc.n,1,1);
		else % discretized matrix
			error('dimensions of mu not correct!')
		end
	else
		error('ndiscPars=n is a very bad choice!')
	end
	mu = muHelp;
	%mu = ppide.ctrl_pars.mu;
	
	%% PDE
	% Lambda(z) K_ddz(z, zeta) - (K(z, zeta) Lambda(zeta))_ddzeta - K(z, zeta) (A(zeta) + mu I) = 0
	test.K.pde = zeros(disc.n, disc.n, n, n); 
% 	for zIdx = 2:disc.n-1
% 		for zetaIdx = 2:zIdx-1
% 			for tIdx = 1:disc.ntKernel
% 				if size(disc.A,4) >1
% 					tIdxA = tIdx;
% 				else
% 					tIdxA = 1;
% 				end
% 				if size(mu,4) >1 
% 					tIdxMu = tIdx;
% 				else
% 					tIdxMu = 1;
% 				end
% 			test.K.pde(zIdx, zetaIdx, :,:, tIdx) = ...
% 				permute(misc.multArray(disc.Lambda(zIdx,:,:),test.K.ddz(zIdx, zetaIdx, :, :, tIdx),3,3), [2 5 1 3 4]) ...
% 				- permute(misc.multArray(test.K.ddzeta(zIdx, zetaIdx, :, :, tIdx), disc.Lambda(zetaIdx,:,:),4,2), [3 5 4 1 2]) ...
% 				- 2 * permute(misc.multArray(test.K.dzeta(zIdx, zetaIdx, :, :, tIdx),disc.Lambda_dz(zetaIdx,:,:),4,2), [3 5 4 1 2]) ...
% 				- permute(misc.multArray(test.K.value(zIdx, zetaIdx, :, :, tIdx),disc.Lambda_ddz(zetaIdx,:,:),4,2), [3 5 4 1 2]) ...
% 				- permute(misc.multArray(test.K.value(zIdx, zetaIdx, :, :, tIdx), disc.A(zetaIdx,:,:,tIdxA),4,2), [3 5 4 1 2])...
% 				+ permute(misc.multArray(mu(zIdx,:,:,tIdxMu),test.K.value(zIdx, zetaIdx, :, :, tIdx),3,3), [2 5 1 3 4]);
% 			end
% 		end
% 	end
	LddzSum = 0;
	KddzetaLSum = 0;
	KdzetaLdzSum = 0;
	KLddzSum = 0;
	KASum = 0;
	MuKSum = 0;
	% to avoid loops in z,zeta and t direction, the matrix multiplication is performed in a
	% loop, which allows to use the .*-operator that is very performant!
	for k=1:n
		LddzSum = LddzSum + reshape(disc.Lambda(:,:,k),disc.n,1,n).*test.K.ddz(:,:,k,:,:);
		KddzetaLSum = KddzetaLSum + test.K.ddzeta(:,:,:,k,:).*reshape(disc.Lambda(:,k,:),1,disc.n,1,n);
		KdzetaLdzSum = KdzetaLdzSum + test.K.dzeta(:,:,:,k,:).*reshape(disc.Lambda_dz(:,k,:),1,disc.n,1,n);
		KLddzSum = KLddzSum + test.K.value(:,:,:,k,:).*reshape(disc.Lambda_ddz(:,k,:),1,disc.n,1,n);
		KASum = KASum + test.K.value(:,:,:,k,:).*reshape(disc.A(:,k,:,:),1,disc.n,1,n,[]);
		MuKSum = MuKSum + reshape(mu(:,:,k,:),disc.n,1,n,1,[]).*test.K.ddz(:,:,k,:,:);
	end
	test.K.pde = LddzSum - KddzetaLSum - 2*KdzetaLdzSum - KLddzSum - KASum - MuKSum - test.Kt.value;
	
	%% BC at zeta=0
	% K(z, 0) Lambda(0) + A0_tilde(z) = 0
	A0_tilde = misc.translate_to_grid(controllerKernel.a0_til, disc.z);
	if ppide.folded
		dzeta_K_z_0 = controllerKernel.get_K('dzeta_K_z_0',2);
		dzeta_K_z_0Interp = numeric.interpolant({disc.zKernel,1:n,1:n,linspace(0,1,disc.ntKernel)},dzeta_K_z_0);
		dzeta_K_z_0Plant = dzeta_K_z_0Interp.eval({disc.z,1:n,1:n,linspace(0,1,disc.ntKernel)});
		A0_neu = misc.translate_to_grid(controllerKernel.a0_neu, disc.z);
		z0 = ppide.ctrl_pars.foldingPoint;
		if isscalar(z0)
			z0 = ones(n/2,1)*z0;
		end
		zdiscPars = linspace(0,1,ppide.ndiscPars);
		z0Pars = zeros(ppide.n/2);
		for i=1:ppide.n/2
			z0Pars(i) = zdiscPars(find(zdiscPars<z0(i),1,'last'));
		end
% 		z0Til = -diag(z0)./(1-diag(z0));
		z0Til = -diag(z0Pars)./(1-diag(z0));
		S2 = [eye(n/2);eye(n/2)];
		S1 = [z0Til;eye(n/2)];
		analytical = 1;
		if ~analytical
			dzetaKz0 = permute(test.K.dzetaNum(:,1,:,:,:),[1 3 4 5 2]);
		else
			dzetaKz0 = dzeta_K_z_0Plant;
		end
		
		if size(test.K.value,5)>1 % time dependent
			test.K.bc.zeta0.value = permute(misc.multArray(...
				permute(misc.multArray(...                      %z i j t - -
					test.K.value(:,1,:,:,:),ppide.l(1,:,:),4,2),[1 3 6 4 2 5])...
					,S1,3,1),[1 2 4 3])...
				+permute(misc.multArray(...
					A0_neu...
				,S1,3,1),[1 2 4 3]); 
			test.K.bc.zeta0.dzeta = permute(misc.multArray(...
				permute(misc.multArray(...                      %z i j t - -test.K.dzetaNum(:,1,:,:,:),ppide.l(1,:,:),4,2),[1 3 6 4 2 5])...
					dzetaKz0,ppide.l(1,:,:),3,2),[1 2 5 3 4])...
					,S2,3,1),[1 2 4 3])...
				+ permute(misc.multArray(...
				permute(misc.multArray(...                      %z i j t - -
					test.K.value(:,1,:,:,:),ppide.l_diff(1,:,:),4,2),[1 3 6 4 2 5])...
					,S2,3,1),[1 2 4 3])...
				-permute(misc.multArray(...
					A0_tilde...
				,S2,3,1),[1 2 4 3]); 
		else
			test.K.bc.zeta0.value = misc.multArray(...
				permute(misc.multArray(...                      %z i j  - -
					test.K.value(:,1,:,:,:),ppide.l(1,:,:),4,2),[1 3 5 2 4])...
					,S1,3,1)...
				+misc.multArray(...
					A0_neu...
				,S1,3,1); 
			test.K.bc.zeta0.dzeta = misc.multArray(...
				permute(misc.multArray(...                         %z i j - -test.K.dzetaNum(:,1,:,:,:),ppide.l(1,:,:),4,2),[1 3 5 2 4])...
					dzetaKz0,ppide.l(1,:,:),3,2),[1 2 4 3])...
					,S2,3,1)...
				+ misc.multArray(...
				permute(misc.multArray(...                      %z i j t - -
					test.K.value(:,1,:,:,:),ppide.l_diff(1,:,:),4,2),[1 3 5 2 4])...
					,S2,3,1)...
				-misc.multArray(...
					A0_tilde...
				,S2,3,1); 
		end
	else
		test.K.bc.zeta0 = zeros(disc.n, n, n);
		for jt = 1:n
			test.K.bc.zeta0(:,:,jt) = reshape(test.K.value(:,1,:,jt), [disc.n, n]) * disc.Lambda(1,jt,jt);
		end
		test.K.bc.zeta0 = test.K.bc.zeta0 + A0_tilde;		
	end
	
	%% Is A0_tilde lower triangular?
	test.A0_tilde_upper = 0;
	test.A0_neu_upper = 0;
	numberOfA0Elements = 1;
	for it = 1:n
		for jt = 1:n
			if ppide.l(1,it,it,1)>= ppide.l(1:jt,jt,1) % A0Til should be 0
				test.A0_tilde_upper = test.A0_tilde_upper + mean(abs(A0_tilde(:, it, jt)));
				test.A0_neu_upper = test.A0_neu_upper + mean(abs(A0_neu(:, it, jt)));
				numberOfA0Elements = numberOfA0Elements+1;
			end
		end
	end
	test.A0_tilde_upper = test.A0_tilde_upper / numberOfA0Elements;
	test.A0_neu_upper = test.A0_neu_upper / numberOfA0Elements;
	
	%% BC at z=zeta (algebraic)
	% K(z, z) Lambda(z) - Lambda(z) K(z, z) = 0
	test.K.zEqualZeta = misc.diag_nd(test.K.value);
	test.K.bc.zzeta.algebraic = zeros(disc.n, n, n, disc.ntKernel);
	for zIdx = 1:disc.n
		for tIdx = 1:disc.ntKernel
			test.K.bc.zzeta.algebraic(zIdx, :, :, tIdx) = ...
				reshape(test.K.zEqualZeta(zIdx, :, :, tIdx), [n, n]) * reshape(disc.Lambda(zIdx, :, :), [n, n]) ...
				- reshape(disc.Lambda(zIdx, :, :), [n, n]) * reshape(test.K.zEqualZeta(zIdx, :, :, tIdx), [n, n]);
		end
	end
	
	%% BC at z=zeta (pde)
	% Lambda(z) (dz K(z, z) + K_dz(z, z)) + K_dzeta(z, z) Lambda(z) ...
	%		+ K(z, z) Lambda_dz(z) + A(z) + Mu(z) = 0
	if n == 1
		test.K.zEqualZetaTotalDiff = numeric.diff_num(disc.z.', test.K.zEqualZeta);
	else
		[~, test.K.zEqualZetaTotalDiff] = gradient(test.K.zEqualZeta, disc.dz);
	end
	test.K.zEqualZeta_dz = misc.diag_nd(test.K.dz);
	test.K.zEqualZeta_dzeta = misc.diag_nd(test.K.dzeta);
	test.K.bc.zzeta.pde = zeros(disc.n, n, n, disc.ntKernel);
	for zIdx = 1:disc.n
		for tIdx = 1:disc.ntKernel
			if size(disc.A,4)>1, tIdxA = tIdx; else, tIdxA = 1; end
			if size(mu,4)>1, tIdxMu = tIdx; else, tIdxMu = 1; end
			test.K.bc.zzeta.pde(zIdx, :, :, tIdx) = ...
				reshape(disc.Lambda(zIdx, :, :), [n, n]) * ...
					(reshape(test.K.zEqualZetaTotalDiff(zIdx, :, :, tIdx), [n, n]) ...
					+ reshape(test.K.zEqualZeta_dz(zIdx, :, :, tIdx), [n, n])) ...
				+ reshape(test.K.zEqualZeta_dzeta(zIdx, :, :, tIdx), [n, n]) * ...
					reshape(disc.Lambda(zIdx, :, :), [n, n]) ...
				+ reshape(test.K.zEqualZeta(zIdx, :, :, tIdx), [n, n]) * ...
					reshape(disc.Lambda_dz(zIdx, :, :), [n, n]) ...
				+ reshape(disc.A(zIdx, :, :, tIdxA), [n, n]) ...
				+ reshape(mu(zIdx,:,:, tIdxMu),[n,n]);
		end
	end
	
	%% BC at z=zeta (solution of pde) for lambda_i == lambda_j
	% K_ij(z, z) = K_ij(0, 0) * sqrt(lambda_i(0)/lambda_i(z)) ...
	%		- 1/sqrt(lambda_i(z)) * ...
	%			int_0^z (A_ij(zeta) + Mu_ij(zeta)) / (2 * sqrt(lambda_i(zeta))) dzeta
	test.K.bc.zzeta.integralSolution = zeros(disc.n, n, n, disc.ntKernel);
	test.K.bc.zzeta.numericSolution = zeros(disc.n, n, n, disc.ntKernel);
	for it = 1:n
		for jt=1:n
			if disc.Lambda(1,it,it,1) == disc.Lambda(1,jt,jt,1)
				test.K.bc.zzeta.numericSolution(:, it, jt, :) = test.K.zEqualZeta(:, it, jt, :);
				test.K.bc.zzeta.integralSolution(:, it, jt, :) = reshape(K(1, 1, it, jt, :),1,1,1,disc.ntKernel) .*sqrt(disc.Lambda(1, it, it) ./ disc.Lambda(:, it, it))...
					- (1./sqrt(disc.Lambda(:, it, it))) .* ...
					numeric.cumtrapz_fast_nDim(disc.z, (disc.A(:, it, jt,:) + mu(:,it, jt,:)) ./ (2*sqrt(disc.Lambda(:, it, it))));
			end
		end
	end
	
	fprintf(['\nAbsolut average of K-PDE-error: ', ...
		num2str(mean(abs(test.K.pde(:)))), '\n']);
	fprintf(['Absolut average of K-BC-error at zeta=0: ', ...
		num2str(mean(abs(test.K.bc.zeta0.value(:)))), '\n']);
	fprintf(['Absolut average of Kdzeta-BC-error at zeta=0: ', ...
		num2str(mean(abs(test.K.bc.zeta0.dzeta(:)))), '\n']);
	fprintf(['Absolut average of upper triangular elements of A0_tilde: ', ...
		num2str(test.A0_tilde_upper), '\n']);
	fprintf(['Absolut average of K-BC-error at z=zeta of algebraic bc: ', ...
		num2str(mean(abs(test.K.bc.zzeta.algebraic(:)))), '\n']);
	fprintf(['Absolut average of K-BC-error at z=zeta of pde bc: ', ...
		num2str(mean(abs(test.K.bc.zzeta.pde(:)))), '\n']);
	fprintf(['Absolut average of K-BC-error at z=zeta of solution of pde and\n', ...
		'        successive approximation are compared: ', ...
		num2str(mean(abs(test.K.bc.zzeta.numericSolution(:)-test.K.bc.zzeta.integralSolution(:)))), '\n\n']);
	
	%% plots?
	if plotResult
		dps.hyperbolic.plot.matrix_4D(test.K.pde, 'hide_upper_triangle', false);
		dps.hyperbolic.plot.matrix_3D(test.K.bc.zeta0.value, 'title', 'K-BC at zeta = 0');
		dps.hyperbolic.plot.matrix_3D(test.K.bc.zeta0.dzeta, 'title', 'Kdzeta-BC at zeta = 0');
		dps.hyperbolic.plot.matrix_3D(test.K.bc.zzeta.algebraic, 'title', 'K-BC algebraic at zeta = z');
		if size(test.K.bc.zzeta.pde,4)>1
			dps.hyperbolic.plot.matrix_4D(permute(test.K.bc.zzeta.pde,[1 4 2 3]), 'title', 'K-BC pde at zeta = z');
		else
			dps.hyperbolic.plot.matrix_3D(test.K.bc.zzeta.pde, 'title', 'K-BC pde at zeta = z');
		end
		dps.hyperbolic.plot.matrix_3D(test.K.bc.zzeta.integralSolution-test.K.bc.zzeta.numericSolution, 'title', 'K-BC integral-fixpoint solution at zeta = z');
	end
end