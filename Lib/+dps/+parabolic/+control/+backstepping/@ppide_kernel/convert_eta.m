function [eta, xi] = convert_eta(kernel,i,j,xi_kl,eta_kl,k,l)
%PPIDE_KERNEL.CONVERT_ETA convert xi_coordinates from kl to ij representation

% created on 11.04.2017 by Simon Kerschbaum


% if nargin < 9
	[xi,eta] = kernel.convert_xi(i,j,xi_kl,eta_kl,k,l);
% else
% 	[xi,eta] = kernel.convert_xi(i,j,xi_kl,eta_kl,k,l,alpha_); %schneller mit alpha
% end
end

