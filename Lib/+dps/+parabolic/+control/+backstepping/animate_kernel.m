function animate_kernel( kernel_iterations )
%ANIMATE_KERNEL plot different steps of fixpoint iteration
%   animate_kernel(kernel_iterations) plots the different iteration steps
%   of the ppide_kernel created by 
%   [~,kernel_iterations] = fixpoint_iteration(...)

% Created on 12.09.2017 by Simon Kerschbaum

zdisc = kernel_iterations{1}.zdisc;
n = size(kernel_iterations{1}.value,1);

figG = figure('Name','Animation G','Position',[10 50 600 800]);
hpanelG = uipanel('position',[0 .05 1 .95],'title','step 1',...
	              'titleposition','centertop',...
				  'parent',figG);
callback = @(src,evt) hscroll_Callback(src,evt,kernel_iterations);
hscrollbarG = uicontrol('style','slider','units','normalized',...
	                 'position',[0 0 1 .05],'min',1,'max',length(kernel_iterations),...
					 'value',1,...
					 'SliderStep',[1/length(kernel_iterations) 1/length(kernel_iterations)],...
					 'callback',callback);
figH = figure('Name','Animation H','Position',[625 50 600 800]);
hpanelH = uipanel('position',[0 .05 1 .95],'title','step 1','titleposition','centertop',...
				  'parent',figH);
hscrollbarH = uicontrol('style','slider','units','normalized',...
	                 'position',[0 0 1 .05],'min',1,'max',length(kernel_iterations),...
					 'value',1,...
					 'SliderStep',[1/length(kernel_iterations) 1/length(kernel_iterations)],...
					 'callback',callback);
% ax1 = axes('parent',hpanel,'outerposition',[0 0 0.5 1]);
kernel_iterations{1}.plot('G','parent',hpanelG)
kernel_iterations{1}.plot('H','parent',hpanelH)


% h = animatedline;
% axis([0,4*pi,-1,1])
% numpoints = 100000;
% x = linspace(0,4*pi,numpoints);
% y = sin(x);
% a = tic; % start timer
% for k = 1:numpoints
%     addpoints(h,x(k),y(k))
%     b = toc(a); % check timer
%     if b > (1/100) 
%         drawnow % update screen every 1/30 seconds
%         a = tic; % reset timer after updating
%     end
% end

function hscroll_Callback(src,evt,kernel_iterations)
	id = round(get(src,'value'));
	set(hscrollbarG,'value',id);
	set(hscrollbarH,'value',id);
	kernel_iterations{id}.plot('G','parent',hpanelG)
	kernel_iterations{id}.plot('H','parent',hpanelH)
	drawnow()
end

end

