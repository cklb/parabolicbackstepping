
function [ G ] = decoupling_equations(system,sigMod,kernel)
%DECOUPLING_EQUATIONS Solve decoupling equatoins
%   Gamma = DECOUPLING_EQUATIONS(system, sigMod, kernel) solves the decoupling 
%       equations belonging to system system with respect to the signal
%       model sigMod
%   If system is of class ppide_sys (parabolic system), the backstepping
%   method is used, for which a ppide_kernel observer kernel is needed.
% 
%   INPUT PARAMETRS:
%     PPIDE_SYS      system   : plant
%     signalModel    sigMod   : signal model of the reference and
%                               disturbance
%     ppide_kernel   kernel   : kernel of the inverse backstepping observer
%                               transformation
% 
%   OUTPUT PARAMETERS:
%
%     ARRAY          G        : (ndisc x n x n_vd):  solution of the 
%                               decoupling equations.
%

% created on 02.05.2018 by Simon Kerschbaum
% TODO:
%  - Doku und verschiedene �bergabeklassen implementieren
%  - varargin erg�nzen, Variable 'silent' um Ausgabe im Command-Window zu
%    unterbinden. Au�erdem odeopt f�r den solver
% 
if ~isempty(sigMod.Sd)

	opts = odeset('RelTol',1e-9,'AbsTol',1e-9,'vectorized','on');
	% opts = odeset();

	import misc.*
	import numeric.*
	import dps.parabolic.system.*
	import dps.parabolic.control.backstepping.*
	import dps.parabolic.control.outputRegulation.*

	regtimer = tic;
	fprintf('Solving decoupling equations: ...\n');
	if isa(system,'ppide_sys')
	%	1. case, ppide_sys.
	%   solve the decoupling equations
	%   L(z)G'' - mu_o*G - - G Sd + H1_bar*pd_bar = 0
	%     b_op_l[G](0) = G2_bar
	%     b_op_r[G](1) = -int_0^1 A0_bar(z)G(z) dz + G3_bar
	%   while Sd may be a non-diagonalizable matrix

		%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
		% 1. Get System parameters in target system representation and do all
		% necessary auxiliary stuff
		% Delete reference part from signal model. --> No use to rewrite
		% everything:
		pd_til = sigMod.pd_til;
		Sd = sigMod.Sd;
		Sr = [];
		pr_til = [];
		sigMod = signalModel(Sd,Sr,pd_til,pr_til);

		a0_bar = kernel.a0_bar;
		zdisc_plant = linspace(0,1,system.ndiscPars);
		zdisc = kernel.zdisc;
		ndisc=length(zdisc);
		ndiscPars = length(zdisc_plant);
		n = system.n;
		nv = sigMod.nd; 
		l_disc = interp1(zdisc_plant,system.l_disc,zdisc(:));
		% mu: may be a constant matrix, an anonymous matrix function or a
		% discretized matrix function!
		if ndisc~= n
			if isa(system.ctrl_pars.mu_o,'function_handle') % function
				muSys = eval_pointwise(system.ctrl_pars.mu_o,zdisc);
			elseif isequal(size(system.ctrl_pars.mu_o,1),ndisc) % is discretized
				muSys = system.ctrl_pars.mu_o;
				warning('Check following: Needs Mu to be resampled?')
			elseif isequal(size(system.ctrl_pars.mu_o,1),n) % constant matrix, two dimensions
				muSys(1:ndisc,:,:) = repmat(shiftdim(system.ctrl_pars.mu_o,-1),ndisc,1,1);
			else % discretized matrix
				error('dimensions of mu_o not correct!')
			end
		else
			error('ndisc=n is a very bad choice!')
		end

	%	G1 = system.g_1; % anonymous function (z) dim: n x nd only used in
	%	discretized version
		G2 = system.g_2; % dim: n x nd % left disturbance input 
		G3 = system.g_3; % dim: n x nd % right disturbance input 
		nd = size(G2,2); % length of disturbance vector
		% check number of Dirichlet/Robin BC
		numDir = 0; % number of Dirichlet BC
		nowRobin = 0; % first non-Dirichlet BC has been detected.
		for i=1:n
			if system.b_op1.b_n(i,i)==0
				if ~nowRobin
					numDir = numDir+1;
				else
					error('The BC on the left side are not sorted. First have to be Dirichlet, then Robin/Neumann!')
				end
			else
				nowRobin = 1;
			end
		end
		if numDir > 0
			error('Decoupling equations currently only implemented for Robin BC left.')
		end
		% for later implementation
	% 	E1 = [eye(numDir);zeros(n-numDir,numDir)]; % selection matrix of DirBC
	% 	E2 = [zeros(numDir,n-numDir); eye(n-numDir)]; % selectio matrix of RobBC
		T1 = [eye(n); zeros(n,n)]; % selection matrix of first solution state
		T2 = [zeros(n,n); eye(n,n)]; % selection matrix of second solution state
		Q0 = []; % get Robin matrix from BC
		if numDir<n
			Q0 = system.b_op1.b_d(numDir+1:n,numDir+1:n);
		end
		% Right Boundary Matrices
		Bdr = system.b_op2.b_d;
		Bnr = system.b_op2.b_n;

		G2_bar = G2*sigMod.pd;
		G3_bar = G3*sigMod.pd;

		p = kernel.KI;
		g1_disc = interp1(zdisc_plant,system.g_1,zdisc(:));
		ToG1 = permute(Tc(permute(g1_disc,[3 1 2]),-p,zdisc,zdisc),[2 3 1]);
		H1_bar = zeros(ndisc,n,nd);
		for z_idx =1:ndisc
			H1_bar(z_idx,1:n,1:nd) = -squeeze(p(z_idx,1,:,:))*squeeze(l_disc(1,:,:))...
								 *G2 + sd(ToG1(z_idx,:,:));
		end

		% preallocate and starting value for solution
		% phi_til belonging to k=0 is zero and so is phi and gamma.
		% phi(1:ndisc,1:2n,k)
		% initialize solution array
		%    ndisc,  n,  k+1 (k starts at zero, index at 1 for each block!)
		%    +1 index for k=0 for each block
		gamma_j(1:ndisc,1:n,1:nv+length(sigMod.blockLength)) =0; 

		% prepare a vector to only select k>=1 later
		selectionVector(1:nv+length(sigMod.blockLength))=0;


		%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
		% 2. Calculate modal solution
		% loop through all jordan blocks:
		for jBlock = 1:length(sigMod.blockLength)
			A_for_vec(1:ndisc,1:(2*n)^2,1:(2*n)^2)=0; % preallocate
			A(1:ndisc,1:2*n,1:2*n) = 0; %preallocate
			Psi_mu_vec(1:ndisc,1:ndisc,1:(2*n)^2)=0; % preallocate
			Psi_mu(1:ndisc,1:ndisc,1:2*n,1:2*n) = 0; %preallocate
			try
				% currently considered eigenvalue
				mu = sigMod.lambda(sum(sigMod.blockLength(1:jBlock-1))+1);
			catch err
				mythrow(err)
			end
			for z_idx = 1:ndisc
				try
				A(z_idx,:,:) = ...
					[zeros(n,n)                                                       eye(n);
					 squeeze(l_disc(z_idx,:,:))\(squeeze(muSys(z_idx,:,:))+mu*eye(n)) zeros(n,n)];
				catch err
					mythrow(err);
				end
				for i=1:2*n %number of columns in Psi
					% create diagonal matrix of gammas
					A_for_vec(z_idx,(i-1)*2*n+1:i*2*n,(i-1)*2*n+1:i*2*n) = A(z_idx,:,:);
				end
			end

			%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
			% calculate fundamental matrix:
			% as the solution of 
			%     dz Psi_mu(z,zeta) = A(z) Psi_mu(z,zeta) z in (0,1),zeta
			%     in (0,z)
			%     Psi_mu(zeta,zeta) = I
			A_gen = @(z) squeeze(interp1(zdisc,A_for_vec,z(:)));
			Psi_0 = eye(2*n);
			Psi_vec_0 = Psi_0(:); % vectorized
			% Problem: matlab-solver only for vectors.
			% --> Matrix must be vectorized
			% --> A blockdiagonalized
			for zeta_idx =1:ndisc-1
				% starting value for z
				z0 = zdisc(zeta_idx); 
				f=@(z,Psi_vec) A_gen(z)*Psi_vec;
				[z,Psi_z_zeta_vec]=ode15s(f,[z0 1],Psi_vec_0,opts);
				Psi_z_zeta_vec_res = interp1(z,Psi_z_zeta_vec,zdisc(zeta_idx:ndisc).');
				Psi_mu_vec(zeta_idx:ndisc,zeta_idx,:) = Psi_z_zeta_vec_res;
			end
			Psi_mu_vec(ndisc,ndisc,:) = Psi_vec_0; %#ok<AGROW>
			% put vector in matrix form:
			for j=1:2*n
				%      z,zeta,i,j            z,zeta,i
				Psi_mu(:,:,:,j) = Psi_mu_vec(:,:,(j-1)*2*n+1:j*2*n);
			end
			% fundamental matrix ready.
			%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

			%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
			% calculate unknown initial value
			% phi_1_i_k_0
			N = zeros(ndisc,2*n,n); % preallocate and reset
			N_diff = zeros(ndisc,2*n,n); % preallocate and reset
			for z_idx=1:ndisc
				N(z_idx,:,:) = squeeze(Psi_mu(z_idx,1,:,:))*T2;
				N_diff(z_idx,:,:) = sd(A(z_idx,:,:))*sd(N(z_idx,:,:));
			end
			a0_bar_T1_N(1:ndisc,1:n,1:n)=0; %preallocate and reset
			for z_idx=1:ndisc
				a0_bar_T1_N(z_idx,:,:) = ...
					sd(a0_bar(z_idx,:,:))*T1.'*sd(N(z_idx,:,:));
			end

			% loop through all generalized eigenvectors of the current
			% jordan block and calculate gamma_j
			for k=1:sigMod.blockLength(jBlock)
				% k is index of genVec in current block
				% overall index of genVec:
				indexOfGenVec = sum(sigMod.blockLength(1:jBlock-1))+k;

				% index in pi:
				indexInGamma = sum(sigMod.blockLength(1:jBlock-1)+1)+k+1;

				% current generalized eigenvector:
				try
					varphi_j_k = sigMod.genEig(:,indexOfGenVec);
				catch err
					mythrow(err)
				end
				% nv x 1
				h1_j_k(1:ndisc,1:n)=0; %preallocate and reset
				h1_j_k_til(1:ndisc,1:2*n)=0; %preallocate and reset
				H1_j_mu(1:ndisc,1:2*n) = 0; %preallocate and reset
				H1_j_mu_diff(1:ndisc,1:2*n) = 0; %preallocate and reset
				% nx1
				h2_j_k = G2_bar*varphi_j_k;
				h3_j_k = G3_bar*varphi_j_k;
	% 			Hij_mu_til(1:ndisc,1:2*n)=0; % preallocate and reset
				% nx1
	% 			T1_H_til(1:ndisc,1:n) = 0;% preallocate and reset
				R1_j_mu_km1(1:ndisc,1:2*n)=0;% preallocate and reset
				R1_j_mu_km1_diff(1:ndisc,1:2*n)=0;% preallocate and reset
				T1_R_mu(1:ndisc,1:n) = 0; % preallocate and reset
				T1_R_mu_diff(1:ndisc,1:n) = 0; % preallocate and reset
				a0_bar_T1_H_R(1:ndisc,1:n) = 0; % preallocate and reset
				% nx1
	% 			h4_j_k = (sigMod.pr-G4_bar)*varphi_j_k;
	% 			M0_mu(1:ndisc,1:2*n,1:n) = 0; % preallocate and reset
				M_mu(1:ndisc,1:2*n,1:n) = 0;  % preallocate and reset
				T1_M_mu(1:ndisc,1:n,1:n) = 0; %preallocate and reset

				% TODO: Case that nd>1!
				for z_idx=1:ndisc %                   ndisc x n x nv*nv x nd
					h1_j_k(z_idx,:) = -shiftdim(H1_bar(z_idx,:,:),1)*pd_til*varphi_j_k;
					h1_j_k_til(z_idx,:) = T2/squeeze(l_disc(z_idx,:,:))...
						*reduce_dimension(h1_j_k(z_idx,:));
					temp_prod=0; %reset
					temp_prod(1:z_idx,1:2*n)=0; % preallocate;
					temp_prod_diff=0; %reset
					temp_prod_diff(1:z_idx,1:2*n)=0; % preallocate;
					for zeta_idx = 1:z_idx %                                        2n x 2n * 2n x 1
						temp_prod(zeta_idx,:) = (reduce_dimension(Psi_mu(z_idx,zeta_idx,:,:))*reduce_dimension(h1_j_k_til(zeta_idx,:)));
						temp_prod_diff(zeta_idx,:) = sd(A(zeta_idx,:,:))*sd(temp_prod(zeta_idx,:,:));
					end%     2n x 1
					H1_j_mu(z_idx,:) = trapz_fast(zdisc(1:z_idx),temp_prod.').';
					H1_j_mu_diff(z_idx,:) = h1_j_k_til(z_idx,:) ...
						+ trapz_fast(zdisc(1:z_idx),temp_prod_diff.').';


					%%%%%
					% get R1 from preceeding run:
					temp_prod_R = 0;
					temp_prod_R(1:z_idx,1:2*n) = 0;
					temp_prod_R_diff = 0;
					temp_prod_R_diff(1:z_idx,1:2*n) = 0;
					for zeta_idx=1:z_idx
						try
						temp_prod_R(zeta_idx,:) = ...
							reduce_dimension(Psi_mu(z_idx,zeta_idx,:,:))...
							* T2 / reduce_dimension(l_disc(zeta_idx,:,:)) ...
							* reduce_dimension(gamma_j(zeta_idx,:,indexInGamma-1));
												   % +1 for k=0 --> index 1
												   % -1 for k-1.
						temp_prod_R_diff(zeta_idx,:) = ...
							sd(A(zeta_idx,:,:))* sd(temp_prod_R(zeta_idx,:));
						catch err
							mythrow(err)
						end
					end
					R1_j_mu_km1(z_idx,:) = trapz_fast(zdisc(1:z_idx),temp_prod_R(:,:).').';
					R1_j_mu_km1_diff(z_idx,:) = (T2/sd(l_disc(z_idx,:,:))*sd(gamma_j(z_idx,:,indexInGamma-1))).'...
						+ trapz_fast(zdisc(1:z_idx),temp_prod_R_diff(:,:).').';
					T1_R_mu(z_idx,:) = T1.'*reduce_dimension(R1_j_mu_km1(z_idx,:));
					T1_R_mu_diff(z_idx,:) = T1.'*reduce_dimension(R1_j_mu_km1_diff(z_idx,:));

					a0_bar_T1_H_R(z_idx,:) = ...
						sd(a0_bar(z_idx,:,:))*T1.'*(sd(H1_j_mu(z_idx,:))+sd(R1_j_mu_km1(z_idx,:)));

					M_mu(z_idx,1:2*n,1:n) = squeeze(Psi_mu(z_idx,1,:,:))...
						*(T1-T2*Q0);
					T1_M_mu(z_idx,1:n,1:n) = T1.'*shiftdim(M_mu(z_idx,:,:),1);

				end
				% Calculate w_k:
				w_k = ...
					- Bdr*T1.'*sd(N(end,:,:))*h2_j_k - Bnr*T1.'*sd(N_diff(end,:,:))*h2_j_k ...
					- Bdr*T1.'*sd(H1_j_mu(end,:)) - Bnr*T1.'*sd(H1_j_mu_diff(end,:)) ...
					- Bdr*sd(T1_R_mu(end,:)) - Bnr*sd(T1_R_mu_diff(end,:)) ...
					+ h3_j_k ...
					- sd(trapz(zdisc,a0_bar_T1_N,1))*h2_j_k ...
					- sd(trapz(zdisc,a0_bar_T1_H_R,1));
				% calculate unknown starting value:
				% phi_1_j_k_0 = ...
				% phi_2_j_k_0 = ...
				theta_1_M = Bdr*sd(T1_M_mu(end,:,:))...
					+ Bnr*T1.'*sd(A(end,:,:))*sd(M_mu(end,:,:));
				a0_bar_T1_M = 0; % reset and preallocate
				a0_bar_T1_M(1:ndisc,1:n,1:n)=0;
				for z_idx=1:ndisc
					a0_bar_T1_M(z_idx,:,:) = ...
						sd(a0_bar(z_idx,:,:))*sd(T1_M_mu(z_idx,:,:));
				end
				temp1 = theta_1_M + sd(trapz(zdisc,a0_bar_T1_M,1));

				if abs(det(temp1))<1e-10
					warining('Decoupling equations hardly solvable')
				end
				if abs(det(temp1))==0
					error('Decoupling equations are not solvable!')
				end
				phi_1_j_k_til_0 = temp1\...
					w_k;
				phi_2_j_k_til_0 = -Q0*phi_1_j_k_til_0 + h2_j_k;
				phi_j_k_til_0 = [phi_1_j_k_til_0;phi_2_j_k_til_0];

				for z_idx =1:ndisc
					gamma_j(z_idx,1:n,indexInGamma) =...
						T1.'*squeeze(Psi_mu(z_idx,1,:,:))...
						* phi_j_k_til_0...
						+ T1.'*sd(H1_j_mu(z_idx,:))...
						+ sd(T1_R_mu(z_idx,:));
				end
				selectionVector(indexInGamma) = 1;
			end % gen eigenVec of current jBlock
		end % current jBlock

		% select pi for k>=1:
		gamma_j_k_g_0 = gamma_j(:,:,selectionVector==1);

		%%% modal solution computed
		%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

		%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
		% 3. Calculate original solution
		G(1:ndisc,1:n,1:nv)=0; %preallocate
		for z_idx=1:length(zdisc)
			G(z_idx,:,:) = sd(gamma_j_k_g_0(z_idx,:,:))/sigMod.genEig;
		end

		% eliminate numical imaginary parts:
		if any(imag(G)>1e-6)
			error('Solution of the decoupling equations contains imaginary parts!')
		else
			G=real(G);
		end



	fprintf('Finished solving decoupling equations! (%0.2fs) \n',toc(regtimer));


	else
		error('Decoupling equations only implemented for ppide_sys!')
	end

else
	G = [];
end

