classdef ppide_observer
	%PPIDE_OBSERVER Store ppide observer_parameters
	%   Contains the parameters L(z), L1 and Ld of a ppide observer.
	
% created on 09.05.2018 by Simon Kerschbaum
	
	properties
		ppide        % corresponding ppide (with ndiscObs)
% 		kernel       % observer kernel % dont store, too much memory needed!
		L            % distributed observer gain
		L1           % left boundary observer gain
		Ld           % disturbance observer gain
		Lr           % reference observer gain  
		Gamma        % solution of the decoupling equations
		sM           % signal model
	end
	
	methods
		[L, L1, Ld] = compute_ppide_observer(obj, GH_obs, ppide, Gamma, sM);
		% constructor
		function obj = ppide_observer(GH_obs, ppide, Gamma, sM)
% 			obj.kernel = GH_obs;
			obsvPpide = ppide;
			obsvPpide.ndisc = length(ppide.ctrl_pars.zdiscObs);
			obj.ppide = obsvPpide;
			obj.Gamma = Gamma;
			obj.sM = sM;
			[obj.L, obj.L1, obj.Ld] = obj.compute_ppide_observer(GH_obs, obsvPpide, Gamma, sM);
			
% 			[Lr, ew] = placeLQR(obj.sM.Sr.', obj.sM.pr_til.', obj.ppide.ctrl_pars.eigRef(:));
% 			[V, J, l] = jordan_complete(obj.sM.Sr);
% 			p_til = obj.sM.pr*V;
% 			decoupled_index=1;
% 			for output=1:size(sM.pr_til, 1)
% 				for block_idx=1:length(l)
% 					for pr_idx = 1:l(block_idx)
% 						if p_til(output, pr_idx) ~= 0
% 							blockInfluencesOutput(output, block_idx)=1;
% 						end
% 					end
% 				end
% 			end
% 			for k=1:sigMod.blockLength(jBlock)
			% k is index of genVec in current block
			% overall index of genVec:
% 			indexOfGenVec = sum(sigMod.blockLength(1:jBlock-1))+k;
			
			% index in pi:
% 			indexInPi = sum(sigMod.blockLength(1:jBlock-1)+1)+k+1;
			
% 			for output=1:size(sM.pr_til, 1)
% 				for block_idx=1:length(l)
% 					blockStart = sum(l(1:block_idx-1))+1;
% 					if blockInfluencesOutput(output, block_idx)==1
% 						Sr_temp_transformed = blkdiag(Sr_temp_transformed, J(blockStart:blockStart+l(block_idx), blockStart:blockStart+l(block_idx)));
% 					end
% 				end
% 			end
				
			% Fuehrungsverhalten kann normalerweise immer f�r jeden
			% Ausgangs getrennt entworfen werden!
% 			designComplete=0;
% 			for y_idx = 1:size(obj.sM.pr_til, 1)
% 				if det(obsv(obj.sM.Sr, obj.sM.pr_til(y_idx, :))) < 1e-3
% 					designComplete=1;
% 				end
% 			end
% 			if ~designComplete
% 				Lr(:, y_idx) = place(obj.sM.Sr.', obj.sM.pr_til(y_idx, :).', obj.ppide.ctrl_pars.eigRef(:)).';
% 			else
% 				warning('Reference observer cannot be designed for each output separately!')
			if ~isempty(obj.sM.Sr)
				obj.Lr = place(obj.sM.Sr.', obj.sM.pr_til.', obj.ppide.ctrl_pars.eigRef(:)).';
			else
				obj.Lr = double.empty(0, size(obj.sM.pr,1));
			end
% 			end
			
% 			if any((ew(:)-obj.ppide.ctrl_pars.eigRef(:))>1e-2)
% 				warning(['Accuracy of reference observer is not so good! Eigenvalues: ' num2str(ew.')])
% 			end
		end
		
		
		function plot(obj)
			%% plot surface of observer
			
			figure('Name','ObserverSurface')
			num=1;

			res_surf_t=31;
			res_surf_z=21;
			zdiscKernel = obj.ppide.ctrl_pars.zdiscObs;
			ndiscKernel = numel(zdiscKernel);
			zres = linspace(0,1,res_surf_z);
			n = obj.ppide.n;
			if size(obj.L,4)>1
				% time dependent surface plot
				tres = linspace(0,1,res_surf_t);
				tDiscKernel = linspace(0,1,size(obj.L,4));
				[Z,T] = meshgrid(zdiscKernel,tDiscKernel);
				[ZRES,TRES] = meshgrid(zres,tres);
				
				for i=1:n
					for j=1:n
						handles(num) = subplot(n,n,num);
						yres = interp2(Z,T,reshape(obj.L(:,i,j,:),[ndiscKernel, length(tDiscKernel)]).',ZRES,TRES);
						surf(TRES,ZRES,yres);
						hold on
						box on
						grid on
						set(gca,'Ydir','reverse')
						xlabel('$t$')
						ylabel('$z$')
						zlabel(['$L_{' num2str(i) num2str(j) '}(z,t)$'])
						num=num+1;
					end
				end
				Link = linkprop(handles,{'view'});
				setappdata(gcf, 'StoreTheLink', Link);
% 				linkaxes(handles);
			else
				for i=1:n
					for j=1:n
						subplot(n,n,num);
						yres = interp1(zdiscKernel,obj.L(:,i,j),zres);
						plot(zres,yres);
						hold on
						box on
						grid on
						xlabel('$z$')
						zlabel(['$L_{' num2str(i) num2str(j) '}(z)$'])
						num=num+1;
					end
				end
			end
		end
	end
	
end

