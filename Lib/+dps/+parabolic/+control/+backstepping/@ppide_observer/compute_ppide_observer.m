function [L, L0, Ld] = compute_ppide_observer(~, GH_obs, system, Gamma, sM)
% COMPUTE_PPIDE_OBSERVER compute observer gains for ppides.
%   [L, L1] = compute_ppide_observer(GH_obs, ppide)
%     computes the gains L(z) and L1 for the ppide-observer
%             \dh{x} = L x'' + A(x) + L(z) * (eta-\hat{x}(0))
%        b_op1[x](0) = L1 * (eta-\hat{x}(0))
%        b_op2[x](1) = u.
%
%   [L, L1, Ld] = compute_ppide_observer(GH_obs, ppide, Gamma, sM)
%     computes the gains for the disturbance observer
%                v_t = Sd v + Ld * (eta-x(0))
%                x_t = L x'' + A(x) + L(z) * (eta-x(0)) + G1(z) * d
%        b_op1[x](0) = L1 * (eta-\hat{x}(0)) + G2 * d
%        b_op2[x](1) = u + G3 * d.
%     where Gamma is the solution of the decoupling equations and sM is the
%     correspondiong signal model.

% created on 03.05.2018 by Simon Kerschbaum
% TODO: Doku!


%% Imports and input check
import misc.*
import numeric.*
import dps.parabolic.system.*
import dps.parabolic.control.backstepping.*
import dps.parabolic.control.outputRegulation.*

%% Init parameters
zdisc = GH_obs.zdisc;
ndisc = length(zdisc);
ndiscPlant = system.ndiscPars;
zdiscPlant = linspace(0, 1, ndiscPlant);
ndiscObs = system.ndisc;
zdiscObs = linspace(0, 1, ndiscObs);
lDisc = interp1(zdiscPlant, system.l_disc, zdisc(:));
lDiffDisc = interp1(zdiscPlant, system.l_diff_disc, zdisc(:));
n = system.n;
pI = GH_obs.get_K;
numtKernel = size(pI,5);
a0Disc = translate_to_grid(system.a_0_disc, zdisc);
if size(a0Disc,4)>1
	a0Disc = permute(translate_to_grid(permute(a0Disc,[4 1 2 3]), linspace(0,1,numtKernel)),[2 3 4 1]);
end
pIZeta = GH_obs.get_K('dzeta_K_z_0',2);
Bdl = system.b_op1.b_d;
tDisc = linspace(0,1,numtKernel);
if isa(Bdl,'function_handle')
	BdlDisc = permute(misc.eval_pointwise(Bdl,tDisc),[2 3 1]);
else
	if size(Bdl,3)== 1
		BdlDisc = repmat(Bdl,1,1,numtKernel);
	else
		BdlDisc = misc.translate_to_grid(Bdl,{1:n,1:n,linspace(0,1,numtKernel)},'gridOld',...
			{1:n,1:n,1:linspace(0,1,size(Bdl,3))});
	end
end

if nargin < 3 || isempty(Gamma)
	% assume that the signal model is empty
	Sd = [];
	Sr = [];
	pd_til = [];
	pr_til = [];
	sM = signalModel(Sd, Sr, pd_til, pr_til);
	ew = [];
	Gamma = double.empty(ndisc, n, 0);
else
	ew = system.ctrl_pars.eigDist;
end

% Backstepping transformation of Gamma
ToInvGamma = permute(Tc(permute(Gamma, [3 1 2]), pI, zdisc, zdisc), [2 3 1]);
ToInvGamma0 = sd(ToInvGamma(1, :, :, :));

%% Observer gain of disturbance observer ode-subsystem
% Ld = place(sM.Sd.', ToInvGamma0.', ew).';
ew_desired = (ew);
% Find Q-Matrix (quadratic to achieve the desired eigenvalues:)
% opts = optimset('Display', 'none');
% Q_vec = fmincon(@(fac)get_ew_diff(fac, ew_desired, sM, ToInvGamma0), 2 * ones(sM.nd, 1), [], [], [], [], zeros(sM.nd, 1), [], [], opts);
% Q = diag(Q_vec);
% 
% % Q = factor * eye(sM.nd);
% R = eye(sM.nd);
% N = zeros(sM.nd, sM.nd);
% [LdT, ~, ~] = lqr(sM.Sd.', ToInvGamma0.', Q, R, N);
% Ld_comp = LdT.';
if any(ToInvGamma0)
	if numtKernel>1
		error('This does not work for time varying kernels!')
	end
	Ld = placeLQR(sM.Sd.', ToInvGamma0.', ew_desired).';
% 	Ld = place(sM.Sd.', ToInvGamma0.', ew_desired).';
else
	Ld = zeros(sM.nd, n);
end
% if any(Ld_comp-Ld)
% 	error('Something is wrong!')
% end


%% Calculate ppide-state-observer
% First, check of the measurement fullfills all assumptions. Second,
% calculate both feedback gains for the Dirichlet and the Neumann+Robin
% case. Thirldy, combine those observer gains to the final kind.

% Check if measurement is of wrong type
if any(system.cm_op.c_1(:)) || any(system.cm_op.c_d1(:)) ...	% measurement only at left boundary
	|| any(system.cm_op.c_m(:)) || any(system.cm_op.c_dm(:)) ...% measurement only at left boundary
	|| ~isdiag(system.cm_op.c_d0) || ~isdiag(system.cm_op.c_0) ...	% only diagonal measurement
	|| ~misc.iseye(system.cm_op.c_d0 + system.cm_op.c_0) ...	% either neumann or dirichlet measurement
	|| ~logical(all((diag(system.cm_op.c_d0) == 0) + (diag(system.cm_op.c_d0) == 1))) ... % all elements of
	|| ~logical(all((diag(system.cm_op.c_0) == 0) + (diag(system.cm_op.c_0) == 1)))	% measure-matrices
																			% must be 0 or 1
	% Neumann matrix must be diagonal
	error(['The current observer design only permits boundary measurements', ...
		' of dirichlet type and of neumann/robin-type with diagonal ', ...
		'neumann matrices cm_op.c_d0 and cm_op.c_d1.'])
end

% check number of Dirichlet/Robin BC
numDir = 0; % number of Dirichlet BC
nowRobin = 0; % first non-Dirichlet BC has been detected.
for i=1:n
	if system.b_op1.b_n(i,i)==0
		if ~nowRobin
			numDir = numDir+1;
		else
			error('The BC on the left side are not sorted. First have to be Dirichlet, then Robin/Neumann!')
		end
	else
		nowRobin = 1;
	end
end
E1 = [eye(numDir);zeros(n-numDir,numDir)]; % selection matrix of DirBC
% E2 = [zeros(numDir,n-numDir); eye(n-numDir)]; % selectio matrix of RobBC
selDirVec = 1:numDir;
selRobVec = numDir+1:n;
mTn = system.ctrl_pars.makeTargetNeumann;

% Neumann+Robin measurement
% Observer gain at boundary
L0E1 = zeros(n,numDir,numtKernel); % yep, L1 really is zero for Dirichlet BCs

% Distributed observer gain can be calculated and afterwards columns are selected
LzE1full = zeros(ndisc, n, n,numtKernel); %preallocate
for zIdx=1:ndisc
	for tIdx =1:numtKernel
		if size(lDisc,4)>1
			tIdxL = tIdx;
		else
			tIdxL = 1;
		end
		if size(ToInvGamma,4)>1, tIdxGamma = tIdx; else tIdxGamma = 1; end
		LzE1full(zIdx, :, :, tIdx) = permute(misc.multArray(ToInvGamma(zIdx, :, :, tIdxGamma),Ld,3,1),[2 3 1 4]) ...
			- permute(misc.multArray(pI(zIdx, 1, :, :, tIdx), lDisc(1, :, :, tIdxL),4,2),[3 5 1 2 4]);
	end
end

% Dirichlet measurement
% Observer gain at boundary
% L0E2
L0E2 = vertcat(...
		zeros(numDir,n-numDir,numtKernel),...
		reshape(pI(1, 1, selRobVec, selRobVec, :),[n-numDir,n-numDir,numtKernel])...
		- mTn*BdlDisc(selRobVec,selRobVec,:)...
		);
	
% Distributed observer gain
LzE2full = zeros(ndisc, n, n, numtKernel); %preallocate
for zIdx=1:ndisc
	for tIdx =1:numtKernel
		if size(lDisc,4)>1, tIdxL = tIdx; else, tIdxL = 1; end
		if size(BdlDisc,3)>1, tIdxBdl = tIdx; else, tIdxBdl = 1;	end
		if size(a0Disc,4)>1, tIdxa0 = tIdx; else, tIdxa0 = 1;	end
		if size(ToInvGamma,4)>1, tIdxGamma = tIdx; else tIdxGamma = 1; end
		LzE2full(zIdx, :, :, tIdx) = ...
			permute(ToInvGamma(zIdx, :, :, tIdxGamma), [2, 3, 1]) * Ld ...
			+ ~mTn*reshape(pI(zIdx, 1, :, :, tIdx), [n, n]) * reshape(lDisc(1, :, :, tIdxL), [n, n]) * BdlDisc(:,:,tIdxBdl) ...
			+ reshape(pIZeta(zIdx, :, :, tIdx), [n, n]) * reshape(lDisc(1, :, :, tIdxL), [n, n]) ...
			+ reshape(pI(zIdx, 1, :, :, tIdx), [n, n]) * reshape(lDiffDisc(1, :, :, tIdxL), [n, n]) ...
			+ reshape(a0Disc(zIdx, :, :, tIdxa0), [n, n]) ...
			- squeeze(pI(zIdx,1,:,:,tIdx))*squeeze(lDisc(1,:,:,tIdxL))...% Vorzeichen pruefen!!
				*(E1*E1.')*squeeze(pI(1,1,:,:,tIdx));
	end
end
% warning('Think about makeTargetNeumann for observer design! LSpDirichlet should not get Bdl then, but the kernel equations do!')

% Select repective Columns out of LzE1full and LzE2 full
Lz= zeros(ndisc, n, n,numtKernel);
for it = 1:n
	if (system.cm_op.c_d0(it, it) == 1) && (system.cm_op.c_0(it, it) == 0) % Neumann measurement at left boundary
% 		Lz(:, :, it, :) = LzE1full(:, :, it, :);
		if any(Ld(:))
			error('Yet, the disturbance observer is not implemented for neumann measurement');
		end
	elseif (system.cm_op.c_0(it, it) == 1)  % Dirichlet measurement at left boundary
% 		Lz(:, :, it, :) = LzE2full(:, :, it, :);
	else % Further parameter checks in the beginning of the calculation 
% 		% of the "%% Calculate ppide-state-observer" section
		error(['Measurement y_m_,_', num2str(it), ' is of wrong type']);
	end
end
% Test
Lz(:,:,selDirVec,:) = LzE1full(:,:,selDirVec,:);
Lz(:,:,selRobVec,:) = LzE2full(:,:,selRobVec,:);
L0 = horzcat(L0E1,L0E2);
% L0(:, it, :) = L0E1(:, it, :);
% L0(:, it, :) = L0E2(:, it, :);
% resample to observer resolution
L = interp1(zdisc, Lz, zdiscObs(:));
% L_comp = interp1(zdisc, LSp_comp, zdiscObs(:));


end

function ew_diff = get_ew_diff(Q, ew_desired, sM, ToInvGamma0)
	Q = diag(Q);
	[~, ~, ewR] = lqr(sM.Sd.', ToInvGamma0.', Q, eye(sM.nd), zeros(sM.nd, sM.nd));
	ew_diff = max(max(abs((ewR)-ew_desired)));

end