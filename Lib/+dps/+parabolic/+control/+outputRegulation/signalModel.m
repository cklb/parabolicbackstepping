classdef signalModel
	%SIGNALMODEL class for a signal model
	%  \dot{v} = S v = diag(Sd,Sr)*[vd vr]^T \\
	%        d = pd v = pd_til * vd
	%        r = pr v = pr_til & vr
	%
	%   signalModel = signalModel(...) creates a new signal model object.
	
	% created on 12.03.2018 by Simon Kerschbaum
	
	% modified on 25.06.2018 by SK:
	%   * enabled passing of pd, pr instead of prTil, pdTil to create a
	%   not-decoupled signal model for robust control!
	
	properties(SetAccess=private)
		nv            % order of the model
		nd            % order of disturbance model
		nr            % order of reference model
		Sd            % disturbance dynamics
		Sr            % reference dynamics
		S             % signal dynamics
		pd_til        % output matrix of disturbance vector in disturbance model
		pr_til        % output matrix of reference vector in reference model
		pd            % output matrix of disturbance vector in full model
		pr            % output matrix of reference vector in full model
		% Evtl. Anfangswert aber eigentlich nicht.
		eigenVec     % matrix of eigenvectors
		lambda        % vector of eigenvalues
		jordan        % jordan-matrix of S
		genEig        % matrix of generalized eigenvectors
		blockLength   % vector containing the lengths of the jordan blocks
		
	end
	methods
		% constructor
		function obj = signalModel(Sd,Sr,pd_til,pr_til,varargin)
			% obj = SIGMOD(Sd,Sr,pr_til,pd_til,varargin)
			%   create new sigMod object.
			%   Currently no arguments can be passed by varargin.
			import misc.*
			
			arglist = {};
			found=0;
			if ~isempty(varargin)
				if mod(length(varargin),2) % uneven number
					error('When passing additional arguments, you must pass them pairwise!')
				end
				for index = 1:2:length(varargin) % loop through all passed arguments:
					for arg = 1:length(arglist)
						if strcmp(varargin{index},arglist{arg})
							obj.(arglist{arg}) = varargin{index+1};
							found=1;
							break
						end 
					end % for arg
					% argument wasnt found in for-loop
					if ~found
						error([varargin{index} ' is not a valid property of class ' class(obj) '!']);
					end
					found=0; % reset found
				end % for index
			end  % if isempty(varargin)
			% if arguments were not passed!
			if ~exist('Sd','var')
				Sd = double.empty(0,0);
				pd_til = double.empty(0,0);
			end
			if ~exist('Sr','var')
				Sr = double.empty(0,0);
				pr_til = double.empty(0,0);
			end
			% check if Sd and Sr are square
			for arg = {'Sd','Sr'}
				eval(['siz.' arg{1} '=size(' arg{1} ');']);
				if siz.(arg{1})(1) ~= siz.(arg{1})(2)
					error([arg{1} ' must be a square matrix!']);
				end
			end
			% check if dimensions of vectors are appropriate:
			if size(pd_til,2)~= size(Sd,1)
				error(['size(pd_til,2) = ' num2str(size(pd_til,2)) ' ~= '...
					'size(Sd,1) = ' num2str(size(Sd,1))]);
			end
			if size(pr_til,2)~= size(Sr,1)
				error(['size(pr_til,2) = ' num2str(size(pr_til,2)) ' ~= '...
					'size(Sr,1) = ' num2str(size(Sr,1))]);
			end
			
			% store variables that simplify usage:
			obj.Sd = Sd;
			obj.Sr = Sr;
			obj.pd_til = pd_til;
			obj.pr_til = pr_til;
			obj.S = blkdiag(Sd,Sr);
			obj.nv = size(obj.S,1); 
			obj.nd = size(obj.Sd,1); 
			obj.nr = size(obj.Sr,1);
			obj.pd = [obj.pd_til zeros(size(obj.pd_til,1),obj.nr)];
			obj.pr = [zeros(size(obj.pr_til,1),obj.nd) obj.pr_til];
			
			% calculate eigenvalues and vectors:
			% problem: cant be calculated with eig, because the order should be
			% according to the jordan form.
% 			[obj.eigenVec,obj.lambda] = eig(obj.S,'vector');
			% and jordan normal form
			[obj.genEig,obj.jordan,obj.blockLength] = jordan_complete(obj.S);
			% calcluate eigenvalues and vectors:
			% important to do it manually, so the order is correct!
			% loop through jordan blocks:
			for jBlock = 1:length(obj.blockLength)
				indexOfGenVec = sum(obj.blockLength(1:jBlock-1))+1;
				% repeat eigenvector and eigenvalue
				obj.eigenVec(:,indexOfGenVec:indexOfGenVec+obj.blockLength(jBlock)-1)...
					= repmat(obj.genEig(:,indexOfGenVec),1,obj.blockLength(jBlock));
				obj.lambda(indexOfGenVec:indexOfGenVec+obj.blockLength(jBlock)-1)...
					= repmat(obj.jordan(indexOfGenVec,indexOfGenVec),1,obj.blockLength(jBlock));
			end
		end % constructor
		
		%%%%%%%%%%%%%%%%%%%%%%%%%
		% nicht vergessen: Setters f�r �nderungen!
	end
	
end

