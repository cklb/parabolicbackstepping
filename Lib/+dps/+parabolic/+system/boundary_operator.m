classdef boundary_operator
% Class BOUNDARY_OPERATOR: represents a boundary operator of the form
%    B[x(t)](z_b) = b_d x(z_b,t) + b_n x_z(z_b,t);

% created by Simon Kerschbaum, 2015
% modified on 09.09.2016 by SK:
%   * modify to be able to imeplemt systems of pdes

    properties
        z_b = 0; 
		b_d = 1;
		b_n = -1; %default: robin BC
		Sd        % matrices to select Dirichlet-states
		Sr        % matrices to select Robin-states
    end
    methods
		function obj = set_params(obj,z_b,b_d,b_n)
		% set boundary_operator parameters:
		% b_op = b_op.set_params(obj,z_b,b_d,b_n)
		
			if z_b<0 || z_b >1
				error('z_b must be set between 0 and 1!')
			end
			if ~isa(b_d,'function_handle')
				if any(size(b_d) ~= size(b_n)) || size(b_d,1) ~= size(b_d,2)
					error('b_d and b_n must be quadratic and of same size!')
				end
			else
				if any(size(b_d(0)) ~= size(b_n)) || size(b_d(0),1) ~= size(b_d(0),2)
					error('b_d and b_n must be quadratic and of same size!')
				end
			end
			obj.z_b =z_b;
			obj.b_d =b_d;
			obj.b_n =b_n;
		end
		function obj= boundary_operator(varargin)
			% obj = BOUNDARY_OPERATOR(varargin)
			%   create new boundary_operator object.
			% parameters: z_b, b_d, b_n
			arglist = {'z_b','b_d','b_n'};
			numfix=0;
			for arg = 1:length(arglist)
				if(nargin > numfix)
					for index = 1:2:(nargin-numfix)
						if nargin==index, break, end
						switch lower(varargin{index})
						case arglist{arg}
						  obj.(arglist{arg}) = varargin{index+1};
						end
					end
				end
			end	
			if isprop(obj,'z_b')
				if obj.z_b<0 || obj.z_b >1
					error('z_b must be set between 0 and 1!')
				end
				if ~isa(obj.b_d,'function_handle')
					if any(size(obj.b_d,1) ~= size(obj.b_n,1)) || size(obj.b_d,1) ~= size(obj.b_d,2)
						error('b_d and b_n must be quadratic and of same size!')
					end
				else
					if any(size(obj.b_d(0)) ~= size(obj.b_n)) || size(obj.b_d(0),1) ~= size(obj.b_d(0),2)
						error('b_d and b_n must be quadratic and of same size!')
					end
				end
			end% if
			
			% calculate Sd, Sr:
			% check number of Dirichlet/Robin BC
% 			numDir = 0; % number of Dirichlet BC
% 			nowRobin = 0; % first non-Dirichlet BC has been detected.
			rowIdxD =1;
			rowIdxR =1;
			n = size(obj.b_n,1);
			SdT = zeros(0,n);
			SrT = zeros(0,n);
			for i=1:n
				if obj.b_n(i,i)==0
					SdT(rowIdxD,i) = 1;
					rowIdxD = rowIdxD+1;
				else
					SrT(rowIdxR,i) = 1;
					rowIdxR = rowIdxR+1;
				end
			end
			obj.Sd = SdT.';
			obj.Sr = SrT.';			
		end
	end
end