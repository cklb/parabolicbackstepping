classdef ppide_sys
% Class ppide_sys: represents a system of 1D parabolic P(I)DEs with
% spatially varying coefficients
%	
%	sys = PPIDE_SYS(...) 
%	  creates a new ppde_sys object with default properties. 
%	  Using ('parameter_name',value) syntax can pass different parameters.
%
%	The pPDE system is represented by the expressions
%
%	  x_t(z,t) = l(z) x_zz(z,t)+ c(z) x_z(z,t) + a(z) x(z,t) 
%				+ a_0(z) x(0,t) + a_1(z) x(1,t) + c_0(z) x_z(0,t) + c_1(z) x_z(1,t)
%               + a_z0(z) x(z0,t) + c_z0(z) x_z(z0,t)
%				+ int_0^z f(z,zeta)x(zeta,t)dzeta
%				+ b(z) u(t) + g_1(z) d(t)
%	  b_op1[x(t)](0) = b_1 u(t) + g_2 d(t)
%	  b_op2[x(t)](1) = b_2 u(t) + g_3 d(t)
%				y(t) = c_op x + g_4 d(t),
%			  y_m(t) = cm_op x
%	  where x(z,t) is a column vector of order n.
%
%   Properties:
%		In the following, the type "DISCRETIZED" describes an array, whose first dimension(s) 
%	  describe the spatial variable(s), the following dimesions are the remaining dimensions 
%	  of the matrix. E.g. l is a matrix dependent on z, so the discretized version will
%	  have 3 dimensions, where the first is the spatial variable z and the second and third
%	  are the dimensions of the matrix.
%		The type "FUNCTION" represents an anonymous function with as many variables as there
%	  are spatial dependencies. The functions must return a matrix of suitable dimensions.
%		All spatially-varying coefficients may be passed as DISCRETIZED or FUNCTION. If passed
%	  as FUNCTION, it will be automatically discretized and stored in that form.
%		The default value of a property is described in the column DEFAULT. Of course, they
%	  are stored in the correct dimensions. NUMERICAL means that the derivatives are computed
%	  numerically. The parameter l is mandatory and needs to be passed!
%
%	 TYPE			   NAME			DEFAULT			: DESCRIPTION
%	 ARRAY or FUNCTION  l			-				: Diffusion matrix
%	 ARRAY or FUNCTION  l_diff		NUMERICAL		: First spatial derivative of diffusion matrix 
%	 ARRAY or FUNCTION	l_diff2		NUMERICAL		: Second spatial derivative of diffusion matrix 
%	 ARRAY or FUNCTION  c			0(z->n x n)		: Convection matrix
%	 ARRAY or FUNCTION  c_diff		NUMERICAL		: First spatial derivative of convection matrix
%	 ARRAY or FUNCTION  a			0(z->n x n)		: Reaction matrix
%	 ARRAY or FUNCTION  a_0			0(z->n x n)		: Local coupling matrix
%	 ARRAY or FUNCTION  a_1			0(z->n x n)		: Local coupling matrix
%	 ARRAY or FUNCTION  a_z0		0(z->n x n)		: Local coupling matrix
%	 ARRAY or FUNCTION  c_0			0(z->n x n)		: Local coupling matrix
%	 ARRAY or FUNCTION  c_1			0(z->n x n)		: Local coupling matrix
%	 ARRAY or FUNCTION  c_z0		0(z->n x n)		: Local coupling matrix
%	 VECTOR				z0			0(1 x n)		: Vector of local coupling points for a_z0
%                                                     and c_z0.
%	 ARRAY or FUNCTION  f			0(z,ze->n x n)	: Integral coupling matrix
%	 ARRAY or FUNCTION  b			0(z->n x p)		: Distributed input matrix
%	 ARRAY or FUNCTION  g_1			0(z->n x nd)	: Distributed disturbance input matrix
%	 MATRIX				b_1			0(n x p)		: Left boundary input matrix
%	 MATRIX				g_2			0(n x nd)		: Left boundary disturbance input matrix
%	 MATRIX				b_2			0(n x p)		: Right boundary input matrix
%	 MATRIX				g_3			0(n x nd)		: Right boundary disturbance input matrix
%	 MATRIX				g_4			0(m x nd)		: Disturbance output matrix
%	 BOUNDARY_OPERATOR  b_op1		NEUMANN			: Left Boundary operator
%	 BOUNDARY_OPERATOR  b_op2		NEUMANN			: Right Boundary operator
%	 OUTPUT_OPERATOR	c_op		0(m x n)		: Output operator
%	 OUTPUT_OPERATOR	cm_op		0(mm x n)		: Measurement output operator


% created by Simon Kerschbaum on 03.04.2017
% modified on 13.03.2018 by SK:
%  * implementation of disturbance inputs
% modified on 20.03.2018 bz SK:
%  * disturbance input matrices are automatically set in appropriate
%	dimension if any is set.
% modified on 28.06.2018 by SK:
%  * only store discretized parameters, so that no mistakes can be made.

% TODO: set-functions so that sizes are checked not only on creation but also on set!
% documentation for time dependent parameters

	properties   % properties with independet set/get functions.
		n					% system order
		l					% diffusion matrix 
		l_diff				% first spatial derivative of diffusion matrix
		l_diff2				% second spatial derivative of diffusion matrix
		l_t					% time-derivative of diffusion matrix
		a					% reaction matrix
		c					% convection matrix
		c_diff				% spatial derivative of convection matrix
		b					% distributed input matrix
		b_op1				% left boundary operator (default Neumann)
		b_op2				% right boundary operator
		b_1					% left boundary input matrix
		b_2					% right boundary input matrix
		c_op				% output_operator 
		cm_op				% measurement output operator (for disturbance observer)
		ctrl_pars			% controller parameters
		a_0;				% local coupling matrix 
		a_1;				% local coupling matrix 
		a_z0;				% local coupling matrix 
		c_0;				% local coupling matrix 
		c_1;				% local coupling matrix 
		c_z0;				% local coupling matrix 
		z0;
		f					% integral coupling matrix
		g_1;				% distributed disturbance input matrix
		g_2					% left boundary disturbance input matrix
		g_3					% right boundary disturbance input matrix
		g_4					% disturbance output matrix
		ndisc = 151;		% order of approximation for simulation
		ndiscTime = 151;	% order of time discretisation for simulation
		name				% name of the system
		x0                  % initial value for simulation. Typically as anonymous function
		folded = 0;         % flag if the ppide is a result of folding
	end
	
	properties (Dependent=true)   % all parameters whose set and get functions influence or are influenced by another property
		ndiscPars   % Number of points with which the system parameters are discretized.
	end
	properties(Access = private)
		ndiscParsPriv = 151;
	end
	
	properties (Dependent = true,Hidden = true) % hidden: kann nicht ueber name-value syntax geschrieben werden!
		l_disc	  % store old names of paramters, to still be used equivalently to new names
		a_disc
		b_disc
		c_disc
		c_diff_disc
		l_diff_disc
		l_diff2_disc
		g_1_disc
		a_0_disc
		a_1_disc
		c_0_disc
		c_1_disc
		f_disc
	end

	
	methods (Static, Access=protected)
		function value = makeAnonymFunction(value)
			  if ~isa(value, 'function_handle')
				value = @(z) 0*z + value;
			  end
		end
	end
	
	methods

		
		function figHandle  = plot(obj,var)
			p = inputParser;
			p.CaseSensitive = false;
			p.addRequired('variable',@(var) ischar(var));
			parse(p,var);
% 			if ~isa(p.Results.variable,'char')
% 				error('var must be a string!')
% 			end
			arglist={'a','l','l_diff','l_diff2','c'};
			isin = 0;
			for idx = 1:length(arglist)
				if strcmp(p.Results.variable,arglist{idx})
					isin=1;
				end
			end
			if ~isin
				warning([p.Results.variable ' cannot be plotted!'])
				return
			end
% 			if size(obj.(p.Results.variable),4)>1
% 				varPerm = permute(obj.(p.Results.variable),[1 4 2 3]);
% 				figHandle = dps.hyperbolic.plot.matrix_4D(varPerm);
% 			else
				figHandle = dps.hyperbolic.plot.matrix_3D(obj.(p.Results.variable),'title',p.Results.variable);
% 			end
		end
			
		
		%% Setters:
		function obj = set.ndiscPars(obj,value)
% 			if ndisc is changed, all the discretized variables need to be
% 			reshaped!
			zdisc_old = linspace(0,1,obj.ndiscPars); %#ok<NASGU>
			zdisc = linspace(0,1,value);
			% first solution: resampling!
			%  if it is the first writing, then l will be empty
			if ~isempty(obj.l)
				zdisc_old = linspace(0,1,size(obj.l,1));
				obj.l = interp1(zdisc_old,obj.l,zdisc(:));
			end
			if ~isempty(obj.l_diff)
				zdisc_old = linspace(0,1,size(obj.l_diff,1));
				obj.l_diff = interp1(zdisc_old,obj.l_diff,zdisc(:));
			end
			if ~isempty(obj.l_diff2)
				zdisc_old = linspace(0,1,size(obj.l_diff2,1));
				obj.l_diff2 = interp1(zdisc_old,obj.l_diff2,zdisc(:));
			end
			if ~isempty(obj.a)
				zdisc_old = linspace(0,1,size(obj.a,1));
				obj.a = interp1(zdisc_old,obj.a,zdisc(:));
			end
			if ~isempty(obj.a_0)
				zdisc_old = linspace(0,1,size(obj.a_0,1));
				obj.a_0 = interp1(zdisc_old,obj.a_0,zdisc(:));
			end
			if ~isempty(obj.a_1)
				zdisc_old = linspace(0,1,size(obj.a_1,1));
				obj.a_1 = interp1(zdisc_old,obj.a_1,zdisc(:));
			end
			if ~isempty(obj.c_0)
				zdisc_old = linspace(0,1,size(obj.c_0,1));
				obj.c_0 = interp1(zdisc_old, obj.c_0, zdisc(:));
			end
			if ~isempty(obj.c_1)
				zdisc_old = linspace(0,1,size(obj.c_1,1));
				obj.c_1 = interp1(zdisc_old,obj.c_1,zdisc(:));
			end
			if ~isempty(obj.c_diff)
				zdisc_old = linspace(0,1,size(obj.c_diff,1));
				obj.c_diff = interp1(zdisc_old,obj.c_diff,zdisc(:));
			end
			if ~isempty(obj.f)
				zdisc_old = linspace(0,1,size(obj.f,1));
				obj.f = interp1(zdisc_old,obj.f,zdisc(:));
				obj.f = permute(interp1(zdisc_old,permute(obj.f,[2 1 3 4]),zdisc(:)),[2 1 3 4]);
			end
			obj.ndiscParsPriv = value; 
		end
		function value = get.ndiscPars(obj)
			value = obj.ndiscParsPriv;
		end
		
		function obj = set.l_disc(obj,value)
			obj.l = value;
		end
		function value = get.l_disc(obj)
			value = obj.l;
		end

		function obj = set.a_disc(obj,value)
			obj.a = value;
		end
		function value = get.a_disc(obj)
			value = obj.a;
		end

		function obj = set.b_disc(obj,value)
			obj.b = value;
		end
		function obj = set.b(obj,value)
			if isa(value,'function_handle')
				bDisc = misc.eval_pointwise(value,linspace(0,1,obj.ndiscPars));
			else
				bDisc = value;
			end
			obj.b = bDisc;
		end
		
								
		
		function value = get.b_disc(obj)
			value = obj.b;
		end

		function obj = set.c_disc(obj,value)
			obj.c = value;
		end
		function value = get.c_disc(obj)
			value = obj.c;
		end

		function obj = set.c_diff_disc(obj,value)
			obj.c_diff = value;
		end
		function value = get.c_diff_disc(obj)
			value = obj.c_diff;
		end

		function obj = set.l_diff_disc(obj,value)
			obj.l_diff = value;
		end
		function value = get.l_diff_disc(obj)
			value = obj.l_diff;
		end

		function obj = set.l_diff2_disc(obj,value)
			obj.l_diff2 = value;
		end
		function value = get.l_diff2_disc(obj)
			value = obj.l_diff2;
		end

		function obj = set.g_1_disc(obj,value)
			obj.g_1 = value;
		end
		function value = get.g_1_disc(obj)
			value = obj.g_1;
		end

		function obj = set.a_0_disc(obj,value)
			obj.a_0 = value;
		end
		function obj = set.a_1_disc(obj,value)
			obj.a_1 = value;
		end
		function obj = set.c_0_disc(obj,value)
			obj.c_0 = value;
		end
		function obj = set.c_1_disc(obj,value)
			obj.c_1 = value;
		end
		function value = get.a_0_disc(obj)
			value = obj.a_0;
		end
		function value = get.a_1_disc(obj)
			value = obj.a_1;
		end
		function value = get.c_0_disc(obj)
			value = obj.c_0;
		end
		function value = get.c_1_disc(obj)
			value = obj.c_1;
		end

		function obj = set.f_disc(obj,value)
			obj.f = value;
		end
		function value = get.f_disc(obj)
			value = obj.f;
		end


	   %% Constructor:
		function obj = ppide_sys(varargin)
			% obj = PPIDE_SYS(varargin)
			%   create new ppide_sys object.
			%   You can pass all class properties by 'name', value syntax
			tic;
			fprintf('  Creating ppide_sys...');
			import misc.*
			import numeric.*
			arglist = properties(obj);
% 			arglist = findAttrValue(obj,'Dependent',false);
			found=0;
			written={};
			storage=struct;
			if ~isempty(varargin)
				if mod(length(varargin),2) % uneven number
					error('When passing additional arguments, you must pass them pairwise!')
				end
				for index = 1:2:length(varargin) % loop through all passed arguments:
					for arg = 1:length(arglist)
						if strcmp(varargin{index},arglist{arg})
							storage.(arglist{arg}) = varargin{index+1};
							written=[written arglist{arg}]; %#ok<AGROW>
							found=1;
							break
						end 
					end % for arg
					% argument wasnt found in for-loop
					if ~found
						error([varargin{index} ' is not a valid property of class ppide_sys!']);
					end
					found=0; % reset found
				end % for index
			end % if isempty varargin
			if isfield(storage,'name')
				revStr = repmat(sprintf('\b'), 1, length('Creating ppide_sys...'));
				fprintf([revStr 'Creating ppide_sys: ' storage.name '...'])
			end
			
			if isfield(storage,'ndiscPars')
				zdisc = linspace(0,1,storage.ndiscPars);
				obj.ndiscPars = storage.ndiscPars;
			else
				zdisc = linspace(0,1,obj.ndiscPars);
			end
			ndisc=length(zdisc);
			if isfield(storage,'l')
				if isa(storage.l,'function_handle')
					if nargin(storage.l) == 1
						l = misc.eval_pointwise(storage.l,zdisc);
					elseif nargin(storage.l) == 2
						% n not yet known so preallocation is difficult
						for z_idx=1:ndisc
							% evaluate time dimension
% 							l(z_idx,:,:,:)= permute(misc.eval_pointwise(permute(@(z)storage.l,[4 2 3 1]),zdisc),[4 2 3 1]);
							l(z_idx,:,:,:)= permute((misc.eval_pointwise(@(z)storage.l(zdisc(z_idx),z),zdisc)),[2 3 1]); %#ok<AGROW>
						end
					else
						error('When passed as function, l must have 1 (space) or 2 (space and time) arguments.');
					end
				elseif size(storage.l,1) == size(storage.l,2) % constant matrix
					l = repmat(reshape(storage.l,[1 size(storage.l)]),ndisc,1,1);
				else % assume correct resolution passed, will be resampled later
					l = storage.l; 
				end
				storage.l = l; % pass to storage so it will be written to object
			else % no lambda is passed --> use default value
				error('l must always be passed, when creating a ppide_sys object!')
			end
			obj.n=size(l,2); % now n is already surely set.
			n = obj.n;
			
			discDiffList = {'l_t','l_diff','l_diff2','c_diff'};
			discList = {'l','a','a_0','a_1','a_z0','c','c_0','c_1','c_z0'};
			% those variabls that can be passed as anonymous function or in
			% discretized version
			for parname = discList
				if isfield(storage,parname{1})
					if isa(storage.(parname{1}),'function_handle') % l will never be function anymore
						% discretize a, a_0, c, c_1
						if nargin(storage.(parname{1})) == 1
							eval([parname{1} ' = misc.eval_pointwise(storage.' parname{1} ',zdisc);']);
						elseif nargin(storage.(parname{1})) == 2
							for z_idx=1:ndisc
								% evaluate time dimension very slow, perhaps a better
								% implementation possible...
								eval([parname{1} '(z_idx,:,:,:) = permute((misc.eval_pointwise(@(z)storage.' parname{1} '(zdisc(z_idx),z),zdisc)),[2 3 1]);']);
% 								l(z_idx,:,:,:)= permute((misc.eval_pointwise(@(z)storage.l(zdisc(z_idx),z),zdisc)),[2 3 1]);
							end
						else
							error(['When passed as function, ' parname{1} ' must have 1 (space) or 2 (space and time) arguments.']);
						end
					else % passed discretized or constant
						eval(['temp.' parname{1} ' = storage.' parname{1} ';']);
						if size(storage.(parname{1}),1) == size(storage.(parname{1}),2) %constant matrix
							% expand first dimension
							eval(['temp.' parname{1} ' = repmat(reshape(storage.' parname{1} ',[1 size(storage.' parname{1} ')]),ndisc,1,1);']);
						else
							if length(size(storage.(parname{1})))>3
								temp.(parname{1}) = permute(misc.translate_to_grid(permute(storage.(parname{1}),[1 4 2 3]),zdisc,'zdim',2),[1 3 4 2]);
							else
								temp.(parname{1}) = misc.translate_to_grid(storage.(parname{1}),zdisc,'zdim',1);
							end
						end
						eval([parname{1} ' = temp.' parname{1} ';']);
					end
					eval(['storage.' parname{1} ' = ' parname{1} ';']); % pass to storage so it will be written to object
				else % nothing passed --> use default value
					eval([parname{1} ' = zeros(ndisc,n,n);']);
					eval(['storage.' parname{1} ' = ' parname{1} ';']); % pass to storage so it will be written to object
				end
			end
			for parname = discDiffList
				if isfield(storage,parname{1})
					if isa(storage.(parname{1}),'function_handle')
						if nargin(storage.(parname{1})) == 1
							eval([parname{1} ' = misc.eval_pointwise(storage.' parname{1} ',zdisc);']);
						elseif nargin(storage.(parname{1})) == 2
							for z_idx=1:ndisc
								% evaluate time dimension
								eval([parname{1} '(z_idx,:,:,:) = permute((misc.eval_pointwise(@(z)storage.' parname{1} '(zdisc(z_idx),z),zdisc)),[2 3 1]);']);
% 								l(z_idx,:,:,:)= permute((misc.eval_pointwise(@(z)storage.l(zdisc(z_idx),z),zdisc)),[2 3 1]);
							end
						else
							error(['When passed as function, ' parname{1} ' must have 1 (space) or 2 (space and time) arguments.']);
						end
					else % discretized or constant
						eval(['temp.' parname{1} ' = storage.' parname{1} ';']);
						if size(storage.(parname{1}),1) == size(storage.(parname{1}),2) %constant matrix
							% expand first dimension
							eval(['temp.' parname{1} ' = repmat(reshape(storage.' parname{1} ',[1 size(storage.' parname{1} ')]),ndisc,1,1);']);
						else
							if length(size(storage.(parname{1})))>3
								temp.(parname{1}) = permute(misc.translate_to_grid(permute(storage.(parname{1}),[1 4 2 3]),zdisc,'zdim',2),[1 3 4 2]);
							else
								temp.(parname{1}) = misc.translate_to_grid(storage.(parname{1}),zdisc,'zdim',1);
							end
						end
						eval([parname{1} ' = temp.' parname{1} ';']);
					end
					eval(['storage.' parname{1} ' = ' parname{1} ';']); % pass to storage so it will be written to object
				else % nothin passed --> write empty to be able to differentiate numerically
					eval([parname{1} ' = [];']);
					eval(['storage.' parname{1} ' = ' parname{1} ';']); % pass to storage so it will be written to object
				end
			end
			if isfield(storage,'f')
				if isa(storage.f,'function_handle')
					if nargin(storage.f)==2 % z,zeta
						% quantity.function kann nicht mit funktionen umgehen, die arrays oder
						% matrizen zurueck liefern! Deswegen der Umweg �ber sym!
						f = quantity.Symbolic(sym(storage.f),...
							'grid',{zdisc,zdisc},...
							'variable',{'z','zeta'});
					elseif nargin(storage.f)==3 % z,zeta,t
						f = quantity.Symbolic(sym(storage.f),...
							'grid',{zdisc,zdisc,zdisc},...
							'variable',{'z','zeta','t'});
					else
						error('When passed as function, f must have 2 (z,zeta) or 3 (z,zeta,t) arguments!')
					end
				else
					if size(storage.f,5)>1
						if size(storage.f,1)~= ndisc
	% 						resample:
							fOld = quantity.Discrete(permute(storage.f,[1 2 5 3 4]),...
								'grid',{linspace(0,1,size(storage.f,1)),...
										linspace(0,1,size(storage.f,2)),...
										linspace(0,1,size(storage.f,5))},...
								'gridName',{'z','zeta','t'});
							f = changeGrid(fOld,...
								{linspace(0,1,ndiscPars),...
								 linspace(0,1,ndiscPars),...
								 linspace(0,1,ndiscPars)},...
								 {'z','zeta','t'});
							warning('f was passed with wrong resolution. Resampled to fit to ndiscPars.');
						else
							f = quantity.Discrete(permute(storage.f,[1 2 5 3 4]),...
								'grid',{linspace(0,1,size(storage.f,1)),...
										linspace(0,1,size(storage.f,2)),...
										linspace(0,1,size(storage.f,5))},...
								'gridName',{'z','zeta','t'});
						end
					else
						if size(storage.f,1)~= ndisc
	% 						resample:
							fOld = quantity.Discrete(storage.f,...
								'grid',{linspace(0,1,size(storage.f,1)),...
										linspace(0,1,size(storage.f,2))},...
								'gridName',{'z','zeta','t'});
							f = changeGrid(fOld,...
								{linspace(0,1,ndiscPars),...
								 linspace(0,1,ndiscPars)},...
								 {'z','zeta'});
							warning('f was passed with wrong resolution. Resampled to fit to ndiscPars.');
						else
							f = quantity.Discrete(storage.f,...
								'grid',{linspace(0,1,size(storage.f,1)),...
										linspace(0,1,size(storage.f,2))},...
								'gridName',{'z','zeta'});
						end
					end
				end
				storage.f = f; % pass to storage so it will be written to object
			else % no f is passed --> use default value
				f = quantity.Discrete(zeros(ndisc,ndisc,n,n),...
					'grid',{linspace(0,1,ndisc),linspace(0,1,ndisc)},...
					'gridName',{'z','zeta'});
				storage.f = f; % pass to storage so it will be written to object
			end
			
			if isfield(storage,'z0')
				if isscalar(storage.z0)
					storage.z0 = repmat(storage.z0,1,n);
				else
					if length(storage.z0)~= n
						error('z0 must be a scalar or contain n elements!')
					end
				end
			end

			nd=0; % default number of disturbances
			p=0; % default number of inputs
			m=0; % default number of outputs
			% check if dimensions of inputs, dist inputs and outputs are correct
			if ~isempty(written)
				written_names = fieldnames(storage);
				% check all passed arguments:
				for i=1:length(written_names)
					switch written_names{i}
						case 'b' % g_1 is written
							if isa(storage.b,'function_handle')
								storage.b = misc.eval_pointwise(storage.b,zdisc);
							end
							b_0 = sd(storage.b(1,:,:));
							p = size(b_0,2);
						case 'b_1'
							p = size(storage.b_1,2);
						case 'b_2'
							p = size(storage.b_2,2);
						case 'g_1' % g_1 is written
							if isa(storage.g_1,'function_handle')
								storage.g_1 = misc.eval_pointwise(storage.g_1,zdisc);
							end
							g_1_0 = sd(storage.g_1(1,:,:));
							nd = size(g_1_0,2);
						case 'g_2'
							nd = size(storage.g_2,2);
						case 'g_3'
							nd = size(storage.g_3,2);
						case 'g_4'
							nd = size(storage.g_4,2);
						case 'c_op'
							m = storage.c_op.m;
							if storage.c_op.n ~= obj.n
								error('number of states in output operator not consistent to system!')
							end						
						case 'cm_op'
							if storage.cm_op.n ~= obj.n
								error('number of states in measurement output operator not consistent to system!')
							end							
					end
				end
			end
			% Jetzt alle Werte auf Default-Werte setzen:
			obj.b = zeros(obj.ndiscPars,obj.n,p);
			obj.b_1 = zeros(obj.n,p);
			obj.b_2 = zeros(obj.n,p);
			
			% Jetzt alle Werte auf Default-Werte setzen:
			obj.g_1 = zeros(obj.ndiscPars,obj.n,nd);
			obj.g_2 = zeros(obj.n,nd);
			obj.g_3 = zeros(obj.n,nd);
			obj.g_4 = zeros(m,nd);
			% und die zwischengespeicherten Wert wiederherstellen:]
			fn = fieldnames(storage);
			for fn_idx = 1:length(fn)
				obj.(fn{fn_idx})=storage.(fn{fn_idx});
			end
			
			% �berpr�fen, ob alle Matrizen die richtige Dimension haben:
			% nur verletzt, falls Matrizen mit falschen Dimensionen
			% �bergeben wurden.
			b_opList = {'b_op1','b_op2'};
			for bOp_ind = 1:length(b_opList)
				if isempty(obj.(b_opList{bOp_ind}))
					% set default boundary operator
					obj.(b_opList{bOp_ind}) = ...
						dps.parabolic.system.boundary_operator('z_b',bOp_ind-1,...
						'b_n',eye(obj.n),'b_d',zeros(n,n));
				else
					if ~isa(obj.(b_opList{bOp_ind}).b_d,'function_handle')
						if size(obj.(b_opList{bOp_ind}).b_d,3)==1
							% time constant
							if any(size(obj.(b_opList{bOp_ind}).b_d)-[obj.n obj.n])
								error(['size(' b_opList{bOp_ind} ')~= ' num2str([obj.n n]) '!']);
							end
						else % time varying
							if any(size(obj.(b_opList{bOp_ind}).b_d)-[obj.n obj.n size(obj.(b_opList{bOp_ind}).b_d,3)])
								error(['size(' b_opList{bOp_ind} ')~= ' num2str([obj.n n]) '!']);
							end
						end
					else
						if any(size(obj.(b_opList{bOp_ind}).b_d(0))-[obj.n obj.n])
							error(['size(' b_opList{bOp_ind} '(0))~= ' num2str([obj.n n]) '!']);
						end
					end
				end
			end	
						
			b_list = {'b_1','b_2'};
			for b_ind = 1:length(b_list)
				if any(size(obj.(b_list{b_ind}))-[obj.n p])
					error(['size(' b_list{b_ind} ')~= ' num2str([obj.n p]) '!']);
				end
			end
			if any(size(sd(obj.b(1,:,:)))-[obj.n p])
				error(['size(b)~= ' num2str([obj.n p]) '!']);
			end
			g_list = {'g_2','g_3'};
			for g_ind = 1:length(g_list)
				if any(size(obj.(g_list{g_ind}))-[obj.n nd])
					error(['size(' g_list{g_ind} ')~= ' num2str([obj.n nd]) '!']);
				end
			end
			if any(size(sd(obj.g_1(1,:,:)))-[obj.n nd])
				error(['size(g_1)~= ' num2str([obj.n nd]) '!']);
			end
			if any(size(obj.g_4)-[m nd])
				error(['size(g_4)~= ' num2str([m nd]) '!']);
			end
			if isempty(obj.c_op)
				obj.c_op = dps.output_operator('c_0',ones(0,obj.n));
			end
			if isempty(obj.cm_op)
				obj.cm_op = dps.output_operator('c_0',ones(0,obj.n));
			end
			
			if isempty(obj.l_t)
				l_t = diff_num(linspace(0,1,size(l,4)),l,4); 
			end
			obj.l_t = l_t;
			if isempty(obj.l_diff)
				l_diff = diff_num(zdisc,l); 
			else
				l_diffComp = diff_num(zdisc,l); 
				if any(abs(l_diff(:)-l_diffComp(:))>1e-1) %#ok<NODEF>
					warning(['It seems like a wrong l_diff was passed! Dev=' num2str(max(abs(l_diff(:)-l_diffComp(:))))]);
					figure('Name','Numerical and passed lambda_diff')
					hold on
					for i=1:n
						plot(zdisc,l_diff(:,i,i),'DisplayName',['Num.: ' num2str(i)])
						plot(zdisc,l_diffComp(:,i,i),'DisplayName',['Pas.: ' num2str(i)])
					end
					legend('-dynamicLegend');
				end				
			end
			obj.l_diff = l_diff;
			
			if isempty(obj.c_diff)
				c_diff = diff_num(zdisc,c); 
			end
			obj.c_diff = c_diff;
			
			if isempty(obj.l_diff2)
				l_diff2 = diff_num(zdisc,l_diff); 
			end
			obj.l_diff2 = l_diff2;
			
			
			if isempty(obj.name)
				obj.name = 'defaultPpide';
			end
			
			if isempty(obj.ctrl_pars)
				for i=1:obj.n
					for j=1:obj.n
						g_strich_temp{i,j} = @(eta) 0+0*eta;  %#ok<AGROW>
					end
				end
				obj.ctrl_pars = dps.parabolic.control.ppde_controller_pars(...
					'mu',zeros(obj.n,obj.n),...
					'mu_o',zeros(obj.n,obj.n),...
					'g_strich',g_strich_temp);					
			end
			time=toc;
			fprintf('  Finished! (%0.2fs) \n', time);
		end % constructor
		
		function ew = eigTarget(obj, mu)
		% Calculate eigenvalues of backstepping target system:
		% Attention: Assumes that matrices are diagonal or non-diagonal elements are
		% compensated by control law or observer gain! No inclusion of non-diagonal elements
		% into the target system is implemented!
			import misc.*
			zdisc = linspace(0,1,obj.ndiscPars);
			objT = obj;
			% decoupling at right boundary. Left boundary is assumed to be
			% without coupling anyway, but can be transformed to neumann bc.
			% if the ROBIN matrix is time-dependent, it is always eliminated in the target
			% system!
			if isa(obj.b_op1.b_d,'function_handle') && ~obj.ctrl_pars.makeTargetNeumann
				error('When the left BC is time-dependent, ctrl_pars.makeTargetNeumann must be set to 1. If the BC shall be constant, it may not be a function!')
			end
			Bdl_til = zeros(obj.n,obj.n);
			if isa(obj.b_op1.b_d,'function_handle') || ~ismatrix(obj.b_op1.b_d) % discretized
				for i=1:obj.n
					if obj.b_op1.b_n(i,i) == 0
						Bdl_til(i,i) = 1; % Dirichlet!
					end
				end
			else
				for i=1:obj.n
					if obj.b_op1.b_n(i,i)~= 0 % only if not Dirichlet!
						Bdl_til(i,i) = ~obj.ctrl_pars.makeTargetNeumann * obj.b_op1.b_d(i,i);
					elseif isdiag(obj.b_op1.b_d) % Dirichlet case probably only implemented for diagonal case
						Bdl_til(i,i) = 1; % Dirichlet!
					else
						error('check boundary conditions in b_op1!')
					end
				end
			end
			Bnl_til = obj.b_op1.b_n;
			objT.b_op1 = dps.parabolic.system.boundary_operator('z_b',0,'b_d',Bdl_til,'b_n',Bnl_til);
			
			Bdr_til = zeros(obj.n,obj.n);
			if isa(obj.b_op2.b_d,'function_handle') || ~ismatrix(obj.b_op2.b_d)
				for i=1:obj.n
					if obj.b_op2.b_n(i,i) == 0
						Bdr_til(i,i) = 1; % Dirichlet!
					end
				end
			else
				for i=1:obj.n
					if obj.b_op2.b_n(i,i) ~= 0 % only if not Dirichlet
						Bdr_til(i,i) = ~obj.ctrl_pars.makeTargetNeumann * obj.b_op2.b_d(i,i);
					elseif isdiag(obj.b_op2.b_d) % Dirichlet case only implemented for diagonal case
						Bdr_til(i,i) = 1; % Dirichlet
					else
						error('check boundary conditions in b_op2!')
					end
				end
			end
			Bnr_til = diag(diag(obj.b_op2.b_n));
			objT.b_op2 = dps.parabolic.system.boundary_operator('z_b',1,'b_d',Bdr_til,'b_n',Bnr_til);
			
			if isa(mu,'function_handle') % mu is function handle
				objT.a = -eval_pointwise(mu,zdisc);
			elseif size(mu,1)==obj.ndiscPars % mu is discretized in ndiscPars (should not be the case)
				objT.a = -mu;
			elseif size(mu,1) == length(obj.ctrl_pars.zdiscCtrl) % mu discretized
				if length(mu)>3
					tDiscnew = translate_to_grid(obj.ctrl_pars.tDiscKernel,linspace(min(obj.ctrl_pars.tDiscKernel),max(obj.ctrl_pars.tDiscKernel),obj.ndiscTime));
					muInterpolant = numeric.interpolant({obj.ctrl_pars.zdiscCtrl,1:obj.n,1:obj.n,obj.ctrl_pars.tDiscKernel}, mu);
					objT.a = -muInterpolant.evaluate(zdisc,1:obj.n,1:obj.n,tDiscnew);
				else
					muInterpolant = numeric.interpolant({obj.ctrl_pars.zdiscCtrl,1:obj.n,1:obj.n}, mu);
					objT.a = -muInterpolant.evaluate(zdisc,1:obj.n,1:obj.n);
				end
			else % mu is constant
				mu_temp = @(z) mu + 0*z;
				objT.a = -eval_pointwise(mu_temp,zdisc);
			end
			
			% The local coupling and the integral coupling are set to zero,
			% since a_0 is strictly lower triangular, hence it does not 
			% influence the eigenvalues of the targetsystem.
			objT.a_0 = 0 * obj.a_0;
			objT.f = 0 * obj.f;
			
			objT.name = ['Target (avg(mu)=' num2str(mean(-objT.a(:))) ')'];
			appr = dps.parabolic.system.ppide_approximation(objT);
			ew = [];
			for tIdx = 1:size(appr.A,3)
				ew = [ew;eig(appr.A(:,:,tIdx))]; %#ok<AGROW>
			end
		end
		
		function x = sys2Sim(~,xvec)
		% sys2Sim transform array from system representation to simulation
		% represenatation
		%
		%   x = sys2Sim(xVec) collapses the last two dimensions of the array xvec
		%   to one dimension.
		%   Example: x in system representation is x(t,z,i), where i is the
		%   number of the state. In Simulation representation, the
		%   different states are ordered after each other. xVec(t,zi)
			sizXvec = size(xvec);
			if numel(sizXvec)<3 % if n=1 sizXvec(1:end-2) does not work.
				x = xvec;
			else
				x = reshape(xvec,[sizXvec(1:end-2) prod(sizXvec(end-1:end))]);
			end
		end
		
		function xVec = sim2Sys(obj,x,n)
			if nargin<3
				nX = obj.n;
			else
				nX = n;
			end
			sizX = size(x);
			ndiscTot = sizX(end);
			ndiscX = ndiscTot/nX;
			if mod(ndiscX,1)~= 0
				error(['x is not fitting to the system, as size(x,end)=' num2str(sizX(end))]);
			else
				xVec = reshape(x,[sizX(1:end-1) ndiscX nX]);
			end
		end
		
		function plotDiffusion(obj)
			figure('Name','Diffusion Coefficients')
			hold on
			zdiscPars = linspace(0,1,obj.ndiscPars);
			tDisc = linspace(0,1,obj.ndiscTime);
			if size(obj.l,4)==1
				for i=1:obj.n
					plot(zdiscPars,obj.l(:,i,i),'DisplayName',['\lambda_' num2str(i)]);
				end
			else
				for i=1:obj.n
					surf(zdiscPars,tDisc,squeeze(obj.l(:,i,i,:)).','DisplayName',['\lambda_' num2str(i)]);
				end
			end
			legend('-dynamicLegend')
		end
		
		function plotLambdaSigns(obj)
			signMat = zeros(obj.n,obj.n);
			mymap = [1 0 0;
				   0 0 1;
				   0 1 0];
			for i=1:obj.n
				for j=1:obj.n
					if obj.l(1,i,i,1) > obj.l(1,j,j,1)
						signMat(i,j) = 1;
					elseif obj.l(1,i,i,1) == obj.l(1,j,j,1)
						signMat(i,j) = 0;
					else
						signMat(i,j) = -1;
					end
				end
			end
			figure('Name','Signs of lambda')
			h = bar3(signMat);
			colormap(mymap);
			for i=1:length(h) % 
				zdata = h(i).ZData;
				zdataTemp = zdata;
				for j=1:obj.n	
					if obj.l(1,j,j,1) > obj.l(1,i,i,1)
						zdataTemp((j-1)*6+1:j*6,:) = 1;
						txt = '\lambda_i < \lambda_j';
					elseif obj.l(1,i,i,1) == obj.l(1,j,j,1)
% 						zdataTemp = nan(obj.n*6,obj.n);
						zdataTemp((j-1)*6+1:j*6,:) = 0;
						txt = '\lambda_i = \lambda_j';
					else
% 						zdataTemp = nan(obj.n*6,obj.n);
						zdataTemp((j-1)*6+1:j*6,:) = -1;
						txt = '\lambda_i > \lambda_j';
					end
					set(h(i),'CData',zdataTemp);
% 					zdata = h(i).ZData;
% 					h(i).ZData = zdata+0.1;
% 					h(i).CData = zdata-0.1;
					text(j,i,1.1,txt,'HorizontalAlignment','center','color','white','interpreter','tex',...
						'FontWeight','bold');
				end
				
			end
			view(2)
			xlabel('j')
			ylabel('i')
			
		end
		
		function plotGrowthAssumption(obj,gamma)
			figure('Name','$\sqrt{lambda_i}-\gamma\sqrt{\lambda_j}$')
			hold on
			zdiscPars = linspace(0,1,obj.ndiscPars);
			num=1;
			if size(obj.l,4)==1
				for i=1:obj.n
					for j=1:obj.n
						subplot(obj.n,obj.n,num);
						title(['i=' num2str(i) 'j=' num2str(j)]);
						surf(zdiscPars,zdiscPars,sqrt(squeeze(obj.l(:,i,i)))-gamma*sqrt(squeeze(obj.l(:,j,j)).'),'DisplayName',['i=' num2str(i) ', j=' num2str(j)])
						num=num+1;
						legend('-dynamicLegend')
						xlabel('$z$')
						ylabel('$\zeta$')
					end
				end
			else
				error('Not yet implemented for time dependent diffusion')
			end
		end
	end
	
end