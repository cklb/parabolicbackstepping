function sim = simulate(ppide_appr, varargin)
%% simulate performs a simulation of a ppide_approximation object
%   sim = simulate(ppide_apprx) peforms a simulation of the system
%   ppide_apprx and stores the result in the struct sim. The input u, the
%   disturbance d, the inital states x0 and the simulation_parameters can
%   be specified as name-value-pair inputs.
%
%	-	created on 06.09.2018 by Jakob Gabriel based on
%			dps.parabolic.control.backstepping.ppide_simulation.simulate
%			by Simon Kerschbaum
	
	
	simstart_tic = tic;
	fprintf('Starting Simulation...\n');
	
	% read mandatory input
	sim.ppide_appr = ppide_appr;
	
	% Init system matrices:
	sim.sys.A = ppide_appr.A;
	sim.sys.G = ppide_appr.G;
	sim.sys.B = ppide_appr.Bl+ppide_appr.Br+ppide_appr.Bv;
	
	% read varargin input
	p = inputParser();
	default.u = @(t) zeros(size(sim.sys.B, 2), 1)*t;
	default.d = @(t) zeros(size(sim.sys.G, 2), 1)*t;
	default.x0 = zeros(size(sim.sys.A, 1), 1);
	default.simulation_parameters = misc.simulation_parameters();
	p.addParameter('u',default.u);
	p.addParameter('d',default.d);
	p.addParameter('x0',default.x0);
	p.addParameter('simulation_parameters',default.simulation_parameters);
	p.parse(varargin{:});
	sim.u = p.Results.u;
	sim.d = p.Results.d;
	sim.x0 = p.Results.x0;
	sim.parameter = p.Results.simulation_parameters;
	
	% Transform initial value to correct representation
	if isa(sim.x0,'function_handle')
		x0_disc_temp = eval_pointwise(sim.x0, ppide_appr.zdisc);
		sim.disc.x0 = x0_disc_temp(:);
		if length(sim.disc.x0)~=size(sim.sys.A,1)
			error('Size of x0 did not fit!')
		end
	else
		sim.disc.x0 = sim.x0(:);
		if size(sim.disc.x0,1)~= size(sim.sys.A,1) 
			error(['size(x0_disc,1)=' num2str(size(sim.disc.x0,1)) ' ~= size(A,1)=' num2str(size(sim.sys.A,1))])
		elseif size(sim.disc.x0,2)~= 1
			error('size(x0) must be [size(A,1),1], or x0 must be an anonymous function.');
		end
	end
	
	% set ODE
	f = @(t,x) sim.sys.A * x + sim.sys.B * sim.u(t) + sim.sys.G * sim.d(t);

	%% solver options
	odeopts = odeset('relTol',sim.parameter.frel,'absTol',sim.parameter.fabs,'MaxStep',sim.parameter.tsmax);
	
	%% run simulation
	[sim.disc.t, sim.disc.x] = ode15s(f, [sim.parameter.tStart sim.parameter.tend], sim.disc.x0, odeopts);
	if any(diff(sim.disc.t))<= 0
		warning('Solver returned non-increasing t. Probably the system is unstable!')
	end

	time=toc(simstart_tic);
	fprintf('Finished! (%0.2fs) \n',time);
end