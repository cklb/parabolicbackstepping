classdef ppide_approximation
	%PPIDE_APPROXIMATION Vectors and matrices for an FEM-approximated ppide
	%model
%     stores a state space approximation 
%       \hat{x}' = A \hat{x} + Br ur + Bl ul + Bv uv + G d
%            y   = C x + Gy d
%     for a ppide-system object.
%                    x_t(z,t) = l(z,t) x_zz(z,t)+ c(z,t) x_z(z,t) + a(z,t) x(z,t)
%                              + b(z) u_v(t) + g_v(z) d(t)
%                              + a_0(z) x(0,t) + a_1(z) x(1,t)
%                              + c_0(z) x_z(0,t) + c_1(z) x_z(1,t)
%                              + a_z0(z) x(z0,t) + c_z0(z) x_z(z0,t)
%                              + int_0^z f(z,zeta)x(zeta,t)dzeta
%              b_op1[x(t)](0) = b_l u_l(t) + g_l d(t)
%              b_op2[x(t)](1) = b_r u_r(t) + g_r d(t)
%                        y(t) = c_op x + g_y d(t),
%     where x und u are vectors of appropirate dimension.
%     The resulting state vector for the system matrix A contains
%     x = [x1;x2;x3;...] (column vector), with each xi beeing a column
%     vector containing the values at the discretisation points zdisc.

% created on 20.03.2018 by Simon Kerschbaum
% modified on 27.09.2018 by SK:
%   - added time varying diffusion, reaction, convection terms

	properties
		A     % (n*ndisc x n*ndisc x numT)  System matrix: has a third dimension if A is time-variant
		Br    % (n*ndisc x pr x numT) right boundary input matrix: has a third dimension if lambda is time-variant
		Bl    % (n*ndisc x pl x numT) left boundary input matrix: has a third dimension if lambda is time-variant
		Bv    % (n*ndisc x pv x numT) distributed boundary input matrix: has a third dimension if lambda is time-variant
		G     % (n*ndisc x nd x numT) disturbance input matrix: has a third dimension if lambda is time-variant
		C     % (m x n*ndisc)         output matrix
		Gy    % (m x nd)        disturbance feedthrough matrix
		zdisc %                 spatial discretization
	end
	
	methods
		% constructor
		function obj = ppide_approximation(ppide)
			tic
			fprintf(['  Computing ppide_approximation for ' ppide.name '...'])
			obj = obj.approximate_ppide(ppide);
			time=toc;
			fprintf('  Finished! (%0.2fs)\n',time);
		end
		
		% simulation
		sim = simulate(ppide_appr, varargin);
		
		% helper for constructor
		function obj = approximate_ppide(obj,ppide_sys)
			%APPROXIMATE_PPIDE get state space approximation of a PPIDE system.
			%
			%  [A,Br] = approximate_ppide(ppde_sys,zdisc)
			%     computes a state space approximation 
			%       \hat{x}' = A \hat{x} + Br ur
			%     for the ppide-system
			%                    x_t(z,t) = l(z,t) x_zz(z,t)+ c(z,t) x_z(z,t) + a(z,t) x(z,t)
			%                               + b(z) u_v(t) + g_v(z) d(t)
			%                               + a_0(z,t) x(0,t) + c_1(z,t) x_z(1,t)
			%                               + a_z0(z) x(z0,t) + c_z0(z) x_z(z0,t)
			%                               + int_0^z f(z,zeta)x(zeta,t)dzeta
			%            b_op1[x(t)](0,t) = b_l u_l(t) + g_l d(t)
			%            b_op2[x(t)](1,t) = b_r u_r(t) + g_r d(t)
			%                        y(t) = c_op x + g_y d(t),
			%     where x und u are vectors of appropirate dimension.
			%     The resulting state vector for the system matrix A contains
			%     x = [x1;x2;x3;...] (column vector), with each xi beeing a column
			%     vector containing the values at the discretisation points zdisc.
			%    
			%     b_opi.b_n must be a diagonal matrix!

			import misc.*
% 			import numeric.*
			import external.fem.*
			% TODO: ALL PARAMETERS TIME-DEPENDENT, NOT ONLY DCR
			
			zdisc = linspace(0,1,ppide_sys.ndisc); %#ok<*PROPLC>
			gradOp = numeric.get_grad_op(zdisc);
			n = size(ppide_sys.l_disc,2);
			ndisc= length(zdisc);
			ndiscTime = ppide_sys.ndiscTime;
			tDisc = linspace(0,1,ndiscTime);
						
			% resample space dimension
			pv = size(ppide_sys.b_disc,3);
			pars.lambda_disc = misc.translate_to_grid(ppide_sys.l_disc,zdisc);
			pars.a_0_disc = misc.translate_to_grid(ppide_sys.a_0_disc,zdisc);
			pars.a_1_disc = misc.translate_to_grid(ppide_sys.a_1_disc,zdisc);
			pars.a_z0_disc = misc.translate_to_grid(ppide_sys.a_z0,zdisc);
			pars.c_1_disc = misc.translate_to_grid(ppide_sys.c_1_disc,zdisc);
			pars.c_0_disc = misc.translate_to_grid(ppide_sys.c_0_disc,zdisc);
			pars.c_z0_disc = misc.translate_to_grid(ppide_sys.c_z0,zdisc);
			if nargin(ppide_sys.f) == 2
				% z zeta i j 
				pars.F_disc = ppide_sys.f.on({zdisc,zdisc});
			elseif nargin(ppide_sys.f)==3
				% z zeta i j t
				pars.F_disc = permute(ppide_sys.f.on({zdisc,zdisc,tDisc},{'z','zeta','t'}),[1 2 4 5 3]);
			end
			pars.a_disc = misc.translate_to_grid(ppide_sys.a_disc,zdisc);
			pars.c_disc = misc.translate_to_grid(ppide_sys.c_disc,zdisc);
			gv_disc = misc.translate_to_grid(ppide_sys.g_1_disc,zdisc);
			bv_disc = misc.translate_to_grid(ppide_sys.b_disc,zdisc);
			
			Bdl =  ppide_sys.b_op1.b_d;
			Bdr = ppide_sys.b_op2.b_d;
			

			% resample time-dimension
			varlist = {'lambda_disc','a_disc','c_disc','a_0_disc','c_0_disc','a_z0_disc',...
					'c_z0_disc', 'c_1_disc','a_1_disc'};
			for it=1:length(varlist)
				if size(pars.(varlist{it}),4)>1
					pars.(varlist{it}) = permute(misc.translate_to_grid(permute(pars.(varlist{it}),[4 1 2 3]),tDisc),[2 3 4 1]);
				end
			end
			
			% all have the same length, but may also be one.
			% TODO: LENGTH 1 NOT CORRECT!
			numtA = size(pars.a_disc,4);
			numtL = size(pars.lambda_disc,4);
			numtC = size(pars.c_disc,4);
			numtA0 = size(pars.a_0_disc,4);
			numtAZ0 = size(pars.a_z0_disc,4);
			numtC0 = size(pars.c_0_disc,4);
			numtCZ0 = size(pars.c_z0_disc,4);
			numtF = size(pars.F_disc,5);

			% evaluate time-dependent BC
			if isa(Bdl,'function_handle')
				BdlDisc = permute(misc.eval_pointwise(Bdl,tDisc),[2 3 1]);
			else
				BdlDisc = Bdl;
			end
			if isa(Bdr,'function_handle')
				BdrDisc = permute(misc.eval_pointwise(Bdr,tDisc),[2 3 1]);
			else
				BdrDisc = Bdr;
			end
			% make Boudary matrices have the same dimensions
			if size(BdlDisc,3)>1 
				if size(BdrDisc,3)==1
					BdrDisc = repmat(BdrDisc,1,1,size(BdlDisc,3));
				end
			end
			if size(BdrDisc,3)>1 
				if size(BdlDisc,3)==1
					BdlDisc = repmat(BdlDisc,1,1,size(BdrDisc,3));
				end
			end
			% may be 1 or numT but all the same!
			numTBdl = size(BdlDisc,3);
			numTBdr = size(BdrDisc,3);
			numTBi = max([numtL;numTBdl;numTBdr]);
						
			Bnl =  ppide_sys.b_op1.b_n;
			if numTBdl>1
				Bdl_od = zeros(n,n,ndiscTime);
				for tIdx = 1:ndiscTime
					Bdl_od(:,:,tIdx) = BdlDisc(:,:,tIdx) - diag(diag(BdlDisc(:,:,tIdx))); % If there is a coupling in the boundary matrix, it is stored for later use.	
				end
			else
				Bdl_od = BdlDisc - diag(diag(BdlDisc)); % If there is a coupling in the boundary matrix, it is stored for later use.
			end
			% attention: coupling via boundary matrices has not been tested intensively!
			
			Bnr = ppide_sys.b_op2.b_n;
			if numTBdr>1
				Bdr_od = zeros(n,n,ndiscTime);
				for tIdx = 1:ndiscTime
					Bdr_od(:,:,tIdx) = BdrDisc(:,:,tIdx) - diag(diag(BdrDisc(:,:,tIdx))); % If there is a coupling in the boundary matrix, it is stored for later use.	
				end
			else
				Bdr_od = BdrDisc - diag(diag(BdrDisc)); % If there is a coupling in the boundary matrix, it is stored for later use.
			end
			
			% if there is a dirichlet BC, norm the matrix entry to 1.
			for it=1:n
				if Bnl(it,it) == 0
					if any(diff(BdlDisc(it,it,:)))
						error('For a dirichlet-BC, the respective bounday matrix entry must be constant.')
					end
					BdlDisc(it,it,:) = 1;
					ppide_sys.b_1(it,:) = ppide_sys.b_1(it,:)/BdlDisc(it,it,1);
				end
				if Bnr(it,it) == 0
					if any(diff(BdrDisc(it,it,:)))
						error('For a dirichlet-BC, the respective bounday matrix entry must be constant.')
					end
					% normalize both input and dirichlet matrix entry for further usage
					ppide_sys.b_2(it,:) = ppide_sys.b_2(it,:)/BdrDisc(it,it,1);
					BdrDisc(it,it,:) = 1;
				end
			end
				

			Bdl_kop = zeros(n*ndisc,n*ndisc,numTBdl); % preallocate
			Bdr_kop = zeros(n*ndisc,n*ndisc,numTBdr); % preallocate
			B_q0(1:n*ndisc,1:n*ndisc,numTBi)=0; % preallocate
			B_q1(1:n*ndisc,1:n*ndisc,numTBi)=0; % preallocate
			A_kop(1:n*ndisc,1:n*ndisc,1:numtA)=0; %preallocate
			a_0_disc_sys = zeros(n*ndisc, n*ndisc, numtA0); 
			a_z0_disc_sys = zeros(n*ndisc, n*ndisc, numtAZ0); 
			a_1_disc_sys = zeros(n*ndisc, n*ndisc); 
			c_0_disc_sys = zeros(n*ndisc, n*ndisc, numtC0); 
			c_z0_disc_sys = zeros(n*ndisc, n*ndisc, numtCZ0);
			c_1_disc_sys = zeros(n*ndisc, n*ndisc); 
			F_disc_sys = zeros(n*ndisc, n*ndisc, numtF);
			
			% for distributed input:
			Bv (1:n*ndisc,1:pv)=0; %preallocate
			% in System representation
			for it=1:n
				Bv((it-1)*ndisc+1:it*ndisc,:) = bv_disc(:,it,:);
			end
			obj.Bv = Bv;  % input matrix for distributed inputs
			
			
			% preallocations for input matrices
			p1 = size(ppide_sys.b_1,2); % left boundary inputs
			p2 = size(ppide_sys.b_2,2); % right boundary inputs
% 			pi = size(ppide_sys.b_i,2); % indomain pointwise inputs, currently only implemented
% 			via boundary operators, see later
			Bl(1:n*ndisc,1:p1,1:numTBi) = 0; % preallocate
			Br(1:n*ndisc,1:p2,1:numTBi) = 0; % preallocate
% 			Bi(1:n*ndisc,1:p2) = 0; % preallocate
			
            % disturbance inputs analog to inputs, but there is only one disturbance d.
            % (vector)
			nd = size(gv_disc,3);
			Gv (1:n*ndisc,1:nd)=0; %preallocate
			for it=1:n
				Gv((it-1)*ndisc+1:it*ndisc,:) = gv_disc(:,it,:);
			end
			Gl(1:n*ndisc,1:nd,1:numTBi) = 0; % preallocate
			Gr(1:n*ndisc,1:nd,1:numTBi) = 0; % preallocate
			
			%%%
			% compute fem-approximation for each state.
			%%%
			for it = 1:n
				lambda = permute(pars.lambda_disc(:, it, it, :),[1, 4, 2, 3]); % permute faster as squeeze
				c = permute(pars.c_disc(:, it, it, :),[1, 4, 2, 3]);
				if (Bnl(it, it) == 0) && (BdlDisc(it, it, 1) ~= 0) % left dirichlet
					q1 = repmat(1e5,numTBi,1); % in case there is also a time dependent robin BC
					% the value of q1 must be a vector to create time dependent matrices
					b1 = 1e5;
				elseif Bnl(it, it) ~= 0 % left Robin/Neumann
					q1 = reshape(BdlDisc(it,it,:)/Bnl(it, it),numTBdl,1,1); % vector of time
					% "virtual" input coefficient 1 to calculate coupling
					b1 = 1/Bnl(it, it);
				else  % no BC, just for test purposes
					q1 = 0;
					b1 = 0; % should behave same as Neumann? Isnt well-posed anyway!
				end

				if ppide_sys.b_op2.z_b == 1
					if Bnr(it, it) == 0 % right dirichlet
						q2 = repmat(1e5,numTBi,1);
						b2 = 1e5;
					else
						q2 = reshape(BdrDisc(it, it, :) / Bnr(it, it), numTBdr, 1); 
						b2 = 1/Bnr(it, it);
					end	
				else
					q2 = 0;
					b2 = 0;
				end
				
				% pde_fem is only used to create the appropriate matrices, but inputs and the
				% reaction term are handled separately!
				q3 = @(z) 0*z;	
				g = @(z) 0*z;

				% dummy output operator: not used, but pde_fem needs correct class of
				% passed argument!
				C_op = dps.output_operator();
				
				% This 
				[Ai,Bi,~,~,~,MM,BMi,CM] = numeric.pde_fem(ndisc,lambda,c,0,q1,b1,q2,b2,q3,C_op,g,'lin');
				% Manually insert pointwise in-domain inputs
				% ATTENTION: In this case, the BC is REPLACED by an in-domain condition, wich
				% leads to an ill-posed problem. This should not be used!
				BMi2 = zeros(ndisc,ndisc,numtL);
				if ppide_sys.b_op1.z_b ~= 0
					% first: approximate by nearest index
					idx = misc.get_idx(ppide_sys.b_op1.z_b,zdisc);
					BMi2(idx,idx,:) = q1.*lambda(idx,:);
					warning('When the left BC is not at z=0, this results in an ill-posed problem!')
				end
				if ppide_sys.b_op2.z_b ~= 1
					% first: approximate by nearest index
					idx = misc.get_idx(ppide_sys.b_op2.z_b,zdisc);
					BMi2(idx,idx,:) = q2.*lambda(idx,:);
					warning('When the right BC is not at z=1, this results in an ill-posed problem!')
				end
				Ai = Ai - multArray(MM^(-1),BMi2);
				
				% insert dynamic for this state into the system matrix of the whole system
				A_pde((it-1)*ndisc+1:it*ndisc,(it-1)*ndisc+1:it*ndisc,:) = Ai;

				% insert reaction matrix
				for z_idx = 1:length(zdisc)
					Az = reshape(pars.a_disc(z_idx,:,:,:),[n n numtA]);
					for jt=1:n
						A_kop((it-1)*ndisc+z_idx,(jt-1)*ndisc+z_idx,:) = ...
								Az(it,jt,:); %coupling matrix			
					end
				end
	
				% insert coupling via BCs
				for jt = 1:n
					Bdl_kop((it-1)*ndisc+1,(jt-1)*ndisc+1,:) = Bdl_od(it,jt,:);% jeweils links oberer Eintrag, wird dann mit x_i(0) multipliziert.
					Bdr_kop(it*ndisc,jt*ndisc,:) = Bdr_od(it,jt,:);% wird dann mit x_i(1) multipliziert.
				end
				
				% ATTENTION: 
				% pde_fem only works for input coefficients = 1. For this reason, influence of
				% the coupling of the BCs is completely implemented outside pde_fem by means of
				% virtual "input matrices"
				B_q0((it-1)*ndisc+1:it*ndisc,(it-1)*ndisc+1,:) = Bi(:,1,:); % so gehen Verkopplungselemente ein (�ber linke Seite) (Neu oder Dir)
				B_q1((it-1)*ndisc+1:it*ndisc,it*ndisc,:) = Bi(:,2,:); % so gehen Verkopplungselemente ein (�ber rechte Seite) (Neu oder Dir)

				% boundary inputs:
				% 1. left:
				B1 = zeros(ndisc,p1,numTBi); 
				% for all left inputs
				for jt =1:p1
					if b1 == 10^5 % left dirichlet
						B1(1,jt,:) = ppide_sys.b_1(it,jt)*BMi(1,1,:);
					else % b1 something
						% Neumann/Robin:
						% In that case, the sign of b must be inverted, as the unit vector
						% normal to the boundary is pointing to the left side.
						B1(1,jt,:) = -1*lambda(1,:)*ppide_sys.b_1(it,jt); % this has been checked several times and appears to be correct.
					end
				end
				Bl((it-1)*ndisc+1:it*ndisc,:,:) = multArray(MM^(-1),B1);
				% 2. right:
				B2 = zeros(ndisc,p2,numTBi); 
				% for all right inputs
				for jt = 1:p2
					if b2 == 10^5 % right dirirchlet
						B2(end,jt,:) = ppide_sys.b_2(it,jt)*BMi(end,end,:);
					else % b2 something
						% Neumann/Robin
						% Here, the sign needn't be inverted, as the normal unit vector is pointing to
						% the right.
						B2(end,jt,:) = 1*lambda(end,:)*ppide_sys.b_2(it,jt);
					end
				end
				Br((it-1)*ndisc+1:it*ndisc,:,:) = multArray(MM^(-1),B2);
				
				% Boundary disturbance inputs, analog to inputs
				% attention: ppide_sys.g_1 : distributed
				%            ppide_sys.g_2 : left
				%            ppide_sys.g_3 : right
				G1 = zeros(ndisc,nd,numTBi); 
				% for all left inputs
				for jt =1:nd
					if b1 == 10^5 % dirichlet left
						G1(1,jt,:) = ppide_sys.g_2(it,jt)*BMi(1,1,:);
					else % g2 something
						G1(1,jt,:) = -1*lambda(1,:)*ppide_sys.g_2(it,jt); % correct
					end
				end
				Gl((it-1)*ndisc+1:it*ndisc,:) = multArray(MM^(-1),G1);
				
				G2 = zeros(ndisc,nd,numTBi); 
				for jt=1:nd
					if b2 == 10^5 % dirichlet right
						G2(end,jt,:) = ppide_sys.g_3(it,jt)*BMi(end,end,:);
					else % g3 something
						G2(end,jt,:) = 1*lambda(end,:)*ppide_sys.g_3(it,jt);
					end
				end
				Gr((it-1)*ndisc+1:it*ndisc,:) = multArray(MM^(-1),G2);

				% local and integral couplings
				% change structure to fit to system representation:
				for jt = 1:n
					if ~isempty(ppide_sys.z0) % there is a midpoint feedback
						z0jIdx = find(zdisc>=ppide_sys.z0(jt),1);
						if z0jIdx == 1 || z0jIdx  == ndisc
							error('z0 is too close to the border!')
						end
						a_z0_disc_sys((it-1)*ndisc+1:it*ndisc, (jt-1)*ndisc+z0jIdx,:) = reshape(pars.a_z0_disc(:,it,jt,:),[ndisc,numtAZ0]);
% 						c_z0_disc_sys((it-1)*ndisc+(1:ndisc), (jt-1)*ndisc+z0jIdx+(0:1),:) = reshape(pars.c_z0_disc(:,it,jt,:) .* ([-1, 1]/(zdisc(z0jIdx)-zdisc(z0jIdx-1))),[ndisc,2,numtC0]);
% 						c_z0_disc_sys((it-1)*ndisc+(1:ndisc), (jt-1)*ndisc+1:jt*ndisc,:) = reshape(pars.c_z0_disc(:,it,jt,:) .* gradOp(z0jIdx,:),[ndisc,ndisc,numtCZ0]);
						c_z0_disc_sys((it-1)*ndisc+(1:ndisc), (jt-1)*ndisc+z0jIdx+(0:1),:) = reshape(pars.c_z0_disc(:,it,jt,:) .* ([-1, 1]/(zdisc(z0jIdx+1)-zdisc(z0jIdx))),[ndisc,2,numtCZ0]);
					end
					a_0_disc_sys((it-1)*ndisc+1:it*ndisc, (jt-1)*ndisc+1,:) = reshape(pars.a_0_disc(:,it,jt,:),[ndisc,numtA0]);
					
					a_1_disc_sys((it-1)*ndisc+1:it*ndisc, jt*ndisc) = pars.a_1_disc(:,it,jt);
					c_0_disc_sys((it-1)*ndisc+(1:ndisc), (jt-1)*ndisc+(1:2),:) = reshape(pars.c_0_disc(:,it,jt,:) .* ([-1, 1]/(zdisc(2)-zdisc(1))),[ndisc,2,numtC0]);
					% attention: Derivative at midpoint could be improved!
					
					% maybe computing with FEM-gradient would be better, but doesnt seem to
					% make the big difference.
% 					c_0_disc_sys((it-1)*ndisc+(1:ndisc), (jt-1)*ndisc+(1:ndisc),:) = ...
% 						permute(...
% 							pars.c_0_disc(:,it,jt,:).*gradOp(1,:)...
% 						,[1,2,4,3]);
					c_1_disc_sys((it-1)*ndisc+(1:ndisc), (jt-1)*ndisc+((ndisc-1):ndisc)) = pars.c_1_disc(:,it,jt) * ([-1, 1]/(zdisc(2)-zdisc(1)));
					for z_idx=1:ndisc
						if z_idx>1						  
							MM_loc = get_lin_mass_matrix(zdisc(1:z_idx));
							% To perform integration as a multiplication:
							if numtF >1
								F_int = [permute(...
									misc.multArray(pars.F_disc(z_idx,1:z_idx,it,jt,:),MM_loc,2, 1)...
									,[1 5 4 2 3])...
									, zeros(1,length(zdisc)-z_idx,numtF)];
							else
								F_int = [pars.F_disc(z_idx,1:z_idx,it,jt,:)*MM_loc...
									  zeros(1,length(zdisc)-z_idx)];
							end
						else
							F_int = zeros(1,ndisc,numtF); % for z = 0 the integral is 0
						end
						F_disc_sys((it-1)*ndisc+1+z_idx-1,(jt-1)*ndisc+1:jt*ndisc,:) = F_int;
					end % for z_idx=1:ndisc
				end % for j=1:n
			end % for i=1:n
			obj.Bl = Bl;
			obj.Br = Br;
			obj.G = Gv+Gl+Gr;
			% St�rdurchgriff:
			obj.Gy = ppide_sys.g_4;
			
			% ACHTUNG: VORZEICHEN: Q ist auf der linken seite!
			AKopBCl = zeros(n*ndisc,n*ndisc,numTBi);
			AKopBCr = zeros(n*ndisc,n*ndisc,numTBi);
			% Hartwig edited
			if size(Bdl_kop,3) ~= numTBi
				if size(Bdl_kop,3) == 1
					Bdl_kop = repmat(Bdl_kop,1,1,numTBi);
				else
					BdlInterpolant = interpolant({1:n*ndisc,1:n*ndisc,linspace(0,1,numTBdl)}, Bdl_kop);
					Bdl_kop = BdlInterpolant.evaluate(1:n*ndisc,1:n*ndisc,linspace(0,1,numTBi));
				end
			end
			if size(Bdr_kop,3) ~= numTBi
				if size(Bdr_kop,3) == 1
					Bdr_kop = repmat(Bdr_kop,1,1,numTBi);
				else
					BdrInterpolant = interpolant({1:n*ndisc,1:n*ndisc,linspace(0,1,numTBdr)}, Bdr_kop);
					Bdr_kop = BdrInterpolant.evaluate(1:n*ndisc,1:n*ndisc,linspace(0,1,numTBi));
				end
			end
			
			for tIdx =1:numTBi
				AKopBCl(:,:,tIdx) = B_q0(:,:,tIdx)*Bdl_kop(:,:,tIdx);
				AKopBCr(:,:,tIdx) = B_q1(:,:,tIdx)*Bdr_kop(:,:,tIdx);
			end
			obj.A = A_pde + A_kop - AKopBCl - AKopBCr ...
				+ a_0_disc_sys + a_1_disc_sys + c_0_disc_sys + c_1_disc_sys ...
				+ a_z0_disc_sys + c_z0_disc_sys + F_disc_sys;
			% Berechne Ausgangsmatrix f�r die Approximation
			obj.C = ppide_sys.c_op.output_matrix(zdisc);
			
			obj.zdisc = zdisc;
		end % function approximate_ppide
	end % methods
	
end % class

