function f1 = matrix_3D(M, varargin)
%BLABLABLA
%           
% INPUT PARAMETERS:
% 	3D-MATRIX                 M : distributed Matrix
%   
% OUTPUT PARAMETERS:
% 	FIGURE                   f1 : BLABLALBA
%
% required subprogramms:
%   - 
%
% global variables:
%   - 
%
% history:
% created on 27.07.2016 by Jakob Gabriel

p = inputParser;
default_domain = linspace(0,1,size(M,1));
default_title = 'Element';
addParameter(p,'domain',default_domain);
addParameter(p,'title',default_title);
parse(p,varargin{:});

f1 = figure('name',p.Results.title);
subfigpos = 1;
for it = 1:size(M,2)
    for jt = 1:size(M,3)
        subplot(size(M,2), size(M,3), subfigpos);
        plot(p.Results.domain, M(:,it,jt));
        grid minor;
        xlabel('z');
        title([p.Results.title, '_', num2str(it),'_',num2str(jt)]);
        subfigpos = 1 + subfigpos;
    end
end
end

