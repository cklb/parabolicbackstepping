function f1 = state_vector_over_time_and_space_3D(time, x, sys)
%UNTITLED Summary of this function goes here
%   Detailed explanation goes here

% f1 = figure('name', 'states over time and space');
f1 = figure(); %TODO Wieder so ändern, dass ein titel für die Figure übergeben werden kann

column_sp = ceil(sqrt(sys.n));
row_sp = ceil(sys.n/column_sp);

if isfield(sys.approx, 'cfl')
%% different grid for every state
	if numel(x) == sys.n
		x_temp = x;
	else
		x_temp = sys.stateVecToCell(x);
	end
	
	for i=1:sys.n
		ndisc = sys.approx.cfl.ndisc{i};
		zdisc = sys.approx.cfl.zdisc{i};
		subplot(row_sp, column_sp, i);
		x_i(:, 1:ndisc) = squeeze(x_temp{i});

		s_i = surf(time, zdisc, x_i.');
		s_i.LineStyle = 'none';
		if exist('viridis.m', 'file')
			colormap(viridis);
		end

		zlabel(['x_', num2str(i), '(z,t)']);
		ylabel('z');
		xlabel('time');
		
		clearvars x_i ndisc zdisc
	end
	
else
%% Regular grid
	if size(x,1) == sys.n
		ndisc = size(x,3);
		grid = linspace(0,1,ndisc);

		for i=1:sys.n
			subplot(row_sp, column_sp, i);
			x_i(:, 1:ndisc) = squeeze(x(i,:,:));

			s_i = surf(time, grid, x_i.');
			s_i.LineStyle = 'none';
			if exist('viridis.m', 'file')
				colormap(viridis);
			end

			zlabel(['x_', num2str(i), '(z,t)']);
			ylabel('z');
			xlabel('time');
		end
	else
		ndisc = size(x,2)/ sys.n;
		grid = linspace(0,1,ndisc);

		for i=1:sys.n
			subplot(row_sp, column_sp, i);
			x_i = zeros(size(x,1), ndisc);

			x_i(:, 1:ndisc) = x(:,(i-1)*ndisc + (1:ndisc));

			s_i = surf(time, grid, x_i.');
			s_i.LineStyle = 'none';
			if exist('viridis.m', 'file')
				colormap(viridis);
			end

			zlabel(['x_', num2str(i), '(z,t)']);
			ylabel('z');
			xlabel('time');
		end
	end
end