%% Script for sampling, plotting and exporting reference tracking plots

%% plot reference tracking
% sample
sim_lr.timeSteps = 800;
sim_lr.duration = 10;
sim_lr.t = linspace(0, sim_lr.duration, sim_lr.timeSteps);
r_interp = griddedInterpolant({sim.t, 1:sys.p}, sim.r);
y_interp = griddedInterpolant({sim.t, 1:sys.p}, sim.y);
sim_lr.r = r_interp({sim_lr.t, 1:sys.p});
sim_lr.y = y_interp({sim_lr.t, 1:sys.p});

% plot
figure();
plot(sim_lr.t, sim_lr.r, 'r--'); hold on;
plot(sim_lr.t, sim_lr.y);
xlim([0, sim_lr.t(end)]);

% export
matlab2tikz('externalData', true);

%% plot observer error norm
% sample
e_x_norm_interp = griddedInterpolant({sim.t}, sim.e_x_norm);
sim_lr.e_x_norm = e_x_norm_interp({sim_lr.t});

% plot
figure();
plot(sim_lr.t, sim_lr.e_x_norm);
xlim([0, sim_lr.t(end)]);

% export
matlab2tikz('externalData', true);

%% plot observer error 3d
% sample
sim_lr3d.timeSteps = 31;
sim_lr3d.spaceSteps = 21;
sim_lr3d.duration = sim.t(1207);
sim_lr3d.t = linspace(0, sim_lr3d.duration, sim_lr3d.timeSteps);
size2X = size(sim.x, 2);
sim_lr3d.e_xVec = sim.x - sim.x_hat;
e_xVec_interp = griddedInterpolant({sim.t, 1:size2X}, sim.x - sim.x_hat);
sim_lr3d.e_xVec = e_x_interp({sim_lr3d.t, 1:size2X});
sim_lr3d.e_xMat = zeros(sim_lr3d.timeSteps, sim_lr3d.spaceSteps, sys.n);
for it = 1:sys.n
	ex_i_temp = sim_lr3d.e_xVec(:, sum([sys.approx.cfl.ndisc{1:it-1}]) + (1:sys.approx.cfl.ndisc{it}));
	ex_i_tempInterpolant = griddedInterpolant({sim_lr3d.t, sys.approx.cfl.zdisc{it}}, ex_i_temp); 
	sim_lr3d.e_xMat(:, :, it) = ex_i_tempInterpolant({sim_lr3d.t, linspace(0, 1, sim_lr3d.spaceSteps)});
end

% test figure();
figure();
column_sp = ceil(sqrt(sys.n));
row_sp = ceil(sys.n/column_sp);	
for it=1:sys.n
	ndisc = sim_lr3d.timeSteps;
	zdisc = linspace(0, 1, sim_lr3d.spaceSteps);
	subplot(row_sp, column_sp, it);
	x_i = sim_lr3d.e_xMat(:, :, it);

	s_i = surf(sim_lr3d.t, zdisc, x_i.');
	s_i.LineStyle = 'none';
	if exist('viridis.m', 'file')
		colormap(viridis);
	end

	zlabel(['x_', num2str(it), '(z,t)']);
	ylabel('z');
	xlabel('time');

	clearvars x_i ndisc zdisc
end

% export
matlab2tikz('externalData', true);

%% plot disturbance estimate
% sample
d_interp = griddedInterpolant({sim.t, 1:sys.q}, sim.d);
d_hat_interp = griddedInterpolant({sim.t, 1:sys.q}, sim.d_hat);
sim_lr.d = d_interp({sim_lr.t, 1:sys.q});
sim_lr.d_hat = d_hat_interp({sim_lr.t, 1:sys.q});
selectD = (sum(abs(sim_lr.d_hat)) ~= 0);
% plot
figure();
plot(sim_lr.t, sim_lr.d_hat(:, selectD), 'r--'); hold on;
plot(sim_lr.t, sim_lr.d(:, selectD));
xlim([0, sim_lr.t(end)]);

% export
matlab2tikz('externalData', true);