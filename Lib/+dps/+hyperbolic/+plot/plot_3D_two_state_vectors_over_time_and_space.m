function f1 = plot_3D_two_state_vectors_over_time_and_space(time, x, x2, sys)
%UNTITLED Summary of this function goes here
%   Detailed explanation goes here

f1 = figure('name', 'states over time and space');

column_sp = 2;
row_sp = sys.n;

for i=1:sys.n
	subplot(row_sp, column_sp, 2*i-1);
	x_i = zeros(size(x,1), sys.numpoints_z);
	for z = 1:sys.numpoints_z
		x_i(:, z) = x(:,(i-1)*sys.numpoints_z + z);
	end
	
	s_i = surf(time, sys.grid, x_i.');
	s_i.LineStyle = 'none';
	if exist('viridis.m', 'file')
		colormap(viridis);
	end
	
	zlabel(['x_', num2str(i), '(z,t)']);
	ylabel('z');
	xlabel('time');
end

for i=1:sys.n
	subplot(row_sp, column_sp, 2*i);
	x_i = zeros(size(x2,1), sys.numpoints_z);
	for z = 1:sys.numpoints_z
		x_i(:, z) = x(:,(i-1)*sys.numpoints_z + z);
	end
	
	s_i = surf(time, sys.grid, x_i.');
	s_i.LineStyle = 'none';
	if exist('viridis.m', 'file')
		colormap(viridis);
	end
	
	zlabel(['x_', num2str(i), '(z,t)']);
	ylabel('z');
	xlabel('time');
end

end