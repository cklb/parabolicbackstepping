function f1 = plot_vector_over_time(y, t, tc_eq_to, subplot, y_label)
%PLOT_VECTOR_OVER_TIME Summary of this function goes here
%   Detailed explanation goes here

f1 = figure('name', 'vector over time');
n = size(y,2);
legend_cell = cell(1,3);

if ~subplot
    hold on;
    for i=1:n
        plot(t, y(:,i));
        legend_cell{i} = [y_label, '_', num2str(i)];
    end
    grid on;
    xlabel('time');
    ylabel([y_label, '_i(t)']);
    if tc_eq_to ~= 0
        plot([tc_eq_to, tc_eq_to], [min(min(y)), max(max(y))], ...
            'm--', 'LineWidth', 1.2);
        plot([2*tc_eq_to, 2*tc_eq_to], [min(min(y)), max(max(y))], ...
            'r--', 'LineWidth', 1.2);
        legend([legend_cell(1,1:n), 't_c = t_o', '2 t_c = 2 t_o']);
    else
        legend(legend_cell(1,1:n));
    end
    
elseif subplot
    for i=1:n
        subplot(sys.n,1,i);
        hold on;
        plot(t, y(:,i))
    end
end

