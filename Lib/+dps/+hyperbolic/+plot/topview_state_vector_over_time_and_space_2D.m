function f1 = topview_state_vector_over_time_and_space_2D(time, x, sys)
%UNTITLED Summary of this function goes here
%   Detailed explanation goes here

f1 = figure('name', 'states over time and space');

column_sp = ceil(sqrt(sys.n));
row_sp = ceil(sys.n/column_sp);

for i=1:sys.n
    subplot(row_sp, column_sp, i);
	x_i = zeros(size(x,1), sys.numpoints_z);
    
    for z = 1:sys.numpoints_z
        x_i(:, z) = x(:,(i-1)*sys.numpoints_z + z);
    end
    
    s_i = pcolor(time, sys.grid, x_i.');
    s_i.LineStyle = 'none';
    
    ylabel('z');
    xlabel('time');
    hold on;
    
    % Ausbreitungsrichtung
    if i > sys.p
        z_start = sys.z0;
        z_end = sys.z1;
    else
        z_start = sys.z1;
        z_end = sys.z0;
    end
    t_start = 0; 
    t_end = t_start + (z_end-z_start) / (-sys.Lambda(i,i)); 
    plot(linspace(t_start, t_end, 2), ...
        linspace(z_start, z_end, 2), ...
        'r',  'LineWidth', 1.5);
    
    % Maximale Zeit bis zum Einschwingen/Abklingen
    max_decay_time = sys.domainLength * diag(abs(inv(sys.Lambda))).' * ...
       [ones(sys.p+1,1); zeros(sys.n-sys.p-1,1)];
   
	plot(linspace(max_decay_time, max_decay_time, 2), ...
        linspace(z_start, z_end, 2), ...
        'r',  'LineWidth', 1.5);
end

