function f1 = state_vector_over_time_and_space(time, x, sys, twoD, threeD)
%PLOT_STATE_VECTOR_OVER_TIME_AND_SPACE Summary of this function goes here
%   Detailed explanation goes here

if twoD && ~threeD
    f1 = dps.hyperbolic.plot.topview_state_vector_over_time_and_space_2D(time, x, sys);
    
elseif ~twoD && threeD
    f1 = dps.hyperbolic.plot.state_vector_over_time_and_space_3D(time, x, sys);
    
elseif twoD && threeD
    f1 = figure('name', 'states over time and space');
    if size(x,3) ~= 1
        error(['x must be a matrix of the size ',...
            'time x (sys.approx.ndisc*sys.n)']);
	end
    
	if sys.n==1
		if isfield(sys.approx,'cfl')
			zdisc = sys.approx.cfl.zdisc{1};
		else
			zdisc = sys.approx.zdisc;
		end
		%% plot 3d
		subplot(2, sys.n, 1);
		s_i = surf(time, zdisc, x.');
		s_i.LineStyle = 'none';
		if exist('viridis.m', 'file')
			colormap(viridis);
		end

		zlabel(['x_', num2str(1), '(z,t)']);
		ylabel('z');
		xlabel('time');

		%% plot 2d
		subplot(2, sys.n, 1+sys.n);
		s_i = pcolor(time, zdisc, x.');
		s_i.LineStyle = 'none';

		ylabel('z');
		xlabel('time');
		hold on;
	else
		for i=1:sys.n
			x_i = zeros(size(x,1), sys.approx.ndisc);

			for z = 1:sys.approx.ndisc
				x_i(:, z) = x(:,(i-1)*sys.approx.ndisc + z);
			end


			%% plot 3d
			subplot(2, sys.n, i);
			s_i = surf(time, sys.approx.zdisc, x_i.');
			s_i.LineStyle = 'none';
			if exist('viridis.m', 'file')
				colormap(viridis);
			end

			zlabel(['x_', num2str(i), '(z,t)']);
			ylabel('z');
			xlabel('time');

			%% plot 2d
			subplot(2, sys.n, i+sys.n);
			s_i = pcolor(time, sys.approx.zdisc, x_i.');
			s_i.LineStyle = 'none';

			ylabel('z');
			xlabel('time');
			hold on;

			% Ausbreitungsrichtung
			if i > sys.p
				z_start = sys.z0;
				z_end = sys.z1;
			else
				z_start = sys.z1;
				z_end = sys.z0;
			end
			t_start = 0; 
			t_end = t_start + (z_end-z_start) / (-sys.Lambda(i,i)); 
			plot(linspace(t_start, t_end, 2), ...
				linspace(z_start, z_end, 2), ...
				'r',  'LineWidth', 1.5);

		end
	end
end
end

