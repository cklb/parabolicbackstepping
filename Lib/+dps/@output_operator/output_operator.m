classdef output_operator
% Class OUTPUT_OPERATOR: represents an output-operator of the form
%    Cx = c_0 x(0) + c_d0 dz x(0)
%			+ c_1 x(1) + c_d1 dz x(1)
%			+ c_m x(z_m) + c_dm x_z(z_m)
%			+ int_0^1 c_c(z) x(z) dz
%
%    The distributed spatial characteristic c_c(z) is stored as an
%    anonymous function. (distinction of cases can be achieved by using
%    the function iff)


%history;
%created by Simon Kerschaum (SK) 2015
%
%modified on 12.05.2016 by Ferdinand Fischer (FF):
%   * added punctual measurement for the spatial derivative
%   * extension to mutlivariable output
%modified on 15.03.2018 by SK:
%   * modified constructor to be able to set parameters
%   * enable different m and n
%   * moved use_output_operator as a class function!

    properties
        c_0 = 0;		% dirichlet measurement left
		c_d0 = 0;		% neumann measurement left
		c_1 = 0;		% dirichlet measurement right
		c_d1 = 0;		% neumann measurement right
		c_m =  0;		% dirichlet measurement anywhere
        c_dm = 0;		% neumann measurement anywhere
		z_m = 0.5;		% point of dirichlet and neumann measurement
		c_c = @(z)0;	% distributed measurement
        m = 1;			% number of outputs
		n = 1;			% number of states
	end
	
	methods
		result = use_output_operator(obj, x, zdisc, xz, grad_op)
		% RESULT = USE_OUTPUT_OPERATOR(obj, x, zdisc, xz, grad_op) uses the output operator 
		%  C on the vector x and returns result = Cx.
	end
	
    methods
		% constructor
        function obj = output_operator(varargin)
			% OUTPUT_OPERATOR create output operator object
			%   obj = output_operator('parName', parValue, ...)
			%   sets the properties of the output operator by name-value
			%   pairs. All parameters which are not set are automatically
			%   set to zero matrices with appropriate dimesions.
			
			obj.m = 1;
			obj.n = 1;
			arglist = properties(obj);
			% n and m cannot be set by name-value pair!
			% all not-set operators' sizes are determined automatically!
			arglist = arglist(1:end-2);
			written = {};
			storage = struct;
			found = 0;
			if mod(length(varargin), 2) % uneven number
				error('When passing additional arguments, you must pass them pairwise!')
			end
			for index = 1:2:length(varargin) % loop through all passed arguments:
				for arg = 1:length(arglist)
					if strcmp(varargin{index}, arglist{arg})
						storage.(arglist{arg}) = varargin{index+1};
						written = [written arglist{arg}]; %#ok<AGROW>
						found = 1;
						break
					end 
				end % for arg
				% argument wasnt found in for-loop
				if ~found
					error([varargin{index} ' is not a valid property of class ' class(obj) '!']);
				end
				found = 0; % reset found
			end % for index
			
			if ~isempty(written)
				if strcmp(written{1}, 'c_c')
					if ~isa(storage.c_c, 'function_handle') % no function passed.
						obj.m = size(storage.(written{1}), 1);
						obj.n = size(storage.(written{1}), 2);
					else
						obj.m = size(storage.(written{1})(0), 1);
						obj.n = size(storage.(written{1})(0), 2);
					end
				elseif strcmp(written{1}, 'z_m')
					if length(written) > 1
						obj.m = size(storage.(written{2}), 1);
						obj.n = size(storage.(written{2}), 2);
					end
				else
					obj.m = size(storage.(written{1}), 1);
					obj.n = size(storage.(written{1}), 2);
				end
			end
			% Jetzt alle werte auf Default-Werte setzen:
			obj.c_0 = zeros(obj.m, obj.n);
			obj.c_d0 = zeros(obj.m, obj.n);
			obj.c_1 = zeros(obj.m, obj.n);
			obj.c_d1 = zeros(obj.m, obj.n);
			obj.c_m = zeros(obj.m, obj.n);
			obj.z_m = 0;
			obj.c_dm = zeros(obj.m, obj.n);
			obj.c_m = zeros(obj.m, obj.n);
			obj.c_c = @(z)0*z+zeros(obj.m, obj.n);
			% und die zwischengespeicherten Wert wiederherstellen:]
			fn = fieldnames(storage);
			for fn_idx = 1:length(fn)
				obj.(fn{fn_idx}) = storage.(fn{fn_idx});
			end
			if ~isa(obj.c_c, 'function_handle')
				obj.c_c = @(z) 0*z+obj.c_c;
			end
			% �berpr�fen, ob alle Matrizen die gleiche Dimension haben:
			for i = 1:length(arglist)-1
				for j = i+1:length(arglist)
					if strcmp(arglist{i}, 'c_c')
						val1 = obj.(arglist{i})(0);
					else
						val1 = obj.(arglist{i});
					end
					if strcmp(arglist{j}, 'c_c')
						val2 = obj.(arglist{j})(0);
					else
						val2 = obj.(arglist{j});
					end
					if ~strcmp(arglist{i}, 'z_m') && ~strcmp(arglist{j}, 'z_m')% z_m ist immer skalar!
						if any(size(val1)-size(val2))
							error(['Dimensions of ' arglist{i} ' and ' arglist{j} ' are not the same!']);
						end
					end
				end
			end
		end % constructor
        
		
        function obj = set_params(obj, c0, cd0, c1, cd1, cc, cm, zm, cdm)
			% set output_operator parameters: OBSOLETE! MUST BE MODIFIED!
			warning('The function output_operator.set_params is obsolete and not up-to-date!')
			% c_op = c_op.set_params(c0, cd0, c1, cd1, cc, cm, zm, cdm)
				if ~exist('cdm', 'var'), cdm = 0; end      
				obj.m = size(c0, 1);

				if zm<0 || zm >1
					error('zm must be set between 0 and 1!')
				end
				obj.c_0 = c0;
				obj.c_d0 = cd0;
				obj.c_1 = c1;
				obj.c_d1 = cd1;
				if isa(cc, 'function_handle')
					obj.c_c = cc;
				else
					error('cc must be a function handle!')
				end
				obj.c_m = cm;
				obj.c_dm = cdm;
				obj.z_m = zm;
		end
		
		function C = output_matrix(obj, zdisc, grad_op)
			import numeric.*
			import misc.*
			import external.fem.*
			% calculate ouput matrix belonging to output operator
			% beeing used as 
			% y = C*x.
			% 1. Create helper matrices, to select certain values:
			if ~exist('grad_op', 'var')
				grad_op = get_grad_op(zdisc);
			end
			massMatrix = get_lin_mass_matrix(zdisc);
			M_diag(1:obj.n^2, 1:obj.n^2) = 0;
			ndisc = length(zdisc);
			X0(1:obj.n, 1:obj.n*ndisc) = 0;
			X1(1:obj.n, 1:obj.n*ndisc) = 0;
			Xm(1:obj.n, 1:obj.n*ndisc) = 0;
			Xzm(1:obj.n, 1:obj.n*ndisc) = 0;
			grad_op_complete(1:obj.n*ndisc, 1:obj.n*ndisc) = 0;
			c_disc = eval_pointwise(obj.c_c, zdisc);
			
			% in Streckenform bringen:
			cc_mat(1:obj.m, 1:obj.n*ndisc) = 0;
			for i = 1:obj.n
				for m_idx = 1:obj.m
					cc_mat(m_idx, (i-1)*ndisc+1:i*ndisc) = squeeze(c_disc(:, m_idx, i));
				end
			end
			
			for i = 1:obj.n
				grad_op_complete((i-1)*ndisc+1:i*ndisc, (i-1)*ndisc+1:i*ndisc) = grad_op;
				X0(i, (i-1)*ndisc+1) = 1; % Auswahlmatrix f�r ersten Eintrag jedes Zustandes
				X1(i, i*ndisc-1) = 1; % Auswahlmatrix f�r letzten Eintrag jedes Zustandes
				Xz0 = X0*grad_op_complete;
				Xz1 = X1*grad_op_complete;
				Xm(i, (i-1)*ndisc+1:i*ndisc) = interp_mat(zdisc, obj.z_m); % Auswahlmatrix f�r den Punkt z_m jedes Zustandes.
				Xzm(i, (i-1)*ndisc+1:i*ndisc) = interp_mat(zdisc, obj.z_m)*grad_op; % Auswahlmatrix f�r den Punkt z_m jedes Zustandes.
				M_diag((i-1)*ndisc+1:i*ndisc, (i-1)*ndisc+1:i*ndisc) = massMatrix;
			end
			
			C = obj.c_0 * X0 + obj.c_1 * X1 ...
				+ obj.c_d0 * Xz0 + obj.c_d1 * Xz1...
				+ obj.c_m * Xm + obj.c_dm * Xzm...
				+ cc_mat * M_diag;
		end
                
	end
end