function  result = use_output_operator(C,x,zdisc,xz,grad_op)
% RESULT = USE_OUTPUT_OPERATOR(C,x,zdisc) uses the output operator 
%     C on the vector x and returns result = Cx.
% 
% x is the function on which the operator shall be used, evaluated at the
% spatial discretization points set by zdisc.
% Needs the gradient operator grad_op for computing the derivative.

% Created on 03.07.2014 by Simon Kerschbaum
%
% modified on 13.08.2014 by SK:
%    * added punctual measurement c_m*x(z_m)
% modified on 15.03.2018 by SK:
%    * added implementation for multiple states
% modified on 21.06.2018 by SK:
%    * added implementation for matrices. (multiple columns)

import misc.*
import numeric.*

zdisc=zdisc(:)';
ndisc = length(zdisc);
if ~exist('grad_op','var')
	grad_op=get_grad_op(zdisc);
end

if ~isa(C,'dps.output_operator')
	error('C must be of class output_operator')
end
n = size(C.c_0,2);
m = size(C.c_0,1);
if isvector(x) % a vector in simulation format was passed
	xTemp = x(:);
	x=zeros(ndisc,n);
	for i=1:n
		x(:,i) = xTemp((i-1)*ndisc+1:i*ndisc); % transform to system representation
	end
end
% number of states
if size(x,1)~=ndisc
	error('x must be a matrix whose first dimension is the spatial variable!');
end
if size(x,2)~= n
	error('The second dimension of x must be the system order n.')
end
numCols = size(x,3); % arbitray number of columns

% Ausgangsoperator anwenden:
c_dis = eval_pointwise(C.c_c,zdisc);
% preallocation
x_m(1:n,1)=0; dzx_m(1:n,1)=0; x0(1:n,1)=0;dzx0(1:n,1)=0;x1(1:n,1)=0;dzx1(1:n,1)=0;
dzx = zeros(ndisc,n,numCols);
for i=1:n
	for j=1:numCols
		if ~exist('xz','var')
			dzx(:,i,j) = grad_op*x(:,i,j);	
		else % xz was passed explicitly
			dzx = xz; 
		end
		% an Messpunkt zm auswerten:
		x_m(i,j) = interp1(zdisc,x(:,i,j),C.z_m);
		x0(i,j) = x(1,i,j);
		x1(i,j) = x(end,i,j);
		dzx_m(i,j) = interp1(zdisc,dzx(:,i,j),C.z_m);
		dzx0(i,j) = dzx(1,i,j);
		dzx1(i,j) = dzx(end,i,j);
	end
end
temp_prod=0;% reset
temp_prod(1:ndisc,1:m,1:numCols)=0; %preallocate
for z_idx=1:ndisc
		temp_prod(z_idx,1:m,1:numCols) = squeeze(c_dis(z_idx,:,:))*reduce_dimension(x(z_idx,:,:));
end
xint = reshape(trapz(zdisc,temp_prod,1),[m numCols]);

result = C.c_0 * x0 + C.c_d0 * dzx0...
	     + C.c_1 * x1 + C.c_d1 * dzx1...
		 + xint...
		 + C.c_m*x_m + C.c_dm*dzx_m;