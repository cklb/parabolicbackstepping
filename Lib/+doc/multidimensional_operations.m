% This is a documentation file for methdods that have been created on purpose to perform
% operations on multidimensional arrays without implementing many for-loops which have poor
% performance.
%
% created on 01.10.2018 by Simon Kerschbau
%
%
% OVERVIEW:
% function                        Short purpose
%
% mult_array                      Multiply arrays of arbitray dimensions.
% mult_array(...,'elemWise')      Element-wise multiplication of arbitrary arrays
% trapz_fast_nDim                 Trapezoidal integration of arrays of arbitrary dimensions over
%                                 arrays of arbitrary dimensions
% trapz_fast_nDim(...,'equal')    Trapezoidal integration of arrays of arbitrary dimensions over
%                                 arrays of same dimension
% cumtrapz_fast_nDim              Cumulative trapezoidal integration of arrays of arbitrary 
%                                 dimensions over arrays of arbitrary dimensions
% cumtrapz_fast_nDim(...,'equal') Cumulative trapezoidal integration of arrays of arbitrary 
%                                 dimensions over arrays of same dimension

%%
clearvars A B C CComp
% USAGE EXAMPLES:
% 1. mult_array
% 1.1. Perform a multidimensional matrix multiplication.
% Just as for a normal matrix multiplication, when performing 
% A*B, the length of the last dimension of A must equal the length of the first dimension of B.
% In contrast to the *-operator, the dimensions of the matrices are not restricted to 2.
% e.g.
A = rand(4,2);
B = rand(2,3);
C = misc.multArray(A,B);
CComp = A*B;
disp(['1.1.: max(C-CComp)= ' num2str(max(C(:)-CComp(:)))]);
%
%%
clearvars A B C CComp
% 1.2. Multiplication of multidimensional arrays
% It is also possible to perform the multiplication over more dimensions:
% HINT: The function uses the reshape-function, wich can induce a numerical error that is
% always <= eps. This should be known.
A = rand(4,3,2);
B = rand(2,4,2);
C = misc.multArray(A,B);
for i=1:size(A,1)
	for j = 1:size(B,3)
		CComp(i,:,:,j) = squeeze(A(i,:,:))*squeeze(B(:,:,j));
	end
end
disp(['1.2.: max(C-CComp)= ' num2str(max(C(:)-CComp(:)))]);
%
%%
clearvars A B C CComp
% 1.3. Multiplication of arbitrary dimensions of multidimensional arrays
% The dimensions of the arrays to be multiplied needn't be sorted. Instead, the corresponding
% dimensions of the matrices to be multiplied can be specified in the argument:
A = rand(4,3,2);
B = rand(2,4,3,2);
% Perform multiplication over 2nd dimension of A and 3rd dimension of B:
C = misc.multArray(A,B,2,3);
for i=1:size(A,1)
	for j = 1:size(B,2)
		for k=1:size(B,4)
			CComp(i,:,:,j,k) = (squeeze(A(i,:,:)).'*squeeze(B(:,j,:,k)).');
		end
	end
end
disp(['1.3.: max(C-CComp)= ' num2str(max(C(:)-CComp(:)))]);
%
%%
clearvars A B C CComp
% 1.4. Permorm multidimensional "element-wise" multiplication
% This functionality might be a bit confusing. In Contrast to .* the matrices are not
% multiplied element-wise in each dimension, but only in a specific dimension.
% Again this is a fast way to achieve multiplications without for loops and is used in the
% (cum)trapz_fast_nDim functions.
A = rand(3,2);
B = rand(2,3,4);
C = misc.multArray(A,B,1,2,'elemWise');
for i=1:size(A,2)
	for j=1:size(B,1)
		for k=1:size(B,3)
			CComp(:,i,j,k) = A(:,i).*B(j,:,k).';
		end
	end
end
disp(['1.4.: max(C-CComp)= ' num2str(max(C(:)-CComp(:)))]);
%%
import numeric.*
%
% 2. trapz_fast_nDim
%
% 2.1. Integration over specific argument:
% integrate multidimensional array over one argument.
clear x y z zComp
x = linspace(0,1,21);
for i=1:3
	for j=1:5
		for k=1:6
			y(i,j,:,k) = (i+1)*(j+1)*k^3*x;
		end
	end
end
% This integration can also be done with trapz but is slightly faster with trapz_fast_nDim.
% It can be made even faster using the function trapz_fast, which only operates over vectors
% and matrices of correct dimensions.
% Speed checks of trapz and trapz_fast_nDim give different results on differnt runs, but are
% about the same speed.
% 
zComp = trapz(x,y,3);
z = trapz_fast_nDim(x,y,3);
disp(['2.1.: max(z-zComp)= ' num2str(max(z(:)-zComp(:)))]);
%
% 2.2. integration without for-loop:
% assume different x-vectors for different y-values:
clear x y z zComp
x(1,1,:) = linspace(0,1,21);
x(2,1,:) = linspace(0.5,1.5,21);
x(1,2,:) = linspace(0.25,1.25,21);
x(2,2,:) = linspace(0.25,2.25,21);
y(1,1,:) = x(1,1,:);
y(2,1,:) = 2*x(2,1,:);
y(1,2,:) = 3*x(1,2,:);
y(2,2,:) = 4*x(2,2,:);
%
% Each function shall be integrated wrt. its own argument with one operation:
z = numeric.trapz_fast_nDim(x,y,3,'equal');
% This is the same as a for loop:
for i=1:2
	for j=1:2
		zComp(i,j) = trapz(squeeze(x(i,j,:)),squeeze(y(i,j,:)));
	end
end
disp(['2.2.: max(z-zComp)= ' num2str(max(z(:)-zComp(:)))]);
%
% 2.3. Multiple combinations of integrations
% Now we want to integrate different functions for different arguments (in all possible
% combinations)
%
clear x y z zComp
for i=1:2
	for j=1:3
		x(i,j,:) = (i+2)*(j+1)*linspace(0,1,21);
		for k=1:4
			y(i,1,:,k) = i+linspace(0,2,21)-k;
		end
	end
end
% Each of the functions y(i,j,:,k)is integrated wrt. each of the arguments x(l,m,:). The result
% is an array with dimensions size(x,1) X size(x,2) X size(y,1) X size(y,2) x 1 x size(y,4). 
% The third dimension of both arguments is collapsed due to the integration.
% The dimensions of x and y may be different excepted the length of the integrated dimension,
% which must of course be equal.
% Somehow there may be a numerical error, which apparently is smaller as when performing the
% normal trapz.
z = numeric.trapz_fast_nDim(x,y,3);
% This is the same as a combination of 4 for-loops:
for i=1:2
	for j=1:3
		for l=1:2
			for k=1:4%
				%   x(1)x(2)y(1)y(2)1y(4)
				zComp(i,j,l,1,1,k) = trapz(squeeze(x(i,j,:)),y(l,1,:,k),3);
			end
		end
	end
end
disp(['2.3.: max(z-zComp)= ' num2str(max(z(:)-zComp(:)))]);
%
%%
% 3. cumtrapz_fast_nDim
% The function cumtrapz_fast_nDim works exactly the same as the trapz_fast_nDim, except that
% the integrated dimension is kept in the position of y with its original size beacause of the 
% cumulative integration.
% That means e.g. with
% x: (2 x 21 x 3)
% y: (3 x 21 x 2 x 4)
% z = cumtrapz_fast_nDim(x,y,2)
% will result in
%    x(1)x(3)y(1)y(2)y(3)y(4)
% z: (2 x 3 x 3 x 21 x 2 x 4)
% Here is the same code as above with cumulative integral:
% 3.1. Integration over specific argument:
% integrate multidimensional array over one argument.
clear x y z zComp
x = linspace(0,1,21);
for i=1:3
	for j=1:5
		for k=1:6
			y(i,j,:,k) = (i+1)*(j+1)*k^3*x;
		end
	end
end
% This integration can also be done with trapz but is slightly faster with trapz_fast_nDim.
% It can be made even faster using the function trapz_fast, which only operates over vectors
% and matrices of correct dimensions.
% Speed checks of trapz and trapz_fast_nDim give different results on differnt runs, but are
% about the same speed.
% 
zComp = cumtrapz(x,y,3);
z = cumtrapz_fast_nDim(x,y,3);
disp(['3.1.: max(z-zComp)= ' num2str(max(z(:)-zComp(:)))]);
%
% 3.2. integration without for-loop:
% assume different x-vectors for different y-values:
clear x y z zComp
x(1,1,:) = linspace(0,1,21);
x(2,1,:) = linspace(0.5,1.5,21);
x(1,2,:) = linspace(0.25,1.25,21);
x(2,2,:) = linspace(0.25,2.25,21);
y(1,1,:) = x(1,1,:);
y(2,1,:) = 2*x(2,1,:);
y(1,2,:) = 3*x(1,2,:);
y(2,2,:) = 4*x(2,2,:);
%
% Each function shall be integrated wrt. its own argument with one operation:
z = cumtrapz_fast_nDim(x,y,3,'equal');
% This is the same as a for loop:
for i=1:2
	for j=1:2
		zComp(i,j,:) = cumtrapz(squeeze(x(i,j,:)),squeeze(y(i,j,:)));
	end
end
disp(['3.2.: max(z-zComp)= ' num2str(max(z(:)-zComp(:)))]);
%
% 3.3. Multiple combinations of integrations
% Now we want to integrate different functions for different arguments (in all possible
% combinations)
%
clear x y z zComp
for i=1:2
	for j=1:3
		x(i,j,:) = (i+2)*(j+1)*linspace(0,1,21);
		for k=1:4
			y(i,1,:,k) = i+linspace(0,2,21)-k;
		end
	end
end
% Each of the functions y(i,j,:,k)is integrated wrt. each of the arguments x(l,m,:). The result
% is an array with dimensions size(x,1) X size(x,2) X size(y,1) X size(y,2) x 1 x size(y,4). 
% The third dimension of both arguments is collapsed due to the integration.
% The dimensions of x and y may be different excepted the length of the integrated dimension,
% which must of course be equal.
% Somehow there may be a numerical error, which apparently is smaller as when performing the
% normal trapz.
z = cumtrapz_fast_nDim(x,y,3);
% This is the same as a combination of 4 for-loops:
for i=1:2
	for j=1:3
		for l=1:2
			for k=1:4%
				%   x(1)x(2)y(1)y(2)y(3)y(4)
				zComp(i,j,l,1,:,k) = cumtrapz(squeeze(x(i,j,:)),y(l,1,:,k),3);
			end
		end
	end
end
disp(['3.3.: max(z-zComp)= ' num2str(max(z(:)-zComp(:)))]);

