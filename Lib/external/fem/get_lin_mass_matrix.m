function m_mat = get_lin_mass_matrix(zdisc)
%
%	m_mat = get_lin_mass_matrix(zdisc)
%		computes the Mass Matrix m_mat for the FEM with discretization
%		nodes zdisc and linear basis functions.
%		Taken from function MATFEM
%
%
%-------------
% element size
%-------------
xe=zdisc;
ne	= length(xe) - 1;
if ne <= 0
m_mat = 0;
return
end
for l=1:ne
h(l) = xe(l+1)-xe(l);
end

%==========================================================================
%       COMPUTATION OF THE MASS MATRIX

%----------------------------------
% initialize the tridiagonal matrix
%----------------------------------
at = zeros(ne+1,1);
bt = zeros(ne+1,1);
ct = zeros(ne+1,1);

%----------------------------------
% loop over the first ne-1 elements
%----------------------------------
for l=1:ne

B11 = h(l)/3.0; B12 = 0.5*B11;  % mass matrix
B21 = B12;      B22 = B11;

at(l)     = at(l) + B11;        % tridiagonal matrix components
bt(l)     = bt(l) + B12;
ct(l)     = ct(l) + B21;
at(l+1)   = at(l+1) + B22;

end

%------------------------
% last element is special
%------------------------
B11 = h(ne)/3.0; B12 = 0.5*B11;

at(ne+1) = B11;

% Construction of the mass matrix from the diagonal vectors
m_mat = diag(at) + diag(bt(1:end-1),1) + diag(ct(1:end-1),-1);