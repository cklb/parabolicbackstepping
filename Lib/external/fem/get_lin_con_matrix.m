function c_mat = get_lin_con_matrix(zdisc)
%
%	c_mat = get_lin_con_matrix(zdisc)
%		computes the Convection Matrix c_mat for the FEM with discretization
%		nodes zdisc and linear basis functions.
%		Taken from function MATFEM
%
%==================================================================
%       COMPUTATION OF THE CONVECTION MATRIX
%-------------
% element size
%-------------
xe=zdisc;
ne	= length(xe) - 1;
for l=1:ne
	h(l) = xe(l+1)-xe(l);
end
%-----------
% initialize
%-----------
at = zeros(ne+1,1);
bt = zeros(ne+1,1);
ct = zeros(ne+1,1);

cf = 0.5;

C11 = -cf; C12 = cf;        % Convection matrix
C21 = -cf; C22 = cf;

%------------------------
% loop over ne elements
%------------------------
for l=1:ne
	at(l)   = at(l) + C11;  % tridiagonal matrix components
	bt(l)   = bt(l) + C12;
	ct(l)   = ct(l) + C21;
	at(l+1) = at(l+1) + C22;
end

%-------------
% last element
%-------------
at(ne+1) = -C11;

% Construction of the convection matrix from the diagonal vectors
c_mat   = diag(at) + diag(bt(1:end-1),1) + diag(ct(1:end-1),-1);
clear at bt ct C11 C12 C21 C22
%==================================================================
