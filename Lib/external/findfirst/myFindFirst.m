function indVec = myFindFirst(in)
%UNTITLED2 Summary of this function goes here
%   Detailed explanation goes here

indVec = zeros(1,size(in,2));
for j=1:size(in,2)
	fnd = find(in(:,j)>=0.5,1);
	if ~isempty(fnd)
		indVec(j) = fnd;
	end
end

end

