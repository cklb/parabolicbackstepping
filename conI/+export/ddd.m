classdef ddd < export.Data
    %DDD export 3D data
    %   
    
    properties
        x;
        y;
        z;
        xRange;
        yRange;
    end

    properties(Dependent)
       out; 
    end
    
    methods
        
        function obj = ddd(varargin)
           obj@export.Data('N', 60); 
           
           for arg=1:2:length(varargin)
               obj.(varargin{arg}) = varargin{arg + 1}; 
           end
           
        end
        
        function xR = get.xRange(obj)
           if isempty(obj.xRange)
              xR = [obj.x(1), obj.x(end)]; 
           else
               xR = obj.xRange;
           end
        end
        
        function yR = get.yRange(obj)
           if isempty(obj.yRange)
              yR = [obj.y(1), obj.y(end)];
           else
               yR = obj.yRange;
           end
        end
        
        function set.z(obj, z)
           obj.z = squeeze(z); 
        end
        
        function out = get.out(obj)
            o.x = linspace(obj.xRange(1), obj.xRange(2), obj.N);
            o.y = linspace(obj.yRange(1), obj.yRange(2), obj.N);
            [o.x, o.y] = meshgrid(o.x, o.y);
            [grid.x, grid.y] = meshgrid(obj.x, obj.y);
            o.z = interp2(grid.x, grid.y, obj.z', o.x, o.y);
            out = [o.x(:), o.y(:), o.z(:)];            
        end

        function i = isempty(obj)
            i = isempty(obj.x) || isempty(obj.y) || isempty(obj.z);
        end
        
    end

    methods( Access = protected)
       function innerexport(obj)
            o = obj.out;
            save(obj.path, 'o',  '-ascii', '-tabs')
        end         
    end
    
    
end

