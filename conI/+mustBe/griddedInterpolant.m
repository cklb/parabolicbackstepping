function griddedInterpolant(A)
%mustBe.griddedInterpolant Validate that A is an griddedInterpolant object
%or empty.
%
% history:
%   -   created on 14.09.2018 by Jakob Gabriel

   if ~isempty(A) && ~isa(A, 'griddedInterpolant') 
      error('Value assigned to Data property must be a griddedInterpolant.')
   end
end