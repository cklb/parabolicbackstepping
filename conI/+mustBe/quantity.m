function quantity(A)
%mustBe.quantity Validate that A is quatratic Matrix or else throw 
% an error.
%
   if ~isempty(A) && ~isa(A, 'quantity.Discrete') 
      error('Value assigned to Data property must be a quantity.Discrete.')
   end
end