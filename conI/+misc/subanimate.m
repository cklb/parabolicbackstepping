function subanimate(x, varargin)
%TODO

assert(isa(x, 'quantity.Discrete'))
myParser = misc.Parser();
myParser.addParameter('frames', 201);
myParser.addParameter('timeGridName', 't');
myParser.addParameter('spatialGridName', 'z');
myParser.addParameter('compareWith', []);
myParser.parse(varargin{:});
lr.t = x.gridOf(myParser.Results.timeGridName);
lr.z = x.gridOf(myParser.Results.spatialGridName);
lr.x = x.on({lr.t, lr.z}, {'t', 'z'});

doCompare = ~isempty(myParser.Results.compareWith);
if doCompare
	assert(isequal(size(myParser.Results.compareWith), size(x)) ...
		&& isa(myParser.Results.compareWith, 'quantity.Discrete'));
	lr.xReference = myParser.Results.compareWith.on({lr.t, lr.z}, {'t', 'z'});
end

figure();
subplotCell = cell(size(x));
for it = 1 : numel(x)
	subplotCell{it}.s = subplot(numel(x),1,it);
	subplotCell{it}.x = animatedline('Color','[0.00000,0.44700,0.74100]','LineWidth',2);
	if doCompare
		subplotCell{it}.xReference = animatedline('LineStyle', '--', ...
			'Color','[0.8500 0.3250 0.0980]','LineWidth', 1);
	end
	grid on;
	axis([0,1,min(-1, min(lr.x(:))), max(1, max(lr.x(:)))]);
	xlabel(['$$', myParser.Results.spatialGridName, '$$'], 'Interpreter','latex');
	ylabel(['$$', x(1).name, '_', num2str(it), '(', myParser.Results.spatialGridName, ...
		', ', myParser.Results.timeGridName, ')$$'], 'Interpreter','latex');
	aTemp = gca();
	aTemp.TickLabelInterpreter = 'latex';
end

for k = 1 : round(numel(lr.t)/myParser.Results.frames) : numel(lr.t)
	for it = 1 : numel(x)
		subplotCell{it}.s;
		clearpoints(subplotCell{it}.x)
		addpoints(subplotCell{it}.x, lr.z, squeeze(lr.x(k, :, it)));
		if doCompare
			clearpoints(subplotCell{it}.xReference)
			addpoints(subplotCell{it}.xReference, lr.z, squeeze(lr.xReference(k, :, it)));
		end
		drawnow
		title(subplotCell{1}.s, ...
			['$$t = ', num2str(lr.t(k), '%10.2f\n'), '$$ of $$', num2str(lr.t(end), '%10.2f\n'), ...
			'$$'], 'Interpreter','latex');
	end
end

end % subanimate()