classdef Parser < inputParser
	properties (Dependent = true)
		UnmatchedNameValuePair cell;
		ResultsNameValuePair cell;
	end % properties (Dependent = true)
		
        
    methods
        function obj = Parser()
            obj = obj@inputParser();
            
            % set default properties
            obj.KeepUnmatched = true;
            obj.CaseSensitive = true;
            obj.PartialMatching = false;
		end % Parser()
        
        function i = isDefault(obj, name)
            i = any(ismember(obj.UsingDefaults(:), {name}));
		end % isDefault()
		
		function parse2ws(obj, varargin)
			% PARSE parses the name-value-pair list
			%	parse(obj, arglist) overloads the default function of the
			%	inputParser, so that the parsed arguments are directly
			%	available in the workspace of the caller of this function.
			obj.parse(varargin{:});
			
			% assign all parsed variables to the workspace of the caller of
			% this function
			structureFields = fieldnames(obj.Results);
			for k=1:length(structureFields)
			   assignin('caller', structureFields{k}, obj.Results.(structureFields{k}));
			end
		end % parse2ws()
		
		%% I don't know why, but it only works for public properties
		function [obj, myParser] = parse2obj(myParser, obj, varargin)
		% PARSE2OBJ is a method that parses the parser objParser
		% with varargin, and sets all matching results to the matching
		% parameters of the object objTarget. This is useful for 
		% constructors of classes.
			myParser.parse(varargin{:});
			objPropertyNames = properties(obj);
			myParserResultNames = fieldnames(myParser.Results);
			for it = 1:numel(myParserResultNames)
				if any(strcmp(myParserResultNames{it}, objPropertyNames))
					evalin('caller', ['[obj.', myParserResultNames{it}, ']', ...
						'= myParser.Results.', myParserResultNames{it} ';']);
				end
			end
		end % parse2obj()
		
		function unmatchedWarning(obj)
			if ~isempty(fields(obj.Unmatched))
				warning('Unmatched parameters: ');
                disp(obj.Unmatched)
			end
		end % unmatchedWarning()
		
		function myUnmatchedNameValuePair = get.UnmatchedNameValuePair(obj)
			myUnmatchedNameValuePair = misc.struct2namevaluepair(obj.Unmatched);
		end % get.UnmatchedNameValuePair()
		
		function myResultsNameValuePair = get.ResultsNameValuePair(obj)
			myResultsNameValuePair = misc.struct2namevaluepair(obj.Results);
		end % get.ResultsNameValuePair()
		
	end % methods
end % classdef Parser < inputParser