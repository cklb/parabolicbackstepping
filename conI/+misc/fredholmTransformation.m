function xOut = fredholmTransformation(xIn, fredholmKernel, signOfIntegralTerm)
%FREDHOLM_TRANSFORMATION calculates the fredholm-transform of x_in
%	x_out(z) = x_in(z) + signOfIntegralTerm ...
%						* int_0^1 fredholmKernel(z, zeta) x_in(zeta) dzeta
% fredholmKernel is sampled to spatial resolution of xIn.
%
% INPUT PARAMETERS:
% 	DOUBLE-ARRAY			  x_in : value to be transformed of format 
%									(spatialGrid, index, optional index)
% 	DOUBLE-ARRAY	fredholmKernel : kernel of transofmration of format
%									 (z-grid, zeta-grid, index, index)
%	INTEGER		signOfIntegralTerm : sign before integral, either +1 or -1
%
% OUTPUT PARAMETERS:
% 	DOUBLE-ARRAY			 x_out : transformed value of format 
%									(spatialGrid, index, optional index)

	%% Init and input check
	xSize = size(xIn);
	xOut = zeros(xSize);
	z.n = xSize(1);
	n = xSize(2);
	columns = size(xIn, 3); % can not be obtained from xSize if ndims(xSize) == 2
	z.grid = linspace(0, 1, z.n);
	z.gridKernel = linspace(0, 1, size(fredholmKernel, 1));
	kernelInterpolant = numeric.interpolant(...
			{z.gridKernel, z.gridKernel, 1:n, 1:n}, fredholmKernel, 'spline');
	kernelSampled = kernelInterpolant.evaluate(z.grid, z.grid, 1:n, 1:n);

	integrand = zeros(z.n, z.n, n, columns);
	for cIdx = 1:columns
		for zIdx = 1:z.n
			for it = 1:n 
				integrand(zIdx, :, it, cIdx) = ...
					sum(reshape(kernelSampled(zIdx, :, it, :), [z.n, n]) ...
													.* xIn(:, :, cIdx), 2);
			end
		end
	end
	integral = reshape(numeric.trapz_fast_nDim(z.grid, integrand, 2), [z.n, n, n]);
	xOut = xIn + signOfIntegralTerm * integral;
end
