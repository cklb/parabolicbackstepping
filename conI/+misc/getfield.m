function myValue = getfield(myStruct, myFieldName)
% misc.getfield returns data of a struct specified by its field name.
% In contrast to the matlab built-in function getfield(), this method misc.getfield
% can also read fields of field by specifying myFieldName with dots as seperator, for
% instance myFieldName = 'topLevel.lowerLevel.data', see example.
%%
% Example:
%		myStruct = misc.setfield(struct(), 'topLevel.lowerLevel.data', 42);
%		result = misc.getfield(myStruct, 'topLevel.lowerLevel.data')

myFieldNameSplitted = split(myFieldName, '.');
myValue = myStruct;
for it = 1 : numel(myFieldNameSplitted)
	if isfield(myValue, myFieldNameSplitted{it}) 
		myValue = myValue.(myFieldNameSplitted{it});
	else
		myValue = [];
		return;
	end
end
end % plot()