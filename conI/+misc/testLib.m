% testLib

% call all test functions to check functionallity of the Lib.

% 1.: check parabolic systems:
resultParabolic = runtests({'dps.parabolic.script.ppideTest', ...
                            'signals.test', ...
                            'dps.faultdiagnosis.test', ...
                            'dps.parabolic.faultdiagnosis.test'});

