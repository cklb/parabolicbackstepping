function [u] = squareSignals(t,changingPoints,gain)
% SQUARESIGNALS Function to create not periodic square signals
%
% Description:
% Creates an arbitrary discrete signal with jumps. T is the discrete time
% vector, CHANGINGPOINTS are the points at which the signalvalue should
% change immediatley and gain is the value for the signal between to
% points. First point is always zero. So the points in CHANGINGPOINTS has
% to be the end of a constant section.
%
% Inputs:
%     t                   Vector containing discrete time values
%     changeingPoints     Vector containing time values at which the
%                         signal should change its value
%     gain                Vector containing the magnitude of the signal
%                         at the corresponding points in changeingPoints
% Outputs:
%     u                   Resultant signal
%
% Example:
% -------------------------------------------------------------------------
% t = 0:0.01:10;
% point = [2 2.5 4 5 7 9.5];
% gain = [1 4 -3 2 1 0];
% u = misc.squareSignals(t,point,gain);
% plot(t,u);
% -------------------------------------------------------------------------
%
u=zeros(size(t));
t0=t(1);
for k=1:length(changingPoints)
    t1=changingPoints(k);
    u(t>=t0&t<=t1)=gain(k);
    t0=t1;
end
