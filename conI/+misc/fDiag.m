function funDiag = fDiag(fun,n,z)
% fDiag Create digaonal repetition of function
%
% Description:
% funDiag = fDiag(fun,n,z) repeats the function fun n times and places 
% the result in a diagonal matrix. Then it is evaluated at z. 
%
% Inputs:
%     fun         Function handle ( fun(z) must retrn at most a 2D array)
%     n           Number of repitions of fun in funDiag
%     z           Single point on which fun is evaluated
% Outputs:
%     funDiag     Diagonal matrix containing fun(z) on main diagonal
%
% Example:
% -------------------------------------------------------------------------
% f = @(z) z^2;
% z = 5;
% n = 4;
% funDiag(f,n,z)
% >> ans = [25  0  0  0
%            0 25  0  0
%            0  0 25  0
%            0  0  0 25]
% -------------------------------------------------------------------------

% created on 03.08.2018 by Simon Kerschbaum

res = fun(z);
siz = size(res);
if length(siz)>2
	error('The function must return at most a 2D array!')
end
rows=siz(1);
cols=siz(2);
funDiag = zeros(n*rows,n*cols);
for i=1:n
	funDiag((i-1)*rows+1:i*rows,(i-1)*cols+1:i*cols) = res;
end

end

