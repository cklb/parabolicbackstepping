function [namevaluepair] = struct2namevaluepair(s, removeEmptyEntries)
% STRUCT2NAMEVALUEPAIR converts struct to name value pairs
%
% Inputs:
%     s                   Struct containing information
% Outputs:
%     namevaluepair       Name value pairs in cell array
%
% Example:
% -------------------------------------------------------------------------
% s.a = 5;
% s.hallo = 1;
% misc.struct2namevaluepair(s)
% >> ans = {'a'}  {[5]}  {'hallo'}  {[1]}
% -------------------------------------------------------------------------
%

names = fieldnames(s);
values = struct2cell(s);

if nargin == 2 && removeEmptyEntries

	isEmpty = cellfun(@isempty, values);
	names = names(~isEmpty);
	values = values(~isEmpty);
	
end


namevaluepair = reshape({names{:}; values{:}}, 1, 2 * length(names));

end

