classdef Backstepping < handle & matlab.mixin.Copyable
%MISC.BACKSTEPPING is a container backstepping of kernels and further information about
%the backstepping transformation (sign, integral bounds, inverse kernel).
		
	properties (SetAccess = protected)
		kernel (:, :) quantity.Discrete;		% contains kernel
		kernelInverse (:, :) quantity.Discrete; % contains kernel of inverse transformation
		signOfIntegralTerm (1, 1);				% sign before integral of transformation
		signOfIntegralTermInverse (1, 1);		% sign before integral of inverse transformation
		integralBounds cell;					% bounds of integral of both transformations
	end % properties (SetAccess = protected)
	
	properties (Dependent = true)
		domainSelector (1, 1) quantity.Discrete;% quantity that is = 0 outside the
		%										% domain specified by integral bounds
		%										% and = 1 else.
	end % properties (Dependent = true)
	
	methods
		function obj = Backstepping(varargin)
			%% The misc.Backstepping constructor can be called in three ways using
			% name-value pairs for each property. In the following [] represent the
			% values that should be assigned for the properties speciefied as
			% char-arrays.
			% 1. With the backstepping kernel:
			%	misc.Backstepping(...
			%		'kernel', [], 'signOfIntegralTerm', +1, ...
			%		'integralBounds', []);
			% 2. With the inverse backstepping kernel:
			%	misc.Backstepping(...
			%		'kernelInverse', [], 'signOfIntegralTermInverse', +1, ...
			%		'integralBounds', []);
			% 3. With the both the backstepping kernel and its inverse:
			%	misc.Backstepping(...
			%		'kernel', [], 'signOfIntegralTerm', +1, ...
			%		'kernelInverse', [], 'signOfIntegralTermInverse', +1,
			%		'integralBounds', []);
			% In the cases 1 and 2, the other kernel (1: kernelInverse, 2. kernel)
			% can be calculated easily by calling the method invert()
			myParser = misc.Parser();
			myParser.addParameter('kernel', []);
			addParameter(myParser, 'kernelInverse', []);
			addParameter(myParser, 'signOfIntegralTerm', []);
			addParameter(myParser, 'signOfIntegralTermInverse', []);
			addParameter(myParser, 'integralBounds', []);
			myParser.parse(varargin{:});
			if ~isempty(fieldnames(myParser.Unmatched))
				warning('unmatched parameter in input parser');
			end
			assert(~isempty(myParser.Results.integralBounds), ...
				'integralBounds must be specified');
			
			if ~isempty(myParser.Results.kernel)
				assert(~isempty(myParser.Results.signOfIntegralTerm), ...
					'signOfIntegralTerm must be specified');
			end
			if ~isempty(myParser.Results.kernelInverse)
				assert(~isempty(myParser.Results.signOfIntegralTermInverse), ...
					'signOfIntegralTermInverse must be specified');
			end
			
			propertyNames = fieldnames(myParser.Results);
			for it = 1 : numel(propertyNames)
				if ~isempty(myParser.Results.(propertyNames{it}))
					obj.(propertyNames{it}) = myParser.Results.(propertyNames{it});
				end
			end
		end % Backstepping() Constructor
		
		function xOut = transform(obj, xIn)
			% forward backstepping transformation:
			% xOut = xIn + obj.signOfIntegralTerm * ...
			%			int_integralBounds{1}^integralBounds{2}
			%				obj.kernel(z, zeta) * xIn(zeta) dzeta
			assert(~isempty(obj.kernel), 'no kernel-property specified'); 
			assert(size(xIn, 1) == size(obj.kernel, 2), 'xIn does not fit to kernel');
			if obj(1).signOfIntegralTerm == 1
				xOut = xIn + ...
					cumInt(obj(1).kernel * subs(xIn, {'z'}, {'zeta'}), ...
						'zeta', obj(1).integralBounds{:});
			elseif obj(1).signOfIntegralTerm == -1
				xOut = xIn - ...
					cumInt(obj(1).kernel * subs(xIn, {'z'}, {'zeta'}), ...
						'zeta', obj(1).integralBounds{:});
			else
				error('signOfIntegralTerm is neither -1 nor +1')
			end
		end % transform
		
		function xOut = transformInverse(obj, xIn)
			% inverse backstepping transformation:
			% xOut = xIn + obj.signOfIntegralTermInverse * ...
			%			int_integralBounds{1}^integralBounds{2}
			%				obj.kernelInverse(z, zeta) * xIn(zeta) dzeta
			assert(~isempty(obj.kernelInverse), 'no kernelInverse-property specified')
			assert(size(xIn, 1) == size(obj.kernel, 2), 'xIn does not fit to kernelInverse');
			if obj(1).signOfIntegralTermInverse == 1
				xOut = xIn + ...
					cumInt(obj(1).kernelInverse * subs(xIn, {'z'}, {'zeta'}), ...
						'zeta', obj(1).integralBounds{:});
			elseif obj(1).signOfIntegralTermInverse == -1
				xOut = xIn - ...
					cumInt(obj(1).kernelInverse * subs(xIn, {'z'}, {'zeta'}), ...
						'zeta', obj(1).integralBounds{:});
			else
				error('signOfIntegralTermInverse is neither -1 nor +1')
			end
		end % transformInverse()
		
		function myError = verifyInversion(obj, AbsTol)
			% verify that kernel and kernelInverse fit together by calculating an
			% numerical example
			if nargin < 2
				AbsTol = 0.001;
			end
			xOriginal = quantity.Discrete(...
				ones(101, size(obj.kernel, 1)), ...
				'grid', {linspace(0, 1, 101)}, 'gridName', {'z'});
			xTilde = obj.transform(xOriginal);
			xOriginalAgain = obj.transformInverse(xTilde);
			myError = max(median(abs(xOriginal-xOriginalAgain)));
			if myError >= AbsTol
				warning( ...
				['in verification of kernel', obj.kernel(1).name, ', ', ...
				obj.kernelInverse(1).name, ' occured an error of ', num2str(myError)]);
			end
		end % verifyInversion()
		
		function obj = invert(obj, newIntegralSign)
			% In this method the kernel is inverted using the receprocity relation.
			% If the property .kernel is set, then kernelInverse is calculated and
			% set. Elseif .kernelInverse is set, then kernel is calculated and set.
			% This method uses the receprocity relation and solves the integral
			% equation using the method of successive approximations.
			
			% init data
			if ~isempty(obj.kernel) && isempty(obj.kernelInverse)
				knownKernelWithSign = obj.signOfIntegralTerm * obj.kernel;
				newKernelName = [obj.kernel(1).name, '_{I}'];
				
			elseif isempty(obj.kernel) && ~isempty(obj.kernelInverse)
				knownKernelWithSign = obj.signOfIntegralTermInverse * obj.kernelInverse;
				newKernelName = strrep(obj.kernelInverse(1).name, '_{I}', '');
				newKernelName = strrep(newKernelName, '_I', '');
				
			else
				myError = verifyInversion(obj);
				error('both kernels are already known');
			end
			newKernel = quantity.Discrete(knownKernelWithSign);
			
			if isequal(obj.integralBounds, {0, 'z'})
				signOfReceprocityIntegral = +1;
			elseif isequal(obj.integralBounds, {'z', 1})
				signOfReceprocityIntegral = -1;
			else
				error('invalid integral bounds');
			end
			
			% successive approximation
			progress = misc.ProgressBar('name', ['Successive Calculation of inverse ', ...
				'of ', knownKernelWithSign(1).name, ': '], ...
				'steps', 50, 'terminalValue', 50, 'printAbsolutProgress', true);
			progress.start();
			for it = 1 : progress.steps
				newKernelLastIteration = newKernel;
				newKernel = - knownKernelWithSign + signOfReceprocityIntegral * ...
					cumInt(knownKernelWithSign.subs('zeta', 'eta') * newKernel.subs('z', 'eta'), ...
					'eta', 'z', 'zeta');

				changeInThisIteration = MAX(abs(newKernel-newKernelLastIteration));
				if changeInThisIteration < 1e-5
					break;
				end
				progress.raise(it, 'addMessage', ...
					['change = ', num2str(changeInThisIteration)]);
			end
			newKernel = newIntegralSign * newKernel;
			progress.stop();
			[newKernel.name] = deal(newKernelName);
			
			% set result
			if ~isempty(obj.kernel) && isempty(obj.kernelInverse)
				obj.kernelInverse = newKernel;
				obj.signOfIntegralTermInverse = newIntegralSign;
				
			elseif isempty(obj.kernel) && ~isempty(obj.kernelInverse)
				obj.kernel = newKernel;
				obj.signOfIntegralTerm = newIntegralSign;
				
			end
		end % invert()
		
		function [K_dzeta, K_dz] = gradient(obj, type, myGrid)
			% gradient() calculates the first order spatial derivatives of the kernel
			% specified by type on the grid myGrid. The method
			% numeric.gradient_on_2d_triangular_domain is called which uses the
			% matlab builtin function gradient() and extrapolation for the diagonal
			% boundary z=zeta.
			%
			% INPUT PARAMETER
			%	type	 	'kernel' or 'kernelInverse'
			%	myGrid		vector of discrete grid on which derivatives are
			%				calcualted
			% OUTPUT PARAMETER
			%	K_dzeta		spatial derivative in zeta direction as quantity.Discrete
			%	K_dz		spatial derivative in z direction as quantity.Discrete
						
			% set parameter domain which must be either 'zeta<=z' or 'zeta>=z'
			if isequal(obj.integralBounds, {0, 'z'})
				domain = 'zeta<=z';
			elseif isequal(obj.integralBounds, {'z', 1})
				domain = 'zeta>=z';
			end
			
			% select kernel according to input
			if strcmp(type, 'kernelInverse')
				thisKernel = obj.kernelInverse;
			elseif strcmp(type, 'kernel')
				thisKernel = obj.kernel;
			else
				error("type must be either 'kernel' or 'kernelInverse'");
			end
			
			% select default grid
			if nargin < 3
				myGrid = thisKernel(1).grid{1};
			end
			spacing = myGrid(2) - myGrid(1);
			assert(numeric.near(diff(myGrid), spacing, 1e-6), ...
				'grid must be homogenious');
			
			% calculate gradient numerically
			[K_dzetaMat, K_dzMat] = numeric.gradient_on_2d_triangular_domain(...
				thisKernel.on({myGrid, myGrid}, {'z', 'zeta'}), spacing, domain);
			
			% create quantities as output parameters
			K_dzeta = quantity.Discrete(K_dzetaMat, 'grid', {myGrid, myGrid}, ...
				'gridName', {'z', 'zeta'}, 'name', ['d_{zeta}', thisKernel(1).name]);
			
			K_dz = quantity.Discrete(K_dzMat, 'grid', {myGrid, myGrid}, ...
				'gridName', {'z', 'zeta'}, 'name', ['d_{z}', thisKernel(1).name]);
		end % gradient()
		
		function domainSelector = get.domainSelector(obj)
			% domainSelector is a quantity that is = 0 outside the spatial domain 
			% specified by this objects integral bounds and = 1 else. This can be
			% used to set values that are outside the domain to zero (by
			% multiplication with domainSelector) in order to ignore them in
			% calculations.
			
			% get grid
			if ~isempty(obj.kernel) && ~isempty(obj.kernelInverse)
				myGrid = obj.kernel.grid;
				if numel(myGrid{1}) < numel(obj.kernelInverse.grid{1})
					myGrid = obj.kernelInverse.grid;
				end
			elseif ~isempty(obj.kernel)
				myGrid = obj.kernel.grid;
			elseif ~isempty(obj.kernelInverse) 
				myGrid = obj.kernelInverse.grid;
			end
			
			% get domainSelector depending on ojb.integralBounds
			syms z zeta
			if isequal(obj.integralBounds, {0, 'z'})
				domainSelector = quantity.Discrete(quantity.Symbolic(zeta<=z, ...
					'grid', obj.kernel(1).grid, ...
					'variable', {z, zeta}, 'name', ''));
				
			elseif isequal(obj.integralBounds, {'z', 1})
				domainSelector = quantity.Discrete(quantity.Symbolic(zeta>=z, ...
					'grid', obj.kernel(1).grid, ...
					'variable', {z, zeta}, 'name', ''));
			end
			
		end % get.domainSelector()
	end % methods
	
	methods (Access = protected)
		% Override copyElement method:
		function cpObj = copyElement(obj)
			% Make a shallow copy of all properties
			cpObj = copyElement@matlab.mixin.Copyable(obj);
			cpObj.input = copy(obj.kernel);
			cpObj.input = copy(obj.kernelInverse);
		end
	end % methods (Access = protected)
	
end

