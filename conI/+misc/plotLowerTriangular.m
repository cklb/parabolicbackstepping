function varargout = plotLowerTriangular(zdisc,val,varargin)
% plotLowerTriangular surf of a lower triangular area based on triangular and quadratic
% polygons.
%
% plotLowerTriangular(zdisc,val) 
%   plots the function val defined over (zdisc,zdisc) over the lower triangular 
%   val(z,zeta<=z).
%
% h = plotLowerTriangular(...) returns the corresponding plot handle.
%
% h = plotLowerTriangular(varargin) passes varargin to the used patch function.


zGes = [];
zetaGes = [];
kGes = [];
F = double.empty(0,4);
ndisc=length(zdisc);
for zetaIdx = 1:ndisc
	% create all verices in one turn:
	zLow = zdisc(zetaIdx:end);
	zetaLow = repmat(zdisc(zetaIdx),1,ndisc-zetaIdx+1);
	kLow = val(zetaIdx:end,zetaIdx).';
	zGes = [zGes zLow];
	zetaGes = [zetaGes zetaLow];
	kGes = [kGes kLow];
end
startingIdx = ones(1,ndisc-1);
startingIdx(1) = 1;
for zetaIdx=2:ndisc
	startingIdx(zetaIdx) = startingIdx(zetaIdx-1)+ndisc-(zetaIdx-2);
end
for zetaIdx=1:ndisc-1
	% create triangle to connect:
	index1 = startingIdx(zetaIdx); % left lower corner of triangle
	index2 = startingIdx(zetaIdx)+1;% left right corner of triangle
	index3 = startingIdx(zetaIdx+1); % upper corner of triangle, leftmost point in next row
	for zIdx = zetaIdx+1:ndisc-1
		% create rectangles to connect
		idxLow(zIdx,:) = startingIdx(zetaIdx)+zIdx-zetaIdx:startingIdx(zetaIdx)+zIdx-zetaIdx+1;
		idxHigh(zIdx,:) = startingIdx(zetaIdx+1)+zIdx-(zetaIdx+1):startingIdx(zetaIdx+1)+zIdx-(zetaIdx);
	end
	F = [F;index1 index2 index3 index1]; % all the faces, that is, vertices (points) to be connected
	for zIdx=zetaIdx+1:ndisc-1
		F = [F;idxLow(zIdx,:) fliplr(idxHigh(zIdx,:))];
	end
end
% Attention: when CData is given as column vector, is assumes RGB values which need to be
% inside 0,1, leading to an error.
	h = patch('Faces',F,'Vertices',[zGes;zetaGes;kGes].',...
		'FaceVertexCData',kGes.','FaceColor','interp',varargin{:});
if nargout>0
	varargout{1} = h;
end



end % function