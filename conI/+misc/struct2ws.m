function [ ] = struct2ws( s )
% STRUCT2WS assigns all the struct fields as local variables in the
% workspace
%
% Description:
% [ ] = struct2ws( s )
% All the values of the fields in the structure *s* are assigned to local
% variables in the current workspace using the fieldname as variable
% name.
%
% Inputs:
%     s       Input struct
%
% Example:
% -------------------------------------------------------------------------
% import misc.*
% s = struct('field1', 1, 'field2', 2);
% struct2ws(s)
% -------------------------------------------------------------------------

structureFields = fieldnames(s);

for k=1:length(structureFields)
   assignin('caller', structureFields{k}, s.(structureFields{k}));
end

end