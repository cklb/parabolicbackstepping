function itIs = isfield(myStruct, myFieldName)
% misc.isfield returns a logical if a field specified by myFieldName exists.
% In contrast to the matlab built-in function isfield(), this method misc.isfield
% can also read fields of field by specifying myFieldName with dots as seperator, for
% instance myFieldName = 'topLevel.lowerLevel.data', see example.
%%
% Example:
%		myStruct = misc.setfield(struct(), 'topLevel.lowerLevel.data', 42);
%		result = misc.isfield(myStruct, 'topLevel.lowerLevel.data')

myFieldNameSplitted = split(myFieldName, '.');
myExplorer = myStruct;
itIs = true;
for it = 1 : numel(myFieldNameSplitted)
	if isfield(myExplorer, myFieldNameSplitted{it})
		myExplorer = myExplorer.(myFieldNameSplitted{it});
	else
		itIs = false;
		break;
	end
end
end % misc.isfield()