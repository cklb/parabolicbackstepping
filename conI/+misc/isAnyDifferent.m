function result = isAnyDifferent(obj1, obj2, parlist)
% isAnyDifferent check if parameters differ in 2 objects
%
% Description:
% result = isAnyDifferent( obj1,obj2,parlist )
% Returns a string array containing the elements of parlist that have
% different values in obj1 and obj2
%
% Inputs:
%     obj1        First object of an class
%     obj2        Second object of an class (could be different class
%                 than of obj1)
%     parlist     List of parameters to be checked
%					default: union(properties(obj1), properties(obj2))
% Outputs:
%     result      String containing parameters of parlist that differ
%
% Example:
% -------------------------------------------------------------------------
% p1 = dps.parabolic.system.ppide_sys('l',1);
% p2 = dps.parabolic.system.ppide_sys('l',2);
% isAnyDifferent(p1,p2,{'n','l','ndisc'})
% >> ans = 'l'
% -------------------------------------------------------------------------

import misc.*
if nargin < 3
	parlist = union(properties(obj1), properties(obj2));
end
result=[];
for idx=1:length(parlist)
	if isobject(obj1.(parlist{idx}))
		if isobject(obj2.(parlist{idx}))
			pars = properties(obj1.(parlist{idx}));
			for par_idx = 1:length(pars)
				if ~isequal(size(obj1.(parlist{idx})), size(obj2.(parlist{idx})))
					result = add2result(result, [parlist{idx}, '.', pars{par_idx}]);
				else
					for numelPar = 1 : numel(obj1.(parlist{idx}))
						if ~isequalF(...
								obj1.(parlist{idx})(numelPar).(pars{par_idx}), ...
								obj2.(parlist{idx})(numelPar).(pars{par_idx}))
							result = add2result(result, [parlist{idx}, '.', pars{par_idx}]);
							break;
						end
					end
				end
			end
		else
			result = add2result(result, parlist{idx});
		end
	else
		if ~isequalF(obj1.(parlist{idx}),obj2.(parlist{idx}))
			result = add2result(result, parlist{idx});
		end
	end
end % for idx=1:length(parlist)
end % function misc.isAnyDifferent()

function result = add2result(result, propertyName)
	if isempty(result) % kein Komma!
		result = propertyName;
	else
		result = [result, ', ', propertyName];
	end
end % add2result
