function [] = plotEigenvalues(A)
%PLOT EIGENVALUES plot eigenvalues in complex plane
%	plotEigenvalues(A) plots the eigenvalues of matrix A in the complex
%	plane.

e = eig(A);

plot(real(e), imag(e), 'r*');
xlabel('real')
ylabel('imag')

end

