function x = makecolumnvector(x)
% X = MAKECOLUMNVECTOR(X) makes x to be a column vector.
%
% Description:
% x = makecolumnvector(x)
% If x is \in (1 x n) it returns x', otherwise, x it self.
% If x is matrix it will be transposed, so that length of rows is more 
% than the length of columns.
%
% Example:
% -------------------------------------------------------------------------
% X = ones(2, 100);
% size(X)
% >> ans = [2 100]
% Y = misc.makecolumnvector(X);
% size(Y)
% >> ans = [100 2]
% -------------------------------------------------------------------------
%
if size(x,1) < size(x,2)
    x=x';
end
