function [X, W, numerator] = solveGenericBvpFirstOrder(S, L, A, A0, Q0, B, Cx, F, varargin)
% MISC.SOLVEGENERICBVPFIRSTORDER solve a generic matrix boundary value problem (BVP)
% with 1st order derivatives.
%
% [X, W] = misc.solveGenericBvpFirstOrder(S, L, A, A0, C0, B, Cx, F, ...
%											'Cw', Cw, 'C', C, 'D', D, 'E', E)
% returns the solution of the BVP
%	L(z) X(z)' + A0(z) X(0) - X(z) S = A(z)
%			   (E2.' - Q0 E1.') X(0) = B
%						Cx[X] + Cw W = F
%						   C W - W S = D + E E1.' X(0)
% [X] = solveGenericBvpFirstOrder(S, L, A, C0, B, Cx, F)
% returns the solution of the BVP
%	L(z) X(z)' + A0(z) X(0) - X(z) S = A(z)
%			   (E2.' - Q0 E1.') X(0) = B
%							   Cx[X] = F
%
% The parameter L, A may be constant matrices or quantity.Discrete.
% Meanwhile, S, Q0, B, Cw, F, C, E are constant matrices and Cx is a dps.Output 
% object. The optional parameter Cw, C, D, E must be given as a name-value-pair.

% input checks
myParser = misc.Parser();
myParser.addRequired('S', @(v) isnumeric(v) && ismatrix(v));
myParser.addRequired('L', @(v) (isnumeric(v) && ismatrix(v)) || isa(v, 'quantity.Discrete'));
myParser.addRequired('A', @(v) (isnumeric(v) && ismatrix(v)) || isa(v, 'quantity.Discrete'));
myParser.addRequired('A0', @(v) (isnumeric(v) && ismatrix(v)) || isa(v, 'quantity.Discrete'));
myParser.addRequired('Q0', @(v) isnumeric(v) && ismatrix(v));
myParser.addRequired('B', @(v) isnumeric(v) && ismatrix(v));
myParser.addRequired('Cx', @(v) isa(v, 'dps.Output'));
myParser.addRequired('F', @(v) isnumeric(v) && ismatrix(v));
myParser.addParameter('Cw', [], @(v) isnumeric(v) && ismatrix(v));
myParser.addParameter('C', [], @(v) isnumeric(v) && ismatrix(v));
myParser.addParameter('D', [], @(v) isnumeric(v) && ismatrix(v));
myParser.addParameter('E', [], @(v) isnumeric(v) && ismatrix(v));
myParser.parse2ws(S, L, A, A0, Q0, B, Cx, F, varargin{:});

% Check if W has to be calculated or not
if any(strcmp(myParser.UsingDefaults, 'Cw')) ...
		|| any(strcmp(myParser.UsingDefaults, 'C')) ... 
		|| any(strcmp(myParser.UsingDefaults, 'D')) ...
		|| any(strcmp(myParser.UsingDefaults, 'W'))
	calculateW = false;
else
	calculateW = true;
end


L0 = L.at(0);
I = eye(size(L));
E1 = I(:, diag(L0)>0);
E2 = I(:, diag(L0)<0);
% get jordan chains
[V, J, lengthJb] = misc.jordan_complete(S);
mu = diag(J);
numJb = numel(lengthJb);

X_ik = cell(1, numel(mu));
W_ik = cell(1, numel(mu));
numerator = cell(1, numJb);
for it = 1 : numJb
	my = mu(sum(lengthJb(1:(it-1)))+1);
	Psi = getPsi(L, my);
	%M = Psi(z, 0, my) * (E1 + E2 * Q0) ...
	%	+ int_0^z Psi(z, zeta, my) inv(L(zeta)) * A0(zeta) dzeta
	M = Psi.subs('zeta', 0) * (E1 + E2 * Q0) ...
		+ cumInt(Psi * subs(inv(L) * A0, 'z', 'zeta'), 'zeta', 0, 'z');
	numerator{it} = Cx.out(M);
	if calculateW
		numerator{it} = numerator{it} - Cw * inv(my * eye(size(C)) - C) * E;
	end
	for kt = 1 : lengthJb(it)
		columnOfV = sum(lengthJb(1:(it-1)))+kt;
		v_ik = V(:, columnOfV);
		A_ik = A * v_ik;
		B_ik = B * v_ik;
		if calculateW
			D_ik = D * v_ik;
		end
		F_ik = F * v_ik;
		
		if kt > 1
			X_ikMinus1 = X_ik{columnOfV-1};
			W_ikMinus1 = W_ik{columnOfV-1};
		else
			X_ikMinus1 = zeros(size(L, 1), 1);
			W_ikMinus1 = zeros(size(Cw, 2), 1);
		end
		% m_ik = Psi(z, 0, my) E2 B_ik ...
		%		+ int_0^z Psi(z, zeta, my) L^-1(zeta) ...
		%				( X_i(k-1)(zeta) - A_ik(zeta) )	dzeta
		m_ik = Psi.subs('zeta', 0) * E2 * B_ik ...
				+ cumInt(Psi * subs(inv(L) * (X_ikMinus1 + A_ik), 'z', 'zeta'), ...
					'zeta', 0, 'z');
		
		E1TX_ik0 = inv(numerator{it}) * (F_ik - Cx.out(m_ik));
		if calculateW
			E1TX_ik0 = E1TX_ik0 + inv(numerator{it}) * ( ...
					Cw * inv(my * eye(size(C)) - C) * (W_ikMinus1 + D_ik));
			W_ik{columnOfV} = inv(my * eye(size(C)) - C) ...
				* (- E * E1TX_ik0 - W_ikMinus1 - D_ik);
		end
		X_ik{columnOfV} = M * E1TX_ik0 + m_ik;
	end % for kt = 1 : lenghtJb(it)
end  % for it = 1 : numJb
X = horzcat(X_ik{:}) / V;
if calculateW
	W = horzcat(W_ik{:}) / V;
else
	W = [];
end
end % function solveGenericBvpFirstOrder()

function Psi = getPsi(L, my)
	% Psi = diag(exp(s * (phi_1(z) - phi_1(zeta), ...
	%				s * (phi_2(z) - phi_2(zeta), ...
	%							...	
	%				s * (phi_n(z) - phi_n(zeta)))
	Phi = getPhi(L);
	Psi = vec2diag(exp(my * (Phi - Phi.subs('z', 'zeta'))));
	Psi.setName('Psi');
end % Psi(obj)

function Phi = getPhi(L)
	% phi_i = int_0^z (1 / L_i(zeta)) dzeta
	% Phi = [phi_1; phi_2; ... ; phi_n]
	Phi = cumInt(L.diag2vec(), 'z', 0, 'z');
	Phi.setName('Phi');
end % get.Phi(obj)