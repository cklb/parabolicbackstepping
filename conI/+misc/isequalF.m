function result = isequalF(a, b)
% isequalF overload isequal
% Use as isequal, instead that anonymous functions are discretized,
% evaluated and compared at the discretization points.
%
% Inputs:
%     a       Value or function
%     b       Value or function
% Outputs:
%     result  TRUE or FALSE value depending if a and b are equal or not
%
% Example:
% -------------------------------------------------------------------------
% f1 = @(z) z^2;
% f2 = @(x) x^2;
% isequalF(f1,f2)
% >> ans = 1          (isequal(f1,f2) would return 0)
% -------------------------------------------------------------------------

% Created on 18.05.2018 by Simon Kerschbaum
import misc.*
result=true;
if isobject(a) && isobject(b)
	if (numel(a) == 0) && (numel(a) == numel(b))
		result = true;
	elseif ~isequal(size(a), size(b))
		result = false;
	else
		props = union(properties(a), properties(b));
		for jt = 1:length(props)
			try
				% it may happen that matlab cannot acces a changed function anymore. Thus try
				% catch is required!
				if ~isequalF({a.(props{jt})}, {b.(props{jt})})
					result=false;
				end
			catch
				result=false;
				return
			end
		end
	end
elseif iscell(a)
	if iscell(b)
		a_temp = a(:);
		b_temp = b(:);
		if length(a_temp) ~= length(b_temp)
			result = false;
			return
		else
			for jt=1:length(a_temp)
				if ~isequalF(a_temp{jt},b_temp{jt})
					result=false;
					return
				end				
			end
		end
	else
		result=false;
		return
	end
elseif isa(a,'function_handle')
	if isa(b,'function_handle')
		if nargin(a)==1 && nargin(b)==1
			% in that case, the name of the argument shall not matter.
			zdisc = linspace(0,5,151);
			% it may happen that matlab cannot acces a changed function anymore. Thus try
			% catch is required!
			try
				a_disc = eval_pointwise(a,zdisc);
				b_disc = eval_pointwise(b,zdisc);
			catch
				result=false;
				return
			end
			if (numel(a_disc) ~= numel(b_disc)) || any(a_disc(:)-b_disc(:))
				result=false;
				return
			end
		elseif nargin(a) ~= nargin(b)
			result=false;
			return
		elseif nargin(a)==-1 && nargin(b) == -1
			% happens when the functions are zero so the independent variables are removed.
			% Then, quantity creates a internal function with varargin inputs --> -1.
			result=true;
			return
		else
			% in that case, the names of the arguments must be the same somehow...
			% one could think of a comparison function that only checks for the same values
			% without checking everything.
			
			aQ = quantity.Symbolic(sym(a));
			bQ = quantity.Symbolic(sym(b));

			if aQ~=bQ
				result=false;
				return
			end
		end
	else
		result=false;
		return
	end
elseif ~isa(b, class(a))
	result=false;
	return
elseif isstruct(a) && isstruct(b)
	result = isequal(a, b);
else
	result = isequal(numeric.acc(a,1e12),numeric.acc(b,1e12));
end % if 
end % function isequalF()

