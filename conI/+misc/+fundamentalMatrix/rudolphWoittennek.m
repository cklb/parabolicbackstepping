function [Phi, F, Psi, P] = rudolphWoittennek(A, varargin)
%FUNDAMENTALMATRIX_WOITTENNEK 

%% input parser:
ip = misc.Parser();
ip.addRequired('A');
ip.addOptional('N', 10);
ip.addOptional('zeta', sym('zeta', 'real'));
ip.addOptional('s', sym('s'));
ip.addOptional('B', []);
ip.parse(A, varargin{:});
misc.struct2ws(ip.Results)

% replace zeta always by symbolic variable because this is required to
% compute the Psi matrix.
zetainput = zeta; %#ok<NODEF>
zeta = sym('zeta', 'real');
xi = sym('xi');

if misc.issym(A) && ~isempty(symvar(A))
    z = symvar(A);
else
    z = sym('z');
end

n = size(A, 1);
d = size(A, 3); % As order of a polynom is zero based

%% initialization of the ProgressBar
counter = N + N;
pbar = misc.ProgressBar(...
    'name', 'Trajectory Planning (Woittennek)', ...
    'steps', counter, ...
    'initialStart', true);

%% computation of transition matrix as power series
F = sym( zeros( [n, n, N] )); % dimensions: ( states, states, polynomial degree)

if misc.issym(A)
    F(:,:,1) = internPeanoBakerSeries( A(:,:,1), z, xi, zeta, 20 ) ; 
else
    F(:,:,1) = expm( A(:,:,1) * (z - zeta) ) ;
end

Phi0 = F(:,:,1);

F = subs(F, zeta, zetainput);
Phi = F(:,:,1);

for kN = 2 : N
   pbar.raise();
    
   dA = min(d, kN);
   tmp_Phi = sym( zeros(n, n) );

% #TODO Check that there are the correct values!   
   
   for i = 2 : dA
      tmp_Phi = tmp_Phi + A(:, :, i) * F(:, :, kN-i+1);
   end

   F(:, :, kN) = int(subs(Phi0, zeta, xi) * subs(tmp_Phi, z, xi), xi, zetainput, z) ;
   
   Phi = Phi + F(:, :, kN) * s^(kN-1);
end

%% computation of inhomogeneous part of the ode solution


if ~isempty(B)
    if misc.issym(B) && ~isempty(symvar(B))
       assert(isequal(symvar(B), z)); 
    end
    
    m = size(B, 2);
    P = sym( zeros( [n, m, N] ));    

    P(:,:,1) = int(subs(Phi0, zeta, xi) * subs(B, z, xi), xi, zetainput, z);
    Psi = P(:,:,1);
    
    for kN = 2 : N
       pbar.raise();

       dA = min(size(A, 3), kN);
       tmp_Psi = sym(zeros(n, m));

       for i=2:dA
          tmp_Psi = tmp_Psi + A(:, :, i) * P(:,:,kN-i+1);
       end

       if size(B, 3) >= kN
          tmp_Psi = tmp_Psi + B(:, :, kN); 
       end

       P(:, :, kN) = int(subs(Phi0, zeta, xi) * subs(tmp_Psi, z, xi), xi, zetainput, z);
       Psi = Psi + P(:,:,kN) * s^(kN-1);
    end  
    P = subs(P, zeta, zetainput);
    Psi = subs(Psi, zeta, zetainput);
else
    P = [];
    Psi = [];
end

pbar.stop();

F = subs(F, zeta, zetainput);
Phi = subs(Phi, zeta, zetainput);

end

%TODO change this function to have the possibility to compute it to a
%certain accuracy
function Phi = internPeanoBakerSeries(A, z, xi, z0, N)
% PEANOBAKERSERIES_SYM Symbolic computation of (partial) peano baker series
% Detailed description for The Peano-Baker Series can be found in:
% The Peano-Baker Series, MICHAEL BAAKE AND ULRIKE SCHLAEGEL, 2012
%
% [ Phi ] = panobakerseries_sym( A, z0, N )
% 
% * A:   System-Operator Matrix \in C[s]^(nxn); with n is the number of
%      states. 
% * z0:  Lower limit of Integration
% * N:  Number of iterations
%
% * Phi: First result of the state transition matrix.(n x n x 1)
    I = sym( zeros(size(A,1), size(A,2), N ));   
    I(:,:,1) = int( subs(A, z, xi), xi, z0, z);
    Phi = eye( size(A,1), size(A,2) ) + I(:, :, 1);

    for k = 1 : N - 1
        I(:, :, k + 1) = int( subs( ( A * I(:, :, k) ), z, xi ), xi, z0, z );
        Phi = Phi + I(:, :, k + 1 );
    end
end

