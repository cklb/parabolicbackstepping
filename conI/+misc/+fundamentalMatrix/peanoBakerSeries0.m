%TODO change this function to have the possibility to compute it to a
%certain accuracy
function Phi = peanoBakerSeries0(A, z, xi, z0, N)
% PEANOBAKERSERIES_SYM Symbolic computation of (partial) peano baker series
% Detailed description for The Peano-Baker Series can be found in:
% The Peano-Baker Series, MICHAEL BAAKE AND ULRIKE SCHLAEGEL, 2012
%
% [ Phi ] = panobakerseries_sym( A, z0, N )
% 
% * A:   System-Operator Matrix \in C[s]^(nxn); with n is the number of
%      states. 
% * z0:  Lower limit of Integration
% * N:  Number of iterations
%
% * Phi: First result of the state transition matrix.(n x n x 1)
    I = sym( zeros(size(A,1), size(A,2), N ));   
    I(:,:,1) = int( subs(A, z, xi), xi, z0, z);
    Phi = eye( size(A,1), size(A,2) ) + I(:, :, 1);

    for k = 1 : N - 1
        I(:, :, k + 1) = int( subs( ( A * I(:, :, k) ), z, xi ), xi, z0, z );
        Phi = Phi + I(:, :, k + 1 );
    end
end