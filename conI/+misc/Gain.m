classdef Gain < handle & matlab.mixin.Copyable
%Gain is class to enease implementation of multiple different input 
%for instance control, disturbance, fault inputs or couplings with other
%systems. Only finite inputs are considered, see dps.Input for inputs for 
%distributed parameter systems.
%It is assumed, that the input signal is multiplied with the input matrices
%from right side, i.e. B * u(t).
%Gain is only one input, for use of multiple input-signals, use Gains!
	
	properties
		% gain matrices
		value (:,:);				% value
		
		% data about gain
		inputType char;		% type of input signal, for instance 
		%							% 'control', 'disturbance', 'fault' etc
		outputType cell;			% type of output signal, for instance 
		%							% 'measurement', 'ode', ...
		lengthInput (1, 1) double;	% number of columns of gain value
		lengthOutput (:, 1) double;	% number of rows of gain value per outputType
	end % properties
	
	properties (Dependent = true)
		OutputName (:, 1) cell;		% enumerated input type of length lengthOutput
		InputName (1, 1) cell;		% enumerated output type of length lengthInput
	end % properties (Dependent = true)
	
	methods
		function obj = Gain(inputType, value, varargin)
			%% Constructor
			if nargin > 0
				% read input
				myParser = misc.Parser();
				myParser.addParameter('outputType', {'out'}, ...
					@(v) iscell(v) | ischar(v) | isstring(v));
				myParser.addParameter('lengthOutput', size(value, 1), ...
					@(v) isvector(v) & isnumeric(v));
				myParser.parse(varargin{:});
				
				obj.inputType = inputType;
				obj.value = value;
				if iscell(myParser.Results.outputType)
					obj.outputType = myParser.Results.outputType;
				else
					obj.outputType = {myParser.Results.outputType};
				end
				
				% set lengths
				obj.lengthInput = size(value, 2);
				obj.lengthOutput = myParser.Results.lengthOutput;
				if numel(obj.outputType) > 1
					% check outputType and lengthOutput
					assert(numel(obj.outputType) == numel(obj.lengthOutput));
					assert(sum(obj.lengthOutput) == size(value, 1));
				end
			end % if nargin > 0
		end % Gain() Constructor
		
		function myGains = plus(a, b)
			% combining misc.Gain and misc.Gains
			if isempty(a)
				myGains = copy(b);
			elseif isempty(b)
				myGains = copy(a);
			elseif isa(b, 'misc.Gains')
				myGains = b + a;
			elseif isa(b, 'misc.Gain')
				if strcmp(b.inputType, a.inputType)
					myGains = parallel(a, b);
				else
					myGains = misc.Gains(copy(a), copy(b));
				end
			else
				error('misc.Gain can only be added with other Gain or misc.Gains');
			end
		end % plus()
		
		function myGains = add(a, b)
			myGains = a + b;
		end % add()
		
		function obj = exchange(obj, newGain)
			if all(strcmp(obj.inputType, newGain.inputType))
				for it = 1 : numel(newGain.outputType)
					thisOutputRows = obj.getOutputRows(newGain.outputType{it});
					if ~isempty(thisOutputRows)
						obj.value(thisOutputRows, :) = newGain.valueOfOutput(newGain.outputType{it});
					end
				end
			end
		end % exchange()
		
		function mySeries = series(obj, nextGain)
			if isa(nextGain, 'misc.Gain')
				mySeries = misc.Gain(nextGain.inputType, obj.value * nextGain.value);
				
			elseif isa(nextGain, 'misc.Gains') && (numel(nextGain.inputType) > 0)
				mySeries = misc.Gain(nextGain.gain(1).inputType, obj.value * nextGain.gain(1).value);
				for it = 2 : numel(nextGain.inputType)
					mySeries = mySeries + ...
						misc.Gain(nextGain.gain(it).inputType, obj.value * nextGain.gain(it).value);
				end
			end
		end % series()
		
		function myParallel = parallel(obj, varargin)
			myParallel = copy(obj);
			for it = 1 : numel(varargin)
				outputNext = varargin{it};
				% check sizes
				assert((obj.lengthInput == outputNext.lengthInput) ...
					&& strcmp(obj.inputType, outputNext.inputType), ...
					'Parallel outputs must be defined for same input length');
				
				for jt = 1 : numel(outputNext.outputType)
					if any(strcmp(myParallel.outputType, outputNext.outputType{jt}))
						
						outputIdx = find(strcmp(myParallel.outputType, outputNext.outputType{jt}));
						selectRowsOfOutput = ...
							sum(myParallel.lengthOutput(1:(outputIdx-1))) + (1 : myParallel.lengthOutput(outputIdx));
						myParallel.value(selectRowsOfOutput, :) = myParallel.value(selectRowsOfOutput, :) ...
							+ outputNext.valueOfOutput(outputNext.outputType{jt});
					else
						myParallel = ...
							misc.Gain(obj.inputType, ...
								[myParallel.value; outputNext.valueOfOutput(outputNext.outputType{jt})], ...
								'outputType', [myParallel.outputType; outputNext.outputType{jt}], ...
								'lengthOutput', [myParallel.lengthOutput; outputNext.lengthOutput(jt)]);
					end
				end
			end
		end % parallel()
		
		function value = inputType2sumblk(obj)
			% returns the string [type(1), '+', type(2), '+', ...]
			value = obj.inputType;
		end % inputType2sumblk()
		
		function mySs = gain2ss(obj, varargin)
			% returns a cell-array containing a ss() representing the gain with the
			% properties InputName and OutputName set according to inputType and
			% outputType.
			mySs = ss([], [], [], obj.value, ...
				'InputName', obj.InputName, ...
				'OutputName', obj.OutputName);
		end % gain2ss()
		
		function thisOutputRows = getOutputRows(obj, outputType)
			outputIdx = find(strcmp(obj.outputType, outputType));
			thisOutputRows = sum(obj.lengthOutput(1:(outputIdx-1))) + (1 : obj.lengthOutput(outputIdx));
		end % getOutputRows()
		
		function newObj = gainOfOutput(obj, outputNames)
			if ~iscell(outputNames)
				outputNames = {outputNames};
			end
			outputNames = outputNames(...
					cellfun(@(v) any(strcmp(v, obj.outputType.')), outputNames));
			if isempty(outputNames)
				newObj = misc.Gain();
			else
				newObj = misc.Gain(obj.inputType, ...
						obj.valueOfOutput(outputNames), ...
						'outputType', outputNames, ...
						'lengthOutput', obj.lengthOfOutput(outputNames));
			end
		end % gainOfOutput()
		
		function thisValue = valueOfOutput(obj, outputNames)
			% obj.valueOfOutput() returns the gain matrix specified by the
			% outputNames in the input parameter. The input outputNames must be a
			% string or char-array or a cell array of strings or char-arrays.
			if ~iscell(outputNames)
				outputNames = {outputNames};
			end
			% get gain matrix of first output
			thisValue = obj.value(obj.getOutputRows(outputNames{1}), :);
			
			% successivly add gain matrices of further outputs
			if numel(outputNames) > 1
				thisValue = [thisValue; obj.valueOfOutput(outputNames{2:end})];
			end
		end % valueOfOutput(obj, outputNames)
		
		function obj = removeOutput(obj, outputType)
			myOutputRows = getOutputRows(obj, outputType);
			if ~isempty(myOutputRows)
				myOutputRowsSelector = true(sum(obj.lengthOutput), 1);
				myOutputRowsSelector(myOutputRows) = false;
				newLengthOutput = obj.lengthOutput(~strcmp(obj.outputType, outputType));
				newOutputType = obj.outputType(~strcmp(obj.outputType, outputType));
				newValue = obj.value(myOutputRowsSelector, :);
				if isempty(newOutputType)
					obj = misc.Gain();
				else
					obj = misc.Gain(obj.inputType, newValue, ...
						'outputType', newOutputType, ...
						'lengthOutput', newLengthOutput);
				end
% 				obj.value = obj.value(myOutputRowsSelector, :);
% 				obj.outputType = obj.outputType(~strcmp(obj.outputType, outputType));
% 				obj.lengthOutput = lengthOutputBackup(~strcmp(obj.outputType, outputType));
				obj.verifySizes();
			end
		end % removeOutput()
		
		function thisLength = lengthOfOutput(obj, varargin)
			if nargin > 1
				thisOutputType = varargin{1};
				thisLength = obj.lengthOutput(strcmp(obj.outputType, thisOutputType));
				thisLength = [thisLength; obj.lengthOfOutput(varargin{2:end})];
			else
				thisLength = [];
			end
		end % lengthOfOutput(obj, outputNames)
		
		function result = isempty(obj)
			result = false(size(obj));
			for it = 1 : numel(obj)
				result(it) = isempty(obj(it).value);
			end 
		end % isempty
		
		function verifySizes(obj)
			assert(isequal([sum(obj.lengthOutput), obj.lengthInput], size(obj.value)));
		end % verifySizes(obj)
		
		function obj = strrepOutputType(obj, oldText, newText)
			% replace strings in type
			assert(ischar(oldText) || isstring(oldText), 'oldText must be a char-array or string');
			assert(ischar(newText) || isstring(newText), 'newText must be a char-array or string');
			for it = 1 : numel(obj.outputType)
				obj.outputType{it} = strrep(obj.outputType{it}, oldText, newText);
			end
		end % strrepOutputType()
		
		function obj = strrepInputType(obj, oldText, newText)
			% replace strings in type
			assert(ischar(oldText) || isstring(oldText), 'oldText must be a char-array or string');
			assert(ischar(newText) || isstring(newText), 'newText must be a char-array or string');
			obj.inputType = strrep(obj.inputType, oldText, newText);
		end % strrepInputType()
		
		function OutputName = get.OutputName(obj)
			OutputName = cell(sum(obj.lengthOutput), 1); 
			myCounter = 1;
			for jt = 1 : numel(obj.lengthOutput)
				if (obj.lengthOutput(jt) == 1)
					% no enumeration in this case
					OutputName{myCounter} = [obj.outputType{jt}];
					myCounter = myCounter + 1;
				else
					for it = 1 : sum(obj.lengthOutput(jt))
						OutputName{myCounter} = [obj.outputType{jt}, '(', num2str(it), ')'];
						myCounter = myCounter + 1;
					end
				end
			end
		end % get.OutputName()
		
		function InputName = get.InputName(obj)
			InputName = cell(sum(obj.lengthInput), 1); 
			if (obj.lengthInput == 1)
				% no enumeration in this case
				InputName{1} = obj.inputType;
			else
				for it = 1 : sum(obj.lengthInput)
					InputName{it} = [obj.inputType ,'(', num2str(it), ')'];
				end
			end
		end % get.InputName()
		
		function result = isequal(A, B, varargin)
			% isequal compares all parameters of A and B and varargin and returns
			% true if they are equal, and false if not.
			if (numel(A) == 0) || (numel(B) == 0)
				result = (numel(A)==numel(B));
			else
				result = isequal(A.value, B.value) & strcmp(A.inputType, B.inputType) ...
					& all(strcmp(A.outputType, B.outputType));
			end
			if result && (nargin > 2)
				result = isa(B, 'misc.Gain') & B.isequal(varargin{:});
			end
		end % isequal()
		
		function [texString, nameValuePairs] = print(obj)
			% Create TeX-code for all non-zero parameter. In this method, the
			% implementation of misc.Gains is used.
			[texString, nameValuePairs] = print(misc.Gains(obj));
		end % print
		
	end % methods
	
	methods (Access = protected)
		% Override copyElement method:
		function cpObj = copyElement(obj)
			% Make a shallow copy of all properties
			cpObj = copyElement@matlab.mixin.Copyable(obj);
		end
	end % methods (Access = protected)
end