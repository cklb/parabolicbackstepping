function newObj = constructorOf(oldObj, varargin)
% misc.constructorOf runs the constructor of the input-object oldObj by using the
% inputs for the constructor specified by varargin.
%%
% Example:
%		oldObj = quantity.Discrete(linspace(0, 1, 11).', ...
%				'grid', linspace(0, 1, 11), 'gridName', 'z', 'name', 'Berthold');
%		newObj = misc.constructorOf(oldObj, ...
%				2*linspace(0, 1, 11).', ...
%				'grid', linspace(0, 1, 11), 'gridName', 'z', 'name', 'Lars');

className = class(oldObj);
newObj = eval([className, '(varargin{:})']);
end % misc.constructorOf()