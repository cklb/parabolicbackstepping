function [ V ] = alln( V )
% ALLN recursive calling of function all until the result has only one
% dimension
%
% Description:
% >> V = alln(V) 
% Returns a scalar boolean value if V contains only true values in each 
% entry!
%
% Inputs:
% V       n-dimensional array (numeric)
% Outputs:
% V       Scalar
%
% Example: 
% -------------------------------------------------------------------------
% M = ones(2,2,2);
% misc.alln(M)
% >> ans = 1
% -------------------------------------------------------------------------

import misc.alln

V = all(V(:));

end