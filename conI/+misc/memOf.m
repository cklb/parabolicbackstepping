function size = memOf(var)
name = who;
r = whos(name{1});
if nargout == 0
	disp(['Size: ' num2str(round(r.bytes/1e6)) 'MB']);
else
	size = r.bytes;
end
end

