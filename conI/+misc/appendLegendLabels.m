function labels = appendLegendLabels(labels, v, name)
   for k = 1:size(v, 2)
      labels{end+1} = [name '_' num2str(k)];  %#ok<AGROW>
   end
end 