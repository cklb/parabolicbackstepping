function myStateSpace = changeSignalName(myStateSpace, oldName, newName, varargin)
% CHANGESSSIGNALNAME sets the property 'OutputName' or 'InputName' of the ss 
% myStateSpace by replacing oldName with newName. If hereby inputs result in having
% the same name, they are connected, see example.
%
% Inputs:
%	myStateSpace	state space whoms OutputName and InputName should be modified
%	oldName			cell array of signal names that should be applied to
%					myStateSpace.OutputName
%	newName			cell array of same size as signal names. Each element defines the
%					length of the signal that is named in signal names
% Optional Inputs (default values):
%	input (true)	logical 
%	output (true) 	
% Outputs:
%	myStateSpace	the state space with modified parameters
%
% Example:
% -------------------------------------------------------------------------
% 	mySimulationModel = ss(1, [1, 2], [1; 2; 3], [], 'InputName', {'d', 'e'});
%	mySimulationModel = misc.ss.setSignalName(mySimulationModel, 'output', {'a', 'b'}, {2, 1});
%	mySimulationModel = misc.ss.changeSignalName(mySimulationModel, {'a', 'd'}, {'c', 'e'})
% -------------------------------------------------------------------------

% input checks
if ~iscell(oldName)
	oldName = {oldName};
end
if ~iscell(newName)
	newName = {newName};
end
myParser = misc.Parser();
myParser.addRequired('myStateSpace', @(v) isa(v, 'ss'));
myParser.addParameter('input', true, @(v) islogical(v));
myParser.addParameter('output', true, @(v) islogical(v));
myParser.parse(myStateSpace, varargin{:});
assert(numel(newName) == numel(oldName), 'number of elements of newName and oldName must be equal');

% consider output
if myParser.Results.output
	myStateSpace = changeOutputName(myStateSpace, oldName, newName);
end

% consider input
if myParser.Results.input
	myStateSpace = changeInputName(myStateSpace, oldName, newName);
end
end

function myStateSpace = changeInputName(myStateSpace, oldName, newName)
	% changeInputName replaces the InputName of myStateSpace that are specified by
	% oldName with the names specified in newName. This is done by creating new gain
	% blocks with inputs according to newName and outputs according to oldName. By
	% doing so, inputs can be renamed and interconnected with other inputs that
	% already had that new name.
	uniqueOldInputName = misc.ss.removeEnumeration(myStateSpace.InputName, true);
	oldInputName = misc.ss.removeEnumeration(myStateSpace.InputName, false);
	for it = 1 : numel(oldName)
		% input check:
		if any(strcmp(oldName{it}, uniqueOldInputName))
			assert(~any(strcmp(oldName{it}, newName)), ...
				'overlaps in oldName and newName are not supported for InputName-changes');
		end
	end	
	
	for it = 1 : numel(oldName)
		if any(strcmp(oldName{it}, uniqueOldInputName))
			% there is an InputName with the name oldName{it} to be replaced.
			numberOfThisInput = sum(strcmp(oldInputName, oldName{it}));
			if any(strcmp(newName{it}, uniqueOldInputName))
				% there is already a InputName with the name of newName{it}
				% -> they are interconnected via thisConnectorGain
				thisConnectorGain = ss([], [], [], ...
					vertcat(eye(numberOfThisInput), eye(numberOfThisInput)), ...
					'InputName', newName{it});
				thisConnectorGain = misc.ss.setSignalName(thisConnectorGain, 'output', ...
					{oldName{it}, [newName{it}, 'tempChangeSignalName']}, ...
					{numberOfThisInput, numberOfThisInput});
				myStateSpace = misc.ss.changeSignalName(myStateSpace, ...
					newName{it}, [newName{it}, 'tempChangeSignalName'], 'output', false);
			else
				% there is no InputName with the name of newName{it}
				% -> thisConnectorGain is just an identy with input newName{it} and
				% output oldName{it}
				thisConnectorGain = ss([], [], [], ...
					vertcat(eye(numberOfThisInput)), ...
					'InputName', newName{it}, 'OutputName', oldName{it});
			end
			inputNameIntermediate = misc.ss.removeEnumeration(myStateSpace.InputName, false);
			newInputNames = [inputNameIntermediate(...
				~strcmp(inputNameIntermediate, oldName{it}) & ...
				~strcmp(inputNameIntermediate, [newName{it}, 'tempChangeSignalName']))];
			if ~any(strcmp(newInputNames, newName{it}))
				newInputNames = [newInputNames; ...
					repmat(newName(it), sum(strcmp(oldInputName, oldName{it})), 1)];
			end
			myStateSpace = misc.ss.connect(...
				newInputNames, myStateSpace.OutputName, ...
				myStateSpace, thisConnectorGain);
		end
	end

end
function myStateSpace = changeOutputName(myStateSpace, oldName, newName)
	% changeOutputName replaces the OutputName of myStateSpace that are specified by
	% oldName with the names specified in newName. This is done by reading the old
	% OutputName property, replacing the strings, and setting them in the end again.
	uniqueOldInputName = misc.ss.removeEnumeration(myStateSpace.OutputName, true);
	for it = 1 : numel(newName)
		assert(~any(strcmp(uniqueOldInputName, newName{it})), ...
			'newName is already OutputName of myStateSpace - not implemented');
	end
	% split signal names
	oldOutputNames = misc.ss.removeEnumeration(myStateSpace.OutputName);
	newOutputNames = misc.ss.removeEnumeration(myStateSpace.OutputName);
	
	% Exchange names of newOutputNames cell array:
	for it = 1 : numel(newName)
		if ~isempty(strcmp(oldOutputNames, oldName{it})) ...
				&& any(strcmp(oldOutputNames, oldName{it}))
			[oldOutputNames{strcmp(oldOutputNames, oldName{it})}] = deal(newName{it});
		end
	end
	uniqueOutputNames = unique(oldOutputNames, 'stable');
	outputLength = cellfun(@(v) sum(strcmp(oldOutputNames, v)), uniqueOutputNames);
	% set output names
	myStateSpace = misc.ss.setSignalName(myStateSpace, 'output', uniqueOutputNames, outputLength);
end