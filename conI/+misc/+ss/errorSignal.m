function simError = errorSignal(simOriginal, simReference)
% MISC.SS.ERRORSIGNAL calculates the difference between all fields of simOriginal and
% simReference, and can be used to calculate the error signals of, for instance, an
% observer simulation, i.e. e = misc.ss.errorSignal(simPlant, simObserver);
% The resulting struct simError contains all fields that are fields of both
% simOriginal and simReference with the value 
%	simError.(fieldName) = simOriginal.(fieldName) - simReference(fieldName);
%
% Inputs:
%	simOriginal		struct of simulation data, for instance, output, state, input...
%	simReference	struct of simulation data, for instance, output, state, input...
% Outputs:
%	simReference	struct of difference of simOriginal and simReference fields
%
% Example:
% -------------------------------------------------------------------------
% 	simOriginal.x = [2; 2];
%	simOriginal.u = 1;
% 	simReference.x = [1; 1];
%	simError = misc.ss.errorSignal(simOriginal, simReference);
% -------------------------------------------------------------------------

simError = struct();
simOriginalFieldNames = fieldnames(simOriginal);
%simReferenceFieldNames = fieldnames(simOriginal);
for it = 1 : numel(simOriginalFieldNames)
	if isfield(simReference, simOriginalFieldNames{it})
		if isstruct(simOriginal.(simOriginalFieldNames{it}))
			simError.(simOriginalFieldNames{it}) ...
				= misc.ss.errorSignal(...
					simOriginal.(simOriginalFieldNames{it}), ...
					simReference.(simOriginalFieldNames{it}));
		else
			simError.(simOriginalFieldNames{it}) ...
				= simOriginal.(simOriginalFieldNames{it}) ...
					- simReference.(simOriginalFieldNames{it});
		end
	end
end % for it = 1 :...
end % misc.ss.errorSignal()

