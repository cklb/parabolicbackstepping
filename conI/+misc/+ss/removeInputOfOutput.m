function myStateSpace = removeInputOfOutput(myStateSpace)
% REMOVEINPUTOFOUTPUT removes all output signals of myStateSpace that are
% inputSignals.
%
% Inputs:
%	myStateSpace	state space which is modified
% Outputs:
%	myStateSpace	the state space with modified parameters
%
% Example:
% -------------------------------------------------------------------------
%  	mySimulationModel = ss(1, [1, 2, 3], [1; 2; 3], [1, 0, 0; 0, 0, 0; 0, 0, 0], ...
% 		'OutputName', {'x', 'y', 'z'}, 'InputName', {'u(1)', 'u(2)', 'a'});
% 	mySimulationModel = misc.ss.addInput2Output(mySimulationModel);
%	mySimulationModel = misc.ss.removeInputOfOutput(mySimulationModel);
% -------------------------------------------------------------------------

% read InputName and OutputName
inputNameAll = misc.ss.removeEnumeration(myStateSpace.InputName);
outputNameAll = misc.ss.removeEnumeration(myStateSpace.OutputName);

% count
inputName = unique(inputNameAll, 'stable');

% remove
myC = myStateSpace.C;
myD = myStateSpace.D;
myOutputName = myStateSpace.OutputName;
selectLinesThatRemain = true(numel(myOutputName), 1);
for it = 1 : numel(inputName)
	selectLinesThatRemain = selectLinesThatRemain & ~strcmp(outputNameAll, inputName(it));
end
myC = myC(selectLinesThatRemain, :);
if ~isempty(myD)
	myD = myD(selectLinesThatRemain, :);
end
myOutputName = myOutputName(selectLinesThatRemain);

myStateSpace = ss(myStateSpace.A, myStateSpace.B, myC, myD, myStateSpace.Ts, ...
	'InputName', myStateSpace.InputName, 'OutputName', myOutputName, ...
	'Name', myStateSpace.Name);

end

