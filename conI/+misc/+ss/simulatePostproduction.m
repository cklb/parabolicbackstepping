function simData = simulatePostproduction(simData, varargin)
% MISC.SS.SIMULATEPOSTPRODUCTION goes through varargin and calls the method
% simulatePostproduction() of all that objects for which such a method is defined.
%
% Inputs:
%	simData			simulation data object
%	varargin		various objects, such as a plants model or a observer
% Outputs:
%	simData			simulation data object

for it = 1 : numel(varargin)
	if ismethod(varargin{it}, 'simulatePostproduction')
		simData = varargin{it}.simulatePostproduction(simData);
	end
end
end

