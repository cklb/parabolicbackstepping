function myStateSpace = addInput2Output(myStateSpace)
% ADDINPUT2OUTPUTNAME adds all inputs to the outputs of the state space model, by
% designing feedthroughs.
%
% Inputs:
%	myStateSpace	state space which is modified
% Outputs:
%	myStateSpace	the state space with modified parameters
%
% Example:
% -------------------------------------------------------------------------
%  	mySimulationModel = ss(1, [1, 2, 3], [1; 2; 3], [1, 0, 0; 0, 0, 0; 0, 0, 0], ...
% 		'OutputName', {'x', 'y', 'z'}, 'InputName', {'u(1)', 'u(2)', 'a'});
% 	mySimulationModel = misc.ss.addInput2Output(mySimulationModel);
% -------------------------------------------------------------------------

% read InputName and OutputName
inputNameAll = misc.ss.removeEnumeration(unique(myStateSpace.InputName, 'stable'));
outputNameAll = misc.ss.removeEnumeration(unique(myStateSpace.OutputName, 'stable'));

% count
inputName = unique(inputNameAll, 'stable');
outputName = unique(outputNameAll, 'stable');
inputNameCount = cellfun(@(v) sum(strcmp(inputNameAll, v)), inputName);
outputNameCount = cellfun(@(v) sum(strcmp(outputNameAll, v)), outputName);

% add feedthrough
isFeedThroughNeccessary = false([numel(inputName), 1]);
for it = 1 : numel(inputName)
	if any(strcmp(outputName, inputName{it}))
		% if the input is already part of the output, assert that it has the same
		% size
		assert(inputNameCount(it) == outputNameCount(strcmp(outputName, inputName{it})), ...
			'There is already an output with the same name as an input, but they have different size!?');
		isFeedThroughNeccessary(it) = false;
	else
		% Create a feedthrough and  add it to myStateSpace
		% feedthrough:
		feedthroughTemp = ss([], [], [], eye(inputNameCount(it)), ...
			'OutputName', inputName(it), 'InputName', inputName(it));
		% connection:
		% first myStateSpace is appended, then the input of the feedthrough and the
		% state space are interconneted using series()
		myStateSpace = append(myStateSpace, feedthroughTemp);
		isFeedThroughNeccessary(it) = true;
	end
end
% create short cut of feedthrough and input
shortCutMatrix = eye(sum(inputNameCount));
shortCutMatrixSelector = true(size(shortCutMatrix));
% remove rows that have now feedthrough
for it = 1 : numel(inputName)
	if ~isFeedThroughNeccessary(it)
		shortCutMatrixSelector(sum(inputNameCount(1:(it-1)))+(1:inputNameCount(it)), :) ...
			= false;
	end
end
shortCutMatrix = reshape(shortCutMatrix(shortCutMatrixSelector), ...
	[sum(inputNameCount(isFeedThroughNeccessary)), sum(inputNameCount)]);
shortCut = ss([], [], [], [eye(sum(inputNameCount)); shortCutMatrix]);
shortCut = misc.ss.setSignalName(shortCut, 'input', inputName, inputNameCount);
myStateSpace = series(shortCut, myStateSpace);

end

