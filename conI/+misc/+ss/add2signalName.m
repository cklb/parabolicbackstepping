function mySs = add2signalName(mySs, signalType, position, newText)
% ADD2SIGNALNAME adds a string to the OutputName or InputName of the state
% space model mySs either at the position 'front' or 'back'.
% Example:
% misc.ss.add2signalName(ss(1, 1, [1; 1], [], 'OutputName', {'a', 'b'}), ...
%		'OutputName', 'front', 'ode.');
assert(any(strcmp(position, {'front', 'back'})));
assert(any(strcmp(signalType, {'OutputName', 'InputName'})));
assert(ischar(newText) || isstring(newText), 'prefix must be a char-array or string');

myNames = mySs.(signalType);
if strcmp(position, 'front')
	for it = 1 : numel(myNames)
		myNames{it} = [newText, myNames{it}];
	end
elseif strcmp(position, 'back')
	for it = 1 : numel(myNames)
		myNames{it} = [myNames{it}, newText];
	end
end
	
mySs.(signalType) = myNames;
end