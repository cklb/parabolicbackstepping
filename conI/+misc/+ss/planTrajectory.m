function [u, y, x] = planTrajectory(sys, t, varargin)
% PLANTRAJECTORY Computes a trajectory for a lumped-parameter systems
%	[u, y, x] = planTrajectory(sys, t, varargin) computes a transition 
%		u(t) : x(t0) = x0 --> x(t1) = x1
%   for a state-space system sys of the form
%		d/dt x = A x + bu.
%	Initial and end value x0 and x1 can be defined by name value pairs.
%	Default value is zero. The computation of the transition is based on
%	the Gramian controllability matrix
%		W(t0, t1) = int_t0^t1 Phi(t0, tau) b b^T Phi^T(t0, tau) d tau.
%   The used formula is
%		u(t) = -b^T Phi^T(t0, t) W^(-1)(t0, t1) ( x0 - Phi(t0, t1) x1 )
%		x(t) = Phi(t0, t) * W(t0, t) * W0^(-1)(t0, t1) * ( Phi(t0, t1) * x1 - x0)
%		y(t) = C x(t)
%
p = misc.Parser();
p.addParameter('x0', zeros(size(sys.A, 1), 1));
p.addParameter('x1', zeros(size(sys.A, 1), 1));
p.parse(varargin{:});
x0 = p.Results.x0;
x1 = p.Results.x1;

% prepare time vectors
t = t(:);		
t0 = t(1);
t1 = t(end);

%% Compute the state transition matrix: Phi(t,tau) = expm(A*(t-tau) )
% Att0 = sys.A * quantity.Symbolic( sym('t') - sym('tau'), 'grid', {t, t}, 'gridName', {'t', 'tau'} );
Phi_t0 = expm(sys.A * quantity.Discrete( t - t0, 'grid', {t}, 'gridName', {'t'} ));

invPhi_t1 = expm(sys.A * quantity.Discrete( t1 - t, 'grid', {t}, 'gridName', {'t'} ));

%% compute the gramian controllability matrix
W1 = misc.ss.gramian(sys, t, t0);
W1_t1  = W1.at(t1);
% Formel aus dem Bernstein:
u = sys.b.' * invPhi_t1.' / W1_t1  * (x1 - Phi_t0.at(t1) * x0);
% Berechnung der 
x = Phi_t0 * x0 + W1 * invPhi_t1' / W1_t1  * (x1 - Phi_t0.at(t1) * x0);
% 
y = sys.c * x;

%% Alternative Lösung nach Chen:
% W0 = int( invPhi_t0 * sys.B * sys.B' * invPhi_t0', 't', t0, 't');
% W0 = invPhi_t0 * W1 * invPhi_t0';
% u1 = -sys.B' * invPhi_t0' / W0.at(t1) * (x0 - Phi_t1.at(t0) * x1);
% xW0 = Phi_t0 * ( x0 - W0 / W0.at(t1) * (x0 - Phi_t1.at(t0) * x1));
% 
% % simulation results have shown, does the average of both soultions leads to a better than each.
% u = (u0 + u1) / 2;
