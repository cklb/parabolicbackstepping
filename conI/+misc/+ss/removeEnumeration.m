function unenumerateNames = removeEnumeration(enumeratedNames, makeUnique)
% REMOVEENUMERATION removes braces with numbers from every char array that is an
% element of the cell array enumeratedNames. This is for instance useful, to deal
% with InputName or OutputName properties of large state-space models (ss).
% unenumerateNames = removeEnumeration(enumeratedNames) returns the
%  enumeratedNames cell-array without enumeration.
% unenumerateNames = removeEnumeration(enumeratedNames, true) returns enumeratedNames
%  without enumeration, but only unique names. The order of names is not changed.
% Examples:
% misc.ss.removeEnumeration({'a(1)', 'a(2)', 'b'})
% misc.ss.removeEnumeration({'a(1)', 'a(2)', 'b'}, true)
unenumerateNames = cell(size(enumeratedNames));
for it = 1 : numel(enumeratedNames)
	namesSplittedTemp = split(enumeratedNames{it}, '(');
	unenumerateNames{it} = namesSplittedTemp{1};
end
if nargin > 1 && makeUnique
	unenumerateNames = unique(unenumerateNames, 'stable');
end
end