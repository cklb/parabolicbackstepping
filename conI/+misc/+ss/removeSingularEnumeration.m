function newNames = removeSingularEnumeration(enumeratedNames)
% REMOVESINGULARENUMERATION removes braces with numbers from char-arrays of singular 
% names that are an element of the cell-array enumeratedNames. 
% This is for instance useful, to deal with InputName or OutputName properties of 
% large state-space models (ss).
% Examples:
% misc.ss.removeSingularEnumeration({'a(1)', 'a(2)', 'b(1)', 'c(1)', 'b', 'c(2)', 'd(1)'})

newNames = enumeratedNames;
for it = 1 : numel(enumeratedNames)
	if contains(enumeratedNames{it}, '(1)')
		namesSplittedTemp = split(enumeratedNames{it}, '(');
		if sum(contains(newNames, [namesSplittedTemp{1}, '('])) == 1
			newNames{it} = strrep(enumeratedNames{it}, '(1)', '');
		end
	end
end
end