function resultSs = connect(InputName, OutputName, options, varargin)
% connect  Block-diagram interconnections of dynamic systems based on ss/connect.
% Different from the built-in function, InputName and OutputName are asserted to be
% unique. And it is more robust, since feedthroughs are removed before calling
% connect and are added afterwards again.

% remove empty elements of varargin
% note that empty(ss) does not work, since state space models without inputs are
% considered as empty. Hence, size(v, 1) > 0 is used instead.
if isa(options, 'ltioptions.connect')
	inputSs = varargin(cellfun(@(v) size(v, 1) > 0, varargin));
else
	if isa(options, 'numlti')
		inputSs = [{options}, varargin(cellfun(@(v) size(v, 1) > 0, varargin))];
	elseif ~isempty(options)
		error("third input must be a connectOptions() or a StateSpaceModel");
	end
	options = connectOptions('Simplify', false);
end

% clean state-spaces
for it = 1 : numel(inputSs)
	% remove feedthroughs
	inputSs{it} = misc.ss.removeInputOfOutput(ss(inputSs{it}));
	inputSs{it}.OutputName = misc.ss.removeSingularEnumeration(inputSs{it}.OutputName);
	inputSs{it}.InputName = misc.ss.removeSingularEnumeration(inputSs{it}.InputName);
end

% call built-in function
resultSs = connect(inputSs{:}, ...
	unique(misc.ss.removeSingularEnumeration(InputName), 'stable'), ...
	unique(misc.ss.removeSingularEnumeration(OutputName), 'stable'), ...
	options);

end