function idxVec = findInColumn(expr,num)
%UNTITLED3 Summary of this function goes here
%   Detailed explanation goes here

% NO IDEA! I DONT GET IT TO WORK IF THERE ARE MULTIPLE SEQUENCES!

if nargin<2
	num=1;
end

% Problem: wenn expression �bergeben wurde, kann man nicht mehr sortieren..
% Wenn unsortiert ist, muss jede Spalte einzeln �berpr�ft werden..
allIdc = expr;
diffIdc = diff([zeros(1,size(expr,2));allIdc;zeros(1,size(expr,2))]);
toRight = ones(1,size(expr,2));
toDown = linspace(1,size(expr,1),size(expr,1)).';

incMat = toDown*toRight;

idxVec = zeros(num,size(expr,2));
if num>1
	startIdc = diffIdc==1;
	endIdc = diffIdc==-1;
	for j=1:num
		idxVec(j,any(diffIdc(1:end-1,:)==1)) = incMat(logical((diffIdc(1:end-1,:)==1)+j-1));
	end
else
	idxVec(any(diffIdc(1:end-1,:)==1)) = incMat(diffIdc(1:end-1,:)==1);
end
end

