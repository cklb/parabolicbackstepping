function s = sizeOf( a )
% SIZEOF get correct size of multidimensional array
% On contrary to SIZE, sizeOf gives length(a), if a is a column vector,
% which is, it is only expanded in the first dimension.
%
% Inputs:
%     a       Array of interest
% Outputs:
%     s       Size of a
%
% Example:
% -------------------------------------------------------------------------
% a = [1;2;3];
% size(a)
% >> ans = [3 1]
% misc.sizeOf(a)
% >> ans = 3
% -------------------------------------------------------------------------

s = size(a);

% remove trailing "1" if column vector.
if s(end)==1
	s=s(1:end-1);
end

end

