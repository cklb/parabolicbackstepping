function [K,p,Q,R] = placeLQR( A,B,pD )
% PLACELQR Pole placement design using ricatti for DOFs
%
% K = placeLQR(A,B,pD)
% place computes a gain matrix K such that the poles of the closed loop
% system matrix (A-BK) are located at p.If it is a mimo system, the degree 
% of freedom is used to minimize the control effort.
%
% Inputs:
%     A       State matrix
%     B       Input matrix
%     pD      Poles of the closed loop System (A-BK)
% Outputs:
%     K       Gain matrix
%     p       Placement of poles
%     Q       Weighting matrix of LQR
%     R       Weighting matrix of LQR
%
% Example:
% -------------------------------------------------------------------------
% A = [1 0; 0 3];
% B = [1 0;1 1];
% pD = [-1;-3];
% misc.placeLQR(A,B,pD)
% >> ans = [ 0.83  2.39
%          [-2.42  4.82]
% -------------------------------------------------------------------------
%       


if size(A,1)~=size(A,2)
	error('A must be a quadratic matrix!')
end
if length(pD)~= size(A,1)
	error('Number of eigenvalues must equal the size of A!')
end
if size(B,1)~=size(A,1)
	error('size(B,1)~=size(A,1)!')
end

if size(B,2) ==1
	K = place(A,B,pD);
	p = pD;
else % mimo case:
	ew_desired = (pD);
	% Find Q-Matrix (quadratic to achieve the desired eigenvalues:)
	n = size(A,1);
	p = size(B,2);
	opts = optimset('Display','none','TolFun',1e-6,'TolX',1e-6,...
		'DiffMinChange',0.01); 
	                          % function                 % initial guess       A  b Aeq beq lb            ub nonlcon         
	Q_vec = fmincon(@(fac) get_ew_diff(fac,ew_desired,A,B),zeros(size(A,1)+1,1),[],[],[],[],zeros(size(A,1)+1,1),[],[],opts);
% 	Q = diag(Q_vec);
	Q = diag(Q_vec(1:n));
	R = Q_vec(n+1)*eye(p);

	% Q = factor*eye;
% 	R = eye(size(B,2));
	N = zeros(size(A,1),size(B,2));
	K = lqr(A,B,Q,R,N);	
	p = eig(A-B*K);
	if any(abs((p-pD))>1e-3)
		warning(['Poles are not placed accurately! max(p-pD)=' num2str(max(max(abs(p-pD)))) '.'])
	end
end


end

function ew_diff = get_ew_diff(Q_vec,ew_desired,A,B)
	n = size(A,1);
	p = size(B,2);
	Q = diag(Q_vec(1:n));
	R = Q_vec(n+1)*eye(p);
	[~,~,ewR] = lqr(A,B,Q,R,zeros(size(A,1),size(B,2)));
	ew_diff = max(max(abs((ewR)-ew_desired)));
end