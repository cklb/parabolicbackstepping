function [i] = issymornumeric(A)
% ISSYMORNUMERIC True for numeric or symbolic values
%
% Description:
% ISSYMORNUMERIC(A) returns true if A is a numeric array or a symbolic
% object. 
%
% Inputs:
%     A       Variable of interest
% Outputs:
%     i       Logical falue
%   
% Example:
% -------------------------------------------------------------------------
% A = eye(2);
% issymornumeric(A)
% >> ans = 1
% issymornumeric(issymornumeric(A))
% >> ans = 0
% -------------------------------------------------------------------------
%
i = isnumeric(A) || misc.issym(A);
end

