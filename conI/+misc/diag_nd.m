function valueDiag = diag_nd(value, varargin)
%DIAG_ND returns the diagonal elements of the first two dimensions of the
%n-D matrix "value" for every further dimension.
%% Example
% testValue = rand(4, 4, 2, 2);
% testDiagValue = misc.diag_nd(testValue);
% % The following code is only for verification of testDiagValue
% testDiagValueReferenceResult = zeros(4, 2, 2);
% for it = 1:size(testValue, 3)
% 	for jt = 1:size(testValue, 4)
% 		testDiagValueReferenceResult(:, it, jt) = ...
% 				diag(testValue(:, :, it, jt));
% 	end
% end
% if all(testDiagValue(:) == testDiagValueReferenceResult(:))
% 	fprintf('Success! \n');
% end
% 	
%
% INPUT PARAMETERS:
% 	ARRAY						value : nd matrix
% OPTIONAL INPUT PARAMTERS 
%	varargin				 varargin : not in use yet, but it would be
%										helpful to extend this function
%										with further functionalities, like
%										choosing which dimensions should be
%										considered for diag().
%   
% OUTPUT PARAMETERS:
% 	ARRAY					valueDiag : diagonal elements in the first two
%										dimensions of value

valueSize = size(value);
if valueSize(1) ~= valueSize(2)
	error(['The two dimensions for which the diagonal elements should', ...
		'be calculated must be quadratic but size(value, 1) ~= size(value, 2)']);
end

% valueDiagSize is initialised and written in such weired way in the
% following to ensure that it a vector with at least 2 elements, even if 
% the variable value is 2D. If this is not done, the final reshape wont
% work.
valueDiagSize = ones(1:max((numel(valueSize)-1), 2));
valueDiagSize(1:numel(valueSize([1, 3:end]))) = valueSize([1, 3:end]); 
% The implemenetation with num2cell and diag(blkdiag(...)) is terribly slow
% because of blkdiag. Since it is slower than for-loops and diag, for-loops
%  are used instead.
% valueCell = num2cell(value, 1:2); % valueDiagUnshaped = diag(blkdiag(valueCell{:}));
valueUnshaped = reshape(value, valueSize(1), valueSize(2), []);
valueDiagUnshaped = zeros(valueSize(1), prod(valueSize(3:end)));
for it = 1:prod(valueSize(3:end))
	valueDiagUnshaped(:,it) = diag(valueUnshaped(:, :, it));
end

valueDiag = reshape(valueDiagUnshaped, valueDiagSize);

end
