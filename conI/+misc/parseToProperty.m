function [obj, myParser] = parseToParameter(obj, myParser, varargin)
% PARSETOPARAMETER is a function that parses the parser (see doc
% inputParser) with varargin, and sets all matching results to the matching
% parameters of the object obj. This is useful for constructors of classes.
% If you want to use this method for protected or private properties,
%  copy it as a (Hidden) class method
	myParser.parse(varargin{:});
	objPropertyNames = properties(obj);
	myParserResultNames = fieldnames(myParser.Results);
	for it = 1:numel(myParserResultNames)
		if any(strcmp(myParserResultNames{it}, objPropertyNames))
			[obj.(myParserResultNames{it})] = ...
				deal(myParser.Results.(myParserResultNames{it}));
		end
	end
end