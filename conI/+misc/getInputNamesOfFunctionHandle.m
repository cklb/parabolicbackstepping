function inputNames = getInputNamesOfFunctionHandle(myHandle)
% this method extracts the names of the input variables of a
% function_handle. This is implemented by converting the handle into a
% string, and then seperating the string by comma between the first
% brackets. The output inputNames is a cell array in which the variable
% names are given in the order of the text.

% Example
% fun = @(z, zeta, x) sin(z)*zeta;
% misc.getInputNamesOfFunctionHandle(fun)
% ans = {'z'}    {'zeta'}    {'x'}

assert(numel(myHandle) == 1, 'Only implemented for scalar function_handles');
originalString = func2str(myHandle);
stringBetweenBrackets = originalString(min(strfind(originalString, '('))+1 : min(strfind(originalString, ')'))-1);
inputNames = strsplit(stringBetweenBrackets, ',');
end