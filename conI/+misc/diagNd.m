function value = diagNd(value, dimensionsToBeDiagonalized)
% diagNd returns the diagonal elements of all dimensions specified with
% dimensionsToBeDiagonalized. The result contains the diagonalized
% dimensions on the first dimension. Not-diagonalized dimensions of value
% follow afterwards.
%% Example 1
% misc.diagNd(ones(3))
% >> ans = [1; 1; 1]
%
%% Example 2
% testValue = ones(2, 2, 2);
% testValue(1,:,:) = 2;
% testValue(2,:,:) = 3;
% misc.diagNd(testValue)
% >> ans = [2; 3]
% misc.diagNd(testValue, [1, 2])
% >> ans = [2, 2; 3, 3]
% misc.diagNd(testValue, [2, 3])
% >> ans = [2, 3; 2, 3]

%% implementation
	if nargin < 2 % default: diagonalize over all dimensions
		dimensionsToBeDiagonalized = 1:ndims(value);
	end
	originalSize = size(value);
	originalOrder = 1:numel(originalSize);
	
	% permute value such that all dimensions that are considered for
	% diagonalization are at the beginning
	value = permute(value, [dimensionsToBeDiagonalized, ...
		originalOrder(all(originalOrder ~= dimensionsToBeDiagonalized(:)))]);
	
	% the result contains the diagonalized dimension first, then the
	% remaining
	tempValueSize = size(value);
	sizeResult = [size(value, 1), ...
		tempValueSize(numel(dimensionsToBeDiagonalized)+1 : end)];
	if numel(sizeResult) == 1
		sizeResult = [sizeResult, 1];
	end
	
	% an ndgrid is used to get the diagonal elements, since 
	% rowIndx = columnIndex has to hold even in the n-D-case.
	valueGridVectors = arrayfun(@(v) 1:v, size(value), 'UniformOutput', false);
	valueNdGrid = cell(1, numel(valueGridVectors));
	[valueNdGrid{:}] = ndgrid(valueGridVectors{:});
	
	% the result contains all elments whos indices are equal, 
	% in the 2d case rowIndx = columnIndex is equivalent to allEqual(...)
	value = value(allEqual(valueNdGrid(1:numel(dimensionsToBeDiagonalized))));
	
	% reshape to correct dimension
	value = reshape(value, sizeResult);
end

function result = allEqual(a)
	if numel(a) > 2
		result = (a{1} == a{2}) & allEqual(a(2:end));
	else
		result = (a{1} == a{2});
	end
		
end