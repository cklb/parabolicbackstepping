function [invFun, gof] = polyInv(f,rmseMax,nDisc)
% POLYINV symbolic inverse function using discretisation and curve fitting
% compute the symbolic inverse of a function by discretizing and using curve fitting.
%
% Description:
% invFun = polyInv(f,rmseMax) discretizes the function f. The order of 
% the polynomial approximation is increased as long as the root mean 
% square error is less then rmseMax
%
% invFun = polyInv(f,rmseMax,nDisc) uses nDisc grid points for the 
% discretisation. The argument of f is always normed to the range (0,1).
%
% [invFun, gof] = polyInv(...) 
% also returs the goodness of fit
%
% Inputs:
%     f           Symbolic function
%     rmseMax     Maximal allowed root mean square error
%     nDisc       Number of discretization points
% Outputs:
%     invFun      Inveres function of f
%     gof         Goodness of fit 
%
% Example:
% -------------------------------------------------------------------------
% syms z real
% fun = z^2;
% invfun = misc.polyInv(fun,0.025,1000);
% y = 0:0.01:1;
% inv = subs(invfun,'x',y);
% plot(y,inv,y,sqrt(y),'r-.');
% -------------------------------------------------------------------------

% created on 25.09.2018 by Simon Kerschbaum

% TODO: Dynamische Eingaben erm�glichen: Feste Polynomordnung, Art des Solvers, weitere
% Parameter f�r den Fit etc.

syms x

if nargin<3
	nDisc = 51;
end
if nargin<2
	rmseMax = 1e-3;
end

xdisc = linspace(0,1,nDisc);
fDisc = double(subs(f,xdisc));

polyOrder = 0;
rmse=inf;
while rmse>rmseMax
	polyOrder = polyOrder+1;
	if polyOrder>9
		warning('Cannot reach tolerances with Polynomial of order 9!')
		break
	end
	[fO, gof] = fit(fDisc(:),xdisc(:),['poly' num2str(polyOrder)]);
	rmse = gof.rmse;
end

maxp = ['p' num2str(polyOrder+1)];
invFun = fO.(maxp);
for j=1:polyOrder
	pnum = ['p' num2str(j)];
	invFun = invFun + fO.(pnum) *x^(polyOrder-j+1);
end

end

