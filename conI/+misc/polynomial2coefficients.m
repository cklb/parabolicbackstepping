function [ APoly ] = polynomial2coefficients(A, s_var, N)
warning('DEPRICATED: Use polyMatrix.polynomial2coefficients instead')
% OPERATOR2POLYNOMIAL Returns the coefficient matrices of an operator
%
% Inputs:
%     A       Operator matrix with polynomials A \in C[s]^(n x n)
%     s_var   symbolic variable
%     N       maximal degree to be regarded for the coefficients. If the
%             polynomial contains higher degree entries than *N*, 
%             the values are cut off.
% Outputs:
%     APoly   Coefficients
%
% Example:
% -------------------------------------------------------------------------
% import misc.*
% syms s;
% A = [10, s; 2*s^2, 3*s^3]
% polynomial2coefficients(A, s)
%   
% >> ans(:,:,1) = [10, 0]
%                 [ 0, 0]
% >> ans(:,:,2) = [ 0, 1]
%                 [ 0, 0]
% >> ans(:,:,3) = [ 0, 0]
%                 [ 2, 0]
% >> ans(:,:,4) = [ 0, 0]
%                 [ 0, 3]
% -------------------------------------------------------------------------
import misc.*
if ~exist('N', 'var')
    % determine maximal degree of spatial variable s
    sDegreeMax = -1;
    for i=1:numel(A)
        sDegreeMax = max([sDegreeMax, length(coeffsAll(A(i),s_var))]);
    end
    N=sDegreeMax;
end

% convert system-operator into polynomal expression
APoly = sym(zeros([size(A), N]));
for i=1:size(A,1)
    for j=1:size(A,2)
        tmp = coeffsAll(A(i,j), s_var);
        for k=1:N
            if k>length(tmp)
                APoly(i, j, k) = sym(0);
            else
                APoly(i, j, k) = sym(tmp(k));
            end
        end
    end
end