classdef  (InferiorClasses = {?quantity.Symbolic}) Discrete < handle & matlab.mixin.Copyable & matlab.mixin.CustomDisplay
	properties (SetAccess = protected)
		% Discrete evaluation of the continuous quantity
		valueDiscrete double;
		
		% Name of the domains that generate the grid.
		gridName {mustBe.unique};
	end
	
	properties (Hidden, Access = protected, Dependent)
		doNotCopy;
	end
	
	properties
		% Grid for the evaluation of the continuous quantity. For the
		% example with the function f(x,t), the grid would be
		%   {[<spatial domain>], [<temporal domain>]}
		% whereas <spatial domain> is the discret description of the
		% spatial domain and <temporal domain> the discrete description of
		% the temporal domain.
		grid; % in set.grid it is ensured that, grid is a (1,:)-cell-array
		
		% ID of the figure handle in which the handle is plotted
		figureID double = 1;
		
		% Name of this object
		name char;
	end
	
	methods
		%--------------------
		% --- Constructor ---
		%--------------------
		function obj = Discrete(valueOriginal, varargin)
			% The constructor requires valueOriginal to be
			% 1) a cell-array of double arrays with
			%	size(valueOriginal) == size(obj) and
			%	size(valueOriginal{it}) == gridSize
			% OR
			% 2) adouble-array with
			%	size(valueOriginal) == [gridSize, size(quantity)] 
			% Furthermore, 'gridName' must be part of the name-value-pairs
			% in varargin. Additional parameters can be specified using
			% name-value-pair-syntax in varargin.
			
			% to allow the initialization as object array, the constructor
			% must be allowed to be called without arguments, i.e. nargin == 0.
			% Then no parameters are set.
			if nargin == 1
				% if nargin == 1 it can be a conversion of child-classes or an empty
				% object
				if isa(valueOriginal, 'quantity.Discrete')
					% allows the conversion of a quantity object without
					% extra check if the object is already from class
					% quantity.Discrete
					obj = valueOriginal;
				else
					% empty object. this is needed for instance, to create
					% quantity.Discrete([]), which is useful for creating default 
					% values.
					obj = quantity.Discrete.empty(size(valueOriginal));
				end
			elseif nargin > 1
				
				%% input parser
				myParser = misc.Parser();
				myParser.addParameter('gridName', [], @(g) ischar(g) || iscell(g));
				myParser.addParameter('grid', [], @(g) isnumeric(g) || iscell(g));
				myParser.addParameter('name', string(), @isstr);
				myParser.addParameter('figureID', 1, @isnumeric);
				myParser.parse(varargin{:});
				assert(all(~contains(myParser.UsingDefaults, 'gridName')), ...
					'gridName is a mandatory input for quantity');
				
				if iscell(myParser.Results.gridName)
					myGridName = myParser.Results.gridName;
				else
					myGridName = {myParser.Results.gridName};
				end
				
				%% allow initialization of empty objects:
				valueOriginalSize = size(valueOriginal);
				if any(valueOriginalSize == 0)
					% If the size is specified in the arguements, it should
					% be chosen instead of the default size from the
					% valueOriginal.
					myParser = misc.Parser();
					myParser.addParameter('size', valueOriginalSize((1+numel(myGridName)):end));
					myParser.parse(varargin{:});
					obj = quantity.Discrete.empty(myParser.Results.size);
					return;
				end
				
				%% get the sizes of obj and grid
				if iscell(valueOriginal)
					if isempty(valueOriginal{1})
						% if valueOriginal is a cell-array with empty
						% cells, then grid must be specified as an input
						% parameter. This case is important for
						% constructing Symbolic or Function quantities
						% without discrete values.
						assert(all(~contains(myParser.UsingDefaults, 'grid')), ...
							['grid is a mandatory input for quantity, ', ...
							'if no discrete values are specified']);
						if ~iscell(myParser.Results.grid)
							gridSize = numel(myParser.Results.grid);
						else
							gridSize = cellfun(@(v) numel(v), myParser.Results.grid);
						end
					else
						gridSize = size(valueOriginal{1});
					end
					objSize = size(valueOriginal);
				elseif isnumeric(valueOriginal)
					gridSize = valueOriginalSize(1 : numel(myGridName));
					objSize = [valueOriginalSize(numel(myGridName)+1 : end), 1, 1];
				end
				
				%% get grid and check size
				if any(contains(myParser.UsingDefaults, 'grid'))
					myGrid = quantity.Discrete.defaultGrid(gridSize);
				else
					myGrid = myParser.Results.grid;
				end
				if ~iscell(myGrid)
					myGrid = {myGrid};
				end
				if isempty(myGridName) || isempty(myGrid)
					if ~(isempty(myGridName) && isempty(myGrid))
						error(['If one of grid and gridName is empty, ', ...
							'then both must be empty.']);
					end
				else
					assert(isequal(size(myGrid), size(myGridName)), ...
						'number of grids and gridNames must be equal');
					myGridSize = cellfun(@(v) numel(v), myGrid);
					assert(isequal(gridSize(gridSize>1), myGridSize(myGridSize>1)), ...
						'grids do not fit to valueOriginal');
				end
				
				%% set valueDiscrete
				if ~iscell(valueOriginal)
					valueOriginal = quantity.Discrete.value2cell(valueOriginal, gridSize, objSize);
				end
				for k = 1:prod(objSize)
					if numel(myGrid) == 1
						obj(k).valueDiscrete = valueOriginal{k}(:);
					else
						obj(k).valueDiscrete = valueOriginal{k};
					end
				end
				
				%% set further properties
				[obj.grid] = deal(myGrid);
				[obj.gridName] = deal(myGridName);
				[obj.name] = deal(myParser.Results.name);
				[obj.figureID] = deal(myParser.Results.figureID);
				
				%% reshape object from vector to matrix
				obj = reshape(obj, objSize);
			end
		end% Discrete() constructor

		%---------------------------
		% --- getter and setters ---
 		%---------------------------
		function itIs = isConstant(obj)
			% the quantity is interpreted as constant if it has no grid or
			% it has a grid that is only one point.
			itIs = isempty(obj.gridSize) || prod(obj.gridSize) == 1;
		end
		function doNotCopy = get.doNotCopy(obj)
			doNotCopy = obj.doNotCopyPropertiesName();
		end
		function set.gridName(obj, name)
			if ~iscell(name)
				name = {name};
			end
			obj.gridName = name;
		end
	
		function set.grid(obj, grid)
			if ~iscell(grid)
				grid = {grid};
			end
			
			isV = cellfun(@(v) (sum(size(v)>1) == 1) || (numel(v) == 1), grid); % also allow 1x1x1x41 grids
			assert(all(isV(:)), 'Please use vectors for the grid entries!');
			
			[obj.grid] = deal(grid);
		end
		function valueDiscrete = get.valueDiscrete(obj)
			% check if the value discrete for this object
			% has already been computed.
			empty = isempty(obj.valueDiscrete);
			if any(empty(:))
				obj.valueDiscrete = obj.on(obj.grid);
			end
			valueDiscrete = obj.valueDiscrete;
		end
		
		%-------------------
		% --- converters ---
		%-------------------
		function exportData = exportData(obj, varargin)
			
			% make the object names:
			if obj.nargin == 1
				headers = cell(1, numel(obj) + 1);
				headers{1} = obj(1).gridName{1};
				for i= 1:numel(obj) %TODO use easier to read headers
					headers{i+1} = [obj(i).name '' num2str(i)];
				end
				exportData = export.dd(...
					'M', [obj.grid{:}, obj.valueDiscrete], ...
					'header', headers, varargin{:});
			elseif obj.nargin == 2
				error('Not yet implemented')
			else
				error('Not yet implemented')
			end
		end
		function d = double(obj)
			d = obj.on();
		end
		function o = quantity.Function(obj)
			props = nameValuePair( obj(1) );
		
			for k = 1:numel(obj)
				F = griddedInterpolant(obj(k).grid{:}', obj(k).on());
				o(k) = quantity.Function(@(varargin) F(varargin{:}), ...
					props{:});
			end
			
			o = reshape(o, size(obj));
		end
		function o = quantity.Operator(obj)
			A = cell(size(obj, 3), 1);
			for k = 1:size(obj, 3)
				A{k} = obj(:,:,k);
			end
			o = quantity.Operator(A);
		end
		
		function o = quantity.Symbolic(obj)
			if isempty(obj)
				o = quantity.Symbolic.empty(size(obj));
			else
				error('Not yet implemented')
			end
		end
			
		function obj = setName(obj, newName)
			% Function to set all names of all elements of the quantity obj to newName.
			[obj.name] = deal(newName);
		end % setName()
	end
	
	methods (Access = public)
		function value = on(obj, myGrid, myGridName)
			% TODO es sieht so aus als w�rde die Interpolation bei
			% konstanten werten ziemlichen Quatsch machen!
			%	Da muss man nochmal ordentlich drauf schauen!
			if isempty(obj)
				value = zeros(size(obj));
			else
				if nargin == 1
					myGrid = obj(1).grid;
					myGridName = obj(1).gridName;
				elseif nargin >= 2 && ~iscell(myGrid)
					myGrid = {myGrid};
				end
				gridPermuteIdx = 1:obj(1).nargin;
				if nargin == 3
					if ~iscell(myGridName)
						myGridName = {myGridName};
					end
					assert(numel(myGrid) == numel(myGridName), ...
						['If on() is called by using gridNames as third input', ...
						', then the cell-array of grid and gridName must have ', ...
						'equal number of elements.']);
					assert(numel(myGridName) == obj(1).nargin, ...
						'All (or none) gridName must be specified');
					gridPermuteIdx = cellfun(@(v) obj(1).gridIndex(v), myGridName);
					myGrid = myGrid(gridPermuteIdx);
				end
				
				value = obj.obj2value();
				
				if nargin >= 2 && (prod(obj(1).gridSize) > 1)
					indexGrid = arrayfun(@(s)linspace(1,s,s), size(obj), 'UniformOutput', false);
					tempInterpolant = numeric.interpolant(...
						[obj(1).grid, indexGrid{:}], value);
					value = tempInterpolant.evaluate(myGrid{:}, indexGrid{:});
				elseif obj.isConstant
					value = repmat(value, [cellfun(@(v) numel(v), myGrid), ones(1, length(size(obj)))]);
					gridPermuteIdx = 1:numel(myGrid);
				end
				value = permute(reshape(value, [cellfun(@(v) numel(v), myGrid), size(obj)]), ...
					[gridPermuteIdx, numel(gridPermuteIdx)+(1:ndims(obj))]);
			end
		end
		
		function interpolant = interpolant(obj)
			% get the interpolant of the obj;
			if isempty(obj)
				value = zeros(size(obj));
				indexGrid = arrayfun(@(s)linspace(1,s,s), size(obj), 'UniformOutput', false);
				interpolant = numeric.interpolant(...
						[indexGrid{:}], value);
			else
				myGrid = obj(1).grid;			
				value = obj.obj2value();
				indexGrid = arrayfun(@(s)linspace(1,s,s), size(obj), 'UniformOutput', false);
				interpolant = numeric.interpolant(...
						[myGrid, indexGrid{:}], value);
			end
		end
		
		
		function assertSameGrid(a, varargin)
			% check if all elements of a have same grid and gridName. If
			% further quantites are inputs via varargin, it is verified if
			% that quantity has same grid and gridName as quantity a as
			% well.
			if isempty(a)
				if nargin > 1
					varargin{1}.assertSameGrid(varargin{2:end});
				end
				return;
			else
				referenceGridName = a(1).gridName;
				referenceGrid= a(1).grid;
			end
			for it = 1 : numel(a)
				assert(isequal(referenceGridName, a(it).gridName), ...
					'All elements of a quantity must have same gridNames');
				assert(isequal(referenceGrid, a(it).grid), ...
					'All elements of a quantity must have same grid');
			end
			if nargin > 1
				b = varargin{1};
				for it = 1 : numel(b)
					assert(isequal(referenceGridName, b(it).gridName), ...
						'All elements of a quantity must have same gridNames');
					assert(isequal(referenceGrid, b(it).grid), ...
						'All elements of a quantity must have same grid');
				end
			end
			if nargin > 2
				% if more then 1 quantity is in varargin, they are checked
				% iteratively by calling assertSameGrid() again.
				assertSameGrid(varargin{:});
			end
		end
		
		function [referenceGrid, referenceGridName] = getFinestGrid(a, varargin)
			% find the finest grid of all input quantities by comparing
			% gridSize for each iteratively.
			
			if isempty(a) || isempty(a(1).grid)
				if nargin > 1
					[referenceGrid, referenceGridName] = varargin{1}.getFinestGrid(varargin{2:end});
				else
					referenceGrid = {};
				referenceGridName = '';
				end
				return;
			else
				referenceGridName = a(1).gridName;
				referenceGrid = a(1).grid;
				referenceGridSize = a(1).gridSize(referenceGridName);				
			end

			for it = 1 : numel(varargin)
				if isempty(varargin{it})
					continue;
				end
				assert(numel(referenceGridName) == numel(varargin{it}(1).gridName), ...
					['For getFinestGrid, the gridName of all objects must be equal', ...
					'. Maybe gridJoin() does what you want?']);
				comparisonGridSize = varargin{it}.gridSize(referenceGridName);
				for jt = 1 : numel(referenceGridName)
					comparisonGrid = varargin{it}.gridOf(referenceGridName{jt});
					assert(referenceGrid{jt}(1) == comparisonGrid(1), 'Grids must have same domain for combining them')
					assert(referenceGrid{jt}(end) == comparisonGrid(end), 'Grids must have same domain for combining them')
					if comparisonGridSize(jt) > referenceGridSize(jt)
						referenceGrid{jt} = comparisonGrid;
						referenceGridSize(jt) = comparisonGridSize(jt);
					end
				end
			end
		end
		
		function [gridJoined, gridNameJoined] = gridJoin(obj1, obj2)
			%% gridJoin combines the grid and gridName of two objects (obj1,
			% obj2), such that every gridName only occurs once and that the
			% finer grid of both is used.
			
			gridNameJoined = unique([obj1(1).gridName, obj2(1).gridName]);
			gridJoined = cell(1, numel(gridNameJoined));
			for it = 1 : numel(gridNameJoined)
				currentGridName = gridNameJoined{it};
				[index1, lolo1] = obj1.gridIndex(currentGridName);
				[index2, lolo2] = obj2.gridIndex(currentGridName);
				if ~any(lolo1)
					gridJoined{it} = obj2(1).grid{index2};
				elseif ~any(lolo2)
					gridJoined{it} = obj1(1).grid{index1};
				else
					tempGrid1 = obj1(1).grid{index1};
					tempGrid2 = obj2(1).grid{index2};
					
					if ~obj1.isConstant && ~obj2.isConstant				
					assert(tempGrid1(1) == tempGrid2(1), 'Grids must have same domain for gridJoin')
					assert(tempGrid1(end) == tempGrid2(end), 'Grids must have same domain for gridJoin')
					end
					if numel(tempGrid1) > numel(tempGrid2)
						gridJoined{it} = tempGrid1;
					else
						gridJoined{it} = tempGrid2;
					end
				end
			end
		end % gridJoin()
		function obj = sort(obj, varargin)
			%SORT sorts the grid of the object in a desired order
			% obj = sortGrid(obj) sorts the grid in alphabetical order. 
			% obj = sort(obj, 'descend') sorts the grid in descending
			% alphabetical order.
				
			if nargin == 2 && strcmp(varargin{1}, 'descend')
				descend = 1;
			else
				descend = 0;
			end
			
			% only sort the grids if there is something to sort
			if obj(1).nargin > 1
				gridNames = obj(1).gridName;
				
				% this is the default case for ascending alphabetical
				% order
				[sortedNames, I] = sort(gridNames);

				% if descending: flip the order of the entries
				if descend 
					sortedNames = flip(sortedNames);
					I = flip(I);
				end

				% sort the grid entries
				[obj.grid] = deal(obj(1).grid(I));

				% assign the new grid names
				[obj.gridName] = deal(sortedNames);

				% permute the value discrete
				for k = 1:numel(obj)
					obj(k).valueDiscrete = permute(obj(k).valueDiscrete, I);
				end	
			end
		end% sort()
		function c = horzcat(a, varargin)
			%HORZCAT Horizontal concatenation.
			%   [A B] is the horizontal concatenation of objects A and B
			%   from the class quantity.Discrete. A and B must have the
			%   same number of rows and the same grid. [A,B] is the same
			%   thing. Any number of matrices can be concatenated within
			%   one pair of brackets. Horizontal and vertical concatenation
			%   can be combined together as in [1 2;3 4].
			%
			%   [A B; C] is allowed if the number of rows of A equals the
			%   number of rows of B and the number of columns of A plus the
			%   number of columns of B equals the number of columns of C.
			%   The matrices in a concatenation expression can themselves
			%   by formed via a concatenation as in [A B;[C D]].  These
			%   rules generalize in a hopefully obvious way to allow fairly
			%   complicated constructions.
			%
			%   N-D arrays are concatenated along the second dimension. The
			%   first and remaining dimensions must match.
			%
			%   C = HORZCAT(A,B) is called for the syntax '[A  B]' when A
			%   or B is an object.
			%
			%   Y = HORZCAT(X1,X2,X3,...) is called for the syntax '[X1 X2
			%   X3 ...]' when any of X1, X2, X3, etc. is an object.
			%
			%	See also HORZCAT, CAT.
			 c = cat(2, a, varargin{:});
		end
		function c = vertcat(a, varargin)
			%VERTCAT Vertical concatenation.
			%   [A;B] is the vertical concatenation of objects A and B from
			%   the class quantity.Discrete. A and B must have the same
			%   number of columns and the same grid. Any number of matrices
			%   can be concatenated within one pair of brackets. Horizontal
			%   and vertical concatenation can be combined together as in
			%   [1 2;3 4].
			%
			%   [A B; C] is allowed if the number of rows of A equals the
			%   number of rows of B and the number of columns of A plus the
			%   number of columns of B equals the number of columns of C.
			%   The matrices in a concatenation expression can themselves
			%   by formed via a concatenation as in [A B;[C D]].  These
			%   rules generalize in a hopefully obvious way to allow fairly
			%   complicated constructions.
			%
			%   N-D arrays are concatenated along the first dimension. The
			%   remaining dimensions must match.
			%
			%   C = VERTCAT(A,B) is called for the syntax '[A; B]' when A
			%   or B is an object.
			%
			%   Y = VERTCAT(X1,X2,X3,...) is called for the syntax '[X1;
			%   X2; X3; ...]' when any of X1, X2, X3, etc. is an object.
			%
			%   See also HORZCAT, CAT.
			c = cat(1, a, varargin{:});
		end
		function c = cat(dim, a, varargin)
			%CAT Concatenate arrays.
			%   CAT(DIM,A,B) concatenates the arrays of objects A and B
			%   from the class quantity.Discrete along the dimension DIM.
			%   CAT(2,A,B) is the same as [A,B]. CAT(1,A,B) is the same as
			%   [A;B].
			%
			%   B = CAT(DIM,A1,A2,A3,A4,...) concatenates the input arrays
			%   A1, A2, etc. along the dimension DIM.
			%
			%   When used with comma separated list syntax, CAT(DIM,C{:})
			%   or CAT(DIM,C.FIELD) is a convenient way to concatenate a
			%   cell or structure array containing numeric matrices into a
			%   single matrix.
			%
			%   Examples:
			%     a = magic(3); b = pascal(3); 
			%     c = cat(4,a,b)
			%   produces a 3-by-3-by-1-by-2 result and
			%     s = {a b};
			%     for i=1:length(s), 
			%       siz{i} = size(s{i});
			%     end
			%     sizes = cat(1,siz{:})
			%   produces a 2-by-2 array of size vectors.

			if nargin == 1
				objCell = {a};			
			else
				objCell = [{a}, varargin(:)'];
				
				% this function has the very special thing that it a does
				% not have to be an quantity.Discrete object. So it has to
				% be checked which of the input arguments is an
				% quantity.Discrete object. This is considered to give
				% the basic values for the initialization of new
				% quantity.Discrete values
				isAquantityDiscrete = cellfun(@(o) isa(o, 'quantity.Discrete'), objCell);
				isEmpty = cellfun(@(o) isempty(o), objCell);
				objIdx = find(isAquantityDiscrete & (~isEmpty), 1);
				
				if all(isEmpty)
					% if there are only empty entries, nothing can be
					% concatenated, so a new empty object is initialized.
					s = cellfun(@(o) size(o), objCell, 'UniformOutput', false);
					if dim == 1
					S = sum(cat(3, s{:}), 3);
					elseif dim == 2
						S = s{1};
					else
						error('Not implemented')
					end
					c = quantity.Discrete.empty(S);
					return
				else
					obj = objCell{objIdx};
				end
				
				for k = 1:numel(objCell(~isEmpty))
					
					if isa(objCell{k}, 'quantity.Discrete')
						o = objCell{k};
					else
						value = objCell{k};
						for l = 1:numel(value)
							M(:,l) = repmat(value(l), prod(obj(1).gridSize), 1);
						end
						if isempty(value)
							M = zeros([prod(obj(1).gridSize), size(value(l))]);
						end
							M = reshape(M, [obj(1).gridSize, size(value)]);
						o = quantity.Discrete( M, ...
							'size', size(value), ...
							'gridName', obj(1).gridName, ...
							'grid', obj(1).grid);
					end
					
					objCell{k} = o;
				end
				
			end
			
			% sort the grid names of each quantity
			for it = 1: (numel(varargin) + 1)
				objCell{it} = objCell{it}.sort;
			end
			
			[fineGrid, fineGridName] = getFinestGrid(objCell{~isEmpty});
			for it = 1 : (numel(varargin) + 1)  % +1 because the first entry is a
				% change the grid to the finest
				objCell{it} = objCell{it}.changeGrid(fineGrid, fineGridName);
			end
			assertSameGrid(objCell{:});
			argin = [{dim}, objCell(:)'];
			c = builtin('cat', argin{:});
		end
		
		function Y = blkdiag(A, varargin)
			% blkdiag  Block diagonal concatenation of matrix input arguments.
			%									|A 0 .. 0|
			% Y = blkdiag(A,B,...)  produces	|0 B .. 0|
			%									|0 0 ..  |
			% Yet, A, B, ... must have the same gridName and grid.
			if nargin == 1
				Y = copy(A);
			else
				B = varargin{1};
				if isempty(B)
					Y = A;
				else
					assert(isequal(A(1).gridName, B(1).gridName), 'only implemented for same grid and gridName');
					assert(isequal(A(1).grid, B(1).grid), 'only implemented for same grid and gridName');
					Y = [A, zeros(size(A, 1), size(B, 2)); ...
						zeros(size(B, 1), size(A, 2)), B];
				end
				if nargin > 2
					Y = blkdiag(Y, varargin{2:end});
				end
			end
		end % blkdiag()
		
		function solution = solveAlgebraic(obj, rhs, gridName, objLimit)
			%% this method solves
			%	obj(gridName) == rhs
			% for the variable specified by gridName.
			% rhs must be of apropriate size and gridName must
			% be an gridName of obj. If the result is constant (i.e., if
			% obj only depends on variable, then a double array is
			% returned. Else the solution is of the type as obj.
			% Yet, this is only implemented for obj with one variable
			% (grid) (see quantity.invert-method).
			% The input objLimit specifies minimum and maximum of the
			% values of obj, between which the solution should be searched.
			assert(numel(obj(1).gridName) == 1);
			assert(isequal(size(obj), [1, 1]));
			
			if ~isequal(size(rhs), size(obj))
				error('rhs has not the same size as quantity');
			end
			if ~iscell(gridName)
				gridName = {gridName};
			end
			if numel(gridName) ~= 1
				error('this function can only solve for one variable');
			end
			if isempty(strcmp(obj(1).gridName, gridName{1}))
				error('quantity does not depend on variable');
			end
			
			if nargin == 4
				assert(numel(objLimit)==2, 'a lower and upper limit must be specified (or neither)');
				objValueTemp = obj.on();
				gridSelector = (objValueTemp >= objLimit(1)) & (objValueTemp <= objLimit(2));
				gridSelector([max(1, find(gridSelector, 1, 'first')-1), ...
					min(find(gridSelector, 1, 'last')+1, numel(gridSelector))]) = 1;
				limitedGrid = obj(1).grid{1}(gridSelector);
				objCopy = obj.copy();
				objCopy = objCopy.changeGrid({limitedGrid}, gridName);
				objInverseTemp = objCopy.invert(gridName);
			else
				objInverseTemp = obj.invert(gridName);
			end
			
			solution = objInverseTemp.on(rhs);

% 			solution = zeros(numel(obj), 1);
% 			for it = 1 : numel(obj)
% 				objInverseTemp = obj(it).invert(gridName);
% 				solution(it) = objInverseTemp.on(rhs(it));				
% 			end
% 			solution = reshape(solution, size(obj));
		end
		
		function inverse = invert(obj, gridName)
			% inverse solves the function representet by the quantity for
			% its variable, for instance, if obj represents y = f(x), then
			% invert returns an object containing x = f^-1(y).
			% Yet, this is only implemented for obj with one variable
			% (grid).
			if iscell(gridName)
				% fixme: by default the first gridName is chosen as new
				% name. This works because the functions is only written
				% for quantities with one variable.
				gridName = gridName{1};
			end
			
			assert(numel(obj(1).gridName) == 1);
			assert(isequal(size(obj), [1, 1]));
			inverse = quantity.Discrete(repmat(obj(1).grid{obj.gridIndex(gridName)}(:), [1, size(obj)]), ...
				'size', size(obj), 'grid', obj.on(), 'gridName', {[obj(1).name]}, ...
				'name', gridName); 
			
		end
		
		function solution = solveDVariableEqualQuantity(obj, varargin)
			%% solves the first order ODE
			%	dvar / ds = obj(var(s))
			%	var(s=0) = ic
			% for var(s, ic). Herein, var is the (only) continuous variale
			% obj.variable. The initial condition of the IVP is a variable
			% of the result var(s, ic).
			assert(numel(obj(1).gridName) == 1, ...
				'this method is only implemented for quanitities with one gridName');
			
			myParser = misc.Parser();
			myParser.addParameter('initialValueGrid', obj(1).grid{1});
			myParser.addParameter('variableGrid', obj(1).grid{1});
			myParser.addParameter('newGridName', 's');
			myParser.addParameter('RelTol', 1e-6);
			myParser.addParameter('AbsTol', 1e-6);
			myParser.parse(varargin{:});
			
			variableGrid = myParser.Results.variableGrid;
			myGridSize = [numel(variableGrid), ... 
				numel(myParser.Results.initialValueGrid)];
			
			% the time (s) vector has to start at 0, to ensure the IC. If
			% variableGrid does not start with 0, it is separated in
			% negative and positive parts and later combined again.
			positiveVariableGrid = [0, variableGrid(variableGrid > 0)];
			negativeVariableGrid = [0, flip(variableGrid(variableGrid < 0))];
			
			% solve ode for every entry in obj and for every initial value
			options = odeset('RelTol', myParser.Results.RelTol, 'AbsTol', myParser.Results.AbsTol);
			odeSolution = zeros([myGridSize, numel(obj)]);
			for it = 1:numel(obj)
				for icIdx = 1:numel(myParser.Results.initialValueGrid)
					resultGridPositive = [];
					odeSolutionPositive = [];
					resultGridNegative = [];
					odeSolutionNegative = [];
					if numel(positiveVariableGrid) > 1
						[resultGridPositive, odeSolutionPositive] = ...
							ode45(@(y, z) obj(it).on(z), ...
								positiveVariableGrid, ...
								myParser.Results.initialValueGrid(icIdx), options);
					end
					if numel(negativeVariableGrid) >1
						[resultGridNegative, odeSolutionNegative] = ...
							ode45(@(y, z) obj(it).on(z), ...
								negativeVariableGrid, ...
								myParser.Results.initialValueGrid(icIdx), options);
					end
					if any(variableGrid == 0)
						resultGrid = [flip(resultGridNegative(2:end)); 0 ; resultGridPositive(2:end)];
						odeSolution(:, icIdx, it) = [flip(odeSolutionNegative(2:end)); ...
							myParser.Results.initialValueGrid(icIdx); odeSolutionPositive(2:end)];
					else
						resultGrid = [flip(resultGridNegative(2:end)); resultGridPositive(2:end)];
						odeSolution(:, icIdx, it) = [flip(odeSolutionNegative(2:end)); ...
							odeSolutionPositive(2:end)];
					end
					assert(isequal(resultGrid(:), variableGrid(:)));
				end
			end
			
			% return result as quantity-object
			solution = quantity.Discrete(...
				reshape(odeSolution, [myGridSize, size(obj)]), ...
				'gridName', {myParser.Results.newGridName, 'ic'}, 'grid', ...
				{variableGrid, myParser.Results.initialValueGrid}, ...
				'size', size(obj), 'name', ['solve(', obj(1).name, ')']);
		end
			
		function solution = subs(obj, gridName2Replace, values)
			if nargin == 1 || isempty(gridName2Replace)
				% if gridName2Replace is empty, then nothing must be done.
				solution = obj;
			elseif isempty(obj)
				% if the object is empty, nothing must be done.
				solution = obj;
			else
				% input checks
				assert(nargin == 3, ['Wrong number of input arguments. ', ...
					'gridName2Replace and values must be cell-arrays!']);
				if ~iscell(gridName2Replace)
					gridName2Replace = {gridName2Replace};
				end
				if ~iscell(values)
					values = {values};
				end
				assert(numel(values) == numel(gridName2Replace), ...
					'gridName2Replace and values must be of same size');
				
				% here substitution starts: 
				% The first (gridName2Replace{1}, values{1})-pair is 
				% replaced. If there are more cell-elements in those inputs
				% then subs() is called again for the remaining pairs
				% (gridName2Replace{2:end}, values{2:end}).
				if ischar(values{1})
					% if values{1} is a char-array, then the gridName is
					% replaced
					if any(strcmp(values{1}, gridName2Replace(2:end)))
						% in the case if a quantity f(z, zeta) should be
						% substituted like subs(f, {z, zeta}, {zeta, z})
						% this would cause an error, since after the first
						% substituion subs(f, z, zeta) the result would be
						% f(zeta, zeta) -> the 2nd subs(f, zeta, z) will
						% result in f(z, z) and not in f(zeta, z) as
						% intended. This is solved, by an additonal
						% substitution:
						values{end+1} = values{1};
						gridName2Replace{end+1} = [gridName2Replace{1}, 'backUp'];
						values{1} = [gridName2Replace{1}, 'backUp'];
					end
					if isequal(values{1}, gridName2Replace{1})
						% replace with same variable... everything stay the
						% same.
						newGrid = obj(1).grid;
						newGridName = obj(1).gridName;
						newValue = obj.on();
					elseif any(strcmp(values{1}, obj(1).gridName))
						% if for a quantity f(z, zeta) this method is
						% called with subs(f, zeta, z), then g(z) = f(z, z)
						% results, hence the dimensions z and zeta are
						% merged.
						gridIndices = [obj(1).gridIndex(gridName2Replace{1}), ...
							obj(1).gridIndex(values{1})];
						newGridForOn = obj(1).grid;
						if numel(obj(1).grid{gridIndices(1)}) > numel(obj(1).grid{gridIndices(2)})
							newGridForOn{gridIndices(2)} = newGridForOn{gridIndices(1)};
						else
							newGridForOn{gridIndices(1)} = newGridForOn{gridIndices(2)};
						end
						newValue = misc.diagNd(obj.on(newGridForOn), gridIndices);
						newGrid = {newGridForOn{gridIndices(1)}, ...
							newGridForOn{1:1:numel(newGridForOn) ~= gridIndices(1) ...
							 & 1:1:numel(newGridForOn) ~= gridIndices(2)}};
						newGridName = {values{1}, ...
							obj(1).gridName{1:1:numel(obj(1).gridName) ~= gridIndices(1) ...
							 & 1:1:numel(obj(1).gridName) ~= gridIndices(2)}};
						
					else
						% this is the default case. just grid name is
						% changed.
						newGrid = obj(1).grid;
						newGridName = obj(1).gridName;
						newGridName{obj(1).gridIndex(gridName2Replace{1})} ...
							= values{1};
						newValue = obj.on();
					end
					
				elseif isnumeric(values{1}) && numel(values{1}) == 1
					% if values{1} is a scalar, then obj is evaluated and
					% the resulting quantity loses that spatial grid and
					% gridName
					newGridName = obj(1).gridName;
					newGridName = newGridName(~strcmp(newGridName, gridName2Replace{1}));
					% newGrid is the similar to the original grid, but the
					% grid of gridName2Replace is removed.
					newGrid = obj(1).grid;
					newGrid = newGrid((1:1:numel(newGrid)) ~= obj.gridIndex(gridName2Replace{1}));
					newGridSize = cellfun(@(v) numel(v), newGrid);
					% newGridForOn is the similar to the original grid, but
					% the grid of gridName2Replace is set to values{1} for
					% evaluation of obj.on().
					newGridForOn = obj(1).grid;
					newGridForOn{obj.gridIndex(gridName2Replace{1})} = values{1};
					newValue = reshape(obj.on(newGridForOn), [newGridSize, size(obj)]);
					
				elseif isnumeric(values{1}) && numel(values{1}) > 1
					% if values{1} is a double vector, then the grid is
					% replaced.
					newGrid = obj(1).grid;
					newGrid{obj.gridIndex(gridName2Replace{1})} = values{1};
					newGridName = obj(1).gridName;
					newValue = obj.on(newGrid);
				else
					error('value must specify a gridName or a gridPoint');
				end
				if isempty(newGridName)
					solution = newValue;
				else
					solution = quantity.Discrete(newValue, ...
						'grid', newGrid, 'gridName', newGridName, ...
						'name', obj(1).name);
				end
				if numel(gridName2Replace) > 1
					solution = solution.subs(gridName2Replace(2:end), values(2:end));
				end
			end
			
		end
		
		function value = at(obj, point)
			% at() evaluates the object at one point and returns it as array 
			% with the same size as size(obj).
			value = reshape(obj.on(point), size(obj));
		end
		
		function value = atIndex(obj, varargin)
			% ATINDEX returns the valueDiscrete at the requested index.
			% value = atIndex(obj, varargin) returns the
			% quantity.Discrete.valueDiscrete at the index defined by
			% varargin.  
			%	value = atIndex(obj, 1) returns the first element of
			%	"valueDiscrete"
			%	value = atIndex(obj, ':') returns all elements of
			%	obj.valueDiscrete in vectorized form. 
			%	value = atIndex(obj, 1, end) returns the obj.valueDiscrete
			%	at the index (1, end).
			%	If a range of index is requested, the result is returned
			%	with the grids as indizes. If scalar values are requested,
			%	than the grid dimensions are neglected.
			if nargin == 1
				
				if numel(obj.gridSize) == 1
					value = zeros(obj.gridSize, 1);
				else
					value = zeros(obj.gridSize, 1);
				end
				if isempty(value)
					value = 0;
				end
			else
				if ~iscell(varargin)
					varargin = {varargin};
				end
				value = cellfun(@(v) v(varargin{:}), {obj.valueDiscrete}, ...
					'UniformOutput', false);
				
				valueSize = size(value{1});
								
				if all(cellfun(@numel, varargin) == 1) && all(cellfun(@isnumeric, varargin))
					outputSize = [];
				else
					outputSize = valueSize(1:obj(1).nargin);
			end

				value = reshape([value{:}], [outputSize, size(obj)]);
		end
		end
		
		function n = nargin(obj)
			% FIXME: check if all funtions in this object have the same
			% number of input values.
			
			% FIXME: for some combinations of constant objects, it seems to be
			% possible, that the quantity has a gridName but no grid.
			% Actually this should not be allowed. This is quick and dirty
			% work around.
			n = min(numel(obj(1).gridName), numel(obj(1).grid));
		end
		
		function d = gridDiff(obj)
			
			% #FIXME:
			%   1) test for multidimensional grids
			%   2) check that the grid is equally spaced
			
			d = diff(obj(1).grid{:});
			d = d(1);
		end
		
		function s = gridSize(obj, myGridName)
			% GRIDSIZE returns the size of all grid entries.
			% todo: this should be called gridLength
			if isempty(obj(1).grid)
				s = [];
			else
				if nargin == 1
					s = cellfun('length', obj(1).grid);
				elseif nargin == 2
					s = cellfun('length', obj(1).gridOf(myGridName));
				end
			end
		end
		
		function matGrid = ndgrid(obj, grid)
			% ndgrid calles ndgrid for the default grid, if no other grid
			% is specified. Empty grid as input returns empty cell as
			% result.
			if nargin == 1
				grid = obj.grid;
			end
			if isempty(grid)
				matGrid = {};
			else
				[matGrid{1:obj.nargin}] = ndgrid(grid{:});
			end
		end % ndgrid()
		
		function H = plot(obj, varargin)
			H = [];
			p = misc.Parser();
			p.addParameter('fig', []);
			p.addParameter('dock', quantity.Settings.instance().plot.dock);
			p.addParameter('showTitle', quantity.Settings.instance().plot.showTitle);
			p.addParameter('titleWithIndex', true');
			p.addParameter('hold', false);
			p.addParameter('export', false);
			p.addParameter('exportOptions',  ...
				{'height', [num2str(0.25*size(obj, 1)), '\textwidth'], ...
				'width', '0.8\textwidth', 'externalData', false, ...
				'showWarnings', false, 'showInfo', false, ...
				'extraAxisOptions', 'every axis title/.append style={yshift=-1.5ex}, every axis x label/.append style={yshift=2mm}'});
			p.parse(varargin{:});
			additionalPlotOptions = misc.struct2namevaluepair(p.Unmatched);
			if prod(obj.gridSize) == 1
				additionalPlotOptions = [additionalPlotOptions(:)', ...
					{'x'}];
			end
			
			fig = p.Results.fig;
			dock = p.Results.dock;
			for figureIdx = 1:size(obj, 3)
				if isempty(p.Results.fig)
					h = figure();
				elseif p.Results.fig == 0
					h = gcf;
				else
					h = figure(fig + figureIdx - 1);
				end
				H = [H, h];
				
				if dock
					set(h, 'WindowStyle', 'Docked');
				end
				
				assert(~isempty(obj), 'Empty quantities can not be plotted');
				assert(obj.nargin() <= 2, 'plot only supports quantities with 2 gridNames');
				
				subplotRowIdx = 1:size(obj, 1);
				subpotColumnIdx = 1:size(obj, 2);
				
				i = 1: numel(obj(:,:,figureIdx));
				i = reshape(i, size(obj, 2), size(obj, 1))';
				
				for rowIdx = subplotRowIdx
					for columnIdx = subpotColumnIdx
						subplot(size(obj, 1), size(obj, 2), i(rowIdx, columnIdx));
						if p.Results.hold
							hold on;
						else
							hold off;
						end
						
						if isempty(obj(rowIdx, columnIdx, figureIdx))
							warning('you are trying to plot an empty quantity');
						elseif obj.nargin() == 0
							plot(0, ...
								obj(rowIdx, columnIdx, figureIdx).valueDiscrete, ...
								additionalPlotOptions{:});
						elseif obj.nargin() == 1
							plot(...
								obj(rowIdx, columnIdx, figureIdx).grid{1}(:), ...
								obj(rowIdx, columnIdx, figureIdx).valueDiscrete, ...
								additionalPlotOptions{:});
						elseif obj.nargin() == 2
							misc.isurf(obj(rowIdx, columnIdx, figureIdx).grid{1}(:), ...
								obj(rowIdx, columnIdx, figureIdx).grid{2}(:), ...
								obj(rowIdx, columnIdx, figureIdx).valueDiscrete, ...
								additionalPlotOptions{:});
							ylabel(labelHelper(2), 'Interpreter','latex');
						else
							error('number inputs not supported');
						end
						xlabel(labelHelper(1), 'Interpreter','latex');
						
						if p.Results.showTitle
						title(titleHelper(), 'Interpreter','latex');
						end
						a = gca();
						a.TickLabelInterpreter = 'latex';
						
					end % for columnIdx = subpotColumnIdx
				end % for rowIdx = subplotRowIdx
				
			end % for figureIdx = 1:size(obj, 3)
			
			if p.Results.export
				matlab2tikz(p.Results.exportOptions{:});
			end
		
			function myLabel = labelHelper(gridNumber)
				if ~isempty(obj(rowIdx, columnIdx, figureIdx).gridName)
				myLabel = ['$$', greek2tex(obj(rowIdx, columnIdx, figureIdx).gridName{gridNumber}), '$$'];
				else
					myLabel = '';
				end
			end % labelHelper()
			function myTitle = titleHelper()
				if numel(obj) <= 1 || ~p.Results.titleWithIndex
					myTitle = ['$${', greek2tex(obj(rowIdx, columnIdx, figureIdx).name), '}$$'];
				elseif ndims(obj) <= 2
					myTitle = ['$$[{', greek2tex(obj(rowIdx, columnIdx, figureIdx).name), ...
						']}_{', num2str(rowIdx), num2str(columnIdx), '}$$'];
				else
					myTitle = ['$${[', greek2tex(obj(rowIdx, columnIdx, figureIdx).name), ...
						']}_{', num2str(rowIdx), num2str(columnIdx), num2str(figureIdx), '}$$'];
				end
			end % titleHelper()
			function myText = greek2tex(myText)
				if ~contains(myText, '\')
					myText = strrep(myText, 'Lambda', '\Lambda');
					myText = strrep(myText, 'lambda', '\lambda');
					myText = strrep(myText, 'Zeta', '\Zeta');
					myText = strrep(myText, 'zeta', '\zeta');
					myText = strrep(myText, 'Gamma', '\Gamma');
					myText = strrep(myText, 'gamma', '\gamma');
					myText = strrep(myText, 'psi', '\psi');
					myText = strrep(myText, 'phi', '\phi');
					myText = strrep(myText, 'Psi', '\Psi');
					myText = strrep(myText, 'Phi', '\Phi');
					myText = strrep(myText, 'Delta', '\Delta');
					myText = strrep(myText, 'delta', '\delta');
					if ~contains(myText, '\zeta') && ~contains(myText, '\Zeta')
						myText = strrep(myText, 'eta', '\eta');
					end
					myText = strrep(myText, 'pi', '\pi');
					myText = strrep(myText, 'Pi', '\Pi');
				end
			end % greek2tex()
		end % plot()
		
		function s = nameValuePair(obj, varargin)
			assert(numel(obj) == 1, 'nameValuePair must NOT be called for an array object');
			s = struct(obj);
			if ~isempty(varargin)
				s = rmfield(s, varargin{:});
			end
			s = misc.struct2namevaluepair(s);
		end % nameValuePair()
		
		function s = struct(obj, varargin)
			if (nargin == 1) && isa(obj, 'quantity.Discrete')
				properties = fieldnames(obj);
				si = num2cell( size(obj) );
				s(si{:}) = struct();
				for l = 1:numel(obj)

					doNotCopyProperties = obj(l).doNotCopy;

					for k = 1:length(properties)
						if ~any(strcmp(doNotCopyProperties, properties{k}))
							s(l).(properties{k}) = obj(1).(properties{k});
						end
					end
				end
			else
				% Without this else-case 
				% this method is in conflict with the default matlab built-in
				% calls struct(). When calling struct('myFieldName', myQuantity) this
				% quantity-method is called (and errors) and not the
				% struct-calls-constructor.
				s = builtin('struct', obj, varargin{:});
			end
		end % struct()
		
		function s = obj2struct(obj)
			warning('depricated');
			s = struct(obj);
		end
		
		function b = flipGrid(a, myGridName)
			% flipGrid() implements a flip of the grids, for example with a(z)
			% b(z) = a(1-z) = a.flipGrid('z');
			% or for the multidimensional case with c(z, zeta)
			% d(z, zeta) = c(1-z, 1-zeta) = c.flipGrid({'z', 'zeta'});
			bMat = a.on();
			if ~iscell(myGridName)
				myGridName = {myGridName};
			end
			gridIdx = a.gridIndex(myGridName);
			for it = 1 : numel(myGridName)
				bMat = flip(bMat, gridIdx(it));
			end
			b = quantity.Discrete(bMat, ...
				'grid', a(1).grid, 'gridName', a(1).gridName, ...
				'name', ['flip(', a(1).name, ')']);
		end % flipGrid()
		
		function newObj = changeGrid(obj, gridNew, gridNameNew)
			% change the grid of the obj quantity. The order of grid and
			% gridName in the obj properties remains unchanged, only the
			% data points are exchanged.
			if isempty(obj)
				newObj = obj.copy();
				return;
			end
			gridIndexNew = obj(1).gridIndex(gridNameNew);
			myGrid = cell(1, numel(obj(1).grid));
			myGridName = cell(1, numel(obj(1).grid));
			for it = 1 : numel(myGrid)
				myGrid{gridIndexNew(it)} = gridNew{it};
				myGridName{gridIndexNew(it)} = gridNameNew{it};
			end
			assert(isequal(myGridName(:), obj(1).gridName(:)), 'rearranging grids failed');
			
			newObj = obj.copy();
			for it = 1 : numel(obj)
				newObj(it).valueDiscrete = obj(it).on(myGrid);
			end
			%[newObj.derivatives] = deal({});
			[newObj.grid] = deal(myGrid);
		end
		
		function [lowerBounds, upperBounds] = getBoundaryValues(obj, varargin)
			% GETBOUNDARYVALUES returns the boundary values of the grid
			% [lowerBounds, upperBounds] = getBoundaryValues(obj, gridName)
			% returns the boundary values of the grid, specified by
			% gridName
			%
			% [lowerBounds, upperBounds] = getBoundaryValues(obj) returns
			% the boudary values of all grids defined for the object.
			
			grids = obj(1).grid(obj.gridIndex(varargin{:}));
			lowerBounds = zeros(1, numel(grids));
			upperBounds = zeros(1, numel(grids));
			
			for k = 1:numel(grids)
				lowerBounds(k) = grids{k}(1);
				upperBounds(k) = grids{k}(end);
			end
			
		end
		
	end
	
	%% math
	methods (Access = public)
		
		function [F] = stateTransitionMatrix(obj, varargin)
			% STATETRANSITIONMATRIX compute the state-transition-matrix
			%	[F0] = stateTransitionMatrix(obj, varargin) computation of the
			% state-transition matrix for a ordinary differential equation
			%	d / dt x(t) = A(t) x(t)
			% with A as this object. The system matrix can be constant or
			% variable. It is the solution of 
			%	d / dt F(t,t0) = A(t) F(t,t0)
			%		   F(t,t) = I.
			% Optional parameters for the computation can be defined as
			% name-value-pairs. The options are:
			%	# 'grid' : the grid for which the state-transition matrix
			%	should be computed.
			%	# 'gridName1' : the name of the first independent variable.
			%	For F(t,t0) this is t. The default is the first gridName of
			%	this object.
			%	# 'gridName2' : the name of the second independent
			%	variable. For F(t,t0) this is t0. The default is the first
			%	gridName + '0'.
			
			assert(size(obj,1) == size(obj,2), 'The quantity must be a quadratic matrix for the computation of state-transition matrices');
			assert(obj(1).nargin <= 1, 'Computation of state-transition matrix is only defined for quantities with one independent variable');
			
			ip = misc.Parser();
			ip.addParameter('grid', []);
			ip.addParameter('gridName1', '');
			ip.addParameter('gridName2', '');	
			ip.parse(varargin{:});
			myGrid = ip.Results.grid;
			gridName1 = ip.Results.gridName1;
			gridName2 = ip.Results.gridName2;
			
			if ip.isDefault('grid')
				assert(numel(obj(1).grid) == 1, 'No grid is defined for the computation of the state-transition matrix. Use the name-value pair option "grid" to define the grid.');
				myGrid = obj(1).grid{1};
			end
			
			if ip.isDefault('gridName1')
				assert(numel(obj(1).gridName) == 1, 'No gridName is defined. Use name-value-pairs property "gridName1" to define a grid name');
				gridName1 = obj(1).gridName{1};
			end
						
			if ip.isDefault('gridName2')
				gridName2 = [gridName1 '0'];
			end
			
			
			assert(numel(myGrid) > 1, 'If the state transition matrix is computed for constant values, a spatial domain has to be defined!')
				
			if obj.isConstant
				% for a constant system matrix, the matrix exponential
				% function can be used.
				z = sym(gridName1, 'real');
				zeta = sym(gridName2, 'real');
				f0 = expm(obj.atIndex(1)*(z - zeta));
				F = quantity.Symbolic(f0, 'grid', {myGrid, myGrid});
									
			elseif isa(obj, 'quantity.Symbolic')
				f0 = misc.fundamentalMatrix.odeSolver_par(obj.function_handle, myGrid);
				F = quantity.Discrete(f0, ...
					'size', [size(obj, 1), size(obj, 2)], ...
					'gridName', {gridName1, gridName2}, ...
					'grid', {myGrid, myGrid});
			else
				f0 = misc.fundamentalMatrix.odeSolver_par( ...
					obj.on(myGrid), ...
					myGrid );
				F = quantity.Discrete(f0, ...
					'size', [size(obj, 1), size(obj, 2)], ...
					'gridName', {gridName1, gridName2}, ...
					'grid', {myGrid, myGrid});
			end
		end				
		
		function s = sum(obj, dim)
			s = quantity.Discrete(sum(obj.on(), obj.nargin + dim), ...
				'grid', obj(1).grid, 'gridName', obj(1).gridName, ...
				'name', ['sum(', obj(1).name, ')']);
		end
		
		function y = sqrt(x)
			% quadratic root for scalar and diagonal quantities
			y = quantity.Discrete(sqrt(x.on()), ...
				'size', size(x), 'grid', x(1).grid, 'gridName', x(1).gridName, ...
				'name', ['sqrt(', x(1).name, ')']);
		end
		
		
		function y = sqrtm(x)
			% quadratic root for matrices
			if isscalar(x)
				% use sqrt(), because its faster.
				y = sqrt(x);
			elseif (size(x, 1) == size(x, 2)) && ismatrix(x)
				% implementation of quadratic root pointwise in space in a
				% simple for-loop.
				xMat = x.on();
				permuteGridAndIdx = [[-1, 0] + ndims(xMat), 1:x.nargin];
				permuteBack = [(1:x.nargin)+2, [1, 2]];
				xPermuted = permute(xMat, permuteGridAndIdx);
				yUnmuted = 0*xPermuted;
				for k = 1 : prod(x(1).gridSize)
					yUnmuted(:,:,k) = sqrt(xPermuted(:,:,k));
				end
				y = quantity.Discrete(permute(yUnmuted, permuteBack), ...
					'size', size(x), 'grid', x(1).grid, 'gridName', x(1).gridName, ...
					'name', ['sqrt(', x(1).name, ')']);
			else
				error('sqrtm() is only implemented for quadratic matrices');
			end
		end
		
		function P = mpower(a, p)
			% a^p implemented by multiplication
			assert(p==floor(p) && p > 0);
			P = a;
			for k = 1:(p-1)
				P = P * a;
			end
		end
		function s = num2str(obj)
			s = obj.name;
		end
		function P = mtimes(a, b)
			% TODO rewrite the selection of the special cases! the
			% if-then-cosntruct is pretty ugly!
		
			% numeric computation of the matrix product
			if isempty(b) || isempty(a)
				% TODO: actually this only holds for multiplication of
				% matrices. If higher dimensional arrays are multiplied
				% this can lead to wrong results.
				sa = size(a);
				sb = size(b);
				
				P = quantity.Discrete.empty([sa(1:end-1) sb(2:end)]);
				return
			end
			if isa(b, 'double')
				if numel(b) == 1
					% simple multiplication in scalar case
					P = quantity.Discrete(a.on() * b, 'size', size(a),...
						'grid', a(1).grid, 'gridName', a(1).gridName, ...
						'name', [a(1).name, num2str(b)]);
					return
				else
					b = quantity.Discrete(b, 'size', size(b), 'grid', {}, ...
					'gridName', {}, 'name', '');
				end
			end
			
			if isa(a, 'double')
				P = (b.' * a.').';
				% this recursion is safe, because isa(b, 'double') is considered in
				% the if above.
				P.setName(['c ', b(1).name]);
				return
			end
			if a.isConstant() && ~b.isConstant()
				% If the first argument a is constant value, then bad
				% things will happen. To avoid this, we calculate
				%	a * b = (b' * a')'
				% instead. Thus we have to exchange both tranposed values
				% transpose the result in the end.
				P = (b' * a')';
				P.setName([a(1).name, ' ', b(1).name]);
				return
			end
			if isscalar(b)
				% do the scalar multiplication in a loop
				for k = 1:numel(a)
					P(k) = innerMTimes(a(k), b);				
				end
				P = reshape(P, size(a));
				return
			end
			
			if isscalar(a)
				% do the scalar multiplication in a loop
				for k = 1:numel(b)
					P(k) = innerMTimes(a, b(k));				
				end
				P = reshape(P, size(b));
				return
			end

			P = innerMTimes(a, b);			
		end
		
		function P = innerMTimes(a, b)
			assert(size(a, 2) == size(b, 1), ['For multiplication the ', ...
				'number of columns of the left array', ...
				'must be equal to number of rows of right array'])
			
			% misc.multArray is very efficient, but requires that the
			% multiple dimensions of the input are correctly arranged.
			[idx, permuteGrid] = computePermutationVectors(a, b);
						
			parameters = struct(a(1));
			parameters.name = [a(1).name, ' ', b(1).name];
			parameters.grid = [...
				a(1).grid(idx.A.common), ...
				b(1).grid(~idx.B.common)];
			parameters.gridName = [...
				a(1).gridName(idx.A.grid), ...
				b(1).gridName(~idx.B.common)];
			
			% select finest grid from a and b
			parameters.grid = cell(1, numel(parameters.gridName));
			[gridJoined, gridNameJoined] = gridJoin(a, b);
			% gridJoin combines the grid of a and b while using the finer
			% of both.
			aGrid = cell(1, a.nargin); % for later call of a.on() with fine grid
			bGrid = cell(1, b.nargin); % for later call of b.on() with fine grid
			for it = 1 : numel(parameters.gridName)
				parameters.grid{it} = gridJoined{...
					strcmp(parameters.gridName{it}, gridNameJoined)};
				if a.gridIndex(parameters.gridName{it})
					aGrid{a.gridIndex(parameters.gridName{it})} = ...
						parameters.grid{it};
				end
				if b.gridIndex(parameters.gridName{it})
					bGrid{b.gridIndex(parameters.gridName{it})} = ...
						parameters.grid{it};
				end		
			end
			parameters = misc.struct2namevaluepair(parameters);
			
			valueA = permute(a.on(aGrid), idx.A.permute);
			valueB = permute(b.on(bGrid), idx.B.permute);
			
			C = misc.multArray(valueA, valueB, idx.A.value(end), idx.B.value(1), idx.common);
			C = permute(C, permuteGrid);
			P = quantity.Discrete(C, parameters{:});
		end
		
		function y = inv(obj)
			% inv inverts the matrix obj at every point of the domain.
			assert(ismatrix(obj) && (size(obj, 1) == size(obj, 2)), ...
				'obj to be inverted must be quadratic');
			objDiscreteOriginal = obj.on();
			if isscalar(obj)
				% use ./ for scalar case
				y = quantity.Discrete(1 ./ objDiscreteOriginal, ...
					'size', size(obj), ...
					'name', ['(', obj(1).name, ')^{-1}'], ...
					'grid', obj(1).grid, 'gridName', obj(1).gridName);
			else
				% reshape and permute objDiscrete such that only on for
				% loop is needed.
				objDiscreteReshaped = permute(reshape(objDiscreteOriginal, ...
					[prod(obj(1).gridSize), size(obj)]), [2, 3, 1]);
				invDiscrete = zeros([prod(obj(1).gridSize), size(obj)]);

				parfor it = 1 : size(invDiscrete, 1)
					invDiscrete(it, :, :) = inv(objDiscreteReshaped(:, :, it));
				end

				y = quantity.Discrete(reshape(invDiscrete, size(objDiscreteOriginal)),...
					'size', size(obj), ...
					'name', ['{(', obj(1).name, ')}^{-1}'], ...
					'grid', obj(1).grid, 'gridName', obj(1).gridName);
			end
		end % inv()
		
		function objT = transpose(obj)
			objT = builtin('transpose', copy(obj));
			[objT.name] = deal(['{', obj(1).name, '}^{T}']);
		end % transpose(obj)
		
		function objCt = ctranspose(obj)
			objT = obj.';
			objCtMat = conj(objT.on());
			objCt = quantity.Discrete(objCtMat, 'grid', obj(1).grid, ...
			'gridName', obj(1).gridName, 'name', ['{', obj(1).name, '}^{H}']);
		end % ctranspose(obj)

		function y = exp(obj)
			% exp() is the exponential function using obj as the exponent.
			y = quantity.Discrete(exp(obj.on()), ...
				'name', ['exp(', obj(1).name, ')'], ...
				'grid', obj(1).grid, 'gridName', obj(1).gridName, ...
				'size', size(obj));
		end % exp()
		
		function xNorm = l2norm(obj, integralGridName, varargin)
			% calculates the l2 norm, for instance
			%	xNorm = sqrt(int_0^1 x.' * x dz).
			% Optionally, a weight can be defined, such that instead
			%	xNorm = sqrt(int_0^1 x.' * weight * x dz).
			% The integral domain is specified by integralGrid.
			
			if obj.nargin > 1
			assert(ischar(integralGridName), 'integralGrid must specify a gridName as a char');
			else
				integralGridName = obj(1).gridName;
			end
			
			myParser = misc.Parser();
			myParser.addParameter('weight', eye(size(obj, 1)));
			myParser.parse(varargin{:});
			if obj.nargin == 1 && all(strcmp(obj(1).gridName, integralGridName))
				xNorm = sqrtm(on(int(obj.' * myParser.Results.weight * obj), integralGridName));
			else
				xNorm = sqrtm(int(obj.' * myParser.Results.weight * obj, integralGridName));
				[xNorm.name] = deal(['||', obj(1).name, '||_{L2}']);
			end
		end % l2norm()
		
		function xNorm = quadraticNorm(obj, varargin)
			% calculates the quadratic norm, i.e,
			%	xNorm = sqrt(x.' * x).
			% Optionally, a weight can be defined, such that instead
			%	xNorm = sqrt(x.' * weight * x).
						
			myParser = misc.Parser();
			myParser.addParameter('weight', eye(size(obj, 1)));
			myParser.parse(varargin{:});
			xNorm = sqrtm(obj.' * myParser.Results.weight * obj);
			xNorm.setName(['||', obj(1).name, '||_{2}']);
		end % quadraticNorm()
		
		function y = expm(x)
			% exp() is the matrix-exponential function using obj as the
			% exponent.
			if isscalar(x)
				% use exp(), because its faster.
				y = exp(x);
			elseif ismatrix(x)
				% implementation of expm pointwise in space in a simple
				% for-loop.
				xMat = x.on();
				permuteGridAndIdx = [[-1, 0] + ndims(xMat), 1:x.nargin];
				permuteBack = [(1:x.nargin)+2, [1, 2]];
				xPermuted = permute(xMat, permuteGridAndIdx);
				yUnmuted = 0*xPermuted;
				for k = 1 : prod(x(1).gridSize)
					yUnmuted(:,:,k) = expm(xPermuted(:,:,k));
				end
				y = quantity.Discrete(permute(yUnmuted, permuteBack), ...
					'size', size(x), 'grid', x(1).grid, 'gridName', x(1).gridName, ...
					'name', ['expm(', x(1).name, ')']);
			end
		end
		
		function x = mldivide(A, B)
			% mldivide see doc mldivide of matlab: "x = A\B is the solution
			% to the equation Ax = B. Matrices A and B must have the same
			% number of rows."
			x = inv(A) * B;
		end
 		
		function x = mrdivide(B, A)
			% mRdivide see doc mrdivide of matlab: "x = B/A solves the
			% system of linear equations x*A = B for x. The matrices A and
			% B must contain the same number of columns"
			x = B * inv(A);
			
		end
		
		function P = matTimes(a, b)
			
			assert(size(a,2) == size(b,1));
			
			p = a(1).gridSize();
			q = p(2);
			p = p(1);
			A = a.on();
			B = b.on();
			
			% dimensions
			n = size(a, 1);
			m = size(b, 2);
			o = size(b, 1);
			
			P = zeros(p, q, n, m);
			
			for k = 1 : n % rows of P
				for l = 1 : m % columns of P
					for r = 1 : o % rows of B or columns of A
						P(:, :, k, l) = P( :, :, k, l ) + A( :, :, k, r) .* B( :, :, r, l );
					end
				end
			end
			
			argin = nameValuePair(a(1));
			P = quantity.Discrete(quantity.Discrete.value2cell(P, [p, q], [n, m]), argin{:});
		end
		function empty = isempty(obj)
			% ISEMPTY checks if the quantity object is empty
			%   empty = isempty(obj)
			
			% Check if there is any dimension which is zero
			empty = any(size(obj) == 0);
			
			% If the constructor is called without arguments, a
			% quantity.Discrete is created without an initialization of
			% the grid. Thus, the quantity is not initialized and empty if
			% the grid is not a cell.
			empty = empty || ~iscell(obj(1).grid);
			
		end % isempty()
		
		function P = ztzTimes(a, b)
			
			assert(size(a,2) == size(b,1))
			
			p = a(1).gridSize();
			q = b(1).gridSize();
			
			assert(p(1) == q(1));
			assert(numel(p) == 2);
			assert(numel(q) == 1);
			q = p(2);
			p = p(1);
			
			A = a.on();
			B = repmat(b.on(), 1, 1, 1, q);
			B = permute(B, [1, 4, 2, 3]);
			
			% dimensions
			n = size(a, 1);
			m = size(b, 2);
			o = size(b, 1);
			
			P = zeros(p, q, n, m);
			
			for k = 1 : n % rows of P
				for l = 1 : m % columns of P
					for r = 1 : o % rows of B or columns of A
						P(:, :, k, l) = P( :, :, k, l ) + A( :, :, k, r) .* B( :, :, r, l );
					end
				end
			end
			
			argin = nameValuePair(a(1));
			P = quantity.Discrete(quantity.Discrete.value2cell(P, [p, q], [n, m]), argin{:});
		end
		
		function myGrid = gridOf(obj, myGridName)
			if ~iscell(myGridName)
				gridIdx = obj(1).gridIndex(myGridName);
				if gridIdx > 0
					myGrid = obj(1).grid{gridIdx};
				else
					myGrid = [];
				end
			else
				myGrid = cell(size(myGridName));
				for it = 1 : numel(myGrid)
					gridIdx = obj(1).gridIndex(myGridName{it});
					if gridIdx > 0
						myGrid{it} = obj(1).grid{gridIdx};
					else
						myGrid{it} = [];
					end
				end
			end
		end
		
		function [idx, logicalIdx] = gridIndex(obj, names)
			%% GRIDINDEX returns the index of the grid
			% [idx, log] = gridIndex(obj, names) searches in the gridName
			% properties of obj for the "names" and returns its index as
			% "idx" and its logical index as "log"
			
			if nargin == 1
				names = obj(1).gridName;
			end
			
			if ~iscell(names)
				names = {names};
			end
			
			idx = zeros(1, length(names));
			nArgIdx = 1:obj.nargin();
			
			logicalIdx = false(1, obj.nargin());
			
			for k = 1:length(names)
				log = strcmp(obj(1).gridName, names{k});
				logicalIdx = logicalIdx | log;
				if any(log)
					idx(k) = nArgIdx(log);
				else
					idx(k) = 0;
				end
			end
			
		end
		
		function result = diff(obj, k, diffGridName)
			% DIFF computation of the derivative
			%	result = DIFF(obj, k, diffGridName) applies the
			% 'k'th-derivative for the variable specified with the input
			% 'diffGridName' to the obj. If no 'diffGridName' is specified,
			% then diff applies the derivative w.r.t. to all gridNames.
			if nargin == 1 || isempty(k)
				k = 1;  % by default, only one derivatve per diffGridName is applied
			else
				assert(isnumeric(k) && (round(k) == k))
			end
			
			if obj.isConstant && isempty(obj(1).gridName)
				result = quantity.Discrete(zeros(size(obj)), ...
					'size', size(obj), 'grid', obj(1).grid, ...
					'gridName', obj(1).gridName, ...
					'name', ['(d_{.}', obj(1).name, ')']);
				return
			end
			
			if nargin < 3 % if no diffGridName is specified, then the derivative 
				% w.r.t. to all gridNames is applied
				diffGridName = obj(1).gridName;
			end
				
			
			% diff for each element of diffGridName (this is rather
			% inefficient, but an easy implementation of the specification)
			if iscell(diffGridName) || isempty(diffGridName)
				if numel(diffGridName) == 0 || isempty(diffGridName)
					result = copy(obj);
				else
					result = obj.diff(k, diffGridName{1}); % init result
					for it = 2 : numel(diffGridName)
						result = result.diff(k, diffGridName{it});
					end
				end
			else
			gridSelector = strcmp(obj(1).gridName, diffGridName);
			gridSelectionIndex = find(gridSelector);
			
			spacing = gradient(obj(1).grid{gridSelectionIndex}, 1);
			assert(numeric.near(spacing, spacing(1)), ...
				'diff is currently only implemented for equidistant grid');
			permutationVector = 1 : (numel(obj(1).grid)+ndims(obj));
			
			objDiscrete = permute(obj.on(), ...
				[permutationVector(gridSelectionIndex), ...
				permutationVector(permutationVector ~= gridSelectionIndex)]);
			
			if size(objDiscrete, 2) == 1
				derivativeDiscrete = gradient(objDiscrete, spacing(1));
			else
				[~, derivativeDiscrete] = gradient(objDiscrete, spacing(1));
			end
			
			rePermutationVector = [2:(gridSelectionIndex), ...
				1, (gridSelectionIndex+1):ndims(derivativeDiscrete)];
			result = quantity.Discrete(...
				permute(derivativeDiscrete, rePermutationVector), ...
				'size', size(obj), 'grid', obj(1).grid, ...
				'gridName', obj(1).gridName, ...
					'name', ['(d_{', diffGridName, '}', obj(1).name, ')']);
				
				if k > 1
% 			% if a higher order derivative is requested, call the function
% 			% recursivly until the first-order derivative is reached
					result = result.diff(k-1, diffGridName);
				end
			end
		end
		
		function I = int(obj, varargin)
			% INT integrate
			% INT(obj) is the definite integral of obj with respect to
			%   all of its independent variables over the grid obj(1).grid.
			% INT(obj, gridName) is the definite integral of obj with
			%	respect to the spatial coordinate grid name, that has to
			%	be one of obj(1).gridName. The names must be a string or a
			%   array of strings.
			% INT(obj, a, b) (question: on which domain? what if numel(gridName)>1?)
			% INT(obj, gridName, a, b)
			if isempty(obj)
				I = obj.copy();
				return
			end
			
			intGridName = obj(1).gridName;
			[a, b] = obj.getBoundaryValues;
			
			if nargin == 1
				% see default settings.
			elseif nargin == 2 || nargin == 4
				% (obj, z) OR (obj, z, a, b)
				intGridName = varargin{1};
			elseif nargin == 3
				% (obj, a, b)
				a = varargin{1};
				b = varargin{2};
			end
			
			if iscell(intGridName) && numel(intGridName) > 1
				obj = obj.int(intGridName{1}, a(1), b(1));
				I = obj.int(intGridName{2:end}, a(2:end), b(2:end));
				return				
			end
			[idxGrid, isGrid] = obj.gridIndex(intGridName);
			if nargin == 4
				I = cumInt( obj, varargin{:});
				return;
% 				
% 				assert(all((varargin{2} == a(idxGrid)) & (varargin{3} == b(idxGrid))), ...
% 					'only integration from beginning to end of domain is implemented');
			end
			
			%% do the integration over one dimension
			I = obj.copy();
			% get index of the variables that should be considered for
			% integration
			
			[domainIdx{1:obj(1).nargin}] = deal(':');
			currentGrid = obj(1).grid{ idxGrid };
			domainIdx{idxGrid} = find(currentGrid >= a(idxGrid) & currentGrid <= b(idxGrid));

			myGrid = currentGrid(domainIdx{idxGrid});
			
			for kObj = 1:numel(obj)
				J = numeric.trapz_fast_nDim(myGrid, obj(kObj).atIndex(domainIdx{:}), idxGrid);

				% If the quantity is integrated, so that the first
				% dimension collapses, this dimension has to be shifted
				% away, so that the valueDiscrete can be set correctly.
				if size(J,1) == 1
					J = shiftdim(J);
				end
				
				if all(isGrid)
					grdName = '';
					newGrid = {};
				else
					grdName = obj(kObj).gridName{~isGrid};
					newGrid = obj(kObj).grid{~isGrid};
				end
				
				% create result:
				I(kObj).valueDiscrete = J;
				I(kObj).gridName = grdName;
				I(kObj).grid = newGrid;
				I(kObj).name = ['int(' obj(kObj).name ')'];
			end
				
		end
		
		function result = cumInt(obj, domain, lowerBound, upperBound)
			% CUMINT cumulative integration
			%	result = cumInt(obj, domain, lowerBound, upperBound)
			%	performes the integration over 'obj' for the 'domain' and
			%	the specified 'bounds'. 'domain' must be a gridName of the
			%	object. 'lowerBound' and 'upperBound' define the boundaries
			%	of the integration domain. These can be either doubles or
			%	gridNames. If it is double, the bound is constant. If it is
			%	a gridName, the bound is variable and the result is a
			%	function dependent of this variable.
			
			% input parser since some default values depend on intGridName.
			myParser = misc.Parser;
			myParser.addRequired('domain', @(d) obj.gridIndex(d) ~= 0);
			myParser.addRequired('lowerBound', ...
				@(l) isnumeric(l) || ischar(l) );
			myParser.addRequired('upperBound', ...
				@(l) isnumeric(l) || ischar(l) );
			myParser.parse(domain, lowerBound, upperBound)
			
			% get grid
			myGrid = obj(1).grid;
			intGridIdx = obj.gridIndex(domain);
			
			% integrate
			F = numeric.cumtrapz_fast_nDim(myGrid{intGridIdx}, ...
				obj.on(), intGridIdx);
			result = quantity.Discrete(F, 'grid', myGrid, ...
				'gridName', obj(1).gridName);
			
			% int_lowerBound^upperBound f(.) = 
			%	F(upperBound) - F(lowerBound)
			result = result.subs({domain}, upperBound) ...
				- result.subs({domain}, lowerBound);
			if isa(result, 'quantity.Discrete')
				result.setName(deal(['int(', obj(1).name, ')']));
			end
		end
				
		function C = plus(A, B)
			%% PLUS is the sum of two quantities.
			assert(isequal(size(A), size(B)), 'plus() not supports mismatching sizes')
			
			if isempty(A) || isempty(B)
				C = quantity.Discrete.empty(size(A));
				return
			end
			
			% for support of numeric inputs:
			if ~isa(A, 'quantity.Discrete')
				if isnumeric(A)
					A = quantity.Discrete(A, 'name', 'c', 'gridName', {}, 'grid', {});
				else
					error('Not yet implemented')
				end
			elseif ~isa(B, 'quantity.Discrete')
				if isnumeric(B)
					B = quantity.Discrete(B, 'name', 'c', 'gridName', {});
				else
					B = quantity.Discrete(B, 'name', 'c', 'gridName', A(1).gridName, 'grid', A.grid);
				end
			end
			
			% combine both domains with finest grid
			[gridJoined, gridNameJoined] = gridJoin(A, B);
			gridJoinedLength = cellfun(@(v) numel(v), gridJoined);
			
			[aDiscrete] = A.expandValueDiscrete(gridJoined, gridNameJoined, gridJoinedLength);
			[bDiscrete] = B.expandValueDiscrete(gridJoined, gridNameJoined, gridJoinedLength);

			% create result object
			C = quantity.Discrete(aDiscrete + bDiscrete, ...
				'grid', gridJoined, 'gridName', gridNameJoined, ...
				'size', size(A), 'name', [A(1).name, '+', B(1).name]);
		end
		
		function [valDiscrete] = expandValueDiscrete(obj, gridJoined, gridNameJoined, gridJoinedLength)
			% EXPANDVALUEDISCRETE
			%	[valDiscrete, gridNameSorted] = ...
			%       expandValueDiscrete(obj, gridIndex, valDiscrete)
			%	Expanses the value of obj, so that 
			%	todo
			
			% get the index of obj.grid in the joined grid
			gridLogical = logical( obj.gridIndex(gridNameJoined) );
			
			valDiscrete = obj.on(gridJoined(gridLogical > 0), gridNameJoined(gridLogical > 0));
			oldDim = ndims(valDiscrete);
			valDiscrete = permute(valDiscrete, [(1:sum(~gridLogical)) + oldDim, 1:oldDim] );
			valDiscrete = repmat(valDiscrete, [gridJoinedLength(~gridLogical), ones(1, ndims(valDiscrete))]);
% 			
   			valDiscrete = reshape(valDiscrete, ...
  				[gridJoinedLength(~gridLogical), gridJoinedLength(gridLogical), size(obj)]);
 			
% 			% permute valDiscrete such that grids are in the order specified
% 			% by gridNameJoined.
 			gridIndex = 1:numel(gridLogical);		
			gridOrder = [gridIndex(~gridLogical), gridIndex(gridLogical)];
  			valDiscrete = permute(valDiscrete, [gridOrder, numel(gridLogical)+(1:ndims(obj))]);
			
		end
		
		
		function C = minus(A, B)
			% minus uses plus()
			C = A + (-B);
			if isnumeric(A)
				[C.name] = deal(['c-', B(1).name]);
				
			elseif isnumeric(B)
				[C.name] = deal([A(1).name, '-c']);
			else
			[C.name] = deal([A(1).name, '-', B(1).name]);
			end
		end
		
		
		function C = uplus(A)
			% unitary plus: C = +A
			C = copy(A);
		end
		
		function C = uminus(A)
			% unitary plus: C = -A
			C = (-1) * A;
			[C.name] = deal(['-', A(1).name]);
		end
		
		function [P, supremum] = relativeErrorSupremum(A, B)
			
			assert(numel(A) == numel(B), 'Not implemented')
			
			P = A.copy();
			
			if ~isa(B, 'quantity.Discrete')
				B = quantity.Discrete(B);
			end
			supremum = nan(size(A));
			for k = 1:numel(A)
				supremum(k) = max(max(abs(A(k).valueDiscrete), abs(B(k).valueDiscrete)));
				P(k).valueDiscrete = (A(k).valueDiscrete - B(k).valueDiscrete) ./ supremum(k);
				P(k).name = sprintf('%.2g', supremum(k));
			end
			supremum = reshape(supremum, size(A));
		end
		
		function maxValue = max(obj)
			% max returns the maximal value of all elements of A over all
			% variables as a double array.
			maxValue = reshape(max(obj.on(), [], 1:obj(1).nargin), [size(obj)]);			
		end
		
		function [i, m, s] = near(obj, B, varargin)
			%% NEAR comparer with numerical tolerance
			% [i, m, s] = near(obj, B) compares the numerical
			%	values of this object with the numerical values of B. B has
			%	to be a quantity.Discrete object.
			% [i, m, s] = near(obj, B, tolerance) compares the numerical
			% values with respect to the given tolerance. Default is 10eps.
			% [i, m, s] = near(obj, B, tolerance, relative) compares the
			% numerical values with respect to the given tolerance and if
			% relative is true, the values are compared, relativley to the
			% maximal value of obj.on(). If relative is a numerical value.
			% The values are compared reltivley to this.
			%
			% The output i is a logical value that is true if the values
			% are inside the given tolerance. m is the maximal derivation
			% of both numerical values. s is the text that is printed if
			% the function is called without output.
			
			if nargin == 1
				b = 0;
			elseif isnumeric(B)
				b = B;
			else
				b = B.on(obj(1).grid, obj(1).gridName);
			end
			
			[i, m, s] = numeric.near(...
				obj.on(obj(1).grid, obj(1).gridName), ...
				b, varargin{:});
			if nargout == 0
				i = s;
			end
		end
		function maxValue = MAX(obj)
			maxValue = obj.max();
			maxValue = max(maxValue(:));
		end
		
		function minValue = min(obj)
			% max returns the minimum value of all elements of A over all
			% variables as a double array.
			minValue = reshape(min(obj.on(), [], 1:obj(1).nargin), [size(obj)]);			
		end % min()
		
		function absQuantity = abs(obj)
			% abs returns the absolut value of the quantity as a quantity
			if isempty(obj)
				absQuantity = quantity.Discrete.empty(size(obj));
			else
			absQuantity = quantity.Discrete(abs(obj.on()), ...
				'grid', obj(1).grid, 'gridName', obj(1).gridName, ...
				'size', size(obj), 'name', ['|', obj(1).name, '|']);	
			end
		end % abs()
		
		function y = real(obj)
			% real() returns the real part of the obj.
			y = quantity.Discrete(real(obj.on()), ...
				'name', ['real(', obj(1).name, ')'], ...
				'grid', obj(1).grid, 'gridName', obj(1).gridName, ...
				'size', size(obj));
		end % real()
		
		function y = imag(obj)
			% real() returns the imaginary part of the obj.
			y = quantity.Discrete(imag(obj.on()), ...
				'name', ['imag(', obj(1).name, ')'], ...
				'grid', obj(1).grid, 'gridName', obj(1).gridName, ...
				'size', size(obj));
		end % imag()
		
		function meanValue = mean(obj)
			% mean returns the mean value of all elements of A over all
			% variables as a double array.
			meanValue = reshape(mean(obj.on(), 1:obj(1).nargin), size(obj));			
		end
		
		function medianValue = median(obj)
			% meadin returns the meadin value of all elements of A over all
			% variables as a double array.
			% ATTENTION:
			% median over multiple dimensions is only possible from MATLAB 2019a and above!
			% TODO Update Matlab!
			medianValue = reshape(median(obj.on(), 1:obj(1).nargin), size(obj));			
		end
						
		function q = obj2value(obj)
			% for a case with a 3-dimensional grid, the common cell2mat
			% with reshape did not work. So it has to be done manually:
			
			v = zeros([numel(obj(1).valueDiscrete), numel(obj)]);
			for k = 1:numel(obj)
				v(:,k) = obj(k).valueDiscrete(:);
			end
			q = reshape(v, [obj(1).gridSize(), size(obj)]);
			
		end
		
		function result = diag2vec(obj)
			% This method creates a vector of quantities by selecting the
			% diagonal elements of the quantity array obj.
			assert(ndims(obj) <= 2, 'quantity.diag2vec is only implemented for quantity matrices');
			
			for it = 1:min(size(obj, 1), size(obj, 2))
				result(it, 1) = copy(obj(it, it));
			end
		end
		
		function result = vec2diag(vec)
			% This method creates a diagonal matrix of quantities which
			% carries the elements of vec on its diagonal
			assert(isvector(vec), 'quantity.vec2diag is only implemented for quantity vectors');
			try
				result = zeros(numel(vec)) * (vec(:) * vec(:).');
			catch
				result = 0 * (vec(:) * vec(:).');
			end
			
			for it = 1 : numel(vec)
				result(it, it) = copy(vec(it));
			end
		end
		
		
	end% (Access = public)
	
	%%
	methods (Static)
		
		function P = zeros(valueSize, grid, varargin)
			%ZEROS initializes an zero quantity.Discrete object
			%   P = zeros(gridSize, valueSize, grid) returns a
			%   quantity.Discrete object that has only zero entries on a
			%   grid with defined by the cell GRID and the value size
			%   defined by the size-vector VALUESIZE
			%       P = quantity.Discrete([n,m], {linspace(0,1)',
			%                                       linspace(0,10)});
			%       creates an (n times m) zero quantity.Discrete on the
			%       grid (0,1) x (0,10)
			%
			if ~iscell(grid)
				grid = {grid};
			end
			gridSize = cellfun('length', grid);
			O = zeros([gridSize(:); valueSize(:)]');
			P = quantity.Discrete(O, 'size', valueSize, 'grid', grid, varargin{:});
		end
		
		function q = value2cell(value, gridSize, valueSize)
			% VALUE2CELL
			gs = num2cell(gridSize(:));
			vs =  arrayfun(@(n) ones(n, 1), valueSize, 'UniformOutput', false);
			s = [gs(:); vs(:)]; %{gs{:}, vs{:}};%
			q = reshape(mat2cell(value, s{:}), valueSize);
		end
		
		function g = defaultGrid(gridSize)
			
			g = cell(1, length(gridSize));
			for k = 1:length(gridSize)
				
				o = ones(1, length(gridSize) + 1); % + 1 is required to deal with one dimensional grids
				o(k) = gridSize(k);
				O = ones(o);
				O(:) = linspace(0, 1, gridSize(k));
				
				g{k} = O;
			end
		end
		
		
		function [p, q, parameters, gridSize] = inputNormalizer(a, b)
			
			parameters = [];
			
			function s = innerInputNormalizer(A, B)
				if isa(A, 'double')
					s.name = num2str(A(:)');
					s.gridName = {};
					s.grid = {};
					s.value = A;
					s.size = size(A);
					s.nargin = 0;
				elseif isa(A, 'quantity.Discrete')
					s.name = A(1).name;
					s.value = A.on();
					s.gridName = A(1).gridName;
					s.grid = A(1).grid;
					s.size = size(A);
					s.nargin = A.nargin();
					parameters = struct(A);
					gridSize = A.gridSize();
				end
			end
			
			q = innerInputNormalizer(b, a);
			p = innerInputNormalizer(a, b);
			
		end
	end %% (Static)
	methods(Access = protected)
		
		function [idx, permuteGrid] = computePermutationVectors(a, b)
			% Computes the required permutation vectors to use
			% misc.multArray for multiplication of matrices
						
			uA = unique(a(1).gridName, 'stable');
			assert(numel(uA) == numel(a(1).gridName), 'Gridnames have to be unique!');
			uB = unique(b(1).gridName, 'stable');
			assert(numel(uB) == numel(b(1).gridName), 'Gridnames have to be unique!');
			
			% 1) find common entries
			common = intersect(a(1).gridName, b(1).gridName);
			
			commonA = false(1, a.nargin());
			commonB = false(1, b.nargin());
			
			idxA0 = 1:a.nargin();
			idxB0 = 1:b.nargin();
			
			
			gridA = zeros(1, a.nargin());
			gridB = zeros(1, b.nargin());
			
			for k = 1:numel(common)
				c = common{k};
				
				% 2) find logical indices for the common entries
				cA = strcmp(c, a(1).gridName);
				commonA = commonA | cA;
				
				% 3) exchange the order of the logical indices
				gridA(k) = idxA0(cA);
				
				cB = strcmp(c, b(1).gridName);
				commonB = commonB | cB;
				
				gridB(k) = idxB0(cB);
			end
			
			gridA(numel(common)+1:end) = idxA0(~commonA);
			gridB(numel(common)+1:end) = idxB0(~commonB);
			
			valueA = a.nargin + (1:numel(size(a)));
			valueB = b.nargin + (1:numel(size(b)));
			
			idx.A.permute = [gridA, valueA];
			idx.A.grid = gridA;
			idx.A.value = valueA;
			idx.A.common = commonA;
			
			idx.B.permute = [gridB, valueB];
			idx.B.grid = gridB;
			idx.B.value = valueB;
			idx.B.common = commonB;
			
			idx.common = 1:numel(common);
			
			% permutation for the new grid
			nGridA = a.nargin();				% number of the grid dimensions of quantity a
			nValueA = ndims(a) - 1;				% number of the value dimensions of a minus the multiplied dimension
			nGridB = b.nargin() - numel(common);% number of the grid dimensions of quantity b minus the common dimensions that are part of nGridA
			nValueB = ndims(b) - 1;				% number of the value dimensions minus the multiplied dimensions
			
			idxGrid = 1:(nGridA + nGridB + nValueA + nValueB);
			
			lGridA = [true(1, nGridA), false(1, nValueA), false(1, nGridB), false(1, nValueB)];
			lGridVA = [false(1, nGridA), true(1, nValueA), false(1, nGridB), false(1, nValueB)];
			lGridB = [false(1, nGridA), false(1, nValueA), true(1, nGridB), false(1, nValueB)];
			lGridVB = [false(1, nGridA), false(1, nValueA), false(1, nGridB), true(1, nValueB)];
			
			% Creates the permutation vector to bring the result of
			% misc.multArray into the required form for the
			% quantity.Discrete class.
			permuteGrid = [idxGrid(lGridA), idxGrid(lGridB), idxGrid(lGridVA), idxGrid(lGridVB)];
		end

		
		function f = setValueContinuous(obj, f)
		end
		function f = getValueContinuous(obj, f)
		end
		
		% Override copyElement method:
		function cpObj = copyElement(obj)
			% Make a shallow copy of all properties
			cpObj = copyElement@matlab.mixin.Copyable(obj);
			% #TODO insert code here if some properties should not be
			% copied.
		end
		
		% 		function s = getHeader(obj)
		% 			s = 'TEST'
		% 		end
		
		function s = getPropertyGroups(obj)
			% Function to display the correct values
			
			if isempty(obj)
				s = getPropertyGroups@matlab.mixin.CustomDisplay(obj);
				return;
			else
				s = getPropertyGroups@matlab.mixin.CustomDisplay(obj(1));	
			end
			
			if numel(obj) ~= 1
				s.PropertyList.valueDiscrete = ...
					[sprintf('%ix', gridSize(obj), size(obj)) sprintf('\b')];
			end
		end
	end % methods (Access = protected)
	
	methods ( Static, Access = protected )
		function doNotCopy = doNotCopyPropertiesName()
			% #DEPRECATED
			doNotCopy = {'valueContinuous', 'valueDiscrete', 'derivatives'};
		end
	end % methods ( Static, Access = protected )
	
end % classdef
