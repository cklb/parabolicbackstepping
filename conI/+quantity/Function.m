classdef Function < quantity.Discrete
	
	properties
		valueContinuous (1, 1);	% function_handle or double
	end
	
	methods
		%--------------------
		% --- Constructor ---
		%--------------------
		function obj = Function(valueOriginal, varargin)
			parentVarargin = {};	% by default the parent-class is called 
									% with no input.
			
			% only do something if there are some paremters. otherwise an
			% empty object will be returned. This is important to allow the
			% initialization as object array.
			if nargin > 0
				sizeValue = size(valueOriginal);
				fun = cell(sizeValue);
				
				% modification of some input parameters to allow the
				% initialization of the object array
				for k = 1:numel(valueOriginal)
					if iscell(valueOriginal)
						fun{k} = valueOriginal{k};
					elseif isnumeric(valueOriginal)
						fun{k} = valueOriginal(k);
					elseif isa(valueOriginal, 'function_handle')
						fun{k} = valueOriginal;
					elseif isa(valueOriginal, 'sym')
						fun{k} = matlabFunction(valueOriginal);
					else
						error('Type of valueContinuous not supported')
					end
				end
				
				myParser = misc.Parser();
				myParser.addParameter('grid', {linspace(0, 1, 101).'});
				if isempty(fun)
					gridNameDefault = {};
				else
					gridNameDefault = misc.getInputNamesOfFunctionHandle(fun{1});
				end
				myParser.addParameter('gridName', gridNameDefault);
				myParser.parse(varargin{:});
				unmatched = misc.struct2namevaluepair(myParser.Unmatched);
				
				parentVarargin = {cell(sizeValue), 'grid', myParser.Results.grid, ...
					'gridName', myParser.Results.gridName, unmatched{:}};
			end
			
			obj@quantity.Discrete(parentVarargin{:});
			if nargin > 0
				for it = 1:numel(obj)
					obj(it).valueContinuous = fun{it};
				end
			end
		end%Function constructor

		function f = function_handle(obj)
			
			if numel(obj) == 1
				f = @obj.valueContinuous;
			else
				for k = 1:numel(obj)
					F{k} = @(varargin) obj(k).valueContinuous(varargin{:});
				end

				f = @(varargin) reshape((cellfun( @(c) c(varargin{:}), F)), size(obj));
			end
		end
		
		%-----------------------------
		% --- overloaded functions ---
		%-----------------------------
		function value = on(obj, myGrid, myGridName)
			% evaluates obj.valueContinuous function and returns an array
			% with the dimensions (n, m, ..., z_disc). n, m, ... are the
			% array dimensions of obj.valueContinuous and z_disc is the
			% dimensions of the grid.
			if nargin == 1
				gridSize = obj(1).gridSize;
				if gridSize > 2
					value = cat(length(gridSize),obj.valueDiscrete);
				else % important to include this case for empty grids!
					value = [obj.valueDiscrete]; % = cat(2,obj.valueDiscrete)
				end
				value = reshape(value, [gridSize, size(obj)]);
				return
			elseif nargin >= 2 && ~iscell(myGrid)
				myGrid = {myGrid};
			end
			gridPermuteIdx = 1:obj(1).nargin;
			if isempty(gridPermuteIdx)
				gridPermuteIdx = 1;
			end
			if nargin == 3
				if ~iscell(myGridName)
					myGridName = {myGridName};
				end
				assert(numel(myGrid) == numel(myGridName), ...
					['If on() is called by using gridNames as third input', ...
					', then the cell-array of grid and gridName must have ', ...
					'equal number of elements.']);
				assert(numel(myGridName) == obj(1).nargin, ...
					'All (or none) gridName must be specified');
				gridPermuteIdx = cellfun(@(v) obj(1).gridIndex(v), myGridName);
				myGrid = myGrid(gridPermuteIdx);
			end
			% at first the value has to be initialized by different
			% dimensions, to simplify the evaluation of each entry:

			gridSize = cellfun('length', myGrid);
			if isempty(gridSize), gridSize = 1; end

			value = nan([numel(obj), gridSize]);
			for k = 1:numel(obj)
				ndGrd = obj.ndgrid( myGrid );
				tmp = obj(k).evaluateFunction( ndGrd{:} );
				% Replaced
				%   double(subs(obj(k).valueSymbolic, obj.variable, grid));
				% because this is very slow!
				value(k,:) = tmp(:);
			end
			value = reshape(permute(value, [1+(gridPermuteIdx), 1]), ...
				[gridSize(gridPermuteIdx), size(obj)]);
		end
		
		function n = nargin(obj)
			% FIXME: check if all funtions in this object have the same
			% number of input values.
			n = numel(obj(1).grid);
		end
	end
	
	
	
	% -------------
	% --- Mathe ---
	%--------------
	methods
		function mObj = uminus(obj)
			mObj = obj.copy();
			
			for k = 1:numel(obj)
				mObj(k).valueContinuous = @(varargin) - obj(k).valueContinuous(varargin{:});
				mObj(k).valueDiscrete = - obj(k).valueDiscrete;
			end
			
			[mObj.name] = deal(['-' obj.name]);
		end
		
		function p = inner(A, B)
			
			AB = A * B;
			
			n = size(A, 1);
			m = size(B, 2);
			p = zeros(n, m);
			
			z0 = A(1).grid{1}(1);
			z1 = A(1).grid{1}(end);
			
			for k = 1 : n*m
				p(k) = AB(k).int(z0, z1);
			end
			
		end
		
		function I = int(S, a, b)
			I = zeros(size(S));
			for k = 1:numel(S)
				I(k) = integral(@(varargin)S(k).evaluateFunction(varargin{:}), a, b, 'AbsTol', 1e-9);
			end
		end

	end
	
	methods (Access = protected)
		function v = evaluateFunction(obj, varargin)
			v = obj.valueContinuous(varargin{:});
		end
	end
	
	methods (Static)%, Access = protected)
		function [o] = zero(varargin)
			% ZERO creates an array containing only zeros with the same
			% dimensions as input ndgrids. This is needed for, function
			% handles that return a constant value and should be able to be
			% called on a ndgrid.
			if nargin >= 1
				o = zeros(size(varargin{1}));
			elseif nargin == 0
				o = 0;
			end
		end
	end
	
end

