classdef Operator < handle & matlab.mixin.Copyable
	%OPERATOR define operator matrices
	% This class describes operators of the form
	%		A[x(t)](z) = A0(z) x(z,t) + A1(z) dz x(z,t) + A2(z) dz^2 x(z,t)
	%			... + Ak dz^k x(z,t)
	
	properties
		% The variable that is used for the algebraic representation of the
		% operator
		s;
		% The values of the operator matrices in form of a
		% quantity.Discrete object
		coefficient quantity.Discrete;
	end
	properties (Dependent)
		% The grid, on which the operator is defined
		grid;
		gridName;
	end
	methods
		function obj = Operator(A, varargin)
			
			if nargin > 0
				
				prsr = misc.Parser();
				prsr.addOptional('s', sym('s'));
				prsr.addOptional('name', '');
				prsr.parse(varargin{:});
				
				s = prsr.Results.s;
				
				%TODO assert that A has entries with the same size
				if iscell(A)
					for k = 1 : numel(A)
						if isnumeric(A{k})
							obj(k).coefficient = ...
								quantity.Discrete(A{k}, 'gridName', {}, 'grid', {});
						else
							obj(k).coefficient = A{k}; %#ok<AGROW>
						end
					end
				elseif isnumeric(A)
					obj(1).coefficient = quantity.Discrete(A, 'gridName', {}, 'grid', {});
				end
					
				[obj.s] = deal(s);
				for k = 1:numel(A)
					[obj(k).coefficient.name] = deal(prsr.Results.name);
				end
				
				prsr.unmatchedWarning();
			end
		end
		
		function g = get.grid(obj)
			g = obj.coefficient.grid;
		end
		function n = get.gridName(obj)
			n = obj.coefficient.gridName;
		end
		function s = gridSize(obj, varargin)
			s = obj(1).coefficient.gridSize(varargin{:});
		end
		
		function I = int(obj, varargin)
			II = cell(size(obj,1), 1);
			for k = 1:size(obj, 1)
				II{k} = int(obj(k).coefficient, varargin{:});
			end
			I = quantity.Operator(II);
		end
		
		function C = mtimes(A, B)
			
% 			if isnumeric(A) && numel(A) == 1
% 				A = quantity.Operator(eye(size(B(1).coefficient, 1)) * A);
% 			end
% 			
			if ~isa(A, 'quantity.Operator')
				A = quantity.Operator(A);
			end
			if ~isa(B, 'quantity.Operator')
				B = quantity.Operator(B);
			end
			
			[n, m] = size(A(1).coefficient * B(1).coefficient);
			myGrid = A(1).grid;
			myGridName = A(1).gridName;
			
% 			assert(size(A(1).coefficient, 2) == size(B(1).coefficient, 1))
			
			for k = 1:max(length(A), length(B))
				C{k} = sumCk(k);
			end
			
			function Ck = sumCk(k)
				Ck = quantity.Discrete.zeros([n, m], myGrid, 'gridName', myGridName);
				for l = 1:k
					if l <= length(A) && k-l+1 <= length(B)
						% do the compuation only if the coefficients do
						% exist
						Ck = Ck + A(l).coefficient * B(k-l+1).coefficient;
					end
				end
			end
			C = quantity.Operator(C);
		end
		function C = plus(A, B)
			
			if ~isa(A, 'quantity.Operator')
				A = quantity.Operator(A);
			end
			if ~isa(B, 'quantity.Operator')
				B = quantity.Operator(B);
			end			
			
			for k = 1 : max(length(A), length(B))
							
				if k <= length(A)
					C{k} =  A(k).coefficient;
				end
				
				if k <= length(B)
					if length(C) == k
						C{k} = C{k} + B(k).coefficient;
					else
						C{k} = B(k).coefficient;
					end
				end
			end
			C = quantity.Operator(C);			
		end
		function C = minus(A, B)
			C = A + (-1)*B;
		end
		function C = adj(A)
			assert(A(1).coefficient.isConstant())
			
			C = polyMatrix.polynomial2coefficients(misc.adj(A.powerSeries), A(1).s);
			c = cell(1, size(C, 3));
			for k = 1:size(C, 3)
				c{k} = quantity.Discrete(double(C(:,:,k)), ...
					'gridName', {}, 'grid', {}, 'name', ['adj(' A(1).coefficient.name ')']);
			end
			
			C = quantity.Operator(c);
		end
		function C = det(A)
			assert(A(1).coefficient.isConstant())
			
			C = det(A.powerSeries);
			C = polyMatrix.polynomial2coefficients(C, A(1).s);
			c = cell(1, size(C, 3));
			for k = 1:size(C, 3)
				c{k} = quantity.Discrete(double(C(:,:,k)), ...
					'gridName', {}, 'grid', {}, 'name', ['det(' A(1).coefficient.name ')']);
			end
			
			C = quantity.Operator(c);
		end
		function C = uminus(A)
			C = (-1)*A;
		end
		function s = size(obj, varargin)
			s = [length(obj), size(obj(1).coefficient)];
			s = s(varargin{:});
		end
		function l = length(obj)
			l = numel(obj);
		end
	end
	
	methods (Access = public)
		function d = double(obj)
			d = double(obj.M);
		end
		function [i, m, s] = near(obj, B, varargin)
			
			if isa(B, "quantity.Operator")
				b = B.M;
			else
				b = B;
			end
			
			[i, m, s] = obj.M.near(b, varargin{:});
			if nargout == 0
				i = s;
			end
		end
		function i = isempty(obj)
			i = numel(obj)==0 || isempty(obj(1).coefficient);
		end
		
		function h = plot(obj, varargin)
			h = obj.M.plot(varargin{:});
		end
		
		function S = powerSeries(obj)
			S =polyMatrix.powerSeries2Sum(squeeze(obj.M.on()), obj(1).s);
		end
		
		function A = ctranspose(obj)
			for k = 1:length(obj)
				M{k} = (-1)^(k-1) * obj(k).coefficient'; %#ok<AGROW>
			end
			A = quantity.Operator(M);
		end
		
		function [Phi, Psi] = stateTransitionMatrixByOdeSystem(A, varargin)
			% STATETRANSTITIONBYODE
			% [Phi, Psi] = stateTransitionMatrixByOdeSystem(A, varargin) computes the
			% state-transition matrix of the boundary-value-problem
			%	dz x(z,s) = A(z,s) x(xi,s) + B(z,s) * u(s)
			% so that Phi is the solution of
			%	dz Phi(z,xi,s) = A(z,s) Phi(z,xi,s)
			% and Psi is given by
			%	Psi(z,xi,s) = int_xi_z Phi(z,zeta,s) B(zeta,s) d zeta
			% if B(z,s) is defined as quantity.Operator object by
			% name-value-pair.
			% To solve this problem the recursion formula from [1]
			%	dz w_k(z) = sum_j=0 ^d A_j(z) * w_{k-j}(z) + B_k nu
			% is used. This recursion of odes can be reformulated to a
			% large ode:
			%
			%	dz w_0 = A_0 w_0 + B_0 nu
			%	dz w_1 = A_1 w_0 + A_0 w_0 + B_1 nu
			%	dz w_2 = A_2 w_0 + A_1 w_1 + A_0 w_2 + B_2 nu
			%	...
			%
			%	[dz w_0]   [ A_0  0   0   0  ... ][w_0]   [B_0]
			%	[dz w_1]   [ A_1 A_0  0   0  ... ][w_1]   [B_1]
			%	[dz w_2] = [ A_2 A_1 A_0  0  ... ][w_2] + [B_2] nu
			%	[ ...  ]   [  ...                [[...]   [...]
			%
			% By some sorting of the terms, as shown in [2], the solution
			% of the recursion can be described by
			%	w_k(z) = Phi_k(z, xi) gamma + Psi_k(z,xi) nu
			% with w_0(xi) = gamma and w_k(xi) = 0, k > 0. From the
			% solution for the ode of the recursion:
			%	W(z) = PHI(z, xi) W(0) + PSI(z,xi) nu
			% and W(z) = [w_0'(z), w_1'(z), w_2'(z), ... ]' it is obvious,
			% that the required Phi_k(z,xi) and Psi_k(z,xi) can be
			% determined by appropriate partitioning of PHI(z,xi),
			% PSI(z,xi):
			%
			%	[w_0]   [ Phi_0  *   *  ... ][gamma]   [Psi_0]
			%	[w_1]   [ Phi_1  *   *  ... ][  0  ]   [Psi_1]
			%	[w_2] = [ Phi_2  *   *  ... ][  0  ] + [Psi_2] nu
			%	[...]   [  ...              ][ ... ]   [ ... ]
			%
			
			n = size(A(1).coefficient, 2);
			
			ip = misc.Parser();
			ip.addOptional('N', 1);
			ip.addOptional('B', quantity.Operator.empty(1, 0));
			ip.parse(varargin{:});
			B = ip.Results.B;
			N = ip.Results.N;
			
			if isempty(B)
				m = 0;
			else
				m = size(B(1).coefficient, 2);
			end
			
			n = size(A(1).coefficient, 1);
			d = length(A);
			spatialDomain = A(1).coefficient(1).grid{:};
			
			% build the large system of odes from the recursion
			A_full = zeros(length(spatialDomain), N*n, N*n);
			B_full = zeros(length(spatialDomain), N*n, m);
			
			idx = 1:n;
			for k = 1:N
				idxk = idx + (k-1)*n;
				if length(B) >= k
					B_full(:, idxk, :) = B(k).coefficient.on();
				end
				for j = 1:N
					idxj = idx + (j-1)*n;
					if k - (j-1) > 0 && k - (j-1) <= length(A)
						A_full(:, idxk, idxj) = A(k - (j-1)).coefficient.on();
					end
				end
			end
			f = quantity.Discrete(...
				misc.fundamentalMatrix.odeSolver_par( A_full, spatialDomain ), ...
				'grid', {spatialDomain, spatialDomain}, 'gridName', {'z', 'zeta'});
			
			b = quantity.Discrete(B_full, 'grid', spatialDomain, ...
				'gridName', 'zeta');
			p = cumInt(f * b, 'zeta', spatialDomain(1), 'z');
						
			z0 = A(1).grid{1}(1);
			for k = 1:N
				idxk = idx + (k-1)*n;
				Phi(:,:,k) = f(idxk, idx).subs('zeta', z0);
				Psi(:,:,k) = p(idxk,:);
			end
			
			Phi = quantity.Operator(Phi);
			Psi = quantity.Operator(Psi);
		end
		
		function [Phi, Psi, F0] = stateTransitionMatrix(A, varargin)
			% STATETRANSITIONMATRIX computation of the
			% state-transition matrix
			%	[Phi, Psi] = stateTransitioMatrix(obj, varargin) computes
			% the state-transition matrix of the boundary value problem
			%	dz x(z,s) = A(z,s) x(xi,s) + B(z,s) * u(s)
			% so that Phi is the solution of
			%	dz Phi(z,xi,s) = A(z,s) Phi(z,xi,s)
			% and Psi is given by
			%	Psi(z,xi,s) = int_xi_z Phi(z,zeta,s) B(zeta,s) d zeta
			% if B(z,s) is defined as quantity.Operator object by
			% name-value-pair.
			
			n = size(A(1).coefficient, 2);
			
			ip = misc.Parser();
			ip.addOptional('N', 1);
			ip.addOptional('B', quantity.Operator.empty(1, 0));
			ip.addParameter('F0', []);
			ip.parse(varargin{:});
			B = ip.Results.B;
			N = ip.Results.N;
			
			if isempty(B)
				m = 0;
			else
				m = size(B(1).coefficient, 2);
			end
			nA = length(A); % As order of a polynom is zero based
						
			% initialization of the ProgressBar
			counter = N - 1;
			pbar = misc.ProgressBar(...
				'name', 'Trajectory Planning (Woittennek)', ...
				'terminalValue', counter, ...
				'steps', counter, ...
				'initialStart', true);
			
			%% computation of transition matrix as power series
			% The fundamental matrix is considered in the laplace space as the state
			% transition matrix of the ODE with complex variable s:
			%   dz w(z,s) = A(z, s) w(z,s) * B(z) v(s),
			% Hence, fundamental matrix is a solution of the ODE:
			%   dz Phi(z,zeta) = A(Z) Phi(z,zeta)
			%   Phi(zeta,zeta) = I
			%
			%   Psi(z,xi,s) = int_zeta^z Phi(z, xi, s) B(xi) d_xi
			%
			% At this, A(z, s) can be described by
			%   A(z,s) = A_0(z) + A_1(z) s + A_2(z) s^2 + ... + A_d(z) s^d
			%
			% With this, the recursion formula
			%   Phi_k(z, zeta) = int_zeta^z Phi_0(z, xi) * sum_i^d A_i(xi) *
			%   Phi_{k-i}(z, xi) d_xi
			%   Psi_k(z, zeta) = int_zeta^z Phi_0(z, xi) * sum_i^d A_i(xi) *
			%   Psi_{k-i}(z, xi) + B_k(xi) d_xi
			
			if ip.isDefault('F0') || isempty(ip.Results.F0)
				F0 = quantity.Discrete(...
					A(1).coefficient.stateTransitionMatrix(...
					'gridName1', 'z', 'gridName2', 'zeta'));
			else
				F0 = ip.Results.F0;
			end
			z = F0(1).grid{1};
			Phi(:,:,1) = F0.subs('zeta', 0);
			if isempty(B)
				Psi(:,:,1) = quantity.Discrete.empty();
			else
				Psi(:,:,1) = cumInt( F0 * B(1).coefficient.subs('z', 'zeta'), ...
				'zeta', z(1), 'z');
			end
						
			On = quantity.Discrete.zeros([n n], z, 'gridName', 'z');
			Om = quantity.Discrete.zeros([n m], z, 'gridName', 'z');
			for k = 2 : N
				pbar.raise();

				% compute the temporal matrices:
				sumPhi = On;
				sumPsi = Om;
				for l = 2 : min(nA, k)
					sumPhi = sumPhi + A(l).coefficient * Phi(:, :, k-l+1);
					sumPsi = sumPsi + A(l).coefficient * Psi(:, :, k-l+1);
				end
				if size(B, 1) >= k && ~isempty(B)
					sumPsi = sumPsi + B(k).coefficient(:, :);
				end
				
				% compute the integration of the fundamental matrix with the temporal
				% values:
				%   Phi_k(z) = int_0_z Phi_0(z, zeta) * M(zeta) d_zeta
				Phi(:, :, k) = cumInt(F0 * sumPhi.subs('z', 'zeta') , ...
					'zeta', z(1), 'z');
				
				Psi(:, :, k) = cumInt(F0 * sumPsi.subs('z', 'zeta'), ...
					'zeta', z(1), 'z');
			end
			%
			pbar.stop();
			
			% TODO: warning schreiben, dass überprüft ab welcher Ordnung
			% die Matrizen nur noch numerisches Rauschen enthalten!
			
			Phi = quantity.Operator(Phi);
			Psi = quantity.Operator(Psi);
		end
		
		function newOperator = changeGrid(obj, gridNew, gridNameNew)
			newOperator = quantity.Operator(obj.M.changeGrid(gridNew, gridNameNew));
		end
		function newOperator = subs(obj, varargin)
			newOperator = obj.M.subs(varargin{:});
		end	
	end
	
	methods (Access = protected)
		function m = M(obj)
			m = builtin('cat', 3, obj.coefficient);
		end
	end
	
	methods (Static)
		function e = empty(varargin)
			o = quantity.Discrete.empty(varargin{:});
			e = quantity.Operator(o);
		end
	end
	
	methods (Static, Access = protected)
		function var = getVariable(symbolicFunction, obj)
			var = getVariable@quantity.Symbolic(symbolicFunction);
			idxS = arrayfun(@(s) isequal(s, obj.operatorVar), var);
			var = var(~idxS);
		end
	end
	
end

