%% example.m gives an overview of basic mathematical operations 
% using quantity.Discrete class

%% Create a new quantity
% First, we want to create a scalar property a(z) that depends on a spatial variable 
% z \in [0, 1] and is defined by a = 1 + z.
zGrid = linspace(0, 1, 101);
a = quantity.Discrete(...
	1+zGrid(:), ...			% numerical data describing 1 + z
	'grid', {zGrid}, ...	% the grid for which  the data is defined
	'gridName', {'z'}, ...	% name of the grid variable
	'name', 'a');			% name of the scalar (optional)
% all those inputs can be accessed by a.[property-name], for instance a.gridName. The
% numerical data is available via a.valueDiscrete or a.on().

% verify that a(z) is the desired variable by plotting it:
a.plot();

%% Access to numerical data
% verify that a(z) = 1 + z by evaluating it for different points in z = 0, 0.5, 1:
[a.on(0), a.on(0.5), a.on(1)]
% or more compact:
a.on([0, 0.5, 1])

% on(gridValues) performs an interpolation, when the specified values do not lie on
%  the grid a.grid, for instance
a.on(0.1234567123123123);

% on() without any input arguments returns the original data, 
isequal(a.on(), 1+zGrid(:));

%% Lets do some basic math operations
% spatial derivatives
a_dz = a.diff(1, 'z'); % reads 1st order derivative of a w.r.t. z
a_dzz = a.diff(2, 'z'); % reads 2nd order derivative of a w.r.t. z
% illustrate result by a plot
plot([a; a_dz; a_dzz]);

% plus and minus
aPlus1 = a + 1;
TwoMinusA = 2-a;
plot([a; aPlus1; TwoMinusA]);

%% Sustitution: b = a(zeta)
b = a.subs('z', 'zeta');
b.name = 'b';

%% Multiplication c(z, zeta) = a(z) * b(zeta)
c = a*b;
c.plot();

%% flip grid, i.e. d(z) = a(1-z)
d = a.flipGrid('z');
[d.name] = deal('d');
plot([a; d]);

%% Access of numerical data of multidimensional data c(z, zeta)
% use c.on({zValue, zetaValue}, {'z', 'zeta'}) for gaining an interpolated value of c
cDisc = c.on({linspace(0, 1, 11), linspace(0, 1, 11)}, {'z', 'zeta'});
misc.subsurf(cDisc, 'xLabel', 'z', 'yLabel', '\zeta', 'myTitle', 'c');

% use subs to replace variables z or zeta with a constant value, for instance,
% c(0.25, zeta)
c.subs('z', 0.25).plot();

%% Computation with matrices and arrays
% create new data: K = [z^2+zeta, zeta-z; z, cos(z)]
% when creating multidimensional quantity matrices, the first dimensions of the 
% numerical input data must represent the spatial variables z, zeta. The ending
% dimensions represent the columns, rows, etc...
[zNdgrid, zetaNdgrid] = ndgrid(zGrid, zGrid);
Kmat = cat(4, ...								% combine column-wise
	cat(3, zNdgrid.^2+zetaNdgrid, zNdgrid), ... % 1. column of K = Kmat(:,:,:,1)
	cat(3, zetaNdgrid-zNdgrid, cos(zNdgrid)));	% 2. column of K = Kmat(:,:,:,2)
% hence, size(Kmat) == [numel(zGrid), numel(zGrid), 2, 2];
K = quantity.Discrete(...	
	Kmat, ...
	'grid', {zGrid, zGrid}, ...
	'gridName', {'z', 'zeta'}, ...
	'name', 'K');
% If you have access to the Symbolic Toolbox, you can use instead
% syms z zeta
% K = quantity.Symbolic([z^2+zeta, zeta-z; z, cos(z)], 'grid', {zGrid, zGrid}, ...
% 	'variable', {z, zeta}, 'name', 'K');

x = quantity.Discrete(...
	[linspace(-1, 1, numel(zGrid)).', linspace(-1, 1, numel(zGrid)).^2.'], ...
	'grid', zGrid, 'gridName', 'z', 'name', 'x');

% Note that K and x are array of quantities, hence size(K)==[2,2], size(x) == [2,1].

%% Volterra integral of 2nd kind
% After this lenghty initialisation of the data, calculation is easy.
% Consider the calculation of y = x + int_0^z K(z, zeta) * x(zeta) dzeta
y = x + cumInt(K*x.subs('z', 'zeta'), 'zeta', 0, 'z');
% cumInt:	1. input: integrand K(z, zeta) * x(zeta), 
%			2. input: integration variable 'zeta'
%			3. input: lower bound of integral 0
%			4. input: upper bound of integral 'z'
% plot result:
y.plot();

%% Further commonly used methods
% - basic opertions * = mtimes(), inv(), exp(), expm(), sqrt(), sqrtm(), uminus()
% - integration: int(), cumInt()
% - real & imaginary parts: imag(), real()
% - min(), max(), mean(), median(), abs(), MAX()