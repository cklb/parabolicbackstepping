function rounded = acc( num,accuracy )
%   ACC round a number to a fixed number of digits
%
%   important function to compare float comma numbers for equality:
% PROBLEM: Gleichheitsoperator, >= und <= Operator haben in Matlab
% einen Fehler f�r float Datentypen!! 
% bestes Beispiel:
% teste
% 0.6==(0.4+0.2),
% 0.6>=(0.4+0.2),
% 0.6<=(0.4+0.2)
% erstaunliches Ergebnis! Scheinbar wird die Fliesskommazahl auch
% nur mit einer Nachkommastelle irgendwo aufgerundet...
% Deshalb wird hier noch eine Sicherheitsrundung eingef�hrt!
% Sinnvoll ist es also, die Diskretisierungen so zu w�hlen, dass
% die Abst�nde gleich gro� sind, oder ein vielfaches von einander!
%
%   Inputs:
%       num         Value to be rounded
%       accuracy    
%   Outputs:
%       rounded     Correct rounded value of num
%
%   Example:
%   -----------------------------------------------------------------------
%   0.6 == (0.4+0.2)
%   >> ans = 0
%   0.6 == (numeric.acc(0.4+0.2))
%   >> ans = 1
%   -----------------------------------------------------------------------
%

if nargin<2
	accuracy = 1e6;
end
rounded = round(num*accuracy)/accuracy;

end

