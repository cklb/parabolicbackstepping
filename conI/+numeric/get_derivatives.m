function [dzx, x_til, grad_op] = get_derivatives(znum, epsilon, x0, q1)
%
% dzx = GET_DERIVATIVES(znum,epsilon,k)
% Computes the spatial partial derivatives \partial_z x(z) of a 
% function x(z) over the spatial domain z \in [0 1] with the boundary
% condition \partial_z x(0)=q1x(0)
% by inverting the trapezodial rule by transforming the problem into
% an optimization problem. 
% At the moment, the spatial discretization must be equidistant.
% (See Tilman Utz: Control of Parabolic Partial Differential Equations
% Based on Semi-Discretisations, p. 82 ff.)
%
%
% INPUT PARAMETRS:
%
%   DOUBLE ARRAY    znum    : number of equidistant discretization points
%   DOUBLE ARRAY    epsilon : coefficient of smoothness of the solution
%   DOUBLE ARRAY    x0	  : values of the function x(z) at the
%								discretization points given by znum;
%   DOUBLE ARRAY    q1	  : parameter of boundary condition at z=0:
%								\partial_z x(0) = q_1 x(0)
%
%
% OUTPUT PARAMETERS:
%
%   DOUBLE ARRAY    dzx     : derivatives of the function x(z) at the 
%       					  discretization points given by znum;
%   DOUBLE ARRAY    x_til   : approximate solution of x(z), computed by
%							  numerical integration over dzx
%   DOUBLE ARRAY    grad_op : gradient operator:
%							  \tilde{x}' = grad_op * x,
%							  where \tilde{x}' contains the value of x at
%							  point z=0, and the derivatives x_i' at all
%							  points i=2...znum
%
%

% history:
% created on 07.04.2014 by Simon Kerschbaum

% Check if znum>1
if ~isnumeric(znum)|| znum<2
	error('znum must be a positive integer value > 1')
end

delta_z  = 1/(znum-1); %L�nge der Abtastschritte (�quidistant)

% force x0 to be column vector:
x0=x0(:);

% Matrix L berechnen: (4.62)
% erste Reihe speziell:
L_til(1,1:znum)=0;
L_til(1,1)=1;
% erst mal gesamtes L
for row = 2:znum
	L_til(row,:)= [1+q1*delta_z/2, delta_z*ones(1,row-2), delta_z/2, zeros(1,znum-row)];
end

% R_reg berechnen
R_reg(1,1:znum) = 0;
R_reg(1:znum,1)=0;
R_til = epsilon*eye(znum-1,znum-1);
R_reg(2:znum,2:znum)=R_til;


grad_op = (L_til'*L_til+R_reg)^(-1)*L_til';
dzx_til = grad_op*x0;

% erster Wert ist keine Ableitung, interessiert nicht
% Randbedingung wieder einf�gen: (durch entfernen des ersten Elements ist
% bp eins nach links gewandert...
dzx = [q1*x0(1); dzx_til(2:end)];

% approximate x(z)
x_til(1)=0;
zdisc=linspace(0,1,znum);
for i=2:znum
	x_til(i) = trapz(zdisc(1:i),dzx(1:i));
end