function x_k = ode4_one_step(fun, dt, x_km1, varargin)
%ode4_one_step calculates one timestep with the Runge-Kutta scheme.
%
% INPUT PARAMETERS:
% 	FUNCTION		fun : rhs of ode
%	SCALAR			 dt : step size
%	VECTOR		  x_km1 : value of state in last time step km1="k minus	1"
%	VARARGIN	varargin: further parameters of fun
%
% OUTPUT PARAMETERS:
%	VECTOR			x_k : value of state in current time step k
%
% Example:
% -------------------------------------------------------------------------
% y(1,1) = 3;
% fun = @(x) -2*x;
% dt = 0.05;
% for it = 1:20
%     y(it+1,1) = numeric.ode4_one_step(fun,dt,y(it,1));
% end
% plot(0:dt:1,y);
% -------------------------------------------------------------------------
%
% required subprogramms:
%   - 
%
% global variables:
%   - 
%
% history:
%	-	created on 17.04.2018 by Jakob Gabriel (inspired by wikipedia)

%%
k_1 = feval(fun, x_km1, varargin{:});
k_2 = feval(fun, x_km1 + k_1*0.5*dt, varargin{:});
k_3 = feval(fun, x_km1 + k_2*0.5*dt, varargin{:});
k_4 = feval(fun, x_km1 + k_3*dt, varargin{:});
x_k = x_km1 + dt*(k_1 + 2*k_2 + 2*k_3 + k_4)/6;
	
end
