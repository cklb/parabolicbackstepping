function [M, K, L, P, PHI] = femMatrices(varargin)
%FEMMATRICES computes the discretization matrices obtained by FE-Method
% [M, K, L, P, PHI] = femMatrices(varargin) computes the matrices required
% for the approximation of a system using FEM. For instance, the rhs of a pde
%		dz (alpha(z) dz x(z)) + beta(z) dz x(z) + gamma(z)
% is approximated as K x(z)
% The matrices that will be returned as described in the following:
%	The mass-matrix M is computed using
%		M = int phi(z) phi(z)
%	The stiffness matrix K is computed with
%		K0 = int phi(z) gamma(z) phi(z)
%		K1 = int phi(z) beta(z) (dz phi(z))
%		K2 = int - (dz phi(z)) alpha(z) (dz phi(z)) 
%		K = K0 + K1 + K2
%	The input matrix L:
%		L = int( phi(z) * b(z) )
%	The output matrix P:
%		P = int( c(z) * phi'(z) )
%
%	PHI is the trial function used for the discretization.
%
%	The input arguments can be given as name-value-pairs. The parameters
%	are described in the following, the default values are written in (.).
%	'grid' (linspace(0, 1, 11)): grid of resulting finite elements
%	'Ne'(111): the implementation uses numerical integration for each element.
%		   The parameter 'Ne' defines the grid for this integration.
%	'alpha' : the system parameter alpha, should be a quantity.Discrete.
%	'beta'  : the system parameter beta, should be a quantity.Discrete
%	'gamma' : the system parameter gamma, should be a quantity.Discrete
%	'b'     : the input vector, should be a quantity.Discrete
%	'c'		: the output vector, should be a quantity.Discrete

myParser = misc.Parser();
myParser.addOptional('grid', linspace(0, 1, 11));
myParser.addOptional('Ne', 111);
myParser.addOptional('alpha', quantity.Discrete.empty(1,0));
myParser.addOptional('beta', quantity.Discrete.empty(1,0));
myParser.addOptional('gamma', quantity.Discrete.empty(1,0));
myParser.addOptional('b', quantity.Discrete.empty(1,0));
myParser.addOptional('c', quantity.Discrete.empty(0,1));
myParser.addOptional('silent', false);
myParser.parse(varargin{:});

% set the system properties
z = myParser.Results.grid(:);
N = numel(z);
z0 = z(1);
z1 = z(end);
pbar = misc.ProgressBar('name', 'FEM', 'terminalValue', N-1, 'steps', N-1, ...
	'initialStart', true, 'silent', myParser.Results.silent);

alf = myParser.Results.alpha;
bta =  myParser.Results.beta;
gma = myParser.Results.gamma;
b = myParser.Results.b;
c = myParser.Results.c;

p = size(b, 2); % number of the input values
m = size(c, 1); % number of the output values

% define the properties of one finite element
element.N = myParser.Results.Ne;
element.delta = (z1 - z0) / (N - 1);
element.z = linspace(0, element.delta, element.N)';

%% verify that grid is homogenious
% Yet, this function only supports homogenious grids for most output parameters.
% Only, the mass matrix M is calculated correct for nonhomogenious grids. The
% following line ensures, that if the grid is not homogenious, only M is returned.
isGridHomogenious = all(diff(z) - element.delta < 10*eps);
if ~isGridHomogenious
	assert(nargout <= 1, 'non-homogenious grids only supported for mass-matrix');
end

%% define the trial function
%         /   z_kp1 - z   \
%         | ------------- |
%         |     deltaZ_k  |
% phi_k = |               |
%         |   z - z_k     |
%         | ------------  |
%         \    deltaZ_k   /

PHI = quantity.Discrete({(element.delta - element.z) / element.delta; ...
	(element.z / element.delta) }, ...
	'size', [2, 1], 'gridName', 'z', 'grid', element.z, 'name', 'phi');

%% compute the mass matrix
M0 = 1 / 6 * [2, 1; 1, 2];
element.diff = diff(z);

%% compute the stiffness matrix

% initialize the trial functions for the computation for element matrices.
% for further comments, see the description in the loop
pp = reshape(double(PHI * PHI'), element.N, 4);
phi = double(PHI);
dphi = [ -1 / element.delta; ...
	1 / element.delta ];
dphidphi = reshape(dphi * dphi', 1, 4);

% initialize the element matrices
K0 = zeros(2,2);
K1 = zeros(2,2);
K2 = zeros(2,2);
L0 = zeros(2,0);
P0 = zeros(0,2);

% initialize the assembly matrices:
M = zeros(N, N);
K = zeros(N, N);
L = zeros(N, p);
P = zeros(m, N);

% set the flags for the values which should be computed:
flags.alpha = ~isempty(alf);
flags.beta = ~isempty(bta);
flags.gamma = ~isempty(gma);
flags.b = ~isempty(b);
flags.c = ~isempty(c);

for k = 1:N-1
	pbar.raise(k);
	idxk=[k, k+1];
	zk = linspace(z(k), z(k+1), element.N)';
	
	if flags.gamma
		% compute the elements for K0 = int phi(z) * gamma(z) * phi'(z)
		%	the matrix phi(z) * phi'(z) is the same for each element. Thus,
		%	this matrix is computed a priori. To simplify the multiplication
		%	with the scalar value gamma(z), the matrix phi*phi' is vectorized
		%	and the result of phi(z)phi'(z) alpha(z) is reshaped as a matrix.
		k0 = misc.multArray(pp, gma.on(zk), 3, 2, 1);
		K0 = reshape( numeric.trapz_fast(zk, k0'), 2, 2);
	end
	
	if flags.beta
		% compute the elements for K1 = int phi(z) * beta(z) * dz phi'(z))
		%	as the values dz phi(z) are constants, the multiplication can be.
		%	simplified.
		k1 = misc.multArray(phi, bta.on(zk) * dphi', 3, 3, 1);
		K1 = reshape( numeric.trapz_fast(zk, reshape(k1, element.N, 4, 1)' ), 2, 2);
	end
	
	if flags.alpha
		% compute the elements for K2 = int dz phi(z) * alpha(z) * dz phi'(z)
		%	as the matrix dzphi*dzphi' is constnat the multiplication
		%	is simplified by vectorization of dzphi*dzphi' and a reshape to
		%	obtain the correct result.
		k2 = alf.on(zk) * dphidphi;
		K2 = reshape( numeric.trapz_fast(zk, k2'), 2, 2);
	end
	
	if flags.b
		% compute the input matrix L = int phi(z) * b(z)
		l = reshape( misc.multArray(phi, b.on(zk), 3, 2, 1), element.N, 2*p);
		L0 = reshape( numeric.trapz_fast(zk, l'), 2, p);
	end
	
	if flags.c
		% compute the output matrix P = int c(z) * phi(z)'
		c0 = reshape( misc.multArray(c.on(zk), phi, 3, 3, 1), element.N, 2*m);
		P0 = reshape( numeric.trapz_fast(zk, c0'), m, 2);
	end
	
	% assemble the matrices with the elements:
	M(idxk, idxk) = M(idxk, idxk) + M0 * element.diff(k);
	
	K(idxk, idxk) = K(idxk, idxk) + K0 + K1 + K2;
	L(idxk, :) = L(idxk, :) + L0;
	P(:, idxk) = P(:, idxk) + P0;
end


pbar.stop();
end

