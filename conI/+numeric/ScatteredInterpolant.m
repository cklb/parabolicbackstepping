classdef ScatteredInterpolant < handle & matlab.mixin.Copyable
	% This class is needed to deal with arrays of scatteredInterpolants.
	% scatteredInterpolants can be used to interpolate between arbitrary
	% sample points, see >> doc scatteredInterpolant
	
	properties (SetAccess = immutable)
		interpolant (1, 1) scatteredInterpolant;
	end % properties (SetAccess = private)
	
	methods
		function obj = ScatteredInterpolant(x, y, value, varargin)
			% x, y, and value must be double arrays of same length
			% containing all sample data.
			if nargin > 0 % this if-clause is needed to support
				% Characteristic-Array. Whenever a new obj(it) is
				% initialized, the constructor is called without
				% input-arguments. This would lead to errors, that are
				% avoided by this condition.
				myParser = misc.Parser();
				myParser.addParameter('Method', 'linear'); % Caution: 'natural' might cause NaN-results.
				myParser.addParameter('ExtrapolationMethod', 'nearest');
				myParser.parse(varargin{:});
				obj.interpolant = scatteredInterpolant(x(:), y(:), value(:));
				obj.interpolant.Method = myParser.Results.Method;
				obj.interpolant.ExtrapolationMethod = myParser.Results.ExtrapolationMethod;
			else
				obj.interpolant = scatteredInterpolant(...
					[0; 0; 1; 1], [0; 1; 0; 1], [0; 0; 0; 0], varargin{:});
			end
		end % ScatteredInterpolant() constructor
		
		function value = evaluate(obj, myGrid, varargin)
			% evaluate returns the interpolated values on the grid myGrid
			% with the size(value) = [size(myGrid{1}), size(myGrid{2}),
			% size(obj)]
			if ~iscell(myGrid)
				myGrid = {myGrid, myGrid};
			end
			if numel(myGrid) == 1
				myGrid = {myGrid{1}, myGrid{1}};
			end
			[ndGrid1, ndGrid2] = ndgrid(myGrid{1}, myGrid{2});
			value = zeros([size(ndGrid1), numel(obj)]);
			parfor it = 1 : numel(obj)
				if ~isempty(obj(it).interpolant.Values)
					% Empty interpolants return 0, others the interpolated
					% value.
					value(:, :, it) = obj(it).interpolant(ndGrid1, ndGrid2);
				end
			end
 			value = reshape(value, [size(ndGrid1), size(obj)]);
			
% 			The following code does similar as the for-loop, but sadly it
% 			is three times slower and faulty. valueCell =
% 			arrayfun(@helperFunction, obj, 'UniformOutput', false);
%  			value = reshape(permute(cell2mat(valueCell), [2, 3, 1]),...
% 				[cellfun(@numel, myGrid), size(obj)]);
% 			function result = helperFunction(obj)
% 				result = reshape(obj.interpolant(myGrid), [1,
% 				numel(myGrid{1}), numel(myGrid{2})]);
% 			end
		end % evaluate()
		
		
	end % methods
	
end % classdef
