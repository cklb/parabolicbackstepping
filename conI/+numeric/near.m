function [i, m, s] = near(nominalValue, actualValue, tolerance, relative)
%NEAR checks 'equality' for double numerical values.
%   i = near(nominalValue, actualValue, tolerance) checks if.
%       abs(nominalValue - actualValue) < tolerance.
%   The argument tolerance is optional and is 10*eps by default.

if nargin <= 2
    tolerance = 10*eps;
end

if nargin == 1
	actualValue = zeros(size(nominalValue));
end

if nargin < 4
	relative = 1;
else
	if islogical(relative)
		relative = max(abs(nominalValue(:)));
	end
end
	

absV = abs(nominalValue ./ relative - actualValue ./ relative);
i = all(absV(:) < tolerance);
m = max( absV(:) );

s = sprintf('Is near %i, maximal difference: %d', [i, m]);

if nargout == 0
	i = s;
end

end