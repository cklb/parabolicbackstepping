function si = smoothStep(deg, varargin)
% TODO@ferdinand: documentation required
z = sym('z', 'real');
fhi = quantity.Symbolic( z.^((deg+1):(2*deg + 1))', varargin{:});

M = zeros(deg + 1);
for k = 1:(deg+1)
    M(k,:) = fhi.diff(k-1).atIndex(end)';
end

b = zeros(deg+1, 1);
b(1) = 1;

al = M \ b;

si = fhi' * al;

end