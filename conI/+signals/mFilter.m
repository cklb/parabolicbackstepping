function y = mFilter(fil, u)
% filter is a multidimensional array of size
%   size(fil) = (Ntau, n, m); 
%       Ntau: number of filter coefficients
%       n: number of rows
%       m: number of columns
%
%   size(u) = (Nt, m, p)
%       Nt: number of time steps
%       m: number of rows
%       p: number of columns

    assert(size(fil, 3) == size(u, 2));

   % dimensions and initialization
   q = size(u, 1);
   n = size(fil, 2);
   o = size(u, 2);
   m = size(u, 3);

   y = zeros(q, n, m);

    for k = 1 : n % rows of P
        for l = 1 : m % columns of P
            for p = 1 : o % rows of B or columns of A
                y(:, k, l) = y( :, k, l ) + filter(fil(:, k, p), 1, u(:, p, l));
            end
        end
    end

end