classdef WhiteGaussianNoise
	%WhiteGaussianNoise 
	
	properties
		sigma;
		mue = 0;
	end
	
	properties (Dependent)
		variance;
	end
	
	
	methods
		function obj = WhiteGaussianNoise(sigma)
			obj.sigma = sigma;
		end
		
		function s = get.variance(obj)
			s = obj.sigma^2;
		end
		
		function o = noise(obj, t)
			o = randn(size(t)) * sqrt(obj.variance);
		end
		
		function plotProbabilityDistributionFunction(obj, noise)
			histogram(noise);
		end
		
		function p = probabilityDensityFunction(obj, varargin)
			
			prsr = misc.Parser();
			prsr.addParameter('grid', linspace(- 5* obj.sigma, 5 * obj.sigma)');
			prsr.parse(varargin{:});
			
			x = sym('x', 'real');
			p = quantity.Symbolic( 1 / sqrt( 2 * pi ) / obj.sigma * ...
				exp( - (x - obj.mue)^2 / 2 / obj.sigma^2 ), ...
				'grid', prsr.Results.grid, ...
				'variable', x);
		end
		
		function P = cumulativeDistributionFunction(obj, varargin)
			p = obj.probabilityDensityFunction(varargin{:});
			P = p.cumInt('x', p.grid{1}(1), 'x');
		end
		
		function p = probability(obj, z)
			% computes the probability, that a value is inside the domain
			%	abs(x) < z
			
			P = obj.cumulativeDistributionFunction;
			
			p = 2 * P.at(abs(z - obj.mue)) - 1;
			
		end
		
	end
end

