classdef GevreyFunction < quantity.BasicVariable
	%UNTITLED Summary of this class goes here
	%   Detailed explanation goes here
	
	properties
		% Gevrey-order
		order double = -1;
	end
	
	properties (Dependent)
		sigma;
	end
	
	methods
		function obj = GevreyFunction(varargin)
			
			if nargin == 0
				g = [];
			else
				g = @signals.gevrey.bump.g;
			end
			obj@quantity.BasicVariable(g, varargin{:});	
			if nargin > 0
				prsr = misc.Parser();
				prsr.addParameter('order', 1.5 );
				prsr.parse(varargin{:});
				obj.order = prsr.Results.order;
			end
		end
		
		
		
		function s = get.sigma(obj)
			s = 1 ./ ( obj.order - 1);
		end
		function set.sigma(obj, s)
			obj.order = 1 + 1/s;
		end
		
	end
	
	methods (Access = protected)
		function v = evaluateFunction(obj, grid)
			v = obj.K * obj.valueContinuous(grid, obj.diffShift, obj.T, obj.sigma);
		end
	end
end

