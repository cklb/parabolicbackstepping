classdef PolyBump < quantity.BasicVariable
	
	properties
		a double;
		b double;
		c double;
		sgn double;
		norm double;
	end
	
	properties ( Access = protected, Hidden)
		symbolicFunction;
		handleFunction;
	end
	
	methods
		function obj = PolyBump(varargin)			
			
			% make default grid:
			preParser = misc.Parser();
			preParser.addParameter('T', 1);
			preParser.addParameter('N_t', 101);
			preParser.addParameter('dt', []);
			preParser.addParameter('z', linspace(0, 1)');
			preParser.parse(varargin{:});

			if ~isempty(preParser.Results.dt)
				Nt = quantity.BasicVariable.setDt(preParser.Results.T, preParser.Results.dt);
			else
				Nt = preParser.Results.N_t;
			end			
						
			prsr = misc.Parser();
			prsr.addParameter('a', 1);
			prsr.addParameter('b', 1);
			prsr.addParameter('c', 0);
			prsr.addParameter('grid', {preParser.Results.z, linspace(0, preParser.Results.T, Nt)'});
			prsr.addParameter('gridName', {'z', 't'});
			prsr.addParameter('name', 'phi');
			prsr.addParameter('sgn', 1);
			prsr.addParameter('T', preParser.Results.T);
			prsr.addParameter('N_t', Nt);
			prsr.addParameter('norm', true);
			prsr.parse(varargin{:});			
			uargin = misc.struct2namevaluepair(prsr.Unmatched);
			argin = [uargin(:)', ...
				{'grid', prsr.Results.grid, ...
				 'gridName', prsr.Results.gridName, ...
				 'name', prsr.Results.name, ...
				 'T', preParser.Results.T, ...
				 'N_T', Nt}];
			
			obj@quantity.BasicVariable(@signals.polyBump, argin{:});

			obj.a = prsr.Results.a;
			obj.b = prsr.Results.b;
			obj.c = prsr.Results.c;
			obj.sgn = prsr.Results.sgn;
			obj.norm = prsr.Results.norm;
			
			if obj.norm ~= 0
				obj.K = 1 / double( int(subs(obj, 'z', 0)) ) / obj.norm;
			end
			
			
		end
	end
	
	methods (Access = protected)
		function v = evaluateFunction(obj, z, t)
			v = obj.K * obj.valueContinuous( t + obj.sgn * obj.c * z, obj.diffShift, obj.T, obj.a, obj.b, obj.c);
		end		
	end
	
	methods (Static)
		
		function [phi_p, phi_m] = makePolyBumpBasicVariable(varargin)
			
			phi_p = signals.PolyBump(varargin{:}, 'sgn', 1);
			phi_m = signals.PolyBump(varargin{:}, 'sgn', -1);
			
		end
	end
end


