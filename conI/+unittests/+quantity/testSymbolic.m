function [tests ] = testSymbolic()
%TESTQUANTITY Unittests for the quantity objects
%   Add all unittests for the quantity files here
tests = functiontests(localfunctions());
end

function testVec2Diag(testCase)
% quantity.Symbolic
syms z
myMatrixSymbolic = quantity.Symbolic([sin(0.5*z*pi)+1, 0; 0, 0.9-z/2]);
myVectorSymbolic = quantity.Symbolic([sin(0.5*z*pi)+1; 0.9-z/2]);

testCase.verifyTrue( myVectorSymbolic.vec2diag.near(myMatrixSymbolic) );
end

function testSymbolicEvaluation(tc)
syms z
myGrid = linspace(0, 1, 7);
fS = quantity.Symbolic(z * sinh(z * 1e5) / cosh(z * 1e5), ...
	'variable', {z}, 'grid', {myGrid}, 'symbolicEvaluation', true);
fF = quantity.Symbolic(z * sinh(z * 1e5) / cosh(z * 1e5), ...
	'variable', {z}, 'grid', {myGrid}, 'symbolicEvaluation', false);

tc.verifyTrue(any(isnan(fF.on)))
tc.verifyFalse(any(isnan(fS.on)))
end

function testCtranspose(tc)
syms z zeta
qSymbolic = quantity.Symbolic(...
	[1+zeta, -zeta; -z, z^2], 'grid', {linspace(0, 1, 21), linspace(0, 1, 41)},...
	'variable', {z, zeta}, 'name', 'q');
qSymbolicCtransp = qSymbolic';
tc.verifyEqual(qSymbolic(1,1).on(), qSymbolicCtransp(1,1).on());
tc.verifyEqual(qSymbolic(2,2).on(), qSymbolicCtransp(2,2).on());
tc.verifyEqual(qSymbolic(1,2).on(), qSymbolicCtransp(2,1).on());
tc.verifyEqual(qSymbolic(2,1).on(), qSymbolicCtransp(1,2).on());

qSymbolic2 = quantity.Symbolic(sym('z') * 2i + sym('zeta'), ...
	'grid', {linspace(0, 1, 21), linspace(0, 1, 11)}, 'variable', sym({'z', 'zeta'}), 'name', 'im');
qSymolic2Ctransp = qSymbolic2';
tc.verifyEqual(qSymbolic2.real.on(), qSymolic2Ctransp.real.on());
tc.verifyEqual(qSymbolic2.imag.on(), -qSymolic2Ctransp.imag.on());
end % testCtranspose

function testTranspose(tc)
syms z zeta
qSymbolic = quantity.Symbolic(...
	[1+z*zeta, -zeta; -z, z^2], 'grid', {linspace(0, 1, 21), linspace(0, 1, 41)},...
	'variable', {z, zeta}, 'name', 'q');
qSymbolicTransp = qSymbolic.';
tc.verifyEqual(qSymbolic(1,1).on(), qSymbolicTransp(1,1).on());
tc.verifyEqual(qSymbolic(2,2).on(), qSymbolicTransp(2,2).on());
tc.verifyEqual(qSymbolic(1,2).on(), qSymbolicTransp(2,1).on());
tc.verifyEqual(qSymbolic(2,1).on(), qSymbolicTransp(1,2).on());
end % testTranspose

function testFlipGrid(tc)
syms z zeta
myGrid = linspace(0, 1, 11);
f = quantity.Symbolic([1+z+zeta; 2*zeta+sin(z)] + zeros(2, 1)*z*zeta, ...
	'variable', {z, zeta}, 'grid', {myGrid, myGrid});
% flip zeta
fReference = quantity.Symbolic([1+z+(1-zeta); 2*(1-zeta)+sin(z)] + zeros(2, 1)*z*zeta, ...
	'variable', {z, zeta}, 'grid', {myGrid, myGrid});
fFlipped = f.flipGrid('zeta');
tc.verifyEqual(fReference.on, fFlipped.on, 'AbsTol', 10*eps);

% flip z and zeta
fReference2 = quantity.Symbolic([1+(1-z)+(1-zeta); 2*(1-zeta)+sin(1-z)] + zeros(2, 1)*z*zeta, ...
	'variable', {z, zeta}, 'grid', {myGrid, myGrid});
fFlipped2 = f.flipGrid({'z', 'zeta'});
tc.verifyEqual(fReference2.on, fFlipped2.on, 'AbsTol', 10*eps);
end % testFlipGrid();

function testCumInt(testCase)
tGrid = linspace(pi, 1.1*pi, 51)';
sGrid = tGrid;
syms s t

a = [ 1, s; t, 1];
b = [ s; 2*s];

%% int_0_t a(t,s) * b(s) ds
% compute symbolic version of the volterra integral
integrandSymbolic = quantity.Symbolic(a*b, 'grid', {tGrid, sGrid}, 'variable', {t, s});
integrandDiscrete = quantity.Discrete(integrandSymbolic);
V = cumInt(integrandSymbolic, 's', tGrid(1), 't');
f = cumInt(integrandDiscrete, 's', sGrid(1), 't');

testCase.verifyEqual(V.on(), f.on(), 'AbsTol', 1e-5);

%% int_s_t a(t,s) * b(s) ds
V = cumInt(integrandSymbolic, 's', 's', 't');
f = cumInt(integrandDiscrete, 's', 's', 't');

testCase.verifyEqual( f.on(f(1).grid, f(1).gridName), V.on(f(1).grid, f(1).gridName), 'AbsTol', 1e-5 );

%% testCumIntWithLowerAndUpperBoundSpecified
fCumInt2Bcs = cumInt(integrandSymbolic, 's', 'zeta', 't');
fCumInt2Cum = cumInt(integrandSymbolic, 's', tGrid(1), 't') ...
	- cumInt(integrandSymbolic, 's', tGrid(1), 'zeta');
testCase.verifyEqual(fCumInt2Bcs.on(fCumInt2Bcs(1).grid, fCumInt2Bcs(1).gridName), ...
	fCumInt2Cum.on(fCumInt2Bcs(1).grid, fCumInt2Bcs(1).gridName), 'AbsTol', 100*eps);
end

function testScalarPlusMinusQuantity(testCase)
syms z
myGrid = linspace(0, 1, 7);
f = quantity.Symbolic([1; 2] + zeros(2, 1)*z, ...
	'variable', {z}, 'grid', {myGrid});
testCase.verifyError(@() 1-f-1, '');
testCase.verifyError(@() 1+f+1, '');
end

function testNumericVectorPlusMinusQuantity(testCase)
syms z
myGrid = linspace(0, 1, 7);
f = quantity.Symbolic([1+z; 2+sin(z)] + zeros(2, 1)*z, ...
	'variable', {z}, 'grid', {myGrid});
a = ones(size(f));
testCase.verifyEqual(on(a-f), 1-f.on(), 'RelTol', 10*eps);
testCase.verifyEqual(on(f-a), f.on()-1, 'RelTol', 10*eps);
testCase.verifyEqual(on(a+f), f.on()+1, 'RelTol', 10*eps);
testCase.verifyEqual(on(f+a), f.on()+1, 'RelTol', 10*eps);
end

function testDiffWith2Variables(testCase)
syms z zeta
myGrid = linspace(0, 1, 7);
f = quantity.Symbolic([z*zeta, z; zeta, 1], ...
	'variable', {z, zeta}, 'grid', {myGrid, myGrid});
fdz = quantity.Symbolic([zeta, 1; 0, 0], ...
	'variable', {z, zeta}, 'grid', {myGrid, myGrid});
fdzeta = quantity.Symbolic([z, 0; 1, 0], ...
	'variable', {z, zeta}, 'grid', {myGrid, myGrid});

fRefTotal = 0*f.on();
fRefTotal(:,:,1,1) = 1;
testCase.verifyEqual(on(diff(f)), fRefTotal);
testCase.verifyEqual(on(diff(f, 1, {'z', 'zeta'})), fRefTotal);
testCase.verifyEqual(on(diff(f, 2, {'z', 'zeta'})), 0*fRefTotal);
testCase.verifyEqual(on(diff(f, 1, 'z')), fdz.on());
testCase.verifyEqual(on(diff(f, 1, 'zeta')), fdzeta.on());

end

function testGridName2variable(testCase)
syms z zeta eta e a
myGrid = linspace(0, 1, 7);
obj = quantity.Symbolic([z*zeta, eta*z; e*a, a], ...
	'variable', {z zeta eta e a}, ...
	'grid', {myGrid, myGrid, myGrid, myGrid, myGrid});

thisGridName = {'eta', 'e', 'z'};
thisVariable = gridName2variable(obj, thisGridName);
variableNames = cellfun(@(v) char(v), thisVariable, 'UniformOutput', false);
testCase.verifyEqual(thisGridName, variableNames);

thisGridName = 'z';
thisVariable = gridName2variable(obj, thisGridName);
testCase.verifyEqual(thisGridName, char(thisVariable));

testCase.verifyError(@() gridName2variable(obj, 't'), '');
testCase.verifyError(@() gridName2variable(obj, 123), '');

end

function testCat(testCase)
syms z zeta
f1 = quantity.Symbolic(1+z*z, 'grid', {linspace(0, 1, 21)}, 'variable', {z}, 'name', 'f1');
f2 = quantity.Symbolic(sin(z), 'grid', {linspace(0, 1, 21)}, 'variable', {z}, 'name', 'f2');

% vertical concatenation
F = [f1; f2];
testCase.verifyTrue(F(1) == f1)
testCase.verifyTrue(F(2) == f2)

% horizontal concatenation
F = [f1, f2];
testCase.verifyTrue(F(1) == f1)
testCase.verifyTrue(F(2) == f2)

% combined concatenation
F = [F; f1, f2];
testCase.verifyTrue(F(1,1) == f1);
testCase.verifyTrue(F(1,2) == f2);
testCase.verifyTrue(F(2,1) == f1);
testCase.verifyTrue(F(2,2) == f2);

% concatenation on different grids
f3 = quantity.Symbolic(cos(z), 'grid', {linspace(0, 1, 13)}, 'variable', {z}, 'name', 'f1');

F = [f1, f3];
testCase.verifyEqual(F(2).gridSize, f1.gridSize)

% concatenate quantitySymbolics and other values
F = [f1, 0];
testCase.verifyEqual(F(2).valueSymbolic, sym(0))
F = [f1, sym(zeros(1, 10))];
testCase.verifyEqual([F(2:end).valueSymbolic], sym(zeros(1,10)))

% 
F = [0, f1];
testCase.verifyEqual(F(1).valueSymbolic, sym(0))
end

function testExp(testCase)
% 1 spatial variable
syms z zeta
s1d = quantity.Symbolic(1+z*z, 'grid', {linspace(0, 1, 21)}, 'variable', {z}, 'name', 's1d');
testCase.verifyEqual(s1d.exp.on(), exp(s1d.on()));

% diagonal matrix
s2dDiag = quantity.Symbolic([1+z*zeta, 0; 0, z^2], 'grid', {linspace(0, 1, 21), linspace(0, 1, 41)},...
	'variable', {z, zeta}, 'name', 's2dDiag');
testCase.verifyEqual(s2dDiag.exp.on(), exp(s2dDiag.on()));
end

function testExpm(testCase)
syms z zeta
mat2d = quantity.Symbolic(ones(2, 2) + [1+z*zeta, 3*zeta; 2+5*z+zeta, z^2], 'grid', {linspace(0, 1, 21), linspace(0, 1, 41)},...
	'variable', {z, zeta}, 'name', 's2d');
mat2dMat = mat2d.on();
mat2dExpm = 0 * mat2d.on();
for zIdx = 1 : 21
	for zetaIdx = 1 : 41
		mat2dExpm(zIdx, zetaIdx, :, :) = expm(reshape(mat2dMat(zIdx, zetaIdx, :, :), [2, 2]));
	end
end
testCase.verifyEqual(mat2d.expm.on(), mat2dExpm, 'RelTol', 100*eps);

end

% function testCumInt(testCase) % not yet implemented
% % test by comparison with quantity-case
% syms z zeta
% gridZ = linspace(0, 1, 101);
% gridZeta = linspace(0, 1, 81);
% ASymbolic = quantity.Symbolic(2+[z, z+zeta; -sin(zeta), z*zeta], 'variable', {z, zeta}, ...
% 	'grid', {gridZ, gridZeta}, 'name', 'A');
% ADiscrete = quantity.Discrete(ASymbolic);
% 
% % no input parameter
% ASymbolicIntNoInputParameter = ASymbolic.cumInt();
% ADiscreteIntNoInputParameter = ADiscrete.cumInt();
% testCase.verifyEqual(ASymbolicIntNoInputParameter.on(), ADiscreteIntNoInputParameter.on(), 'AbsTol', 1e-12);
% 
% % specify integration variable
% ASymbolicIntZeta = ASymbolic.cumInt('intGridName', {'zeta'});
% ADiscreteIntZeta = ADiscrete.cumInt('intGridName', {'zeta'});
% testCase.verifyEqual(ASymbolicIntZeta.on(), ADiscreteIntZeta.on(), 'AbsTol', 1e-5);
% ASymbolicIntZ = ASymbolic.cumInt('intGridName', {'z'});
% ADiscreteIntZ = ADiscrete.cumInt('intGridName', {'z'});
% testCase.verifyEqual(ASymbolicIntZ.on(), ADiscreteIntZ.on(), 'AbsTol', 1e-5);
% 
% % specify boundaries variable
% ASymbolicIntZeta2Z = ASymbolic.cumInt('intGridName', {'zeta'}, 'boundaryGridName', {'z'});
% ADiscreteIntZeta2Z = ADiscrete.cumInt('intGridName', {'zeta'}, 'boundaryGridName', {'z'});
% testCase.verifyEqual(ASymbolicIntZeta2Z.on(), ADiscreteIntZeta2Z.on(), 'AbsTol', 1e-5);
% 
% % 
% %%
% % fCumInt2Bcs = cumInt(K, 'intGridName', 's', 'boundaryGridName', {'zeta', 't'});
% % fCumInt2Cum = cumInt(K, 'intGridName', 's', 'boundaryGridName', 't') ...
% % 	- cumInt(K, 'intGridName', 's', 'boundaryGridName', 'zeta');
% % testCase.verifyEqual(fCumInt2Bcs.on(), fCumInt2Cum.on(), 'AbsTol', 10*eps);
% end


function testOn(testCase)
syms z zeta
gridZ = linspace(0, 1, 40);
gridZeta = linspace(0, 1, 21);
A = quantity.Symbolic(2+[z, z^2, z+zeta; -sin(zeta), z*zeta, 1], 'variable', {z, zeta}, ...
	'grid', {gridZ, gridZeta}, 'name', 'A');

%%
AOnPure = A.on();
testCase.verifyEqual(2+gridZ(:)*ones(1, numel(gridZeta)), AOnPure(:,:,1,1));
testCase.verifyEqual(2+gridZ(:)*gridZeta, AOnPure(:,:,2,2));
testCase.verifyEqual(3+0*gridZ(:)*gridZeta, AOnPure(:,:,2,3));

%%
gridZ2 = linspace(0, 1, 15);
gridZeta2 = linspace(0, 1, 31);
AOnChangedGrid = A.on({gridZ2, gridZeta2}, {'z', 'zeta'});
testCase.verifyEqual(2+gridZ2(:)*ones(1, numel(gridZeta2)), AOnChangedGrid(:,:,1,1));
testCase.verifyEqual(2+gridZ2(:)*gridZeta2, AOnChangedGrid(:,:,2,2));
testCase.verifyEqual(3+0*gridZ2(:)*gridZeta2, AOnChangedGrid(:,:,2,3));

%%
AOnChangedGrid2 = A.on({gridZeta2, gridZ2}, {'zeta', 'z'});
testCase.verifyEqual((2+gridZ2(:)*ones(1, numel(gridZeta2))).', AOnChangedGrid2(:,:,1,1));
testCase.verifyEqual((2+gridZ2(:)*gridZeta2).', AOnChangedGrid2(:,:,2,2));
testCase.verifyEqual((3+0*gridZ2(:)*gridZeta2).', AOnChangedGrid2(:,:,2,3));
% testCase.verifyEqual(ones([21, 31, 2, 3]), ...
% 	A.on({linspace(0, 1, 21), linspace(0, 1, 31)}));
% testCase.verifyEqual(ones([21, 31, 2, 3]), ...
% 	A.on({linspace(0, 1, 21), linspace(0, 1, 31)}, {'z', 'zeta'}));
% testCase.verifyEqual(ones([21, 31, 2, 3]), ...
% 	A.on({linspace(0, 1, 21), linspace(0, 1, 31)}, {'zeta', 'z'}));
end

function testSqrt(testCase)
% 1 spatial variable
syms z zeta
s1d = quantity.Symbolic(1+z*z, 'grid', {linspace(0, 1, 21)}, 'variable', {z}, 'name', 's1d');
testCase.verifyEqual(s1d.sqrt.on(), sqrt(s1d.on()));

% 2 spatial variables
s2d = quantity.Symbolic(1+z*zeta, 'grid', {linspace(0, 1, 21), linspace(0, 1, 41)},...
	'variable', {z, zeta}, 'name', 's2d');
testCase.verifyEqual(s2d.sqrt.on(), sqrt(s2d.on()));

% diagonal matrix
s2dDiag = quantity.Symbolic([1+z*zeta, 0; 0, z^2], 'grid', {linspace(0, 1, 21), linspace(0, 1, 41)},...
	'variable', {z, zeta}, 'name', 's2dDiag');
testCase.verifyEqual(s2dDiag.sqrt.on(), sqrt(s2dDiag.on()));

end

function testSqrtm(testCase)
syms z zeta
mat2d = quantity.Symbolic(ones(2, 2) + [1+z*zeta, 3*zeta; 2+5*z+zeta, z^2], 'grid', {linspace(0, 1, 21), linspace(0, 1, 41)},...
	'variable', {z, zeta}, 'name', 's2d');
mat2dMat = mat2d.on();
mat2dSqrtm = 0 * mat2d.on();
for zIdx = 1 : 21
	for zetaIdx = 1 : 41
		mat2dSqrtm(zIdx, zetaIdx, :, :) = sqrtm(reshape(mat2dMat(zIdx, zetaIdx, :, :), [2, 2]));
	end
end
testCase.verifyEqual(mat2d.sqrtm.on(), mat2dSqrtm, 'AbsTol', 10*eps);
mat2drootquad = sqrtm(mat2d)*sqrtm(mat2d);
testCase.verifyEqual(mat2drootquad.on(), mat2d.on(), 'AbsTol', 100*eps);

end

function testInv(testCase)
%% scalar example
syms z
zGrid = linspace(0, 1, 21);
v = quantity.Symbolic(1+z*z, ...
	'grid', {zGrid}, 'variable', {z}, 'name', 's1d');
vInvReference = 1 ./ (1 + zGrid.^2);
vInv = v.inv();

%% matrix example
syms zeta
zetaGrid = linspace(0, pi);
zGrid = linspace(0, 1, 21);
[zNdGrid, zetaNdGrid] = ndgrid(zetaGrid, zGrid);

K = quantity.Symbolic([1+z*z, 0; 0, 1+z*zeta], ...
	'grid', {zGrid, zetaGrid}, 'gridName', {'z', 'zeta'});
kInv = inv(K);

%%
testCase.verifyEqual(vInv.on(), vInvReference(:))
testCase.verifyEqual(kInv(1, 1).on(), repmat(vInvReference(:), [1, 100]))
testCase.verifyEqual(kInv(2, 2).on(), 1./(1+zNdGrid .* zetaNdGrid).')

end

function testZero(testCase)

x = quantity.Symbolic(0);

verifyEqual(testCase, x.on(), 0);
verifyTrue(testCase, misc.alln(on(x) * magic(5) == zeros(5)));

end

function testCast2Quantity(testCase)

syms z t
x = quantity.Symbolic(sin(z * t * pi), 'grid', {linspace(0, 1).', linspace(0, 2, 200)});

X = quantity.Discrete(x);

verifyTrue(testCase, numeric.near(X.on(), x.on()));
end

function testMrdivide(testCase)
syms z t
x = quantity.Symbolic(sin(z * t * pi), 'grid', {linspace(0, 1).', linspace(0, 2, 200)});
o = x / x;

verifyTrue(testCase, misc.alln(o.on == 1));

% test constant values
invsin = 1 / ( x + 8);
[Z, T] = meshgrid(x.grid{:});
verifyTrue(testCase, numeric.near(1 ./ (sin(Z' .* T' * pi) + 8), invsin.on()));

end

function testMTimesConstants(testCase)
syms z t
x = quantity.Symbolic(sin(z * t * pi), 'grid', {linspace(0, 1).', linspace(0, 2, 200)});

% test multiplicaiton iwth a constant scalar
x5 = x * 5;
[Z, T] = meshgrid(x(1).grid{:}); % meshgrid is stupid!
X = sin(Z' .* T' * pi);

verifyTrue(testCase, numeric.near(X*5, x5.on()));

% test multiplication with a constant vector
v = [2, 3];
x11 = x * v;
X11On = x11.on();

verifyTrue(testCase, numeric.near(X*v(1), X11On(:,:,:,1)));
verifyTrue(testCase, numeric.near(X*v(2), X11On(:,:,:,2)));

% test multiplication with zero:
x0 = x*0;
testCase.verifyEqual(x0.on(), 0*ndgrid(x0.grid{:}));
end

function testMLRdivide2ConstantSymbolic(testCase)

syms z
assume(z>0 & z<1);
Lambda = quantity.Symbolic(eye(2), 'variable', z, ...
	'grid', linspace(0, 1), 'gridName', 'z', 'name', 'Lambda');
A = quantity.Symbolic(eye(2), 'variable', z, ...
	'grid', linspace(0, 1), 'gridName', 'z', 'name', 'A');
C1 = A / Lambda;
C2 = A \ Lambda;
testCase.verifyEqual(C1.on(), A.on());
testCase.verifyEqual(C2.on(), A.on());

end
function testGridNamesNotAllowed(testCase)

try
	quantity.Symbolic(sym('z'), 'gridName',  {'y', 't', 'asdf'}, 'grid', {linspace(0, 1, 7), linspace(0, 1, 5), linspace(0, 1, 3)});
	
	% the initialization above has to throw an exception. If the code is
	% here, the test is not valid.
	verifyTrue(testCase, false);
catch ex
	verifyTrue(testCase, isa(ex, 'MException'));
end

end

function testCastToQuantity(testCase)

syms z t
x = quantity.Symbolic(sin(z * t * pi), 'grid', {linspace(0, 1).', linspace(0, 2, 200)});

X1 = quantity.Discrete(x);

%%
verifyEqual(testCase, X1.on, x.on);

end

% function testmTimesFunctionHandleSymbolic(testCase)
%
% TODO repair this kind of multiplication!
%
% %
% f = quantity.Function(@(z) z, 'name', 'f');
% s = quantity.Symbolic(sym('z'), 'name', 'f');
% verifyTrue(testCase, isa(f*s, 'quantity.Function'));
%
% end


function testDiff(testCase)
%%
z = sym('z', 'real');
f1 = sinh(z * pi);
f2 = cosh(z * pi);

f = quantity.Symbolic([f1 ; f2 ], 'name', 'f');

d2f = f.diff(2);
d1f = f.diff(1);

%%

F1 = symfun([f1; f2], z);
D1F = symfun([diff(f1,1); diff(f2,1)], z);
D2F = symfun([diff(f1,2); diff(f2,2)], z);

z = f(1).grid{:};

R1 = F1(z);
R2 = D1F(z);
R3 = D2F(z);

%%
verifyTrue(testCase, numeric.near(double([R1{:}]), f.on(), 1e-12));
verifyTrue(testCase, numeric.near(double([R2{:}]), f.diff(1).on(), 1e-12));
verifyTrue(testCase, numeric.near(double([R3{:}]), f.diff(2).on(), 1e-12));

end


function testInit(testCase)

syms z
A = [0 1; 1 0];

%% compute comparative solution
P1 = expm(A * z);

end

function testPlus(testCase)
syms z zeta

%% 1 + 2 variables
fSymVec3 = quantity.Symbolic([sinh(pi * z)], 'name', 'sinh');
fSymVec4 = quantity.Symbolic([cosh(zeta * pi * z)], 'name', 'cosh');

result = fSymVec3 - fSymVec4;
resultDiscrete = quantity.Discrete(fSymVec3) - quantity.Discrete(fSymVec4);

%%
testCase.verifyEqual(result.on(), resultDiscrete.on(), 'AbsTol', 10*eps);

%% quantity + constant
myQuan = quantity.Symbolic([sinh(pi * z), cosh(zeta * pi * z); 1, z^2], 'name', 'myQuan');
myQuanPlus1 = myQuan + ones(size(myQuan));
my1PlusQuan =  ones(size(myQuan)) + myQuan;

%%
testCase.verifyEqual(myQuan.on()+1, myQuanPlus1.on(), 'AbsTol', 10*eps);
testCase.verifyEqual(myQuan.on()+1, my1PlusQuan.on(), 'AbsTol', 10*eps);


%% 1 variable
fSymVec = quantity.Symbolic([sinh(z * pi) ; cosh(z * pi)], 'name', 'sinhcosh');

F = fSymVec + fSymVec;
val = F.on();

f1fun = @(z) sinh(z * pi);
f2fun = @(z) cosh(z * pi) + 0*z;

zGrid = fSymVec(1).grid{:};

%%
verifyTrue(testCase, numeric.near(2 * f1fun(zGrid) , val(:, 1), 1e-12));
verifyTrue(testCase, numeric.near(2 * f2fun(zGrid) , val(:, 2), 1e-12));

%% 2 variable
syms zeta
f1fun = @(z, zeta) sinh(z * pi);
f2fun = @(z, zeta) cosh(zeta * pi) + 0*z;

fSymVec = quantity.Symbolic([sinh(z * pi); cosh(zeta * pi)], 'name', 'sinhcosh');
fSymVec2 = [fSymVec(2); fSymVec(1)];

F = fSymVec + fSymVec;
F1Minus2 = fSymVec - fSymVec2;

[zGrid, zetaGrid] = ndgrid(fSymVec(1).grid{:});

%%
verifyTrue(testCase, numeric.near(2 * f1fun(zGrid, zetaGrid) , F(1).on(), 1e-12));
verifyTrue(testCase, numeric.near(2 * f2fun(zGrid, zetaGrid) , F(2).on(), 1e-12));
verifyTrue(testCase, numeric.near(f1fun(zGrid, zetaGrid) - f2fun(zGrid, zetaGrid), ...
	F1Minus2(1).on(), 1e-12));
verifyTrue(testCase, numeric.near(-F1Minus2(2).on(), ...
	F1Minus2(1).on(), 1e-12));
end

function testMTimes(testCase)
%%
syms z
f1 = sinh(z * pi);
f2 = cosh(z * pi);

f = quantity.Symbolic([f1 ; f2 ], 'name', 'sinhcosh');

F = f * f.';
val = F.on();

f1 = matlabFunction(f1);
f2 = matlabFunction(f2);


z = f(1).grid{:};

%%
verifyTrue(testCase, numeric.near(f1(z).^2, val(:, 1, 1), 1e-12));
verifyTrue(testCase, numeric.near(f2(z).^2, val(:, 2, 2), 1e-12));
verifyTrue(testCase, numeric.near(f1(z) .* f2(z) , val(:, 1, 2), 1e-12));


%% test multiplication with a scalar
ff = f * 2;
verifyTrue(testCase, numeric.near(ff.on(), [f1(z), f2(z)] * 2, 1e-12));


end

function testVariable(testCase)
syms z
q = quantity.Symbolic(z);

verifyEqual(testCase, q.variable, z);

end

function testUMinus(testCase)
%%
syms z;
f1 = (sin(z .* pi) );
f2 =  z;
f3 = (cos(z .* pi) );
zz = linspace(0, 1, 42).';
f = quantity.Symbolic([f1, f2; 0, f3], ...
	'grid', {zz});

mf = -f;

%%
verifyEqual(testCase, [f.valueDiscrete], -[mf.valueDiscrete]);
end

function testConstantValues(testCase)
%%
syms z
q = quantity.Symbolic(magic(7), 'grid', {linspace(0, 1)'}, 'variable', z);

%%
magic7 = magic(7);
magic700 = zeros(100, 7, 7);
for k = 1:q.gridSize()
	magic700(k, :,:) = magic7;
end

%%
testCase.verifyEqual(magic700, q.on());
testCase.verifyTrue(q.isConstant());

p = quantity.Symbolic([0 0 0 0 z], 'grid', linspace(0,1)', 'variable', z);
testCase.verifyFalse(p.isConstant());

end

function testIsConstant(testCase)
%%
syms z
Q_constant = quantity.Symbolic(1);
Q_var = quantity.Symbolic(z);

%%
verifyTrue(testCase, Q_constant.isConstant);
verifyFalse(testCase, Q_var.isConstant);
end

function testEqual(testCase)

%%
Q = quantity.Symbolic(0, 'name', 'P');
P = quantity.Symbolic(0, 'name', 'Q');

syms Z;
f1 = (sin(Z .* pi) );
f2 =  Z;
f3 = (cos(Z .* pi) );
z = linspace(0, 1, 42).';
R1 = quantity.Symbolic([f1, f2; 0, f3], ...
	'grid', {z});

R2 = quantity.Symbolic([f2, f1; 0, f3], ...
	'grid', {z});

R3 = quantity.Symbolic([f1, f2; 0, f3], ...
	'grid', {z});

%%
verifyTrue(testCase, Q == P);
verifyTrue(testCase, Q == Q);
verifyTrue(testCase, R1 == R1);
verifyFalse(testCase, R1 == R2);
verifyTrue(testCase, R1 == R3);
end

function testEvaluateConstant(testCase)
%%
syms z zeta
Q = quantity.Symbolic(magic(7), 'grid', {1}, 'variable', z);
Q2D = quantity.Symbolic(magic(7), 'grid', {1, 1}, 'variable', {z, zeta});

%%
verifyEqual(testCase, shiftdim(Q.on(), 1), magic(7))
verifyEqual(testCase, shiftdim(Q2D.on(), 2), magic(7))
end

function testSize(testCase)
Q = quantity.Symbolic(ones(1,2,3,4));
verifyEqual(testCase, size(Q), [1, 2, 3, 4]);
end

function testEvaluate(testCase)
%%
syms Z;
f1 = (sin(Z .* pi) );
f2 =  Z;
f3 = (cos(Z .* pi) );
z = linspace(0, 1, 42).';
f = quantity.Symbolic([f1, f2; 0, f3], ...
	'grid', {z});

value = f.on();

%%
verifyTrue(testCase, numeric.near(value(:,1,1), double(subs(f1, Z, z))));
verifyTrue(testCase, numeric.near(value(:,1,2), double(subs(f2, Z, z))));
verifyTrue(testCase, numeric.near(value(:,2,1), zeros(size(z))));
verifyTrue(testCase, numeric.near(value(:,2,2), double(subs(f3, Z, z))));
end

function testSolveAlgebraic(testCase)
syms x
assume(x>0 & x<1);
% scalar
fScalar = quantity.Symbolic([(1+x)^2]);
solutionScalar = fScalar.solveAlgebraic(2, fScalar.gridName{1});
testCase.verifyEqual(solutionScalar, sqrt(2)-1, 'AbsTol', 1e-12);

% array
f = quantity.Symbolic([2*x, 1]);
solution = f.solveAlgebraic([1, 1], f(1).gridName{1});
testCase.verifyEqual(solution, 0.5);
end

function testSubs(testCase)
%%
% init
syms x y z bla
f = quantity.Symbolic([x, y^2; y+x, 1]);

% subs on variable
Ff1 = [1, 1^2; 1+1, 1];
Ffz = quantity.Symbolic([z, z^2; z+z, 1]);
Fyx = quantity.Symbolic([bla, x^2; x+bla, 1]);

fOf1 = f.subs({'x', 'y'}, {1, 1});
fOfz = f.subs({'x', 'y'}, {'z', 'z'});
fOfyx = f.subs({'x', 'y'}, {'bla', 'x'});

testCase.verifyEqual(Ff1, fOf1);
testCase.verifyEqual(Ffz.sym(), fOfz.sym());
testCase.verifyEqual(Fyx.sym(), fOfyx.sym());

%%
syms z zeta
assume(z>0 & z<1); assume(zeta>0 & zeta<1);
F = quantity.Symbolic(zeros(1), 'grid', {linspace(0,1), linspace(0,1)}, 'variable', {z, zeta});
Feta = F.subs('z', 'eta');
testCase.verifyEqual(Feta(1).gridName, {'eta', 'zeta'});

%%
fMessy = quantity.Symbolic(x^1*y^2*z^4*bla^5, 'variables', {x, y, z, bla}, ...
	'grid', {linspace(0, 1, 5), linspace(0, 1, 11), linspace(0, 1, 15), linspace(0, 1, 21)});
fMessyA = fMessy.subs({'x', 'y', 'z', 'bla'}, {'a', 'a', 'a', 'a'});
fMessyYY = fMessy.subs({'bla', 'x', 'z'}, {'bli', 'y', 'y'});

testCase.verifyEqual(fMessyA.gridName, {'a'});
testCase.verifyEqual(fMessyA.grid, {linspace(0, 1, 21)});
testCase.verifyEqual(fMessyYY.gridName, {'y', 'bli'});
testCase.verifyEqual(fMessyYY.grid, {linspace(0, 1, 15), linspace(0, 1, 21)});
% % sub multiple numerics -> not implemented yet
% f11 = f.subs('x', [1; 2]);
% f1 = f.subs('x', 1);
% f2 = f.subs('x', 2);
% testCase.verifyEqual(reshape(f11(1,:,:).sym(), size(f)), fOf1);
% testCase.verifyEqual(reshape(f11(2,:,:).sym(), size(f)), f2.sym());
%
% % sub multiple multiple variables
% f1232 = f.subs({'x', 'y'}, {[1; 2], [3; 2]});
% f13 = f.subs({'x', 'y'}, {1, 3});
% f22 = f.subs({'x', 'y'}, {2, 2});
% testCase.verifyEqual(reshape(f1232(1,:,:), size(f)), f22);
% testCase.verifyEqual(reshape(f1232(2,:,:), size(f)), f13);
end
