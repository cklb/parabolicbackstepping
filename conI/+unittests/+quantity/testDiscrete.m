function [tests] = testDiscrete()
%testQuantity Summary of this function goes here
%   Detailed explanation goes here
tests = functiontests(localfunctions);
end

function testCastDiscrete2Function(testCase)

z = linspace(0, 2*pi)';
d = quantity.Discrete({sin(z); cos(z)}, 'grid', z, 'gridName', 'z');
f = quantity.Function(d);

testCase.verifyTrue(all(size(f) == size(d)));

end

function testQuadraticNorm(tc)
blub = quantity.Discrete(ones(11, 4), 'grid', linspace(0, 1, 11), ...
	'gridName', 'z', 'name', 'b');
blubQuadraticNorm = blub.quadraticNorm();
blubQuadraticWeightedNorm = blub.quadraticNorm('weight', 4*eye(4));

tc.verifyEqual(blubQuadraticNorm.on(), 2*ones(11,1));
tc.verifyEqual(blubQuadraticWeightedNorm.on(), 4*ones(11,1));
end % testQuadraticNorm()

function testBlkdiag(tc)
% init some data
syms z zeta
t = linspace(0, 1, 25);
A = quantity.Symbolic(...
	[1+z*zeta, -zeta; -z, z^2], 'grid', {linspace(0, 1, 21), linspace(0, 1, 41)},...
	'variable', {z, zeta}, 'name', 'q');
B = 2*A(:, 1);
C = 3*A(1);

% 1 input
tc.verifyEqual(A.on, A.blkdiag.on());

% 2 x same input
AA = blkdiag(A, A);
tc.verifyEqual(AA(1:2, 1:2).on(), A.on());
tc.verifyEqual(AA(3:4, 3:4).on(), A.on());
tc.verifyEqual(AA(1:2, 3:4).on(), 0*A.on());
tc.verifyEqual(AA(3:4, 1:2).on(), 0*A.on());

% 3 different sized quantites
ABCB = blkdiag(A, B, C, B);
tc.verifyEqual(ABCB(1:2, 1:2).on(), A.on());
tc.verifyEqual(ABCB(3:4, 3).on(), B.on());
tc.verifyEqual(ABCB(5, 4).on(), C.on());
tc.verifyEqual(ABCB(6:7, 5).on(), B.on());

zeroElements = ~logical(blkdiag(true(size(A)), true(size(B)), true(size(C)), true(size(B))));
tc.verifyEqual(ABCB(zeroElements(:)).on(), 0*ABCB(zeroElements(:)).on());

end % testBlkdiag()

function testSetName(tc)
blub = quantity.Discrete(ones(11, 2), 'grid', linspace(0, 1, 11), 'gridName', 'z');
blub.setName('asdf');
tc.verifyEqual(blub(1).name, 'asdf');
tc.verifyEqual(blub(2).name, 'asdf');
end

function testCtranspose(tc)
syms z zeta
qSymbolic = quantity.Symbolic(...
	[1+z*zeta, -zeta; -z, z^2], 'grid', {linspace(0, 1, 21), linspace(0, 1, 41)},...
	'variable', {z, zeta}, 'name', 'q');
qDiscrete = quantity.Discrete(qSymbolic);
qDiscreteCtransp = qDiscrete';
tc.verifyEqual(qDiscrete(1,1).on(), qDiscreteCtransp(1,1).on());
tc.verifyEqual(qDiscrete(2,2).on(), qDiscreteCtransp(2,2).on());
tc.verifyEqual(qDiscrete(1,2).on(), qDiscreteCtransp(2,1).on());
tc.verifyEqual(qDiscrete(2,1).on(), qDiscreteCtransp(1,2).on());

qDiscrete2 = quantity.Discrete(ones(11, 1) + 2i, ...
	'grid', linspace(0, 1, 11), 'gridName', 'z', 'name', 'im');
qDiscrete2Ctransp = qDiscrete2';
tc.verifyEqual(qDiscrete2.real.on(), qDiscrete2Ctransp.real.on());
tc.verifyEqual(qDiscrete2.imag.on(), -qDiscrete2Ctransp.imag.on());
end % testCtranspose

function testTranspose(tc)
syms z zeta
qSymbolic = quantity.Symbolic(...
	[1+z*zeta, -zeta; -z, z^2], 'grid', {linspace(0, 1, 21), linspace(0, 1, 41)},...
	'variable', {z, zeta}, 'name', 'q');
qDiscrete = quantity.Discrete(qSymbolic);
qDiscreteTransp = qDiscrete.';
tc.verifyEqual(qDiscrete(1,1).on(), qDiscreteTransp(1,1).on());
tc.verifyEqual(qDiscrete(2,2).on(), qDiscreteTransp(2,2).on());
tc.verifyEqual(qDiscrete(1,2).on(), qDiscreteTransp(2,1).on());
tc.verifyEqual(qDiscrete(2,1).on(), qDiscreteTransp(1,2).on());
end % testTranspose

function testFlipGrid(tc)
syms z zeta
myGrid = linspace(0, 1, 11);
f = quantity.Discrete(quantity.Symbolic([1+z+zeta; 2*zeta+sin(z)] + zeros(2, 1)*z*zeta, ...
	'variable', {z, zeta}, 'grid', {myGrid, myGrid}));
% % flip one grid
fReference = quantity.Discrete(quantity.Symbolic([1+z+(1-zeta); 2*(1-zeta)+sin(z)] + zeros(2, 1)*z*zeta, ...
	'variable', {z, zeta}, 'grid', {myGrid, myGrid}));
fFlipped = f.flipGrid('zeta');
tc.verifyEqual(fReference.on(), fFlipped.on(), 'AbsTol', 10*eps);

% flip both grids
fReference2 = quantity.Discrete(quantity.Symbolic([1+(1-z)+(1-zeta); 2*(1-zeta)+sin(1-z)] + zeros(2, 1)*z*zeta, ...
	'variable', {z, zeta}, 'grid', {myGrid, myGrid}));
fFlipped2 = f.flipGrid({'z', 'zeta'});
fFlipped3 = f.flipGrid({'zeta', 'z'});
tc.verifyEqual(fReference2.on(), fFlipped2.on(), 'AbsTol', 10*eps);
tc.verifyEqual(fReference2.on(), fFlipped3.on(), 'AbsTol', 10*eps);
end % testFlipGrid();

function testScalarPlusMinusQuantity(testCase)
syms z
myGrid = linspace(0, 1, 7);
f = quantity.Discrete(quantity.Symbolic([1; 2] + zeros(2, 1)*z, ...
	'variable', {z}, 'grid', {myGrid}));
testCase.verifyError(@() 1-f-1, '');
testCase.verifyError(@() 1+f+1, '');
end % testScalarPlusMinusQuantity

function testNumericVectorPlusMinusQuantity(testCase)
syms z
myGrid = linspace(0, 1, 7);
f = quantity.Discrete(quantity.Symbolic([1+z; 2+sin(z)] + zeros(2, 1)*z, ...
	'variable', {z}, 'grid', {myGrid}));
a = ones(size(f));
testCase.verifyEqual(on(a-f), 1-f.on());
testCase.verifyEqual(on(f-a), f.on()-1);
testCase.verifyEqual(on(a+f), f.on()+1);
testCase.verifyEqual(on(f+a), f.on()+1);
end % testNumericVectorPlusMinusQuantity

function testUnitaryPluasAndMinus(testCase)
syms z zeta
qSymbolic = quantity.Symbolic(...
	[1+z*zeta, -zeta; -z, z^2], 'grid', {linspace(0, 1, 21), linspace(0, 1, 41)},...
	'variable', {z, zeta}, 'name', 'q');
qDiscrete = quantity.Discrete(qSymbolic);
qDoubleArray = qSymbolic.on();

testCase.verifyEqual(on(-qSymbolic), -qDoubleArray);
testCase.verifyEqual(on(-qDiscrete), -qDoubleArray);
testCase.verifyEqual(on(+qSymbolic), +qDoubleArray);
testCase.verifyEqual(on(+qDiscrete), +qDoubleArray);
testCase.verifyEqual(on(+qDiscrete), on(+qSymbolic));
testCase.verifyEqual(on(-qDiscrete), on(-qSymbolic));

end

function testConcatenate(testCase)

t = linspace(0, pi, 7)';
A = quantity.Discrete({sin(t); cos(t)}, 'grid', {t}, 'gridName', 't');
B = quantity.Discrete({tan(t); exp(t)}, 'grid', {t}, 'gridName', 't');

AB = [A, B];
AB_ = [A', B'];
ABA = [A, B, A];

testCase.verifyTrue(all(size(AB) == [2, 2]));
testCase.verifyTrue(all(size(AB_) == [1, 4]));
testCase.verifyTrue(all(size(ABA) == [2, 3]));

t = linspace(0, pi, 13)';
C = quantity.Discrete({sin(t); cos(t)}, 'grid', {t}, 'gridName', 't');
AC = [A; C];

testCase.verifyTrue(all(size(AC) == [4, 1]));

A0s = [A, zeros(2,3)];
testCase.verifyTrue(all(all(all(A0s(:, 2:end).on() == 0))))

O = quantity.Discrete.empty([5, 0]);
O_horz = [O, O];
O_vert = [O; O];

testCase.verifyEqual(size(O_horz, 1), 5);
testCase.verifyEqual(size(O_vert, 1), 10);

z = linspace(0, 1, 5);
D = quantity.Discrete({sin(t * z); cos(t * z)}, 'grid', {t, z}, 'gridName', {'t', 'z'});
E = quantity.Discrete({tan(z' * t'); cos(z' * t')}, 'grid', {z, t}, 'gridName', {'z', 't'});

DE = [D, E];
compareDE = quantity.Discrete({sin(t * z), tan(t*z); cos(t * z), cos(t*z)}, 'grid', {t, z}, 'gridName', {'t', 'z'});

testCase.verifyEqual(DE.on(), compareDE.on())

ED = [E, D];
compareED = quantity.Discrete({tan(t*z), sin(t * z); cos(t * z), cos(t*z)}, 'grid', {t, z}, 'gridName', {'t', 'z'});
testCase.verifyEqual(ED.on(), compareED.on())
end

function testExp(testCase)
% 1 spatial variable
syms z zeta
s1d = quantity.Discrete(quantity.Symbolic(...
	1+z*z, 'grid', {linspace(0, 1, 21)}, 'variable', {z}, 'name', 's1d'));
testCase.verifyEqual(s1d.exp.on(), exp(s1d.on()));

% diagonal matrix
s2dDiag = quantity.Discrete(quantity.Symbolic(...
	[1+z*zeta, 0; 0, z^2], 'grid', {linspace(0, 1, 21), linspace(0, 1, 41)},...
	'variable', {z, zeta}, 'name', 's2dDiag'));
testCase.verifyEqual(s2dDiag.exp.on(), exp(s2dDiag.on()));
end

function testExpm(testCase)
syms z zeta
mat2d = quantity.Discrete(quantity.Symbolic(...
	ones(2, 2) + [1+z*zeta, 3*zeta; 2+5*z+zeta, z^2], 'grid', {linspace(0, 1, 21), linspace(0, 1, 41)},...
	'variable', {z, zeta}, 'name', 's2d'));
mat2dMat = mat2d.on();
mat2dExpm = 0 * mat2d.on();
for zIdx = 1 : 21
	for zetaIdx = 1 : 41
		mat2dExpm(zIdx, zetaIdx, :, :) = expm(reshape(mat2dMat(zIdx, zetaIdx, :, :), [2, 2]));
	end
end
testCase.verifyEqual(mat2d.expm.on(), mat2dExpm, 'RelTol', 100*eps);

end

function testSqrt(testCase)
% 1 spatial variable
quanScalar1d = quantity.Discrete((linspace(0,2).^2).', 'size', [1, 1], 'grid', linspace(0, 1), ...
	'gridName', 'z', 'name', 's1d');
quanScalarRoot = quanScalar1d.sqrt();
testCase.verifyEqual(quanScalarRoot.on(), linspace(0, 2).');

% 2 spatial variables
quanScalar2d = quantity.Discrete((linspace(0, 2, 21).' + linspace(0, 2, 41)).^2, 'size', [1, 1], ...
	'grid', {linspace(0, 1, 21), linspace(0, 1, 41)}, ...
	'gridName', {'z', 'zeta'}, 'name', 's2d');
quanScalar2dRoot = quanScalar2d.sqrt();
testCase.verifyEqual(quanScalar2dRoot.on(), (linspace(0, 2, 21).' + linspace(0, 2, 41)));

% diagonal matrix
quanDiag = [quanScalar1d, 0*quanScalar1d; 0*quanScalar1d, (2)*quanScalar1d];
quanDiagRoot = quanDiag.sqrt();
testCase.verifyEqual(quanDiagRoot(1,1).on(), quanScalarRoot.on());
testCase.verifyEqual(quanDiagRoot(1,2).on(), 0*quanDiagRoot(1,2).on());
testCase.verifyEqual(quanDiagRoot(2,1).on(), 0*quanDiagRoot(2,1).on());
testCase.verifyEqual(quanDiagRoot(2,2).on(), sqrt(2)*quanDiagRoot(1,1).on(), 'AbsTol', 10*eps);

%testCase.verifyEqual(1, 0);
end

function testSqrtm(testCase)
quanScalar1d = quantity.Discrete((linspace(0,2).^2).', 'size', [1, 1], 'grid', linspace(0, 1), ...
	'gridName', 'z', 'name', 's1d');
quanScalar2d = quantity.Discrete((linspace(0, 2, 21).' + linspace(0, 2, 41)).^2, 'size', [1, 1], ...
	'grid', {linspace(0, 1, 21), linspace(0, 1, 41)}, ...
	'gridName', {'z', 'zeta'}, 'name', 's2d');
% diagonal matrix - 1 spatial variable
quanDiag = [quanScalar1d, 0*quanScalar1d; 0*quanScalar1d, (2)*quanScalar1d];
quanDiagRoot = quanDiag.sqrt(); % only works for diagonal matrices
quanDiagRootMatrix = quanDiag.sqrtm();
testCase.verifyEqual(quanDiagRootMatrix.on(), quanDiagRoot.on());

% full matrix - 1 spatial variable
quanMat1d = [quanScalar1d, 4*quanScalar1d; quanScalar1d, (2)*quanScalar1d];
quanMat1dMat = quanMat1d.on();
quanMat1dReference = 0*quanMat1dMat;
for zIdx = 1:size(quanMat1dReference, 1)
	quanMat1dReference(zIdx, :, :) = sqrt(reshape(quanMat1dMat(zIdx, :), [2, 2]));
end
testCase.verifyEqual(quanMat1d.sqrtm.on(), quanMat1dReference, 'AbsTol', 10*eps);

% full matrix - 2 spatial variables
quanMat2d = [quanScalar2d, 4*quanScalar2d; quanScalar2d, (2)*quanScalar2d];
quanMat2dMat = quanMat2d.on();
quanMat2dReference = 0*quanMat2dMat;
for zIdx = 1:size(quanMat2dMat, 1)
	for zetaIdx = 1:size(quanMat2dMat, 2)
		quanMat2dReference(zIdx, zetaIdx, :, :) = ...
			sqrt(reshape(quanMat2dMat(zIdx, zetaIdx, :), [2, 2]));
	end
end
testCase.verifyEqual(quanMat2d.sqrtm.on(), quanMat2dReference, 'AbsTol', 10*eps);

end

function testDiag2Vec(testCase)
% quantity.Symbolic
syms z
myMatrixSymbolic = quantity.Symbolic([sin(0.5*z*pi)+1, 0; 0, 0.9-z/2]);
myVectorSymbolic = diag2vec(myMatrixSymbolic);
testCase.verifyEqual(myMatrixSymbolic(1,1).valueContinuous, myVectorSymbolic(1,1).valueContinuous);
testCase.verifyEqual(myMatrixSymbolic(2,2).valueContinuous, myVectorSymbolic(2,1).valueContinuous);
testCase.verifyEqual(numel(myVectorSymbolic), size(myMatrixSymbolic, 1));

% quantity.Discrete
myMatrixDiscrete = quantity.Discrete(myMatrixSymbolic);
myVectorDiscrete = diag2vec(myMatrixDiscrete);
testCase.verifyEqual(myMatrixDiscrete(1,1).on(), myVectorDiscrete(1,1).on());
testCase.verifyEqual(myMatrixDiscrete(2,2).on(), myVectorDiscrete(2,1).on());
testCase.verifyEqual(numel(myVectorDiscrete), size(myMatrixDiscrete, 1));
end
function testVec2Diag(testCase)
% quantity.Discrete
n = 7;
z = linspace(0,1,n)';
myMatrixDiscrete = quantity.Discrete(...
	{sin(0.5*z*pi)+1, zeros(n,1); zeros(n,1), 0.9-z/2}, ...
	'grid', z, ...
	'gridName', 'z');
myVectorDiscrete = quantity.Discrete(...
	{sin(0.5*z*pi)+1; 0.9-z/2}, ...
	'grid', z, ...
	'gridName', 'z');

testCase.verifyTrue( myVectorDiscrete.vec2diag.near(myMatrixDiscrete) );
end

function testInvert(testCase)
myGrid = linspace(0, 1, 21);
% scalar
fScalar = quantity.Discrete((myGrid.').^2, 'grid', myGrid, 'gridName', 'x');
fScalarInverse = fScalar.invert(fScalar.gridName{1});
testCase.verifyEqual(fScalarInverse.on(fScalar.on()), myGrid.'),
end

function testSolveAlgebraic(testCase)
myGrid = linspace(0, 1, 21);
% scalar
fScalar = quantity.Discrete((1+myGrid.').^2, 'grid', myGrid, 'gridName', 'x', ...
	'size', [1, 1]);
solutionScalar = fScalar.solveAlgebraic(2, fScalar.gridName{1});
testCase.verifyEqual(solutionScalar, sqrt(2)-1, 'AbsTol', 1e-3);

% array
% fArray = quantity.Discrete([2*myGrid.', myGrid.' + ones(numel(myGrid), 1)], ...
% 	'grid', myGrid', 'gridName', 'x', 'size', [2, 1]);
% solution = fArray.solveAlgebraic([1; 1], fArray(1).gridName{1});
% testCase.verifyEqual(solution, 0.5);
end

function testSolveDVariableEqualQuantityConstant(testCase)
%% simple constant case
quan = quantity.Discrete(ones(51, 1), 'grid', linspace(0, 1, 51), ...
	'gridName', 'z', 'size', [1, 1], 'name', 'a');
solution = quan.solveDVariableEqualQuantity();
[referenceResult1, referenceResult2] = ndgrid(linspace(0, 1, 51), linspace(0, 1, 51));
testCase.verifyEqual(solution.on(), referenceResult1 + referenceResult2, 'AbsTol', 10*eps);
end

function testSolveDVariableEqualQuantityNegative(testCase)
syms z zeta
assume(z>0 & z<1); assume(zeta>0 & zeta<1);
myParameterGrid = linspace(0, 1, 51);
Lambda = quantity.Symbolic(-0.1-z^2, ...%, -1.2+z^2]),...1+z*sin(z)
	'variable', z, ...
	'grid', myParameterGrid, 'gridName', 'z', 'name', '\Lambda');

%%
myGrid = linspace(-2, 2, 101);
sEval = -0.9;
solveThisOdeDiscrete = solveDVariableEqualQuantity(...
	diag2vec(quantity.Discrete(Lambda)), 'variableGrid', myGrid);
solveThisOdeSymbolic = solveDVariableEqualQuantity(...
	diag2vec(Lambda), 'variableGrid', myGrid);

testCase.verifyEqual(solveThisOdeSymbolic.on({sEval, 0.5}), solveThisOdeDiscrete.on({sEval, 0.5}), 'RelTol', 5e-4);
end

function testSolveDVariableEqualQuantityComparedToSym(testCase)
%% compare with symbolic implementation
syms z
assume(z>0 & z<1);
quanBSym = quantity.Symbolic([1+z], 'grid', {linspace(0, 1, 21)}, ...
	'gridName', 'z', 'name', 'bSym', 'variable', {z});
quanBDiscrete = quantity.Discrete(quanBSym.on(), 'grid', {linspace(0, 1, 21)}, ...
	'gridName', 'z', 'name', 'bDiscrete', 'size', size(quanBSym));
solutionBSym = quanBSym.solveDVariableEqualQuantity();
solutionBDiscrete = quanBDiscrete.solveDVariableEqualQuantity();
%solutionBSym.plot(); solutionBDiscrete.plot();
testCase.verifyEqual(solutionBDiscrete.on(), solutionBSym.on(), 'RelTol', 1e-6);
end

function testSolveDVariableEqualQuantityAbsolut(testCase)
%% compare with symbolic implementation
syms z
assume(z>0 & z<1);
quanBSym = quantity.Symbolic([1+z], 'grid', {linspace(0, 1, 51)}, ...
	'gridName', 'z', 'name', 'bSym', 'variable', {z});
quanBDiscrete = quantity.Discrete(quanBSym.on(), 'grid', {linspace(0, 1, 51)}, ...
	'gridName', 'z', 'name', 'bDiscrete', 'size', size(quanBSym));
solutionBDiscrete = quanBDiscrete.solveDVariableEqualQuantity();
myGrid = solutionBDiscrete.grid{1};
solutionBDiscreteDiff = solutionBDiscrete.diff(1, 's');
quanOfSolutionOfS = zeros(length(myGrid), length(myGrid), 1);
for icIdx = 1 : length(myGrid)
	quanOfSolutionOfS(:, icIdx, :) = quanBSym.on(solutionBDiscrete.on({myGrid, myGrid(icIdx)}));
end
%%
testCase.verifyEqual(solutionBDiscreteDiff.on({myGrid(2:end-1), myGrid}), quanOfSolutionOfS(2:end-1, :), 'AbsTol', 1e-3);

end
function testMtimesDifferentGridLength(testCase)
%%
a = quantity.Discrete(ones(11, 1), 'grid', linspace(0, 1, 11), 'gridName', 'z', ...
	'size', [1, 1], 'name', 'a');
b = quantity.Discrete(ones(5, 1), 'grid', linspace(0, 1, 5), 'gridName', 'z', ...
	'size', [1, 1], 'name', 'b');
ab  = a*b;
%
syms z
c = quantity.Symbolic(1, 'grid', linspace(0, 1, 21), 'variable', z, 'name', 'c');
ac = a*c;

%%
%%
testCase.verifyEqual(ab.on(), ones(11, 1));
testCase.verifyEqual(ac.on(), ones(21, 1));
end

function testDiff1d(testCase)
%% 1d
constant = quantity.Discrete([2*ones(11, 1), linspace(0, 1, 11).'], 'grid', linspace(0, 1, 11), ...
	'gridName', 'z', 'name', 'constant', 'size', [2, 1]);

constantDiff = diff(constant);
testCase.verifyEqual(constantDiff.on(), [zeros(11, 1), ones(11, 1)], 'AbsTol', 10*eps);

z = linspace(0,pi)';
sinfun = quantity.Discrete(sin(z), 'grid', z, 'gridName', 'z');

% do the comparison on a smaller grid, because the numerical derivative is
% very bad a the boundarys of the domain.
Z = linspace(0.1, pi-0.1)';
testCase.verifyTrue(numeric.near(sinfun.diff().on(Z), cos(Z), 1e-3));
testCase.verifyTrue(numeric.near(sinfun.diff(2).on(Z), -sin(Z), 1e-3));
testCase.verifyTrue(numeric.near(sinfun.diff(3).on(Z), -cos(Z), 1e-3));



end

function testDiffConstant2d(testCase)
%% 2d
[zNdgrid, zetaNdgrid] = ndgrid(linspace(0, 1, 11), linspace(0, 1, 21));
myQuantity = quantity.Discrete(cat(3, 2*ones(11, 21), zNdgrid, zetaNdgrid), ...
	'grid', {linspace(0, 1, 11), linspace(0, 1, 21)}, ...
	'gridName', {'z', 'zeta'}, 'name', 'constant', 'size', [3, 1]);
myQuantityDz = diff(myQuantity, 1, 'z');
myQuantityDzeta = diff(myQuantity, 1, 'zeta');
myQuantityDZzeta = diff(myQuantity, 1);
myQuantityDZzeta2 = diff(myQuantity, 1, {'z', 'zeta'});

testCase.verifyEqual(myQuantityDZzeta.on(), myQuantityDZzeta2.on());

% constant
testCase.verifyEqual(myQuantityDz(1).on(), zeros(11, 21));
testCase.verifyEqual(myQuantityDzeta(1).on(), zeros(11, 21));
testCase.verifyEqual(myQuantityDZzeta(1).on(), zeros(11, 21));

% zNdgrid
testCase.verifyEqual(myQuantityDz(2).on(), ones(11, 21), 'AbsTol', 10*eps);
testCase.verifyEqual(myQuantityDzeta(2).on(), zeros(11, 21), 'AbsTol', 10*eps);
testCase.verifyEqual(myQuantityDZzeta(2).on(), zeros(11, 21), 'AbsTol', 10*eps);

% zetaNdgrid
testCase.verifyEqual(myQuantityDz(3).on(), zeros(11, 21), 'AbsTol', 10*eps);
testCase.verifyEqual(myQuantityDzeta(3).on(), ones(11, 21), 'AbsTol', 10*eps);
testCase.verifyEqual(myQuantityDZzeta(3).on(), zeros(11, 21), 'AbsTol', 10*eps);
end

function testOn(testCase)
%% init data
gridVecA = linspace(0, 1, 27);
gridVecB = linspace(0, 1, 41);
[value] = createTestData(gridVecA, gridVecB);
a = quantity.Discrete(value, 'size', [2, 3], ...
	'grid', {gridVecA, gridVecB}, 'gridName', {'z', 'zeta'}, 'name', 'A');

%%
testCase.verifyEqual(value, a.on());
testCase.verifyEqual(permute(value, [2, 1, 3, 4]), ...
	a.on({gridVecB, gridVecA}, {'zeta', 'z'}));
testCase.verifyEqual(createTestData(linspace(0, 1, 100), linspace(0, 1, 21)), ...
	a.on({linspace(0, 1, 100), linspace(0, 1, 21)}), 'AbsTol', 100*eps);
testCase.verifyEqual(createTestData(linspace(0, 1, 24), linspace(0, 1, 21)), ...
	a.on({linspace(0, 1, 24), linspace(0, 1, 21)}, {'z', 'zeta'}), 'AbsTol', 24*eps);
testCase.verifyEqual(permute(createTestData(linspace(0, 1, 21), linspace(0, 1, 24)), [2, 1, 3, 4]), ...
	a.on({linspace(0, 1, 24), linspace(0, 1, 21)}, {'zeta', 'z'}), 'AbsTol', 2e-4);

	function [value] = createTestData(gridVecA, gridVecB)
		[zGridA , zetaGridA] = ndgrid(gridVecA, gridVecB);
		value = ones(numel(gridVecA), numel(gridVecB), 2, 3);
		value(:,:,1,1) = 1+zGridA;
		value(:,:,2,1) = 1+zetaGridA;
		value(:,:,2,2) = 1+2*zGridA - zetaGridA.^2;
		value(:,:,1,2) = 1+zGridA + zetaGridA.^2;
		value(:,:,2,3) = 1+zeros(numel(gridVecA), numel(gridVecB));
	end
end

function testOn2(testCase)
zeta = linspace(0, 1);
eta = linspace(0, 1, 71)';
z = linspace(0, 1, 51);

[ZETA, ETA, Z] = ndgrid(zeta, eta, z);

a = quantity.Discrete(cat(4, sin(ZETA.*ZETA.*Z)+ETA.*ETA, ETA+cos(ZETA.*ETA.*Z)), ...
	'size', [2 1], 'grid', {zeta, eta, z}, 'gridName', {'zeta', 'eta', 'z'});

testCase.verifyEqual(a.on({zeta, eta, z}), ...
	permute(a.on({eta, zeta, z}, {'eta', 'zeta', 'z'}), [2, 1, 3, 4]));
testCase.verifyEqual([a(1).on({zeta(2), eta(3), z(4)}); a(2).on({zeta(2), eta(3), z(4)})], ...
	[sin(zeta(2)*zeta(2)*z(4))+eta(3)*eta(3); eta(3)+cos(zeta(2)*eta(3)*z(4))]);
end

function testMtimesZZeta2x2(testCase)
gridVecA = linspace(0, 1, 101);
[zGridA , ~] = ndgrid(gridVecA, gridVecA);

a = quantity.Discrete(ones([size(zGridA), 2, 2]), 'size', [2, 2], ...
	'grid', {gridVecA, gridVecA}, 'gridName', {'z', 'zeta'}, 'name', 'A');

syms zeta
assume(zeta>=0 & zeta>=1)
gridVecB = linspace(0, 1, 100);
b = quantity.Symbolic((eye(2, 2)), 'variable', zeta, ...
	'grid', gridVecB);
c = a*b;

%%
testCase.verifyEqual(c.on(), a.on());
end

function testMTimesPointWise(testCase)

syms z zeta
Z = linspace(0, 1, 501)';
ZETA = Z;
P = quantity.Symbolic([z, z^2; z^3, z^4] * zeta, 'grid', {Z, ZETA});
B = quantity.Symbolic([sin(zeta); cos(zeta)], 'grid', ZETA);

PB = P*B;

p = quantity.Discrete(P);
b = quantity.Discrete(B);

pb = p*b;

end

function testMldivide(testCase)
%% scalar example
s = linspace(0, 1, 21);
vL = quantity.Discrete(2*ones(21, 1), ...
	'size', [1, 1], 'grid', {s}, 'gridName', {'asdf'});
vR = quantity.Discrete((linspace(1, 2, 21).').^2, ...
	'size', [1, 1], 'grid', {s}, 'gridName', {'asdf'});
vLdVR = vL \ vR;


%% matrix example
t = linspace(0, pi);
s = linspace(0, 1, 21);
[T, ~] = ndgrid(t, s);

KL = quantity.Discrete(permute(repmat(2*eye(2), [1, 1, size(T)]), [3, 4, 1, 2]), ...
	'size', [2 2], 'grid', {t, s}, 'gridName', {'t', 's'});
KR = quantity.Discrete(permute(repmat(4*eye(2), [1, 1, size(T)]), [3, 4, 1, 2]), ...
	'size', [2 2], 'grid', {t, s}, 'gridName', {'t', 's'});
KLdKR = KL \ KR;

%%
testCase.verifyEqual(vLdVR.on(), 2 .\ (linspace(1, 2, 21).').^2);
testCase.verifyEqual(KLdKR.on(), permute(repmat(2*eye(2), [1, 1, size(T)]), [4, 3, 1, 2]));
end

function testMrdivide(testCase)
%% scalar example
s = linspace(0, 1, 21);
vL = quantity.Discrete(2*ones(21, 1), ...
	'size', [1, 1], 'grid', {s}, 'gridName', {'asdf'});
vR = quantity.Discrete((linspace(1, 2, 21).').^2, ...
	'size', [1, 1], 'grid', {s}, 'gridName', {'asdf'});
vLdVR = vL / vR;


%% matrix example
t = linspace(0, pi);
s = linspace(0, 1, 21);
[T, ~] = ndgrid(t, s);

KL = quantity.Discrete(permute(repmat(2*eye(2), [1, 1, size(T)]), [3, 4, 1, 2]), ...
	'size', [2 2], 'grid', {t, s}, 'gridName', {'t', 's'});
KR = quantity.Discrete(permute(repmat(4*eye(2), [1, 1, size(T)]), [3, 4, 1, 2]), ...
	'size', [2 2], 'grid', {t, s}, 'gridName', {'t', 's'});
KLdKR = KL / KR;

%%
testCase.verifyEqual(vLdVR.on(), 2 ./ (linspace(1, 2, 21).').^2);
testCase.verifyEqual(KLdKR.on(),  permute(repmat(0.5*eye(2), [1, 1, size(T)]), [4, 3, 1, 2]));
end

function testInv(testCase)
%% scalar example
s = linspace(0, 1, 21);
v = quantity.Discrete((linspace(1, 2, 21).').^2, ...
	'size', [1, 1], 'grid', {s}, 'gridName', {'asdf'});
vInv = v.inv();

%% matrix example
t = linspace(0, pi);
s = linspace(0, 1, 21);
[T, ~] = ndgrid(t, s);

K = quantity.Discrete(permute(repmat(2*eye(2), [1, 1, size(T)]), [3, 4, 1, 2]), ...
	'size', [2 2], 'grid', {t, s}, 'gridName', {'t', 's'});
kInv = inv(K);

%%
testCase.verifyEqual(vInv.on(), 1./(linspace(1, 2, 21).').^2)
testCase.verifyEqual(kInv.on(), permute(repmat(0.5*eye(2), [1, 1, size(T)]), [3, 4, 1, 2]))

end

function testCumInt(testCase)

tGrid = linspace(pi, 1.1*pi, 51)';
s = tGrid;
[T, S] = ndgrid(tGrid, tGrid);

syms sy t

a = [ 1, sy; t, 1];
b = [ sy; 2*sy];

A = zeros([size(T), size(a)]);
B = zeros([length(s), size(b)]);

for i = 1:size(a,1)
	for j = 1:size(a,2)
		A(:,:,i,j) = subs(a(i,j), {sy, t}, {S, T});
	end
end

for i = 1:size(b,1)
	B(:,i) = subs(b(i), sy, s);
end

%% int_0_t a(t,s) * b(s) ds
% compute symbolic version of the volterra integral
v = int(a*b, sy, tGrid(1), t);
V = quantity.Symbolic(v, 'grid', tGrid, 'gridName', 't');

% compute the numeric version of the volterra integral
k = quantity.Discrete(A, 'size', size(a), 'grid', {tGrid, s}, 'gridName', {'t', 's'});
x = quantity.Discrete(B, 'size', size(b), 'grid', {s}, 'gridName', 's');

f = cumInt(k * x, 's', s(1), 't');

testCase.verifyEqual(V.on(), f.on(), 'AbsTol', 1e-5);

%% int_s_t a(t,s) * b(s) ds
v = int(a*b, sy, sy, t);
V = quantity.Symbolic(subs(v, {t, sy}, {'t', 's'}), 'grid', {tGrid, s});

f = cumInt(k * x, 's', 's', 't');

testCase.verifyEqual( f.on(f(1).grid, f(1).gridName), V.on(f(1).grid, f(1).gridName), 'AbsTol', 1e-5 );

%% int_s_t a(t,s) * c(t,s) ds
c = [1, sy+1; t+1, 1];
C = zeros([size(T), size(c)]);
for i = 1:numel(c)
	C(:,:,i) = double(subs(c(i), {t sy}, {T S}));
end
y = quantity.Discrete(C, 'size', size(c), 'grid', {tGrid s}, 'gridName', {'t' 's'});

v = int(a*c, sy, sy, t);
V = quantity.Symbolic(subs(v, {t, sy}, {'t', 's'}), 'grid', {tGrid, s});
f = cumInt( k * y, 's', 's', 't');

testCase.verifyEqual( f.on(f(1).grid, f(1).gridName), V.on(f(1).grid, f(1).gridName), 'AbsTol', 1e-5 );

%% testCumIntWithLowerAndUpperBoundSpecified
tGrid = linspace(pi, 1.1*pi, 51)';
s = tGrid;
[T, S] = ndgrid(tGrid, tGrid);
syms sy tt
a = [ 1, sy; t, 1];

A = zeros([size(T), size(a)]);

for i = 1:size(a,1)
	for j = 1:size(a,2)
		A(:,:,i,j) = subs(a(i,j), {sy, t}, {S, T});
	end
end

% compute the numeric version of the volterra integral
k = quantity.Discrete(A, 'size', size(a), 'grid', {tGrid, s}, 'gridName', {'t', 's'});

fCumInt2Bcs = cumInt(k, 's', 'zeta', 't');
fCumInt2Cum = cumInt(k, 's', tGrid(1), 't') ...
	- cumInt(k, 's', tGrid(1), 'zeta');
testCase.verifyEqual(fCumInt2Bcs.on(), fCumInt2Cum.on(), 'AbsTol', 10*eps);

%% testCumInt with one independent variable
tGrid = linspace(0, pi, 51)';

a = [ 1, sin(t); t, 1];

% compute the numeric version of the volterra integral
f = quantity.Symbolic(a, 'grid', {tGrid}, 'variable', {'t'});
f = quantity.Discrete(f);

intK = cumInt(f, 't', 0, 't');

K = quantity.Symbolic( int(a, 0, t), 'grid', {tGrid}, 'variable', {'t'});

testCase.verifyEqual(intK.on(), K.on(), 'AbsTol', 1e-3);


end

function testAtIndex(testCase)

z = linspace(0,1).';
a = sin(z * pi);

A = quantity.Discrete({a}, 'grid', {z}, 'gridName', {'z'});

testCase.verifyEqual(a(end), A.atIndex(end));
testCase.verifyEqual(a(1), A.atIndex(1));
testCase.verifyEqual(a(23), A.atIndex(23));

y = linspace(0,2,51);
b1 = sin(z * pi * y);
b2 = cos(z * pi * y);
B = quantity.Discrete({b1; b2}, 'grid', {z, y}, 'gridName', {'z', 'y'});

B_1_y = B.atIndex(1,1);
b_1_y = [sin(0); cos(0)];
testCase.verifyTrue(numeric.near(B_1_y, b_1_y));

B_z_1 = B.atIndex(':',1);

testCase.verifyTrue(all(B_z_1(:,1,1) == 0));
testCase.verifyTrue(all(B_z_1(:,:,2) == 1));

end

function testGridJoin(testCase)
s = linspace(0, 1);
z = linspace(0, 1, 71)';
t = linspace(0, 1, 51);

[Z, T, S] = ndgrid(z, t, s);

a = quantity.Discrete(cat(4, sin(Z.*Z.*S), cos(Z.*T.*S)), ...
	'size', [2 1], 'grid', {z, t, s}, 'gridName', {'z', 't', 's'});
b = quantity.Discrete(ones(numel(s), numel(t)), ...
	'size', [1 1], 'grid', {s, t}, 'gridName', {'p', 's'});
c = quantity.Discrete(ones(numel(t), 2, 2), ...
	'size', [2 2], 'grid', {t}, 'gridName', {'p'});

[gridJoinedAB, gridNameJoinedAB] = gridJoin(a, b);
[gridJoinedCC, gridNameJoinedCC] = gridJoin(c, c);
[gridJoinedBC, gridNameJoinedBC] = gridJoin(b, c);

testCase.verifyEqual(gridNameJoinedAB, {'p', 's', 't', 'z'});
testCase.verifyEqual(gridJoinedAB, {s, s, t, z});
testCase.verifyEqual(gridNameJoinedCC, {'p'});
testCase.verifyEqual(gridJoinedCC, {t});
testCase.verifyEqual(gridNameJoinedBC, {'p', 's'});
testCase.verifyEqual(gridJoinedBC, {s, t});
end

function testGridIndex(testCase)


z = linspace(0, 2*pi, 71)';
t = linspace(0, 3*pi, 51);
s = linspace(0, 1);

[Z, T, S] = ndgrid(z, t, s);

a = quantity.Discrete(cat(4, sin(Z.*Z.*S), cos(Z.*T.*S)), ...
	'size', [2 1], 'grid', {z, t, s}, 'gridName', {'z', 't', 's'});

idx = a.gridIndex('z');

testCase.verifyEqual(idx, 1);


idx = a.gridIndex({'z', 't'});
testCase.verifyEqual(idx, [1 2]);

idx = a.gridIndex({'z', 's'});
testCase.verifyEqual(idx, [1 3]);

idx = a.gridIndex('t');
testCase.verifyEqual(idx, 2);

end


function testMTimes(testCase)
% multiplication of a(z) * a(z)'
% a(z) \in (3, 2), z \in (100)
z = linspace(0, 2*pi)';
a = cat(3, [z*0, z, sin(z)], [z.^2, z.^3, cos(z)]); % dim = (100, 3, 2)
aa = misc.multArray(a, permute(a, [1, 3, 2]), 3, 2, 1);    % dim = (100, 3, 3)

A = quantity.Discrete(a, 'size', [3, 2], 'grid', z, 'gridName', {'z'});
AA = A*A';

testCase.verifyTrue(numeric.near(aa, AA.on()));

z = linspace(0, 2*pi, 31);
t = linspace(0, 1, 13);
v = linspace(0, 1, 5);
[z, t] = ndgrid(z, t);
[v, t2] = ndgrid(v, t(1,:));

% scalar multiplication of b(z, t) * b(v, t)
b = z .* t;
b1b1 = misc.multArray(b, b, 4, 3, [1, 2]);
B1 = quantity.Discrete(b(:,:,1,1)', 'size', [ 1, 1 ], 'gridName', {'t', 'z'}, 'grid', {t(1,:), z(:,1)});
B1B1 = B1*B1;
testCase.verifyTrue(numeric.near(b1b1', B1B1.on()));

% vector multiplication of b(z, t) * b(v, t)
b2 = cat(3, z .* t, z*0 +1);
b2b2 = misc.multArray(b2, permute(b2, [1 2 4 3]), 4, 3, [1, 2]);
b2b2 = permute(b2b2, [2 1 3 4]);
B2 = quantity.Discrete(b2, 'size', [ 2, 1 ], 'gridName', {'z', 't'}, 'grid', {z(:,1), t(1,:)});
B2B2 = B2*B2';
testCase.verifyTrue(numeric.near(b2b2, B2B2.on()));

c2 = cat(4, sin(z), sin(t), cos(z));
b2c2 = misc.multArray(b2, c2, 4, 3, [1 2]);
C2 = quantity.Discrete(c2, 'size', [1 3],  'gridName', {'z', 't'}, 'grid', {z(:,1), t(1,:)});
B2C2 = B2 * C2;
testCase.verifyTrue(numeric.near(permute(b2c2, [2 1 3 4]), B2C2.on()));

% matrix b(z,t) * b(z,t)
b = cat(4, cat(3, z .* t, z*0 +1), cat(3, sin( z ), cos( z .* t )));
bb = misc.multArray(b, b, 4, 3, [1, 2]);
bb = permute(bb, [2 1 3 4]);
B = quantity.Discrete(b, 'size', [ 2, 2 ], 'gridName', {'z', 't'}, 'grid', {z(:,1), t(1,:)});
BB = B*B;
testCase.verifyTrue(numeric.near(bb, BB.on));

% matrix multiplication with one commmon and one distinct domain
c = cat(4, cat(3, v .* t2, v*0 +1), cat(3, sin( v ), cos( v .* t2 )), cat(3, cos( t2 ), tan( v .* t2 )));
bc = misc.multArray(b, c, 4, 3, 2);
bc = permute(bc, [ 1 2 4 3 5 ]);
B = quantity.Discrete(b, 'size', [ 2, 2 ], 'gridName', {'z', 't'}, 'grid', {z(:,1), t(1,:)});
C = quantity.Discrete(c, 'size', [2, 3], 'gridName', {'v', 't'}, 'grid',{v(:,1),  t2(1,:)});
BC = B*C;
testCase.verifyTrue(numeric.near(bc, BC.on));

%%
z = linspace(0,1).';
a = quantity.Discrete({sin(z * pi), cos(z* pi)}, 'grid', {z}, ...
	'gridName', 'z', 'name', 'a');
aa = a.' * a;
%%
testCase.verifyTrue( numeric.near( aa(1,1).on() , sin(z * pi) .* sin(z * pi)));
testCase.verifyTrue( numeric.near( aa(1,2).on() , sin(z * pi) .* cos(z * pi)));
testCase.verifyTrue( numeric.near( aa(2,2).on() , cos(z * pi) .* cos(z * pi)));

%% test multiplicatin with constants:
C = [3 0; 0 5];
c = quantity.Discrete(C, 'gridName', {}, 'grid', {}, 'name', 'c');
testCase.verifyTrue( numeric.near( squeeze(double(a*c)), [sin(z * pi) * 3, cos(z * pi)  * 5]));
testCase.verifyTrue( numeric.near( squeeze(double(a*[3 0; 0 5])), [sin(z * pi) * 3, cos(z * pi)  * 5]));
testCase.verifyTrue( numeric.near( double(c*c), C*C));

%% test multiplication with a scalar:
s = quantity.Discrete(42, 'gridName', {}, 'grid', {}', 'name', 's');
testCase.verifyTrue( numeric.near( squeeze(double(42 * a)), [sin(z * pi) * 42, cos(z * pi) * 42]));
testCase.verifyTrue( numeric.near( squeeze(double(a * 42)), [sin(z * pi) * 42, cos(z * pi) * 42]));

testCase.verifyTrue( numeric.near( squeeze(double(s * a')), [sin(z * pi) * 42, cos(z * pi) * 42]));
testCase.verifyTrue( numeric.near( squeeze(double(a * s)), [sin(z * pi) * 42, cos(z * pi) * 42]));

%% test 



end

function testIsConstant(testCase)

C = quantity.Discrete(rand(3,7), 'gridName', {});
testCase.verifyTrue(C.isConstant());

z = linspace(0, pi)';
A = quantity.Discrete(sin(z), 'grid', z, 'gridName', 'z');
testCase.verifyFalse(A.isConstant());

end

function testMTimesConstant(testCase)

myGrid = linspace(0, 2*pi)';
a = cat(3, [myGrid*0, myGrid, sin(myGrid)], [myGrid.^2, myGrid.^3, cos(myGrid)]); % dim = (100, 3, 2)
c = rand(2, 3);  % dim = (2, 3)

A = quantity.Discrete(a, 'size', [3, 2], 'grid', myGrid, 'gridName', {'z'});
C = quantity.Discrete(c, 'gridName', {});

ac = misc.multArray(a, c, 3, 1);    % dim = (100, 3, 3)
AC = A*C;
Ac = A*c;

ca = permute(misc.multArray(c, a, 2, 2), [2, 1, 3]);
CA = C*A;
cA = c*A;

verifyTrue(testCase, numeric.near(Ac.on(), ac));
verifyTrue(testCase, numeric.near(AC.on(), ac));
verifyTrue(testCase, numeric.near(cA.on(), ca, 1e-12));
verifyTrue(testCase, numeric.near(CA.on(), ca, 1e-12));

end

function testMPower(testCase)

%%
z = linspace(0,1).';
a = sin(z * pi);

A = quantity.Discrete({a}, 'grid', {z}, 'gridName', 'z');

aa = sin(z * pi) .* sin(z * pi);
AA = A^2;

verifyTrue(testCase, numeric.near(aa, AA.on()));


end

function testPlus(testCase)
%%
z = linspace(0,1,31).';
zeta = linspace(0,1,51);
[zGrid, zetaGrid] = ndgrid(z, zeta);
a = sin(z * pi);
aLr = sin(zeta * pi);
b = cos(z * pi);
C = zeta;
bc = cos(zGrid * pi) + zetaGrid;
azZeta = sin(zGrid * pi) + sin(zetaGrid * pi);
bzZeta = cos(zGrid * pi) + cos(zetaGrid * pi);

AB = quantity.Discrete({a, b}, 'grid', {z}, 'gridName', 'blub');
A = quantity.Discrete({a}, 'grid', {z}, 'gridName', 'z');
ALr = quantity.Discrete({aLr}, 'grid', {zeta}, 'gridName', 'z');
B = quantity.Discrete({b}, 'grid', {z}, 'gridName', 'z');
C = quantity.Discrete({C}, 'grid', {zeta}, 'gridName', 'zeta');
AZZETA = quantity.Discrete({azZeta}, 'grid', {z, zeta}, 'gridName', {'z', 'zeta'});
BZZETA = quantity.Discrete({bzZeta}, 'grid', {z, zeta}, 'gridName', {'z', 'zeta'});

ABAB = AB + AB;
BC = B+C;
ApB = A+B;
ABZZETA = AZZETA + BZZETA;
ALrA = ALr+A;

%%
testCase.verifyEqual(a+a, ABAB(1).on());
testCase.verifyEqual(b+b, ABAB(2).on());
testCase.verifyEqual(a+b, ApB.on());
numeric.near(bc, BC.on())
testCase.verifyEqual(azZeta+bzZeta, ABZZETA.on());
testCase.verifyEqual(a+a, ALrA.on(z), 'RelTol', 1e-3);

%% additional test
c = quantity.Discrete(zGrid, 'grid', {z, zeta}, ...
	'gridName', {'z', 'zeta'}, 'name', 'a');
d = quantity.Discrete(zetaGrid.', 'grid', {zeta, z}, ...
	'gridName', {'zeta', 'z'}, 'name', 'b');
e = c + d;
eMat = e.on();
eMatReference = zGrid + zetaGrid;
%%
testCase.verifyEqual(numel(eMat), numel(eMatReference));
testCase.verifyEqual(eMat(:), eMatReference(:));


%% addition with constant values
AB12 = AB + [1 2];
testCase.verifyEqual(permute([a b], [1 3 2]), AB.on());

AB2 = AB' * AB;
tst(:,1,1) = a.^2 + 1;
tst(:,1,2) = a.*b + 2;
tst(:,2,1) = b.*a + 3;
tst(:,2,2) = b.^2 + 4;

AB2c = AB2 + [1 2; 3 4];
cAB2 = [1 2; 3 4] + AB2;

testCase.verifyEqual(AB2c.on(), tst)
testCase.verifyEqual(cAB2.on(), tst)

end

function testInit(testCase)
%%
z = linspace(0,1).';
t = linspace(0,1,101);
v = {sin(z * t * pi); cos(z * t * pi)};
V = cat(3, v{:});
b = quantity.Discrete(v, 'grid', {z, t}, 'gridName', {'z', 't'});
c = quantity.Discrete(V, 'grid', {z, t}, 'gridName', {'z', 't'});
d = quantity.Discrete(V, 'gridName', {'z', 't'});

%%
verifyTrue(testCase, misc.alln(b.on() == c.on()));
verifyTrue(testCase, misc.alln(b.on() == d.on()));

end

function testInt(testCase)
%%
% z = linspace(0,1).';
% t = linspace(0,1,101);
% v = {sin(z * t * pi); cos(z * t * pi)};
% b = quantity.Discrete(v, 'grid', {z, t}, 'gridNames', {'z', 't'});


z = linspace(0, 2*pi, 701)';
t = linspace(0, 3*pi, 501);

F = {@(z,t) sin(z*t), @(z,t) cos(z*t)};

a = quantity.Discrete(cat(3, sin(z*t), cos(z*t)), ...
	'size', [2 1], 'grid', {z, t}, 'gridName', {'z', 't'});

At = int(a, 'z');
Anumt = [];
for tau = t
	Anumt = [Anumt; ...
		integral(@(z)F{1}(z,tau), z(1), z(end)), ...
		integral(@(z)F{2}(z,tau), z(1), z(end))];
end

verifyTrue(testCase, numeric.near(At.on(), Anumt, 1e-3));

Az = int(a, 't');
AnumZ = [];
for zeta = z'
	AnumZ = [AnumZ; ...
		integral(@(t)F{1}(zeta,t), t(1), t(end)), ...
		integral(@(t)F{2}(zeta,t), t(1), t(end))];
end

verifyTrue(testCase, numeric.near(Az.on(), AnumZ, 1e-3));

A = int(a);
Anum = [integral2(@(z,t) sin(z.*t), z(1), z(end), t(1), t(end)); ...
	integral2(@(z,t) cos(z.*t), z(1), z(end), t(1), t(end))];

verifyTrue(testCase, numeric.near(A, Anum, 1e-2));

end

function testNDGrid(testCase)
%%
z = linspace(0,1).';
t = linspace(0,1,101);
b = quantity.Discrete({sin(z * t * pi); cos(z * t * pi)}, 'grid', {z, t}, 'gridName', {'z', 't'});
% #TODO
end

function testDefaultGrid(testCase)
v = quantity.Discrete.value2cell( rand([100, 42, 2, 3]), [100, 42], [2, 3]);
g = quantity.Discrete.defaultGrid([100, 42]);
testCase.verifyEqual(g{1}, linspace(0, 1, 100).');
testCase.verifyEqual(g{2}, linspace(0, 1, 42));
end

function testValue2Cell(testCase)
v = rand([100, 42, 2, 3]);
V = quantity.Discrete( quantity.Discrete.value2cell(v, [100, 42], [2, 3]), 'gridName', {'z', 't'} );
verifyTrue(testCase, misc.alln(v == V.on()));
end

function testMtimesDifferentGrid(testCase)
%%
zetaA = linspace(0, 1, 21);
zA = linspace(0, 1);
[zetaAGrid, zAGrid] = ndgrid(zetaA, zA);
zX = linspace(0, 1, 21);
tX = linspace(1, 2, 31);
[zXGrid, tXGrid] = ndgrid(zX, tX);
a = quantity.Discrete({sin(zetaAGrid * pi), cos(zAGrid* pi)}, ...
	'grid', {zetaA, zA}, 'gridName', {'zeta', 'z'});
x = quantity.Discrete({zXGrid; tXGrid}, ...
	'grid', {zX, tX}, 'gridName', {'z', 't'});
ax = a * x;
ax2 = x.' * a.';
% calculate reference result with stupid for loops
referenceResult = zeros(numel(zA), numel(zetaA), numel(tX));
aMat = a.on();
xMat = x.on({zA, tX});
for zIdx = 1:numel(zA)
	for zetaIdx = 1:numel(zetaA)
		for tIdx = 1:numel(tX)
			referenceResult(zIdx, zetaIdx, tIdx) = ...
				reshape(aMat(zetaIdx, zIdx, :, :), [1, 2]) ...
				* reshape(xMat(zIdx, tIdx, :), [2, 1]);
		end
	end
end

%%
testCase.verifyEqual(referenceResult, ax.on(), 'AbsTol', 1e-12);
testCase.verifyEqual(referenceResult, permute(ax2.on(), [1, 3, 2]), 'AbsTol', 1e-12);
%%
end

function testZeros(testCase)
z = quantity.Discrete.zeros([2, 7, 8], {linspace(0,10)', linspace(0, 23, 11)}, 'gridName', {'a', 'b'});
O = zeros(100, 11, 2, 7, 8);

testCase.verifyEqual(z.on(), O);

end

function testChangeGrid(testCase)
z = linspace(0, 1, 21).';
t = linspace(0, 1, 31);
quan = quantity.Discrete({sin(z * t * pi); cos(z * t * pi)}, 'grid', {z, t}, 'gridName', {'z', 't'});
gridSampled = {linspace(0, 1, 11), linspace(0, 1, 21)};
quanCopy = copy(quan);
quanSampled = quanCopy.changeGrid(gridSampled, {'z', 't'});
testCase.verifyEqual(quanSampled.on(), quan.on(gridSampled))

quanCopy2 = copy(quan);
quanSampled2 = quanCopy2.changeGrid(gridSampled, {'t', 'z'});
testCase.verifyEqual(quanSampled2.on(), permute(quan.on(gridSampled, {'t', 'z'}), [2, 1, 3]));
end



function testSubs(testCase)
%%
myTestArray = ones(21, 41, 2, 3);
myTestArray(:,1,1,1) = linspace(0, 1, 21);
myTestArray(:,:,2,:) = 2;
quan = quantity.Discrete(myTestArray, 'gridName', {'z', 'zeta'}, 'size', [2, 3]);
quanZetaZ = quan.subs({'z', 'zeta'}, {'zeta', 'z'});
quan10 = quan.subs({'z', 'zeta'}, {1, 0});
quan01 = quan.subs({'z', 'zeta'}, {0, 1});
quant1 = quan.subs({'z', 'zeta'}, {'t', 1});
quanZetaZeta = quan.subs({'z'}, 'zeta');
quanPt = quan.subs({'z'}, {1});
quanEta = quan.subs({'z'}, {'eta'});
%%
testCase.verifyEqual(quan10, shiftdim(quan.on({1, 0})));
testCase.verifyEqual(quan01, shiftdim(quan.on({0, 1})));
testCase.verifyEqual(quanZetaZ.on(), quan.on());
testCase.verifyEqual(quant1.on(), squeeze(quan.on({quan(1).grid{1}, 1})));
testCase.verifyEqual(quanZetaZ(1).gridName, {'zeta', 'z'})
testCase.verifyEqual(quant1(1).gridName, {'t'})

quanZetaZetaReference = misc.diagNd(quan.on({linspace(0, 1, 41), linspace(0, 1, 41)}), [1, 2]);
testCase.verifyEqual(quanZetaZeta.on(), quanZetaZetaReference);
testCase.verifyEqual(quanEta(1).gridName, {'eta', 'zeta'})
testCase.verifyEqual(quanPt.on(), shiftdim(quan.on({1, quan(1).grid{2}})));

%% 4d-case
myTestArray4d = rand(11, 21, 31, 41, 1, 2);
quan4d = quantity.Discrete(myTestArray4d, 'gridName', {'z', 'zeta', 'eta', 'beta'}, 'name', 'fun5d');
quan4dAllEta = quan4d.subs({'z', 'zeta', 'eta', 'beta'}, {'eta', 'eta', 'eta', 'eta'});
testCase.verifyEqual(reshape(quan4d.on({1, 1, 1, 1}), size(quan4d)), ...
	reshape(quan4dAllEta.on({1}), size(quan4dAllEta)));
%
quan4dArbitrary = quan4d.subs({'z', 'zeta', 'eta', 'beta'}, {'zeta', 'beta', 'z', 1});
testCase.verifyEqual(reshape(quan4d.on({1, 1, 1, 1}), size(quan4d)), ...
	reshape(quan4dArbitrary.on({1, 1, 1}), size(quan4dAllEta)));
end

function testZTTimes(testCase)
%%
z = linspace(0,1).';
a = quantity.Discrete({sin(z * pi), cos(z* pi)}, 'grid', z, 'gridName', {'z'});
t = linspace(0,1,101);
b = quantity.Discrete({sin(t * pi); cos(t * pi)}, 'grid', {t'}, 'gridName', {'t'});

AB = a * b;

F = @(z,t) sin(z*pi) .* sin( t * pi) + cos(z*pi) .* cos( t * pi);

%%
verifyTrue(testCase, numeric.near(AB.on(), F(z,t)) );

end


function testZZTTimes(testCase)
%%
z = linspace(0,1).';
a = quantity.Discrete({sin(z * pi), cos(z* pi)}, 'grid', z, 'gridName', {'z'});
t = linspace(0,1,101);
b = quantity.Discrete({sin(z * t * pi); cos(z * t * pi)}, 'grid', {z, t}, 'gridName', {'z', 't'});

%%
verifyTrue(testCase, numeric.near(b(1).on(), sin(z * t * pi)));
verifyTrue(testCase, numeric.near(b(2).on(), cos(z * t * pi)));

ab = a*b;

F = @(z,t) sin(z*pi) .* sin(z * t * pi) + cos(z*pi) .* cos(z * t * pi);
verifyTrue(testCase, numeric.near(ab.on(), F(z,t)) );
end

function testZZZTmTimes(testCase)

%%
z = linspace(0,1).';
t = linspace(0,2,101);
a = quantity.Discrete({sin(z * pi), cos(z* pi)}, 'grid', z, 'gridName', {'z'});
b = quantity.Discrete({sin(z * t * pi); cos(z * t * pi)}, 'grid', {z, t}, 'gridName', {'z', 't'});

A = a.' * a;

syms Z T
c = quantity.Symbolic([sin(Z * pi), cos(Z* pi)], 'grid', z);
d = quantity.Symbolic([sin(Z * T * pi); cos(Z * T * pi)], 'grid', {z, t});

C = c' * c;
Cd = C * d;

Anum = cat(3, sin(z * pi).^2 .* sin(z * t * pi) + sin(z * pi) .* cos(z * pi) .* cos(z * t * pi), ...
	sin(z * pi) .* cos(z * pi) .* sin(z * t * pi) + cos(z * pi).^2 .* cos(z * t * pi));
%
% figure(2);clf;
% subplot(211);
% misc.isurf( z, t, Anum(:,:,1))
% subplot(212);
% misc.isurf( z, t, Anum(:,:,2))

Ab = A * b;
verifyTrue(testCase, numeric.near(Ab.on(), Anum, 1e-9))


end

function testIsEmpty(tc)

% create an empty quantity and check if it is empty:
e = quantity.Discrete.empty([5, 0]);
tc.verifyTrue(isempty(e));

% create an empty object by calling the constructor without arguements.
tc.verifyTrue(isempty(quantity.Discrete()));

% create a constant quantity.Discrete. This should not be empty:
c = quantity.Discrete(1, 'grid', {}, 'gridName', {});
tc.verifyTrue(~isempty(c));
end


























