function [tests] = testGevrey()
%TESTGEVREY Summary of this function goes here
%   Detailed explanation goes here
tests = functiontests(localfunctions);
end

function testBasicVariableInit(testCase)

%%
t = linspace(0, 1, 11)';
g1 = signals.GevreyFunction('order', 1.8, 't', t);
g2 = signals.GevreyFunction('order', 1.5, 't', t);

G = [g1; g2];
N_diff = 2;
derivatives = G.diff(0:N_diff);

GD = zeros(length(t), N_diff+1, 2);
for k = 1:N_diff+1
	GD(:, k, 1) = signals.gevrey.bump.g(t, k-1, t(end), g1.sigma);
	GD(:, k, 2) = signals.gevrey.bump.g(t, k-1, t(end), g2.sigma);
end
	
testCase.verifyEqual(derivatives.on(), GD);
end