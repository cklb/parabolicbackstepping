function [tests] = testFunction()
%TESTQUANTITYFUNCTIONHANDLE Summary of this function goes here
%   Detailed explanation goes here
tests = functiontests(localfunctions);
end

function testCastSymbolic2Function(testCase)

z = linspace(0, 2*pi)';
Z = sym("z");
s = quantity.Symbolic([sin(Z); cos(Z)], 'grid', z, 'gridName', 'z');

f = quantity.Function(s);

testCase.verifyTrue(all(size(f) == size(s)));

end

function testTimesSymbolic(testCase)

z = linspace(0, 2*pi)';
f = quantity.Function({@(z) sin(z), @(z) cos(z)}, 'grid', z, 'gridName', 'z');
s = quantity.Symbolic([sym("z"); 1], 'grid', z, 'gridName', 'z');

fs = f*s;
sf = s' * f';

testCase.verifyTrue( fs.near(sf) );

end

function testOn(testCase)
z = linspace(0, 2*pi)';
f = quantity.Function({@(z) sin(z), @(z) cos(z)}, 'grid', z, 'gridName', 'z');

F1 = f.on(z);
F2 = f.on();

testCase.verifyTrue( numeric.near(F1, F2) );

end

function testUMinus(testCase)
f = quantity.Function(@(z) z, 'grid', linspace(0,1).');
mf = -f;
verifyTrue(testCase, all( mf.on() + f.on() == 0));

%%
f1 = @(z) sinh(z * pi);
f2 = @(z) cosh(z * pi);
f3 = @(z) sin(z * pi);

f = quantity.Function({f1, f3 ; f2, f2; f1, f3 }, 'name', 'sinhcosh');
F = -f;
%
z = sym('z', 'real');
fsym = sym({f1, f3 ; f2, f2; f1, f3});
fsym = symfun(- fsym, z);

z = f(1).grid{:};

Fsym = fsym(z);
%%
for k = 1:numel(F)
    verifyTrue(testCase, numeric.near(double(Fsym{k}), F(k).valueDiscrete, 1e-12));    
end

end

function testGrid(testCase)

% erlaubte initialisierung
f = quantity.Function(@(z) z, 'grid', linspace(0,1).');

% nicht erlaubte initialisierung
try
   f = quantity.Function(@(z) z, 'grid', linspace(0,1));
catch ex
    verifyTrue(testCase, isa(ex, 'MException'));
end

% zwei unabhängige Variablen
f = quantity.Function(@(z,t) z .* t, 'grid', ...
	{linspace(0,1).', linspace(0,1, 20)}, 'name', 'test');

try
   f = quantity.Function(@(z,t) z .* t, 'grid', ...
	   {linspace(0,1).', linspace(0,1, 20).'}, 'name', 'test');
catch ex
    verifyTrue(testCase, isa(ex, 'MException'))
end


end

function testInner(testCase)

f1 = @(z) sinh(z * pi);
f2 = @(z) cosh(z * pi);

f = quantity.Function({f1 ; f2 }, 'grid', linspace(0,1,1e4).');

F = f.inner(f.');

verifyTrue(testCase, numeric.near( integral(@(z) f1(z).^2, 0, 1), F(1,1), 1e-6));
verifyTrue(testCase, numeric.near( integral(@(z) f2(z).^2, 0, 1 ), F(2, 2), 1e-6));
verifyTrue(testCase, numeric.near( integral(@(z) f1(z) .* f2(z), 0, 1 ) , F(1, 2), 1e-6));
end

function testMTimes(testCase)
%%
f1 = @(z) sinh(z * pi);
f2 = @(z) cosh(z * pi);
f3 = @(z) sin(z * pi);

z = sym('z', 'real');
fsym = sym({f1, f3 ; f2, f2; f1, f3});
fsym = symfun(fsym * fsym.', z);

f = quantity.Function({f1, f3 ; f2, f2; f1, f3 }, 'name', 'sinhcosh');

F = f * f.';

z = f(1).grid{:};

Fsym = fsym(z);
%%
for k = 1:numel(F)
    verifyTrue(testCase, numeric.near(double(Fsym{k}), F(k).valueDiscrete, 1e-12));    
end

end

function testInit(testCase)
fun = @(z) z.^2;
q = quantity.Function(fun, 'name', 'test');
verifyEqual(testCase, q.valueContinuous, fun);
verifyEqual(testCase, q.name, 'test');
end

function testDimensions(testCase)
%%
f = quantity.Function(...
    {@(z) (sin(z .* pi) ), @(z) z; @(z) 0.*z, @(z)(cos(z .* pi) )}, ...
    'grid', {linspace(0, 1, 42)'}, ...
    'gridName', {'z'});

%%
verifyEqual(testCase, f(1).grid, {linspace(0,1, 42).'});
verifyEqual(testCase, f.gridSize, 42);

end

function testEvaluate(testCase)
%%
f1 = @(z) (sin(z .* pi) );
f2 = @(z) z;
f3 = @(z) (cos(z .* pi) );
z = linspace(0, 1, 42).';
f = quantity.Function({f1, f2; @(z) z.^2, f3}, ...
    'grid', {z}, ...
    'gridName', {'z'});

value = f.on();

%%
verifyTrue(testCase, all(squeeze(value(:,1,1)) == f1(z)));
verifyTrue(testCase, all(squeeze(value(:,1,2)) == f2(z)));
verifyTrue(testCase, all(squeeze(value(:,2,1)) == z.^2));
verifyTrue(testCase, all(squeeze(value(:,2,2)) == f3(z)));


end

function testSize(testCase)
%%
f = quantity.Function(...
    {@(z) (sin(z .* pi) ),   @(z) z,             @(z) 0.*z; ...
     @(z) 0.*z,              @(z)(cos(z .* pi)), @(z) 0.*z}, ...
    'grid', {linspace(0, 1, 42).'}, ...
    'gridName', {'z'});

%%
verifyEqual(testCase, size(f), [2, 3]);
verifyEqual(testCase, size(f, 2), 3);

end