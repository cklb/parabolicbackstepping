function [tests] = testOperator()
%TESTGEVREY Summary of this function goes here
%   Detailed explanation goes here
tests = functiontests(localfunctions);
end

function setupOnce(testCase)
%%
z = sym('z', 'real');
s = sym('s');
Z = linspace(0, 1, 11)';

% init with symbolic values
A = cat(3, [ 0, 0, 0, 0; ...
		     1, 0, 0, 0; ...
			 0, 1, 0, 0; ...
			 0, 0, 1, 0], ...
            [0, 0, 0, -3 + z; ...
			 0, 0, 0,   0; ...
			 0, 0, 0,   0; ...
			 0, 0, 0,   0], ...
			[0, 0, 0, -4; ...
			 0, 0, 0, 0; ...
			 0, 0, 0, 0; ...
			 0, 0, 0, 0]);

B = cat(3, [1, 0, 0, 0]', zeros(4, 1), zeros(4, 1));
		 
for k = 1:3
	a{k} = quantity.Symbolic(A(:,:,k), 'grid', Z, 'variable', z);
end
		 
A = quantity.Operator(a, 's', s);

testCase.TestData.A = A;
testCase.TestData.a = a;

end

function testCTranspose(testCase)
	At = testCase.TestData.A';
	testCase.verifyTrue(At(3).coefficient == testCase.TestData.A(3).coefficient');	
end

% 
function testFundamentalMatrixSpaceDependent(testCase)
%
	a = 20;
	z = sym('z', 'real');
	Z = linspace(0, 1, 32)';
	A = quantity.Operator(...
		{quantity.Symbolic([1, z; 0, a], 'variable', 'z', 'grid', Z)});
	
	F = A.stateTransitionMatrix();
		
	% compute the exact solution from the peano-baker series paper.
	if numeric.near(a, 1)
		f = 0.5 * z^2 * exp(z);
	else
		f = (exp(z) - exp(a * z) - (1 - a) * z * exp( a * z ) ) / (1 - a)^2; 
	end   

	PhiExact = quantity.Symbolic([exp(z), f; 0, exp(a*z)], 'grid', Z, 'name', 'Phi_A');
		
	testCase.verifyEqual(double(F), PhiExact.on(), 'RelTol', 1e-6);
	
end
% 
function testStateTransitionMatrix(testCase)
N = 2;
Z = linspace(0,1,11)';
A0 = quantity.Symbolic([0 1; 1 0], 'grid', Z, 'variable', 'z');
A1 = quantity.Symbolic([0 1; 0 0], 'grid', Z, 'variable', 'z');
A = quantity.Operator({A0, A1});
B = quantity.Operator(quantity.Symbolic([-1 -1; 0 0], 'grid', Z, 'variable', 'z'));

[Phi1, Psi1] = A.stateTransitionMatrix(N, B);
[Phi2, Psi2] = A.stateTransitionMatrixByOdeSystem(N, B);

testCase.verifyEqual(double(Phi1), double(Phi2), 'AbsTol', 1e-2);
testCase.verifyEqual(double(Psi1), double(Psi2), 'AbsTol', 1e-2);
end

function testChangeGrid(testCase)
	A = testCase.TestData.A;
	a = testCase.TestData.a;
	A0 = A.changeGrid({0}, {'z'});
	
	testCase.verifyEqual( A0.gridSize, 1);
	
	for k = 1:3
		testCase.verifyEqual( ...
			a{k}.at(0), squeeze(A0(k).coefficient.on()), 'AbsTol', 10*eps);
	end
end
	
	
function testMTimes(testCase)

	z = linspace(0, pi, 15)';
	A0 = quantity.Discrete(reshape((repmat([1 2; 3 4], length(z), 1)), length(z), 2, 2),...
		'gridName', 'z', 'grid', z, 'name', 'A');
	A1 = quantity.Discrete(reshape((repmat([5 6; 7 8], length(z), 1)), length(z), 2, 2), ...
		'gridName', 'z', 'grid', z, 'name', 'A');
	A2 = quantity.Discrete(reshape((repmat([9 10; 11 12], length(z), 1)), length(z), 2, 2), ...
	'gridName', 'z', 'grid', z, 'name', 'A');
	
	A = quantity.Operator({A0, A1, A2});
	B = quantity.Operator({A0});
	
	C = A*B;
	
	s = sym('s');
	
	As = A0.at(0) + A1.at(0)*s + A2.at(0)*s^2;
	Bs = A0.at(0);
	
	Cs = misc.polynomial2coefficients(As*Bs, s);
	
	for k = 1:3
		testCase.verifyTrue( numeric.near(Cs(:,:,k), C(k).coefficient.at(0)) );
	end
	
	
	K = [0 1; 1 0];
	
	C =	K*A;
	
	for k = 1:3
		numeric.near(double(C(k).coefficient), double(K * A(k).coefficient));
	end
	
end

function testSum(testCase)
z = linspace(0, pi, 15)';
	A0 = quantity.Discrete(reshape((repmat([1 2; 3 4], length(z), 1)), length(z), 2, 2),...
		'gridName', 'z', 'grid', z, 'name', 'A');
	A1 = quantity.Discrete(reshape((repmat([5 6; 7 8], length(z), 1)), length(z), 2, 2), ...
		'gridName', 'z', 'grid', z, 'name', 'A');
	A2 = quantity.Discrete(reshape((repmat([9 10; 11 12], length(z), 1)), length(z), 2, 2), ...
	'gridName', 'z', 'grid', z, 'name', 'A');
	
	A = quantity.Operator({A0, A1, A2});
	B = quantity.Operator({A0});
	
	C = A + B;
		
	testCase.verifyTrue( numeric.near(double(C(1).coefficient), double(A0 + A0)) );
	testCase.verifyTrue( numeric.near(double(C(2).coefficient), double(A1)) );
	testCase.verifyTrue( numeric.near(double(C(3).coefficient), double(A2)) );
	
	C = B + A;
	
	testCase.verifyTrue( numeric.near(double(C(1).coefficient), double(A0 + A0)) );
	testCase.verifyTrue( numeric.near(double(C(2).coefficient), double(A1)) );
	testCase.verifyTrue( numeric.near(double(C(3).coefficient), double(A2)) );
	
end
	
	
	
	
	
	
	
	
	
	
	
	
	
	