function [tests] = signalsTest()
%UNTITLED Summary of this function goes here
%   Detailed explanation goes here
tests = functiontests(localfunctions);
end

function taylorPolynomialTest(testCase)

t = linspace(0, 12, 11)';
c = [1, 2]';
f = signals.faultmodels.TaylorPolynomial('coefficients', c, ...
	'T', 1, 'globalGrid', t);

testCase.verifyEqual(c(1) + c(2) * t, f.on())
end

function testSmoothStep(testCase)
%% computed step by hand:
z = sym('z', 'real');
d = 1;
sig = 3 / d^2 * z^2 - 2 / d^3 * z^3;

SI = quantity.Symbolic(sig);

%% compute step by function
si = signals.smoothStep(1);

%%
verifyEqual(testCase, si.on(), SI.on());

end

function testGevreyBump(testCase)
% Test script to check if the class for the bumb function returns the right
% signals.

g = signals.GevreyFunction('diffShift', 1);
b = signals.gevrey.bump.dgdt_1(g.T, g.sigma, g.grid{1}) * 1 / signals.gevrey.bump.dgdt_0(g.T, g.sigma, g.T);

%% Test scaling of Gevrey function
verifyEqual(testCase, b, g.on(), 'AbsTol', 1e-13);

% test the derivatives of the bump
g = signals.GevreyFunction('diffShift', 0);
g.diff(0:5);


end