function [tests] = testSs()
%TESTMISC Summary of this function goes here
%   Detailed explanation goes here
tests = functiontests(localfunctions());
end

function testPlanTrajectory(tc)
%%
	A = [-30 0; 0 -50];
	B = [6; -5];
	C = [1, 1];
	D = [];
	
	S = ss(A, B, C, D);
	t = linspace(0, 0.01, 1e2)';
	x0 = [10; 12];
	x1 = [-5; -5];
	[trj.u, trj.y, trj.x] = misc.ss.planTrajectory(S, t, 'x0', x0, 'x1', x1);
		
	[y, t, x] = lsim(S, trj.u.on(), t, x0);
% 	subplot(3,1,1);
% 	plot(t, trj.u.on());
% 	
% 	subplot(3,1,2);
% 	plot(t,y);
% 	
% 	subplot(313);
% 	plot(t, x);
% 	disp(x(end,:))

	tc.verifyEqual(trj.x.on(), x, 'AbsTol', 2e-2);
	tc.verifyEqual(trj.y.on(), y, 'AbsTol', 1e-2);

end

function testParallel(tc)
s1 = ss(ones(2), [-1, 2; -3, -4], [2, 3; 4, 5], eye(2), ...
'InputName', {'u', 'v'}, 'OutputName', {'x', 'y'});
s2 = ss(ones(2), [-1, -2; 3, -4], [2, 3; 4, 5], eye(2), ...
'InputName', {'w', 'u'}, 'OutputName', {'z', 'y'});
s12 = misc.ss.parallel(s1, s2);

tc.verifyEqual(s12.InputName.', {'v', 'u', 'w'});
tc.verifyEqual(s12.OutputName.', {'x', 'y', 'z'});
% tc.verifyEqual(s12.A, blkdiag(s1.A, s2.A));
% tc.verifyEqual(s12.B(:, 1), s1.B(:, 2));
% tc.verifyEqual(s12.B(:, 2), s1.B(:, 1) + s2.B(:, 2));
% tc.verifyEqual(s12.B(:, 3), s2.B(:, 1));
% tc.verifyEqual(s12.C(1, :), [s1.C(1, :), zeros(1, 2)]);
% tc.verifyEqual(s12.C(2, :), [s1.C(2, :), s2.C(2, :)]);
% tc.verifyEqual(s12.C(3, :), [zeros(1, 2), s2.C(1, :));

time = linspace(0, 5, 201);
u = time.^2;
v = time-3;
w = sin(time);
y1 = lsim(s1, [u; v], time);
y2 = lsim(s2, [w; u], time);
y12 = lsim(s12, [v; u; w], time);
myTol = 1e-6;
tc.verifyEqual(y12(:, 1), y1(:, 1), 'AbsTol', myTol);
tc.verifyEqual(y12(:, 2), y1(:, 2) + y2(:, 2), 'AbsTol', myTol);
tc.verifyEqual(y12(:, 3), y2(:, 1), 'AbsTol', myTol);

end % testParallel()

function testAddState2Output(tc)
stateName = 'x';
mySimulationModelOld = ss(ones(2), zeros(2, 3), [2, 1; 3, 1], ones(2, 3), ...
	'OutputName', {'y', 'z'}, 'InputName', {'u(1)', 'u(2)', 'a'});
mySimulationModelNew = misc.ss.addState2output(mySimulationModelOld, stateName);
tc.verifyEqual(mySimulationModelNew.OutputName, ...
	[mySimulationModelOld.OutputName; {'x(1)'; 'x(2)'}]);
tc.verifyEqual(mySimulationModelNew.C, ...
	vertcat(mySimulationModelOld.C, eye(size(mySimulationModelOld.A))));
end % testAddState2Output()

function testRemoveSingularEnumeration(tc)
thisResult = misc.ss.removeSingularEnumeration({'a(1)', 'a(2)', 'b(1)', 'c(1)', 'b', 'c(2)', 'd(1)'});
tc.verifyEqual(thisResult, {'a(1)', 'a(2)', 'b', 'c(1)', 'b', 'c(2)', 'd'});

end % testRemoveSingularEnumeration()

function testErrorSignal(tc)
simOriginal.x = [2; 2];
simOriginal.u = 1;
simReference.x = [1; 1];
simError = misc.ss.errorSignal(simOriginal, simReference);
tc.verifyEqual(simError.x, [1; 1]);
tc.verifyEqual(fieldnames(simError), {'x'});
end

function testAdd2SignalName(tc)
mySys = misc.ss.add2signalName(ss(1, 1, [1; 1], [], 'OutputName', {'a', 'b'}, ...
		'InputName', {'c'}), ...
	'OutputName', 'front', 'ode.');
mySys = misc.ss.add2signalName(mySys, 'InputName', 'back', '.ode');
tc.verifyEqual(mySys.OutputName, {'ode.a'; 'ode.b'});
tc.verifyEqual(mySys.InputName, {'c.ode'});
end

function testCombineInputSignals(tc)
myStateSpace = ss(-1, [1, 2, 3], 1, [], 'InputName', {'control', 'disturbance', 'my.fault'});
t = linspace(0, 4, 201);
disturbanceSignal = quantity.Symbolic(sin(sym('t')), 'grid', t, ...
	'variable', sym('t'), 'name', 'disturbance');
faultSignal = quantity.Symbolic(sin(sym('t')), 'grid', t, ...
	'variable', sym('t'), 'name', 'my.fault');

% case 1: only disturbance
u = misc.ss.combineInputSignals(myStateSpace, t, 'disturbance', disturbanceSignal);
y = quantity.Discrete(lsim(myStateSpace, u.on(), t), 'grid', t, ...
	'gridName', 't', 'name', 'y');
odeResiduum = - y.diff(1, 't') - y + 2*disturbanceSignal;
tc.verifyEqual(odeResiduum.abs.median(), 0, 'AbsTol', 5e-4);

% case 2: fault and disturbance
u2 = misc.ss.combineInputSignals(myStateSpace, t, 'disturbance', disturbanceSignal, ...
	'my.fault', faultSignal);
y2 = quantity.Discrete(lsim(myStateSpace, u2.on(), t), 'grid', t, ...
	'gridName', 't', 'name', 'y');
odeResiduum = - y2.diff(1, 't') - y2 + 2*disturbanceSignal + 3*faultSignal;
tc.verifyEqual(odeResiduum.abs.median(), 0, 'AbsTol', 5e-4);
end

function testRemoveEnumeration(tc)
tc.verifyEqual(misc.ss.removeEnumeration({'a(1)', 'a(2)', 'b'}), {'a', 'a', 'b'});
tc.verifyEqual(misc.ss.removeEnumeration({'a(1)', 'a(2)', 'b'}, false), {'a', 'a', 'b'});
tc.verifyEqual(misc.ss.removeEnumeration({'a(1)', 'a(2)', 'b'}, true), {'a', 'b'});
end

function testRemoveInputOfOutput(tc)
mySimulationModelOriginal = ss(1, [1, 2, 3], [1; 2; 3], [1, 0, 0; 0, 0, 0; 0, 0, 0], ...
	'OutputName', {'x', 'y', 'z'}, 'InputName', {'u(1)', 'u(2)', 'a'});
mySimulationModel = misc.ss.addInput2Output(mySimulationModelOriginal);
mySimulationModel = misc.ss.removeInputOfOutput(mySimulationModel);

for myProp = {'A', 'B', 'C', 'D', 'Ts', 'InputName', 'OutputName', 'Name'}
	tc.verifyEqual(mySimulationModelOriginal.(myProp{1}), mySimulationModel.(myProp{1}), ...
		['The property ', myProp{1}, ' is faulty']);
end

end % testRemoveInputOfOutput

function testAddInput2Output(tc)
% first example
mySimulationModelOriginal = ss(1, [1, 2, 3], [1; 2; 3], [1, 0, 0; 0, 0, 0; 0, 0, 0], ...
	'OutputName', {'x', 'y', 'z'}, 'InputName', {'u(1)', 'u(2)', 'a'});
mySimulationModelModified = misc.ss.addInput2Output(mySimulationModelOriginal);
verifyAddInput2Output(tc, mySimulationModelModified, mySimulationModelOriginal);

% second example
mySimulationModelOriginal2 = ss(1, [1, 2, 3], [1; 2; 3], [1, 0, 0; 0, 0, 0; 0, 0, 0], ...
	'OutputName', {'x(1)', 'x(2)', 'y'}, 'InputName', {'u(1)', 'u(2)', 'y'});
mySimulationModelModified2 = misc.ss.addInput2Output(mySimulationModelOriginal2);
verifyAddInput2Output(tc, mySimulationModelModified2, mySimulationModelOriginal2);

function testCase = verifyAddInput2Output(testCase, ssModified, ssOriginal)
	testCase.verifyEqual(ssModified.A, ssOriginal.A);
	testCase.verifyEqual(ssModified.B, ssOriginal.B);
	resultingOutputNames = unique([ssOriginal.OutputName; ssOriginal.InputName], 'stable');
	testCase.verifyEqual(ssModified.C, ...
		[ssOriginal.C; zeros(numel(resultingOutputNames)-size(ssOriginal.C, 1), size(ssOriginal.A, 1))]);
	testCase.verifyEqual(ssModified.D(1:size(ssOriginal.C, 1), :), ssOriginal.D);
	testCase.verifyEqual(size(ssModified.D), [numel(resultingOutputNames), size(ssOriginal.B, 2)]);
	for it = 1 : size(ssOriginal.B, 2)
		thisInputName = ssOriginal.InputName{it};
		if ~any(strcmp(ssOriginal.OutputName, thisInputName))
			testCase.verifyEqual(...
				ssModified.D(strcmp(ssModified.OutputName, thisInputName), ...
					strcmp(ssModified.InputName, thisInputName)), 1);
		end
	end
	testCase.verifyEqual(ssModified.InputName, ssOriginal.InputName);
	testCase.verifyEqual(ssModified.OutputName, resultingOutputNames);
end

end % testAddInput2Output

function testChangeSignalName(tc)
% change output
mySimulationModel = ss(1, 1, [1; 2; 3;], [], 'InputName', 'e');
mySimulationModel = misc.ss.setSignalName(mySimulationModel, 'output', {'a', 'b'}, {2, 1});
mySimulationModel = misc.ss.changeSignalName(mySimulationModel, {'a'}, {'c'});
tc.verifyEqual(mySimulationModel.OutputName, {'c(1)'; 'c(2)'; 'b'});

% change both and combine input
mySimulationModel2 = ss(1, [1, 2], [1; 2; 3], [], 'InputName', {'d', 'e'});
mySimulationModel2 = misc.ss.setSignalName(mySimulationModel2, 'output', {'a', 'b'}, {2, 1});
mySimulationModel2 = misc.ss.changeSignalName(mySimulationModel2, {'a', 'd'}, {'c', 'e'});
tc.verifyEqual(mySimulationModel2.OutputName, {'c(1)'; 'c(2)'; 'b'});
tc.verifyEqual(mySimulationModel2.InputName, {'e'});
tc.verifyEqual(mySimulationModel2.B, sum([1, 2]));

% change and combine enumerated input
mySimulationModel3 = ss(1, [1, 2, 3, 4], [5; 6], [], ...
	'InputName', {'a(1)', 'a(2)', 'b(1)', 'b(2)'});
mySimulationModel3modified = misc.ss.changeSignalName(mySimulationModel3, {'b'}, {'a'});
tc.verifyEqual(mySimulationModel3modified.InputName, {'a(1)'; 'a(2)'});
tc.verifyEqual(mySimulationModel3modified.B, [4, 6]);

end % testChangeSignalName

function testSetSignalName(tc)
mySimulationModel = ss(1, [1, 0], [1; 2; 3], []);
mySimulationModel = misc.ss.setSignalName(mySimulationModel, 'output', {'a', 'b'}, {2, 1});
mySimulationModel = misc.ss.setSignalName(mySimulationModel, 'input', {'c'}, {2});
tc.verifyEqual(mySimulationModel.OutputName, {'a(1)'; 'a(2)'; 'b'})
tc.verifyEqual(mySimulationModel.InputName, {'c(1)'; 'c(2)'})
end % testSetSignalName

function testSimulationOuput2Quantity(tc)
mySimulationModelA = ss(1, 1, [1; 2], [], 'OutputName', {'a'}, 'InputName', 'u');
mySimulationModelB = ss(1, 1, [1; 2], [], 'OutputName', {'b'}, 'InputName', 'u');
mySimulationModel = append(mySimulationModelA, mySimulationModelB);
myIc = 0.5*ones(size(mySimulationModel.A, 1), 1);
time = linspace(0, 1, 51);
myInput = zeros(numel(time), size(mySimulationModel, 2));
simulationOutputArray = lsim(mySimulationModel, myInput, time, myIc);
simulationOutputQuantity = misc.ss.simulationOutput2Quantity( ...
					simulationOutputArray, time, mySimulationModel.OutputName);
tc.verifyEqual(simulationOutputQuantity.a.on(), simulationOutputQuantity.b.on());
end % testSimulationOuput2Quantity