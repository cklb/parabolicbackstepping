function [tests] = testSsOdes()
% unittests for the class misc.ss.Odes.
tests = functiontests(localfunctions());
end

function testConstructor(tc)
ss1 = ss(1, 1, 1, 3, 'InputName', 'u1', 'OutputName', 'y1');
ss2 = ss(2, 2, 2, 4, 'InputName', 'u2', 'OutputName', 'y2');
testOdes = misc.ss.Odes(ss1, 'ss1', ss2, 'ss2');
testOde1 = misc.ss.Odes(ss1, 'ss1');
testOde2 = misc.ss.Odes(ss2, 'ss2');
testOdes12 = testOde1 + testOde2;
tc.verifyTrue(isequal(testOdes12, testOdes))
tc.verifyTrue(isequal(testOdes12.odes, testOdes.odes))
testOdes12backupRemove1 = copy(testOdes12);
testOdes12backupRemove1 = testOdes12backupRemove1.remove('ss1');
tc.verifyTrue(isequal(testOdes12backupRemove1, testOde2))
tc.verifyTrue(isequal(testOdes12backupRemove1.odes, testOde2.odes))
end % testConstructor()

function testExchange(tc)
ss1 = ss(1, 1, 1, 3, 'InputName', 'u1', 'OutputName', 'y1');
ss2 = ss(2, 2, 2, 4, 'InputName', 'u2', 'OutputName', 'y2');
Ode = misc.ss.Odes(ss1, 'ss1');
Ode.exchange('ss1', ss2);
time = linspace(0, 1, 21);
[Yss2, ~, Xss2] = step(ss2, time);
[YOde, ~, XOde] = step(Ode.odes, time);
tc.verifyTrue(isequal(XOde, Xss2));
tc.verifyTrue(isequal(Yss2, YOde(:, 2)));
end % testExchange(tc)

function testInputOutputName(tc)
myOdes = misc.ss.Odes(ss(zeros(2), 0.8*magic(2), eye(2), [], ...
	'InputName', 'error.measurement1', 'OutputName', 'observer.integrator'), 'integrator');
tc.verifyEqual(myOdes.InputName, myOdes.ss{1}.InputName)
tc.verifyEqual(myOdes.OutputName, ...
	[myOdes.ss{1}.OutputName; 'observer.integrator(1)'; 'observer.integrator(2)'])
end % testInputOutputName(tc)