function [tests] = testMisc()
%TESTMISC Summary of this function goes here
%   Detailed explanation goes here
tests = functiontests(localfunctions());
end

function testNameValuePairRemoveDots(tc)
nameValuePairsCleaned = misc.nameValuePairRemoveDots('asdf', 123', 'bli.bla', 'test');
tc.verifyEqual(nameValuePairsCleaned, {'asdf', 123});
end % testNameValuePairRemoveDots()

function testIsfield(tc)
myStruct = misc.setfield(struct(), 'topLevel.lowerLevel.data', 42);
tc.verifyTrue(misc.isfield(myStruct, 'topLevel'));
tc.verifyTrue(misc.isfield(myStruct, 'topLevel.lowerLevel'));
tc.verifyTrue(misc.isfield(myStruct, 'topLevel.lowerLevel.data'));
tc.verifyFalse(misc.isfield(myStruct, 'lowerLevel.data'));
tc.verifyFalse(misc.isfield(myStruct, 'abc'));
end

function testGetfield(tc)
myStruct = misc.setfield(struct(), 'topLevel.lowerLevel.data', 42);
result = misc.getfield(myStruct, 'topLevel.lowerLevel.data');
tc.verifyEqual(result, 42);
end

function testSetfield(tC)
myStruct = struct('asdf', 123);
myStruct = misc.setfield(myStruct, 'topLevel.lowerLevel.data', 42);
myStruct = misc.setfield(myStruct, 'topLevel.asdf', 1337);
myStruct = misc.setfield(myStruct, 'topLevel.lowerLevel.dieter', 3);

tC.verifyEqual(myStruct.asdf, 123);
tC.verifyEqual(myStruct.topLevel.lowerLevel.data, 42);
tC.verifyEqual(myStruct.topLevel.lowerLevel.dieter, 3);
tC.verifyEqual(myStruct.topLevel.asdf, 1337);
end

function testIsunique(testCase)
testCase.verifyTrue(misc.isunique({'bli', 'bla', 'blu'}));
testCase.verifyTrue(misc.isunique([1, 2, 3]));
testCase.verifyFalse(misc.isunique([1, 2, 1]));
testCase.verifyFalse(misc.isunique({'bli', 'bla', 'blu', 'bla'}));
testCase.verifyTrue(misc.isunique({'bli', 'bla', 'blu'}.'));
testCase.verifyTrue(misc.isunique([1, 2, 3].'));
testCase.verifyFalse(misc.isunique([1, 2, 1].'));
testCase.verifyFalse(misc.isunique({'bli', 'bla', 'blu', 'bla'}.'));
end


function testMTimesPointWise(testCase)

z = linspace(0, 2*pi, 1e2)';

A = cat(3, [sin(z), cos(z)],  [tan(z), sinh(z)]);
B = cat(3, [sin(z), cos(z)],  [tan(z), sinh(z)], [z, z.^2]);
C = misc.mTimesPointwise(A, B);

% symbolic computation for verification
Z = sym('z', 'real');
D = [sin(Z), tan(Z); cos(Z), sinh(Z)];
E = [sin(Z), tan(Z) Z; cos(Z) sinh(Z) Z^2];
F = D*E;

testCase.verifyTrue(numeric.near(C, ...
	reshape(double(subs(F, Z, z)), length(z), 2, 3), 1e-9));

end

function testGetInputNamesOfFunctionHandle(testCase)
myFun = @(z, zeta, x) sin(z)*zeta;
inputArgs = misc.getInputNamesOfFunctionHandle(myFun);
testCase.verifyEqual({'z', 'zeta', 'x'}, inputArgs);
end