function [tests] = testGains()
%TESTGAINS Summary of this function goes here
%   Detailed explanation goes here
tests = functiontests(localfunctions());
end

function testRemove(tc)
a = misc.Gain('in1', ones(10),...
	'outputType', {'x', 'y', 'xy', 'X'}, ...
	'lengthOutput', [2; 3; 4; 1]);
A = misc.Gains(a);
A.remove('asdf');
tc.verifyEqual(a.value, A.value)
B = copy(A);
tc.verifyTrue(isempty(B.remove('in1')));

e1 = misc.Gain('in1', ones(3, 2), ...
	'outputType', {'bliblublu', 'bliblablub'}, ...
	'lengthOutput', [2; 1]);
e2 = misc.Gain('in2', ones(4, 2),...
	'outputType', {'xyz', 'bliblabla'}, ...
	'lengthOutput', [2; 2]);
ee = copy(e1 + e2);
tc.verifyTrue(isequal(ee.remove('in1').gain(1), e2));
end % testRemove()

function testRemoveOutput(tc)
e1 = misc.Gain('in1', ones(3, 2), ...
	'outputType', {'bliblublu', 'bliblablub'}, ...
	'lengthOutput', [2; 1]);
e2 = misc.Gain('in2', ones(4, 2),...
	'outputType', {'xyz', 'bliblabla'}, ...
	'lengthOutput', [2; 2]);
ee = copy(e1 + e2);
ee = ee.removeOutput('xyz');
ee = ee.removeOutput('bliblabla');
tc.verifyTrue(isequal(ee, misc.Gains(e1)))
end % testRemove()

function testStrrepOutputType(tc)
e1 = misc.Gain('in1', ones(3, 2), ...
	'outputType', {'bliblublu', 'bliblablub'}, ...
	'lengthOutput', [2; 1]);
e2 = misc.Gain('in2', ones(4, 2),...
	'outputType', {'xyz', 'bliblabla'}, ...
	'lengthOutput', [2; 2]);
ee = e1 + e2;
e1BLA = strrepOutputType(copy(e1), 'bla', 'BLA');
e2BLA = strrepOutputType(copy(e2), 'bla', 'BLA');
eBLA = strrepOutputType(copy(ee), 'bla', 'BLA');
tc.verifyEqual(e1BLA.outputType, {'bliblublu', 'bliBLAblub'});
tc.verifyEqual(e2BLA.outputType, {'xyz', 'bliBLABLA'});
tc.verifyEqual(eBLA.outputType, {'bliblublu', 'bliBLAblub', 'xyz', 'bliBLABLA'});
end % testStrrepOutputType()

function testEqual(tc)
a = misc.Gain('in1', ones(10),...
	'outputType', {'x', 'y', 'xy', 'X'}, ...
	'lengthOutput', [2; 3; 4; 1]);
b = copy(a);
e1 = misc.Gain('in1', ones(10)+blkdiag(zeros(4), 1, zeros(5)), ...
	'outputType', {'x', 'y', 'xy', 'X'}, ...
	'lengthOutput', [2; 3; 4; 1]);
e2 = misc.Gain('in1', ones(10),...
	'outputType', {'x', 'y', 'xw', 'X'}, ...
	'lengthOutput', [2; 3; 4; 1]);

tc.verifyTrue(isequal(a, b));
tc.verifyFalse(isequal(a, a, e1));
tc.verifyFalse(isequal(a, e2, a));
tc.verifyTrue(isequal(a+b+e1, a+b+e1));
tc.verifyTrue(isequal(a+b+e1, a+e1+b));

% misc.Gains
e12 = misc.Gain('in2', ones(10)+blkdiag(zeros(4), 1, zeros(5)), ...
	'outputType', {'x', 'y', 'xy', 'X'}, ...
	'lengthOutput', [2; 3; 4; 1]);
b2 = misc.Gain('in3', ones(10),...
	'outputType', {'x', 'y', 'xy', 'X'}, ...
	'lengthOutput', [2; 3; 4; 1]);
tc.verifyTrue(isequal(a+b2+e12, a+b2+e12));
tc.verifyFalse(isequal(a+b2+e12, a+e12+b2));
end % testEqual()

function testExchange(tc)
a = misc.Gain('in1', ones(10),...
	'outputType', {'x', 'y', 'xy', 'X'}, ...
	'lengthOutput', [2; 3; 4; 1]);
newY = misc.Gain('in1', 2*ones(3, 10), 'outputType', 'y');
a.exchange(newY);
tc.verifyEqual(a.valueOfOutput('y'), 2*ones(3, 10));
end % testExchange()

function testGainParallel(tc)
a = misc.Gain('in', ones(2),...
	'outputType', {'x', 'y'}, ...
	'lengthOutput', [1; 1]);
b = misc.Gain('in', ones(1, 2),...
	'outputType', {'y'});
c = parallel(a, b);
tc.verifyEqual(c.valueOfOutput('x'), [1, 1]);
tc.verifyEqual(c.valueOfOutput('y'), 2*[1, 1]);

d = parallel(b, a);
tc.verifyEqual(d.valueOfOutput('x'), [1, 1]);
tc.verifyEqual(d.valueOfOutput('y'), 2*[1, 1]);
%d = parallel(b, a);
end % testGainParallel()


function testGain2Gains(tc)
a = misc.Gain('in1', ones(2),...
	'outputType', {'x', 'y'}, ...
	'lengthOutput', [1; 1]);
b = misc.Gain('in2', ones(1, 2),...
	'outputType', {'y'});
c = a + b;
tc.verifyEqual(c.value, [ones(2), [zeros(1, 2); ones(1, 2)]]);
end % testGains2Gains()

function testGains2Gains(tc)
a = misc.Gains(misc.Gain('in', ones(2),...
	'outputType', {'x', 'y'}, ...
	'lengthOutput', [1; 1]));
b = misc.Gains(misc.Gain('in', ones(1, 2),...
	'outputType', {'y'}));
c = a + b;
tc.verifyEqual(c.value, a.value + [zeros(1, 2); b.value]);
end % testGains2Gains()

function testLengthOfOutput(tc)
a = misc.Gain('in1', ones(10),...
	'outputType', {'x', 'y', 'xy', 'X'}, ...
	'lengthOutput', [2; 3; 4; 1]);
tc.verifyEqual(a.lengthOfOutput('x', 'y'), [2; 3])
tc.verifyEqual(a.lengthOfOutput('y', 'x'), [3; 2])
tc.verifyEqual(a.lengthOfOutput('x', 'y', 'xy', 'X'), [2; 3; 4; 1])
tc.verifyEqual(a.lengthOfOutput('X', 'x', 'y', 'xy'), [1; 2; 3; 4])
end % testLengthOfOutput()