function [tests] = testMultArray()
%testQuantity Summary of this function goes here
%   Detailed explanation goes here
tests = functiontests(localfunctions);
end

% function testMixedEqualDim(testCase)
% 
% Z = linspace(0, 2*pi, 501)';
% T = linspace(0, 1, 701);
% 
% [Z, T] = ndgrid(Z, T);
% 
% a = cat(4, cat(3, Z*0, Z .* T, sin(Z)), cat(3, Z.^2 .* cos(T * pi), Z.^3, cos(Z))); % dim = (501, 701, 3, 2)
% z = Z(:,1);
% c = cat(3, [z.^2, z.^2], [cos(z), z], [sin(z), z*0 + 1]) ;  % dim = (501, 2, 3)
% 
% ac = misc.multArray(c, a, 2, 4, 1);    % dim = (100, 3, 3)
% 
% if ~exist('ac.mat', 'file')
% 	save('ac', 'ac', 'a', 'c', 'Z', 'T', 'z');
% end
% 
% AC = load('ac');
% 
% testCase.verifyTrue(numeric.near(AC.ac, ac));
% 
% end

function testMultConstant(testCase)
z = linspace(0, 2*pi)';
a = cat(3, [z*0, z, sin(z)], [z.^2, z.^3, cos(z)]); % dim = (100, 3, 2)
c = rand(2, 3);  % dim = (2, 3)
ac = misc.multArray(a, c, 3, 1);    % dim = (100, 3, 3)

syms Z;
A = [Z*0, Z, sin(Z); Z.^2, Z.^3, cos(Z)]';
A_ = double(subs(A * sym(c), Z, z));
A_ = reshape(A_, length(z), 3, 3);

verifyTrue(testCase, numeric.near(A_, ac, 1e-11));

end

function testVectorMultiplication(testCase)
%%
numpoints = 100;
z = linspace(0, 2*pi,numpoints)';
t = reshape(linspace(0,2*pi),1,1,1,numpoints);
% z i 1 t (like a column vector)
b = [cos(z+t), sin(z+t)];
% z 1 i t (like a row vector)
a = permute(b,[1 3 2 4]);

ab = misc.multArray(a, b, 3, 2, [1 4]); % scalar product
ba = misc.multArray(b, a, 3, 2, [1 4]); % matrix

a_ = shiftdim(a, 1);
b_ = permute(b, [2, 3, 4, 1]);

a_b_ = misc.multArray(a_, b_, 2, 1, [3 4]);
b_a_ = misc.multArray(b_, a_, 2, 1, [3 4]);

% important: test case, where multiplied dimension is at the end, so needs not permutation, but
% the equal dim is not at the beginning so permutation is required.
a__ = permute(a,[1 2 4 3]);
b__ = permute(b,[1 3 4 2]);
a__b__ = misc.multArray(a__, b__, 4,4,[1 3]);

a__ = permute(a,[1 3 4 2]);
b__ = permute(b,[1 2 4 3]);
b__a__ = misc.multArray(a__, b__,4,4,[1 3]);

syms Z T
B = [cos(Z+T); sin(Z+T)];
A = B.';

AB = A*B;
BA = B*A;

%%
tResh = reshape(t,1,numpoints);
z3 = reshape(z,1,1,numpoints);
t4 = reshape(t,1,1,1,numpoints);
verifyTrue(testCase, numeric.near(ab, subs(AB, {Z, T}, {repmat(z,1,numpoints), repmat(tResh,numpoints,1)})));
verifyTrue(testCase, numeric.near(a_b_, subs(AB, {Z T}, {repmat(z,1,numpoints), repmat(tResh,numpoints,1)})));
verifyTrue(testCase, numeric.near(a__b__, subs(AB, {Z T}, {repmat(z,1,numpoints), repmat(tResh,numpoints,1)})));
% the following is numerically more challenging, why a higher tolerance of the default 10*eps is needed.
verifyTrue(testCase, numeric.near(b_a_, permute(double(subs(BA, {Z T}, {repmat(z3,1,1,1,numpoints), repmat(t4,1,1,numpoints,1)})), [3 4 1 2]),100*eps));
verifyTrue(testCase, numeric.near(ba, permute(double(subs(BA, {Z T}, {repmat(z3,1,1,1,numpoints), repmat(t4,1,1,numpoints,1)})), [3 4 1 2]),100*eps));
verifyTrue(testCase, numeric.near(b__a__, permute(double(subs(BA, {Z T}, {repmat(z3,1,1,1,numpoints), repmat(t4,1,1,numpoints,1)})), [3 4 1 2]),100*eps));

end

function test2Variables(testCase)

z = linspace(0, sqrt(2*pi), 31)';
t = linspace(0, sqrt(2*pi), 71);
b = cat(3, cos(z*t), sin(z*t));
a = permute(cat(3, cosh(z*t), sinh(z*t)), [1 2 4 3]);

ab = misc.multArray(a, b, 4, 3, [1 2]);
ba = misc.multArray(b, a, 4, 3, [1 2]);


%%
syms Z T
B = [ cos(Z*T); sin(Z*T) ];
A = [ cosh(Z*T), sinh(Z*T) ];

[tt, zz] = meshgrid(t, z);

testCase.verifyTrue(numeric.near(ab, double(subs(A*B, {Z, T}, {zz, tt})), 1e-9));

BA = B*A;
for k = 1:numel(BA)
    verifyTrue(testCase, numeric.near(ba(:,:,k), double(subs(BA(k), {Z, T}, {zz, tt})), 1e-9));
end

end


function test3variables(testCase)
z = linspace(0, pi, 7);
t = linspace(0, pi, 5);
v = 0:pi/2:3*pi/2;
[z, t] = ndgrid(z, t);
[v, t2] = ndgrid(v, t(1,:));

syms V T Z
bb = [cos(Z), 0; 0, sin(T)];
cc = [cos(T), 2, tan(V*T); 4, sin(V), sin(T)];

[T, Z, V] = ndgrid(t(1,:), z(:,1), v(:,1));
bbbccc = bb * cc;
bbcc = zeros(size(t,2), size(z, 1), size(v,1), 2, 3);
b = zeros(size(z,1), size(t,2), 2, 2);
c = zeros(size(v,1), size(t2, 2), 2, 3);
for k = 1:2
	for l = 1:3
		bbcc(:,:,:,k,l) = subs(bbbccc(k,l), {'T', 'Z', 'V'}, {T, Z, V});
	end
end

for k = 1:numel(bb)
	b(:,:,k) = subs(bb(k), {'Z', 'T'}, {z, t});
end

for k=1:numel(cc)
	c(:,:,k) = subs(cc(k), {'V', 'T'}, {v, t2});
end

bc = misc.multArray(b, c, 4, 3, 2);
bc = permute(bc, [ 1 2 4 3 5 ]);

testCase.verifyTrue(numeric.near(bc, bbcc));

B = quantity.Discrete(b, 'size', [ 2, 2 ], 'gridName', {'z', 't'}, 'grid', {z(:,1), t(1,:)});
C = quantity.Discrete(c, 'size', [ 2, 3 ], 'gridName', {'v', 't'}, 'grid', {v(:,1),  t2(1,:)});

BC = B*C;
bcon = BC.on();

testCase.verifyTrue(numeric.near(bc, BC.on()));

end
































