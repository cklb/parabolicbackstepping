% function [tests] = testTransitionMatrices()
% %TESTTRANSITIONMATRICES Summary of this function goes here
% %   Detailed explanation goes here
% 
% tests = functiontests(localfunctions);
% 
% end
% TODO: Ferdinand
% function testPeanoBaker_sym(testCase)
% 
% syms z
% A = [0 1; 1 0];
% 
% %% compute comparative solution
% P1 = expm(A * z);
% P1 = matlabFunction(P1(:));
% 
% %% compute transition matrix with the peanobaker series
% P2 = misc.fundamentalMatrix.peanobakerseries_sym(A, 0, 20);
% P2 = matlabFunction(P2(:));
% 
% %% evaluate both solutions
% z = linspace(0, 1, 1e4);
% 
% p1 = P1(z);
% p2 = P2(z);
% 
% verifyTrue(testCase, max(max(p1 - p2)) < 1e-15);
% 
% end

% TODO: Ferdinand
% function testRudolphWoittennek(testCase)
% 
% syms z
% A = [0 1; 1 0];
% 
% %% compute comparative solution
% P1 = expm(A * z);
% P1 = matlabFunction(P1(:));
% 
% %% compute transition matrix with the peanobaker series
% P2 = misc.fundamentalMatrix.rudolphWoittennek(A, 0, 20);
% P2 = matlabFunction(P2(:));
% 
% %% evaluate both solutions
% z = linspace(0, 1, 1e4);
% 
% p1 = P1(z);
% p2 = P2(z, 0);
% 
% verifyTrue(testCase, max(max(p1 - p2)) < 1e-15);
% 
% end

% TODO: Ferdinand
% function testPeanoBaker_poly(testCase)
% 
% I = 1;
% syms z zeta
% 
% A0 = [0 1 + z; 1 0];
% A1 = [ 0 1; 0 0];
% A = cat(3, A0, A1);
% B = [1; 1];
% 
% tic
% [Phi.rudolph.sym, F.rudolph.sym, Psi.rudolph.sym, P.rudolph.sym] = misc.fundamentalMatrix.rudolphWoittennek(A, 'N', I, 'B', B);
% toc
% 
% tic
% [Phi.peano.sym, F.peano.sym, Psi.peano.sym, P.peano.sym] = misc.fundamentalMatrix.peanobakerseries_poly(A, 'N', I, 'B', B);
% toc
% 
% Phi.rudolph.fun = matlabFunction(Phi.rudolph.sym(:));
% Phi.peano.fun = matlabFunction(Phi.peano.sym(:));
% 
% 
% z = linspace(0, 1, 1e4);
% 
% p1 = Phi.rudolph.fun(z, 0);
% p2 = Phi.peano.fun(z, 0);
% 
% end





















