function [tests] = testQuantityOpertor()
%TESTQUANTITYOPERTOR Summary of this function goes here
%   Detailed explanation goes here
tests = functiontests(localfunctions);
end

function testTimes(testCase)

%%
syms z s

A0 = [z, z.^2; z.^3, z.^4];
A1 = [sin(z), cos(z); tan(z), sinh(z)];

o = quantity.Operator(A0 + A1*s, 'grid', {linspace(0,1)'});


end

function testDet(testCase)
%%
A = quantity.Operator( cat(3, [0 -1; 1 0], [0  1; 0 0]), 'grid', {0} );

d1 = det(A);
d2 = det(A.valueContinuous);

%%
verifyEqual(testCase, d1.valueContinuous, d2);

end

function testAdj(testCase)
A = quantity.Operator( cat(3, [0 -1; 1 0], [0  1; 0 0]), 'grid', {0} );

a1 = adj(A);
a2 = misc.adj(A.valueContinuous);

%%
verifyEqual(testCase, a1.valueContinuous, a2);
end

function testInitialization(testCase)
A = quantity.Operator( cat(3, [0 -1; 1 0], [0  1; 0 0]), 'grid', {0} );

verifyClass(testCase, A, 'quantity.Operator');

end

function testVariable(testCase)
syms z s
o = quantity.Operator([z * s; 1 + s], 'grid', {linspace(0,1)'});

verifyEqual(testCase, o.variable, z);
verifyEqual(testCase, o.operatorVar, s);
end



function testEvaluate(testCase)
%%
syms z s

A0 = [z, z.^2; z.^3, z.^4];
A1 = [sin(z), cos(z); tan(z), sinh(z)];

o = quantity.Operator(A0 + A1*s, 'grid', {linspace(0,1)'});

value = o.valueDiscrete();
Z = o.grid{1};


%%
for k = 1:2
    for l = 1:2
        verifyLessThan(testCase, max(value(:,k,l,1) - double(subs(A0(k,l), z, Z))), 2 * eps);        
        verifyLessThan(testCase, max(value(:,k,l,2) - double(subs(A1(k,l), z, Z))), 2 * eps);        
    end
end

end

function testUminus(testCase)
%%
syms z s

A0 = [z, z.^2; z.^3, z.^4];
A1 = [sin(z), cos(z); tan(z), sinh(z)];

o = quantity.Operator(A0 + A1*s, 'grid', {linspace(0,1)'});

mo = -o;

value = o.valueDiscrete();
mValue = mo.valueDiscrete();
Z = o.grid{1};

%%
verifyEqual(testCase, value, -mValue);

end