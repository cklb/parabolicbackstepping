classdef testFd < matlab.unittest.TestCase
	% result = runtests('unittests.numeric.testFd')
	
	properties
	end % properties
	
	methods(TestClassSetup)
	end % methods(TestClassSetup)
	
	methods (Test)
		
		function testMatrix5pointCentral(tc)
			testData = linspace(0, 1, 21).'.^2; % testData = x^2;
			% derivative of 1st order
			myD = numeric.fd.matrix('5pointCentral', linspace(0, 1, 21), 1);
			tc.verifyEqual(myD * testData, 2*linspace(0, 1, 21).', 'AbsTol', 1e-6)
			
			% derivative of 2nd order
			myD2 = numeric.fd.matrix('5pointCentral', linspace(0, 1, 21), 2);
			tc.verifyEqual(myD2 * testData, 2*ones(size(testData)), 'AbsTol', 1e-6)
		end % testMatrix5pointCentral()
		
		function testMatrix3pointCentral(tc)
			testData = linspace(0, 1, 21).'.^2; % testData = x^2;
			% derivative of 1st order
			myD = numeric.fd.matrix('3pointCentral', linspace(0, 1, 21), 1);
			tc.verifyEqual(myD * testData, 2*linspace(0, 1, 21).', 'AbsTol', 1e-6)
			
			% derivative of 2nd order
			myD2 = numeric.fd.matrix('3pointCentral', linspace(0, 1, 21), 2);
			tc.verifyEqual(myD2 * testData, 2*ones(size(testData)), 'AbsTol', 1e-6)
		end % testMatrix5pointCentral()
		
		function testStencilBoundary(tc)
			testData = linspace(0, 1, 11).'.^2; % testData = x^2;
			[myStencil, myGridSelector] = numeric.fd.stencilBoundary(...
				2, linspace(0, 1, 11), 1);
			testDataDerivative = myStencil * testData(myGridSelector);
			testDataReferenceResult = gradient(testData, 0.1);
			tc.verifyEqual(testDataDerivative, testDataReferenceResult(end), 'AbsTol', 1e-6)
		end % testSimulationWaveOde()
	end % methods
end % classdef