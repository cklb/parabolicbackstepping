function [tests] = femMatricesTest()
tests = functiontests(localfunctions);
end

function mTest(testCase)
[M] = numeric.femMatrices('grid', linspace(0,1,3));

M_ = [1/6, 1/12 0; ...
	1/12, 1/3, 1/12; ...
	0,	1/12, 1/6];

testCase.verifyEqual( M, M_, 'AbsTol', 10*eps );
end