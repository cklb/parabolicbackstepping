function [tests] = polyMatrixTest()
%UNTITLED Summary of this function goes here
%   Detailed explanation goes here
tests = functiontests(localfunctions);
end

function cTransposeTest(testCase)

	K = magic(4);
	KT = polyMatrix.cTranspose(K);
	
	testCase.verifyEqual(KT, K');
	
	K = cat(3, K(1:2, :), K(3:4, :));
	
	KT = polyMatrix.cTranspose(K);
	
	testCase.verifyEqual(K(:,:,1)', KT(:,:,1))
	testCase.verifyEqual(K(:,:,2)', -KT(:,:,2))

end
