%% Simulation 2:
% System with n=1, time-dependent coefficients, comparison of quasi-static to non-quasi static
% case.

% creates the simulation results for 
% Example 2.5.13

% conI and Lib must be in the current directory
addpath(genpath([pwd '\' 'conI']));
addpath(genpath([pwd '\' 'Lib']));
set(groot,'defaulttextinterpreter','none'); % do not interpret tex-string for export 


%% Paths and names of simulations
myPath = 'Lib/+dps/+parabolic/+mat/+diss/+exampleQuasiStatic/'; % path to save and load mat-files
plotPath = 'Lib/+dps/+parabolic/+plot/+diss/+exampleQuasiStatic/'; % path to save and load mat-files

simSet.name = 'simQuasiStatic';

% Most important resolutions to determine file name
simSet.ndiscKernel = 15;
simSet.ndiscObsKernel = 3;


%% Configure simulation settings
simSet = dps.parabolic.script.diss.simulationSettings.simulationSettingsQuasiStatic(simSet);

%% configure plant
[ppide{1}, lambda,a,mu,a0,F,g_strich] = dps.parabolic.script.diss.examples.exampleQuasiStatic(simSet);
 
%%
plots = [2 3];
export=0;


%% Simulate

%% 1. Simulate Open-Loop:
simpars{1} = {...
		'x0',simSet.x0,...
		'simPars',simSet.sim_pars,...
		'plotPars',simSet.plot_pars,...
		'useReferenceObserver',0,...
		'useDisturbanceObserver',0,...
		'useStateObserver',0,...
		'useStateFeedback',0,...
		'useOutputRegulation',0,...
		'useIntMod',0,...
		'name',simSet.name,...
		'path',myPath
};

if ~exist('simulations','var')
	simulations{1} = dps.parabolic.control.backstepping.ppide_simulation(ppide{1},simpars{1});
else
	if ~isempty(simulations{1}) && isa(simulations{1},'dps.parabolic.control.backstepping.ppide_simulation')
		simulations{1} = simulations{1}.setPars(ppide{1},simpars{1});
	else
		simulations{1} = dps.parabolic.control.backstepping.ppide_simulation(ppide{1},simpars{1});
	end
end
% Normal but resampled mesh:
if simulations{1}.controllerKernelFlag
	simulations{1} = simulations{1}.initializeSimulation();
else
	simulations{1} = simulations{1}.initializeSimulation();
% 	save(simulations{1}.saveStr,'simulations')
end
simulations{1} = simulations{1}.simulate;

%%
%% 1. Simulate Closed-Loop:
simpars{2} = {...
		'x0',simSet.x0,...
		'simPars',simSet.sim_pars,...
		'plotPars',simSet.plot_pars,...
		'useReferenceObserver',0,...
		'useDisturbanceObserver',0,...
		'useStateObserver',0,...
		'useStateFeedback',1,...
		'useOutputRegulation',0,...
		'useIntMod',0,...
		'name',simSet.name,...
		'path',myPath
};
ppide{2} = ppide{1};
ppide{3} = ppide{1};
ppide{3}.ctrl_pars.quasiStatic = 1;
simpars{3} = simpars{2};

for k=plots
	disp(['k=' num2str(k) ':...'])
	if ~exist('simulations','var')
		simulations{k} = dps.parabolic.control.backstepping.ppide_simulation(ppide{k},simpars{k});
	else
		if length(simulations)>k-1 && ~isempty(simulations{k}) && isa(simulations{k},'dps.parabolic.control.backstepping.ppide_simulation')
			simulations{k} = simulations{k}.setPars(ppide{k},simpars{k});
		else
			i=1;
			while i<k
				if ~isempty(simulations{k-i}) && isa(simulations{k-i},'dps.parabolic.control.backstepping.ppide_simulation')
					simulations{k} = simulations{k-i};
					simulations{k} = simulations{k}.setPars(ppide{k},simpars{k});
					break
				end
				i=i+1;
				if i==k % no other simulations is available
					simulations{k} = dps.parabolic.control.backstepping.ppide_simulation(ppide{k},simpars{k});
				end
			end
		end
	end
	if simulations{k}.controllerKernelFlag
		simulations{k} = simulations{k}.initializeSimulation();
	else
		simulations{k} = simulations{k}.initializeSimulation();
% 		save(simulations{k}.saveStr,'simulations')
	end
	if ~simulations{k}.simulated
		simulations{k} = simulations{k}.simulate;
	end
end


%% Export parameters to tex:
if export
texStringargs = {};
for k=plots
	texStringargs = {texStringargs{:},...
			'\Lambda(z)', lambda,...
			'M', mu,...
			'B_{0}^n',simulations{k}.ppide.b_op1.b_n,...
			'B_{0}^d',simulations{k}.ppide.b_op1.b_d,...
			'B_{1}^n',simulations{k}.ppide.b_op2.b_n,...
			'B_{1}^d',simulations{k}.ppide.b_op2.b_d,...
		};
end
opts.filename = [plotPath 'pars.txt'];
opts.digits = 3;
opts.rational = 0;
string = misc.variables2tex(opts,texStringargs{:});
end

%% plot a
figure('Name','a')
plot(linspace(0,1,simulations{1}.ppide.ndiscPars),squeeze(simulations{1}.ppide.a(1,1,1,:)));
xlabel('$t$');
ylabel('$A(t)$');

%% Surface of controller
for k=plots
	rz = simulations{k}.controller.rz;
	figure('Name',['ControllerSurface_' num2str(k)])
	num=1;
	res_surf_t=31;
	res_surf_z=21;
	t_plot{k} = simulations{k}.t;
	zdiscKernel = simulations{k}.controllerKernel.zdisc;
	ndiscKernel = length(zdiscKernel);
	tDiscKernel = linspace(0,1,size(simulations{k}.controllerKernel.value{1,1}.K,3));
	tres = linspace(t_plot{k}(1),t_plot{k}(end),res_surf_t);
	zres = linspace(0,1,res_surf_z);
	[Z,T]=meshgrid(zdiscKernel,tDiscKernel);
	[ZRES,TRES] = meshgrid(zres,tres);
	n = simulations{k}.ppide.n;
	for i=1:n
		for j=1:n
			subplot(n,n,num);
			yres = interp2(Z,T,permute(rz(i,(j-1)*ndiscKernel+1:j*ndiscKernel,:),[3 2 1]),ZRES,TRES);
			surf(TRES,ZRES,yres);
			hold on
			box on
			grid on
			set(gca,'Ydir','reverse')
			xlabel('$t$')
			ylabel('$z$')
			zlabel(['$R_{' num2str(i) num2str(j) '}(z,t)$'])
			num=num+1;
		end
	end
end


%% plot K
for k=plots
	simulations{k}.controllerKernel.plot('K','numPlots',3,'showBoundaries',0,'showGrid',1);
end

% plot u just for info
for k=plots
	simulations{k}.plotU;
end


%% export
if export
relpath = 'plots/exampleTime/~/';
misc.export2tikz(plotPath,'width','0.35\linewidth','height','0.35\linewidth',...
	        'parseStrings',false,...
			'extraAxisOptions',...
			[...'every axis title/.append style={font=\footnotesize},',...'every axis label/.append style={font=\footnotesize},'...'every tick label/.append style={font=\scriptsize},'...'every x tick label/.style={},'...'every y tick label/.style={},'...'every z tick label/.style={},'...'yticklabel style={/pgf/number format/fixed,/pgf/number format/precision=2},'...
			'before end axis/.code={\legend{}},',...
			'every axis plot/.append style={'...
				'contour prepared/.append style={line width={0.5pt},contour/draw color=black},},'...
			'every axis plot post/.append style={table/row sep=newline}'
			],...			
			'floatFormat','%.6g',...			
			'extraTikzpictureOptions',[...
				'execute at begin picture={'...
					'\colorlet{mycolor1}{\mycolA}'...
					'\colorlet{mycolor2}{\mycolB}'...
					'\colorlet{mycolor3}{\mycolC}'...
					'\colorlet{mycolor4}{\mycolD}}'...
			],...
			'externalData',true,...			
			'relativeDataPath',relpath,...			
			'dataPath','./+data/'...			'standalone',true
			);%,...
	close all
end



